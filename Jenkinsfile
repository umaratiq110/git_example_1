#!/usr/bin/env groovy

@Library('pipeline-library@v1.0.0')
import com.isogruppe.webteam.jenkins.*

pipeline {
    agent { label 'docker-slave-kubectl' }
    options {
        skipDefaultCheckout()
        disableConcurrentBuilds()
    }
    environment {
        DOCKER_REGISTRY = "hub.docker.isoad.isogmbh.de:18079"
        BUILD_DIR = ".build/${env.BUILD_ID}"
        SANITIZED_BRANCH_NAME = DnsUtils.sanitizeHostLabelWithFingerprint(env.BRANCH_NAME, 32).toLowerCase()
        CONTAINER_NAME = DnsUtils.sanitizeHostLabelWithFingerprint(env.BUILD_TAG, 32)
        TEST_BASE_DOMAIN = "test.bkz.webteam.isoad.isogmbh.de"
        DEPLOYMENT_HOST = "issnbg6vm46"
        PROD_HOST = "tcp://${env.DEPLOYMENT_HOST}.dc.iso-hosting.net:2376"
        K8S_REGISTRY = "harbor.isonbgvm156.isoad.isogmbh.de"
        FILEBEAT_PASSWORD = credentials('bkz_filebeat_password')
        ELASTICSEARCH_HOST = "https://isonbgvm138.isoad.isogmbh.de:9200"
    }
    stages {
        stage('SCM Checkout') {
            steps {
                script {
                    env.GIT_COMMIT = checkout(scm).GIT_COMMIT[0..10]
                }
            }
        }
        stage('Build and deploy Staging') {
            environment {
                LOCALCONFIG_CREDENTIAL_ID = "${env.BRANCH_NAME == 'master' ? 'bkz_local_ini_staging_postgres' : 'bkz_local_ini_staging_branch_postgres'}"
            }
            steps{
                withDockerServer([uri: "${env.DOCKER_HOST}", credentialsId: 'jenkins-docker-client']) {
                    sh '''
                        docker-compose -f docker-compose.yml -f docker-compose.staging.yml build bkz-frontend
                        docker-compose -f docker-compose.yml -f docker-compose.staging.yml build bkz-backend
                        
                        docker-compose -f docker-compose.yml -f docker-compose.development.yml build bkz-frontend
                        docker-compose -f docker-compose.yml -f docker-compose.development.yml build bkz-backend
                        
                        docker-compose -f docker-compose.yml -f docker-compose.production.yml build bkz-frontend
                        docker-compose -f docker-compose.yml -f docker-compose.production.yml build bkz-backend
                        docker-compose -f docker-compose.yml -f docker-compose.production.yml build filebeat
                    '''
                    withDockerRegistry(url: "https://${env.K8S_REGISTRY}", credentialsId: 'k8s-registry') {
                        sh '''
                            docker-compose -f docker-compose.yml -f docker-compose.staging.yml push
                        '''
                    }
                    withCredentials([
                            file(credentialsId: "${env.LOCALCONFIG_CREDENTIAL_ID}", variable: 'LOCALCONFIG_SECRET_FILE'),
                            file(credentialsId: "kubectl_config", variable: 'KUBECTL_CONFIG'),
                            file(credentialsId: 'ldap_bind_password', variable: 'LDAP_BIND_PASSWORD_SECRET_FILE')
                    ]) {
                        sh 'cp $KUBECTL_CONFIG /home/jenkins/.kube'
                        script {
                            env.BKZ_LOCAL_INI = sh(returnStdout: true, script: 'openssl base64 -A -in $LOCALCONFIG_SECRET_FILE')
                            env.LDAP_BIND_PASSWORD = sh(returnStdout: true, script: 'openssl base64 -A -in $LDAP_BIND_PASSWORD_SECRET_FILE')
                        }
                        sh '''
                            envsubst < deployment/staging/010-deployment.yaml > deployment/staging/010-deployment-replaced.yaml
                            kubectl apply -f deployment/staging/010-deployment-replaced.yaml
                        '''
                    }
                }
            }
        }
        stage('Code analysis') {
            parallel {
                stage("Code analysis - Backend") {
                    steps {
                        withDockerServer([uri: "${env.DOCKER_HOST}", credentialsId: 'jenkins-docker-client']) {
                            sh '''
                                docker run \\
                                    --name ${CONTAINER_NAME}-backend \\
                                    webteam/bkz-backend:${GIT_COMMIT}-dev \\
                                    build/ci/analyse-backend.sh
                                
                                docker cp ${CONTAINER_NAME}-backend:/var/www/html/.build .
                            '''
                        }
                    }
                    post {
                        always {
                            withDockerServer([uri: "${env.DOCKER_HOST}", credentialsId: 'jenkins-docker-client']) {
                                sh '''
                                    docker rm -f ${CONTAINER_NAME}-backend
                                    docker image rm webteam/bkz-backend:${GIT_COMMIT}-dev
                                '''
                            }
                        }
                    }
                }
                stage('Code Analysis - Frontend') {
                    steps {
                        withDockerServer([uri: "${env.DOCKER_HOST}", credentialsId: 'jenkins-docker-client']) {
                            sh '''
                                docker run \\
                                    --name ${CONTAINER_NAME}-frontend \\
                                    webteam/bkz-frontend:${GIT_COMMIT}-dev \\
                                    build/ci/analyse-frontend.sh
                                
                                docker cp ${CONTAINER_NAME}-frontend:/var/www/html/.build .
                            '''
                        }
                    }
                    post {
                        always {
                            withDockerServer([uri: "${env.DOCKER_HOST}", credentialsId: 'jenkins-docker-client']) {
                                sh '''
                                    docker rm -f ${CONTAINER_NAME}-frontend
                                    docker image rm webteam/bkz-frontend:${GIT_COMMIT}-dev
                                '''
                            }
                        }
                    }
                }
            }
            post {
                success {
                    step($class: 'CheckStylePublisher', pattern: '.build/reports/checkstyle-php.xml, .build/reports/checkstyle-js.xml')
                    step($class: 'PmdPublisher', pattern: '.build/reports/pmd.xml')
                    step($class: 'DryPublisher', pattern: '.build/reports/pmd-cpd.xml')
                    step($class: 'AnalysisPublisher', warningsActivated: false)

                    archiveArtifacts '.build/reports/pdepend/*.svg'
                    script {
                        def testUrl = "https://${env.SANITIZED_BRANCH_NAME}.${env.TEST_BASE_DOMAIN}"
                        getContext(hudson.model.Job).setDescription("""
                            <h2>Test system</h2>
                            <ul>
                                <li><a href="${testUrl}/" target="_blank">BKZ Cockpit</a></li>
                                <li><a href="https://mailhog.${env.TEST_BASE_DOMAIN}/" target="_blank">Mailhog</a></li>
                            </ul>
                            <div>
                                <img type="image/svg+xml" style="width:48%; height:auto;" src="${env.BUILD_URL}artifact/.build/reports/pdepend/overview-pyramid.svg"></img>
                                <img type="image/svg+xml" style="width:48%; height:auto;" src="${env.BUILD_URL}artifact/.build/reports/pdepend/dependencies.svg"></img>
                            </div>
                        """)
                    }
                }
            }
        }
        stage('Promotion') {
            agent none
            when {
                branch 'master'
            }
            steps {
                input message: "Deploy to Production (${env.PROD_HOST})?"
            }
        }
        stage("Schedule Downtime") {
            when {
                branch 'master'
            }
            environment {
                APPLICATION_NAME = "INTRANET-POSTGRES_BKZ-COCKPIT"
                ICINGA_PASSWORD = credentials('icinga_password')
            }
            steps {
                script {
                    sh '''inform-ops.sh'''
                }
            }
        }
        stage('Deploy - Production') {
            when {
                branch 'master'
            }
            steps {
                withDockerServer([uri: "${env.DOCKER_HOST}", credentialsId: 'jenkins-docker-client']) {
                    withDockerRegistry(url: "http://${env.DOCKER_REGISTRY}", credentialsId: 'docker-registry') {
                        script {
                            sh '''
                                docker-compose -f docker-compose.yml -f docker-compose.production.yml push
                            '''
                        }
                    }
                }
                withDockerServer([uri: "${env.PROD_HOST}", credentialsId: 'jenkins-docker-client']) {
                    withDockerRegistry(url: "http://${env.DOCKER_REGISTRY}", credentialsId: 'docker-registry') {
                        withCredentials([file(credentialsId: "bkz_local_ini_prod_postgres", variable: 'BKZ_LOCALCONFIG_SECRET_FILE')]) {
                            script {
                                env.BKZ_LOCALCONFIG_SECRET_NAME = DockerUtils.getVersionedSecretName(
                                        'bkz_local_ini_prod_postgres',
                                        readFile(env.BKZ_LOCALCONFIG_SECRET_FILE)
                                )
                            }
                            sh '''
                                docker stack deploy --with-registry-auth -c docker-compose.yml -c docker-compose.production.yml bkz-cockpit
                            '''
                        }
                    }
                }
            }
        }
    }
    post {
        failure {
            emailext(
                subject: "[Jenkins] Build failed: ${env.JOB_NAME} ${env.BUILD_DISPLAY_NAME}",
                body: "See <${env.BUILD_URL}>",
                recipientProviders: [
                    [$class: 'RequesterRecipientProvider'],
                    [$class: 'CulpritsRecipientProvider'],
                    [$class: 'DevelopersRecipientProvider']
                ]
            )
        }
        changed {
            script {
                if (currentBuild.result == 'SUCCESS') {
                    emailext(
                        subject: "[Jenkins] Build is back to normal: ${env.JOB_NAME} ${env.BUILD_DISPLAY_NAME}",
                        body: "See <${env.BUILD_URL}>",
                        recipientProviders: [
                            [$class: 'RequesterRecipientProvider'],
                            [$class: 'CulpritsRecipientProvider'],
                            [$class: 'DevelopersRecipientProvider']
                        ]
                    )
                } else if (currentBuild.result == 'UNSTABLE') {
                    emailext(
                        subject: "[Jenkins] Build is unstable: ${env.JOB_NAME} ${env.BUILD_DISPLAY_NAME}",
                        body: "See <${env.BUILD_URL}>",
                        recipientProviders: [
                            [$class: 'RequesterRecipientProvider'],
                            [$class: 'CulpritsRecipientProvider'],
                            [$class: 'DevelopersRecipientProvider']
                        ]
                    )
                }
            }
        }
    }
}
