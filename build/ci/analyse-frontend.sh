#!/bin/sh

node_modules/.bin/grunt \
    --no-color \
    static-code-analysis

# convert absolute paths to relative paths in report file
sed -i "s|$(pwd)/||g" .build/reports/checkstyle-js.xml
