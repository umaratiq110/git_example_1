#!/bin/sh

mkdir -p \
    .build/reports/ \
    .build/reports/pdepend

set +e
vendor/bin/phpcs \
    --extensions=php,phtml \
    --standard=build/phpcs/ruleset.xml \
    --report=checkstyle \
    --report-file=.build/reports/checkstyle-php.xml \
    --basepath=/var/www/html/ \
    src/

vendor/bin/phpcpd \
    --exclude=data \
    --log-pmd=.build/reports/pmd-cpd.xml \
    src/

# workdir is changed to read config files from CWD
cd build/

../vendor/bin/phpmd \
    ../src/ \
    xml \
    phpmd.xml \
    --exclude data \
    --reportfile ../.build/reports/pmd.xml

../vendor/bin/pdepend \
    --ignore=data \
    --jdepend-xml=../.build/reports/jdepend.xml \
    --jdepend-chart=../.build/reports/pdepend/dependencies.svg \
    --overview-pyramid=../.build/reports/pdepend/overview-pyramid.svg \
    ../src/


cd ../

# convert absolute paths to relative paths in report files
find .build/ -type f -name '*.xml' -exec sed -i "s|$(pwd)/||g" {} \;
