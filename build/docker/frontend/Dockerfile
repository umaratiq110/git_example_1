##
# Build GUI framework
#
FROM poum/senchacmd:6 AS build-sencha

COPY src/public/ /usr/local/src/bkz/src/public

WORKDIR /usr/local/src/bkz/src/public/files
RUN /opt/Sencha/Cmd/6.6.0.13/sencha -sdk ./ext \
    compile -classpath=app \
        page -name=bcgui --input-file ../index.build.html -out /dev/null and \
        restore bcgui and \
        concat bcgui-all-debug.js and \
        concat --compress bcgui-all.js


##
# Production stage
#
FROM nginx:1.15-alpine AS production

# create intranet user/group
RUN addgroup -g 9999 bkz \
    && adduser -DHS -u 9999 -G bkz -h /var/www/html -s /sbin/nologin bkz \
    && sed -i "s|^user  .*|user bkz bkz;|" /etc/nginx/nginx.conf

# forward access log to stdout
RUN rm -f /var/log/nginx/access.log \
    && ln -sf /dev/stdout /var/log/nginx/bkz.log

COPY build/docker/frontend/nginx.conf /etc/nginx/conf.d/default.conf

COPY --from=build-sencha --chown=bkz:bkz /usr/local/src/bkz/src/public/files /var/www/html/files

RUN chown -R bkz:bkz /var/www/html

WORKDIR /var/www/html


FROM production AS staging

RUN sed -i "s/bkz-backend/127.0.0.1/" /etc/nginx/conf.d/default.conf

##
# Development stage
#
FROM production AS development

RUN apk add --no-cache libstdc++

COPY --from=node:8-alpine /usr/local/bin/node /usr/local/bin/node
COPY --from=node:8-alpine /usr/local/include/node/ /usr/local/include/node
COPY --from=node:8-alpine /usr/local/lib/node_modules/ /usr/local/lib/node_modules
COPY --from=node:8-alpine /usr/local/share/doc/node/ /usr/local/share/doc/node
RUN ln -s ../lib/node_modules/npm/bin/npm-cli.js /usr/local/bin/npm \
    && ln -s ../lib/node_modules/npm/bin/npx-cli.js /usr/local/bin/npx

COPY --chown=bkz:bkz package.json package-lock.json Gruntfile.js .eslintrc /var/www/html/
COPY --chown=bkz:bkz build/ci/analyse-frontend.sh /var/www/html/build/ci/analyse-frontend.sh

USER bkz

RUN npm install \
    && chmod -R u+x /var/www/html/build/ci/*.*

USER root
