/*eslint-env node*/
module.exports = function (grunt) {
    var path = require('path');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        env: process.env,
        dirs: {
            config: 'build',
            bin: {
                node: 'node_modules/.bin'
            },
            build: {
                base: '.build',
                current: '<%= env.BUILD_DIR %>'
            },
            reports: '<%= dirs.build.base %>/reports',
            "export": {
                base: '<%= dirs.build.base %>/export',
                current: '<%= dirs.build.current %>/export'
            }
        },
        mkdir: {
            "export": {
                options: {
                    create: [
                        '<%= dirs.export.base %>',
                        '<%= dirs.export.current %>'
                    ]
                }
            },
            report: {
                options: {
                    create: [
                        '<%= dirs.reports %>'
                    ]
                }
            }
        },
        eslint: {
            jslint: {
                options: {
                    format: 'jslint-xml',
                    outputFile: '<%= dirs.reports %>/jslint.xml',
                    silent: true
                },
                src: [
                    'Gruntfile.js',
                    '<%= dirs.source %>/public/files/app/**/*.js',
                    '<%= dirs.source %>/public/files/app/bcgui.js'
                ]
            },
            checkstyle: {
                options: {
                    format: 'checkstyle',
                    outputFile: '<%= dirs.reports %>/checkstyle-js.xml',
                    silent: true
                },
                src: [
                    'Gruntfile.js',
                    '<%= dirs.source %>/public/files/app/**/*.js',
                    '<%= dirs.source %>/public/files/app/bcgui.js'
                ]
            }
        }
    });

    /**
     * NPM tasks
     */
    require('load-grunt-tasks')(grunt);
    grunt.loadNpmTasks("gruntify-eslint");

    grunt.registerTask('static-code-analysis', ['mkdir:report', 'eslint:jslint', 'eslint:checkstyle']);
};
