Ext.application({
    name: 'BC',
    appFolder: 'files/app',

    controllers: ['ProfileList', 'Config', 'General', 'Report'],

    autoCreateViewport: true,

    init: function() {
        // init & setup quicktips
        Ext.tip.QuickTipManager.init();
        Ext.apply(Ext.tip.QuickTipManager.getQuickTip(), {
            // do not hide qtips until mouseleave
            dismissDelay: 0
        });
    },

    launch: function() {
        var loadMask = Ext.select('.x-loading-mask');
        if (loadMask) {
            loadMask.each(function(el) {
                el.remove();
            });
        }
    }
});
