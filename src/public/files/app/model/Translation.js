Ext.define('BC.model.Translation', {
    extend: 'Ext.data.Model',
    fields: ['bezeichnung', 'sprache']
});
