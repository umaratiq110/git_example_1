Ext.define('BC.model.StructureUnitType', {
    extend: 'Ext.data.Model',
    fields: [
         {name: 'id', mapping: 'ID', type: 'int'},
         {name: 'name', mapping: 'NAME', type: 'string'}
    ]
});
