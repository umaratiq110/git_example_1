Ext.define('BC.model.AdditionalCol', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', mapping: 'ID', type: 'int'},
        {name: 'reportType', mapping: 'REPORT_TYPE', type: 'string'},
        {name: 'name', mapping: 'NAME', type: 'string'},
        {name: 'tooltip', mapping: 'TOOLTIP', type: 'string'}
    ]
});
