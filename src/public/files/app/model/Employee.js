Ext.define('BC.model.Employee', {
    extend: 'Ext.data.Model',

    idProperty: 'uid',
    fields: [
        {name: 'uid', type: 'string'},
        {name: 'id', type: 'int'},
        {name: 'text', type: 'string'},
        {name: 'name', type: 'string'},
        {name: 'shortcut', type: 'string'},
        {name: 'first_name', type: 'string'},
        {name: 'last_name', type: 'string'},
        {name: 'description', type: 'string', defaultValue: ''},
        {name: 'checked', type: 'boolean', useNull: true},
        {name: 'leaf', type: 'boolean', defaultValue: false},
        {name: 'locked', type: 'boolean', defaultValue: false},
        {name: 'isGroup', type: 'boolean', defaultValue: false}
    ]
});
