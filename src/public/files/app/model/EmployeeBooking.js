Ext.define('BC.model.EmployeeBooking', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'employee', type: 'string'},
        {name: 'emptyBookingDates', type: 'string'}
    ]
});
