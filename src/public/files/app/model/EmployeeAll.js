Ext.define('BC.model.EmployeeAll', {
    extend: 'Ext.data.Model',
    fields: ['ID', 'SHORTCUT', 'NAME']
});
