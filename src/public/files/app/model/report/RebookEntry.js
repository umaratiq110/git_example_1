Ext.define('BC.model.report.RebookEntry', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'bookingId',
        type: 'int'
    }, {
        name: 'date',
        type: 'date',
        dateFormat: 'd.m.Y'
    }, {
        name: 'employee',
        type: 'string'
    }, {
        name: 'ccId',
        type: 'int'
    }, {
        name: 'cc',
        type: 'string'
    }, {
        name: 'hours',
        type: 'float'
    }, {
        name: 'bookingType',
        type: 'string'
    }, {
        name: 'defaultBookingType',
        type: 'string'
    }, {
        name: 'note',
        type: 'string'
    }, {
        name: 'newNote',
        type: 'string'
    }, {
        name: 'pdbIds'
    }, {
        name: 'isHoliday',
        type: 'boolean'
    }, {
        name: 'canOnlyRebookedByFinance',
        type: 'boolean'
    }, {
        name: 'isAfterLockOfTargetCC',
        type: 'boolean'
    }]
    // NOTE: All available booking types columns are added to the model dynamically
});
