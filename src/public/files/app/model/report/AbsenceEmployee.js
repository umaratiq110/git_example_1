Ext.define('BC.model.report.AbsenceEmployee', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'id', type: 'int'},
        {name: 'employee_id', type: 'int'},
        {name: 'shortcut', type: 'string'},
        {name: 'first_name', type: 'string'},
        {name: 'last_name', type: 'string'},
        {name: 'group', type: 'string'}
    ],

    hasMany: [
        {model: 'BC.model.report.AbsenceEntry', name: 'absences'}
    ]
});
