Ext.define('BC.model.report.ListEntry', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'presenceFrom', mapping: 'PRESENCE_FROM', type: 'date', dateFormat: 'H:i'},
        {name: 'presenceTo', mapping: 'PRESENCE_TO', type: 'date', dateFormat: 'H:i'},
        {name: 'bookingBegin', mapping: 'BOOKING_BEGIN', type: 'date', dateFormat: 'H:i'},
        {name: 'bookingEnd', mapping: 'BOOKING_END', type: 'date', dateFormat: 'H:i'},
        {name: 'date', mapping: 'CDATE', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'employee', mapping: 'SHORTCUT', type: 'string'},
        {name: 'firstName', mapping: 'FIRST_NAME', type: 'string'},
        {name: 'lastName', mapping: 'LAST_NAME', type: 'string'},
        {name: 'companyName', mapping: 'COMPANY_NAME', type: 'string'},
        {name: 'divisionName', mapping: 'DIVISION_NAME', type: 'string'},
        {name: 'dayType', mapping: 'DAY_TYPE', type: 'int'},
        {name: 'costcentre', mapping: 'NAME', type: 'string'},
        {name: 'hours', mapping: 'CHOURS', type: 'float'},
        {name: 'note', mapping: 'CNOTE', type: 'string'},
        {name: 'bookingType', mapping: 'BOOKING_TYPE'},
        {name: 'pdbIds', mapping: 'PDBIDS'},
        {name: 'break', mapping: 'BREAK', type: 'float'}
    ]
});
