Ext.define('BC.model.report.AbsenceEntry', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'id', type: 'int'},
        {name: 'start_date', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'end_date', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'start_time', type: 'string'},
        {name: 'end_time', type: 'string'},
        {name: 'category_id', type: 'int'},
        {name: 'description', type: 'string'}
    ],

    belongsTo: 'BC.model.report.AbsenceEmployee'
});
