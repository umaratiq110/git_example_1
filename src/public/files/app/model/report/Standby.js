Ext.define('BC.model.report.Standby', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'company',
        mapping: 'COMPANY',
        type: 'string'
    }, {
        name: 'division',
        mapping: 'DIVISION',
        type: 'string'
    }, {
        name: 'employeeShortcut',
        mapping: 'EMP_SHORTCUT',
        type: 'string'
    }, {
        name: 'employeeName',
        mapping: 'EMP_NAME',
        type: 'string'
    }, {
        name: 'dayType',
        mapping: 'DAY_TYPE',
        type: 'string',
        sortType: function(value) {
            return BC.translate('dayType_' + value);
        }
    }, {
        name: 'startDate',
        mapping: 'START_DATE',
        type: 'date',
        dateFormat: 'd.m.Y'
    }, {
        name: 'startTime',
        mapping: 'START_TIME',
        type: 'date',
        dateFormat: 'H:i'
    }, {
        name: 'startDateTime',
        convert: function(v, record) {
            var date = Ext.Date.clearTime(record.get('startDate'), true);

            date.setHours(record.get('startTime').getHours());
            date.setMinutes(record.get('startTime').getMinutes());

            return date;
        },
        persist: false
    }, {
        name: 'hoursActual',
        mapping: 'HOURS_ACTUAL',
        type: 'float'
    }, {
        name: 'cc',
        mapping: 'CC',
        type: 'string'
    }, {
        name: 'note',
        mapping: 'NOTE',
        type: 'string'
    }]
});
