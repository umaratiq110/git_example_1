Ext.define('BC.model.report.RebookLog', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'rebooking_id',
        mapping: 'ID',
        type: 'string'
    }, {
        name: 'rebooking_date',
        mapping: 'REBOOKING_DATE',
        type: 'string'
    }, {
        name: 'booking_date',
        mapping: 'BOOKING_DATE',
        type: 'string'
    }, {
        name: 'requestor',
        mapping: 'REQUESTOR',
        type: 'string'
    }, {
        name: 'performer',
        mapping: 'PERFORMER',
        type: 'string'
    }, {
        name: 'reason',
        mapping: 'REASON',
        type: 'string'
    }, {
        name: 'hours',
        mapping: 'HOURS',
        type: 'string'
    }, {
        name: 'new_booking_type',
        mapping: 'NEW_BOOKING_TYPE',
        type: 'string'
    }, {
        name: 'old_booking_type',
        mapping: 'OLD_BOOKING_TYPE',
        type: 'string'
    }, {
        name: 'cc_old',
        mapping: 'CC_OLD',
        type: 'string'
    }, {
        name: 'cc_new',
        mapping: 'CC_NEW',
        type: 'string'
    }, {
        name: 'old_note',
        mapping: 'OLD_NOTE',
        type: 'string'
    }, {
        name: 'note',
        mapping: 'NOTE',
        type: 'string'
    }, {
        name: 'booking_employee',
        mapping: 'BOOKING_EMPLOYEE',
        type: 'string'
    }, {
        name: 'sum_of_modified_totals',
        mapping: 'SUM_OF_MODIFIED_TOTALS',
        type: 'float'
    }]
});
