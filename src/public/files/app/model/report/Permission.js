Ext.define('BC.model.report.Permission', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'employeeName',
        mapping: 'EMPLOYEENAME',
        type: 'string'
    }, {
        name: 'permissionKind',
        mapping: 'PERMISSIONKIND',
        type: 'string'
    }, {
        name: 'value',
        mapping: 'VALUE',
        type: 'string',
        defaultValue: ''
    }, {
        name: 'nodeID',
        mapping: 'NODEID',
        type: 'int',
        defaultValue: 0
    }]
});
