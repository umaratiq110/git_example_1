Ext.define('BC.model.report.AbsenceCategory', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'id', type: 'int', mapping: 'ID'},
        {name: 'name', type: 'string', mapping: 'NAME'},
        {name: 'color', type: 'string', mapping: 'COLOR'},
        {name: 'striped', type: 'bool', mapping: 'STRIPED'},
        {name: 'priority', type: 'int', mapping: 'PRIORITY'}
    ]
});
