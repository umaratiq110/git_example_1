Ext.define('BC.model.report.Table', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'name',
        type: 'string'
    }, {
        name: 'isHeader',
        type: 'boolean',
        defaultValue: false
    }, {
        name: 'isTotal',
        type: 'boolean',
        defaultValue: false
    }, {
        name: 'sapKey',
        type: 'string'
    }, {
        name: 'total'
    }, {
        name: 'targetEffort',
        convert: function(value, record) {
            if (record.get('isHeader')) {
                return value;
            }

            return value ? value.toFixed(2) : '';
        }
    }, {
        name: 'percent',
        convert: function(value) {
            return (value ? value : '0') + ' %';
        }
    }],

    hasMany: [
        {name: 'xAxisData', model: 'BC.model.report.TableAxisData'}
    ]
});
