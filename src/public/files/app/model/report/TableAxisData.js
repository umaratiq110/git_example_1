Ext.define('BC.model.report.TableAxisData', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'shortcut',
        type: 'string'
    }, {
        name: 'firstName',
        type: 'string'
    }, {
        name: 'lastName',
        type: 'string'
    }, {
        name: 'sapMaterialNumber',
        type: 'string'
    }, {
        name: 'month',
        type: 'string'
    }, {
        name: 'name',
        persist: false,
        convert: function(value, record) {
            if (record.get('shortcut')) {
                value = record.get('shortcut');
                if (record.get('sapMaterialNumber')) {
                    value += '<br />(' + record.get('sapMaterialNumber') + ')';
                }
            } else if (record.get('month')) {
                value = record.get('month');
            }

            return value;
        }
    }, {
        name: 'tooltip',
        persist: false,
        convert: function(value, record) {
            if (record.get('shortcut')) {
                return record.get('firstName') + ' ' + record.get('lastName');
            }
            return '';
        }
    }, {
        name: 'hours',
        type: 'float'
    }, {
        name: 'hoursAL',
        type: 'float'
    }, {
        name: 'isTotal',
        type: 'boolean',
        defaultValue: false
    }]
});
