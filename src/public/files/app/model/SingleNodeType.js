Ext.define('BC.model.SingleNodeType', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'name', mapping: 'NAME', type: 'string'},
        {name: 'value', mapping: 'VALUE', type: 'int'},
        {name: 'forceChildSelection', type: 'bool', convert: function(value, record) {
            switch (record.data.name) {
                case 'MIS_INTERNAL':
                case 'MIS_SALES':
                    return true;
            }
            return false;
        }}
    ]
});
