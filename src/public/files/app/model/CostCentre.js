Ext.define('BC.model.CostCentre', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'id', type: 'int', mapping: 'ID'},
        {name: 'name', type: 'string', mapping: 'NAME'},
        {name: 'bookable', type: 'bool', mapping: 'BOOKABLE'},
        {name: 'notYetBookable', type: 'bool', mapping: 'NOT_YET_BOOKABLE'},
        {name: 'bookingTypeId', type: 'int', mapping: 'BOOKING_TYPE_ID'},
        {name: 'lastAccountingDate', type: 'date', mapping: 'LAST_ACCOUNTING_DATE', dateFormat: 'd.m.Y'},
        {name: 'lockDate', type: 'date', mapping: 'LOCK_DATE', dateFormat: 'd.m.Y'}
    ]
});
