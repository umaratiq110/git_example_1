Ext.define('BC.model.Division', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', mapping: 'ID', type: 'int'},
        {name: 'name', mapping: 'NAME', type: 'string'},
        {name: 'longName', mapping: 'LONG_NAME', type: 'string'},
        {name: 'active', mapping: 'ACTIVE', type: 'int'},
        {
            name: 'displayName',
            persist: false,
            convert: function(value, record) {
                return record.get('name') + ' -- ' + record.get('longName');
            }
        }
    ]
});
