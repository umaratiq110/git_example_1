Ext.define('BC.model.BtModifications', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'modification',
        mapping: 'MODIFICATION',
        type: 'string'
    }, {
        name: 'datetime',
        mapping: 'DATETIME',
        type: 'string'
    }, {
        name: 'node_name',
        mapping: 'NODE_NAME',
        type: 'string'
    },
    {
        name: 'node_type',
        mapping: 'NODE_TYPE',
        type: 'string'
    }, {
        name: 'is_leaf',
        mapping: 'IS_LEAF',
        type: 'int'
    }, {
        name: 'authorized',
        mapping: 'AUTHORIZED',
        type: 'string'
    }, {
        name: 'sap_key_cost_object',
        mapping: 'SAP_KEY_COST_OBJECT',
        type: 'string'
    }, {
        name: 'sap_key_cost_center',
        mapping: 'SAP_KEY_COST_CENTER',
        type: 'string'
    }, {
        name: 'node_id',
        mapping: 'ID',
        type: 'int'
    }]
});
