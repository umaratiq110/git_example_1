Ext.define('BC.model.status.LockedNode', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'name', mapping: 'NAME', type: 'string'},
        {name: 'value', mapping: 'VALUE', type: 'int'}
    ]
});
