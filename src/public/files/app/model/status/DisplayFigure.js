Ext.define('BC.model.status.DisplayFigure', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'name', mapping: 'NAME', type: 'string'},
        {name: 'value', mapping: 'VALUE', type: 'int'}
    ]
});
