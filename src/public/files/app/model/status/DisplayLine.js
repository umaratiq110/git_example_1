Ext.define('BC.model.status.DisplayLine', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'name', mapping: 'NAME', type: 'string'},
        {name: 'value', mapping: 'VALUE', type: 'int'}
    ]
});
