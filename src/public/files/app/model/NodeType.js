Ext.define('BC.model.NodeType', {
    extend: 'Ext.data.Model',
    fields: [
         {name: 'id', mapping: 'ID', type: 'int'},
         {name: 'shortcut', mapping: 'SHORTCUT', type: 'string'},
         {name: 'name', mapping: 'NAME', type: 'string'}
    ]
});
