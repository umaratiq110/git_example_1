Ext.define('BC.model.Profile', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'id', type: 'int'},
        {name: 'name', type: 'string'},
        {name: 'hint', type: 'string', defaultValue: null},
        {name: 'showToUsers'},
        {name: 'shareWithUsers'},
        {name: 'limit', type: 'float'},
        {name: 'limitexceeded', type: 'boolean'},
        {name: 'act', type: 'float', useNull: true},
        {name: 'type', type: 'string', sortType: function(value) {
            return BC.translateReport(value);
        }},
        {name: 'mail'},
        {name: 'reportMail'},
        {name: 'own', type: 'boolean', defaultValue: true},
        {name: 'startValue'},
        {name: 'from', type: 'date', dateFormat: 'Y-m-d H:i:s'},
        {name: 'to', type: 'date', dateFormat: 'Y-m-d H:i:s'},
        {name: 'dynamicPeriod', type: 'int'},
        {name: 'displayPeriod', sortType: function(value) {
            switch (value.dynamicPeriod) {
                case '1': return BC.translate('currMonth');
                case '2': return BC.translate('lastMonthh');
                case '3': return BC.translate('currrentWeek');
                case '4': return BC.translate('lastWeek');
                case '5': return BC.translate('currentDay');
                case '6': return BC.translate('lastDay');
                case '7': return BC.translate('currentQuarter');
                case '8': return BC.translate('lastQuarter');
                case '9': return BC.translate('currentYear');
                case '10': return BC.translate('lastYear');
                case '11': return BC.translate('all');
                case '12': return BC.translate('next6months');
                case '13': return BC.translate('next12months');
                default: return value.from ? value.from : '---';
            }
        }},
        {name: 'monitoredUsers'},
        {name: 'hoursNote', type: 'string'},
        {name: 'diagramType', type: 'string'},
        {name: 'ccWithHourBookings', type: 'int'},
        {name: 'xAxis', type: 'string'},
        {name: 'showEmpBookings', type: 'int'},
        {name: 'showCostAndRevenue', type: 'int'},
        {name: 'showBTHours', type: 'int'},
        {name: 'showTargetHours', type: 'int'},
        {name: 'showBookingsWithBeginEnd', type: 'int'},
        {name: 'showRemoteWorkDays', type: 'int'},
        {name: 'showEmployeesWithoutBCPermission', type: 'int'},
        {name: 'showEmployeesAsGroups', type: 'int'},
        {name: 'showEmployeesWithAbsences', type: 'int'},
        {name: 'showPerformedOrBookingDate', type: 'int'},
        {name: 'showWithLockedNodes', type: 'int'},
        {name: 'divisionId', type: 'int'},
        {name: 'targetHourlyRate', type: 'int'},
        {name: 'showClosedOrders', type: 'int'},
        {name: 'showOrdersAfterPeriod', type: 'int'},
        {name: 'showWithoutOrdersAfterPeriod', type: 'int'},
        {name: 'showWithHigherLevelPosition', type: 'int'},
        {name: 'showExternalEmployees', type: 'int'},
        {name: 'nodes'},
        {name: 'employees'},
        {name: 'bookingTypes'},
        {name: 'additionalCols'},
        {name: 'showZeroHoursPerWeek'},
        {name: 'showOneLinePerEmployee'},
        {name: 'showOneTabPerCompany'},
        {name: 'displayFigures', type: 'int'},
        {name: 'displayLines', type: 'int'},
        {name: 'displayOnlySubtrees'},
        {name: 'contingentDataReportType', type: 'int'},
        {name: 'startValue'},
        {name: 'sharedProfile', type: 'boolean', defaultValue: false},
        {name: 'hasRootNodePermission', type: 'boolean', defaultValue: false},
        {name: 'materialFilter'}
    ]
});
