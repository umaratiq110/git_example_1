Ext.define('BC.model.Node', {
    extend: 'Ext.data.Model',

    idProperty: 'uid',
    fields: [
        {name: 'uid', type: 'string'},
        {name: 'id', type: 'int', useNull: true},
        {name: 'name', type: 'string'},
        {name: 'text', type: 'string'},
        {name: 'iconCls', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'path_id', type: 'string', useNull: true},
        {name: 'path_name', type: 'string', useNull: true},
        {name: 'sapKeyCostCenter', type: 'string', useNull: true},
        {name: 'sapKeyCostUnit', type: 'string', useNull: true},
        {name: 'debtorNumber', type: 'string', useNull: true},
        {name: 'targetEffort', type: 'string', useNull: true},
        {name: 'nodeTypeId', type: 'int', useNull: true},
        {name: 'structureUnitTypeId', type: 'int', useNull: true},
        {name: 'bookingTypeId', type: 'int', useNull: true},
        {name: 'singleNodeTypes', type: 'int', useNull: true},
        {name: 'checked', type: 'boolean', useNull: true},
        {name: 'leaf', type: 'boolean', defaultValue: false},
        {name: 'disabled', type: 'boolean', defaultValue: false},
        {name: 'locked', type: 'boolean', defaultValue: false},
        {name: 'bookingAllowed', type: 'boolean', defaultValue: true},
        {name: 'isSet', type: 'boolean', defaultValue: false},
        {name: 'isHoliday', type: 'boolean', defaultValue: false}
    ]
    // TODO: use hasOne associations for nodeTypeId, structureUnitTypeId and bookingTypeId
    // as of ExtJS 4.2.2, such associations cause problems if they can be nullable
});
