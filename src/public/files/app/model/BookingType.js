Ext.define('BC.model.BookingType', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', mapping: 'ID', type: 'int'},
        {name: 'name', mapping: 'NAME', type: 'string'},
        {name: 'longName', mapping: 'LONG_NAME', type: 'string'},
        {name: 'information', mapping: 'INFORMATION_NAME', type: 'string'},
        {
            name: 'displayName',
            persist: false,
            convert: function(value, record) {
                return record.get('name') + ' (' + record.get('longName') + ')';
            }
        },
        {
            name: 'displayInformation',
            persist: false,
            convert: function(value, record) {
                return record.get('information') + ' (' + record.get('name') + ')';
            }
        }
    ],
    proxy: {
        type: 'memory'
    }
});
