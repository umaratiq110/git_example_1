Ext.define('BC.model.MaterialFilter', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int', mapping: 'ID'},
        {name: 'name', type: 'string', mapping: 'NAME'},
    ]
});