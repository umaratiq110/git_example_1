Ext.define('BC.model.SAPKeyHours', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'sapkey',
        mapping: 'SAP_KEY',
        type: 'string',
        defaultValue: ''
    }, {
        name: 'nodetype',
        mapping: 'SAP_KEY_NODE_TYPE',
        type: 'string',
        defaultValue: ''
    }, {
        name: 'name',
        mapping: 'SAP_KEY_NAME',
        type: 'string',
        defaultValue: ''
    }, {
        name: 'employee',
        mapping: 'SHORTCUT',
        type: 'string'
    }, {
        name: 'employeenumber',
        mapping: 'SAP_MATERIAL_NUMBER',
        type: 'string'
    }, {
        name: 'total',
        mapping: 'TOTALS',
        type: 'float'
    }]
    // NOTE: All avaliable booking types columns are added to the model dynamically
});
