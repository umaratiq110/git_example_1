Ext.define('BC.model.AbleToShareEmployees', {
    extend: 'Ext.data.Model',
    fields: ['ID', 'SHORTCUT', 'NAME']
});
