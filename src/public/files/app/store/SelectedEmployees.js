Ext.define('BC.store.SelectedEmployees', {
    extend: 'Ext.data.Store',

    model: 'BC.model.Employee',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});
