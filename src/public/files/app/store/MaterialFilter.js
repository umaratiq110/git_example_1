Ext.define('BC.store.MaterialFilter', {
    extend: 'Ext.data.Store',
    model: 'BC.model.MaterialFilter',
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'config/getmaterialfilter',
        reader: {
            type: 'json'
        }
    }
});
