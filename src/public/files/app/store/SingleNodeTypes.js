Ext.define('BC.store.SingleNodeTypes', {
    extend: 'Ext.data.Store',

    model: 'BC.model.SingleNodeType',
    data: global.singleNodeTypes,
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },

    load: function() {
        this.callParent(arguments);

        this.each(function(record) {
            var obj = {};
            obj[record.get('name')] = record.get('value');

            this.addStatics(obj);
        }, this.self);
    }
});
