Ext.define('BC.store.Translations', {
    extend: 'Ext.data.Store',
    model: 'BC.model.Translation',

    data: translations,

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            root: 'translations'
        }
    },

    translation: function(desc, modification) {
        var index = this.findExact('bezeichnung', desc),
            translation;

        if (index === -1) {
            return "TRANSLATION '" + desc + "' NOT FOUND!";
        }

        translation = this.getAt(index).get('sprache');

        if (modification) {
            switch (modification) {
                case "ucfirst" :
                    translation = translation.replace(/^./, translation.match(/^./)[0].toUpperCase());
                    break;
                case "lcfirst" :
                    translation = translation.replace(/^./, translation.match(/^./)[0].toLowerCase());
                    break;
                case "uc" :
                    translation = translation.toUpperCase();
                    break;
                case "lc" :
                    translation = translation.toLowerCase();
                    break;
                case "cb" :
                    var div = document.createElement('div');
                    div.innerHTML = translation;
                    translation = div.innerHTML;
                    break;
            }
        }

        return translation;
    }
});
