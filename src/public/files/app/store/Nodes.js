Ext.define('BC.store.Nodes', {
    extend: 'Ext.data.TreeStore',
    model: 'BC.model.Node',

    root: {
        uid: 'root',
        id: null,
        text: ".",
        expanded: true,
        children: global.tree.nodes
    },
    proxy: {
        type: 'ajax',
        url: 'tree/getnode',
        reader: {
            type: 'json'
        }
    }
});
