Ext.define('BC.store.PrismaEmployeesAll', {
    extend: 'Ext.data.Store',
    model: 'BC.model.PrismaEmployeeAll',
    data: global.employees,
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});
