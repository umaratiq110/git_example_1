Ext.define('BC.store.CostCentres', {
    extend: 'Ext.data.Store',
    model: 'BC.model.CostCentre',
    data: global.costcentres.user,
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});
