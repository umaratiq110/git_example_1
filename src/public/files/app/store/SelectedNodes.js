Ext.define('BC.store.SelectedNodes', {
    extend: 'Ext.data.Store',

    model: 'BC.model.Node',

    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },

    sorters: [
        {property: 'isSet', direction: 'ASC'},
        {property: 'path_name', direction: 'ASC'}
    ]
});
