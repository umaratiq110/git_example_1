Ext.define('BC.store.EmployeesAll', {
    extend: 'Ext.data.Store',
    model: 'BC.model.EmployeeAll',
    data: global.bcUsers,
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});
