Ext.define('BC.store.report.AbsenceEmployees', {
    extend: 'Ext.data.Store',

    model: 'BC.model.report.AbsenceEmployee',
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});
