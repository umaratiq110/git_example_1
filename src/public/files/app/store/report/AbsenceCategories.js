Ext.define('BC.store.report.AbsenceCategories', {
    extend: 'Ext.data.Store',

    model: 'BC.model.report.AbsenceCategory',
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});
