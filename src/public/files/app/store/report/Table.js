Ext.define('BC.store.report.Table', {
    extend: 'Ext.data.Store',
    model: 'BC.model.report.Table',
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});
