Ext.define('BC.store.report.ListEntries', {
    extend: 'Ext.data.Store',

    requires: ['BC.lib.PagingMemoryProxy'],

    model: 'BC.model.report.ListEntry',
    proxy: {
        type: 'pagingmemory',
        reader: {
            type: 'json'
        }
    },

    sort: function() {
        var sorters;
        if (arguments.length === 1
            && arguments[0] instanceof Object
            && arguments[0].property !== undefined
        ) {
            // add secondary sorters when store is sorted via grid
            sorters = [arguments[0]];
            Ext.each(['date', 'employee', 'costcentre'], function(p) {
                if (p !== this[0].property) {
                    sorters.push({property: p, direction: 'ASC'});
                }
            }, arguments);
            arguments[0] = sorters;

            this.fireEvent('serversortchange');
        }
        this.callParent(arguments);
    },

    buffered: true,
    remoteSort: true,
    remoteFilter: true,
    // use high values, since the data is already available in the memory proxy
    pageSize: 100000,
    trailingBufferZone: 100000,
    leadingBufferZone: 100000,
    purgePageCount: 0
});
