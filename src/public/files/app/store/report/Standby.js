Ext.define('BC.store.report.Standby', {
    extend: 'Ext.data.Store',

    requires: ['BC.lib.PagingMemoryProxy'],

    model: 'BC.model.report.Standby',
    proxy: {
        type: 'pagingmemory',
        reader: {
            type: 'json'
        }
    },

    buffered: true,
    remoteSort: true,
    remoteFilter: true,
    // use high values, since the data is already available in the memory proxy
    pageSize: 100000,
    trailingBufferZone: 100000,
    leadingBufferZone: 100000,
    purgePageCount: 0,

    sorters: [
        {property: 'company', direction: 'ASC'},
        {property: 'employeeShortcut', direction: 'ASC'},
        {property: 'startDateTime', direction: 'ASC'}
    ],

    sortOnLoad: true
});
