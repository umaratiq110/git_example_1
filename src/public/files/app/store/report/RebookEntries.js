Ext.define('BC.store.report.RebookEntries', {
    extend: 'Ext.data.Store',

    model: 'BC.model.report.RebookEntry',
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});
