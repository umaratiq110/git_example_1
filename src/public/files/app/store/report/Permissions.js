Ext.define('BC.store.report.Permissions', {
    extend: 'Ext.data.Store',

    model: 'BC.model.report.Permission',
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },

    sorters: [
        {property: 'employeeName', direction: 'ASC'}
    ],

    sortOnLoad: true
});
