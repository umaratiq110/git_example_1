Ext.define('BC.store.Profiles', {
    extend: 'Ext.data.Store',
    model: 'BC.model.Profile',
    autoLoad: true,
    id: 'ProfilesStore',
    proxy: {
        type: 'ajax',
        url: 'index/loadprofiles',
        reader: {
            type: 'json',
            root: 'profiles'
        }
    },
    defaultProfile: null,
    listeners: {
        beforeload: function() {
            this.clearFilter();
        },
        load: function() {
            // store default profile locally and remove from store by filter (why? dunno...)
            this.defaultProfile = this.getAt(this.findExact('name', ''));
            this.filter({
                property: "name",
                value: /.+/
            });
        }
    }
});
