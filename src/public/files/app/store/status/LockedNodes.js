Ext.define('BC.store.status.LockedNodes', {
    extend: 'Ext.data.Store',
    model: 'BC.model.status.LockedNode',
    data: global.lockedNodes,
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },

    load: function() {
        this.callParent(arguments);

        this.each(function(record) {
            var obj = {};
            obj[record.get('name')] = record.get('value');

            this.addStatics(obj);
        }, this.self);
    }

});
