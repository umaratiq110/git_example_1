Ext.define('BC.store.status.DisplayLines', {
    extend: 'Ext.data.Store',
    model: 'BC.model.status.DisplayLine',
    data: global.displayLines,
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },

    load: function() {
        this.callParent(arguments);

        this.each(function(record) {
            var obj = {};
            obj[record.get('name')] = record.get('value');

            this.addStatics(obj);
        }, this.self);
    }

});
