Ext.define('BC.store.BookingTypes', {
    extend: 'Ext.data.Store',
    model: 'BC.model.BookingType',
    data: global.bookingTypes,
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});
