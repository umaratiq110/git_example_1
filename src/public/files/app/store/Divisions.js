Ext.define('BC.store.Divisions', {
    extend: 'Ext.data.Store',
    model: 'BC.model.Division',
    data: global.divisions,
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },
    filters: [
       {property: "active", value: 1}
    ]
});
