Ext.define('BC.store.Employees', {
    extend: 'Ext.data.TreeStore',
    model: 'BC.model.Employee',
    root: {id: 0, expanded: true, text: ".", children: global.tree.employees}
});
