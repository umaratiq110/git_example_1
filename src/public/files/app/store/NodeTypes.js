Ext.define('BC.store.NodeTypes', {
    extend: 'Ext.data.Store',
    model: 'BC.model.NodeType',
    data: global.nodeTypes,
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});
