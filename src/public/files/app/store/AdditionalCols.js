Ext.define('BC.store.AdditionalCols', {
    extend: 'Ext.data.Store',
    model: 'BC.model.AdditionalCol',
    data: global.additionalCols,
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});
