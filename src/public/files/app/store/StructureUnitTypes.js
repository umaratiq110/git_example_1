Ext.define('BC.store.StructureUnitTypes', {
    extend: 'Ext.data.Store',

    model: 'BC.model.StructureUnitType',

    data: global.structureUnitTypes,
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});
