Ext.define('BC.store.AbleToShareEmployees', {
    extend: 'Ext.data.Store',
    model: 'BC.model.AbleToShareEmployees',
    data: global.ableToShareEmployees,
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    }
});
