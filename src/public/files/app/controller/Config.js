Ext.define('BC.controller.Config', {
    extend: 'Ext.app.Controller',
    views: [
        'MainPanel',
        'config.ConfigPanel',
        'config.ReportConfiguration',
        'config.SelectedEmployeesDialog',
        'config.SelectedNodesDialog',
        'config.MaterialFilter'
    ],

    stores: [
        'Profiles', 'Employees', 'EmployeesAll', 'AbleToShareEmployees',
        'Nodes', 'NodeTypes', 'StructureUnitTypes', 'BookingTypes', 'SingleNodeTypes',
        'SelectedNodes', 'SelectedEmployees', 'AdditionalCols', 'Divisions',
        'status.DisplayFigures', 'status.LockedNodes', 'status.DisplayLines', 'MaterialFilter'
    ],

    models: [
        'Profile', 'Employee', 'EmployeeAll',
        'Node', 'NodeType', 'StructureUnitType', 'BookingType', 'SingleNodeType',
        'AdditionalCol', 'Division', 'status.DisplayFigure', 'status.LockedNode', 'status.DisplayLine',
        'MaterialFilter'
    ],

    refs: [{
        ref: 'mainPanel',
        selector: 'mainPanel'
    }, {
        ref: 'configPanel',
        selector: 'configPanel'
    }, {
        ref: 'repConf',
        selector: 'reportConfiguration'
    }, {
        ref: 'selectedNodesDialog',
        selector: '#selectedNodesDialog'
    }, {
        ref: 'nodesTree',
        selector: '#selectedNodesDialog treepanel'
    }, {
        ref: 'selectedEmpDialog',
        selector: '#selectedEmpDialog'
    }, {
        ref: 'employeesTree',
        selector: '#selectedEmpDialog treepanel'
    }, {
        ref: 'configSidebar',
        selector: "#configSidebar"
    }, {
        ref: 'dateRangeFrom',
        selector: '#dateRangeFrom'
    }, {
        ref: 'dateRangeUntil',
        selector: "#dateRangeUntil"
    }, {
        ref: 'confButtonGroup',
        selector: 'buttonGroup'
    }, {
        ref: 'configNodes',
        selector: '#configNodes'
    }, {
        ref: 'removeCCButton',
        selector: '#removeCC'
    }, {
        ref: 'removeCCsButton',
        selector: '#removeCCs'
    }],

    selectedEmps: null,
    selectedNodes: null,

    onLaunch: function() {

        Ext.getStore('Nodes').on({
            'beforeexpand': function(node) {
                // set listener to restore original icon
                node.on('expand', function(n) {
                    n.set('iconCls', this.iconCls);
                }, {iconCls: node.get('iconCls')}, {single: true});
                // remove iconCls while loading in order to show loading indicator
                node.set('iconCls', null);
            }
        });

        this.getSelectedNodesStore().on({
            'add': function() {
                this.enable();
            },
            'bulkremove': function(store) {
                this.setDisabled(store.getCount() === 0);
            },
            scope: this.getRemoveCCsButton()
        });
    },

    init: function() {
        this.control({
            'configSidebar': {
                selectionchange: function(view, selection) {
                    this.activateReportCfg(selection[0].get('abbr'));
                }
            },
            'configPanel': {
                activate: function() {
                    if (this.getConfigPanel().isOpenedFromProfileList) {
                        this.selectedEmps = null;
                    }

                    let repConfig = this.getRepConf(),
                        profile = repConfig.profile,
                        profileData = this.getConfigPanel().profileData;

                    if (profile == null) {
                        profile = Ext.getStore('Profiles').defaultProfile;
                        if (repConfig.newProfileButton) {
                            repConfig.setProfileValues(profile, true);
                        } else {
                            repConfig.setProfileValues(profile, false);
                        }
                    }

                    this.getConfigSidebar().select(this.getConfigSidebar().store.findExact('abbr', profile.get('type')));
                    repConfig.disableProfileData(profile, profileData);

                    // Disable the save button if there is no value in 'profile name'
                    if (this.getConfigPanel().profileData.getComponent('profileName').getValue()) {
                        this.getConfButtonGroup().getComponent('saveProfileButton').setDisabled(false);
                    } else {
                        this.getConfButtonGroup().getComponent('saveProfileButton').setDisabled(true);
                    }

                    this.getConfigPanel().isOpenedFromEditIcon = false;
                }
            },
            '#configNodes': {
                selectionchange: function(view, selection) {
                    this.getRemoveCCButton().setDisabled(selection.length === 0);
                }
            },
            '#configNodes gridview': {
                showPathToRoot: function(view, rowIndex, pos) {
                    var record = view.getRecord(view.getNode(rowIndex));
                    this.showPathToRootTooltip(record.get('id'), pos);
                }
            },
            '#configNodes button[action=addNodes]': {
                click: function() {
                    this.getSelectedNodesDialog().show();
                }
            },
            '#configNodes button[action=clearNodes]': {
                click: function() {
                    this.getSelectedNodesStore().removeAll();
                }
            },
            '#configNodes button[action=clearNode]': {
                click: function() {
                    var selectedNodesStore = this.getSelectedNodesStore(),
                        selection = this.getConfigNodes().getSelectionModel().getSelection();

                    if (selection) {
                        selectedNodesStore.remove(selection[0]);
                    }
                }
            },
            '#selectedNodesDialog': {
                beforeshow: function() {
                    this.applyProfileNodesToTree();

                    this.getNodesTree().liveSearch.clear();
                },
                activate: function() {
                    this.getSelectedNodesDialog().selectedNodes.searchField.focus(false, 500);
                }
            },
            '#selectedNodesDialog button[action=updateNodeList]': {
                click: function() {
                    // close BEFORE doing anything else to prevent layouting
                    this.getSelectedNodesDialog().close();

                    this.applyTreeNodesToProfile();
                }
            },
            '#selectedNodesDialog button[action=revertNodeList]': {
                click: function() {
                    // close BEFORE doing anything else to prevent layouting
                    this.getSelectedNodesDialog().close();
                }
            },
            '#selectedNodesDialog treepanel': {
                itemexpand: function(node) {
                    var treePanel = this.getNodesTree(),
                        selectedNodesStore = this.getSelectedNodesStore(),
                        iteration = 0;

                    if (node.get('uid').substr(0, 1) !== 'n') {
                        return;
                    }

                    // set check state of newly loaded children
                    node.cascadeBy(function(childNode) {
                        if (iteration++ === 0) {
                            return;
                        }

                        var record = selectedNodesStore.getById(childNode.get('uid'));
                        if (record) {
                            this.setTreeNodeCheckState(childNode, record);
                        }
                    }, this);

                    if (node.get('checked')) {
                        treePanel.fireEvent('checkchange', node, true);
                    }
                    this.getSingleNodeTypesStore().each(function(record) {
                        if (record.get('forceChildSelection')
                            && (
                                (record.get('value') & node.get('singleNodeTypes')) > 0
                                || node.get('singleNode' + record.get('value') + 'Disabled')
                            )
                        ) {
                            node.bubble(function() {
                                if ((this.get('singleNodeTypes') & record.get('value')) > 0) {
                                    treePanel.fireEvent('singlenodecheckchange', this, record.get('value'), true);
                                }
                            });
                        }
                    });
                },
                checkchange: function(node, checked) {
                    // Check/uncheck and disable/enable all child nodes
                    var treeView = this.getNodesTree().getView(),
                        reportType = this.getRepConf().profile.get('type'),
                        iteration = 0,
                        preventRefreshFn = function () {
                            return false;
                        };

                    treeView.on('beforerefresh', preventRefreshFn);
                    node.cascadeBy(function(isChecked) {
                        if (this.get('checked') !== null) {
                            this.set('checked', isChecked);

                            if (iteration > 0) {
                                this.set('disabled', isChecked);
                            }

                            // for report 'Diagram': change state of single node
                            // selection if the parent node was unchecked
                            if (reportType === 'D'
                                && isChecked === false
                                && this.get('leaf')
                                && (this.get('singleNodeTypes') & BC.store.SingleNodeTypes.DIAGRAM_SINGLE_CC) > 0
                            ) {
                                this.set('singleNodeTypes', this.get('singleNodeTypes') ^ BC.store.SingleNodeTypes.DIAGRAM_SINGLE_CC);
                                this.commit();
                            }
                        }
                        iteration++;
                    }, null, [checked]);

                    node.commit();

                    treeView.un('beforerefresh', preventRefreshFn);
                    treeView.refresh();
                },
                singlenodecheckchange: function(node, singleNodeType, checkChildren) {
                    var treeView = this.getNodesTree().getView(),
                        iteration = 0,
                        checked = (node.get('singleNodeTypes') & singleNodeType) > 0;

                    if (checkChildren) {
                        // workaround as suspendEvents on treeStore does not work
                        // TODO: revalidate with next ExtJS release
                        treeView.viewReady = false;

                        node.cascadeBy(function() {
                            if (iteration++ > 0) {
                                this.set('singleNode' + singleNodeType + 'Disabled', checked);
                                if (checked) {
                                    this.set('singleNodeTypes', this.get('singleNodeTypes') & ~singleNodeType);
                                }
                            }
                            this.commit();
                        });

                        // see above workaround description
                        treeView.viewReady = true;
                        treeView.refresh();
                    }

                    node.commit();
                }
            },
            '#selectedNodesDialog treeview': {
                itemclick: function(view, record, itemEl, i, e) {
                    var t = Ext.get(e.getTarget()),
                        matchSingleNode = /x-grid-singlenode-col/.exec(t.dom.className),
                        matchSingleNodeDisabled = /x-grid-singlenode-col-disabled/.exec(t.dom.className),
                        singleNodeType;

                    if (matchSingleNode && !matchSingleNodeDisabled) {
                        singleNodeType = this.getSingleNodeTypesStore().findRecord('value', t.getAttribute('data-singlenodetype'), 0, false, false, true);

                        record.set('singleNodeTypes', record.get('singleNodeTypes') ^ singleNodeType.get('value'));

                        this.getSelectedNodesDialog().selectedNodes.fireEvent('singlenodecheckchange', record, singleNodeType.get('value'), singleNodeType.get('forceChildSelection'));

                        if (singleNodeType.get('value') === BC.store.SingleNodeTypes.STATUS_WITHNEXTLEVEL
                            && (record.get('singleNodeTypes') & BC.store.SingleNodeTypes.STATUS_WITHNEXTLEVEL) > 0
                            && (record.get('singleNodeTypes') & BC.store.SingleNodeTypes.STATUS_DISTINCTROW) === 0
                        ) {
                            record.set('singleNodeTypes', record.get('singleNodeTypes') ^ BC.store.SingleNodeTypes.STATUS_DISTINCTROW);
                            record.commit();
                        }

                        if (singleNodeType.get('value') === BC.store.SingleNodeTypes.STATUS_DISTINCTROW
                            && (record.get('singleNodeTypes') & BC.store.SingleNodeTypes.STATUS_DISTINCTROW) === 0
                            && (record.get('singleNodeTypes') & BC.store.SingleNodeTypes.STATUS_WITHNEXTLEVEL) > 0
                        ) {
                            record.set('singleNodeTypes', record.get('singleNodeTypes') ^ BC.store.SingleNodeTypes.STATUS_WITHNEXTLEVEL);
                            record.commit();
                        }
                    }
                }
            },
            '#configEmployees button[action=addEmployees]': {
                click: function() {
                    this.getSelectedEmpDialog().show();
                }
            },
            '#configEmployees button[action=clearEmployees]': {
                click: function() {
                    this.getSelectedEmployeesStore().removeAll();
                }
            },
            '#selectedEmpDialog': {
                beforeshow: function() {
                    this.getEmployeesTree().liveSearch.clear();

                    this.applyProfileEmployeesToTree();
                },
                close: function() {
                    this.getEmployeesStore().getRootNode().childNodes[0].childNodes[0].collapse();
                    this.getEmployeesStore().getRootNode().childNodes[0].childNodes[1].collapse();
                    //this.getEmployeesStore().getRootNode().childNodes[1].collapse();
                },
                activate: function() {
                    this.getSelectedEmpDialog().selectedEmployees.searchField.focus(false, 500);
                }
            },
            '#selectedEmpDialog button[action=updateEmpList]': {
                click: function() {
                    this.getSelectedEmpDialog().close();

                    this.applyTreeEmployeesToProfile();
                }
            },
            '#selectedEmpDialog button[action=revertEmpList]': {
                click: function() {
                    this.getSelectedEmpDialog().close();
                }
            },
            '#employeesTreeSearchField': {
                render: function (field) {
                    // hide useless trigger in search field
                    field.triggerEl.item(1).up('td').setDisplayed(false);
                }
            },
            'buttonGroup button[action=saveProfile]': {
                click: function() {
                    var reportType = this.getRepConf().profile.get('type');
                    if (/^[PFQ]$/.test(reportType) === true) {
                        this.saveProfile(false);
                    } else {
                        // Check if the report's from date is before 01.01.2012
                        Ext.Ajax.request({
                            url: "index/determineperiodfromvalues",
                            params: {
                                from: this.getRepConf().getComponent('configDateRange').getComponent('dateRangeFrom').getValue(),
                                until: this.getRepConf().getComponent('configDateRange').getComponent('dateRangeUntil').getValue(),
                                dynamicPeriod: this.getRepConf().getComponent('configDateRange').getComponent('dynamicPeriod').getValue(),
                                profileType: this.getRepConf().profile.get('type')// if profile is valid from 2012 for 'all' period then 0, if from 2013 then 1
                            },
                            scope: this,
                            success: function(response) {
                                var parsed = response.responseText.split('-'),
                                    from = Number(parsed[0]),
                                    till = Number(parsed[1]),
                                    fromDate = new Date(from * 1000),
                                    tillDate = new Date(till * 1000),
                                    year2013 = new Date(2013, 0, 1),
                                    year2012 = new Date(2012, 0, 1),
                                    year2016 = new Date(2016, 0, 1);

                                // Show warning
                                if (/^[IJK]$/.test(reportType) === true && from * 1000 < year2013.getTime()) {
                                    Ext.MessageBox.alert(BC.translate('undefinederror_subject'), Ext.String.format(BC.translate('frombefore'), Ext.Date.format(year2013, Ext.Date.defaultFormat)));
                                } else if (from * 1000 < year2012.getTime()) {
                                    Ext.MessageBox.alert(BC.translate('undefinederror_subject'), Ext.String.format(BC.translate('frombefore'), Ext.Date.format(year2012, Ext.Date.defaultFormat)));
                                } else if (/^[K]$/.test(this.getRepConf().profile.get('type')) === true && Ext.getCmp('perPeriod').getValue() && fromDate.getYear() !== tillDate.getYear()) {
                                    Ext.MessageBox.show({
                                        buttons: Ext.Msg.OK,
                                        title: ' ',
                                        width: 500,
                                        msg: BC.translate('oneyear'),
                                        animateTarget: 'reportConfiguration',
                                        icon: Ext.MessageBox.WARNING
                                    });
                                } else if (/^[V]$/.test(reportType) === true && from * 1000 < year2016.getTime()) {
                                    Ext.MessageBox.alert(BC.translate('undefinederror_subject'), Ext.String.format(BC.translate('frombefore'), Ext.Date.format(year2016, Ext.Date.defaultFormat)));
                                } else {
                                    this.saveProfile(false);
                                }
                            }
                        });
                    }
                }
            },
            'buttonGroup button[action=clearFields]': {
                click: function() {
                    Ext.MessageBox.show({
                        buttons: Ext.Msg.OKCANCEL,
                        title: ' ',
                        msg: BC.translate('clearConfigConfirm'),
                        scope: this,
                        fn: function(btn) {
                            if (btn === 'ok') {
                                this.getRepConf().setProfileValues(this.getRepConf().profile, true);
                            }
                        },
                        animateTarget: 'reportConfiguration',
                        icon: Ext.MessageBox.WARNING
                    });
                }
            },
            'buttonGroup button[action=showProfile]': {
                click: function() {
                    var reportType = this.getRepConf().profile.get('type'),
                        fieldFrom = this.getRepConf().getComponent('configDateRange').getComponent('dateRangeFrom'),
                        fieldTill = this.getRepConf().getComponent('configDateRange').getComponent('dateRangeUntil'),
                        year2013 = new Date(2013, 0, 1),
                        year2012 = new Date(2012, 0, 1),
                        year2016 = new Date(2016, 0, 1);

                    if (fieldFrom.isValid() === false) {
                        fieldFrom.focus();
                        return;
                    }
                    if (fieldTill.isValid() === false) {
                        fieldTill.focus();
                        return;
                    }

                    if (reportType === 'F') {
                        this.getRepConf().profile.set('hint', null);
                        this.getRepConf().getMaskTarget().mask('...');
                        this.saveProfile(true, function(profile) {
                            if (!(profile instanceof BC.model.Profile)) {
                                profile = Ext.getStore('Profiles').defaultProfile;
                            }
                            var callbackFn = function(record, scope, result, btn) {
                                if (btn === 'yes') {
                                    window.location = 'index/showdirect/type/F/timestamp/' + result.timestamp;
                                }
                            };

                            Ext.Ajax.request({
                                url: "index/validate",
                                params: {
                                    chartprofil: profile.get('id')
                                },
                                scope: this,
                                success: function (response) {
                                    var result = Ext.JSON.decode(response.responseText),
                                        btn;
                                    if (result.hint) {
                                        Ext.MessageBox.show({
                                            buttons: Ext.Msg.YESNO,
                                            title: ' ',
                                            msg: BC.translate('btOrderDisplayMode'),
                                            scope: this,
                                            fn: function(btnId) {
                                                if (btnId === 'yes') {
                                                    this.getController('BC.controller.ProfileList').directReport(profile, callbackFn, result, btnId, 'reportConfiguration');
                                                }
                                            },
                                            animateTarget: 'reportConfiguration',
                                            icon: Ext.MessageBox.QUESTION
                                        });
                                    } else {
                                        btn = 'yes';
                                        this.getController('BC.controller.ProfileList').directReport(profile, callbackFn, result, btn, 'reportConfiguration');
                                    }
                                }
                            });
                        });
                    } else {
                        // Check if the report's from date is before 01.01.2012
                        Ext.Ajax.request({
                            url: "index/determineperiodfromvalues",
                            params: {
                                from: fieldFrom.getValue(),
                                until: fieldTill.getValue(),
                                dynamicPeriod: this.getRepConf().getComponent('configDateRange').getComponent('dynamicPeriod').getValue(),
                                profileType: this.getRepConf().profile.get('type')
                            },
                            scope: this,
                            success: function(response) {
                                var parsed = response.responseText.split('-'),
                                    from = Number(parsed[0]),
                                    till = Number(parsed[1]),
                                    fromDate = new Date(from * 1000),
                                    tillDate = new Date(till * 1000);
                                // Show warning (makes no sense for reports without period configuration)
                                if (/^[PFQ]$/.test(this.getRepConf().profile.get('type')) === false && from * 1000 < year2012.getTime()) {
                                    Ext.MessageBox.alert(BC.translate('undefinederror_subject'), Ext.String.format(BC.translate('frombefore'), Ext.Date.format(year2012, Ext.Date.defaultFormat)));
                                } else if (/^[IJK]$/.test(this.getRepConf().profile.get('type')) === true && from * 1000 < year2013.getTime()) {
                                    Ext.MessageBox.alert(BC.translate('undefinederror_subject'), Ext.String.format(BC.translate('frombefore'), Ext.Date.format(year2013, Ext.Date.defaultFormat)));
                                } else if (/^[K]$/.test(this.getRepConf().profile.get('type')) === true && Ext.getCmp('perPeriod').getValue() && fromDate.getYear() !== tillDate.getYear()) {
                                    Ext.MessageBox.show({
                                        buttons: Ext.Msg.OK,
                                        title: ' ',
                                        width: 500,
                                        msg: BC.translate('oneyear'),
                                        animateTarget: 'reportConfiguration',
                                        icon: Ext.MessageBox.WARNING
                                    });
                                } else if (/^[V]$/.test(this.getRepConf().profile.get('type')) === true && from * 1000 < year2016.getTime()) {
                                    Ext.MessageBox.alert(BC.translate('undefinederror_subject'), Ext.String.format(BC.translate('frombefore'), Ext.Date.format(year2016, Ext.Date.defaultFormat)));
                                } else {
                                    this.getRepConf().profile.set('hint', null);
                                    this.getRepConf().getMaskTarget().mask('...');
                                    this.saveProfile(true, function(profile) {
                                        if (!(profile instanceof BC.model.Profile)) {
                                            profile = Ext.getStore('Profiles').defaultProfile;
                                        }
                                        var date = new Date();
                                        var callbackFn;

                                        switch (reportType) {
                                            // accounting report
                                            case 'A':
                                                callbackFn = function (record, scope, result) {
                                                    Ext.MessageBox.show({
                                                        buttons: Ext.Msg.OK,
                                                        title: ' ',
                                                        width: 500,
                                                        msg: result.success === true ? Ext.String.format(BC.translate('accountingPopupMessage'), global.accountingPath) : BC.translate('accountingPopupMessageEmptyReport'),
                                                        animateTarget: 'reportConfiguration',
                                                        icon: Ext.MessageBox.INFO
                                                    });
                                                };
                                                this.getController('BC.controller.ProfileList').directReport(profile, callbackFn, {}, {}, 'reportConfiguration');
                                                break;
                                            // direct download reports
                                            case 'I':
                                            case 'J':
                                            case 'G':
                                            case 'Q':
                                            case 'Z':
                                            case 'Y':
                                                callbackFn = function(record, scope, result) {
                                                    window.location = 'index/showdirect/type/' + reportType + '/timestamp/' + result.timestamp;
                                                };
                                                this.getController('BC.controller.ProfileList').directReport(profile, callbackFn, {}, {}, 'reportConfiguration');
                                                break;
                                            // default tab behavior
                                            default:
                                                var titlestring = " (" + Ext.Date.format(date, 'H:i') + ")",
                                                    mainPanel = this.getMainPanel(),
                                                    report = new BC.view.report.ReportPanel(titlestring, reportType + "Icon", profile);

                                                mainPanel.add(report);
                                                report.tab.addCls('report-tab');

                                                //activate the appropriate report-tab
                                                mainPanel.setActiveTab(report);
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            },
            'configTreePanel triggerfield': {
                trigger1click: function(field) {
                    field.up('configTreePanel').liveSearch.clear();
                },
                trigger2click: function(field) {
                    var panel = field.up('configTreePanel'),
                        liveSearch = panel.liveSearch,
                        value = Ext.String.trim(field.getValue());

                    if (value.length === 0) {
                        liveSearch.clear();
                        return;
                    }
                    if (this.validateTreeSearchField(field) === false) {
                        return;
                    }

                    if (liveSearch.isModified()) {
                        liveSearch.search(value);
                    } else {
                        liveSearch.next();
                    }
                },
                // the code is a lie! ->
                trigger3click: function(field) {
                    var panel = field.up('configTreePanel'),
                        liveSearch = panel.liveSearch,
                        value = Ext.String.trim(field.getValue());

                    if (value.length === 0) {
                        return;
                    }
                    if (this.validateTreeSearchField(field) === false) {
                        return;
                    }

                    if (liveSearch.isModified()) {
                        panel.on('searchcomplete', function (t, p, results) {
                            if (results.length > 0) {
                                this.prepareSearchResultNodeSelection(panel);
                            }
                        }, this, {single: true});

                        liveSearch.search(value);
                    } else if (liveSearch.indexes.length > 0) {
                        this.prepareSearchResultNodeSelection(panel);
                        liveSearch.currentIndex = -1;
                        liveSearch.next();
                    }
                },
                // <-
                specialkey: function(field, e) {
                    switch (e.getKey()) {
                        case e.ENTER:
                            field.fireEvent('trigger2click', field);
                            break;
                        case e.ESC:
                            field.fireEvent('trigger1click', field);
                            break;
                    }
                }
            },
            'configTreePanel combobox': {
                change: function(field, newValue) {
                    var panel = field.up('configTreePanel');

                    panel.liveSearch.clear(false);
                    switch (newValue) {
                        case 0:
                            panel.liveSearch.nodeMatchField = 'text';
                            panel.liveSearch.nodeMatchFn = Ext.emptyFn;
                            break;
                        case 1:
                            panel.liveSearch.nodeMatchField = 'debtorNumber';
                            panel.liveSearch.nodeMatchFn = function(record, searchField, searchValue) {
                                if (record.get('checked') === null) {
                                    return false;
                                }

                                return searchValue === record.get(searchField);
                            };
                            break;
                        case 2:
                            panel.liveSearch.nodeMatchField = ['sapKeyCostCenter', 'sapKeyCostUnit'];
                            panel.liveSearch.nodeMatchFn = function(record, searchField, searchValue) {
                                if (record.get('checked') === null) {
                                    return false;
                                }

                                if (!Ext.isArray(searchField)) {
                                    searchField = [searchField];
                                }
                                for (var i = 0; i < searchField.length; ++i) {
                                    if (searchValue === record.get(searchField[i])) {
                                        return true;
                                    }
                                }
                                return false;
                            };
                            break;
                    }
                }
            },
            'configTreePanel button[action=previous]': {
                click: function(btn) {
                    btn.up('configTreePanel').liveSearch.previous();
                }
            },
            'configTreePanel button[action=next]': {
                click: function(btn) {
                    btn.up('configTreePanel').liveSearch.next();
                }
            },
            '#profileName': {
                change: function(field, newValue) {
                    // Disable the save button if there is no value in 'profile name'
                    if (newValue) {
                        this.getConfButtonGroup().getComponent('saveProfileButton').setDisabled(false);
                    } else {
                        this.getConfButtonGroup().getComponent('saveProfileButton').setDisabled(true);
                    }
                }
            },
            '#configDateRange #dynamicPeriod': {
                change: function(field, newVal) {
                    if (newVal !== 0) {
                        this.dateFrom = this.getDateRangeFrom().getValue();
                        this.dateUntil = this.getDateRangeUntil().getValue();

                        this.getDateRangeFrom().setValue('');
                        this.getDateRangeFrom().setDisabled(true);
                        this.getDateRangeUntil().setValue('');
                        this.getDateRangeUntil().setDisabled(true);
                    } else {
                        this.getDateRangeFrom().setDisabled(false);
                        this.getDateRangeUntil().setDisabled(false);

                        this.getDateRangeFrom().setValue(this.dateFrom);
                        this.getDateRangeUntil().setValue(this.dateUntil);
                    }
                }
            },
            '#displayOnlySubtrees': {
                hide: this.setBookingTypeCheckboxState,
                show: this.setBookingTypeCheckboxState,
                change: this.setBookingTypeCheckboxState
            }
        });
    },

    // Show the apprpriate fielsets for the specified reportType
    activateReportCfg: function(type) {
        Ext.suspendLayouts();

        var repConf = this.getRepConf(),
            configPanel = this.getConfigPanel(),
            configBtnGroup = this.getConfButtonGroup(),
            selectedNodesDlg = this.getSelectedNodesDialog(),
            selectedNodesStore = this.getSelectedNodesStore(),
            i;

        repConf.profile.set('type', type);
        repConf.resetDisabledConfigFields(type);

        // Set url for report type help link
        configPanel.down('#reportHelp').getActionEl().dom.href = "index/help/report/" + type;

        // Hide everything
        repConf.items.each(function(item) {
            item.hide();
        });

        repConf.getComponent('configNodes').columns[0].show();

        // Clear and hide single node columns
        repConf.getComponent('configNodes').columns[4].hide();
        repConf.getComponent('configNodes').columns[5].hide();
        repConf.getComponent('configNodes').columns[5].setText(BC.translate('displayCC'));
        repConf.getComponent('configNodes').columns[6].hide();
        repConf.getComponent('configNodes').columns[7].hide();
        repConf.getComponent('configNodes').columns[8].hide();
        repConf.getComponent('configNodes').columns[9].hide();
        repConf.getComponent('configNodes').columns[10].hide();
        repConf.getComponent('configNodes').columns[11].hide();

        selectedNodesDlg.selectedNodes.showSets();
        selectedNodesDlg.selectedNodes.findPlugin("nodedisabled").disabledFn = null;
        selectedNodesDlg.selectedNodes.findPlugin("nodedisabled").disabledCls = 'tree-node-disabled';

        if (selectedNodesDlg.rendered) {
            selectedNodesDlg.selectedNodes.columns[0].setText(BC.translate('abkbkz'));
            selectedNodesDlg.selectedNodes.columns[1].show(); //show description
            for (i = 2; i < selectedNodesDlg.selectedNodes.columns.length; i++) {
                selectedNodesDlg.selectedNodes.columns[i].hide();
            }
        } else {
            selectedNodesDlg.selectedNodes.columns[0].text = BC.translate('abkbkz');
            selectedNodesDlg.selectedNodes.columns[1].hidden = false; //show description
            for (i = 2; i < selectedNodesDlg.selectedNodes.columns.length; i++) {
                selectedNodesDlg.selectedNodes.columns[i].hidden = true;
            }
        }

        // Hide auto mail field
        configPanel.profileData.getComponent('autoMail').hide();
        configPanel.profileData.getComponent('autoMailIcon').hide();

        // Hide report mail combo
        configPanel.profileData.getComponent('reportMail').hide();
        configPanel.profileData.getComponent('reportMailIcon').hide();

        // Hide able to share employees field
        configPanel.profileData.getComponent('shareProfile').hide();

        //Show limit field
        repConf.getComponent('groupHrsNoteLimit').getComponent('configLimit').show();

        // Reset booking type config
        repConf.getComponent('configBookingTypes').setTitle(BC.translate('hrsbookingwithbookingtype'));
        repConf.getComponent('configBookingTypes').getComponent('displayOnlySubtrees').hide();
        this.updateBookingTypeCheckboxLabels('displayName');

        //Show profile data fieldset
        configPanel.profileData.show();
        //Show config buttons
        configBtnGroup.getComponent('saveProfileButton').show();
        configBtnGroup.getComponent('clearProfileButton').show();
        //Set default show button style
        configBtnGroup.getComponent('showProfileButton').setText(BC.translate('anzeigen'));
        configBtnGroup.getComponent('showProfileButton').setIcon('files/images/show.png');

        // clear filter for single node types in store
        selectedNodesStore.clearFilter(true);

        switch (type) {
            case 'D':
                this.activateDiagramCfg();
                break;
            case 'L':
                this.activateListCfg();
                break;
            case 'T':
                this.activateTableCfg();
                break;
            case 'E':
                this.activateEmployeeBookingCfg();
                break;
            case 'B':
                this.activateBookingCfg();
                break;
            case 'A':
                this.activateAccountingCfg();
                break;
            case 'R':
                this.activateRebookCfg();
                break;
            case 'P':
                this.activatePermissionCfg();
                break;
            case 'M':
                this.activateMonthEndCfg();
                break;
            case 'N':
                this.activateBtModificationsCfg();
                break;
            case 'S':
                this.activateSAPKeyHoursCfg();
                break;
            case 'O':
                this.activateRebookLogCfg();
                break;
            case 'F':
                this.activateBtOrderCfg();
                break;
            case 'H':
                this.activateStandbyCfg();
                break;
            case 'C':
                this.activateAbsenceCalendarCfg();
                break;
            case 'I':
                this.activateMisDivisionCfg();
                break;
            case 'J':
                this.activateMisOverallCfg();
                break;
            case 'K':
                this.activateStatusCfg();
                break;
            case 'G':
                this.activateAccountableHoursCfg();
                break;
            case 'V':
                this.activateOrderOverviewCfg();
                break;
            case 'Q':
                this.activateContingentRangeCfg();
                break;
            case 'Z':
                this.activateContingentDataCfg();
                break;
            case 'X':
                this.activateContingentSapInvoiceComparison();
                break;
            case 'Y':
                this.activatePrismaValues();
                break;
        }

        if (!selectedNodesStore.isFiltered()) {
            selectedNodesStore.filter('checked', true);
        }

        Ext.resumeLayouts(true);
    },

    activateDiagramCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configPanel = me.getConfigPanel();

        repConfig.clearProfileData();
        repConfig.getComponent('configDateRange').show();
        repConfig.getComponent('configNodes').show();
        repConfig.getComponent('configNodes').columns[4].show();
        if (this.getSelectedNodesDialog().rendered) {
            me.getSelectedNodesDialog().selectedNodes.columns[3].show();
        } else {
            me.getSelectedNodesDialog().selectedNodes.columns[3].hidden = false;
        }
        repConfig.getComponent('groupAccType').show();
        repConfig.getComponent('groupHrsNoteLimit').show();
        repConfig.getComponent('configEmployees').show();
        configPanel.profileData.getComponent('autoMail').show();
        configPanel.profileData.getComponent('autoMailIcon').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();

        this.getSelectedNodesStore().filter({
            filterFn: function(record) {
                return (
                    record.get('checked')
                    || (record.get('singleNodeTypes') & BC.store.SingleNodeTypes.DIAGRAM_SINGLE_CC) > 0
                );
            }
        });
    },
    activateListCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configPanel = me.getConfigPanel();

        repConfig.clearProfileData();
        repConfig.getComponent('configDateRange').show();
        repConfig.getComponent('configNodes').show();
        repConfig.getComponent('groupHrsNoteLimit').show();
        repConfig.getComponent('configEmployees').show();
        repConfig.getComponent('configListExtraCols').show();
        repConfig.getComponent('configListOptions').show();
        configPanel.profileData.getComponent('autoMail').show();
        configPanel.profileData.getComponent('autoMailIcon').show();
        configPanel.profileData.getComponent('reportMail').show();
        configPanel.profileData.getComponent('reportMailIcon').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },
    activateTableCfg: function() {
        var me = this,
            configPanel = me.getConfigPanel(),
            repConfig = me.getRepConf();

        repConfig.clearProfileData();
        repConfig.getComponent('configDateRange').show();
        repConfig.getComponent('configNodes').show();
        repConfig.getComponent('configNodes').columns[5].show();
        repConfig.getComponent('configNodes').columns[7].show();
        if (this.getSelectedNodesDialog().rendered) {
            me.getSelectedNodesDialog().selectedNodes.columns[0].setText(BC.translate('nodeSelectionToDisplay'));
            me.getSelectedNodesDialog().selectedNodes.columns[5].show();
        } else {
            me.getSelectedNodesDialog().selectedNodes.columns[0].text = BC.translate('nodeSelectionToDisplay');
            me.getSelectedNodesDialog().selectedNodes.columns[5].hidden = false;
        }
        repConfig.getComponent('groupHourBookings').show();
        repConfig.getComponent('groupHrsNoteLimit').show();
        repConfig.getComponent('configEmployees').show();
        configPanel.profileData.getComponent('autoMail').show();
        configPanel.profileData.getComponent('autoMailIcon').show();
        configPanel.profileData.getComponent('reportMail').show();
        configPanel.profileData.getComponent('reportMailIcon').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();

        this.getSelectedNodesStore().filter({
            filterFn: function(record) {
                return (
                    record.get('checked')
                    || (record.get('singleNodeTypes') & BC.store.SingleNodeTypes.TABLE_DISPLAY_NODE) > 0
                );
            }
        });
    },
    activateEmployeeBookingCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configPanel = me.getConfigPanel();

        repConfig.clearProfileData();
        repConfig.getComponent('configDateRange').show();
        repConfig.getComponent('configEmployees').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },
    activateBookingCfg: function() {
        var me = this,
            repConf = me.getRepConf(),
            configPanel = me.getConfigPanel();

        repConf.clearProfileData();
        repConf.getComponent('configDateRange').show();
        repConf.getComponent('configNodes').show();
        repConf.getComponent('configEmployees').show();
        repConf.getComponent('groupHrsNoteLimit').show();
        repConf.getComponent('configTreeParams').show();

        repConf.getComponent('configBookingTypes').show();
        repConf.getComponent('configBookingTypes').setTitle(BC.translate('restrictBookingTree'));
        repConf.getComponent('configBookingTypes').getComponent('displayOnlySubtrees').show();

        configPanel.profileData.getComponent('autoMail').show();
        configPanel.profileData.getComponent('autoMailIcon').show();
        configPanel.profileData.getComponent('reportMail').show();
        configPanel.profileData.getComponent('reportMailIcon').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },
    activateAccountingCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configPanel = me.getConfigPanel();

        repConfig.clearProfileData();
        repConfig.getComponent('configDateRange').show();
        repConfig.getComponent('configNodes').show();
        repConfig.getComponent('configEmployees').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();

        this.getConfButtonGroup().getComponent('showProfileButton').setText(BC.translate('generate'));
        this.getConfButtonGroup().getComponent('showProfileButton').setIcon('files/images/disk.png');
    },
    activateRebookCfg: function() {
        var me = this,
            repConf = me.getRepConf();

        repConf.getComponent('configDateRange').show();
        repConf.getComponent('configNodes').show();
        repConf.getComponent('configNodes').columns[6].show();
        repConf.getComponent('configNodes').columns[5].show();
        repConf.getComponent('configNodes').columns[5].setText(BC.translate('rebookFromCC'));
        if (me.getSelectedNodesDialog().rendered) {
            me.getSelectedNodesDialog().selectedNodes.columns[0].setText(BC.translate('rebookFromCC'));
            me.getSelectedNodesDialog().selectedNodes.columns[4].show();
        } else {
            me.getSelectedNodesDialog().selectedNodes.columns[0].text = BC.translate('rebookFromCC');
            me.getSelectedNodesDialog().selectedNodes.columns[4].hidden = false;
        }
        repConf.getComponent('configBookingTypes').show();
        repConf.getComponent('groupHrsNoteLimit').show();
        repConf.getComponent('groupHrsNoteLimit').getComponent('configLimit').hide();
        repConf.getComponent('configEmployees').show();

        me.getSelectedNodesStore().filter({
            filterFn: function(record) {
                return (
                    record.get('checked')
                    || (record.get('singleNodeTypes') & BC.store.SingleNodeTypes.REBOOK_TO_CC) > 0
                );
            }
        });
    },
    activatePermissionCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configPanel = me.getConfigPanel();

        repConfig.clearProfileData();
        repConfig.getComponent('configEmployees').show();
        repConfig.getComponent('configNoBCPermission').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },
    activateMonthEndCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configPanel = me.getConfigPanel();

        repConfig.clearProfileData();
        repConfig.getComponent('configDateRange').show();
        repConfig.getComponent('configEmployees').show();
        configPanel.profileData.getComponent('reportMail').show();
        configPanel.profileData.getComponent('reportMailIcon').show();
        repConfig.getComponent('configMonthEndExtraCols').show();
        repConfig.getComponent('configMonthEndInformationToBeDisplayed').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },
    activateBtModificationsCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configPanel = me.getConfigPanel();

        repConfig.clearProfileData();
        repConfig.getComponent('configDateRange').show();
        repConfig.getComponent('configNodes').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },
    activateSAPKeyHoursCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configPanel = me.getConfigPanel();

        repConfig.clearProfileData();
        repConfig.getComponent('configDateRange').show();
        repConfig.getComponent('configNodes').show();
        repConfig.getComponent('configEmployees').show();
        configPanel.profileData.getComponent('reportMail').show();
        configPanel.profileData.getComponent('reportMailIcon').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },
    activateRebookLogCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configPanel = me.getConfigPanel();

        repConfig.clearProfileData();
        repConfig.getComponent('configDateRange').show();
        repConfig.getComponent('configNodes').show();
        repConfig.getComponent('configPerformedOrBookingDate').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },
    activateBtOrderCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configPanel = me.getConfigPanel(),
            configButtonGroup = me.getConfButtonGroup();

        repConfig.clearProfileData();
        repConfig.getComponent('configNodes').show();
        configButtonGroup.getComponent('showProfileButton').setText(BC.translate('generate'));
        configButtonGroup.getComponent('showProfileButton').setIcon('files/images/disk.png');
        repConfig.getComponent('configDisplayLockedNodes').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },
    activateStandbyCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configPanel = me.getConfigPanel();

        repConfig.clearProfileData();
        repConfig.getComponent('configDateRange').show();
        repConfig.getComponent('configNodes').show();
        repConfig.getComponent('configEmployees').show();
        configPanel.profileData.getComponent('reportMail').show();
        configPanel.profileData.getComponent('reportMailIcon').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },
    activateAbsenceCalendarCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configPanel = me.getConfigPanel();

        repConfig.clearProfileData();
        repConfig.getComponent('configDateRange').show();
        repConfig.getComponent('configEmployees').show();
        configPanel.profileData.getComponent('reportMail').show();
        configPanel.profileData.getComponent('reportMailIcon').show();
        repConfig.getComponent('configAbsenceConfig').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },
    activateMisDivisionCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configPanel = me.getConfigPanel(),
            selectedNodesDialog = me.getSelectedNodesDialog(),
            configButtonGroup = me.getConfButtonGroup();

        repConfig.getComponent('configDateRange').show();
        repConfig.getComponent('configNodes').show();
        repConfig.clearProfileData();
        repConfig.disableProfileData(repConfig.profile, configPanel.profileData);

        repConfig.getComponent('configNodes').columns[0].hide();
        repConfig.getComponent('configNodes').columns[5].show();
        repConfig.getComponent('configNodes').columns[5].setText(BC.translate('debtorPart'));
        repConfig.getComponent('configNodes').columns[8].show();
        repConfig.getComponent('configNodes').columns[9].show();
        if (selectedNodesDialog.rendered) {
            selectedNodesDialog.selectedNodes.columns[0].setText(BC.translate('debtors'));
            selectedNodesDialog.selectedNodes.columns[1].hide();
            selectedNodesDialog.selectedNodes.columns[6].show();
            selectedNodesDialog.selectedNodes.columns[7].show();
        } else {
            selectedNodesDialog.selectedNodes.columns[0].text = BC.translate('debtors');
            selectedNodesDialog.selectedNodes.columns[1].hidden = true;
            selectedNodesDialog.selectedNodes.columns[6].hidden = false;
            selectedNodesDialog.selectedNodes.columns[7].hidden = false;
        }
        repConfig.getComponent('groupDivisionTargetHourlyRate').show();
        configButtonGroup.getComponent('showProfileButton').setText(BC.translate('generate'));
        configButtonGroup.getComponent('showProfileButton').setIcon('files/images/disk.png');
        selectedNodesDialog.selectedNodes.hideSets();

        this.getSelectedNodesStore().filter({
            filterFn: function(record) {
                return (
                    record.get('checked') && !record.get('isSet')
                    || (record.get('singleNodeTypes') & BC.store.SingleNodeTypes.MIS_INTERNAL) > 0
                    || (record.get('singleNodeTypes') & BC.store.SingleNodeTypes.MIS_SALES) > 0
                );
            }
        });
    },
    activateMisOverallCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configButtonGroup = me.getConfButtonGroup(),
            configPanel = me.getConfigPanel();

        repConfig.clearProfileData();
        repConfig.getComponent('configDateRange').show();
        configButtonGroup.getComponent('showProfileButton').setText(BC.translate('generate'));
        configButtonGroup.getComponent('showProfileButton').setIcon('files/images/disk.png');
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },
    activateStatusCfg: function() {
        var me = this,
            repConf = me.getRepConf(),
            selectedNodesDialog = me.getSelectedNodesDialog(),
            configPanel = me.getConfigPanel();

        repConf.clearProfileData();
        repConf.getComponent('configDateRange').show();
        repConf.getComponent('configNodes').show();
        repConf.getComponent('configNodes').columns[0].hide();
        repConf.getComponent('configNodes').columns[5].show();
        repConf.getComponent('configNodes').columns[5].setText(BC.translate('singleNodeStartPart'));
        repConf.getComponent('configNodes').columns[10].show();
        repConf.getComponent('configNodes').columns[11].show();

        selectedNodesDialog.selectedNodes.hideSets();

        repConf.getComponent('groupProjectStatus').show();
        repConf.getComponent('configBookingTypes').show();
        repConf.getComponent('configBookingTypes').setTitle(BC.translate('configInformationToBeDisplayed'));
        me.updateBookingTypeCheckboxLabels('displayInformation');

        if (this.getSelectedNodesDialog().rendered) {
            selectedNodesDialog.selectedNodes.columns[0].setText(BC.translate('singleNodeStartPart'));
            selectedNodesDialog.selectedNodes.columns[1].hide();
            selectedNodesDialog.selectedNodes.columns[2].show();
            selectedNodesDialog.selectedNodes.columns[8].show();
            selectedNodesDialog.selectedNodes.columns[9].show();
            selectedNodesDialog.selectedNodes.columns[10].show();
        } else {
            selectedNodesDialog.selectedNodes.columns[0].text = BC.translate('singleNodeStartPart');
            selectedNodesDialog.selectedNodes.columns[1].hidden = true;
            selectedNodesDialog.selectedNodes.columns[2].hidden = false;
            selectedNodesDialog.selectedNodes.columns[8].hidden = false;
            selectedNodesDialog.selectedNodes.columns[9].hidden = false;
            selectedNodesDialog.selectedNodes.columns[10].hidden = false;
        }
        configPanel.profileData.getComponent('reportMail').show();
        configPanel.profileData.getComponent('reportMailIcon').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();

        this.getSelectedNodesStore().filter({
            filterFn: function(record) {
                return (
                    record.get('checked') && !record.get('isSet')
                    || (record.get('singleNodeTypes') & BC.store.SingleNodeTypes.STATUS_DISTINCTROW) > 0
                    || (record.get('singleNodeTypes') & BC.store.SingleNodeTypes.STATUS_WITHNEXTLEVEL) > 0
                );
            }
        });
    },
    activateAccountableHoursCfg: function() {
        var me = this;
       me.getRepConf().getComponent('configDateRange').show();
        //it might be needed again
        //me.getRepConf().getComponent('configEmployees').show();
        me.getConfButtonGroup().getComponent('showProfileButton').setText(BC.translate('generate'));
        me.getConfButtonGroup().getComponent('showProfileButton').setIcon('files/images/disk.png');
    },
    activateOrderOverviewCfg: function() {
        var me = this,
            repConf = me.getRepConf(),
            nodeDisabledPlugin = me.getSelectedNodesDialog().selectedNodes.findPlugin("nodedisabled"),
            configPanel = me.getConfigPanel();

        repConf.clearProfileData();
        repConf.getComponent('configDateRange').show();
        repConf.getComponent('configNodes').show();
        repConf.getComponent('configOrderOverviewOptions').show();
        repConf.getComponent('configMaterialFilter').show();

        configPanel.profileData.getComponent('reportMail').show();
        configPanel.profileData.getComponent('reportMailIcon').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();

        nodeDisabledPlugin.disabledCls = 'tree-node-disabled tree-node-disabled-checkbox-hidden';
        nodeDisabledPlugin.disabledFn = function (record) {
            var disabledNodeTypes = [3, 4, 5, 6],
                disabledStructureUnitTypes = [4];

            if (record.get('nodeTypeId') === 1 && disabledStructureUnitTypes.length !== 0
                && disabledStructureUnitTypes.indexOf(record.get('structureUnitTypeId')) !== -1
            ) {
                return true;
            } else if (disabledNodeTypes.indexOf(record.get('nodeTypeId')) !== -1) {
                return true;
            }
        };
    },
    activateContingentRangeCfg: function () {
        var me = this,
            repConfig = me.getRepConf(),
            configButtonGroup = me.getConfButtonGroup(),
            configPanel = me.getConfigPanel();

        repConfig.clearProfileData();
        repConfig.getComponent('configEmployees').show();
        repConfig.getComponent('configContingentRangeInformationToBeDisplayed').show();

        configButtonGroup.getComponent('showProfileButton').setText(BC.translate('generate'));
        configButtonGroup.getComponent('showProfileButton').setIcon('files/images/disk.png');
        configPanel.profileData.getComponent('reportMail').show();
        configPanel.profileData.getComponent('reportMailIcon').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },

    activateContingentDataCfg: function() {
        var me = this,
            repConfig = me.getRepConf(),
            configButtonGroup = me.getConfButtonGroup(),
            configPanel = me.getConfigPanel();

        repConfig.clearProfileData();
        repConfig.getComponent('configDateRange').show();
        repConfig.getComponent('configContingentDataInformationToBeDisplayed').show();

        configButtonGroup.getComponent('showProfileButton').setText(BC.translate('generate'));
        configButtonGroup.getComponent('showProfileButton').setIcon('files/images/disk.png');
        configPanel.profileData.getComponent('reportMail').show();
        configPanel.profileData.getComponent('reportMailIcon').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },

    activateContingentSapInvoiceComparison: function () {
        var me = this,
            configPanel = me.getConfigPanel(),
            repConfig = me.getRepConf();

        repConfig.clearProfileData();
        me.getRepConf().getComponent('configDateRange').show();
        configPanel.profileData.getComponent('showProfile').show();
        configPanel.profileData.getComponent('profileName').enable();
    },

    activatePrismaValues: function() {
        this.getRepConf().getComponent('configEmployees').show();
        this.getRepConf().getComponent('configDateRange').show();
        this.getConfButtonGroup().getComponent('showProfileButton').setText(BC.translate('generate'));
        this.getConfButtonGroup().getComponent('showProfileButton').setIcon('files/images/disk.png');
    },

    updateBookingTypeCheckboxLabels: function (displayField) {
        var btStore = Ext.getStore('BookingTypes');

        Ext.each(this.getRepConf().getComponent('configBookingTypes').query('checkboxgroup checkbox'), function (checkbox) {
            var btRecord = btStore.getById(checkbox.inputValue);
            checkbox.setBoxLabel(btRecord.get(displayField));
        });
    },

    setBookingTypeCheckboxState: function() {
        var fieldset = this.getRepConf().getComponent('configBookingTypes'),
            toggleCheckbox = fieldset.getComponent('displayOnlySubtrees'),
            checkboxes = fieldset.query('checkboxgroup checkbox'),
            disabled = toggleCheckbox.isVisible() && toggleCheckbox.getValue() !== true;

        Ext.each(checkboxes, function(item) {
            item.setDisabled(disabled);
        });
    },

    // Actually only validates and then calls the 'real' save method
    saveProfile: function(isDefault, callbackfunction) {
        var config = this.getRepConf(),
            state = null;

        if (isDefault) {
            config.profile.set('hint', null);
        }

        if (!isDefault) {
            var name = this.getConfigPanel().profileData.getComponent('profileName').getValue();

            // Check if profile name consists of nothing but blanks
            if (Ext.String.trim(name) === '') {
                Ext.MessageBox.show({
                    buttons: Ext.Msg.OK,
                    title: ' ',
                    msg: BC.translate('profileNameBlanks'),
                    animateTarget: 'reportConfiguration',
                    icon: Ext.MessageBox.WARNING
                });
                return false;
            }

            // Trim blanks at the end and begining of the name-field,
            // after this is also done by the server
            this.getConfigPanel().profileData.getComponent('profileName').setValue(Ext.String.trim(name));

            state = this.sendProfileDataToServer(isDefault);
        } else {
            state = this.sendProfileDataToServer(isDefault, callbackfunction);
        }

        if (state === false) {
            this.getRepConf().getMaskTarget().unmask();
        }
    },

    /**
     * the 'real' saveProfile code,
     * extracted to its own method,
     * afer the user can confirm or cancel
     * when he tries to save under a name that is already in use
     */
    sendProfileDataToServer: function(isDefault, callbackfunction) {
        function ProfileSettings() {
            this.diagrammtyp = '';
            this.startwert = '';
            this.zeitraum_von = '';
            this.zeitraum_bis = '';
            this.eMail = '';
            this.reportmail = '';
            this.limit = '';
            this.projekt = '';
            this.nodes = [];
            this.employees = [];
            this.anzeige = '';
            this.notiztext = '';
            this.ccWithHourBookings = '';
            this.xAxis = '';
            this.showOnlyHoursBookingsWithBeginEnd = '';
            this.showEmpBookings = 0;
            this.showCostAndRevenue = 0;
            this.showBTHours = 0;
            this.showTargetHours = 0;
            this.showEmployeesWithoutBCPermission = '';
            this.showEmployeesAsGroups = '';
            this.showEmployeesWithAbsences = '';
            this.dynamicPeriod = '';
            this.divisionId = '';
            this.targetHourlyRate = '';
            this.bookingTypes = [];
            this.additionalCols = [];
            this.showZeroHoursPerWeek = '';
            this.showOneLinePerEmployee = '';
            this.showOneTabPerCompany = '';
            this.showToUsers = [];
            this.shareWithUsers = [];
            this.displayFigures = 0;
            this.showWithLockedNodes = 0;
            this.displayLines = 0;
            this.displayOnlySubtrees = 0;
            this.showClosedOrders = 0;
            this.showOrdersAfterPeriod = 0;
            this.showWithoutOrdersAfterPeriod = 0;
            this.showWithHigherLevelPosition = 0;
            this.showExternalEmployees = 0;
            this.contingentDataReportType = 0;
            this.materialFilter = [];
        }

        var profile = new ProfileSettings(),
            config = this.getRepConf();

        // Check if valid period has been entered
        var period = this.processPeriod(config);
        if (period.isValid === false) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: BC.translate(period.message),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        var selectedNodes = [],
            selectedSingleNodes = {},
            singleNodeTypes = this.getSingleNodeTypesStore().collect('value'),
            selectedEmployees = [],
            i;

        for (i = 0; i < singleNodeTypes.length; i++) {
            selectedSingleNodes[singleNodeTypes[i]] = [];
        }

        // Sets and nodes
        if (config.getComponent('configNodes').isVisible()) {
            this.getSelectedNodesStore().each(function(record) {
                profile.nodes.push({
                    uid: record.get('uid'),
                    id: record.get('id'),
                    isSet: record.get('isSet'),
                    name: record.get('name'),
                    checked: record.get('checked') && !record.get('disabled'),
                    singleNodeTypes: record.get('singleNodeTypes'),
                    nodeType: record.get('nodeType'),
                    structureUnitType: record.get('structureUnitType'),
                    iconCls: record.get('iconCls')
                });

                if (record.get('checked') && !record.get('disabled')) {
                    selectedNodes.push(record);
                }
                if (record.get('singleNodeTypes') !== 0) {
                    for (i = 0; i < singleNodeTypes.length; i++) {
                        if ((record.get('singleNodeTypes') & singleNodeTypes[i]) > 0) {
                            selectedSingleNodes[singleNodeTypes[i]].push(record);
                        }
                    }
                }
            });
        }

        // Employees and groups
        if (config.getComponent('configEmployees').isVisible()) {
            this.getSelectedEmployeesStore().each(function(record) {
                profile.employees.push({
                    uid: record.get('uid'),
                    id: record.get('id'),
                    isGroup: record.get('isGroup'),
                    name: record.get('name'),
                    shortcut: record.get('shortcut'),
                    first_name: record.get('first_name'),
                    last_name: record.get('last_name')
                });

                selectedEmployees.push(record);
            });
        }

        // If 'show cost & revenue' is selected in a report from type 'Booking'
        if (config.profile.get('type') === 'B' && config.getComponent('configTreeParams').getComponent('treeParamsCostAndRevenue') && config.getComponent('configTreeParams').getComponent('treeParamsCostAndRevenue').getValue()) {
            //it has to be validated if nodes are selected
            if (selectedNodes.length === 0) {
                Ext.MessageBox.alert(BC.translate('undefinederror_subject'), BC.translate('nodeNeeded4CostRevenue'));
                return false;
            }

            //it has to be validated that no employees are selected
            if (selectedEmployees.length > 0) {
                Ext.MessageBox.alert(BC.translate('undefinederror_subject'), BC.translate('noEmps4CostRevenue'));
                return false;
            }
        }

        // check list or remote work report
        if (config.profile.get('type') === 'L') {
            var onlyRemoteDaysCheckBox = Ext.getCmp('listOptionOnlyRemoteWorkDays'),
                remoteWorkReport = onlyRemoteDaysCheckBox !== undefined && onlyRemoteDaysCheckBox.getValue(),
                errorMessageKey = '';

            if (remoteWorkReport && selectedEmployees.length === 0) {
                errorMessageKey = 'employeeOrGroupRequired';
            } else if (!remoteWorkReport && selectedNodes.length === 0 && selectedEmployees.length === 0) {
                errorMessageKey = 'neitherNodeNorEmpl';
            }

            if (errorMessageKey.length > 0) {
                Ext.MessageBox.show({
                    buttons: Ext.Msg.OK,
                    title: ' ',
                    closable: false,
                    msg: BC.translate(errorMessageKey),
                    scope: this,
                    animateTarget: 'reportConfiguration',
                    icon: Ext.MessageBox.WARNING
                });
                return false;
            }
        }

        // Default check if either booking tree node or employee is selected
        if (/^[TEAMNRPOFGCIJKQZXVL]$/.test(config.profile.get('type')) === false && selectedNodes.length === 0 && selectedEmployees.length === 0) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: config.profile.get('type') !== 'R' ? BC.translate('neitherNodeNorEmpl') : BC.translate('neitherNodeNorEmplRebook'),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        // Check if either booking tree node or display node or employee is selected
        if (/^[AT]$/.test(config.profile.get('type')) === true
            && selectedNodes.length === 0
            && selectedSingleNodes[BC.store.SingleNodeTypes.TABLE_DISPLAY_NODE].length === 0
            && selectedEmployees.length === 0
        ) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: BC.translate('neitherNodeNorEmpl'),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        // For 'Rebook' report
        if (config.profile.get('type') === 'R') {
            // Only one not leaf node and one target cc can be selected
            // Target-CC can be holiday node only if all leaf nodes are holiday nodes
            var nonLeafNodes = 0,
                leafNodes = 0,
                sets = 0,
                hasHolidayNodeSelection = false,
                hasHolidaySingleNodeSelection = false;

            Ext.Array.each(selectedNodes, function(item) {
                if (item.get('isSet') === true) {
                    sets += 1;
                    return true;
                }

                if (item.get('leaf') === false && item.get('disabled') === false) {
                    nonLeafNodes += 1;
                } else if (item.get('leaf') === true && item.get('disabled') === false) {
                    leafNodes += 1;
                }

                if (item.get('leaf') === true && !hasHolidayNodeSelection) {
                    hasHolidayNodeSelection = item.get('isHoliday');
                }
            });

            Ext.Array.each(selectedSingleNodes[BC.store.SingleNodeTypes.REBOOK_TO_CC], function(item) {
                if (!hasHolidaySingleNodeSelection) {
                    hasHolidaySingleNodeSelection = item.get('isHoliday');
                }
            });

            var msg = null;
            if (!(sets === 0 && selectedNodes.length === 0
                  && selectedSingleNodes[BC.store.SingleNodeTypes.REBOOK_TO_CC].length === 0
                  && selectedEmployees.length > 0)
            ) {
                if (hasHolidayNodeSelection || hasHolidaySingleNodeSelection) {
                    msg = BC.translate('holidayLeafNodeSelected');
                } else if (sets > 0) {
                    msg = BC.translate('onlyOneSectionBeam');
                } else if (selectedSingleNodes[BC.store.SingleNodeTypes.REBOOK_TO_CC].length > 1) {
                    msg = BC.translate('moreThanOneRebookingTargetSelected');
                }
            }

            if (msg) {
                Ext.MessageBox.show({
                    buttons: Ext.Msg.OK,
                    title: ' ',
                    closable: false,
                    msg: msg,
                    scope: this,
                    animateTarget: 'reportConfiguration',
                    icon: Ext.MessageBox.WARNING
                });
                return false;
            }
        }

        if (config.getComponent('configMonthEndInformationToBeDisplayed').getComponent('monthEndOneTabPerCompany').getValue() === true &&
            config.profile.get('type') === 'M' &&
            Number(config.getComponent('configDateRange').getComponent('dynamicPeriod').getValue()) !== 1 &&
            Number(config.getComponent('configDateRange').getComponent('dynamicPeriod').getValue()) !== 2 &&
            !this.isFullMonthPeriod(config.getComponent('configDateRange').getComponent('dateRangeFrom').getValue(), config.getComponent('configDateRange').getComponent('dateRangeUntil').getValue(), true)
        ) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: BC.translate('oneTabPerCompanyErrorMessage'),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        // Check if a booking tree node or set is selected
        if (/^[FIOV]$/.test(config.profile.get('type')) === true && selectedNodes.length === 0) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: BC.translate('nodeOrSetRequired'),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        // Same as above but 'MisDivision' don't have sets.
        if (/^[I]$/.test(config.profile.get('type')) === true
            && selectedNodes.length === 0
            && selectedSingleNodes[BC.store.SingleNodeTypes.MIS_INTERNAL].length === 0
            && selectedSingleNodes[BC.store.SingleNodeTypes.MIS_SALES].length === 0
        ) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: BC.translate('nodeRequired'),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        // Same as above, but 'Status' can have either node or single node (at project) selected.
        if (/^[K]$/.test(config.profile.get('type')) === true
            && selectedNodes.length === 0
            && selectedSingleNodes[BC.store.SingleNodeTypes.STATUS_DISTINCTROW].length === 0
        ) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: BC.translate('nodeRequired'),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        // Check if an employee or group is selected
        if (/^[MECPY]$/.test(config.profile.get('type')) === true && selectedEmployees.length === 0) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: BC.translate('employeeOrGroupRequired'),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        // for report "Contingent range" check if an employee/group is selected or external employees are included
        if (config.profile.get('type') === 'Q'
            && selectedEmployees.length === 0
            && !config.getComponent('configContingentRangeInformationToBeDisplayed').getComponent('contingentRangeExternalEmployees').getValue()
        ) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: BC.translate('contingentRangeEmployeeRequired'),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        // For "MIS" reports check if division is set
        if (/^[I]$/.test(config.profile.get('type')) === true && !config.getComponent('groupDivisionTargetHourlyRate').getComponent('configDivision').getValue()) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: BC.translate('divisionRequired'),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        // For "MIS" reports check if target hourly rate is set and is not zero
        var targetHourlyRateValue = config.getComponent('groupDivisionTargetHourlyRate').getComponent('configTargetHourlyRate').getValue();

        if (/^[I]$/.test(config.profile.get('type')) === true && !targetHourlyRateValue) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: BC.translate('targetHourlyRateRequired'),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        if (/^[I]$/.test(config.profile.get('type')) === true && targetHourlyRateValue === 0) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: BC.translate('targetHourlyRateNotZero'),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        if (/^[I]$/.test(config.profile.get('type')) === true && targetHourlyRateValue === 0) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: BC.translate('targetHourlyRateNotZero'),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        if (/^[I]$/.test(config.profile.get('type')) === true && targetHourlyRateValue === 0) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: BC.translate('targetHourlyRateNotZero'),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        //Display figures
        if (/^[K]$/.test(config.profile.get('type')) === true) {
            if (config.getComponent('groupProjectStatus').getComponent('configDisplayFigures').getComponent('perPeriod').getValue()) {
                profile.displayFigures = BC.store.status.DisplayFigures.ONCE_FOR_WHOLE_PERIOD;
            } else if (config.getComponent('groupProjectStatus').getComponent('configDisplayFigures').getComponent('perMonth').getValue()) {
                profile.displayFigures = BC.store.status.DisplayFigures.PER_MONTH;
            }
        } else { //if not report 'K' reset radio button
            profile.displayFigures = BC.store.status.DisplayFigures.ONCE_FOR_WHOLE_PERIOD;
            config.getComponent('groupProjectStatus').getComponent('configDisplayFigures').getComponent('perPeriod').setValue(true);
            config.getComponent('groupProjectStatus').getComponent('configDisplayFigures').getComponent('perMonth').setValue(false);
        }

        //Locked nodes
        if (/^[K]$/.test(config.profile.get('type')) === true) {
            if (config.getComponent('groupProjectStatus').getComponent('configLockedNodes').getComponent('withLockedNodes').getValue()) {
                profile.showWithLockedNodes = BC.store.status.LockedNodes.WITH_LOCKED_NODES;
            } else if (config.getComponent('groupProjectStatus').getComponent('configLockedNodes').getComponent('withoutLockedNodes').getValue()) {
                profile.showWithLockedNodes = BC.store.status.LockedNodes.WITHOUT_LOCKED_NODES;
            }
        } else if (/^[F]$/.test(config.profile.get('type')) === true) {
            // Show btOrder with locked or without nodes
            profile.showWithLockedNodes = config.getComponent('configDisplayLockedNodes').getComponent('displayLockedNodesNo').getValue() ? 1 : 0;
        } else { //if not report 'K' reset radio button
            profile.showWithLockedNodes = BC.store.status.LockedNodes.WITH_LOCKED_NODES;
            config.getComponent('groupProjectStatus').getComponent('configLockedNodes').getComponent('withLockedNodes').setValue(true);
            config.getComponent('groupProjectStatus').getComponent('configLockedNodes').getComponent('withoutLockedNodes').setValue(false);
        }

        if (/^[B]$/.test(config.profile.get('type')) === true) {
            if (config.getComponent('configTreeParams').getComponent('withoutLockedNodesCheckbox').getValue()) {
                profile.showWithLockedNodes = BC.store.status.LockedNodes.WITHOUT_LOCKED_NODES;
            } else {
                profile.showWithLockedNodes = BC.store.status.LockedNodes.WITH_LOCKED_NODES;
            }
        }

        //Display lines
        if (/^[K]$/.test(config.profile.get('type')) === true) {
            if (config.getComponent('groupProjectStatus').getComponent('configDisplayLines').getComponent('onlyIfHoursAmountsInSelectedGeneralPeriod').getValue()) {
                profile.displayLines = BC.store.status.DisplayLines.ONLY_IF_HOURS_AMOUNTS_IN_SELECTED_GENERAL_PERIOD;
            } else if (config.getComponent('groupProjectStatus').getComponent('configDisplayLines').getComponent('onlyIfHoursAmountsInSelectedPeriod').getValue()) {
                profile.displayLines = BC.store.status.DisplayLines.ONLY_IF_HOURS_AMOUNTS_IN_SELECTED_PERIOD;
            } else {
                profile.displayLines = BC.store.status.DisplayLines.ONLY_IF_HOURS_IN_SELECTED_PERIOD;
            }
        } else {
            profile.displayLines = BC.store.status.DisplayLines.ONLY_IF_HOURS_AMOUNTS_IN_SELECTED_GENERAL_PERIOD;
            config.getComponent('groupProjectStatus').getComponent('configDisplayLines').getComponent('onlyIfHoursAmountsInSelectedGeneralPeriod').setValue(true);
            config.getComponent('groupProjectStatus').getComponent('configDisplayLines').getComponent('onlyIfHoursAmountsInSelectedPeriod').setValue(false);
            config.getComponent('groupProjectStatus').getComponent('configDisplayLines').getComponent('onlyIfHoursInSelectedPeriod').setValue(false);
        }

        // Dynamic period - Permission Report has no dynamic period
        if (/^[PF]$/.test(config.profile.get('type')) === false) {
            profile.dynamicPeriod = config.getComponent('configDateRange').getComponent('dynamicPeriod').getValue();
        } else {
            profile.dynamicPeriod = 0;
        }

        if (period.from) {
            profile.zeitraum_von = Ext.Date.format(period.from, 'Y/m/d');
        }
        if (period.till) {
            profile.zeitraum_bis = Ext.Date.format(period.till, 'Y/m/d');
        }

        // Day or month values
        if (config.getComponent('groupAccType').getComponent('configAccuracy').getComponent('dayValuesRadio').getValue()) {
            profile.diagrammtyp = 'T';
        } else {
            profile.diagrammtyp = 'M';
        }

        // Is 'cumulated' & has 'startvalue'
        if (config.getComponent('groupAccType').getComponent('configDiagramType').getComponent('diagramTypeRadioCumWith').getValue()) {
            profile.diagrammtyp += 'K';
            profile.startwert = true;
        } else if (config.getComponent('groupAccType').getComponent('configDiagramType').getComponent('diagramTypeRadioCumWithOut').getValue()) {
            profile.diagrammtyp += 'K';
            profile.startwert = false;
        } else { // case: 'single'
            profile.diagrammtyp += 'E';
            // startvalue is not really relevant for 'single'
            profile.startwert = false;
        }

        // CC with hours bookings
        profile.ccWithHourBookings = config.getComponent('groupHourBookings').getComponent('configCCWithHourBookings').getComponent('ccWithHourBookingsYes').getValue() ? 1 : 0;

        // X-Axis
        profile.xAxis = config.getComponent('groupHourBookings').getComponent('configXAxis').getComponent('xAxisEmployees').getValue() ? 'E' : 'M';

        // Division
        if (/^[I]$/.test(config.profile.get('type')) === true) {
            profile.divisionId = config.getComponent('groupDivisionTargetHourlyRate').getComponent('configDivision').getValue();
        }

        // Target hourly rate
        if (/^[I]$/.test(config.profile.get('type')) === true) {
            profile.targetHourlyRate = config.getComponent('groupDivisionTargetHourlyRate').getComponent('configTargetHourlyRate').getValue();
        }

        // eMail
        profile.eMail =
            this.getConfigPanel().profileData.getComponent('autoMail').isHidden()
            ? '0'
            : this.getConfigPanel().profileData.getComponent('autoMail').getValue();

        // Report type mail
        profile.reportMail =
            this.getConfigPanel().profileData.getComponent('reportMail').isHidden()
            ? '0'
            : this.getConfigPanel().profileData.getComponent('reportMail').getValue();

        // Report type
        profile.anzeige = config.profile.get('type');

        // Limit
        if (/^[DBLT]$/.test(config.profile.get('type')) === true) {
            profile.limit = config.getComponent('groupHrsNoteLimit').getComponent('configLimit').getValue();
        } else {
            profile.limit = 0;
        }

        if (/^Z$/.test(config.profile.get('type')) === true) {
            profile.contingentDataReportType = config.getComponent('configContingentDataInformationToBeDisplayed').getComponent('contingentDataReportType').getValue().contingentDataReportType;
        }

        // 0 does not make sense
        if (profile.limit === 0) {
            profile.limit = '';
            config.getComponent('groupHrsNoteLimit').getComponent('configLimit').setRawValue('');
        }

        // Hrs note
        profile.notiztext =
            config.getComponent('groupHrsNoteLimit').isHidden()
            ? ''
            : config.getComponent('groupHrsNoteLimit').getComponent('configHrsNote').getValue();

        // Show EmployeeBookings
        profile.showEmpBookings = config.getComponent('configTreeParams').getComponent('treeParamsEmpBookings').getValue() ? 1 : 0;

        // Show Cost&Revenue
        if (global.privileges.costAndRevenue === true) {
            profile.showCostAndRevenue = config.getComponent('configTreeParams').getComponent('treeParamsCostAndRevenue').getValue() ? 1 : 0;
        } else {
            profile.showCostAndRevenue = 0;
        }

        // Show booking type hours
        profile.showBTHours = config.getComponent('configTreeParams').getComponent('treeParamsBTHours').getValue() ? 1 : 0;

        // Show target hours
        profile.showTargetHours = config.getComponent('configTreeParams').getComponent('treeParamsTargetHours').getValue() ? 1 : 0;

        // Show only hour bookings with begin/end
        profile.showBookingsWithBeginEnd = config.getComponent('configListOptions').getComponent('listOptionOnlyHourBookingsWithBeginEnd').getValue() ? 1 : 0;

        // Show only remote work days
        profile.showRemoteWorkDays = config.getComponent('configListOptions').getComponent('listOptionOnlyRemoteWorkDays').getValue() ? 1 : 0;

        // show with closed positions of orders which relates to the configured period
        profile.showClosedOrders = config.getComponent('configOrderOverviewOptions').getComponent('orderOverViewOptionClosedOrders').getValue() ? 1 : 0;

        // show with orders and positions which start after the configured period
        profile.showOrdersAfterPeriod = config.getComponent('configOrderOverviewOptions').getComponent('orderOverViewOptionOrdersAfterPeriod').getValue() ? 1 : 0;

        // show without orders terminating before the configured period and still having open positions
        profile.showWithoutOrdersAfterPeriod = config.getComponent('configOrderOverviewOptions').getComponent('orderOverViewOptionWithoutOrdersAfterPeriod').getValue() ? 1 : 0;

        // show without orders terminating before the configured period and still having open positions
        profile.showWithHigherLevelPosition = config.getComponent('configOrderOverviewOptions').getComponent('orderOverViewOptionWithHigherLevelPosition').getValue() ? 1 : 0;

        // Show Employees without Permission
        profile.showEmployeesWithoutBCPermission = config.getComponent('configNoBCPermission').getComponent('noBCPermission').getValue() ? 1 : 0;

        // Show Employees as Groups
        profile.showEmployeesAsGroups = config.getComponent('configAbsenceConfig').getComponent('absenceConfigGroupEmployees').getValue() ? 1 : 0;

        // Show only employees with absences
        profile.showEmployeesWithAbsences = config.getComponent('configAbsenceConfig').getComponent('absenceConfigEmpWithAbsences').getValue() ? 1 : 0;

        // Show rebooks with the following performed or booking date
        profile.showPerformedOrBookingDate = config.getComponent('configPerformedOrBookingDate').getComponent('performedOrBookingDatePerformed').getValue() ? 0 : 1;

        // Display only subtrees with the selected booking types
        profile.displayOnlySubtrees = config.getComponent('configBookingTypes').getComponent('displayOnlySubtrees').getValue() ? 1 : 0;

        // Booking types
        if (!config.getComponent('configBookingTypes').isHidden()) {
            Ext.Array.each(config.getComponent('configBookingTypes').query('checkboxgroup checkbox'), function(checkbox) {
                if (checkbox.getValue()) {
                    profile.bookingTypes.push(checkbox.inputValue);
                }
            }, this);
        }

        if (profile.bookingTypes.length === 0
            && (
                /^[RK]$/.test(config.profile.get('type')) === true ||
                config.profile.get('type') === 'B' && profile.displayOnlySubtrees === 1
            )
        ) {
            Ext.MessageBox.show({
                buttons: Ext.Msg.OK,
                title: ' ',
                closable: false,
                msg: BC.translate(config.profile.get('type') === 'R' ? 'bookingTypeRequired' : 'noBookingTypeSelected'),
                scope: this,
                animateTarget: 'reportConfiguration',
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }

        // MonthEnd / List extra columns
        Ext.each(['MonthEnd', 'List'], function(rep) {
            var cmp = config.getComponent('config' + rep + 'ExtraCols');
            if (!cmp.isHidden()) {
                Ext.Array.each(cmp.items.items, function(checkbox) {
                    if (checkbox.getValue()) {
                        profile.additionalCols.push(checkbox.inputValue);
                    }
                }, this);
            }
        });

        // Show Employees with zero hours per week
        profile.showZeroHoursPerWeek = config.getComponent('configMonthEndInformationToBeDisplayed').getComponent('monthEndZeroHours').getValue() ? 1 : 0;

        profile.showOneLinePerEmployee = config.getComponent('configMonthEndInformationToBeDisplayed').getComponent('monthEndOneLinePerEmployee').getValue() ? 1 : 0;

        profile.showOneTabPerCompany = config.getComponent('configMonthEndInformationToBeDisplayed').getComponent('monthEndOneTabPerCompany').getValue() ? 1 : 0;

        profile.showExternalEmployees = config.getComponent('configContingentRangeInformationToBeDisplayed').getComponent('contingentRangeExternalEmployees').getValue() ? 1 : 0;

        // Users to whom this profile should be shown
        profile.showToUsers = this.getConfigPanel().profileData.getComponent('showProfile').getValue();

        // Users to share this profile with
        profile.shareWithUsers = this.getConfigPanel().profileData.getComponent('shareProfile').getValue();

        profile.materialFilter = config.getComponent('configMaterialFilter').getComponent(0).getValue();

        var name = '',
            hint = null,
            getRequestParamsFn = function(btn, fn, scope) {
                var params = {
                    create: null,
                    update: null,
                    remove: null,
                    profileData: null
                };

                if (fn) {
                    Ext.callback(fn, scope || this, [btn, params]);
                }

                return params;
            },
            prepareParamsFn = null;

        msg = null;

        if (!isDefault) {
            name = this.getConfigPanel().profileData.getComponent('profileName').getValue();
            hint = this.getRepConf().profile.get('hint');
            let profileData = {
                'id' : this.getRepConf().profile.get('id'),
                'own' : this.getRepConf().profile.get('own')
            }

            var idxOfProfile = this.findProfile('name', name),
                buttons = Ext.MessageBox.YES + Ext.MessageBox.CANCEL,
                buttonText = {
                    ok: "Ok",
                    yes: BC.translate('replace'),
                    cancel: BC.translate('abbrechen')
                };

            if (name === hint) {
                msg = BC.translate('replaceProfileInfo');
                prepareParamsFn = function(btn, params) {
                    params.update = hint;
                    params.profileData = profileData;
                };
            }

            if (!msg && hint && name.toUpperCase() === hint.toUpperCase()) {
                msg = Ext.String.format(BC.translate('replaceNamedProfileInfo'), hint);

                prepareParamsFn = function(btn, params) {
                    params.update = hint;
                    params.profileData = profileData;
                };
            }

            if (!msg && hint && name.toUpperCase() !== hint.toUpperCase() && idxOfProfile === -1) {
                msg = BC.translate('profileNameChangedInfo');
                buttons = Ext.MessageBox.OK + Ext.MessageBox.YES + Ext.MessageBox.CANCEL;
                buttonText = {
                    ok: BC.translate('replaceProfile'),
                    yes: BC.translate('createNewProfile'),
                    cancel: BC.translate('abbrechen')
                };
                prepareParamsFn = function(btn, params) {
                    switch (btn) {
                        case "ok":
                            params.update = hint;
                            params.profileData = profileData;
                            break;
                        case "yes":
                            params.create = name;
                            break;
                    }
                };
            }

            if (!msg && hint && name.toUpperCase() !== hint.toUpperCase() && idxOfProfile !== -1) {
                msg = Ext.String.format(BC.translate('replaceProfileXInfo'), hint);
                buttons = Ext.MessageBox.OK + Ext.MessageBox.YES + Ext.MessageBox.CANCEL;
                buttonText = {
                    ok: BC.translate('removeProfileX'),
                    yes: BC.translate('keepProfileX'),
                    cancel: BC.translate('abbrechen')
                };

                prepareParamsFn = function(btn, params) {
                    switch (btn) {
                        case "ok":
                            params.update = this.getStore('Profiles').getAt(idxOfProfile).get('name');
                            params.profileData = profileData;
                            params.remove = hint;
                            break;
                        case "yes":
                            params.update = this.getStore('Profiles').getAt(idxOfProfile).get('name');
                            params.profileData = profileData;
                            break;
                    }
                };
            }

            if (!msg && !hint && idxOfProfile !== -1) {
                msg = BC.translate('profileExistsInfo');
                buttonText = {
                    yes: BC.translate('overwriteProfile'),
                    cancel: BC.translate('abbrechen')
                };

                prepareParamsFn = function(btn, params) {
                    params.update = this.getStore('Profiles').getAt(idxOfProfile).get('name');
                    params.profileData = profileData;
                };
            }

            if (!msg && !hint && idxOfProfile === -1) {
                prepareParamsFn = function(btn, params) {
                    params.create = name;
                };
            }

            if (msg) {
                var restoreOriginalBtnText = function(msgBox) {
                        Ext.Array.each(msgBox.msgButtons, function(button) {
                            if (button.itemId === 'ok') {
                                button.setText(Ext.MessageBox.buttonText.ok);
                            }
                            if (button.itemId === 'yes') {
                                button.setText(Ext.MessageBox.buttonText.yes);
                            }
                            if (button.itemId === 'no') {
                                button.setText(Ext.MessageBox.buttonText.no);
                            }
                            if (button.itemId === 'cancel') {
                                button.setText(Ext.MessageBox.buttonText.cancel);
                            }
                        });
                    },
                    msgBox = Ext.MessageBox.show({
                        buttons: buttons,
                        title: ' ',
                        msg: msg,
                        scope: this,
                        closable: false,
                        fn: function(btn) {
                            restoreOriginalBtnText(msgBox);
                            if (btn === 'ok' || btn === 'yes') {
                                this.getRepConf().getMaskTarget().mask('...');
                                this.doSaveRequest(name, callbackfunction, config, profile, getRequestParamsFn(btn, prepareParamsFn, this));
                            } else {
                                config.getMaskTarget().unmask();
                            }
                        },
                        animateTarget: 'reportConfiguration',
                        icon: Ext.MessageBox.WARNING
                    });

                // Change mesage dialog button text
                Ext.Array.each(msgBox.msgButtons, function(button) {
                    if (button.itemId === 'ok') {
                        button.setText(buttonText.ok);
                    }
                    if (button.itemId === 'yes') {
                        button.setText(buttonText.yes);
                    }
                    if (button.itemId === 'cancel') {
                        button.setText(buttonText.cancel);
                    }
                });
            } else {
                this.getRepConf().getMaskTarget().mask('...');
                this.doSaveRequest(name, callbackfunction, config, profile, getRequestParamsFn(null, prepareParamsFn, this));
            }
        } else {
            this.doSaveRequest(name, callbackfunction, config, profile, getRequestParamsFn());
        }
    },

    doSaveRequest: function(name, callbackfunction, config, profile, params) {
        Ext.Ajax.request({
            url: "index/savechartprofile",
            params: {
                bezeichnung: name,
                configuration: Ext.JSON.encode(profile),
                params: Ext.JSON.encode(params)
            },
            scope: this,
            success: function(response) {
                if (response && response.responseText) {
                    var result = Ext.JSON.decode(response.responseText);

                    if (result && result.result && result.result !== false) {
                        config.getMaskTarget().unmask();

                        var record = null,
                            store = this.getStore('Profiles'),
                            from = Ext.Date.parse(profile.zeitraum_von, 'Y/m/d'),
                            to = Ext.Date.parse(profile.zeitraum_bis, 'Y/m/d'),
                            properties = {
                                'hint': name || null,
                                'type': profile.anzeige,
                                'from': from,
                                'to': to,
                                'dynamicPeriod': profile.dynamicPeriod,
                                'displayPeriod': {
                                    from: Ext.isDate(from) ? Ext.Date.format(from, 'Ymd') : '',
                                    to: Ext.isDate(to) ? Ext.Date.format(to, 'Ymd') : '',
                                    dynamicPeriod: profile.dynamicPeriod
                                },
                                'limit': profile.limit,
                                'diagramType': profile.diagrammtyp,
                                'hoursNote': profile.notiztext,
                                'startValue': profile.startwert,
                                'ccWithHourBookings': profile.ccWithHourBookings,
                                'xAxis': profile.xAxis,
                                'divisionId': profile.divisionId,
                                'targetHourlyRate': profile.targetHourlyRate,
                                'bookingTypes': profile.bookingTypes,
                                'additionalCols': profile.additionalCols,
                                'showCostAndRevenue': profile.showCostAndRevenue,
                                'showEmpBookings': profile.showEmpBookings,
                                'showBTHours': profile.showBTHours,
                                'showTargetHours': profile.showTargetHours,
                                'showBookingsWithBeginEnd': profile.showBookingsWithBeginEnd,
                                'showRemoteWorkDays': profile.showRemoteWorkDays,
                                'showPerformedOrBookingDate': profile.showPerformedOrBookingDate,
                                'showWithLockedNodes': profile.showWithLockedNodes,
                                'showZeroHoursPerWeek': profile.showZeroHoursPerWeek,
                                'showOneLinePerEmployee': profile.showOneLinePerEmployee,
                                'showOneTabPerCompany': profile.showOneTabPerCompany,
                                'showToUsers': profile.showToUsers,
                                'shareWithUsers': profile.shareWithUsers,
                                'mail': profile.eMail,
                                'reportMail': profile.reportMail,
                                'nodes': profile.nodes,
                                'employees': profile.employees,
                                'information': profile.information,
                                'displayFigures': profile.displayFigures,
                                'displayLines': profile.displayLines,
                                'displayOnlySubtrees': profile.displayOnlySubtrees,
                                'showClosedOrders': profile.showClosedOrders,
                                'showOrdersAfterPeriod': profile.showOrdersAfterPeriod,
                                'showWithoutOrdersAfterPeriod': profile.showWithoutOrdersAfterPeriod,
                                'showWithHigherLevelPosition': profile.showWithHigherLevelPosition,
                                'showExternalEmployees': profile.showExternalEmployees,
                                'contingentDataReportType': profile.contingentDataReportType,
                                'materialFilter': profile.materialFilter
                            };

                        // Default profile
                        if (name === "") {
                            record = store.defaultProfile;
                            record.set(properties);
                            record.commit();
                        } else {
                            if (params.create) {
                                record = new BC.model.Profile(Ext.apply(properties, {
                                    id: result.result,
                                    name: name
                                }));
                                store.add(record);
                                store.sort();
                            }

                            if (params.update) {
                                record = store.getAt(store.findExact('name', params.update));
                                Ext.apply(properties, {name: name});
                                record.set(properties);
                                record.commit();
                            }

                            if (params.remove) {
                                store.removeAt(store.findExact('name', params.remove));
                            }

                            if (this.getRepConf().profile.get('hint')) {
                                this.getRepConf().profile.set('hint', properties.hint);
                            }
                        }

                        Ext.callback(callbackfunction, this, [record.copy()]);
                    } else {
                        Ext.MessageBox.alert(BC.translate('undefinederror_subject'), BC.translate('allgemeinerfehler'));
                    }
                }
            },
            failure: function() {
                config.getMaskTarget().unmask();
                Ext.MessageBox.alert(BC.translate('undefinederror_subject'), BC.translate('allgemeinerfehler'));
            }
        });
    },

    processPeriod: function(config) {
        var fieldFrom = config.getComponent('configDateRange').getComponent('dateRangeFrom'),
            fieldTill = config.getComponent('configDateRange').getComponent('dateRangeUntil'),
            dynPeriod = Number(config.getComponent('configDateRange').getComponent('dynamicPeriod').getValue()),
            from = fieldFrom.getValue(),
            till = fieldTill.getValue(),
            response = {
                isValid: false,
                message: 'zeitraumungueltig',
                from: from,
                till: till
            };

        // reports without period configuration
        if (/^[PFQ]$/.test(config.profile.get('type')) === true) {
            return Ext.apply(response, {
                isValid: true,
                message: '',
                from: null,
                till: null
            });
        }

        if (
            !fieldFrom.isValid() ||
            !fieldTill.isValid() ||
            dynPeriod === 0 && (
                !(from instanceof Date) ||
                !(till instanceof Date) && from > Ext.Date.clearTime(new Date()) ||
                till instanceof Date && from > till
            )
        ) {
            return response;
        }

        // valid dynamic periods per report
        // current/last year[9,10],
        // current/last quarter[7,8],
        // current/last month[1,2],
        // none[0]
        var reportType = config.profile.get('type'),
            reportPeriods = {
                // first index indicates whether it must be a single month period or not
                'B': [false, 0, 1, 2, 7, 8, 9, 10, 11],
                'A': [true, 0, 1, 2],
                'M': [false, 0, 1, 2, 7, 8, 9, 10, 11],
                'I': [false, 0, 1, 2, 7, 8, 9, 10, 11],
                'J': [false, 0, 1, 2, 7, 8, 9, 10, 11],
                'K': [false, 0, 1, 2, 7, 8, 9, 10, 11],
                'G': [false, 0, 1, 2, 7, 8, 9, 10, 11],
                'Z': [true, 0, 1, 2],
                'X': [true, 0, 1, 2]
            };

        // validate dynamic period for certain report types / configurations
        switch (reportType) {
            case 'B':
                if (!config.getComponent('configTreeParams').getComponent('treeParamsCostAndRevenue')
                    || !config.getComponent('configTreeParams').getComponent('treeParamsCostAndRevenue').getValue()
                ) {
                    break;
                }
                response.message = 'onlyFullMonths';
            case 'A':
            case 'M':
            case 'I':
            case 'J':
            case 'K':
            case 'G':
            case 'Z':
            case 'X':
                if (
                    dynPeriod === 0 && !this.isFullMonthPeriod(from, till, reportPeriods[reportType][0]) ||
                    Ext.Array.indexOf(reportPeriods[reportType], dynPeriod) === -1
                ) {
                    return response;
                }
                break;
            case 'Y':
                // only today is a valid date-selection
                if (dynPeriod !== 5) {
                    return response;
                }
                break;
        }

        // everything is valid
        return Ext.apply(response, {
            isValid: true,
            message: ''
        });
    },

    /**
     * Checks if period dates belongs to the same month, i.e. starts at first day
     * and ends of last day of the same month
     */
    isFullMonthPeriod: function(from, till, sameMonth) {
        return (
            from instanceof Date &&
            till instanceof Date &&
            from.getDate() === 1 &&
            Ext.Date.getLastDateOfMonth(till).getDate() === till.getDate() &&
            (
                !sameMonth ||
                from.getMonth() === till.getMonth() && from.getYear() === till.getYear()
            )
        );
    },

    /**
     * Set the checkbox state of a tree node according to the passed profile node
     * If no profile node is passed to the function, the tree node is unchecked
     */
    setTreeNodeCheckState: function(treeNode, profileNode) {
        var treePanel = this.getNodesTree(),
            oldChecked = treeNode.get('checked'),
            oldSingleNodeTypes = treeNode.get('singleNodeTypes'),
            newChecked = false,
            newSingleNodeTypes = 0;

        // make sure node is checkable first
        if (oldChecked === null) {
            return;
        }

        // if parameter profile node was passed, use it for setting the checked state
        if (profileNode instanceof BC.model.Node) {
            newChecked = profileNode.get('checked');
            newSingleNodeTypes = profileNode.get('singleNodeTypes');
        }

        // IMPORTANT: only set values in node and fire events if something has
        // actually changed to prevent performance problems (this function
        // is called from iterations over the whole tree)

        // regular selection
        if (oldChecked !== newChecked
            && !treeNode.get('disabled')
        ) {
            treeNode.set('checked', newChecked);
            treePanel.fireEvent('checkchange', treeNode, newChecked);
        }

        // single node selection
        if (oldSingleNodeTypes !== newSingleNodeTypes) {
            this.getSingleNodeTypesStore().each(function(singleNodeType) {
                // fire event for old and new singleNodeTypes
                if ((singleNodeType.get('value') & oldSingleNodeTypes) > 0
                    || (singleNodeType.get('value') & newSingleNodeTypes) > 0
                ) {
                    treeNode.set('singleNodeTypes', newSingleNodeTypes);
                    treePanel.fireEvent('singlenodecheckchange', treeNode, singleNodeType.get('value'), singleNodeType.get('forceChildSelection'));
                }
            });
        }
    },

    /**
     * Applies the profile's node to the tree
     */
    applyProfileNodesToTree: function() {
        var selectedNodesStore = this.getSelectedNodesStore(),
            treeStore = this.getNodesStore(),
            treeView = this.getNodesTree().getView(),
            preventRefreshFn = function () {
                return false;
            };

        Ext.suspendLayouts();
        treeView.on('beforerefresh', preventRefreshFn);
        treeStore.getRootNode().cascadeBy(function(node) {
            var record = selectedNodesStore.getById(node.get('uid'));
            this.setTreeNodeCheckState(node, record);
        }, this);
        treeView.un('beforerefresh', preventRefreshFn);
        treeView.refresh();
        Ext.resumeLayouts(true);
    },

    /**
     * Applies the selection from the tree to the grid store
     */
    applyTreeNodesToProfile: function() {
        var selectedNodesStore = this.getSelectedNodesStore(),
            treeStore = this.getNodesStore(),
            iteration = 0;

        // add new records to the grid store
        Ext.suspendLayouts();
        treeStore.getRootNode().cascadeBy(function(node) {
            if (iteration++ === 0) {
                return;
            }

            var record = selectedNodesStore.getById(node.get('uid'));

            if (node.get('checked') && !node.get('disabled') || node.get('singleNodeTypes')) {
                if (record) {
                    record.set(node.getData());
                } else {
                    // TODO: might be better to not copy the whole NodeInterface, but only the required attributes
                    record = node.copy();
                    selectedNodesStore.add(record);
                }
                // set node path
                record.set({
                    'path_id': node.getPath('id').replace(/\/{2,}/, '/'),
                    'path_name': node.getPath('name').replace(/\/{2,}/, '/')
                });

                // check if any child nodes (which have not been loaded yet) exist in the grid
                var regExp = new RegExp('\\/' + node.get('id') + '\\/'),
                    i = -1,
                    childRecord,
                    childRecordFn = function(singleNodeType) {
                        if (singleNodeType.get('forceChildSelection')
                            && (singleNodeType.get('value') & record.get('singleNodeTypes')) > 0
                            && (singleNodeType.get('value') & childRecord.get('singleNodeTypes')) > 0
                        ) {
                            childRecord.set('singleNodeTypes', childRecord.get('singleNodeTypes') & ~singleNodeType.get('value'));
                        }
                    };
                i = selectedNodesStore.find('path', regExp, i + 1);
                while (i !== -1) {
                    childRecord = selectedNodesStore.getAt(i);
                    // set checked state according to parent record
                    childRecord.set('checked', !record.get('checked'));
                    // set single node types (which force child selection) according to parent record
                    this.getSingleNodeTypesStore().each(childRecordFn);
                    // if neither regular nor single node type selection is given
                    // after the changes above, remove the record
                    if (!childRecord.get('checked')
                        && childRecord.get('singleNodeTypes') === 0
                    ) {
                        selectedNodesStore.removeAt(i--);
                    }
                }

                selectedNodesStore.sync();
            } else if (record) {
                selectedNodesStore.remove(record);
                selectedNodesStore.sync();
            }
        }, this);
        Ext.resumeLayouts(true);

        // re-apply current filter
        var filters = selectedNodesStore.filters.getRange();
        selectedNodesStore.clearFilter(true);
        selectedNodesStore.filter(filters);
    },

    setEmployeeNodeCheckState: function(treeNode, profileEmployee) {
        var treePanel = this.getEmployeesTree(),
            oldChecked = treeNode.get('checked'),
            newChecked = false;

        // make sure node is checkable first
        if (oldChecked === null) {
            return;
        }

        // if parameter profile node was passed, use it for setting the checked state
        if (profileEmployee instanceof BC.model.Employee) {
            newChecked = profileEmployee.get('checked');
        }

        // IMPORTANT: only set values in node and fire events if something has
        // actually changed to prevent performance problems (this function
        // is called from iterations over the whole tree)

        if (oldChecked !== newChecked) {
            treeNode.set('checked', newChecked);
            treePanel.fireEvent('checkchange', treeNode, newChecked);
        }
    },

    /**
     * Applies the profile's node to the tree
     */
    applyProfileEmployeesToTree: function() {
        var selectedEmployeesStore = this.getSelectedEmployeesStore(),
            treeStore = this.getEmployeesStore();

        Ext.suspendLayouts();
        treeStore.getRootNode().cascadeBy(function(node) {
            var record = selectedEmployeesStore.getById(node.get('uid'));
            this.setEmployeeNodeCheckState(node, record);
        }, this);
        Ext.resumeLayouts(true);
    },

    /**
     * Applies the selection from the tree to the grid store
     */
    applyTreeEmployeesToProfile: function() {
        var selectedEmployeesStore = this.getSelectedEmployeesStore(),
            treeStore = this.getEmployeesStore(),
            iteration = 0;

        // add new records to the grid store
        Ext.suspendLayouts();
        treeStore.getRootNode().cascadeBy(function(node) {
            if (iteration++ === 0) {
                return;
            }

            var record = selectedEmployeesStore.getById(node.get('uid'));

            if (node.get('checked')) {
                if (record) {
                    record.set(node.getData());
                    record.commit();
                } else {
                    // TODO: might be better to not copy the whole NodeInterface, but only the required attributes
                    record = node.copy();
                    selectedEmployeesStore.add(record);
                    selectedEmployeesStore.sync();
                }
            } else if (record) {
                selectedEmployeesStore.remove(record);
                selectedEmployeesStore.sync();
            }
        }, this);
        Ext.resumeLayouts(true);
    },

    uncheckAllTreeNodes: function(tree) {
        Ext.suspendLayouts();

        tree.getStore().getRootNode().cascadeBy(function(node) {
            if (node.get('checked') === true) {
                node.set('checked', false);
                tree.fireEvent('checkchange', node, false);
                return false;
            }
        }, this);

        Ext.resumeLayouts(true);
    },

    showPathToRootTooltip: function(nodeId, pos) {
        pos[0] += 15;
        pos[1] -= 15;

        Ext.create('BC.lib.tooltip.TreePath', {
            nodeId: nodeId
        }).showAt(pos);
    },

    findProfile: function(property, value) {
        return this.getStore('Profiles').find(property, value, 0, false, false, true);
    },

    validateTreeSearchField: function (field) {
        var panel = field.up('configTreePanel'),
            value = Ext.String.trim(field.getValue()),
            criteria = 0,
            isSapKeyCostCenter,
            isSapKeyCostUnit;
        //field.setValue(value);

        if (panel.customSearch) {
            criteria = parseInt(panel.comboboxAdvancedSearch.getValue());
        }

        switch (criteria) {
            // free text
            case 0:
                if (value.length < 2) {
                    field.markInvalid(Ext.String.format(BC.translate('xMinChars'), 2));
                    return false;
                }
                break;
            // debtor number
            case 1:
                value = value.replace(/^[0]*/g, '');

                if (value.length === 0 || value.length > 6 || !/^\d{1,6}$/.test(value)) {
                    field.markInvalid(BC.translate("invalidDebtorNumber"));
                    return false;
                }
                break;
            // sap key (cost center or cost unit)
            case 2:
                value = value.replace(/[\s\-]*/g, '');

                isSapKeyCostCenter = /(^\d{9}$)|(^\d{12}$)/.test(value);
                isSapKeyCostUnit = /^\d{10,11}$/.test(value);

                if (value.length === 0 ||
                    isSapKeyCostCenter === false && isSapKeyCostUnit === false
                ) {
                    field.markInvalid(BC.translate("invalidSapKey"));
                    return false;
                }

                if (isSapKeyCostCenter) {
                    value = value.replace(/^(\d)(\d{3})(\d{3})(\d{2})(\d{3})?$/, '$1-$2-$3-$4-$5').replace(/\-$/, '');
                } else if (isSapKeyCostUnit) {
                    value = value.replace(/^(\d{5,6})(\d{3})(\d{2})$/, '$1-$2-$3');
                }
                break;
        }

        field.setValue(value);

        return true;
    },

    prepareSearchResultNodeSelection: function (panel) {
        var view = panel.getView(),
            liveSearch = panel.liveSearch,
            cancellationRequested = false,
            messageBox = Ext.Msg.show({
                title: BC.translate('pleaseWait'),
                msg: BC.translate('searchResultsProcessing'),
                buttons: Ext.MessageBox.CANCEL,
                progress: true,
                progressText: '0%',
                fn: function () {
                    cancellationRequested = true;
                }
            });

        view.hide();

        liveSearch.disablePreloader();

        panel.on('searchexpand', function callbackFn (searchPanel, liveSearchObj, node) {
            var index = liveSearchObj.currentIndex,
                total = liveSearchObj.indexes.length,
                progress = (index + 1) / total;

            messageBox.progressBar.updateProgress(progress, Ext.util.Format.number(progress * 100, '0') + '%');

            if (node.get('checked') !== true && node.get('disabled') === false) {
                node.set('checked', true);
            }

            if (index === total - 1 || cancellationRequested === true) {
                searchPanel.un('searchexpand', callbackFn);
                liveSearchObj.enablePreloader();
                view.show();
                messageBox.close();
                return;
            }
            liveSearchObj.next();
        });
    }
});
