Ext.define('BC.controller.Report', {
    extend: 'Ext.app.Controller',

    requires: ['BC.lib.tooltip.ConfigInfo', 'Ext.window.MessageBox'],

    views: [
        'config.ReportConfiguration',
        'report.ReportPanel'
    ],
    stores: [
        'Profiles', 'Translations', 'BookingTypes', 'PrismaEmployeesAll', 'Nodes', 'CostCentres', 'AllCostCentres',
        'report.ListEntries',
        'report.Permissions',
        'report.RebookEntries',
        'report.Table',
        'report.AbsenceCategories',
        'report.AbsenceEmployees'
    ],
    models: [
        'Profile', 'Translation', 'BookingType', 'PrismaEmployeeAll', 'Node', 'CostCentre',
        'report.ListEntry',
        'report.Permission',
        'report.RebookEntry',
        'report.Table',
        'report.TableAxisData',
        'report.AbsenceEntry',
        'report.AbsenceEmployee'
    ],

    refs: [{
        ref: 'mainPanel',
        selector: 'mainPanel'
    }, {
        ref: 'repConf',
        selector: 'reportConfiguration'
    }],

    init: function() {
        this.control({
            'reportPanel': {
                afterrender: function(panel) {
                    // component is not visible yet, therefore not layed out
                    panel.on({
                        'activate': this.initReport,
                        scope: this,
                        single: true
                    });
                }
            },
            'reportSummary button[action=showConfigButton]': {
                click: function() {
                    //activate the config tab
                    var configTab = this.getMainPanel().items.get(1),
                        record = this.getMainPanel().getActiveTab().record;

                    this.getRepConf().profile = record.copy();
                    this.getMainPanel().setActiveTab(configTab);

                    configTab.isOpenedFromProfileList = false;
                    configTab.isOpenedFromEditIcon = false;
                    configTab.getComponent('reportConfiguration').setProfileValues(this.getRepConf().profile, false);
                }
            },
            'diagram': {
                resize: {
                    fn: function(panel) {
                        panel.reloadDiagramImage();
                    },
                    buffer: 1000
                }
            },
            'bookinglist': {
                sortchange: {
                    fn: this.sortList,
                    buffer: 1000
                }
            },
            'bookinglist gridview': {
                refresh: function(view) {
                    this.initJiraEntryTooltips(view);
                }
            },
            'bookinglist button[action=filterList]': {
                click: function() {
                    this.filterList();
                }
            },
            'bookinglist textfield': {
                specialkey: function(field, event) {
                    if (event.keyCode === 13 && field === this.getMainPanel().getActiveTab().report.searchField) {
                        this.filterList();
                    }
                }
            },
            'bookinglist menu': {
                click: this.exportList
            },
            'diagram button[action=btnSwitchLegendPosition]': {
                click: function() {
                    var reportPanel = this.getMainPanel().getActiveTab().report;

                    reportPanel.switchLegendPosition();
                }
            },
            'table button[action=toggleAlHours]': {
                click: function() {
                    var reportTab = this.getMainPanel().getActiveTab(),
                        table = reportTab.report.getEl().down('table.rt-table');

                    table.select('div.hours-al').each(function(flyEl) {
                        var el = flyEl.dom,
                            parent = el.parentNode;

                        flyEl.toggleCls(Ext.baseCSSPrefix + 'hide-display');
                        // using Ext.Element's is way too slow here. instead, change style attributes directly on dom layer
                        parent.style.backgroundColor = flyEl.hasCls(Ext.baseCSSPrefix + 'hide-display') ? '' : el.style.backgroundColor;
                    });
                }
            },
            'table button[action=togglePercent]': {
                click: function() {
                    var reportTab = this.getMainPanel().getActiveTab(),
                        table = reportTab.report.getEl().down('table.rt-table');

                    table.select('tr').each(function(flyEl) {
                        flyEl.down('.percent').toggleCls(Ext.baseCSSPrefix + 'hide-display');
                    });
                }
            },
            'table button[action=toggleTargetHours]': {
                click: function() {
                    var reportTab = this.getMainPanel().getActiveTab(),
                        table = reportTab.report.getEl().down('table.rt-table');

                    table.select('tr').each(function(flyEl) {
                        flyEl.down('.target-effort').toggleCls(Ext.baseCSSPrefix + 'hide-display');
                    });
                }
            },
            'table button[action=toggleSAPKey]': {
                click: function() {
                    var reportTab = this.getMainPanel().getActiveTab(),
                        table = reportTab.report.getEl().down('table.rt-table');

                    table.select('tr').each(function(flyEl) {
                        flyEl.down('.sap-key').toggleCls(Ext.baseCSSPrefix + 'hide-display');
                    });
                }
            },
            'table button[action=downloadExcel]': {
                click: function() {
                    var report = this.getMainPanel().getActiveTab().report;

                    this.download(report, "table", {
                        alhours: report.tbAlbtn.pressed ? 'show' : 'hide',
                        percent: report.tbPercentbtn.pressed ? 'pshow' : 'phide',
                        targetHours: report.tbTargetHoursbtn.pressed ? 'thshow' : 'thhide',
                        sapKey: report.tbSAPKeybtn.pressed ? 'skshow' : 'skhide'
                    });
                }
            },
            'booking button[action=btnExpandAll]': {
                click: function() {
                    this.getMainPanel().getActiveTab().report.expandAll();
                }
            },
            'booking button[action=btnCollapseAll]': {
                click: function() {
                    this.getMainPanel().getActiveTab().report.collapseAll();
                }
            },
            'booking button[action=expandLevelOne]': {
                click: function() {
                    var nodeSelection = this.getMainPanel().getActiveTab().report.getSelectionModel().getSelection();
                    if (nodeSelection.length > 0) {
                        nodeSelection[0].expand();
                    }
                }
            },
            'booking button[action=expandLevelTwo]': {
                click: function() {
                    var levels = 2;
                    var nodeSelection = this.getMainPanel().getActiveTab().report.getSelectionModel().getSelection();
                    if (nodeSelection.length > 0) {
                        this.expandBooking(levels, nodeSelection[0]);
                    }
                }
            },
            'booking button[action=expandLevelThree]': {
                click: function() {
                    var levels = 3;
                    var nodeSelection = this.getMainPanel().getActiveTab().report.getSelectionModel().getSelection();

                    if (nodeSelection.length > 0) {
                        this.expandBooking(levels, nodeSelection[0]);
                    }
                }
            },
            'booking menuitem[action=exportWith]': {
                click: function() {
                    this.download(this.getMainPanel().getActiveTab().report, "bookingsWith");
                }
            },
            'booking *[action=exportWithout]': {
                click: function() {
                    this.download(this.getMainPanel().getActiveTab().report, "bookingsWithout");
                }
            },
            'booking treeview': {
                beforeitemdblclick: function(view, record, item, index, e) {
                    return Ext.get(e.getTarget()).parent('td').hasCls('tree-cell');
                }
            },
            'rebook': {
                reloadReport: function(panel) {
                    this.initReport(panel.ownerCt);
                },
                beforeedit: function(editor, ctx) {
                    var cellEditor = ctx.grid.getView().getHeaderAtIndex(ctx.colIdx).getEditor();
                    cellEditor.addListener('focus', function(field, e, eOpts) {
                        if (/bookingType_.*/.test(field.name)) {
                            this.setValue(eOpts.record.get('hours'));
                            if (Ext.isEmpty(eOpts.record.get('newNote'))) {
                                eOpts.record.set('newNote', eOpts.record.get('note'));
                            }
                        }
                        this.inputEl.dom.select();
                    }, null, {single: true, record: ctx.record});

                    Ext.Array.each(ctx.grid.columns, function(column, index) {
                        if (column.dataIndex && column.dataIndex.match(/bookingType_.*/)) {
                            var cell = ctx.grid.getView().getCellByPosition({row: ctx.rowIdx, column: index});
                            cell.removeCls('grid-cell-invalid');
                            Ext.QuickTips.unregister(cell);
                        }
                    }, this);
                },
                edit: function(editor, ctx) {
                    ctx.record.commit();
                    this.getMainPanel().getActiveTab().report.updateStatusBar();
                }
            },
            'rebook gridview': {
                refresh: function(view) {
                    view.up('reportPanel').report.updateStatusBar();
                    this.initJiraEntryTooltips(view);
                }
            },
            'rebook button[action=performRebooking]': {
                click: function () {
                    var result = this.getMainPanel().getActiveTab().report.validate();

                    if (result.valid) {
                        if (result.warning) {
                            Ext.MessageBox.show({
                                buttons: Ext.MessageBox.YESNO,
                                title: '',
                                msg: BC.translate('diffrentBookingTypeQuestion'),
                                closable: false,
                                animateTarget: this,
                                icon: Ext.MessageBox.WARNING,
                                fn: function(btn) {
                                    if (btn === 'yes') {
                                        this.getMainPanel().getActiveTab().report.rebook();
                                    }
                                },
                                scope: this
                            });
                        } else {
                            this.getMainPanel().getActiveTab().report.rebook();
                        }
                    }
                }
            },
            'rebook button[action=rebookAllLines]': {
                click: function() {
                    this.getMainPanel().getActiveTab().report.setHoursAtSameBookingTypeForAllLines();
                }
            },
            'rebook button[action=sendRebookingRequestMail]': {
                click: this.showRebookingRequestMailDialog
            },
            'monthEnd button[action=downloadExcel]': {
                click: function() {
                    this.download(this.getMainPanel().getActiveTab().report, "monthEnd");
                }
            },
            'button[action=downloadExcelRemoteWorkReport]': {
                click: function() {
                    this.exportListReport(this.getMainPanel().getActiveTab().report, "list-export-excel");
                }
            },
            'BtModifications button[action=downloadExcel]': {
                click: function() {
                    this.download(this.getMainPanel().getActiveTab().report, "btModifications");
                }
            },
            'permission button[action=downloadExcel]': {
                click: function() {
                    var report = this.getMainPanel().getActiveTab().report,
                        params = {
                            sort: report.sort
                        };

                    if (Ext.isEmpty(report.searchedValue) === false) {
                        params.text = report.searchedValue;
                    }

                    this.download(report, "permission", params);
                }
            },
            'sapKeyHours button[action=downloadExcel]': {
                click: function() {
                    this.download(this.getMainPanel().getActiveTab().report, "sapKeyHours");
                }
            },
            'status button[action=downloadExcel]': {
                click: function() {
                    this.download(this.getMainPanel().getActiveTab().report, "status");
                }
            },
            'standby button[action=downloadExcel]': {
                click: function() {
                    this.download(this.getMainPanel().getActiveTab().report, "standby");
                }
            },
            'absencecalendar gridview': {
                // synchronize scroll state between both grids
                bodyscroll: function(e, t) {
                    var views = this.getMainPanel().getActiveTab().query('gridview'),
                        // views must be an array of exactly 2 views
                        syncView = views[0].id === t.id ? views[1] : views[0];

                    syncView.scrollBy(0, t.scrollTop - syncView.el.getScroll().top, false);
                }
            },
            'absencecalendar button[action=downloadExcel]': {
                click: function() {
                    this.download(this.getMainPanel().getActiveTab().report, "absenceCalendar");
                }
            },
            'orderOverview button[action=downloadExcel]': {
                click: function() {
                    this.download(this.getMainPanel().getActiveTab().report, "orderOverview");
                }
            }
        });
    },

    initReport: function(panel) {
        var report = panel.report,
            record = report.record;

        if (!record.get('own')) {
            panel.down('button[action=showConfigButton]').hide();
        }

        report.body.mask('loading...', 'x-mask-report');

        Ext.Ajax.request({
            url: "index/show",
            params: {
                chartprofil: record.get("id"),
                timestamp: panel.timestamp
            },
            scope: this,
            success: function(response) {
                var result = Ext.JSON.decode(response.responseText);

                // update summary
                var summaryData = Ext.apply({
                    timestamp: panel.timestamp,
                    dateFrom: Ext.Date.format(new Date(result.from * 1000), BC.translate('dateFormat')),
                    dateUntil: Ext.Date.format(new Date(result.until * 1000), BC.translate('dateFormat'))
                }, record.data);

                if (result.mitarbeiterinfo.booked !== '') {
                    summaryData.monitoredUsers = result.mitarbeiterinfo.booked;
                }

                panel.summary.summaryPanel.update(summaryData);

                // initialize report with result data
                report.init(result);

                if (result.acthours) {
                    // update profile act hours
                    record.set('act', result.acthours);
                    record.commit();
                    this.calculateActHours(record.get("id"));
                }

                report.body.unmask();
            }
        });
    },

    download: function(report, type, params) {
        report.getEl().mask('loading...', 'x-mask-report');

        var url = "export/generate/profile/" + report.record.get('id') + "/timestamp/" + report.timestamp + "/type/" + type;

        if (params instanceof Object) {
            Ext.Object.each(params, function(key, value) {
                url += "/" + key + "/" + value;
            });
        }

        Ext.Ajax.request({
            url: url,
            callback: function(options, success, response) {
                if (success) {
                    window.location = "export/download/name/" + response.responseText;
                }
                report.getEl().unmask();
            }
        });
    },

    initJiraEntryTooltips: function(view) {
        var onMouseover = function() {
                this.hovered = true;
            },
            onMouseout = function() {
                this.hovered = false;
            };

        Ext.each(view.el.query('a.report-jiraid'), function(a) {
            var el = Ext.get(a);

            // remove all listeners
            el.removeAllListeners();

            el.on({
                'mouseover': onMouseover,
                'mouseout': onMouseout
            });

            el.on({
                'mouseover': function(e) {
                    Ext.Ajax.request({
                        method: 'POST',
                        url: 'index/pdbinfo',
                        params: {
                            jiraid: this.id.split('_')[1]
                        },
                        success: function(response) {
                            var tip = Ext.create('Ext.tip.ToolTip', {
                                target: this,
                                width: 200,
                                dismissDelay: 20000,
                                html: response.responseText
                            });

                            // show tooltip if element is still hovered
                            if (this.hovered) {
                                tip.showAt([e.getX(), e.getY() + 10]);
                            }

                            // cleaning up
                            delete this.hovered;
                            el.un(onMouseover);
                            el.un(onMouseout);
                        },
                        scope: this
                    });
                },
                single: true
            });
        });
    },

    sortList: function(headerCt, column, direction) {
        if (Ext.isEmpty(direction)) {
            return;
        }

        var report = this.getMainPanel().getActiveTab().report;

        Ext.Ajax.request({
            url: "index/sortlist",
            params: {
                profileId: report.record.get('id'),
                column: column.dataIndex,
                timestamp: report.timestamp,
                direction: direction
            }
        });
    },

    // Filter list accoring to the parameters in searchField and column-ComboBox
    filterList: function() {
        var report = this.getMainPanel().getActiveTab().report,
            store = report.getStore(),
            searchVal = report.searchField.getValue(),
            searchRex = new RegExp(Ext.String.escapeRegex(searchVal), 'i'),
            searchCol = report.filterColumnCombo.getValue();

        if (Ext.isEmpty(searchVal)) {
            store.clearFilter();
            return;
        }

        store.clearFilter(true);
        if (searchCol !== 'alle') {
            store.filter(searchCol, searchRex);
        } else {
            store.filter({
                filterFn: function(rec) {
                    return (
                        searchRex.test(rec.data.companyName) ||
                        searchRex.test(rec.data.divisionName) ||
                        searchRex.test(rec.data.employee) ||
                        searchRex.test(rec.data.costcentre) ||
                        searchRex.test(rec.data.note) ||
                        searchRex.test(rec.data.bookingType) ||
                        searchRex.test(Ext.util.Format.number(rec.data.hours, '0.00')) ||
                        searchRex.test(Ext.util.Format.date(rec.data.date, 'd.m.Y'))
                    );
                }
            });
        }
    },

    exportList: function(menu, item) {
        if (item === undefined) {
            return;
        }

        var report = this.getMainPanel().getActiveTab().report,
            itemId = item.itemId;
        this.exportListReport(report, itemId);
    },

    exportListReport: function(report, itemId) {
        var searchVal = report.searchField.getValue(),
            searchCol = report.filterColumnCombo.getValue(),
            type,
            params = {};

        switch (itemId) {
            case 'list-export-excel':
                type = "list";
                params.format = 'excel';
                break;
            case 'list-export-pdbinfo-excel':
                type = "pdbinfosliste";
                params.format = 'excel';
                break;
            default:
                return;
        }

        if (!Ext.isEmpty(searchCol)) {
            params.column = searchCol;
        }
        if (!Ext.isEmpty(searchVal)) {
            params.text = encodeURIComponent(searchVal).replace('/', '%2F').replace('\\', '%5C').replace('\$', '%24').replace('\§', '%A7');
        }

        this.download(report, type, params);
    },

    //Expand booking tree with different levels
    expandBooking: function (level, node) {
        level--;
        if (level !== 0) {
            for (var i = 0; i < node.childNodes.length; ++i) {
                this.expandBooking(level, node.childNodes[i]);
            }
        }
        if (!node.isExpanded()) {
            node.expand();
        }
    },

    showRebookingRequestMailDialog: function () {
        var report = this.getMainPanel().getActiveTab().report,
            dialog;

        dialog = Ext.create('BC.view.report.rebook.RequestMailDialog', {
            report: report
        });
        dialog.show();
    },

    // win the actHours of the given report.
    // Executed in background after the reports show-request has finished
    calculateActHours: function (profileId) {
        Ext.Ajax.request({
            url: "index/calculateacthours",
            params: {
                profileId: profileId
            },
            scope: this,
            success: function() {
                Ext.getStore('Profiles').load();
            }
        });
    }
});
