Ext.define('BC.controller.ProfileList', {
    extend: 'Ext.app.Controller',

    views: ['profile.ProfileList', 'MainPanel'],

    stores: ['Profiles', 'Translations', 'Nodes', 'Employees', 'BookingTypes'],

    models: ['Profile', 'Translation', 'Node', 'Employee', 'BookingType'],

    refs: [{
        ref: 'profileList',
        selector: 'profileList'
    }, {
        ref: 'mainPanel',
        selector: 'mainPanel'
    }],

    requires: ['BC.view.report.ReportPanel', 'BC.lib.tooltip.ConfigInfo', 'Ext.window.MessageBox'],

    init: function() {
        this.control({
            'profileList gridview': {
                itemdblclick: function(view, record) {
                    record.set('hint', record.get('name'));
                    this.showReport(record);
                },
                clickConfigInfoIcon: function(view, rowIndex, position) {
                    var record = view.getRecord(view.getNode(rowIndex));
                    this.showConfigInfo(record, position);
                },
                clickShowIcon: function(view, rowIndex) {
                    var record = view.getRecord(view.getNode(rowIndex));
                    record.set('hint', record.get('name'));
                    this.showReport(record);
                },
                clickEditIcon: function(view, rowIndex) {
                    //activate the config tab
                    var configTab = this.getMainPanel().items.get(1);
                    var record = view.getRecord(view.getNode(rowIndex));
                    record.set('hint', record.get('name'));
                    configTab.isOpenedFromEditIcon = true;
                    // Store a clone of the record in the 'ReportConfiguration'
                    configTab.getComponent('reportConfiguration').setProfileValues(record.copy(), false);
                    configTab.isOpenedFromProfileList = true;
                    this.getMainPanel().setActiveTab(configTab);
                },
                clickDeleteIcon: function(view, rowIndex) {
                    var record = view.getRecord(view.getNode(rowIndex));

                    var box = Ext.MessageBox.show({
                        title: BC.translate('delete'),
                        msg: BC.translate('profilloeschen').replace(/@@@/, record.get('name')),
                        buttons: Ext.MessageBox.OKCANCEL,
                        fn: function(btn) {
                            if (btn === 'ok') {
                                Ext.Ajax.request({
                                    url: "index/deletechartprofile",
                                    params: {
                                        chartprofil: record.get('id')
                                    },
                                    success: function(response) {
                                        if (response && response.responseText) {
                                            var result = Ext.JSON.decode(response.responseText);
                                            if (result && result.result && result.result === true) {
                                                // Aktualisieren der Chart-Profil-Liste
                                                view.getStore().load();
                                            } else {
                                                Ext.MessageBox.alert(BC.translate('undefinederror_subject'), BC.translate('allgemeinerfehler'));
                                            }
                                        }
                                    },
                                    failure: function() {
                                        Ext.MessageBox.alert(BC.translate('undefinederror_subject'), BC.translate('allgemeinerfehler'));
                                    }
                                });
                            }
                        },
                        animateTarget: 'bcMainPanel',
                        icon: Ext.MessageBox.WARNING
                    });

                    // Nasty hack: Change the button text
                    Ext.Array.each(box.msgButtons, function(button) {
                        if (button.text === 'Ok') {
                            button.setText(BC.translate('ok'));
                        }
                        if (button.text === 'Cancel') {
                            button.setText(BC.translate('abbrechen'));
                        }
                    });
                }
            }
        });
    },

    onLaunch: function() {
        this.getProfilesStore().on({
            load: function() {
                this.sort('name', 'asc');
            },
            single: true
        });
    },

    showConfigInfo: function(record, position) {
        Ext.create('BC.lib.tooltip.ConfigInfo').show(record, position);
    },

    showReport: function(profileRecord) {
        // Check if the report's from date is before 01.01.2012
        Ext.Ajax.request({
            url: "index/determineperiod",
            params: {
                profileId: profileRecord.get('id')
            },
            scope: this,
            success: function(response) {
                var from = response.responseText.split('-')[0],
                    year2012 = new Date(2012, 0, 1);
                // Show warning
                if (from * 1000 < year2012.getTime() && /^[PF]$/.test(profileRecord.get('type')) === false) {
                    Ext.MessageBox.alert(BC.translate('undefinederror_subject'), BC.translate('frombefore2012'));
                } else {
                    this.validateReport(profileRecord, function(record, scope, result) {
                        var date = new Date();

                        switch (record.get('type')) {
                            // accounting report
                            case 'A':
                                Ext.MessageBox.show({
                                    buttons: Ext.Msg.OK,
                                    title: ' ',
                                    width: 500,
                                    msg: result.success === true ? Ext.String.format(BC.translate('accountingPopupMessage'), global.accountingPath) : BC.translate('accountingPopupMessageEmptyReport'),
                                    animateTarget: 'bcMainPanel',
                                    icon: Ext.MessageBox.INFO
                                });
                                break;
                            // direct download reports
                            case 'F':
                            case 'I':
                            case 'J':
                            case 'Q':
                            case 'Y':
                                window.location = 'index/showdirect/type/' + record.get('type') + '/timestamp/' + result.timestamp;
                                break;
                            // default tab behavior
                            default:
                                var titlestring = record.get('name') + " (" + Ext.Date.format(date, 'H:i') + ")",
                                    report = new BC.view.report.ReportPanel(titlestring, record.get('type') + "Icon", record);

                                scope.getMainPanel().add(report);

                                //activate the appropriate report-tab
                                scope.getMainPanel().setActiveTab(report);
                                break;
                        }
                    });
                }
            }
        });
    },

    validateReport: function(record, callbackFn) {
        Ext.getCmp('bcMainPanel').el.mask();
        Ext.Ajax.request({
            url: "index/validate",
            params: {
                chartprofil: record.get("id")
            },
            scope: this,
            callback: function (options, success, response) {
                Ext.getCmp('bcMainPanel').el.unmask();

                var result = Ext.JSON.decode(response.responseText);

                if (result.success) {
                    if (/^[AFIJQY]$/.test(record.get('type')) === true) {
                        if (result.hint) {
                            Ext.MessageBox.show({
                                title: ' ',
                                msg: BC.translate('btOrderDisplayMode'),
                                buttons: Ext.Msg.YESNO,
                                icon: Ext.MessageBox.QUESTION,
                                animateTarget: 'bcMainPanel',
                                fn: function(btnId) {
                                    if (btnId === 'yes') {
                                        this.directReport(record, callbackFn, result, btnId);
                                    }
                                },
                                scope: this
                            });
                        } else {
                            this.directReport(record, callbackFn, result, 'yes');
                        }
                    } else {
                        callbackFn.apply(this, [record, this, result]);
                    }
                } else {
                    Ext.MessageBox.show({
                        buttons: Ext.Msg.OK,
                        title: ' ',
                        msg: result.message,
                        animateTarget: 'bcMainPanel',
                        icon: Ext.MessageBox.WARNING
                    });
                }
            }
        });
    },

    directReport: function(record, callbackFn, args, btn, loadMask) {
        Ext.getCmp(loadMask || 'bcMainPanel').el.mask('...');
        Ext.Ajax.request({
            url: "index/show",
            params: {
                chartprofil: record.get("id")
            },
            scope: this,
            callback: function (options, success, response) {
                Ext.getCmp(loadMask || 'bcMainPanel').el.unmask();

                var result = Ext.JSON.decode(response.responseText);
                callbackFn.apply(this, [record, this, Ext.Object.merge(args, result), btn]);
            }
        });
    }
});
