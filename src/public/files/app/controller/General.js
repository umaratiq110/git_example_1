Ext.define('BC.controller.General', {
    extend: 'Ext.app.Controller',

    views: ['Header', 'MainPanel', 'report.ReportPanel', 'SwitchUserDialog'],
    stores: ['Nodes', 'Employees', 'EmployeesAll', 'Profiles', 'AbleToShareEmployees'],

    refs: [{
        ref: 'mainPanel',
        selector: 'mainPanel'
    }],

    init: function() {
        this.control({
            '#changeLangGerman': {
                click: function() {
                    window.location = 'login/language/language/german';
                }
            },
            '#changeLangEnglish': {
                click: function() {
                    window.location = 'login/language/language/english';
                }
            },
            'bcHeader button[action=showInfo]': {
                click: function() {
                    window.open('infoseite/information', 'Fenstername');
                }
            },
            'bcHeader button[action=showHelp]': {
                click: function() {
                    window.open('index/help', 'Help');
                }
            },
            'bcHeader button[action=logOut]': {
                click: function() {
                    window.location = 'login/logout';
                }
            },
            'mainPanel': {
                savedProfiles: function() {
                    this.getMainPanel().setActiveTab(this.getMainPanel().items.get(0));
                },
                configuration: function() {
                    var configPanel = this.getMainPanel().items.get(1);
                    configPanel.isOpenedFromProfileList = false;
                    this.getMainPanel().setActiveTab(configPanel);
                },
                beforetabchange: function(panel, newTab, oldTab) {
                    // TODO: revalidate with library update (buggy as of 4.1.3)
                    // save scroll position
                    if (Ext.isIE && oldTab) {
                        var view = oldTab.down('reportConfiguration') || oldTab.down('dataview'),
                            el = view ? view.body || view.el : null;
                        if (el) {
                            oldTab.scrollPosition = el.getScroll();
                        }
                    }
                    var lastActiveItem = this.getMainPanel().lastActiveItem;

                    if (lastActiveItem.activate) {
                        lastActiveItem.activate = false;
                        panel.setActiveTab(lastActiveItem.index);
                        return false;
                    }
                },
                tabchange: function(panel, newTab, oldTab) {
                    //TODO: revalidate with library update (buggy as of 4.1.3)
                    // restore scroll position
                    if (Ext.isIE) {
                        var view = newTab.down('reportConfiguration') || newTab.down('dataview'),
                            el = view ? view.body || view.el : null;
                        if (el) {
                            el.scrollBy(newTab.scrollPosition.left, newTab.scrollPosition.top, false);
                        }
                    }

                    if (newTab instanceof BC.view.report.ReportPanel &&
                        newTab.items.items[1] && Ext.isFunction(newTab.items.items[1].resizePanel)
                    ) {
                        newTab.items.items[1].resizePanel(newTab.items.items[1].getSize());
                    }

                    var activeCls = 'x-active x-tab-active x-tab-default-active x-top-active x-tab-top-active x-tab-default-top-active',
                        lastActiveItem = this.getMainPanel().lastActiveItem;

                    // remove active class on old "fake" tab
                    if (oldTab instanceof BC.view.profile.ProfileList) {
                        panel.tabProfiles.removeCls(activeCls);
                    } else if (oldTab instanceof BC.view.config.ConfigPanel) {
                        panel.tabConfig.removeCls(activeCls);
                    }

                    // add active class on new "fake" tab
                    if (newTab instanceof BC.view.profile.ProfileList) {
                        lastActiveItem.index = 0;
                        panel.tabProfiles.addCls(activeCls);
                    } else if (newTab instanceof BC.view.config.ConfigPanel) {
                        lastActiveItem.index = 1;
                        panel.tabConfig.addCls(activeCls);
                    }
                }
            },
            'mainPanel tab': {
                beforeclose: function(tab) {
                    if (tab.active) {
                        this.getMainPanel().lastActiveItem.activate = true;
                    }
                }
            },
            'profileList button[action=newProfile]': {
                click: function() {
                    var mainPanel = this.getMainPanel(),
                        configPanel = mainPanel.items.get(1),
                        reportConfiguration = configPanel.getComponent('reportConfiguration');

                    reportConfiguration.newProfileButton = true;

                    mainPanel.setActiveTab(configPanel);

                    reportConfiguration.setProfileValues(Ext.getStore('Profiles').defaultProfile, true);
                    reportConfiguration.newProfileButton = false;
                }
            },
            'bcHeader button[action=newPrivilege]': {
                click: function() {
                    window.location.href = 'mailto:' + global.bcPrivilegeMail + '?subject=' + BC.translate('newPrivilegeSubject');
                }
            },
            'switchUserDialog button[action=switchUser]': {
                click: function() {
                    var shortcut = Ext.getCmp('switchUserDialog').down('[name=user]').getValue();
                    if (shortcut) {
                        window.location = 'index/switchtouser?user=' + shortcut;
                    }
                }
            }
        });
    },

    onLaunch: function() {
        var fn = this.onConfigStoreLoad;
        // define stores which are required before enabling the config tab
        Ext.apply(fn, {
            storesRequired: [
                this.getProfilesStore()
            ],
            storesLoaded: []
        });

        // add listener on those stores
        Ext.each(fn.storesRequired, function(store) {
            store.on('load', fn, this);
        }, this);

        if (global.prevUser.name) {
            Ext.get('bcHeader').down('a[data-log=back]').on({
                click: function(e) {
                    window.location = 'index/switchback';
                    e.preventDefault();
                }
            });
        } else if (global.privileges.adminRights === true) {
            Ext.get('bcHeader').down('a[data-log=in]').on({
                click: function(e) {
                    Ext.create('BC.view.SwitchUserDialog').show();
                    e.preventDefault();
                }
            });
        }
    },

    onConfigStoreLoad: function(store) {
        var me = this.onConfigStoreLoad;

        me.storesLoaded.push(store);

        // check if all stores were loaded
        if (me.storesRequired.length === me.storesLoaded.length) {
            var tabConfig = this.getMainPanel().tabConfig;

            // enable config tab
            tabConfig.enable();
            tabConfig.removeCls('x-item-disabled x-disabled x-tab-disabled x-tab-default-disabled x-top-disabled x-tab-top-disabled x-tab-default-top-disabled');

            // remove store listeners
            Ext.each(me.storesLoaded, function(requiredConfigStore) {
                requiredConfigStore.un('load', me, this);
            }, this);
        }
    }
});
