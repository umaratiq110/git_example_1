Ext.define('BC.view.SwitchUserDialog', {
    extend: 'Ext.window.Window',
    alias: 'widget.switchUserDialog',

    initComponent: function() {
        this.id = 'switchUserDialog';

        this.comboBoxEmployees = new Ext.form.ComboBox({
            name: 'user',
            fieldLabel: BC.translate('benutzer'),
            labelAlign: 'top',
            width: 200,
            store: new Ext.data.Store({
                data: global.bcUsers,
                fields: [
                    {name: 'shortcut', mapping: 'SHORTCUT'},
                    {name: 'name', mapping: 'NAME'}
                ]
            }),
            triggerAction: 'all',
            displayField: 'name',
            valueField: 'shortcut',
            queryMode: 'local',
            anyMatch: true
        });

        Ext.apply(this, {
            title: BC.translate('switchUser'),
            width: 300,
            height: 120,
            closable: false,
            resizable: false,
            modal: true,
            items: [{
                xtype: 'form',
                baseCls: 'x-plain',
                labelWidth: 95,
                bodyStyle: 'background-color:#DFE8F6; padding:5px 10px 0',
                border: false,
                items: [
                    this.comboBoxEmployees
                ]
            }],
            buttons: [
                {
                    text: BC.translate('switchUser'),
                    action: 'switchUser'
                },
                {
                    text: BC.translate('abbrechen'),
                    handler: function() {
                        this.close();
                    },
                    scope: this
                }
            ]
        });

        this.callParent();
    }
});
