Ext.define('BC.view.profile.ProfileList', {
    extend: 'Ext.grid.Panel',

    alias: 'widget.profileList',

    cls: 'profileList',

    padding: '5 15 15 15',
    columnLines: true,
    border: false,
    disableSelection: true,
    // TODO: revalidate with library update (buggy as of 4.1.3)
    // used for storing scroll position in IE
    scrollPosition: {left: 0, top: 0},

    store: 'Profiles',

    requires: ['Ext.grid.column.Action'],

    initComponent: function() {
        this.columns = [{
            header: BC.translate('name'),
            dataIndex: 'name',
            flex: 1,
            renderer: function(value, params, record) {
            if (record.get('own') === true) {
                return '<b>' + value + '</b>';
            } else if (record.get('shareWithUsers').length > 0) {
                return '<img src="files/images/page_white_edit.png" class="sharedProfileIcon" /><b>' + value + '</b>';
            }
                return '<img src="files/images/sharedProfile.png" class="sharedProfileIcon" /><b>' + value + '</b>';
            }
        }, {
            header: BC.translate('reportType'),
            dataIndex: 'type',
            flex: 1,
            renderer: function(value) {
                switch (value) {
                    case 'D':
                        value = '<img src="files/images/diagram.png"> ' + BC.translate('diagramm');
                        break;
                    case 'L':
                        value = '<img src="files/images/list.png"> ' + BC.translate('liste');
                        break;
                    case 'T':
                        value = '<img src="files/images/table.png"> ' + BC.translate('tabelle');
                        break;
                    case 'E':
                        value = '<img src="files/images/employeeBooking.png"> ' + BC.translate('notBooked');
                        break;
                    case 'B':
                        value = '<img src="files/images/tree.png"> ' + BC.translate('bookingReport');
                        break;
                    case 'A':
                        value = '<img src="files/images/page_white_acrobat.png"> ' + BC.translate('accountingReport');
                        break;
                    case 'R':
                        value = '<img src="files/images/rebook.png"> ' + BC.translate('rebookReport');
                        break;
                    case 'M':
                        value = '<img src="files/images/monthEnd.png"> ' + BC.translate('monthEndReport');
                        break;
                    case 'N':
                        value = '<img src="files/images/btModifications.png"> ' + BC.translate('btModifications');
                        break;
                    case 'S':
                        value = '<img src="files/images/sap.png"> ' + BC.translate('sapKeyHours');
                        break;
                    case 'P':
                        value = '<img src="files/images/question_mark.png"> ' + BC.translate('permissionReport');
                        break;
                    case 'O':
                        value = '<img src="files/images/rebookLog.png"> ' + BC.translate('rebookLog');
                        break;
                    case 'F':
                        value = '<img src="files/images/tree_edit.png"> ' + BC.translate('btOrder');
                        break;
                    case 'H':
                        value = '<img src="files/images/power_on.png"> ' + BC.translate('standby');
                        break;
                    case 'C':
                        value = '<img src="files/images/clock.png"> ' + BC.translate('absenceCalendar');
                        break;
                    case 'I':
                        value = '<img src="files/images/line_graph-y.png"> ' + BC.translate('misDivision');
                        break;
                    case 'J':
                        value = '<img src="files/images/line_graph-b.png"> ' + BC.translate('misOverall');
                        break;
                    case 'K':
                        value = '<img src="files/images/zoom.png"> ' + BC.translate('projectStatus');
                        break;
                    case 'G':
                        value = '<img src="files/images/zoom.png"> ' + BC.translate('accountableHours');
                        break;
                    case 'V':
                        value = '<img src="files/images/page_white_stack.png"> ' + BC.translate('orderOverview');
                        break;
                    case 'Q':
                        value = '<img src="files/images/arrow.png"> ' + BC.translate('contingentRange');
                        break;
                    case 'Z':
                        value = '<img src="files/images/arrow.png"> ' + BC.translate('contingentData');
                        break;
                    case 'X':
                        value = '<img src="files/images/euro.png"> ' + BC.translate('contingentSapInvoiceComparison');
                        break;
                    case 'Y':
                        value = '<img src="files/images/table.png"> ' + BC.translate('prismaValues');
                        break;
                }
                return value;
            }
        }, {
            header: BC.translate('zeitraumal'),
            flex: 1,
            dataIndex: 'displayPeriod',
            renderer: function(val, metaData, record) {
                var from = record.get('from'),
                    to = record.get('to'),
                    dynamicPeriod = record.get('dynamicPeriod');

                // Reports with no period value
                if (/^[PFQ]$/.test(record.get('type')) === true) {
                    return '---';
                }
                if (!to && dynamicPeriod === 0) {
                    return Ext.Date.format(from, 'd.M.Y') + ' - ' + BC.translate('today');
                }
                if (from && to) {
                    return Ext.Date.format(from, 'd.M.Y') + ' - ' + Ext.Date.format(to, 'd.M.Y');
                }

                switch (dynamicPeriod) {
                    case 1: return BC.translate('currMonth');
                    case 2: return BC.translate('lastMonthh');
                    case 3: return BC.translate('currrentWeek');
                    case 4: return BC.translate('lastWeek');
                    case 5: return BC.translate('currentDay');
                    case 6: return BC.translate('lastDay');
                    case 7: return BC.translate('currentQuarter');
                    case 8: return BC.translate('lastQuarter');
                    case 9: return BC.translate('currentYear');
                    case 10: return BC.translate('lastYear');
                    case 11: return BC.translate('all');
                    case 12: return BC.translate('next6months');
                    case 13: return BC.translate('next12months');
                    default: return val;
                }
            }
        }, {
            header: BC.translate('abkist'),
            dataIndex: 'act',
            width: 75,
            renderer: function(value, params, record) {
                // Reports with no act value
                if (/^[MAENSPOFCIJQ]$/.test(record.get('type')) === true) {
                    return '';
                }

                if (record.get('limitexceeded') === true && /^[DLT]$/.test(record.get('type')) === true) {
                    params.tdAttr = 'style= "color:red;"';
                } else {
                    params.tdAttr = 'style= "color:green;"';
                }
                return value >= 0 ? value : '';
            }
        }, {
            header: BC.translate('limit'),
            dataIndex: 'limit',
            width: 75,
            renderer: function(value) {
                // If the limit in db is 'null'
                // the server gives us 0
                if (value === 0) {
                    return '';
                }

                return value;
            }
        }, {
            header: BC.translate('actHourMail'),
            dataIndex: 'mail',
            flex: 1,
            renderer: function(value) {
                switch (value) {
                    case '0':
                        value = '---';
                        break;
                    case '1':
                        value = BC.translate('taeglich');
                        break;
                    case '2':
                        value = BC.translate('exceeding');
                        break;
                }
                return value;
            }
        }, {
            header: BC.translate('reportmail'),
            dataIndex: 'reportMail',
            flex: 1,
            renderer: function(value) {
                switch (value) {
                    case '0':
                        value = '---';
                        break;
                    case '1':
                        value = BC.translate('taeglich');
                        break;
                    case '2':
                        value = BC.translate('weekly');
                        break;
                    case '3':
                        value = BC.translate('monthly');
                        break;
                }
                return value;
            }
        }, {
            header: BC.translate('otherUsers'),
            dataIndex: 'showToUsers',
            renderer: function(value, meta, record) {
                var text = '---',
                    employees = [],
                    bcEmployeesStore = Ext.data.StoreManager.lookup('EmployeesAll'),
                    ableToShareEmployeesStore = Ext.data.StoreManager.lookup('AbleToShareEmployees'),
                    shareWithUsers = record.data.shareWithUsers;

                if (value.length > 0 && record.get('own') === true) {
                    for (var i = 0, len = value.length; i < len; i++) {
                        var idx = bcEmployeesStore.findExact('ID', value[i]);
                        if (idx !== -1) {
                            employees.push(bcEmployeesStore.getAt(idx).get('SHORTCUT'));
                        }
                    }
                    employees.sort();
                    text = employees.join(', ');
                }

                if (shareWithUsers.length > 0) {
                    for (var i = 0, len = shareWithUsers.length; i < len; i++) {
                        var idx = ableToShareEmployeesStore.findExact('ID', shareWithUsers[i]);
                        if (idx !== -1) {
                            employees.push(ableToShareEmployeesStore.getAt(idx).get('SHORTCUT') + ' (edit)');
                        }
                    }
                    employees.sort();
                    text = employees.join(', ');
                }

                return text;
            },
            flex: 1
        }, {
            header: BC.translate('actions'),
            xtype: 'actioncolumn',
            items: [{
                icon: 'files/images/configInfo.png',
                tooltip: BC.translate('details'),
                handler: function(view, rowIndex, colIndex, item, e) {
                    view.fireEvent('clickConfigInfoIcon', view, rowIndex, e.getXY());
                }
            }, {
                icon: 'files/images/show.png',
                tooltip: BC.translate('anzeigen'),
                handler: function(view, rowIndex) {
                    view.fireEvent('clickShowIcon', view, rowIndex);
                }
            }, {
                icon: 'files/images/edit.png',
                tooltip: BC.translate('configure'),
                handler: function(view, rowIndex) {
                    view.fireEvent('clickEditIcon', view, rowIndex);
                },
                getClass: function(v, meta, record) {
                    if (record.get('own') === false && record.get('sharedProfile') === false) {
                        return 'x-hide-visibility';
                    }
                }
            }, {
                icon: 'files/images/delete.gif',
                tooltip: BC.translate('delete'),
                handler: function(view, rowIndex) {
                    view.fireEvent('clickDeleteIcon', view, rowIndex);
                },
                getClass: function(v, meta, record) {
                    if (record.get('own') === false) {
                        return 'x-hide-visibility';
                    }
                }
            }],
            width: 150,
            align: 'center'
        }];

        this.tbar = [{
            xtype: 'button', text: BC.translate('newProfile'),
            icon: 'files/images/newProfile.png',
            action: 'newProfile'
        }];

        this.callParent(arguments);
    }
});
