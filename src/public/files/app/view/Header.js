Ext.define('BC.view.Header', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.bcHeader',

    requires: ['Ext.button.Split', 'Ext.layout.container.Table'],
    uses: [
        'Ext.Img',
        'Ext.toolbar.Toolbar', 'Ext.toolbar.Separator', 'Ext.toolbar.Spacer',
        'Ext.menu.Menu'
    ],

    bodyCls: 'bcHeader',

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    initComponent: function() {
        this.id = 'bcHeader';

        var isoLogo = Ext.create('Ext.Img', {
            src: 'files/images/ISO_Gruppe_logo.png',
            height: 40,
            width: 177
        });

        var loggedInHtml;

        loggedInHtml = "<span style='position:relative; top:-1px; color: #333333;'>" + BC.translate('loggedInAs') + ": " + global.user + "</span>";

        if (global.prevUser.name) {
            loggedInHtml += "<br/> <a style='color:red' data-log='back' href='#'>" + BC.translate('switchBack') + "</a>";
        } else if (global.privileges.adminRights === true) {
            loggedInHtml += "<br/> <a data-log='in' href='#'>" + BC.translate('logInAsAnotherUser') + "</a>";
        }

        var toolbar = Ext.create('Ext.toolbar.Toolbar', {
            height: 40,
            id: 'bcHeaderToolbar',
            items: [{
                text: BC.translate('newPrivilege'),
                icon: 'files/images/email_add.png',
                action: 'newPrivilege',
                margin: '0 0 0 15'
            }, {
                xtype: 'tbseparator'
            }, {
                text: BC.translate('informationen'),
                icon: 'files/images/info.png',
                action: 'showInfo'
            }, {
                xtype: 'tbseparator'
            }, {
                text: BC.translate('sprache'),
                iconCls: Ext.select('html').first().getAttribute('lang') + 'Icon',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    items: [{
                        id: 'changeLangGerman',
                        text: BC.translate('deutsch'),
                        iconCls: 'deIcon'
                    }, {
                        id: 'changeLangEnglish',
                        text: BC.translate('englisch'),
                        iconCls: 'enIcon'
                    }]
                }
            }, {
                xtype: 'tbseparator'
            }, {
                text: BC.translate('hilfe'),
                icon: 'files/images/help.png',
                action: 'showHelp'
            }, {
                xtype: 'tbseparator'
            }, {
                html: loggedInHtml,
                xtype: 'label'
            }, {
                xtype: 'tbseparator'
            }, {
                text: BC.translate('logout'),
                icon: 'files/images/logout.png',
                action: 'logOut',
                margin: '0 15 0 0'
            }]
        });

        this.items = [isoLogo, {
            html: '<div id="headline">BKZ-Cockpit</div>',
            id: 'bcTitle',
            bodyCls: 'bcHeader',
            height: 40,
            width: 350,
            border: false
        }];

        this.dockedItems = [{
            dock: 'right',
            items: [toolbar],
            bodyCls: 'bcHeader'
        }];

        this.callParent(arguments);
    }
});
