Ext.define('BC.view.config.TreePanel', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.configTreePanel',

    requires: ['BC.lib.tree.NodeDisabled'],
    uses: [
        'Ext.form.field.Trigger', 'Ext.tree.Panel', 'Ext.grid.column.Template'
    ],

    border: true,
    layout: 'fit',
    id: null,
    store: null,
    columns: null,
    customSearch: false,

    initComponent: function() {
        this.setsNode = null;

        this.searchField = Ext.create('Ext.form.field.Trigger', {
            id: this.id + 'SearchField',
            width: 200,
            maxLength: 100,
            emptyText: BC.translate('emptyFilterText'),
            trigger1Cls: 'x-form-clear-trigger',
            trigger2Cls: 'x-form-useless-trigger',
            trigger3Cls: 'x-form-search-trigger',
            onTrigger1Click: function() {
                this.fireEvent('trigger1click', this);
            },
            onTrigger2Click: function() {
                this.fireEvent('trigger3click', this);
            },
            onTrigger3Click: function() {
                this.fireEvent('trigger2click', this);
            },
            listeners: {
                render: function() {
                    Ext.tip.QuickTipManager.register({
                        target: this.triggerEl.item(0).up('td'),
                        text: BC.translate('resetUc')
                    });
                    Ext.tip.QuickTipManager.register({
                        target: this.triggerEl.item(1).up('td'),
                        text: BC.translate('searchAndSelect')
                    });
                    Ext.tip.QuickTipManager.register({
                        target: this.triggerEl.item(2).up('td'),
                        text: BC.translate('suchen')
                    });
                }
            }
        });

        this.btnPrev = Ext.create('Ext.button.Button', {
            icon: 'files/images/control_start_blue.png',
            action: 'previous',
            disabled: true
        });

        this.btnNext = Ext.create('Ext.button.Button', {
            icon: 'files/images/control_end_blue.png',
            action: 'next',
            disabled: true
        });

        this.statusField = Ext.create('Ext.toolbar.TextItem', {
            width: 100,
            text: ''
        });

        Ext.apply(this, {
            id: this.id + 'Tree',
            border: false,
            store: this.store,
            viewConfig: {
                animate: false,
                preserveScrollOnRefresh: true,
                getRowClass: function(record) {
                    if (record.get('locked')) {
                        return 'locked-item';
                    }
                }
            },
            autoScroll: true,
            rootVisible: false,
            collapsible: false,
            collapseFirst: false,
            tbar: Ext.Array.merge(
                [
                    this.searchField,
                    this.btnPrev,
                    this.btnNext,
                    this.statusField
                ],
                this.customSearch ? ['->', this.customCriteria] : []
            ),
            columns: this.columns
        });

        if (!Ext.isArray(this.plugins)) {
            this.plugins = [];
        }
        this.plugins.push({
            ptype: 'nodedisabled',
            preventSelection: false
        });

        this.callParent();
    },

    initEvents: function() {
        this.searchField.addEvents({
            trigger1click: true,
            trigger2click: true,
            trigger3click: true
        });

        this.callParent();
    },

    hideSets: function() {
        this.setsNode = this.getRootNode().firstChild.nextSibling;
        this.getRootNode().firstChild.nextSibling.remove();
    },

    showSets: function() {
        if (this.setsNode != null) {
            this.getRootNode().appendChild(this.setsNode);
        }
    }
});
