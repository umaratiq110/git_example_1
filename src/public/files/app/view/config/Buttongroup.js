Ext.define('BC.view.config.Buttongroup', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.buttonGroup',
    uses: [
        'Ext.layout.container.Column'
    ],

    height: 30,
    cls: 'configButtongroup',
    border: false,

    layout: 'column',

    bodyStyle: {
        background: '#dfe8f7'
    },

    initComponent: function() {

        this.items = [{
            xtype: 'button',
            id: 'saveProfileButton',
            margin: '5 5 0 5',
            text: BC.translate('saveProfile'),
            icon: 'files/images/save.png',
            width: 128,
            hideMode: 'visibility',
            action: 'saveProfile'
        }, {
            xtype: 'tbseparator'
        }, {
            xtype: 'button',
            id: 'clearProfileButton',
            margin: '5 5 0 0',
            width: 128,
            text: BC.translate('clearConfig'),
            icon: 'files/images/clear.png',
            hideMode: 'visibility',
            action: 'clearFields'
        }, {
            xtype: 'tbseparator'
        }, {
            // text and icon will be set dynamically
            cls: 'special-btn',
            xtype: 'button',
            id: 'showProfileButton',
            width: 128,
            margin: '5 5 0 0',
            action: 'showProfile'
        }];

        this.callParent(arguments);
    }

});
