Ext.define('BC.view.config.reportConfiguration.Utils', {
    statics: {
        setNonRemoteWorkConfigFieldsDisabled: function (disabled) {
            var nonRemoteWorkElementIds = [
                'configNodes',
                'configHrsNote',
                'configLimit',
                'addCol17', // begin/end
                'addCol18', // from/till
                'addCol22', // break
                'listOptionOnlyHourBookingsWithBeginEnd'
            ];

            for (var arrayIndex = 0; arrayIndex < nonRemoteWorkElementIds.length; arrayIndex++) {
                var nonRemoteWorkElement = Ext.getCmp(nonRemoteWorkElementIds[arrayIndex]);

                if (nonRemoteWorkElement !== undefined) {
                    nonRemoteWorkElement.setDisabled(disabled);
                }
            }
        }
    }
});

Ext.define('BC.view.config.ReportConfiguration', {
    extend: 'Ext.panel.Panel',

    alias: 'widget.reportConfiguration',

    uses: [
        'BC.view.config.TreePanel',
        'BC.view.config.SelectedEmployeesDialog',
        'BC.view.config.SelectedNodesDialog',
        'Ext.form.Label',
        'Ext.form.field.Date',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Radio',
        'Ext.form.RadioGroup'
    ],

    id: 'reportConfiguration',
    border: false,
    autoScroll: true,

    profile: null,
    configPanel: null,
    newProfileButton: false,
    misDivisionType: 'I',

    initComponent: function() {
        Ext.create('BC.view.config.SelectedEmployeesDialog');
        Ext.create('BC.view.config.SelectedNodesDialog');

        //Store for the dynamic period combobox
        this.dynamicPeriodStore = Ext.create('Ext.data.Store', {
            fields: ['name', 'key'],
            data: [{
                "key": 0,
                "name": BC.translate('specialPeriod')
            }, {
                "key": 5,
                "name": BC.translate('currentDay')
            }, {
                "key": 6,
                "name": BC.translate('lastDay')
            }, {
                "key": 3,
                "name": BC.translate('currrentWeek')
            }, {
                "key": 4,
                "name": BC.translate('lastWeek')
            }, {
                "key": 1,
                "name": BC.translate('currMonth')
            }, {
                "key": 2,
                "name": BC.translate('lastMonthh')
            }, {
                "key": 7,
                "name": BC.translate('currentQuarter')
            }, {
                "key": 8,
                "name": BC.translate('lastQuarter')
            }, {
                "key": 9,
                "name": BC.translate('currentYear')
            }, {
                "key": 10,
                "name": BC.translate('lastYear')
            }, {
                "key": 11,
                "name": BC.translate('all')
            }, {
                "key": 12,
                "name": BC.translate('next6months')
            }, {
                "key": 13,
                "name": BC.translate('next12months')
            }]
        });

        var dateRange = {
            id: 'configDateRange',
            xtype: 'fieldset',
            title: BC.translate('zeitraumal'),
            layout: 'hbox',
            width: 790,
            margin: '5 0 5 10',
            padding: 10,
            defaultType: 'datefield',
            items: [{
                xtype: 'combo',
                fieldLabel: '',
                width: 260,
                labelWidth: 130,
                margin: '0 25 0 0',
                id: 'dynamicPeriod',
                name: 'dynamicPeriod',
                store: this.dynamicPeriodStore,
                queryMode: 'local',
                displayField: 'name',
                valueField: 'key',
                editable: false
            }, {
                fieldLabel: BC.translate('from'),
                labelWidth: 30,
                id: 'dateRangeFrom',
                name: 'from',
                margin: '0 20 0 0',
                format: BC.translate('dateFormat'),
                altFormats: BC.translate('dateFormatAlt'),
                invalidText: BC.translate('zeitraumungueltig')
            }, {
                fieldLabel: BC.translate('until'),
                labelWidth: 30,
                id: 'dateRangeUntil',
                name: 'until',
                format: BC.translate('dateFormat'),
                altFormats: BC.translate('dateFormatAlt'),
                invalidText: BC.translate('zeitraumungueltig')
            }]
        };

        Ext.create('Ext.data.Store', {
            id: 'BC.ReportConfiguration.SelectedNodesStore',
            fields: [
                'text',
                {name: 'id', type: 'int'},
                'icon',
                'isSet',
                'isSingleCC',
                'isTargetCC',
                'isDisplayCC',
                'isDisplayNode',
                'nodeType',
                'structureUnitType',
                'singleNodeTypes',
                'targetEffort',
                {name: 'bookingAllowed', type: 'boolean'},
                {name: 'leaf', type: 'boolean'}
            ],
            data: []
        });

        this.selectedNodesPanel = Ext.create('Ext.grid.Panel', {
            title: BC.translate('bestehendeknotenbkzs'),
            id: 'configNodes',
            store: 'SelectedNodes',
            width: 790,
            height: 200,
            margin: '5 0 5 10',
            columns: [{
                header: BC.translate('kind'),
                dataIndex: 'isSet',
                menuDisabled: true,
                renderer: function(value) {
                    if (value) {
                        return '<b>' + BC.translate('set') + '</b>';
                    }
                    return '<b>' + BC.translate('node') + '</b>';
                }
            }, {
                header: BC.translate('nodeType'),
                dataIndex: 'iconCls',
                tdCls: 'node-type-fake-cell-cls',
                align: 'center',
                menuDisabled: true,
                renderer: function(value) {
                    switch (value) {
                        case 'folder':
                            value = '<img src="files/images/folder.png">';
                            break;
                        case 'star':
                            value = '<img src="files/images/star.png">';
                            break;
                        case 'page-white-stack':
                            value = '<img src="files/images/page_white_stack.png">';
                            break;
                        case 'page-white-text':
                            value = '<img src="files/images/page_white_text.png">';
                            break;
                        case 'page-white-edit':
                            value = '<img src="files/images/page_white_edit.png">';
                            break;
                        case 'set':
                            value = '<img src="files/images/set.png">';
                            break;
                        case 'sap':
                            value = '<img src="files/images/coins.png">';
                            break;
                    }

                    return value;
                }
            }, {
                header: BC.translate('name'),
                menuDisabled: true,
                dataIndex: 'name',
                flex: 1,
                viewConfig: {
                    forceFit: true
                },
                renderer: function(value, params, record) {
                    if (value) {
                        params.tdAttr = 'data-qtip="' + value + '"';
                    }
                    if (record.get('bookingAllowed') === false && record.get('leaf')) {
                        value += " -- (" + BC.translate('keinbuchbar') + ")";
                    }
                    return value;
                }
            }, {
                header: BC.translate('treepos'),
                xtype: 'actioncolumn',
                align: 'center',
                menuDisabled: true,
                width: 95,
                items: [{
                    icon: 'files/images/wayUp.png',
                    handler: function(view, rowIndex, colIndex, item, e) {
                        view.fireEvent('showPathToRoot', view, rowIndex, e.getXY());
                    },
                    getClass: function(v, meta, record) {
                        if (record.get('isSet')) {
                            return 'x-hide-display';
                        }
                    }
                }]
            }, {
                header: BC.translate('einzelbkz'),
                dataIndex: 'singleNodeTypes',
                menuDisabled: true,
                renderer: function(value) {
                    if (value && (value & BC.store.SingleNodeTypes.DIAGRAM_SINGLE_CC) > 0) {
                        return '<img src="files/images/yes.png">';
                    }
                    return '-';
                },
                align: 'center'
            }, {
                header: BC.translate('displayCC'),
                dataIndex: 'checked',
                menuDisabled: true,
                renderer: function(value, params, record) {
                    if (value && !record.get('disabled')) {
                        return '<img src="files/images/yes.png">';
                    }
                    return '-';
                },
                align: 'center'
            }, {
                header: BC.translate('rebookToCC'),
                dataIndex: 'singleNodeTypes',
                menuDisabled: true,
                renderer: function(value) {
                    if (value && (value & BC.store.SingleNodeTypes.REBOOK_TO_CC) > 0) {
                        return '<img src="files/images/yes.png">';
                    }
                    return '-';
                },
                align: 'center'
            }, {
                header: BC.translate('displayNode'),
                dataIndex: 'singleNodeTypes',
                menuDisabled: true,
                renderer: function(value) {
                    if (value && (value & BC.store.SingleNodeTypes.TABLE_DISPLAY_NODE) > 0) {
                        return '<img src="files/images/yes.png">';
                    }
                    return '-';
                },
                align: 'center'
            }, {
                header: BC.translate('singleNodeInternalPart'),
                dataIndex: 'singleNodeTypes',
                menuDisabled: true,
                renderer: function(value) {
                    if (value && (value & BC.store.SingleNodeTypes.MIS_INTERNAL) > 0) {
                        return '<img src="files/images/yes.png">';
                    }
                    return '-';
                },
                align: 'center'
            }, {
                header: BC.translate('singleNodeSalesPart'),
                dataIndex: 'singleNodeTypes',
                menuDisabled: true,
                renderer: function(value) {
                    if (value && (value & BC.store.SingleNodeTypes.MIS_SALES) > 0) {
                        return '<img src="files/images/yes.png">';
                    }
                    return '-';
                },
                align: 'center'
            }, {
                header: BC.translate('singleNodeDistinctRow'),
                dataIndex: 'singleNodeTypes',
                menuDisabled: true,
                renderer: function(value) {
                    if (value && (value & BC.store.SingleNodeTypes.STATUS_DISTINCTROW) > 0) {
                        return '<img src="files/images/yes.png">';
                    }
                    return '-';
                },
                align: 'center'
            }, {
                header: BC.translate('singleNodeAtNextLevel'),
                dataIndex: 'singleNodeTypes',
                menuDisabled: true,
                renderer: function(value) {
                    if (value && (value & BC.store.SingleNodeTypes.STATUS_WITHNEXTLEVEL) > 0) {
                        return '<img src="files/images/yes.png">';
                    }
                    return '-';
                },
                align: 'center'
            }],
            bbar: [{
                text: BC.translate('change'),
                action: 'addNodes',
                icon: 'files/images/pencil.png'
            }, {
                text: BC.translate('bc_remove_cc'),
                id: 'removeCC',
                action: 'clearNode',
                icon: 'files/images/clearOne.png',
                disabled: true
            }, {
                text: BC.translate('bc_remove_allcc'),
                action: 'clearNodes',
                id: 'removeCCs',
                icon: 'files/images/clearAll.png',
                disabled: true
            }]
        });

        this.selectedNodesPanel.getView().on({
            'render': function(view) {
                view.nodeTypeToolTip = Ext.create('Ext.tip.ToolTip', {
                    target: view.el,
                    delegate: view.cellSelector + ' .node-type-fake-cell-cls',
                    trackMouse: true,
                    listeners: {
                        beforeshow: function(toolTip) {
                            var record = view.getRecord(toolTip.triggerElement.parentNode);
                            if (record.get('isSet')) {
                                return false;
                            }

                            var nodeType = Ext.data.StoreManager.lookup('NodeTypes').getById(record.get('nodeTypeId')),
                                structureUnitType = Ext.data.StoreManager.lookup('StructureUnitTypes').getById(record.get('structureUnitTypeId')),
                                toolTipText = '';

                            if (nodeType !== null) {
                                toolTipText += nodeType.get('name');
                                if (structureUnitType !== null) {
                                    toolTipText += '/' + structureUnitType.get('name');
                                }
                            }
                            toolTip.update(toolTipText);
                        }
                    }
                });
            }
        });

        var accuracy = {
            xtype: 'fieldset',
            id: 'configAccuracy',
            title: BC.translate('values'),
            width: 270,
            margin: 0,
            padding: 10,
            defaultType: 'radiofield',
            layout: {
                type: 'table',
                columns: 4
            },
            items: [{
                xtype: 'image',
                src: 'files/images/diagramAccDay.png'
            }, {
                boxLabel: BC.translate('tageswerte'),
                name: 'valuesTypeRadio',
                inputValue: 'dayValues',
                id: 'dayValuesRadio',
                margin: '0 5 0 5',
                checked: true
            }, {
                xtype: 'image',
                src: 'files/images/diagramAccMonth.png'
            }, {
                boxLabel: BC.translate('monatswerte'),
                name: 'valuesTypeRadio',
                inputValue: 'monthValues',
                id: 'monthValuesRadio',
                margin: '0 5 0 5'
            }]
        };

        var diagramType = {
            xtype: 'fieldset',
            id: 'configDiagramType',
            title: BC.translate('diagrammtyp'),
            width: 500,
            margin: '0 20 0 0',
            padding: 10,
            rowspan: 2,
            align: 'center',
            defaultType: 'radiofield',
            layout: {
                type: 'table',
                columns: 6
            },
            items: [{
                xtype: 'image',
                src: 'files/images/DiagramTypeSingle.png'
            }, {
                boxLabel: BC.translate('einzeln'),
                name: 'diagramTypeRadio',
                id: 'diagramTypeRadioSingle',
                inputValue: 'single',
                margin: '0 10 0 5',
                checked: true
            }, {
                xtype: 'image',
                src: 'files/images/DiagramTypeCumalatedWithout.png'
            }, {
                boxLabel: BC.translate('cumulatedWithOutStarValue'),
                name: 'diagramTypeRadio',
                id: 'diagramTypeRadioCumWithOut',
                margin: '0 10 0 5',
                inputValue: 'cumulatedWithOutStarValue'
            }, {
                xtype: 'image',
                src: 'files/images/DiagramTypeCumalatedWith.png'
            }, {
                boxLabel: BC.translate('cumulatedWithStarValue'),
                name: 'diagramTypeRadio',
                id: 'diagramTypeRadioCumWith',
                margin: '0 10 0 5',
                inputValue: 'cumulatedWithStarValue'
            }]
        };

        var ccWithHourBookings = {
            xtype: 'fieldset',
            id: 'configCCWithHourBookings',
            title: BC.translate('displayCCWithHourBookings'),
            width: 385,
            margin: '0 20 0 0',
            padding: 10,
            align: 'center',
            defaultType: 'radiofield',
            layout: {
                type: 'table',
                columns: 2
            },
            items: [{
                boxLabel: BC.translate('onlyBooked'),
                id: 'ccWithHourBookingsYes',
                name: 'ccWithHourBookingsRadio',
                margin: '0 15 0 5',
                checked: true
            }, {
                boxLabel: BC.translate('allCCs'),
                id: 'ccWithHourBookingsNo',
                name: 'ccWithHourBookingsRadio',
                margin: '0 15 0 5'
            }]
        };

        var xAxis = {
            xtype: 'fieldset',
            id: 'configXAxis',
            title: BC.translate('xAxis'),
            width: 385,
            margin: 0,
            padding: 10,
            align: 'center',
            defaultType: 'radiofield',
            layout: {
                type: 'table',
                columns: 2
            },
            items: [{
                boxLabel: BC.translate('employees'),
                id: 'xAxisEmployees',
                name: 'xAxisRadio',
                margin: '0 15 0 5',
                checked: true
            }, {
                boxLabel: BC.translate('monatswerte'),
                id: 'xAxisMonthValues',
                name: 'xAxisRadio',
                margin: '0 15 0 5'
            }]
        };

        var hrsNote = {
            xtype: 'textfield',
            id: 'configHrsNote',
            name: 'hrsNote',
            fieldLabel: BC.translate('onlyhrsbookingswithnote'),
            width: 650,
            labelWidth: 190,
            margin: '0 20 0 0',
            maxLength: 20,
            enforceMaxLength: true
        };

        var limit = {
            xtype: 'textfield',
            id: 'configLimit',
            name: 'configLimit',
            fieldLabel: BC.translate('limit'),
            width: 120,
            labelWidth: 32,
            margin: 0,
            maxLength: 8,
            maskRe: /[0-9]/,
            enforceMaxLength: true,
            hideTrigger: true
        };

        var division = {
            xtype: 'combo',
            id: 'configDivision',
            name: 'configDivision',
            fieldLabel: BC.translate('division'),
            store: 'Divisions',
            queryMode: 'local',
            typeAhead: true,
            forceSelection: true,
            selectOnFocus: true,
            lastQuery: '',
            displayField: 'displayName',
            valueField: 'id',
            width: 400,
            labelWidth: 50,
            margin: '0 90 0 0'
        };

        var targetHourlyRate = {
            xtype: 'textfield',
            id: 'configTargetHourlyRate',
            name: 'configTargetHourlyRate',
            fieldLabel: BC.translate('targethourlyrate'),
            width: 300,
            labelWidth: 125,
            maxLength: 8,
            maskRe: /[0-9]/,
            enforceMaxLength: true,
            hideTrigger: true
        };

        var displayFigures = {
            xtype: 'fieldset',
            id: 'configDisplayFigures',
            name: 'configDisplayFigures',
            title: BC.translate('configDisplayFigures'),
            width: 790,
            margin: '5 0 5 0',
            padding: 10,
            layout: {
                type: 'table',
                columns: 2
            },
            items: [{
                boxLabel: BC.translate('displayFiguresOncePerPeriod'),
                name: 'dispFig',
                xtype: 'radio',
                id: 'perPeriod',
                inputValue: BC.store.status.DisplayFigures.ONCE_FOR_WHOLE_PERIOD,
                checked: true,
                width: 180,
                margin: '0 10 0 0'
            }, {
                boxLabel: BC.translate('displayFiguresPerMonth'),
                xtype: 'radio',
                name: 'dispFig',
                id: 'perMonth',
                inputValue: BC.store.status.DisplayFigures.PER_MONTH,
                width: 180,
                margin: '0 10 0 0'
            }]
        };

        var displayLines = {
            xtype: 'fieldset',
            id: 'configDisplayLines',
            name: 'configDisplayLines',
            title: BC.translate('configDisplayLines'),
            width: 790,
            margin: '5 0 5 0',
            padding: 10,
            layout: {
                type: 'table',
                columns: 1
            },
            items: [{
                boxLabel: BC.translate('onlyIfHoursAmountsInSelectedGeneralPeriod'),
                name: 'displayLines',
                xtype: 'radio',
                id: 'onlyIfHoursAmountsInSelectedGeneralPeriod',
                inputValue: BC.store.status.DisplayLines.ONLY_IF_HOURS_AMOUNTS_IN_SELECTED_GENERAL_PERIOD,
                checked: true,
                width: 540,
                margin: '0 10 0 0'
            }, {
                boxLabel: BC.translate('onlyIfHoursAmountsInSelectedPeriod'),
                xtype: 'radio',
                name: 'displayLines',
                id: 'onlyIfHoursAmountsInSelectedPeriod',
                inputValue: BC.store.status.DisplayLines.ONLY_IF_HOURS_AMOUNTS_IN_SELECTED_PERIOD,
                width: 540,
                margin: '0 10 0 0'
            }, {
                boxLabel: BC.translate('onlyIfHoursInSelectedPeriod'),
                xtype: 'radio',
                name: 'displayLines',
                id: 'onlyIfHoursInSelectedPeriod',
                width: 540,
                inputValue: BC.store.status.DisplayLines.ONLY_IF_HOURS_IN_SELECTED_PERIOD,
                margin: '0 10 0 0'
            }]
        };

        var lockedNodes = {
            width: 780,
            xtype: 'fieldset',
            collapsible: false,
            id: 'configLockedNodes',
            name: 'configLockedNodes',
            title: BC.translate('configLockedNodes'),
            items: [{
                boxLabel: BC.translate('withLockedNodes'),
                name: 'lockedNodes',
                xtype: 'radio',
                id: 'withLockedNodes',
                inputValue: BC.store.status.LockedNodes.WITH_LOCKED_NODES,
                checked: true,
                width: 180,
                margin: '0 10 0 0'
            }, {
                boxLabel: BC.translate('withoutLockedNodes'),
                xtype: 'radio',
                name: 'lockedNodes',
                id: 'withoutLockedNodes',
                inputValue: BC.store.status.LockedNodes.WITHOUT_LOCKED_NODES,
                width: 180,
                margin: '0 10 0 0'
            }],
            margin: '6 90 0 0',
            padding: '10 10 5 10',
            layout: {
                type: 'table',
                columns: 2
            }
        };

        this.selectedEmployeesPanel = Ext.create('Ext.panel.Panel', {
            title: BC.translate('selectedEmployees'),
            id: 'configEmployees',
            width: 790,
            height: 113,
            margin: '5 0 5 10',
            overflowY: 'auto',
            bbar: [{
                text: BC.translate('change'),
                action: 'addEmployees',
                icon: 'files/images/pencil.png'
            }, {
                text: BC.translate('bc_remove_allcc'),
                action: 'clearEmployees',
                icon: 'files/images/clearAll.png'
            }],
            items: [{
                xtype: 'dataview',
                store: 'SelectedEmployees',
                tpl: Ext.create('Ext.XTemplate',
                    '<table>',
                        '<tpl if="this.hasEmployees()">',
                            '<tr>',
                                '<td>',
                                    '<img src="files/images/user_green.png" />',
                                '</td>',
                                '<td>',
                                    // non breaking spaces prevent line breaks.. who would've guessed!
                                    '<b>' + BC.translate('distinctEmps').replace(' ', '&nbsp;') + ':</b>',
                                '</td>',
                                '<td>',
                                    '{% var count = 0; %}',
                                    '<tpl for=".">',
                                        '<tpl if="!isGroup">',
                                            '{[count++ > 0 ? ", " : ""]}',
                                            '{shortcut}',
                                        '</tpl>',
                                    '</tpl>',
                                '</td>',
                            '</tr>',
                        '</tpl>',
                        '<tpl if="this.hasGroups()">',
                            '<tr>',
                                '<td>',
                                    '<img src="files/images/group.png" />',
                                '</td>',
                                '<td>',
                                    '<b>' + BC.translate('groups') + ':</b>',
                                '</td>',
                                '<td>',
                                    '{% var count = 0; %}',
                                    '<tpl for=".">',
                                        '<tpl if="isGroup">',
                                            '{[count++ > 0 ? ", " : ""]}',
                                            '{name}',
                                        '</tpl>',
                                    '</tpl>',
                                '</td>',
                            '</tr>',
                        '</tpl>',
                    '</table>',
                    {
                        store: Ext.StoreMgr.get('SelectedEmployees'),
                        hasEmployees: function() {
                            return Ext.Array.contains(this.store.collect('isGroup'), false);
                        },
                        hasGroups: function() {
                            return Ext.Array.contains(this.store.collect('isGroup'), true);
                        }
                    }
                ),
                itemSelector: 'td.group'
            }]
        });

        var groupAccType = {
            xtype: 'container',
            id: 'groupAccType',
            layout: {
                type: 'table',
                columns: 2
            },
            width: 790,
            height: 70,
            margin: '5 0 5 10',
            items: [diagramType, accuracy]
        };

        var groupHourBookings = {
            xtype: 'container',
            id: 'groupHourBookings',
            layout: {
                type: 'table',
                columns: 2
            },
            width: 790,
            height: 70,
            margin: '5 0 5 10',
            items: [ccWithHourBookings, xAxis]
        };

        var groupHrsNoteLimit = {
            xtype: 'container',
            id: 'groupHrsNoteLimit',
            width: 790,
            margin: '5 0 5 10',
            layout: {
                type: 'table',
                columns: 2
            },
            items: [hrsNote, limit]
        };

        var groupDivisionTargetHourlyRate = {
            xtype: 'container',
            id: 'groupDivisionTargetHourlyRate',
            width: 790,
            margin: '5 0 5 10',
            layout: {
                type: 'table',
                columns: 2
            },
            items: [division, targetHourlyRate]
        };

        var projectStatus = {
            xtype: 'container',
            id: 'groupProjectStatus',
            width: 790,
            margin: '5 0 5 10',
            layout: {
                type: 'table',
                columns: 1,
                rows: 2
            },
            items: [displayFigures, displayLines, lockedNodes]
        };

        var treeParams = {
            xtype: 'fieldset',
            id: 'configTreeParams',
            title: BC.translate('displayedInfo'),
            width: 790,
            margin: '5 0 5 10',
            padding: 10,
            rowspan: 2,
            align: 'center',
            defaultType: 'checkboxfield',
            layout: {
                type: 'table',
                columns: 8
            },
            items: Ext.Array.merge(
                [{
                    xtype: 'image',
                    src: 'files/images/empBookings.png'
                }, {
                    boxLabel: BC.translate('showEmpBookings'),
                    id: 'treeParamsEmpBookings',
                    inputValue: 'single',
                    margin: '0 10 0 10'
                }],
                global.privileges.costAndRevenue === true ? [{
                    xtype: 'image',
                    src: 'files/images/money.png',
                    margin: '0 0 10 0'
                }, {
                    boxLabel: BC.translate('showCostAndRevenue'),
                    id: 'treeParamsCostAndRevenue',
                    margin: '0 10 0 10',
                    inputValue: 'cumulatedWithOutStarValue'
                }] : [],
                [{
                    xtype: 'image',
                    src: 'files/images/btHours.png',
                    margin: '0 0 10 0'
                }, {
                    boxLabel: BC.translate('showBTHours'),
                    id: 'treeParamsBTHours',
                    margin: '0 10 0 10'
                }, {
                    xtype: 'image',
                    src: 'files/images/target.png',
                    margin: '0 0 10 0'
                }, {
                    boxLabel: BC.translate('targetRemainingEffort'),
                    id: 'treeParamsTargetHours',
                    margin: '0 10 0 10'
                }, {
                    xtype: 'image',
                    src: 'files/images/lock.png',
                    margin: '0 0 10 0'
                }, {
                    boxLabel: BC.translate('withoutLockedNodes'),
                    id: 'withoutLockedNodesCheckbox',
                    margin: '0 10 0 10'
                }]
            )
        };

        var noPermission = {
            xtype: 'fieldset',
            id: 'configNoBCPermission',
            title: BC.translate('displayedInfo'),
            width: 790,
            margin: '5 0 5 10',
            padding: 10,
            rowspan: 2,
            align: 'center',
            defaultType: 'checkboxfield',
            layout: {
                type: 'table',
                columns: 2
            },
            items: [{
                xtype: 'image',
                src: 'files/images/no_permission.png'
            }, {
                boxLabel: BC.translate('noBCPermission'),
                id: 'noBCPermission',
                inputValue: 'single',
                margin: '0 30 0 10'
            }]
        };

        var absenceConfig = {
            xtype: 'fieldset',
            id: 'configAbsenceConfig',
            title: BC.translate('displayedInfo'),
            width: 790,
            margin: '5 0 5 10',
            padding: 10,
            rowspan: 2,
            align: 'center',
            defaultType: 'checkboxfield',
            layout: {
                type: 'table',
                columns: 2
            },
            items: [{
                boxLabel: BC.translate('employeesWithAbsences'),
                id: 'absenceConfigEmpWithAbsences',
                inputValue: 'single',
                margin: '0 30 0 10'
            }, {
                boxLabel: BC.translate('groupEmployees'),
                id: 'absenceConfigGroupEmployees',
                inputValue: 'single',
                margin: '0 30 0 10'
            }]
        };

        var performedOrBookingDate = {
            xtype: 'fieldset',
            id: 'configPerformedOrBookingDate',
            title: BC.translate('displayedInfo'),
            width: 790,
            margin: '5 0 5 10',
            padding: 10,
            align: 'center',
            defaultType: 'radiofield',
            layout: {
                type: 'table',
                columns: 2
            },
            items: [{
                boxLabel: BC.translate('performedRebookings'),
                id: 'performedOrBookingDatePerformed',
                name: 'bookingDateRadio',
                margin: '0 30 0 10',
                checked: true
            }, {
                boxLabel: BC.translate('rebookingsWithBookingDate'),
                id: 'performedOrBookingDateBooked',
                name: 'bookingDateRadio',
                inputValue: 'bookingDay',
                margin: '0 30 0 10'
            }]
        };

        var displayLockedNodes = {
            xtype: 'fieldset',
            id: 'configDisplayLockedNodes',
            title: BC.translate('displayedInfo'),
            width: 790,
            margin: '5 0 5 10',
            padding: 10,
            align: 'center',
            defaultType: 'radiofield',
            layout: {
                type: 'table',
                columns: 2
            },
            items: [{
                boxLabel: BC.translate('withLockedNodes'),
                id: 'displayLockedNodesYes',
                name: 'btRadio',
                margin: '0 30 0 10',
                checked: true
            }, {
                boxLabel: BC.translate('withoutLockedNodes'),
                id: 'displayLockedNodesNo',
                name: 'btRadio',
                inputValue: 'bookingDay',
                margin: '0 30 0 10'
            }]
        };

        var btCols = [],
            btRecords = Ext.getStore('BookingTypes').getRange(),
            i,
            len;

        for (i = 0, len = btRecords.length; i < len; i++) {
            btCols.push({
                xtype: 'checkbox',
                boxLabel: btRecords[i].get('displayName'),
                name: 'bookingType-' + btRecords[i].get('id'),
                inputValue: btRecords[i].get('id'),
                checked: true,
                height: 18,
                width: 240
            });
        }

        var bookingTypes = {
            xtype: 'fieldset',
            id: 'configBookingTypes',
            title: BC.translate('hrsbookingwithbookingtype'),
            width: 790,
            margin: '5 0 5 10',
            padding: 10,
            autoScroll: true,
            items: [{
                xtype: 'checkbox',
                boxLabel: BC.translate('displayOnlySubtrees'),
                name: 'displayOnlySubtrees',
                id: 'displayOnlySubtrees',
                inputValue: true,
                checked: false,
                hidden: true,
                height: 18,
                width: 600
            }, {
                xtype: 'checkboxgroup',
                defaults: {
                    margin: '0 0 5 0'
                },
                margin: 0,
                columns: 3,
                items: btCols
            }]
        };

        var additionalCols = Ext.getStore('AdditionalCols').getRange(),
            monthEndCols = [],
            listCols = [],
            colCfg;

        for (i = 0, len = additionalCols.length; i < len; i++) {
            colCfg = {
                id: "addCol" + additionalCols[i].get('id'),
                xtype: 'checkbox',
                boxLabel: additionalCols[i].get('name'),
                inputValue: additionalCols[i].get('id'),
                checked: false,
                height: 18,
                width: 180
            };

            if (additionalCols[i].get('tooltip')) {
                colCfg.boxLabel += '<div data-qtip="' + additionalCols[i].get('tooltip') + '" class="infoIcon"></div>';
            }

            switch (additionalCols[i].get('reportType')) {
                case 'M':
                    colCfg.name = 'monthEndCol-' + additionalCols[i].get('id');
                    monthEndCols.push(colCfg);
                    break;
                case 'L':
                    colCfg.name = 'listCol-' + additionalCols[i].get('id');
                    listCols.push(colCfg);
                    break;
            }
        }

        var listExtraCols = {
            xtype: 'fieldset',
            id: 'configListExtraCols',
            title: BC.translate('configAdditionalColumns'),
            width: 790,
            margin: '5 0 5 10',
            padding: 10,
            autoScroll: false,
            layout: {
                type: 'table',
                columns: 4
            },
            items: listCols
        };

        var listOptions = {
            xtype: 'fieldset',
            id: 'configListOptions',
            title: BC.translate('displayedInfo'),
            width: 790,
            margin: '5 0 5 10',
            padding: 10,
            rowspan: 2,
            align: 'center',
            defaultType: 'checkboxfield',
            layout: {
                type: 'table',
                columns: 1
            },
            items: [{
                boxLabel: BC.translate('onlyHourBookingsWithBeginEnd'),
                id: 'listOptionOnlyHourBookingsWithBeginEnd',
                inputValue: true
            },{
                boxLabel: BC.translate('onlyRemoteWorkDays'),
                id: 'listOptionOnlyRemoteWorkDays',
                inputValue: true,
                listeners: {
                    change: function(me, checked) {
                        BC.view.config.reportConfiguration.Utils.setNonRemoteWorkConfigFieldsDisabled(checked);
                    }
                }
            }]
        };

        var monthEndExtraCols = {
            xtype: 'fieldset',
            id: 'configMonthEndExtraCols',
            title: BC.translate('configAdditionalColumns'),
            width: 790,
            margin: '5 0 5 10',
            padding: 10,
            autoScroll: false,
            layout: {
                type: 'table',
                columns: 4
            },
            items: monthEndCols
        };

        var monthEndInformationToBeDisplayed = {
            xtype: 'fieldset',
            id: 'configMonthEndInformationToBeDisplayed',
            title: BC.translate('displayedInfo'),
            width: 790,
            margin: '5 0 5 10',
            padding: 10,
            rowspan: 2,
            align: 'center',
            defaultType: 'checkboxfield',
            layout: {
                type: 'table',
                columns: 1
            },
            items: [{
                boxLabel: BC.translate('monthEndZeroHoursPerWeek'),
                id: 'monthEndZeroHours',
                inputValue: 'single'
            }, {
                boxLabel: BC.translate('configOneLinePerEmployee'),
                id: 'monthEndOneLinePerEmployee',
                inputValue: 'single'
            }, {
                boxLabel: BC.translate('configOneTabPerCompany'),
                id: 'monthEndOneTabPerCompany',
                inputValue: 'single'
            }]
        };

        var orderOverViewOptions = {
            xtype: 'fieldset',
            id: 'configOrderOverviewOptions',
            title: BC.translate('displayedInfo'),
            width: 790,
            margin: '5 0 5 10',
            padding: 10,
            rowspan: 2,
            align: 'center',
            defaultType: 'checkboxfield',
            layout: {
                type: 'table',
                columns: 1
            },
            items: [{
                boxLabel: BC.translate('closedOrders'),
                id: 'orderOverViewOptionClosedOrders',
                inputValue: true
            }, {
                boxLabel: BC.translate('ordersAfterPeriod'),
                id: 'orderOverViewOptionOrdersAfterPeriod',
                inputValue: true
            }, {
                boxLabel: BC.translate('withoutOrdersAfterPeriod'),
                id: 'orderOverViewOptionWithoutOrdersAfterPeriod',
                inputValue: true
            }, {
                boxLabel: BC.translate('withHigherLevelPosition'),
                id: 'orderOverViewOptionWithHigherLevelPosition',
                inputValue: true
            }]
        };

        var contingentRangeInformationToBeDisplayed = {
            xtype: 'fieldset',
            id: 'configContingentRangeInformationToBeDisplayed',
            title: BC.translate('displayedInfo'),
            width: 790,
            margin: '5 0 5 10',
            padding: 10,
            rowspan: 2,
            align: 'center',
            defaultType: 'checkboxfield',
            layout: {
                type: 'table',
                columns: 1
            },
            items: [{
                boxLabel: BC.translate('externalEmployees'),
                id: 'contingentRangeExternalEmployees',
                inputValue: true
            }]
        };

        var contingentDataInformationToBeDisplayed = {
            xtype: 'fieldset',
            id: 'configContingentDataInformationToBeDisplayed',
            title: BC.translate('displayedInfo'),
            width: 790,
            margin: '5 0 5 10',
            padding: 10,
            rowspan: 2,
            align: 'center',
            items: [{
                xtype: 'radiogroup',
                columns: 1,
                vertical: true,
                id: 'contingentDataReportType',
                items: [{
                    boxLabel: BC.translate('allColumns'),
                    inputValue: 1,
                    name: 'contingentDataReportType',
                    checked: true
                },{
                    boxLabel: BC.translate('purchaseInvoiceCheck'),
                    name: 'contingentDataReportType',
                    inputValue: 2
                },{
                    boxLabel: BC.translate('contractManagement'),
                    name: 'contingentDataReportType',
                    inputValue: 3
                }]
            }]
        };

        let materialFilterSelection = Ext.create('BC.view.config.MaterialFilter', {
            id: 'configMaterialFilter',
            width: 790,
            margin: '5 0 5 10',
            padding: 10
        });

        this.items = [
            dateRange,
            this.selectedNodesPanel,
            groupAccType,
            groupHourBookings,
            groupHrsNoteLimit,
            groupDivisionTargetHourlyRate,
            bookingTypes,
            treeParams,
            projectStatus,
            this.selectedEmployeesPanel,
            noPermission,
            absenceConfig,
            performedOrBookingDate,
            displayLockedNodes,
            listExtraCols,
            listOptions,
            monthEndExtraCols,
            monthEndInformationToBeDisplayed,
            orderOverViewOptions,
            contingentRangeInformationToBeDisplayed,
            contingentDataInformationToBeDisplayed,
            materialFilterSelection
        ];

        this.callParent(arguments);
    },

    setProfileValues: function(profile, clear) {
        var diagramType = 'E',
            startValue = 'N',
            me = this,
            from,
            until,
            listAdditionalColumnsCheckboxReset,
            listAdditionalColumnsCheckboxSet;

        Ext.suspendLayouts();
        me.profile = profile;

        // Dynamic period
        if (clear) {
            me.getComponent('configDateRange').getComponent('dynamicPeriod').setValue(0);
        } else {
            me.getComponent('configDateRange').getComponent('dynamicPeriod').setValue(me.profile.get('dynamicPeriod'));
        }

        if (clear) {
            me.getComponent('configDateRange').getComponent('dateRangeFrom').setRawValue('');
            me.getComponent('configDateRange').getComponent('dateRangeUntil').setRawValue('');
            me.configPanel.profileData.getComponent('profileName').enable();
            me.configPanel.profileData.getComponent('showProfile').show();
            profile.set('hint', null);

            if (profile.get('type') === this.misDivisionType && profile.get('hasRootNodePermission') === true) {
                me.configPanel.profileData.getComponent('shareProfile').show();
            }
        } else {
            // DateRange
            from = me.profile.get('from');
            if (from) {
                from = Ext.Date.format(from, BC.translate('dateFormat'));
                me.getComponent('configDateRange').getComponent('dateRangeFrom').setRawValue(from);
            }
            until = me.profile.get('to');
            if (until) {
                until = Ext.Date.format(until, BC.translate('dateFormat'));
                me.getComponent('configDateRange').getComponent('dateRangeUntil').setRawValue(until);
            }
        }

        // ProfileData
        me.configPanel.profileData.getComponent('profileName').setRawValue(clear ? '' : me.profile.get('name'));
        me.configPanel.profileData.getComponent('autoMail').setValue(clear ? '0' : me.profile.get('mail'));
        me.configPanel.profileData.getComponent('reportMail').setValue(clear ? '0' : me.profile.get('reportMail'));
        me.configPanel.profileData.getComponent('showProfile').setValue(clear ? [] : me.profile.get('showToUsers'));
        me.configPanel.profileData.getComponent('shareProfile').setValue(clear ? [] : me.profile.get('shareWithUsers'));

        // Accuracy
        if (clear || me.profile.get('diagramType').substring(0, 1) === 'T') {
            me.getComponent('groupAccType').getComponent('configAccuracy').getComponent('dayValuesRadio').setValue(true);
        } else {
            me.getComponent('groupAccType').getComponent('configAccuracy').getComponent('monthValuesRadio').setValue(true);
        }

        // Diagram type
        if (!clear) {
            diagramType = me.profile.get('diagramType').substring(1, 2); //K oder E
            startValue = me.profile.get('startValue'); //J oder N
        }

        if (diagramType === 'K') {
            if (startValue === 'J') {
                me.getComponent('groupAccType').getComponent('configDiagramType').getComponent('diagramTypeRadioCumWith').setValue(true);
            } else {
                me.getComponent('groupAccType').getComponent('configDiagramType').getComponent('diagramTypeRadioCumWithOut').setValue(true);
            }
        } else {
            // acc == 'E' for 'einzeln' which means single
            // starValue is irrelevant for type 'single'

            me.getComponent('groupAccType').getComponent('configDiagramType').getComponent('diagramTypeRadioSingle').setValue(true);
        }

        // Hours with bookings
        if (clear || me.profile.get('ccWithHourBookings') === 1) {
            me.getComponent('groupHourBookings').getComponent('configCCWithHourBookings').getComponent('ccWithHourBookingsYes').setValue(true);
        } else {
            me.getComponent('groupHourBookings').getComponent('configCCWithHourBookings').getComponent('ccWithHourBookingsNo').setValue(true);
        }

        // xAxis
        if (clear || me.profile.get('xAxis') === 'E') {
            me.getComponent('groupHourBookings').getComponent('configXAxis').getComponent('xAxisEmployees').setValue(true);
        } else {
            me.getComponent('groupHourBookings').getComponent('configXAxis').getComponent('xAxisMonthValues').setValue(true);
        }

        // Hrs Note
        me.getComponent('groupHrsNoteLimit').getComponent('configHrsNote').setRawValue(clear ? '' : me.profile.get('hoursNote'));

        //Limit
        me.getComponent('groupHrsNoteLimit').getComponent('configLimit').setRawValue(clear ? '' : me.profile.get('limit'));

        //Division
        if (me.profile.get('divisionId') !== 0) {
            var divisionCombo = me.getComponent('groupDivisionTargetHourlyRate').getComponent('configDivision');
            divisionCombo.getStore().clearFilter();
            divisionCombo.setValue(clear ? '' : me.profile.get('divisionId'));
            divisionCombo.getStore().filter('active', 1);
        }
        // Target Hourly Rate
        me.getComponent('groupDivisionTargetHourlyRate').getComponent('configTargetHourlyRate').setRawValue(clear ? '' : me.profile.get('targetHourlyRate'));

        // If limit in db is 'null'
        // the server gives us 0
        if (!clear && me.profile.get('limit') === 0) {
            me.getComponent('groupHrsNoteLimit').getComponent('configLimit').setRawValue('');
        }

        // Show targetHours
        me.getComponent('configTreeParams').getComponent('treeParamsTargetHours').setValue(clear ? false : me.profile.get('showTargetHours'));

        // Show EmployeeBookings
        me.getComponent('configTreeParams').getComponent('treeParamsEmpBookings').setValue(clear ? false : me.profile.get('showEmpBookings'));

        // Show Cost&Revenue
        if (global.privileges.costAndRevenue === true) {
            me.getComponent('configTreeParams').getComponent('treeParamsCostAndRevenue').setValue(clear ? false : me.profile.get('showCostAndRevenue'));
        }

        // Show booking type hours
        me.getComponent('configTreeParams').getComponent('treeParamsBTHours').setValue(clear ? false : me.profile.get('showBTHours'));

        // Selected nodes/sets
        me.selectedNodesPanel.getStore().removeAll();
        if (!clear) {
            var nodes = me.profile.get('nodes');
            me.selectedNodesPanel.getStore().add(nodes);
        }

        // Selected employees/groups
        me.selectedEmployeesPanel.down('dataview').getStore().removeAll();
        if (!clear) {
            me.selectedEmployeesPanel.down('dataview').getStore().add(me.profile.get('employees'));
        }

        // Show only bookings with begin/end
        me.getComponent('configListOptions').getComponent('listOptionOnlyHourBookingsWithBeginEnd').setValue(me.profile.get('showBookingsWithBeginEnd'));

        // Show only absences of category Homeoffice
        me.getComponent('configListOptions').getComponent('listOptionOnlyRemoteWorkDays').setValue(me.profile.get('showRemoteWorkDays'));

        // show with closed positions of orders which relates to the configured period
        me.getComponent('configOrderOverviewOptions').getComponent('orderOverViewOptionClosedOrders').setValue(me.profile.get('showClosedOrders'));

        // show with orders and positions which start after the configured period
        me.getComponent('configOrderOverviewOptions').getComponent('orderOverViewOptionOrdersAfterPeriod').setValue(me.profile.get('showOrdersAfterPeriod'));

        // show without orders terminating before the configured period and still having open positions
        me.getComponent('configOrderOverviewOptions').getComponent('orderOverViewOptionWithoutOrdersAfterPeriod').setValue(me.profile.get('showWithoutOrdersAfterPeriod'));

        // show without orders terminating before the configured period and still having open positions
        me.getComponent('configOrderOverviewOptions').getComponent('orderOverViewOptionWithHigherLevelPosition').setValue(me.profile.get('showWithHigherLevelPosition'));

        // Show Employees with no BC permission
        me.getComponent('configNoBCPermission').getComponent('noBCPermission').setValue(me.profile.get('showEmployeesWithoutBCPermission'));

        // Show employees as groups
        me.getComponent('configAbsenceConfig').getComponent('absenceConfigGroupEmployees').setValue(clear ? 0 : me.profile.get('showEmployeesAsGroups'));

        // Show only employees with absences
        me.getComponent('configAbsenceConfig').getComponent('absenceConfigEmpWithAbsences').setValue(clear ? 0 : me.profile.get('showEmployeesWithAbsences'));

        // Show rebookings with the following performed or booking date
        if (me.profile.get('showPerformedOrBookingDate') === 1) {
            me.getComponent('configPerformedOrBookingDate').getComponent('performedOrBookingDateBooked').setValue(true);
        } else {
            me.getComponent('configPerformedOrBookingDate').getComponent('performedOrBookingDatePerformed').setValue(true);
        }

        // Show btOrder with locked or without nodes
        if (me.profile.get('showWithLockedNodes') === 0 || clear) {
            me.getComponent('configDisplayLockedNodes').getComponent('displayLockedNodesYes').setValue(true);
            me.getComponent('groupProjectStatus').getComponent('configLockedNodes').getComponent('withoutLockedNodes').setValue(true);
            me.getComponent('configTreeParams').getComponent('withoutLockedNodesCheckbox').setValue(true);
        } else {
            me.getComponent('configDisplayLockedNodes').getComponent('displayLockedNodesNo').setValue(true);
            me.getComponent('groupProjectStatus').getComponent('configLockedNodes').getComponent('withLockedNodes').setValue(true);
            me.getComponent('configTreeParams').getComponent('withoutLockedNodesCheckbox').setValue(false);
        }

        var bookingTypes = profile.get('bookingTypes');

        // Booking overview filter tree by booking types
        me.getComponent('configBookingTypes').getComponent('displayOnlySubtrees').setValue(clear ? false : me.profile.get('displayOnlySubtrees') === 1);

        // Booking types (by default all are selected)
        if (clear || bookingTypes.length === 0 && /^[^RKB]$/.test(profile.get('type')) === true) {
            Ext.Array.each(me.getComponent('configBookingTypes').query('checkboxgroup checkbox'), function(checkbox) {
                checkbox.setValue(true);
            }, this);
        } else {
            Ext.Array.each(me.getComponent('configBookingTypes').query('checkboxgroup checkbox'), function(checkbox) {
                checkbox.setValue(Ext.Array.indexOf(bookingTypes, checkbox.inputValue) !== -1);
            }, this);
        }

        listAdditionalColumnsCheckboxReset = function(checkbox) {
            checkbox.setValue(false);
        };

        listAdditionalColumnsCheckboxSet = function(checkbox) {
            Ext.Array.each(profile.get('additionalCols'), function(additionalCol) {
                if (checkbox.inputValue === additionalCol) {
                    checkbox.setValue(true);
                }
            }, this);
        };

        //Listadditional columns
        for (var reports = ['MonthEnd', 'List'], i = 0, cmp; i < reports.length; i++) {
            cmp = me.getComponent('config' + reports[i] + 'ExtraCols');
            if (clear) {
                Ext.Array.each(cmp.items.items, listAdditionalColumnsCheckboxReset, this);
            } else {
                Ext.Array.each(cmp.items.items, listAdditionalColumnsCheckboxSet, this);
            }
        }

        // Status display figures
        if (clear) {
            me.getComponent('groupProjectStatus').getComponent('configDisplayFigures').getComponent('perPeriod').setValue(true);
            me.getComponent('groupProjectStatus').getComponent('configDisplayFigures').getComponent('perMonth').setValue(false);
            me.getComponent('groupProjectStatus').getComponent('configDisplayLines').getComponent('onlyIfHoursAmountsInSelectedGeneralPeriod').setValue(true);
        } else {
            if (profile.get('displayFigures') === BC.store.status.DisplayFigures.ONCE_FOR_WHOLE_PERIOD) {
                me.getComponent('groupProjectStatus').getComponent('configDisplayFigures').getComponent('perPeriod').setValue(true);
                me.getComponent('groupProjectStatus').getComponent('configDisplayFigures').getComponent('perMonth').setValue(false);
            } else {
                me.getComponent('groupProjectStatus').getComponent('configDisplayFigures').getComponent('perPeriod').setValue(false);
                me.getComponent('groupProjectStatus').getComponent('configDisplayFigures').getComponent('perMonth').setValue(true);
            }

            me.getComponent('groupProjectStatus').getComponent('configDisplayLines').getComponent('onlyIfHoursAmountsInSelectedGeneralPeriod').setValue(
                profile.get('displayLines') === BC.store.status.DisplayLines.ONLY_IF_HOURS_AMOUNTS_IN_SELECTED_GENERAL_PERIOD
            );
            me.getComponent('groupProjectStatus').getComponent('configDisplayLines').getComponent('onlyIfHoursAmountsInSelectedPeriod').setValue(
                profile.get('displayLines') === BC.store.status.DisplayLines.ONLY_IF_HOURS_AMOUNTS_IN_SELECTED_PERIOD
            );
            me.getComponent('groupProjectStatus').getComponent('configDisplayLines').getComponent('onlyIfHoursInSelectedPeriod').setValue(
                profile.get('displayLines') === BC.store.status.DisplayLines.ONLY_IF_HOURS_IN_SELECTED_PERIOD
            );
        }

        // Show Employees with zero hours per week
        me.getComponent('configMonthEndInformationToBeDisplayed').getComponent('monthEndZeroHours').setValue(clear ? '0' : me.profile.get('showZeroHoursPerWeek'));
        me.getComponent('configMonthEndInformationToBeDisplayed').getComponent('monthEndOneLinePerEmployee').setValue(clear ? '0' : me.profile.get('showOneLinePerEmployee'));
        me.getComponent('configMonthEndInformationToBeDisplayed').getComponent('monthEndOneTabPerCompany').setValue(clear ? '0' : me.profile.get('showOneTabPerCompany'));

        me.getComponent('configContingentRangeInformationToBeDisplayed').getComponent('contingentRangeExternalEmployees').setValue(!clear && me.profile.get('showExternalEmployees'));
        me.getComponent('configContingentDataInformationToBeDisplayed').getComponent('contingentDataReportType').setValue({
            contingentDataReportType: clear ? 1 : me.profile.get('contingentDataReportType')
        });

        me.getComponent('configMaterialFilter').getComponent(0).setValue(clear ? [] : me.profile.get('materialFilter'));

        Ext.resumeLayouts(true);
    },

    clearProfileData: function() {
        let configPanel = this.configPanel,
            profileData = configPanel.profileData;

        if (configPanel.isOpenedFromEditIcon === false) {
            profileData.getComponent('profileName').setRawValue('');
            profileData.getComponent('autoMail').setValue('0');
            profileData.getComponent('reportMail').setValue('0');
            profileData.getComponent('showProfile').setValue([]);
            profileData.getComponent('shareProfile').setValue([]);
            this.profile.set('hint', null);
        }
    },

    disableProfileData: function(profile, profileData) {
        if (profile.get('type') !== this.misDivisionType) {
            return;
        }

        if (profile.get('own') === false
            && profileData.getComponent('profileName').rawValue !== undefined
            && profileData.getComponent('profileName').rawValue.length > 0
        ) {
            profileData.getComponent('shareProfile').hide();
            profileData.getComponent('showProfile').hide();
            profileData.getComponent('profileName').disable();
        } else {
            profileData.getComponent('showProfile').show();
            profileData.getComponent('profileName').enable();

            if (profile.get('hasRootNodePermission') === true) {
                profileData.getComponent('shareProfile').show();
            }
        }
    },

    resetDisabledConfigFields: function(reportType) {
        // if not list report
        if (reportType !== 'L') {
            BC.view.config.reportConfiguration.Utils.setNonRemoteWorkConfigFieldsDisabled(false);
        } else {
            if (Ext.getCmp('listOptionOnlyRemoteWorkDays').getValue()) {
                BC.view.config.reportConfiguration.Utils.setNonRemoteWorkConfigFieldsDisabled(true);
            }
        }
    }
});
