Ext.define('BC.view.config.ConfigPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.configPanel',

    cls: 'configPanel',

    border: false,

    layout: 'border',

    requires: ['BC.view.config.Sidebar', 'BC.view.config.Buttongroup', 'BC.view.config.ReportConfiguration', 'Ext.form.FieldSet', 'BC.lib.Multiselect'],

    isOpenedFromProfileList: false,
    // TODO: revalidate with library update (buggy as of 4.1.3)
    // used for storing scroll position in IE
    scrollPosition: {left: 0, top: 0},

    initComponent: function() {
        var me = this;

        this.notificationTypeStore = Ext.create('Ext.data.Store', {
            fields: ['name', 'key'],
            data: [{
                "key": '0',
                "name": Ext.util.Format.htmlDecode(BC.translate('keine', 'cb'))
            }, {
                "key": '1',
                "name": Ext.util.Format.htmlDecode(BC.translate('taeglich', 'cb'))
            }, {
                "key": '2',
                "name": Ext.util.Format.htmlDecode(BC.translate('exceeding', 'cb'))
            }]
        });

        this.reportMailStore = Ext.create('Ext.data.Store', {
            fields: ['name', 'key'],
            data: [{
                "key": '0',
                "name": Ext.util.Format.htmlDecode(BC.translate('keine', 'cb'))
            }, {
                "key": '1',
                "name": Ext.util.Format.htmlDecode(BC.translate('taeglich', 'cb'))
            }, {
                "key": '2',
                "name": Ext.util.Format.htmlDecode(BC.translate('weekly', 'cb'))
            }, {
                "key": '3',
                "name": Ext.util.Format.htmlDecode(BC.translate('monthly', 'cb'))
            }]
        });

        this.showToCombo = new BC.lib.Multiselect({
            fieldLabel: BC.translate('showProfile'),
            labelAlign: 'top',
            id: 'showProfile',
            width: 240,
            margin: '0 0 0 10',
            name: 'showProfile',
            store: 'EmployeesAll',
            queryMode: 'local',
            displayField: 'NAME',
            valueField: 'ID',
            editable: false
        });

        this.shareWithCombo = new BC.lib.Multiselect({
            fieldLabel: BC.translate('shareProfile'),
            labelAlign: 'top',
            id: 'shareProfile',
            width: 240,
            margin: '0 0 0 10',
            name: 'shareProfile',
            store: 'AbleToShareEmployees',
            queryMode: 'local',
            displayField: 'NAME',
            valueField: 'ID',
            editable: false
        });

        this.profileData = new Ext.form.FieldSet({
            width: 825,
            id: 'configProfileData',
            cls: 'profileData-fieldset',
            title: BC.translate('profileData'),
            defaultType: 'textfield',
            layout: 'hbox',
            margin: '10 0 10 175',
            padding: '0 10 10 10',
            items: [{
                fieldLabel: BC.translate('profileName'),
                labelAlign: 'top',
                width: 140,
                margin: '0 0 0 0',
                id: 'profileName',
                name: 'profileName',
                maxLength: 100,
                enforceMaxLength: true,
                maskRe: /[a-zA-Z0-9ÖÄÜßöäü_+\-()\[\]. ]/
            }, {
                xtype: 'combo',
                fieldLabel: BC.translate('autoMail'),
                labelAlign: 'top',
                width: 160,
                margin: '0 0 0 10',
                id: 'autoMail',
                name: 'autoMail',
                store: this.notificationTypeStore,
                queryMode: 'local',
                displayField: 'name',
                valueField: 'key',
                editable: false
            }, {
                xtype: 'component',
                cls: 'profileInfoIcon',
                id: 'autoMailIcon',
                html: '<img src="./files/images/info.png" title="' + BC.translate('actHrsTooltip') + '"/>'
            }, {
                xtype: 'combo',
                fieldLabel: BC.translate('reportmailLabel'),
                labelAlign: 'top',
                width: 160,
                margin: '0 0 0 10',
                id: 'reportMail',
                name: 'reportMail',
                store: this.reportMailStore,
                queryMode: 'local',
                displayField: 'name',
                valueField: 'key',
                editable: false
            }, {
                xtype: 'component',
                cls: 'profileInfoIcon',
                id: 'reportMailIcon',
                html: '<img src="./files/images/info.png" title="' + BC.translate('reportMailTooltip') + '"/>'
            },
            this.showToCombo,
            this.shareWithCombo]
        });

        Ext.apply(me, {
            items: [{
                region: 'west',
                xtype: 'configSidebar',
                width: 175
            },
            {
                baseCls: 'configSpacer',
                xtype: 'panel',
                region: 'north',
                height: 10,
                border: false
            }, {
                xtype: 'panel',
                id: 'configPanelSouth',
                region: 'south',
                border: false,
                items: [this.profileData, {
                    xtype: 'panel',
                    layout: 'hbox',
                    height: 30,
                    margin: '2 0 10 10',
                    border: false,
                    items: [{
                        xtype: 'button',
                        text: BC.translate('reportHelp'),
                        icon: 'files/images/help.png',
                        href: "index/help",
                        target: "_self",
                        id: 'reportHelp',
                        cls: 'reportHelp',
                        style: {
                            marginTop: '5px'
                        },
                        width: 160
                    }, {
                        xtype: 'buttonGroup',
                        id: 'buttonGroupBottom',
                        margin: '0 0 0 430',
                        width: 410
                    }]
                }]
            }, {
                region: 'center',
                xtype: 'reportConfiguration',
                configPanel: this
            }]
        });

        me.callParent(arguments);
    }
});
