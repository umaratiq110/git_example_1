Ext.define('BC.view.config.MaterialFilter', {
    extend: 'Ext.form.FieldSet',
    initComponent() {
        this.callParent();
        this.setTitle(BC.translate('filterResult'));
        this.add([
            {
                xtype: 'combo',
                store: 'MaterialFilter',
                anchor: '100%',
                name: 'materialFilter',
                multiSelect: true,
                forceSelection: true,
                editable: false,
                displayField: 'name',
                queryMode: 'local',
                valueField: 'id'
            }
        ]);
    }
});