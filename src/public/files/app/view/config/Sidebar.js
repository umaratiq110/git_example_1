Ext.define('BC.view.config.Sidebar', {
    alias: 'widget.configSidebar',

    extend: 'Ext.view.View',

    initComponent: function() {
        Ext.apply(this, {
            id: 'configSidebar',
            border: false,
            cls: 'sidebar-list',
            selModel: {
                deselectOnContainerClick: false
            },
            store: {
                fields: [
                    {name: 'abbr'},
                    {name: 'type', sortType: function(value) {
                        switch (value) {
                            case BC.translate('bookingReport'): return 1;
                            case BC.translate('tabelle'): return 2;
                            case BC.translate('liste'): return 3;
                            case BC.translate('diagramm'): return 4;
                            case BC.translate('sapKeyHours'): return 5;
                            case BC.translate('accountingReport'): return 6;
                            case BC.translate('notBooked'): return 7;
                            case BC.translate('monthEndReport'): return 8;
                            case BC.translate('rebookReport'): return 9;
                            case BC.translate('rebookLog'): return 10;
                            case BC.translate('btOrder'): return 11;
                            case BC.translate('btModifications'): return 12;
                            case BC.translate('permissionReport'): return 13;
                            case BC.translate('standby'): return 14;
                            case BC.translate('absenceCalendar'): return 15;
                            case BC.translate('misDivision'): return 16;
                            case BC.translate('misOverall'): return 17;
                            case BC.translate('projectStatus'): return 18;
                            case BC.translate('accountableHours'): return 19;
                            case BC.translate('orderOverview'): return 20;
                            case BC.translate('contingentRange'): return 21;
                            case BC.translate('contingentData'): return 22;
                            case BC.translate('contingentSapInvoiceComparison'): return 23;
                            case BC.translate('prismaValues'): return 24;
                            default: break;
                        }
                    }}
                ],
                data: [
                    {abbr: 'D', type: BC.translate('diagramm')},
                    {abbr: 'L', type: BC.translate('liste')},
                    {abbr: 'T', type: BC.translate('tabelle')},
                    {abbr: 'E', type: BC.translate('notBooked')},
                    {abbr: 'B', type: BC.translate('bookingReport')},
                    {abbr: 'N', type: BC.translate('btModifications')},
                    {abbr: 'F', type: BC.translate('btOrder')}
                ],
                sorters: 'type'
            },
            itemSelector: '.reportType',
            tpl: new Ext.XTemplate(
                '<tpl for=".">',
                    '<div id="reportType{abbr}" class="reportType">{type}</div>',
                '</tpl>',
                '<div id="placeHolderDiv" class="reportTypePlaceholder" style="height: 100%; width: 100%;"></div>'
            )
        });

        if (global.privileges.accounting === true) {
            this.getStore().data.push({
                type: BC.translate('accountingReport'),
                abbr: 'A'
            });
        }
        if (global.privileges.rebook === true) {
            this.getStore().data.push({
                type: BC.translate('rebookReport'),
                abbr: 'R'
            });
        }

        if (global.privileges.rebookLog === true) {
            this.getStore().data.push({
                type: BC.translate('rebookLog'),
                abbr: 'O'
            });
        }

        if (global.privileges.permission === true) {
            this.getStore().data.push({
                type: BC.translate('permissionReport'),
                abbr: 'P'
            });
        }

        if (global.privileges.monthEnd === true) {
            this.getStore().data.push({
                type: BC.translate('monthEndReport'),
                abbr: 'M'
            });
        }

        if (global.privileges.sapKeyHours === true) {
            this.getStore().data.push({
                type: BC.translate('sapKeyHours'),
                abbr: 'S'
            });
        }

        if (global.privileges.standby === true) {
            this.getStore().data.push({
                type: BC.translate('standby'),
                abbr: 'H'
            });
        }

        if (global.privileges.absenceCalendar === true) {
            this.getStore().data.push({
                type: BC.translate('absenceCalendar'),
                abbr: 'C'
            });
        }

        if (global.privileges.misDivisionOverall === true) {
            this.getStore().data.push({
                type: BC.translate('misDivision'),
                abbr: 'I'
            }, {
                type: BC.translate('misOverall'),
                abbr: 'J'
            });
        }

        if (global.privileges.projectStatus === true) {
            this.getStore().data.push({
                type: BC.translate('projectStatus'),
                abbr: 'K'
            });
        }

        if (global.privileges.accountableHours === true) {
            this.getStore().data.push({
                type: BC.translate('accountableHours'),
                abbr: 'G'
            });
        }

        if (global.privileges.orderOverview === true) {
            this.getStore().data.push({
                type: BC.translate('orderOverview'),
                abbr: 'V'
            });
        }

        if (global.privileges.contingentRange === true) {
            this.getStore().data.push({
                type: BC.translate('contingentRange'),
                abbr: 'Q'
            });
        }

        if (global.privileges.contingentData === true) {
            this.getStore().data.push({
                type: BC.translate('contingentData'),
                abbr: 'Z'
            });
        }

        if (global.privileges.contingentSapInvoiceComparison === true) {
            this.getStore().data.push({
                type: BC.translate('contingentSapInvoiceComparison'),
                abbr: 'X'
            });
        }

        if (global.privileges.prismaValues === true) {
            this.getStore().data.push({
                type: BC.translate('prismaValues'),
                abbr: 'Y'
            });
        }

        //For the correct presentation at 1024 X 768
        if (this.getStore().data.length > 12) {
            this.addCls('sidebar-list-small');
        }
        if (this.getStore().data.length > 16) {
            this.addCls('sidebar-list-tiny');
        }

        this.callParent(arguments);
    }
});
