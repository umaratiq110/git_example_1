Ext.define('BC.view.config.SelectedNodesDialog', {
    extend: 'Ext.window.Window',
    alias: 'widget.selectedNodesDialog',

    uses: [
        'Ext.button.Button',
        'BC.view.config.TreePanel',
        'BC.lib.tree.AjaxSearch'
    ],

    id: 'selectedNodesDialog',
    layout: 'fit',
    closeAction: 'hide',
    height: 400,
    width: 800,
    modal: true,
    closable: false,
    resizable: true,
    initComponent: function() {
        this.okBtn = Ext.create('Ext.button.Button', {
            id: 'okButton',
            icon: 'files/images/monthEnd.png',
            text: BC.translate('ok'),
            action: 'updateNodeList'
        });

        this.cancelBtn = Ext.create('Ext.button.Button', {
            id: 'cancelButton',
            icon: 'files/images/cancel.png',
            text: BC.translate('abbrechen'),
            action: 'revertNodeList'
        });

        this.selectedNodes = Ext.create('BC.view.config.TreePanel', {
            id: 'nodesTree',
            border: false,
            store: 'Nodes',
            customSearch: true,
            columns: [{
                xtype: 'treecolumn',
                text: BC.translate('abkbkz'),
                flex: 1,
                sortable: false,
                menuDisabled: true,
                dataIndex: 'text',
                renderer: function(value, params, record) {
                    if (record.get('bookingAllowed') === false && record.get('leaf')) {
                        value += " -- (" + BC.translate('keinbuchbar') + ")";
                    }
                    return value;
                }
            }, {
                text: BC.translate('description'),
                width: 200,
                sortable: false,
                menuDisabled: true,
                dataIndex: 'description'
            }, {
                text: BC.translate('bookingType').replace(' ', '<br />'),
                width: 75,
                sortable: false,
                menuDisabled: true,
                align: 'center',
                renderer: function(value, params, record) {
                    var btId = record.get('bookingTypeId');
                    if (btId) {
                        return Ext.data.StoreManager.lookup('BookingTypes').getById(btId).get('name');
                    }
                    return '';
                }
            }, {
                xtype: 'templatecolumn',
                text: BC.translate('einzelbkz'),
                width: 80,
                sortable: false,
                menuDisabled: true,
                dataIndex: 'singleNodeTypes',
                align: 'center',
                tpl: new Ext.XTemplate(
                    '<div data-singlenodetype="' + BC.store.SingleNodeTypes.DIAGRAM_SINGLE_CC + '" class="{[this.getCls(values)]}">&#160;</div>',
                    {
                        getCls: function(values) {
                            var cssPrefix = Ext.baseCSSPrefix,
                                cls = [cssPrefix + 'grid-singlenode-col'];

                            if (values.isSet
                                || values.checked === null
                                // render only for leaves
                                || !values.leaf
                            ) {
                                return '';
                            }

                            if (!values.checked) {
                                cls.push(cssPrefix + 'grid-singlenode-col-disabled');
                            }
                            if ((values.singleNodeTypes & BC.store.SingleNodeTypes.DIAGRAM_SINGLE_CC) > 0) {
                                cls.push(cssPrefix + 'grid-singlenode-col-checked');
                            }

                            return cls.join(' ');
                        }
                    }
                )
            }, {
                xtype: 'templatecolumn',
                text: BC.translate('rebookToCC'),
                width: 95,
                sortable: false,
                menuDisabled: true,
                dataIndex: 'singleNodeTypes',
                align: 'center',
                tpl: new Ext.XTemplate(
                    '<div data-singlenodetype="' + BC.store.SingleNodeTypes.REBOOK_TO_CC + '" class="{[this.getCls(values)]}">&#160;</div>',
                    {
                        getCls: function(values) {
                            var cssPrefix = Ext.baseCSSPrefix,
                                cls = [cssPrefix + 'grid-singlenode-col'];

                            if (values.isSet
                                || values.checked === null
                                // render only for leaves different than 'Structure unit'
                                || !values.leaf
                                || values.nodeTypeId === 1
                            ) {
                                return '';
                            }

                            if ((values.singleNodeTypes & BC.store.SingleNodeTypes.REBOOK_TO_CC) > 0) {
                                cls.push(cssPrefix + 'grid-singlenode-col-checked');
                            }

                            return cls.join(' ');
                        }
                    }
                )
            }, {
                xtype: 'templatecolumn',
                text: BC.translate('displayNode'),
                width: 95,
                sortable: false,
                menuDisabled: true,
                dataIndex: 'singleNodeTypes',
                align: 'center',
                tpl: new Ext.XTemplate(
                    '<div data-singlenodetype="' + BC.store.SingleNodeTypes.TABLE_DISPLAY_NODE + '" class="{[this.getCls(values)]}">&#160;</div>',
                    {
                        getCls: function(values) {
                            var cssPrefix = Ext.baseCSSPrefix,
                                cls = [cssPrefix + 'grid-singlenode-col'];

                            if (values.isSet
                                || values.checked === null
                                // render only for non leaf nodes
                                || values.leaf
                            ) {
                                return '';
                            }

                            if ((values.singleNodeTypes & BC.store.SingleNodeTypes.TABLE_DISPLAY_NODE) > 0) {
                                cls.push(cssPrefix + 'grid-singlenode-col-checked');
                            }

                            return cls.join(' ');
                        }
                    }
                )
            }, {
                xtype: 'templatecolumn',
                text: BC.translate('singleNodeInternal'),
                width: 95,
                sortable: false,
                menuDisabled: true,
                dataIndex: 'singleNodeTypes',
                align: 'center',
                tpl: new Ext.XTemplate(
                    '<div data-singlenodetype="' + BC.store.SingleNodeTypes.MIS_INTERNAL + '" class="{[this.getCls(values)]}">&#160;</div>',
                    {
                        getCls: function(values) {
                            var cssPrefix = Ext.baseCSSPrefix,
                                cls = [cssPrefix + 'grid-singlenode-col'];

                            if (values.isSet
                                || values.checked === null
                            ) {
                                return '';
                            }

                            if (values['singleNode' + BC.store.SingleNodeTypes.MIS_INTERNAL + 'Disabled']) {
                                cls.push(cssPrefix + 'grid-singlenode-col-disabled');
                                cls.push(cssPrefix + 'grid-singlenode-col-checked');
                            } else if ((values.singleNodeTypes & BC.store.SingleNodeTypes.MIS_INTERNAL) > 0) {
                                cls.push(cssPrefix + 'grid-singlenode-col-checked');
                            }

                            return cls.join(' ');
                        }
                    }
                )
            }, {
                xtype: 'templatecolumn',
                text: BC.translate('singleNodeSales'),
                width: 105,
                sortable: false,
                menuDisabled: true,
                dataIndex: 'singleNodeTypes',
                align: 'center',
                tpl: new Ext.XTemplate(
                    '<div data-singlenodetype="' + BC.store.SingleNodeTypes.MIS_SALES + '" class="{[this.getCls(values)]}">&#160;</div>',
                    {
                        getCls: function(values) {
                            var cssPrefix = Ext.baseCSSPrefix,
                                cls = [cssPrefix + 'grid-singlenode-col'];

                            if (values.isSet
                                || values.checked === null
                            ) {
                                return '';
                            }

                            if (values['singleNode' + BC.store.SingleNodeTypes.MIS_SALES + 'Disabled']) {
                                cls.push(cssPrefix + 'grid-singlenode-col-disabled');
                                cls.push(cssPrefix + 'grid-singlenode-col-checked');
                            } else if ((values.singleNodeTypes & BC.store.SingleNodeTypes.MIS_SALES) > 0) {
                                cls.push(cssPrefix + 'grid-singlenode-col-checked');
                            }

                            return cls.join(' ');
                        }
                    }
                )
            }, {
                text: BC.translate('target'),
                width: 50,
                sortable: false,
                menuDisabled: true,
                dataIndex: 'targetEffort',
                align: 'center',
                renderer: function(value) {
                    if (value) {
                        return BC.translate('given');
                    }
                    return "";
                }
            }, {
                xtype: 'templatecolumn',
                text: BC.translate('singleNodeDistinctRow') + '<br/>' + BC.translate('singleNodeWithTargetRest'),
                width: 110,
                sortable: false,
                menuDisabled: true,
                dataIndex: 'singleNodeTypes',
                align: 'center',
                tpl: new Ext.XTemplate(
                    '<div data-singlenodetype="' + BC.store.SingleNodeTypes.STATUS_DISTINCTROW + '" class="{[this.getCls(values)]}">&#160;</div>',
                    {
                        getCls: function(values) {
                            var cssPrefix = Ext.baseCSSPrefix,
                                cls = [cssPrefix + 'grid-singlenode-col'];

                            if (values.isSet
                                || values.checked === null
                            ) {
                                return '';
                            }

                            if ((values.singleNodeTypes & BC.store.SingleNodeTypes.STATUS_DISTINCTROW) > 0) {
                                cls.push(cssPrefix + 'grid-singlenode-col-checked');
                            }

                            return cls.join(' ');
                        }
                    }
                )
            }, {
                xtype: 'templatecolumn',
                text: BC.translate('singleNodeAtNextLevel'),
                width: 105,
                sortable: false,
                menuDisabled: true,
                dataIndex: 'singleNodeTypes',
                align: 'center',
                tpl: new Ext.XTemplate(
                    '<div data-singlenodetype="' + BC.store.SingleNodeTypes.STATUS_WITHNEXTLEVEL + '" class="{[this.getCls(values)]}">&#160;</div>',
                    {
                        getCls: function(values) {
                            var cssPrefix = Ext.baseCSSPrefix,
                                cls = [cssPrefix + 'grid-singlenode-col'];

                            if (values.isSet
                                || values.checked === null
                            ) {
                                return '';
                            }

                            if ((values.singleNodeTypes & BC.store.SingleNodeTypes.STATUS_WITHNEXTLEVEL) > 0) {
                                cls.push(cssPrefix + 'grid-singlenode-col-checked');
                            }

                            return cls.join(' ');
                        }
                    }
                )
            }]
        });

        this.selectedNodes.addPlugin({
            ptype: 'treeajaxsearch'
        });

        this.selectedNodes.comboboxAdvancedSearch = Ext.create('Ext.form.field.ComboBox', {
            fieldLabel: BC.translate('suchennach'),
            labelAlign: 'right',
            store: new Ext.data.Store({
                data: [
                   {"id": 0, "name": BC.translate('knotenbkzs')},
                   {"id": 1, "name": BC.translate('debtorNumber')},
                   {"id": 2, "name": BC.translate('sapKey')}
                ],
                fields: [
                    {name: 'id', type: 'int'},
                    {name: 'name', type: 'string'}
                ]
            }),
            queryMode: 'local',
            displayField: 'name',
            valueField: 'id',
            value: 0,
            editable: false
        });
        this.selectedNodes.getDockedItems('toolbar[dock="top"]')[0].add(['->', this.selectedNodes.comboboxAdvancedSearch]);

        Ext.apply(this, {
            title: BC.translate('bookingtreeSelection'),
            items: [this.selectedNodes],
            buttons: [
                this.okBtn,
                this.cancelBtn
            ]
        });

        this.callParent();
    }
});
