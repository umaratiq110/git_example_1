Ext.define('BC.view.config.SelectedEmployeesDialog', {
    extend: 'Ext.window.Window',
    alias: 'widget.selectedEmployeesDialog',

    uses: [
        'Ext.button.Button',
        'BC.view.config.TreePanel',
        'BC.lib.tree.LiveSearch'
    ],

    id: 'selectedEmpDialog',
    layout: 'fit',
    closeAction: 'hide',
    height: 400,
    width: 800,
    modal: true,
    closable: false,
    resizable: true,
    initComponent: function() {
        this.okBtn = Ext.create('Ext.button.Button', {
            id: 'selEmpOkButton',
            icon: 'files/images/monthEnd.png',
            text: BC.translate('ok'),
            action: 'updateEmpList'
        });

        this.cancelBtn = Ext.create('Ext.button.Button', {
            id: 'elEmpCancelButton',
            icon: 'files/images/cancel.png',
            text: BC.translate('abbrechen'),
            action: 'revertEmpList'
        });

        this.selectedEmployees = Ext.create('BC.view.config.TreePanel', {
            id: 'employeesTree',
            border: false,
            store: 'Employees',
            columns: [{
                xtype: 'treecolumn',
                text: BC.translate('mitarbeiter'),
                flex: 1,
                sortable: false,
                menuDisabled: true,
                dataIndex: 'text'
            }, {
                text: BC.translate('description'),
                width: 300,
                sortable: false,
                menuDisabled: true,
                dataIndex: 'description'
            }]
        });

        this.selectedEmployees.addPlugin({
            ptype: 'treelivesearch'
        });

        Ext.apply(this, {
            title: BC.translate('selectedEmployees'),
            items: [this.selectedEmployees],
            buttons: [
                this.okBtn,
                this.cancelBtn
            ]
        });

        this.callParent();
    }
});
