Ext.define('BC.view.MainPanel', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.mainPanel',

    activeItem: 0,
    lastActiveItem: {
        index: 0,
        activate: false
    },
    tabBar: {
        height: 25
    },

    margins: '0 0 0 0',

    cls: 'mainPanel',

    requires: ['BC.view.config.ConfigPanel', 'BC.store.report.Standby'],

    initComponent: function() {
        this.items = [{
            xtype: 'profileList',
            hidden: true
        }, {
            xtype: 'configPanel',
            hidden: true
        }];

        this.id = 'bcMainPanel';

        this.callParent(arguments);
    },

    afterRender: function() {
        this.callParent(arguments);

        Ext.core.DomHelper.append('bcMainPanel',
                '<div id="fakeTabs">' +
                    '<div id="fakeTab1"></div>' +
                    '<div id="fakeTab2"></div>' +
                '</div>');

        this.tabProfiles = Ext.create('Ext.Button', {
            id: 'fakeTabProfiles',
            text: BC.translate('savedProfiles'),
            iconCls: 'savedProfilesIcon',
            renderTo: Ext.getDom('fakeTab1'),
            cls: 'x-tab x-tab-default x-top x-tab-top x-tab-default-top x-active x-tab-active x-tab-default-active x-top-active x-tab-top-active x-tab-default-top-active',
            padding: '3 2 0 2',
            handler: function() {
                this.fireEvent('savedProfiles');
                if (Ext.isIE8) {
                    this.tabProfiles.doComponentLayout();
                    this.tabConfig.doComponentLayout();
                }
            },
            scope: this
        });

        this.tabConfig = Ext.create('Ext.Button', {
            id: 'fakeTabConfig',
            text: BC.translate('config'),
            iconCls: 'configIcon',
            renderTo: Ext.getDom('fakeTab2'),
            cls: 'x-tab x-tab-default x-top x-tab-top x-tab-default-top x-item-disabled x-disabled x-tab-disabled x-tab-default-disabled x-top-disabled x-tab-top-disabled x-tab-default-top-disabled',
            disabled: true,
            padding: '3 2 0 2',
            handler: function() {
                this.fireEvent('configuration');
                if (Ext.isIE8) {
                    this.tabProfiles.doComponentLayout();
                    this.tabConfig.doComponentLayout();
                }
            },
            scope: this
        });
    }
});
