Ext.define('BC.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires: ['Ext.layout.container.Border', 'BC.view.Header', 'BC.view.MainPanel', 'BC.lib.Override'],

    layout: 'border',

    items: [{
        region: 'center',
        xtype: 'mainPanel'
    }, {
        region: 'north',
        height: 40,
        xtype: 'bcHeader'
    }],

    initComponent: function() {
        var runner = new Ext.util.TaskRunner();

        runner.start({
            run: function () {
                Ext.Ajax.request({url: 'index/timeoutcheck'});
            },
            interval: 15 * 60 * 1000 // = 15 minutes
        });

        Ext.Ajax.timeout = 300000;
        Ext.Ajax.on({
            'requestexception': function(conn, response) {
                switch (response.status) {
                    case 403:
                        runner.stopAll();

                        var resp = Ext.decode(response.responseText, true);
                        if (resp) {
                            switch (resp.code) {
                                case 'missingchecksum':
                                    Ext.Msg.alert(
                                        'Error',
                                        resp.message,
                                        function() {
                                            window.location.reload();
                                        }
                                    );
                                    break;
                            }
                            break;
                        }

                        Ext.Msg.alert(
                            BC.translate('sessionDialogTitle'),
                            BC.translate('sessionDialogText'),
                            function() {
                                window.location.reload();
                            }
                        );
                        break;
                }
            }
        });

        if (global.prevUser.checksum) {
            Ext.Ajax.on({
                'beforerequest': function(conn, options) {
                    switch (typeof options.params) {
                        case "string":
                            options.params += "&checksum=" + global.prevUser.checksum;
                            break;
                        case "undefined":
                            options.params = {};
                        case "object":
                            options.params.checksum = global.prevUser.checksum;
                            break;
                    }
                }
            });
        }

        BC.translate = function(desc, modifications) {
            return Ext.getStore('Translations').translation(desc, modifications);
        };

        BC.sortList = function(id, col, timestamp, profileId) {
            var list = Ext.getCmp(id);
            list.sortList(col, timestamp, profileId);
        };

        BC.translateReport = function(abbr) {
            var reportName;

            switch (abbr) {
                case 'D':
                    reportName = 'diagramm';
                    break;
                case 'T':
                    reportName = 'tabelle';
                    break;
                case 'L':
                    reportName = 'liste';
                    break;
                case 'E':
                    reportName = 'notBooked';
                    break;
                case 'B':
                    reportName = 'bookingReport';
                    break;
                case 'A':
                    reportName = 'accountingReport';
                    break;
                case 'R':
                    reportName = 'rebookReport';
                    break;
                case 'P':
                    reportName = 'permissionReport';
                    break;
                case 'M':
                    reportName = 'monthEndReport';
                    break;
                case 'N':
                    reportName = 'btModifications';
                    break;
                case 'S':
                    reportName = 'sapKeyHours';
                    break;
                case 'O':
                    reportName = 'rebookLog';
                    break;
                case 'F':
                    reportName = 'btOrder';
                    break;
                case 'H':
                    reportName = 'standby';
                    break;
                case 'C':
                    reportName = 'absenceCalendar';
                    break;
                case 'I':
                    reportName = 'misDivision';
                    break;
                case 'J':
                    reportName = 'misOverall';
                    break;
                case 'K':
                    reportName = 'projectStatus';
                    break;
                case 'G':
                    reportName = 'accountableHours';
                    break;
                case 'V':
                    reportName = 'orderOverview';
                    break;
                case 'Q':
                    reportName = 'contingentRange';
                    break;
                case 'Z':
                    reportName = 'contingentData';
                    break;
                case 'X':
                    reportName = 'contingentSapInvoiceComparison';
                    break;
                case 'Y':
                    reportName = 'prismaValues';
                    break;
            }
            return BC.translate(reportName);
        };

        this.callParent(arguments);
    }
});
