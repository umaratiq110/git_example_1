/**
 * View for ContingentManagement - SAP Invoice comparison
 */
Ext.define('BC.view.report.ContingentSapInvoiceComparison', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.contingentSapInvoiceComparison',

    mixins: ['BC.view.report.Abstract'],

    autoScroll: true,

    initComponent: function() {
        Ext.apply(this, {
            autoScroll: true
        });

        this.callParent(arguments);
    },

    init: function(result) {
        this.update(result.view);
    }
});
