Ext.define('BC.view.report.Table', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.table',

    mixins: ['BC.view.report.Abstract'],

    cls: 'report-table',
    cssPrefix: 'rt-',

    initComponent: function() {
        this.tbAlbtn = new Ext.Button({
            action: 'toggleAlHours',
            iconCls: 'power-off-ico',
            enableToggle: true,
            toggleHandler: function(btn, state) {
                if (state) {
                    this.setIconCls('power-on-ico');
                } else {
                    this.setIconCls('power-off-ico');
                }
            },
            text: BC.translate('showhidealhours') //AL-Hours on/off'
        });

        this.tbPercentbtn = new Ext.Button({
            enableToggle: true,
            iconCls: 'power-off-ico',
            action: 'togglePercent',
            toggleHandler: function(btn, state) {
                if (state) {
                    this.setIconCls('power-on-ico');
                } else {
                    this.setIconCls('power-off-ico');
                }
            },
            text: BC.translate('showhidepercent')// '%-partition on/off'
        });

        this.tbTargetHoursbtn = new Ext.Button({
            action: 'toggleTargetHours',
            iconCls: 'power-off-ico',
            enableToggle: true,
            toggleHandler: function(btn, state) {
                if (state) {
                    this.setIconCls('power-on-ico');
                } else {
                    this.setIconCls('power-off-ico');
                }
            },
            text: BC.translate('showHideTargetHours') //'Target-Hours on/off'
        });

        this.tbSAPKeybtn = new Ext.Button({
            action: 'toggleSAPKey',
            iconCls: 'power-off-ico',
            enableToggle: true,
            toggleHandler: function(btn, state) {
                if (state) {
                    this.setIconCls('power-on-ico');
                } else {
                    this.setIconCls('power-off-ico');
                }
            },
            text: BC.translate('showHideSAPKey') //'SAPKey on/off'
        });

        this.tbExcelbtn = new Ext.Button({
            iconCls: 'excel-ico',
            action: 'downloadExcel',
            text: BC.translate('export') + '-' + BC.translate('excel')

        });

        this.store = Ext.create('BC.store.report.Table');

        this.tableTpl = new Ext.XTemplate(
            '<table class="' + this.cssPrefix + 'table" border="0" cellspacing="0" cellpadding="0">',
                '<tbody>',
                '<tpl for=".">',
                    '<tpl if="isHeader">',
                    '<tr class="' + this.cssPrefix + 'header-row">',
                        '<th class="{[this.headerCls(1)]}">{name:htmlEncode}</th>',
                        '<th class="{[this.headerCls(2)]}">{sapKey}</th>',
                        '<th class="{[this.headerCls(3)]}">{total}</th>',
                        '<th class="{[this.headerCls(4)]}">{targetEffort}</th>',
                        '<th class="{[this.headerCls(5)]}">{percent}</th>',
                        '<tpl for="xAxisData">',
                            '<th data-qtip="{tooltip}">{name}</th>',
                          '</tpl>',
                    '</tr>',
                    '</tpl>',
                    '<tpl if="false === isHeader">',
                    '<tr class="' + this.cssPrefix + 'row">',
                        '<td class="{[this.cls(1)]}">{name:htmlEncode}</td>',
                        '<td class="{[this.cls(2)]}">{sapKey}</td>',
                           '<td class="{[this.cls(3)]}">{[this.renderHours(values.total)]}</td>',
                        '<td class="{[this.cls(4)]}">{targetEffort}</td>',
                        '<td class="{[this.cls(5)]}">{percent}</td>',
                        '<tpl for="xAxisData">',
                            '<td class="{[this.cls(0, parent.isTotal)]}">{[this.renderHours(values)]}</td>',
                          '</tpl>',
                    '</tr>',
                    '</tpl>',
                '</tpl>',
                '</tbody>',
            '</table>',
            {
                compiled: true,
                hourCellTpl: new Ext.XTemplate(
                    '<tpl if="false === this.isEmpty(values.hours, values.hoursAL)">',
                    '{hours}<br />',
                    '<div class="hours-al ' + Ext.baseCSSPrefix + 'hide-display" style="{[this.hourStyle(values.hours, values.hoursAL, values.isTotal)]}">AL:{hoursAL}</div>',
                    '</tpl>',
                    {
                        compiled: true,
                        isEmpty: function(hours, hoursAL) {
                            return parseFloat(hours) === 0 && parseFloat(hoursAL) === 0;
                        },
                        hourStyle: function(hours, hoursAL, isTotal) {
                            var style = "background-color: ";
                            if (parseFloat(hoursAL) === 0) {
                                style += "#FF5E5E;";
                            } else if (parseFloat(hours) > parseFloat(hoursAL)) {
                                style += "#FFFF00;";
                            } else if (isTotal) {
                                style += "#EEEEEE;";
                            } else {
                                style += "#FFFFFF;";
                            }
                            return style;
                        }
                    }
                ),
                renderHours: function(values) {
                    values.hours = parseFloat(values.hours);
                    values.hoursAL = parseFloat(values.hoursAL);
                    values.hours = values.hours.toFixed(2);
                    values.hoursAL = values.hoursAL.toFixed(2);

                    return this.hourCellTpl.apply(values);
                },
                headerCls: function(col) {
                    var cls = [].concat(this.toggleCls(col, true).split(" "));

                    if (col === 1) {
                        cls.push("unit");
                    }

                    return cls.join(" ");
                },
                cls: function(col, isTotal) {
                    var cls = [].concat(this.toggleCls(col, false).split(" "));

                    if (col === 1) {
                        cls.push("first");
                        cls.push("grey");
                    }

                    if (isTotal) {
                        cls.push("light-grey");
                    }

                    return cls.join(" ");
                },
                toggleCls: function(col, headerRow) {
                    var cls = [];
                    if (col >= 2 && col <= 5) {
                        if (headerRow) {
                            cls.push("grey");
                        } else {
                            cls.push("light-grey");
                        }

                        switch (col) {
                            case 2:
                                cls.push("sap-key");
                                break;
                            case 4:
                                cls.push("target-effort");
                                break;
                            case 5:
                                cls.push("percent");
                                break;
                        }

                        if (col !== 3) {
                            cls.push(Ext.baseCSSPrefix + 'hide-display');
                        }
                    }

                    return cls.join(" ");
                }
            }
        );

        this.tableView = Ext.create('Ext.view.View', {
            store: this.store,
            tpl: this.tableTpl,
            itemSelector: 'tr',
            autoScroll: true,
            border: false,
            layout: 'fit'
        });

        Ext.apply(this, {
            items: [this.tableView],
            tbar: [this.tbAlbtn, this.tbTargetHoursbtn, this.tbPercentbtn, this.tbSAPKeybtn, this.tbExcelbtn]
        });

        this.callParent(arguments);
    },

    init: function(result) {
        this.store.getProxy().data = result.data;
        this.store.load();
    }
});
