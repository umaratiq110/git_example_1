Ext.define('BC.view.report.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.bookinglist',

    mixins: ['BC.view.report.Abstract'],

    cls: 'reportListPanel',

    initComponent: function() {
        // additional columns
        var additionalCols = this.record.get('additionalCols'),
            additionalColsStore = Ext.getStore('AdditionalCols'),
            onlyRemoteDaysCheckBox = Ext.getCmp('listOptionOnlyRemoteWorkDays'),
            remoteWorkReport = onlyRemoteDaysCheckBox !== undefined && onlyRemoteDaysCheckBox.getValue(),
            noteTitle = remoteWorkReport ? 'remoteWorkMarker' : 'notiz';

        this.searchField = Ext.create('Ext.form.field.Text');

        var filterOptions = [{
            "id": "alle",
            "name": BC.translate('alle')
        }, {
            "id": "companyName",
            additionalCol: 13
        }, {
            "id": "divisionName",
            additionalCol: 14
        }, {
            "id": "employee",
            "name": BC.translate('mitarbeiter')
        }];

        if (!remoteWorkReport) {
            filterOptions.push({
                "id": "costcentre",
                "name": BC.translate('abkbkz')
            });
        }

        filterOptions.push({
            "id": "note",
            "name": BC.translate(noteTitle)
        });

        if (!remoteWorkReport) {
            filterOptions.push({
                "id": "bookingType",
                "name": BC.translate('bookingType')
            });
        }

        this.filterColumnStore = Ext.create('Ext.data.Store', {
            fields: [
                {name: 'id', type: 'string'},
                {name: 'name', type: 'string'},
                {name: 'additionalCol', type: 'int', useNull: true}
            ],
            data: filterOptions
        });

        Ext.each(this.filterColumnStore.getRange(), function(rec) {
            var colId = rec.get('additionalCol');
            if (!colId) {
                return;
            }
            if (Ext.Array.contains(additionalCols, colId)) {
                rec.set('name', additionalColsStore.getById(colId).get('name'));
                return;
            }
            this.remove(rec);
        }, this.filterColumnStore);

        this.filterColumnCombo = Ext.create('Ext.form.field.ComboBox', {
            store: this.filterColumnStore,
            queryMode: 'local',
            displayField: 'name',
            valueField: 'id',
            value: 'alle',
            editable: false
        });

        this.searchButton = Ext.create('Ext.button.Button', {
            text: BC.translate('suche'),
            icon: 'files/images/search.png',
            action: 'filterList'
        });

        if (remoteWorkReport) {
            this.exportButton = new Ext.Button({
                iconCls: 'excel-ico',
                action: 'downloadExcelRemoteWorkReport',
                text: BC.translate('export') + '-' + BC.translate('excel')
            });
        } else {
            this.exportButton = Ext.create('Ext.button.Button', {
                text: BC.translate('export'),
                icon: 'files/images/export.png',
                menu: {
                    xtype: 'menu',
                    plain: true,
                    items: [{
                        itemId: 'list-export-excel',
                        text: BC.translate('exp_liste_excel'),
                        iconCls: 'excel-ico'
                    }, {
                        itemId: 'list-export-pdbinfo-excel',
                        text: BC.translate('exp_info_pdb_excel'),
                        iconCls: 'excel-ico'
                    }]
                }
            });
        }

        this.statusTextSum = Ext.create('Ext.toolbar.TextItem', {
            width: 200,
            text: remoteWorkReport ? '' : Ext.String.format(BC.translate('sumListEntries'), '-')
        });

        var countTextKey = remoteWorkReport ? 'foundRemoteWorkDays' : 'foundListEntries';

        this.statusTextCount = Ext.create('Ext.toolbar.TextItem', {
            width: 200,
            text: Ext.String.format(BC.translate(countTextKey), '-'),
            style: 'text-align:right'
        });

        // setup columns
        var columns = [{
            dataIndex: 'companyName',
            width: 55,
            menuDisabled: true,
            additionalCol: 13
        }, {
            dataIndex: 'divisionName',
            width: 150,
            menuDisabled: true,
            additionalCol: 14
        }];

        if (!remoteWorkReport) {
            columns.push({
                header: BC.translate('fromDate'),
                dataIndex: 'presenceFrom',
                width: 50,
                menuDisabled: true,
                renderer: Ext.util.Format.dateRenderer('H:i'),
                additionalCol: 18
            }, {
                header: BC.translate('toDate'),
                dataIndex: 'presenceTo',
                width: 50,
                menuDisabled: true,
                renderer: Ext.util.Format.dateRenderer('H:i'),
                additionalCol: 18
            });
        }

        columns.push({
            header: BC.translate('datum'),
            dataIndex: 'date',
            width: 75,
            menuDisabled: true,
            renderer: Ext.util.Format.dateRenderer('d.m.Y')
        }, {
            dataIndex: 'dayType',
            width: 75,
            menuDisabled: true,
            renderer: function(value) {
                return Ext.util.Format.htmlEncode(BC.translate('dayType_' + value));
            },
            additionalCol: 16
        }, {
            header: BC.translate('employeeShortcut'),
            dataIndex: 'employee',
            width: 95,
            menuDisabled: true
        }, {
            width: 100,
            menuDisabled: true,
            renderer: function(val, meta, rec) {
                return rec.get('firstName') + ' ' + rec.get('lastName');
            },
            additionalCol: 15
        });

        if (!remoteWorkReport) {
            columns.push({
                header: BC.translate('abkbkz'),
                dataIndex: 'costcentre',
                width: 200,
                menuDisabled: true
            }, {
                header: Ext.String.capitalize(BC.translate('stunden')),
                dataIndex: 'hours',
                align: 'right',
                width: 60,
                menuDisabled: true,
                renderer: Ext.util.Format.numberRenderer('0.00')
            }, {
                header: BC.translate('bookingType'),
                dataIndex: 'bookingType',
                width: 75,
                menuDisabled: true
            }, {
                header: BC.translate('begin'),
                dataIndex: 'bookingBegin',
                width: 55,
                menuDisabled: true,
                renderer: Ext.util.Format.dateRenderer('H:i'),
                additionalCol: 17
            }, {
                header: BC.translate('end'),
                dataIndex: 'bookingEnd',
                width: 55,
                menuDisabled: true,
                renderer: Ext.util.Format.dateRenderer('H:i'),
                additionalCol: 17
            }, {
                header: BC.translate('break'),
                dataIndex: 'break',
                width: 55,
                menuDisabled: true,
                renderer: Ext.util.Format.numberRenderer('0.00'),
                additionalCol: 22
            });
        }

        columns.push({
            header: BC.translate(noteTitle),
            dataIndex: 'note',
            flex: 1,
            menuDisabled: true
        });

        if (!remoteWorkReport) {
            columns.push({
                header: BC.translate('meldungsids'),
                dataIndex: 'pdbIds',
                width: 100,
                menuDisabled: true,
                renderer: this.renderJiraIds
            });
        }

        for (var i = 0; i < columns.length; ++i) {
            var colId = columns[i].additionalCol;
            if (!colId) {
                continue;
            }

            if (Ext.Array.contains(additionalCols, colId)) {
                if (!columns[i].header) {
                    columns[i].header = additionalColsStore.getById(colId).get('name');
                }
                continue;
            }
            columns.splice(i--, 1);
        }

        Ext.apply(this, {
            layout: 'fit',
            store: Ext.create('BC.store.report.ListEntries'),
            columns: columns,
            tbar: [
                BC.translate('suche') + ': ', this.searchField, ' ',
                BC.translate('in') + ' ' + BC.translate('spalte') + ': ', this.filterColumnCombo, ' ',
                this.searchButton, '->',
                this.exportButton
            ],
            bbar: [this.statusTextSum, '->', this.statusTextCount],
            enableColumnMove: false,
            disableSelection: true,
            viewConfig: {
                enableTextSelection: true,
                loadMask: false
            },
            verticalScroller: {
                trailingBufferZone: 150,
                leadingBufferZone: 150
            }
        });

        this.callParent(arguments);
    },

    initEvents: function() {
        this.callParent();

        this.getStore().on({
            'refresh': function() {
                this.updateStatusBar();
            },
            scope: this
        });
    },

    init: function(result) {
        var store = this.getStore();

        store.getProxy().data = result.liste;
        store.load();
    },

    renderJiraIds: function(val) {
        if (!Ext.isArray(val)) {
            return '';
        }

        var urls = [];
        for (var i = 0; i < val.length; i++) {
            urls.push(Ext.String.format(
                '<a class="report-jiraid" id="{1}_{0}" href="{2}/browse/{0}" target="_blank">{0}</a>',
                val[i],
                Ext.id(),
                global.jiraUrl
            ));
        }
        return urls.join(', ');
    },

    updateStatusBar: function() {
        var data = this.getStore().data.map[1], sum = 0,
            onlyRemoteDaysCheckBox = Ext.getCmp('listOptionOnlyRemoteWorkDays'),
            remoteWorkReport = onlyRemoteDaysCheckBox !== undefined && onlyRemoteDaysCheckBox.getValue(),
            amountTextKey = remoteWorkReport ? 'foundRemoteWorkDays' : 'foundListEntries';

        if (data) {
            Ext.each(data.value, function(rec) {
                sum += rec.data.hours;
            });
        }

        if (!remoteWorkReport) {
            this.statusTextSum.update(
                Ext.String.format(BC.translate('sumListEntries'), Ext.util.Format.number(sum, '0.00'))
            );
        }

        this.statusTextCount.update(
            Ext.String.format(BC.translate(amountTextKey), this.getStore().totalCount)
        );
    }
});
