Ext.define('BC.view.report.Summary', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.reportSummary',

    region: 'north',
    layout: 'fit',
    margin: '5 10',
    border: false,

    cls: 'report-summary',

    record: null,

    initComponent: function() {
        this.summaryTpl = new Ext.XTemplate(
            '<tpl for=".">',
                '<table class="report-summary-table" id="summary-table" >',
                    '<tr>',
                        '<tpl if="this.supportsPeriod(type)">',
                            '<th>' + BC.translate('zeitraumal') + '</th>',
                        '</tpl>',
                        '<tpl if="this.hasNote(type, hoursNote)">',
                            '<th>' + BC.translate('bc_notiztext') + '</th>',
                        '</tpl>',
                        '<tpl if="this.supportsEmployees(type)">',
                            '<th class="SummaryEmployees" >' + BC.translate('employees') + '</th>',
                        '</tpl>',
                        '<tpl if="this.supportsNodes(type)">',
                            '<tpl if="type ==\'K\'">',
                                '<th>' + BC.translate('singleNodeStartPart') + '</th>',
                            '<tpl else>',
                                '<th>' + BC.translate('nodes') + '/' + BC.translate('sets') + '</th>',
                            '</tpl>',
                        '</tpl>',
                        '<tpl if="type == \'R\'">',
                            '<th>' + BC.translate('rebookToCC') + '</th>',
                            '<th>' + BC.translate('bookingType') + '</th>',
                        '</tpl>',
                        '<tpl if="type == \'B\' && values.displayOnlySubtrees == 1">',
                        '<th>' + BC.translate('bookingType') + '</th>',
                        '</tpl>',
                        '<tpl if="type == \'K\'">',
                            '<th>' + BC.translate('singleNodeDistinctRow') + '</th>',
                            '<th>' + BC.translate('configInformationToBeDisplayed') + '</th>',
                            '<th>' + BC.translate('configDisplayLines') + '</th>',
                            '<th>' + BC.translate('configLockedNodes') + '</th>',
                        '</tpl>',
                        '<tpl if="type == \'V\' && this.orderOverviewInformationToBeDisplayed(values)">',
                            '<th>' + BC.translate('displayedInfo') + '</th>',
                        '</tpl>',
                        '<tpl if="type == \'V\' && values.materialFilter.length != 0">',
                            '<th>' + BC.translate('filteredBy') + '</th>',
                        '</tpl>',
                    '</tr>',
                    '<tr>',
                        '<tpl if="this.supportsPeriod(type)">',
                            '<td>{dateFrom:defaultValue("--.--.----")} - {dateUntil:defaultValue("--.--.----")}</td>',
                        '</tpl>',
                        '<tpl if="this.hasNote(type, hoursNote)">',
                            '<td>{hoursNote}</td>',
                        '</tpl>',
                        '<tpl if="this.supportsEmployees(type)">',
                            '<td>',
                                '<div data-qtip="{[this.renderEmployees(values.monitoredUsers, true)]}">',
                                    '{[Ext.util.Format.defaultValue(this.renderEmployees(values.monitoredUsers), "---")]}',
                                '</div>',
                            '</td>',
                        '</tpl>',
                        '<tpl if="this.supportsNodes(type)">',
                            '<td>',
                                '<div data-qtip="{[this.renderNodes(values.nodes, true)]}">',
                                    '{[Ext.util.Format.defaultValue(this.renderNodes(values.nodes), "---")]}',
                                '</div>',
                            '</td>',
                        '</tpl>',
                        '<tpl if="type == \'R\'">',
                            '<td><div class="SummaryAutoOverflow" id="targetCC-{id}-{timestamp}">---</div></td>',
                        '</tpl>',
                        '<tpl if="type == \'K\'">',
                            '<td>',
                                '<div data-qtip="{[this.renderDistinctRow(values.nodes, true)]}">',
                                    '{[Ext.util.Format.defaultValue(this.renderDistinctRow(values.nodes), "---")]}',
                                '</div>',
                            '</td>',
                        '</tpl>',
                        '<tpl if="this.supportsBookingTypeOrInformation(type,values.displayOnlySubtrees)">',
                            '<td>{[this.renderBookingTypesOrInformation(values.bookingTypes)]}</td>',
                        '</tpl>',
                        '<tpl if="type == \'K\'">',
                            '<td>',
                                '<tpl if="displayLines == 0">',
                                    '<span data-qtip="' + BC.translate('onlyIfHoursAmountsInSelectedGeneralPeriod') + '">',
                                        BC.translate('onlyIfHoursAmountsInSelectedGeneralPeriod_short'),
                                    '</span>',
                                '</tpl>',
                                '<tpl if="displayLines == 1">',
                                    '<span data-qtip="' + BC.translate('onlyIfHoursAmountsInSelectedPeriod') + '">',
                                        BC.translate('onlyIfHoursAmountsInSelectedPeriod_short'),
                                    '</span>',
                                '</tpl>',
                                '<tpl if="displayLines == 2">',
                                    '<span data-qtip="' + BC.translate('onlyIfHoursInSelectedPeriod') + '">',
                                        BC.translate('onlyIfHoursInSelectedPeriod_short'),
                                    '</span>',
                                '</tpl>',
                            '</td>',
                        '</tpl>',
                        '<tpl if="type == \'K\'">',
                        '<td>',
                            '<tpl if="showWithLockedNodes == 1">',
                                 BC.translate('withLockedNodes'),
                            '<tpl else>',
                                BC.translate('withoutLockedNodes'),
                            '</tpl>',
                        '</td>',
                    '</tpl>',
                    '<tpl if="type == \'V\' && this.orderOverviewInformationToBeDisplayed(values)">',
                        '<td>',
                            '<div data-qtip="{[this.orderOverviewInformationToBeDisplayed(values, true)]}">',
                                '{[Ext.util.Format.defaultValue(this.orderOverviewInformationToBeDisplayed(values))]}',
                            '</div>',
                        '</td>',
                    '</tpl>',
                    '<tpl if="type == \'V\' && values.materialFilter.length != 0">',
                        '<td>',
                            '<div data-qtip="{[this.renderMaterialFilter(values, true)]}">',
                            '{[Ext.util.Format.defaultValue(this.renderMaterialFilter(values))]}',
                            '</div>',
                        '</td>',
                    '</tpl>',
                    '</tr>',
                '</table>',
            '</tpl>',
            {
                orderOverviewInformationToBeDisplayed: function(value, newLines) {
                    var translation = [];

                    if (value.showClosedOrders) {
                        translation.push(BC.translate('closedOrdersShort'));
                    }

                    if (value.showOrdersAfterPeriod) {
                        translation.push(BC.translate('ordersAfterPeriodShort'));
                    }

                    if (value.showWithoutOrdersAfterPeriod) {
                        translation.push(BC.translate('withoutOrdersAfterPeriodShort'));
                    }

                    if (value.showWithHigherLevelPosition) {
                        translation.push(BC.translate('withHigherLevelPositionShort'));
                    }

                    return translation.join(newLines === true ? "<br />\n" : '; ');
                },
                supportsPeriod: function(type) {
                    return /^[DTLEBRMNSOHCIJKVX]$/.test(type);
                },
                hasNote: function(type, note) {
                    return /^[DLBTR]$/.test(type) && !Ext.isEmpty(note);
                },
                supportsEmployees: function(type) {
                    return /^[DLTEBRMPSHC]$/.test(type);
                },
                supportsNodes: function(type) {
                    return /^[DLTBNARSOHIKV]$/.test(type);
                },
                supportsBookingTypeOrInformation: function(type, displayOnlySubtrees) {
                    return /^[RK]$/.test(type) || type === 'B' && displayOnlySubtrees === 1;
                },
                renderEmployees: function(data, newLines) {
                    var employees = data.employees,
                        groups = [],
                        output = [],
                        group;

                    if (!data) {
                        return '';
                    }

                    for (var i = 0; i < data.groups.length; i++) {
                        group = data.groups[i].name;
                        if (Ext.isArray(data.groups[i].members) && data.groups[i].members.length > 0) {
                            group += ' (' + data.groups[i].members.join(', ') + ')';
                        }
                        groups.push(group);
                    }
                    if (groups.length > 0) {
                        output.push(groups.join(newLines === true ? "<br />\n" : ', '));
                    }
                    if (employees.length > 0) {
                        output.push(employees.join(', '));
                    }
                    return output.join(newLines === true ? "<br />\n" : ' - ');
                },
                renderNodes: function(nodes, newLines) {
                    var values = [];

                    for (var i = 0; i < nodes.length; ++i) {
                        if (nodes[i].checked) {
                            values.push(nodes[i].name);
                        }
                    }
                    return values.join(newLines === true ? "<br />\n" : ', ');
                },
                renderBookingTypesOrInformation: function(value) {
                    var btStore = Ext.data.StoreManager.lookup('BookingTypes'),
                        btArr = [], record;

                    for (var i = 0; i < value.length; i++) {
                        if (value[i] === 0) {
                            btArr.push(BC.translate('noBookingType'));
                        } else {
                            record = btStore.findRecord('id', value[i]);
                            if (record) {
                                btArr.push(record.get('name'));
                            }
                        }
                    }
                    return btArr.join(',');
                },
                renderDistinctRow: function(value, newLines) {
                    var singleNodesArr = [],
                        node;

                    for (var i = 0; i < value.length; i++) {
                        node = "";
                        if ((value[i].singleNodeTypes & BC.store.SingleNodeTypes.STATUS_DISTINCTROW) > 0) {
                            node += value[i].name;
                        }
                        if ((value[i].singleNodeTypes & BC.store.SingleNodeTypes.STATUS_WITHNEXTLEVEL) > 0) {
                            node += ' (' + BC.translate('singleNodeAtNextLevel') + ')';
                        }
                        if (node) {
                            singleNodesArr.push(node);
                        }
                    }
                    return singleNodesArr.join(newLines === true ? "<br />\n" : ', ');
                },
                renderMaterialFilter: function(value, newLines) {
                    let materialFilterIds = value.materialFilter,
                        materialStore = Ext.getStore('MaterialFilter'),
                        materialFilterNames = [];
                    if (!materialFilterIds || materialFilterIds.length < 1) {
                        return false;
                    }

                    Ext.Array.each(materialFilterIds, function(id) {
                        let rec = materialStore.getById(id);
                        materialFilterNames.push(rec.get('name'));
                    });

                    return materialFilterNames.join(newLines === true ? "<br />\n" : ', ');
                }
            }
        );

        this.summaryPanel = Ext.create('Ext.panel.Panel', {
            layout: 'fit',
            height: 50,
            border: false,
            tpl: this.summaryTpl,
            data: this.record.data,
            dockedItems: [{
                dock: 'right',
                border: false,
                items: [{
                    xtype: 'button',
                    text: BC.translate('showAppropriateConfig'),
                    action: 'showConfigButton',
                    iconCls: 'configIcon'
                }]
            }]
        });

        this.items = [{
            xtype: 'fieldset',
            height: 80,
            collapsible: true,
            cls: 'report-summary-fieldset',
            title: BC.translateReport(this.record.data.type) + ' - ' + BC.translate('summary'),
            items: [this.summaryPanel]
        }];

        this.callParent(arguments);
    }
});
