Ext.define('BC.view.report.Abstract', {
    region: 'center',
    layout: 'fit',
    margin: "5 10",

    // abstract method
    init: Ext.emptyFn
});
