Ext.define('BC.view.report.Rebook', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.rebook',

    mixins: ['BC.view.report.Abstract'],
    uses: [
        'BC.view.report.rebook.CostCentreCombo',
        'BC.view.report.rebook.RequestMailDialog',
        'BC.lib.HourField',
        'Ext.grid.plugin.CellEditing'
    ],

    cls: 'rebook',

    holidayRebooking: false,
    targetCC: null,

    initComponent: function() {
        // Create booking types fields in model and as grid columns
        var rebookModel = Ext.ModelManager.getModel('BC.model.report.RebookEntry'),
            gridBt = [],
            bt,
            noEditIfHoliday = function(val, metaData, record) {
                //If CC of record is a holiday CC then no edit is possible
                metaData.tdCls = this.isHolidayCC(record.data.isHoliday) ? 'grid-cell-grey' : '';

                return val !== 0 ? val : '';
            };

        this.storeBookingTypes = Ext.data.StoreManager.lookup('BookingTypes');
        bt = this.storeBookingTypes.getRange();

        for (var i = 0; i < bt.length; ++i) {
            rebookModel.prototype.fields.add(new Ext.data.Field({
                name: 'bookingType_' + bt[i].get('name'),
                type: 'float',
                persist: false
            }));

            gridBt.push({
                text: bt[i].get('name'),
                width: 50,
                dataIndex: 'bookingType_' + bt[i].get('name'),
                tooltip: bt[i].get('longName'),
                editor: {
                    xtype: 'hourfield',
                    selectOnFocus: true
                },
                renderer: noEditIfHoliday,
                scope: this,
                sortable: true,
                menuDisabled: true,
                tdCls: 'grid-cell-yellow',
                align: 'right'
            });
        }

        this.statusTextSum = Ext.create('Ext.toolbar.TextItem', {
            width: 250,
            text: Ext.String.format(BC.translate('sumListEntries'), '-')
        });

        this.statusTextBookingTypeSum = Ext.create('Ext.toolbar.TextItem', {
            width: 300,
            text: Ext.String.format(BC.translate('sumBookingTypeEntries'), '', '', ''),
            style: 'text-align:right'
        });

        this.cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        });

        this.comboRebookingRequester = Ext.create('Ext.form.ComboBox', {
            fieldLabel: BC.translate('rebookingRequestor'),
            labelWidth: 170,
            flex: 1,
            margin: '0 30 0 0',
            store: 'PrismaEmployeesAll',
            queryMode: 'local',
            displayField: 'NAME',
            valueField: 'ID',
            typeAhead: true,
            forceSelection: true
        });

        this.comboTargetCc = Ext.create('BC.view.report.rebook.CostCentreCombo', {
            fieldLabel: BC.translate('targetCC'),
            labelWidth: 75,
            flex: 1,
            margin: '0 30 0 0',
            store: 'CostCentres'
        });

        this.textFieldRebookingReason = Ext.create('Ext.form.TextField', {
            fieldLabel: BC.translate('rebookingReason'),
            labelWidth: 170,
            flex: 1,
            margin: '0 30 0 0'
        });

        this.btnRebookAllLines = Ext.create('Ext.button.Button', {
            text: BC.translate('rebookAllLines'),
            action: 'rebookAllLines',
            width: 220,
            hidden: true
        });

        this.actionCt = Ext.create('Ext.container.Container', {
            border: false,
            cls: 'rebookAction',
            layout: {
                type: 'fit'
            },
            items: [{
                layout: 'hbox',
                margin: '0 0 10 0',
                items: [
                    this.comboRebookingRequester,
                    this.comboTargetCc,
                    {
                        xtype: 'container',
                        width: 220,
                        items: [this.btnRebookAllLines]
                    }
                ]
            }, {
                layout: 'hbox',
                items: [
                    this.textFieldRebookingReason,
                    {
                        xtype: 'button',
                        text: BC.translate('sendRebookingRequestMail'),
                        width: 220,
                        margin: '0 30 0 0',
                        action: 'sendRebookingRequestMail'
                    }, {
                        xtype: 'button',
                        text: BC.translate('performRebooking'),
                        width: 220,
                        action: 'performRebooking'
                    }
                ]
            }]
        });

        Ext.apply(this, {
            cls: 'rebook-grid',
            enableColumnMove: false,
            columnLines: true,
            store: Ext.create('BC.store.report.RebookEntries'),
            viewConfig: {
                overItemCls: '',
                trackOver: false,
                stripeRows: true
            },
            plugins: [
                this.cellEditing
            ],
            columns: Ext.Array.merge([{
                text: BC.translate('datum'),
                width: 110,
                sortable: true,
                menuDisabled: true,
                dataIndex: 'date',
                align: 'center',
                renderer: function(value, meta, record) {
                    var result = Ext.util.Format.dateRenderer('d.M.Y')(value);
                    if (record.get('canOnlyRebookedByFinance')) {
                        result += "<span class='rebook-icon before-accounting-date-icon' data-qtip=\"" + BC.translate('beforeAccountingTooltip') + "\"></span>";
                    }
                    if (record.get('isAfterLockOfTargetCC')) {
                        result += "<span class='rebook-icon locked-icon' data-qtip=\"" + BC.translate('targetCCIsLockedTooltip') + "\"></span>";
                    }
                    return result;
                }
            }, {
                text: BC.translate('mitarbeiter'),
                width: 80,
                sortable: true,
                menuDisabled: true,
                dataIndex: 'employee',
                align: 'center'
            }, {
                text: BC.translate('bkz'),
                flex: 2,
                sortable: true,
                menuDisabled: true,
                dataIndex: 'cc',
                align: 'left'
            }, {
                text: BC.translate('stunden', 'ucfirst'),
                width: 80,
                sortable: true,
                menuDisabled: true,
                dataIndex: 'hours',
                align: 'right'
            }, {
                text: BC.translate('bookingType'),
                width: 100,
                sortable: true,
                menuDisabled: true,
                dataIndex: 'bookingType',
                align: 'center'
            }, {
                text: BC.translate('notiz'),
                flex: 2,
                dataIndex: 'note',
                sortable: true,
                menuDisabled: true,
                align: 'left'
            }, {
                text: BC.translate('meldungsids'),
                dataIndex: 'pdbIds',
                width: 100,
                menuDisabled: true,
                renderer: this.renderJiraIds
            }], gridBt, [{
                text: BC.translate('newNote'),
                flex: 1,
                dataIndex: 'newNote',
                sortable: true,
                menuDisabled: true,
                align: 'left',
                editor: {
                    xtype: 'textfield',
                    selectOnFocus: true
                },
                renderer: function(val, metaData, record) {
                    //If CC of record is a holiday CC then no edit is possible
                    metaData.tdCls = this.isHolidayCC(record.data.isHoliday) ? 'grid-cell-grey' : '';

                    return val !== 0 ? val : '';
                },
                tdCls: 'grid-cell-yellow'
            }, {
                xtype: 'actioncolumn',
                width: 50,
                align: 'center',
                hidden: true,
                sortable: false,
                menuDisabled: true,
                items: [{
                    icon: 'files/images/arrow-return-270.png',
                    tooltip: BC.translate('setHoursAtSameBookingType'),
                    getClass: function(value, meta, record) {
                        if (this.isHolidayCC(record.data.isHoliday)) {
                            return 'x-item-disabled';
                        }
                        return '';
                    },
                    handler: this.setHoursAtSameBookingType,
                    scope: this
                }]
            }]),
            dockedItems: [{
                xtype: 'panel',
                dock: 'bottom',
                bodyPadding: '10 5 5 5',
                border: '0 1 1 1',
                items: [
                    this.actionCt
                ]
            }, {
                xtype: 'toolbar',
                dock: 'bottom',
                border: '1 1 0 1',
                items: [this.statusTextSum, '->', this.statusTextBookingTypeSum]
            }]
        });

        this.callParent(arguments);
    },

    initEvents: function () {
        this.callParent(arguments);

        this.cellEditing.on({
            'beforeedit': function (editor, ctx) {
                //If CC of record is a holiday CC then no edit is possible
                if (this.isHolidayCC(ctx.record.data.isHoliday) === true) {
                    return false;
                }
            },
            scope: this
        });

        this.comboTargetCc.on({
            'select': function (field, records) {
                var record = Ext.isArray(records) ? records[0] : records,
                    bookingType = this.storeBookingTypes.getById(record.get('bookingTypeId')),
                    targetCc = {
                        id: record.get('id').toString(),
                        name: record.get('name'),
                        bookingType: bookingType.get('name'),
                        lastAccountingDate: record.get('lastAccountingDate'),
                        lockDate: record.get('lockDate')
                    };

                this.setTargetCc(targetCc);
                this.calculateLockDateOfBookings(targetCc);
            },
            scope: this
        });
    },

    init: function(result) {
        var targetCc = null;

        if (Ext.Object.getSize(result.summary) > 0 && Ext.Object.getSize(result.summary.targetCC) && result.summary.targetCC.name !== '') {
            targetCc = result.summary.targetCC;
            targetCc.lastAccountingDate = Ext.Date.parse(targetCc.lastAccountingDate, 'd.m.Y');
            targetCc.lockDate = Ext.Date.parse(targetCc.lockDate, 'd.m.Y');
        }
        this.setTargetCc(targetCc);

        this.initHolidayRebooking(result.bookings, this.targetCc ? this.targetCc.isHoliday : null);
        window.actionCt = this.actionCt;

        // speed up rendering by suspending layouts
        Ext.suspendLayouts();
        this.store.getProxy().data = result.bookings;
        this.store.load();
        Ext.resumeLayouts(true);
        this.calculateLockDateOfBookings(targetCc);

        this.comboRebookingRequester.clearValue();
        this.textFieldRebookingReason.reset();
    },

    renderJiraIds: function(val) {
        if (!Ext.isArray(val)) {
            return '';
        }

        var urls = [];
        for (var i = 0; i < val.length; i++) {
            urls.push(Ext.String.format(
                '<a class="report-jiraid" id="{1}_{0}" href="{2}/browse/{0}" target="_blank">{0}</a>',
                val[i],
                Ext.id(),
                global.jiraUrl
            ));
        }
        return urls.join(', ');
    },

    setTargetCc: function (targetCc) {
        var summaryEl = Ext.get('targetCC' + '-' + this.record.get('id') + '-' + this.timestamp),
            hasTargetCc = targetCc !== null,
            targetCcName = hasTargetCc ? targetCc.name : '---';

        this.targetCc = targetCc;

        if (summaryEl) {
            summaryEl.update(targetCcName);
            summaryEl.set({'data-qtip': targetCcName});
        }

        this.comboTargetCc.setValue(hasTargetCc ? parseInt(targetCc.id) : null);

        this.headerCt.down('actioncolumn').setVisible(hasTargetCc);
        this.btnRebookAllLines.setVisible(hasTargetCc);
    },

    calculateLockDateOfBookings: function(targetCc) {
        if (targetCc && targetCc.lockDate) {
            Ext.suspendLayouts();
            this.store.each(function(entry) {
                entry.set('isAfterLockOfTargetCC', entry.get('date').getTime() >= targetCc.lockDate.getTime());
            });
            Ext.resumeLayouts(true);
        } else {
            Ext.suspendLayouts();
            this.store.each(function(entry) {
                entry.set('isAfterLockOfTargetCC', false);
            });
            Ext.resumeLayouts(true);
        }
    },

    setHoursAtSameBookingType: function(view, rowIndex, colIndex, item, e, rec) {
        var targetCCBookingType,
            bookingTypes;

        if (this.isHolidayCC(rec.data.isHoliday)) {
            return;
        }
        targetCCBookingType = this.targetCc.bookingType;

        // determine booking types & values to be set
        bookingTypes = {};
        bookingTypes['bookingType_' + targetCCBookingType] = rec.get('hours');

        // set booking types
        rec.set(bookingTypes);
        if (Ext.isEmpty(rec.get('newNote'))) {
            rec.set('newNote', rec.get('note'));
        }
        rec.commit();

        this.updateStatusBar();
    },

    setHoursAtSameBookingTypeForAllLines: function() {
        var targetCCBookingType = 'bookingType_' + this.targetCc.bookingType,
            bookingTypes = {},
            range = this.store.getRange(),
            rec,
            i;

        Ext.suspendLayouts();

        if (this.holidayRebooking) {
            return;
        }

        for (i = 0; i < range.length; i++ ) {
            rec = range[i];
            if (Boolean(Number(rec.data.isHoliday))) {
                continue;
            }
            bookingTypes[targetCCBookingType] = rec.get('hours');
            rec.set(bookingTypes);

            if (Ext.isEmpty(rec.get('newNote'))) {
                rec.set('newNote', rec.get('note'));
            }
        }

        this.store.commitChanges();

        Ext.resumeLayouts();

        this.updateStatusBar();
    },

    updateStatusBar: function() {
        var store = this.getStore();

        this.statusTextSum.update(
            Ext.String.format(BC.translate('sumListEntries'), Ext.util.Format.number(store.sum('hours'), '0.00'))
        );

        var statusText = BC.translate("sumBookingTypeEntries") + " ";

        this.storeBookingTypes.each(function(type) {
            statusText += type.get('name') + ":" + Ext.util.Format.number(store.sum('bookingType_' + type.get('name')), '0.00') + " ";
        });

        this.statusTextBookingTypeSum.update(statusText);
    },

    validate: function() {
        var requestor = this.comboRebookingRequester.getValue() != null,
            reason = Ext.String.trim(this.textFieldRebookingReason.getValue()) !== '',
            msg = BC.translate('rebookingValidationFailed'),
            err = false,
            warn = null;

        // Checks grid hours rebooking
        Ext.Array.each(this.getStore().getRange(), function(rec, index) {
            var bt = {},
                sum = 0;

            this.storeBookingTypes.each(function(type) {
                var btName = "bookingType_" + type.get('name');
                bt[btName] = rec.get(btName);
                sum += rec.get(btName);
                if ((this.targetCc && this.targetCc.bookingType !== type.get('name') ||
                    !this.targetCc && rec.get('defaultBookingType') !== type.get('name'))
                    && rec.get(btName) > 0
                ) {
                    warn = "diffrentBookingType";
                }

            }, this);

            if (sum > 0 && rec.get('hours') < sum) {
                var row = index;
                Ext.Array.each(this.columns, function(column) {
                    var match = column.dataIndex ? column.dataIndex.match(/bookingType_.*/) : null;
                    if (column.dataIndex && match && bt[match[0]] > 0) {
                        var cell = this.getView().getCellByPosition({row: row, column: index});
                        cell.addCls('grid-cell-invalid');
                        Ext.QuickTips.register({
                            target: cell,
                            iconCls: 'x-icon-error',
                            text: BC.translate('rebookingInvalidHours')
                        });
                    }
                }, this);

                err = true;
            }
        }, this);

        if (!requestor || !reason || err) {
            Ext.MessageBox.show({
                buttons: Ext.MessageBox.OK,
                title: ' ',
                msg: msg,
                closable: false,
                animateTarget: this,
                icon: Ext.MessageBox.WARNING
            });

            if (!requestor) {
                this.comboRebookingRequester.markInvalid(BC.translate('fieldRequired'));
            }
            if (!reason) {
                this.textFieldRebookingReason.markInvalid(BC.translate('fieldRequired'));
            }

            return {
                valid: false,
                warning: null
            };
        }

        return {
            valid: true,
            warning: warn
        };
    },

    getRebookingRecords: function () {
        var bookings = [];

        this.getStore().each(function (record) {
            var bookingTypes = {},
                sum = 0;

            this.storeBookingTypes.each(function (type) {
                var value = record.get("bookingType_" + type.get('name'));
                bookingTypes[type.get('name')] = value;
                sum += value;
            });

            if (sum > 0 || record.get("newNote")) {
                bookings.push(Ext.apply(
                    {
                        id: record.get('bookingId'),
                        date: record.get('date'),
                        note: record.get('note'),
                        newNote: record.get('newNote'),
                        canOnlyRebookedByFinance: record.get('canOnlyRebookedByFinance'),
                        isAfterLockOfTargetCC: record.get('isAfterLockOfTargetCC')
                    },
                    bookingTypes
                ));
            }
        }, this);

        return bookings;
    },

    rebook: function() {
        var bookings = this.getRebookingRecords();

        if (bookings.length === 0) {
            return;
        }

        this.validateAccountingPeriod(bookings, Ext.Function.bind(function(bookings1) {
            this.validateTargetCC(bookings1, Ext.Function.bind(function(bookings2) {
                this.validateIfTargetCCIsLocked(bookings2, Ext.Function.bind(function(bookings3) {
                    this.validateForNotesWithoutHours(bookings3, Ext.Function.bind(this.doRequest, this));
                }, this));
            }, this));
        }, this));
    },

    validateForNotesWithoutHours: function(bookings, callback) {
        var valid = true;
        Ext.each(bookings, function(booking) {
            if (!Ext.isEmpty(booking.newNote)) {
                var sum = 0;
                Ext.each(['FP', 'CR', 'TM', 'MC', 'NA'], function(bookingType) {
                    sum += booking[bookingType];
                });
                if (sum === 0) {
                    Ext.MessageBox.show({
                        buttons: Ext.MessageBox.OK,
                        title: '',
                        msg: BC.translate('newNotesWithoutHours'),
                        closable: false,
                        animateTarget: this,
                        icon: Ext.MessageBox.WARNING,
                        scope: this
                    });
                    valid = false;
                    return false;
                }
            }
        });

        if (valid) {
            callback(bookings);
        }
    },

    validateAccountingPeriod: function(bookings, callback) {
        var allBookingsCanOnlyRebookedByFinance = Ext.Array.every(bookings, function(booking) {
            return !booking.canOnlyRebookedByFinance;
        });

        if (!allBookingsCanOnlyRebookedByFinance) {
            if (global.privileges.rebookAllBookings === true) {
                Ext.MessageBox.show({
                    buttons: Ext.MessageBox.YESNO,
                    title: '',
                    msg: BC.translate('allBookingsBeforeLastAccountingWarning'),
                    closable: false,
                    animateTarget: this,
                    icon: Ext.MessageBox.WARNING,
                    fn: function(btn) {
                        if (btn === 'yes') {
                            callback(bookings);
                        }
                    }
                });
            } else {
                Ext.MessageBox.show({
                    buttons: Ext.MessageBox.OK,
                    title: '',
                    msg: BC.translate('allBookingsBeforeLastAccountingInfo'),
                    closable: false,
                    animateTarget: this,
                    icon: Ext.MessageBox.WARNING
                });
            }
        } else {
            callback(bookings);
        }
    },

    validateIfTargetCCIsLocked: function(bookings, callback) {
        if (this.targetCc) {
            var isAnyBookingAfterLockOfTargetCC = false;
            Ext.each(bookings, function(booking) {
                if (booking.isAfterLockOfTargetCC) {
                    isAnyBookingAfterLockOfTargetCC = true;
                    return false;
                }
            }, this);
            if (isAnyBookingAfterLockOfTargetCC) {
                if (global.privileges.rebookAllBookings === true) {
                    Ext.MessageBox.show({
                        buttons: Ext.MessageBox.YESNO,
                        title: '',
                        msg: BC.translate('targetCCIsLockedWarning'),
                        closable: false,
                        icon: Ext.MessageBox.WARNING,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                callback(bookings);
                            }
                        }
                    });
                } else {
                    Ext.MessageBox.show({
                        buttons: Ext.MessageBox.OK,
                        title: '',
                        msg: BC.translate('targetCCIsLockedInformation'),
                        closable: false,
                        animateTarget: this,
                        icon: Ext.MessageBox.WARNING
                    });
                }
            } else {
                callback(bookings);
            }
        } else {
            callback(bookings);
        }
    },

    validateTargetCC: function(bookings, callback) {
        if (this.targetCc) {
            var bookingsAfterLastAccountingDateOfTargetCC = [],
                msg;

            Ext.each(bookings, function(booking) {
                if (!this.targetCc.lastAccountingDate ||
                    this.targetCc.lastAccountingDate &&
                    Ext.Date.clearTime(booking.date).getTime() > Ext.Date.clearTime(this.targetCc.lastAccountingDate).getTime()
                ) {
                    bookingsAfterLastAccountingDateOfTargetCC.push(booking);
                }
            }, this);

            //if there are any booking before last accounting period of target CC.
            if (bookingsAfterLastAccountingDateOfTargetCC.length < bookings.length) {
                if (global.privileges.rebookAllBookings === true) {
                    if (bookingsAfterLastAccountingDateOfTargetCC.length === 0) {
                        msg = Ext.String.format(
                            BC.translate('allBookingBeforeLastAccountingTargetCCWarning'),
                            Ext.Date.format(this.targetCc.lastAccountingDate, 'd.m.Y')
                        );
                    } else {
                        msg = Ext.String.format(
                            BC.translate('bookingBeforeLastAccountingTargetCCWarning'),
                            Ext.Date.format(this.targetCc.lastAccountingDate, 'd.m.Y')
                        );
                    }

                    Ext.MessageBox.show({
                        buttons: Ext.MessageBox.YESNO,
                        title: '',
                        msg: msg,
                        closable: false,
                        animateTarget: this,
                        icon: Ext.MessageBox.WARNING,
                        scope: this,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                callback(bookings);
                            } else {
                                callback(bookingsAfterLastAccountingDateOfTargetCC);
                            }
                        }
                    });
                } else {
                    Ext.MessageBox.show({
                        buttons: Ext.MessageBox.OK,
                        title: '',
                        msg: Ext.String.format(BC.translate('bookingBeforeLastAccountingTargetCCInformation'), Ext.Date.format(this.targetCc.lastAccountingDate, 'd.m.Y')),
                        closable: false,
                        animateTarget: this,
                        icon: Ext.MessageBox.WARNING,
                        scope: this,
                        fn: function() {
                            callback(bookingsAfterLastAccountingDateOfTargetCC);
                        }
                    });
                }
            } else {
                callback(bookings);
            }
        } else {
            callback(bookings);
        }
    },

    doRequest: function(bookings) {
        if (bookings.length === 0) {
            return;
        }

        var totalValue = 0;
        this.storeBookingTypes.each(function(type) {
            totalValue += this.getStore().sum("bookingType_" + type.get('name'));
        }, this);

        this.el.mask('loading...', 'x-mask-report');

        Ext.each(bookings, function(booking) {
            booking.date = Ext.Date.format(booking.date, 'd.m.Y');
        });

        Ext.Ajax.request({
            url: "report/rebook",
            params: {
                profile: this.record.get('id'),
                bookings: Ext.JSON.encode(bookings),
                targetCC: this.targetCc ? this.targetCc.id : null,
                requestor: this.comboRebookingRequester.getValue(),
                reason: this.textFieldRebookingReason.getValue(),
                total: totalValue
            },
            success: function (response) {
                this.el.unmask();
                if (response && response.responseText) {
                    var result = Ext.JSON.decode(response.responseText);

                    if (result) {
                        if (result.result === true) {
                            Ext.MessageBox.show({
                                icon: Ext.MessageBox.INFO,
                                buttons: Ext.MessageBox.OK,
                                title: ' ',
                                msg: BC.translate('rebookingCompleted'),
                                closable: false,
                                animateTarget: this,
                                fn: function() {
                                    this.fireEvent('reloadReport', this);
                                },
                                scope: this
                            });
                        } else if (result.result === "targetCCLocked") {
                            Ext.MessageBox.show({
                                buttons: Ext.MessageBox.OK,
                                title: '',
                                msg: BC.translate('targetCCIsLockedInformation'),
                                closable: false,
                                animateTarget: this,
                                icon: Ext.MessageBox.WARNING
                            });
                        }
                    }
                }
            },
            failure: function () {
                this.el.unmask();
            },
            scope: this
        });
    },

    isHolidayCC: function(isHoliday) {
        return this.holidayRebooking ? false : Boolean(Number(isHoliday));
    },

    initHolidayRebooking: function(bookings, isHoliday) {
        var hasOnlyHolidayCCs = bookings.length !== 0,
            hasHolidayTargetCC = Boolean(Number(isHoliday));

        Ext.Array.each(bookings, function(value) {
            hasOnlyHolidayCCs = Boolean(Number(value.isHoliday));
            if (!hasOnlyHolidayCCs) {
                return false;
            }
        }, this);
        this.holidayRebooking = hasOnlyHolidayCCs && (isHoliday == null || hasHolidayTargetCC);
    }
});
