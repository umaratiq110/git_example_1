Ext.define('BC.view.report.Employee', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.employee',

    mixins: ['BC.view.report.Abstract'],
    requires: ['BC.model.EmployeeBooking'],

    cls: 'report-employee',

    initComponent: function() {
        this.noBookingsLabel = Ext.create('Ext.form.Label', {
            region: 'north',
            html: BC.translate('imganzenmonatbuchtennichtimmer') + ':',
            style: 'font-weight: bold;',
            padding: '5',
            height: 25
        });

        this.store = Ext.create('Ext.data.Store', {
            model: 'BC.model.EmployeeBooking',
            data: []
        });

        this.view = Ext.create('Ext.view.View', {
            region: 'center',
            padding: '5',
            store: this.store,
            itemSelector: 'span.employee-booking',
            tpl: new Ext.XTemplate(
                    '<tpl for=".">',
                    '<span class="employee-booking" style="margin-bottom: 5px;">',
                    '<div style="width: 50px; margin-right: 10px; text-align: right; float: left;">{employee}:</div>',
                    '<div style="margin-left: 60px;">{emptyBookingDates}</div>',
                    '</span><br />',
                '</tpl>'
            ),
            autoScroll: true
        });

        this.items = [{
            xtype: 'panel',
            layout: 'border',
            border: 0,
            height: '100%',
            width: '100%',
            margin: 0,
            padding: 0,
            items: [
                this.noBookingsLabel,
                this.view
            ]
        }];

        this.callParent(arguments);
    },

    init: function(result) {
        this.store.loadData(result.emptyBookings);
    }
});
