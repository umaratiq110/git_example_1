Ext.define('BC.view.report.ReportPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.reportPanel',

    uses: [
        'BC.view.report.Summary',
        'BC.view.report.Diagram', 'BC.view.report.Table',
        'BC.view.report.List', 'BC.view.report.Employee',
        'BC.view.report.Booking', 'BC.view.report.Rebook',
        'BC.view.report.Permission', 'BC.view.report.MonthEnd',
        'BC.view.report.BtModifications', 'BC.view.report.SAPKeyHours',
        'BC.view.report.RebookLog', 'BC.view.report.Standby',
        'BC.view.report.AbsenceCalendar', 'BC.view.report.Status',
        'BC.view.report.OrderOverview',
        'BC.view.report.ContingentSapInvoiceComparison'
    ],

    layout: 'border',
    closable: true,
    // TODO: revalidate with library update (buggy as of 4.2.2)
    // used for storing scroll position in IE
    scrollPosition: {left: 0, top: 0},

    constructor: function(title, iconCls, record) {
        Ext.apply(this, {
            title: title,
            iconCls: iconCls,
            record: record,
            timestamp: Ext.Date.format(new Date(), 'U')
        });

        this.callParent();
    },

    initComponent: function() {
        var className,
            objConfReport = {
                region: 'center',
                title: BC.translateReport(this.record.data.type) + " - " + this.title,
                record: this.record,
                timestamp: this.timestamp
            },
            objConfSummary = {
                region: 'north',
                record: this.record,
                timestamp: this.timestamp
            };

        switch (this.record.get('type')) {
            case 'D':
                className = 'BC.view.report.Diagram';
                break;
            case 'T':
                className = 'BC.view.report.Table';
                break;
            case 'L':
                className = 'BC.view.report.List';
                break;
            case 'E':
                className = 'BC.view.report.Employee';
                break;
            case 'B':
                className = 'BC.view.report.Booking';
                break;
            case 'R':
                className = 'BC.view.report.Rebook';
                break;
            case 'P':
                className = 'BC.view.report.Permission';
                break;
            case 'M':
                className = 'BC.view.report.MonthEnd';
                break;
            case 'N':
                className = 'BC.view.report.BtModifications';
                break;
            case 'S':
                className = 'BC.view.report.SAPKeyHours';
                break;
            case 'O':
                className = 'BC.view.report.RebookLog';
                break;
            case 'H':
                className = 'BC.view.report.Standby';
                break;
            case 'C':
                className = 'BC.view.report.AbsenceCalendar';
                break;
            case 'K':
                className = 'BC.view.report.Status';
                break;
            case 'V':
                className = 'BC.view.report.OrderOverview';
                break;
            case 'X':
                className = 'BC.view.report.ContingentSapInvoiceComparison';
                break;
        }

        this.report = Ext.create(className, objConfReport);
        this.summary = Ext.create('BC.view.report.Summary', objConfSummary);

        this.items = [this.summary, this.report];

        this.callParent(arguments);
    }
});
