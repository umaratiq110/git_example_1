Ext.define('BC.view.report.Booking', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.booking',

    mixins: ['BC.view.report.Abstract'],
    requires: ['BC.model.Booking'],
    uses: ['Ext.container.ButtonGroup'],

    statics: {
        levelDefault: 1
    },

    cls: 'booking',

    initComponent: function() {
        var gridBTHours = [],
            numberRenderer = Ext.util.Format.numberRenderer('0,000.00');

        this.levelOneButton = Ext.create('Ext.button.Button', {
            text: '1',
            action: 'expandLevelOne',
            disabled: true
        });

        this.levelTwoButton = Ext.create('Ext.button.Button', {
            text: '2',
            action: 'expandLevelTwo',
            disabled: true
        });

        this.levelThreeButton = Ext.create('Ext.button.Button', {
            text: '3',
            action: 'expandLevelThree',
            disabled: true
        });

        if (this.record.get('showBTHours') === 1) {
            // Create booking types fields in model and as grid columns
            var bt = Ext.data.StoreManager.lookup('BookingTypes').getRange(),
                bookingModel = Ext.ModelManager.getModel('BC.model.Booking');
            for (var i = 0; i < bt.length; ++i) {

                if (Ext.Array.contains(this.record.data.bookingTypes, bt[i].getId()) || Number(this.record.get('displayOnlySubtrees')) === 0) { //add only selected booking types
                    bookingModel.prototype.fields.add(new Ext.data.Field({
                        name: bt[i].get('name') + 'Hours',
                        type: 'string'
                    }));

                    gridBTHours.push({
                        text: bt[i].get('name'),
                        width: 60,
                        dataIndex: bt[i].get('name') + 'Hours',
                        tooltip: bt[i].get('longName'),
                        menuDisabled: true,
                        sortable: false,
                        align: 'center',
                        renderer: numberRenderer
                    });
                }
            }
        }

        this.store = Ext.create('Ext.data.TreeStore', {
            model: 'BC.model.Booking',
            root: {
                expanded: true,
                children: []
            }
        });

        this.exportMenu = new Ext.button.Button({
            text: BC.translate('export'),
            icon: 'files/images/export.png',
            menu: {
                xtype: 'menu',
                plain: true,
                items: [{
                    text: BC.translate('bookingExportWith'),
                    icon: 'files/images/excel.png',
                    action: 'exportWith'
                }, {
                    text: BC.translate('bookingExportWithout'),
                    icon: 'files/images/excel.png',
                    action: 'exportWithout'
                }]
            },
            scope: this
        });

        this.exportButton = new Ext.button.Button({
            text: BC.translate('export') + '-' + BC.translate('excel'),
            icon: 'files/images/excel.png',
            action: 'exportWithout'
        });

        Ext.apply(this, {
            store: this.store,
            rootVisible: false,
            singleExpand: false,
            selModel: {
                mode: 'SINGLE'
            },
            useArrows: true,
            viewConfig: {
                animate: false
            },
            columns: Ext.Array.merge(
                this.record.get('showTargetHours') === 1 ? [{
                    text: BC.translate('abbrTargetHours') + '<div data-qtip="' + BC.translate('targetHoursAggregatedValuesTooltip') + '" class="infoIcon"></div>',
                    width: 110,
                    dataIndex: 'target_effort',
                    renderer: function(val, rec) {
                        if (val !== 0) {
                            if (rec.record.get('target_effort_is_aggregated')) {
                                return '<i>' + numberRenderer(val) + '</i>';
                            }
                            return numberRenderer(val);
                        }
                    },
                    sortable: true,
                    menuDisabled: true,
                    align: 'center'
                }, {
                    text: BC.translate('abbrRemainingHours') + '<div data-qtip="' + BC.translate('remainingHoursAggregatedValuesTooltip') + '" class="infoIcon"></div>',
                    width: 120,
                    dataIndex: 'remaining_effort',
                    renderer: function(val, rec) {
                        if (val != null) {
                            if (rec.record.get('remaining_effort_is_aggregated')) {
                                return '<i>' + numberRenderer(val) + '</i>';
                            }

                            var date = rec.record.get('remaining_effort_month');
                            if (date) {
                                return numberRenderer(val) + " (" + Ext.Date.format(date, 'd.m.Y') + ")";
                            }
                            return numberRenderer(val);
                        }
                    },
                    sortable: true,
                    menuDisabled: true,
                    align: 'center'
                }] : [],
                gridBTHours,
                [{
                    text: BC.translate('stunden', 'ucfirst'),
                    width: 90,
                    sortable: true,
                    menuDisabled: true,
                    dataIndex: 'hours',
                    align: 'center',
                    renderer: numberRenderer
                }, {
                    xtype: 'treecolumn', //this is so we know which column will show the tree
                    text: BC.translate('tree'),
                    flex: 1,
                    sortable: true,
                    menuDisabled: true,
                    dataIndex: 'node',
                    tdCls: 'tree-cell',
                    // Value contains HTML markups which must not be escaped
                    defaultRenderer: function (value) {
                        return value;
                    }
                }],
                this.record.get('showEmpBookings') === 1 ? [{
                    text: BC.translate('bookings'),
                    flex: 1,
                    dataIndex: 'bookings',
                    sortable: false,
                    menuDisabled: true,
                    align: 'left',
                    // Value contains HTML markups which must not be escaped
                    renderer: function(value) {
                        return value;
                    }
                }] : [],
                this.record.get('showCostAndRevenue') === 1 ? [{
                    text: Ext.util.Format.htmlDecode(BC.translate('revenue')),
                    width: 100,
                    dataIndex: 'revenue',
                    sortable: true,
                    menuDisabled: true,
                    align: 'center',
                    renderer: numberRenderer
                }, {
                    text: Ext.util.Format.htmlDecode(BC.translate('cost')),
                    width: 100,
                    dataIndex: 'cost',
                    sortable: true,
                    menuDisabled: true,
                    align: 'center',
                    renderer: numberRenderer
                }] : [],
                [{
                    text: BC.translate('number'),
                    width: 80,
                    dataIndex: 'debtor',
                    sortable: true,
                    menuDisabled: true,
                    align: 'center'
                }, {
                    text: BC.translate('sapKeyCostUnit'),
                    width: 100,
                    dataIndex: 'sapKeyCostUnit',
                    sortable: true,
                    menuDisabled: true,
                    align: 'center'
                }, {
                    text: BC.translate('sapKeyCostCenter'),
                    width: 100,
                    dataIndex: 'sapKeyCostCenter',
                    sortable: true,
                    menuDisabled: true,
                    align: 'center'
                }]
            ),
            tbar: [
                BC.translate('open') + ' ',
                {
                    xtype: 'buttongroup',
                    items: [this.levelOneButton]
                }, {
                    xtype: 'buttongroup',
                    items: [this.levelTwoButton]
                }, {
                    xtype: 'buttongroup',
                    items: [this.levelThreeButton]
                },
                ' ' + BC.translate('level'),
                '-',
                this.record.get('showEmpBookings') === 1 ? this.exportMenu : this.exportButton
            ]
        });

        this.callParent(arguments);
    },

    initEvents: function() {
        this.callParent(arguments);

        this.on({
            'afteritemcollapse': function(clickNode, indexNode) {
                clickNode.collapseChildren(true);
                this.getSelectionModel().deselect(indexNode);
                this.getSelectionModel().select(indexNode);
            },
            'select': function() {
                this.levelOneButton.setDisabled(false);
                this.levelTwoButton.setDisabled(false);
                this.levelThreeButton.setDisabled(false);
            }
        }, this);
    },

    init: function(result) {
        this.bookings = result.bookings;
        if (!(Ext.isArray(this.bookings.children) && this.bookings.children.length === 0)) {
            this.getRootNode().appendChild(this.bookings.children);
        }
    }

});
