Ext.define('BC.view.report.Standby', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.standby',

    mixins: ['BC.view.report.Abstract'],

    cls: 'standby',

    initComponent: function() {
        this.exportButton = new Ext.Button({
            iconCls: 'excel-ico',
            action: 'downloadExcel',
            text: BC.translate('export') + '-' + BC.translate('excel')

        });

        Ext.apply(this, {
            layout: 'fit',
            store: Ext.create('BC.store.report.Standby'),
            columns: [{
                header: BC.translate('firma'),
                dataIndex: 'company',
                width: 70,
                menuDisabled: true
            }, {
                header: BC.translate('division'),
                dataIndex: 'division',
                width: 140,
                menuDisabled: true,
                renderer: function(value, metadata) {
                    if (value) {
                        metadata.tdAttr = 'data-qtip="' + Ext.util.Format.htmlEncode(Ext.util.Format.htmlEncode(value)) + '"';
                        value = Ext.util.Format.htmlEncode(value);
                    }

                    return value;
                }
            }, {
                header: BC.translate('employeeShortcut'),
                dataIndex: 'employeeShortcut',
                width: 100,
                menuDisabled: true
            }, {
                header: BC.translate('employeeName'),
                dataIndex: 'employeeName',
                width: 160,
                menuDisabled: true
            }, {
                header: BC.translate('dayType'),
                dataIndex: 'dayType',
                width: 150,
                menuDisabled: true,
                renderer: function(value) {
                    return Ext.util.Format.htmlEncode(BC.translate('dayType_' + value));
                }
            }, {
                header: BC.translate('datum'),
                dataIndex: 'startDate',
                width: 75,
                menuDisabled: true,
                renderer: Ext.util.Format.dateRenderer('d.m.Y'),
                doSort: function(state) {
                    var ds = this.up('tablepanel').store;
                    ds.sort({
                        property: 'startDateTime',
                        direction: state
                    });
                }
            }, {
                header: BC.translate('begin'),
                dataIndex: 'startTime',
                width: 65,
                align: 'center',
                renderer: Ext.util.Format.dateRenderer('H:i')
            }, {
                header: BC.translate('hoursActual'),
                dataIndex: 'hoursActual',
                align: 'right',
                width: 140,
                menuDisabled: true,
                renderer: Ext.util.Format.numberRenderer('0.00')
            }, {
                header: BC.translate('abkbkz'),
                dataIndex: 'cc',
                width: 200,
                menuDisabled: true
            }, {
                header: BC.translate('notiz'),
                dataIndex: 'note',
                flex: 1,
                menuDisabled: true
            }],
            tbar: [
                this.exportButton
            ],
            enableColumnMove: false,
            disableSelection: true,
            viewConfig: {
                enableTextSelection: true,
                loadMask: false
            },
            verticalScroller: {
                trailingBufferZone: 150,
                leadingBufferZone: 150
            }
        });

        this.callParent(arguments);
    },

    initEvents: function() {
        this.callParent();
    },

    init: function(result) {
        var store = this.getStore();

        Ext.suspendLayouts();
        store.getProxy().data = result.data;
        store.load();
        Ext.resumeLayouts(true);
    }
});
