Ext.define('BC.view.report.AbsenceCalendar', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.absencecalendar',

    mixins: ['BC.view.report.Abstract'],

    uses: [
        'Ext.grid.Panel',
        'Ext.view.View',
        'Ext.XTemplate'
    ],

    statics: {
        colWidth: 20
    },

    cls: 'absence-calendar',

    // report config
    dateFrom: null,
    dateTill: null,
    publicHolidays: null,

    // private
    employeeRowHtml: null,

    initComponent: function() {
        this.barColumnCfg = {
            tdCls: 'absence-cell',
            width: 0,
            renderer: this.renderBars,
            scope: this,
            resizable: false
        };

        this.storeAbsences = Ext.create('BC.store.report.AbsenceEmployees');
        this.storeCategories = Ext.StoreMgr.get('report.AbsenceCategories');

        this.panelCalendar = Ext.create('Ext.grid.Panel', {
            region: 'center',
            cls: 'absence-calendar-grid',
            columns: [this.barColumnCfg],
            store: this.storeAbsences,
            viewConfig: {
                overItemCls: '',
                disableSelection: true,
                stripeRows: false,
                trackOver: false
            },
            features: [{
                ftype: 'grouping',
                groupHeaderTpl: '&nbsp;',
                collapsible: false
            }],
            split: true
        });

        // overwrite getColumnsForTpl method of header container
        this.panelCalendar.headerCt.getColumnsForTpl = function(flushCache) {
            var headers = this.getGridColumns(flushCache);
            // always just return the first column (= column for bars) to
            // prevent rendering of one cell for each day of the period
            return [{
                dataIndex: headers[0].dataIndex,
                align: headers[0].align,
                id: headers[0].id,
                cls: headers[0].tdCls,
                columnId: headers[0].getItemId()
            }];
        };

        Ext.apply(this, {
            layout: 'border',
            tbar: [{
                xtype: 'button',
                iconCls: 'excel-ico',
                action: 'downloadExcel',
                text: BC.translate('export') + '-' + BC.translate('excel')
            }],
            items: [{
                xtype: 'grid',
                region: 'west',
                store: this.storeAbsences,
                columns: [{
                    text: BC.translate('month'),
                    resizable: false,
                    menuDisabled: true,
                    columns: [{
                        text: BC.translate('calendarWeekShortcut'),
                        resizable: false,
                        menuDisabled: true,
                        columns: [{
                            text: BC.translate('day'),
                            dataIndex: 'shortcut',
                            align: 'left',
                            // somehow flex doesn't work on the column, so use a fixed column width
                            width: 250,
                            resizable: false,
                            sortable: false,
                            menuDisabled: true,
                            renderer: this.renderEmployee
                        }]
                    }]
                }],
                viewConfig: {
                    overItemCls: '',
                    disableSelection: true,
                    enableTextSelection: true,
                    stripeRows: false,
                    trackOver: false
                },
                features: [{
                    ftype: 'grouping',
                    groupHeaderTpl: [
                        '{name:this.renderName}',
                        {
                            renderName: function(name) {
                                if (name === '{SINGLE}') {
                                    return BC.translate('singleSelectedEmp');
                                }
                                return name;
                            }
                        }
                    ],
                    collapsible: false
                }],
                cls: 'absence-employee-grid',
                split: true,
                width: 200,
                minWidth: 150,
                maxWidth: 250
            },
                this.panelCalendar
            ],
            bbar: [
                BC.translate('legend') + ':',
                {
                    xtype: 'dataview',
                    store: this.storeCategories,
                    tpl: new Ext.XTemplate(
                        '<div class="absence-category">',
                            '<div class="absence-category-name" style="font-weight: bold">' + BC.translate('publicHolidayShortcut') + '</div>',
                            '<div class="absence-category-name">' + BC.translate('publicHoliday') + '</div>',
                        '</div>',
                        '<tpl for=".">',
                            '<div class="absence-category">',
                                '<div class="absence-category-bar <tpl if="striped">absence-bar-striped</tpl>" style="background-color:#{color}"></div>',
                                '<div class="absence-category-name">{name}</div>',
                            '</div>',
                        '</tpl>'
                    ),
                    itemSelector: 'div.absence-legend-category'
                }
            ]
        });

        this.callParent(arguments);
    },

    initEvents: function() {
        this.callParent();
    },

    init: function(result) {
        var store = this.storeAbsences,
            from = Ext.Date.clearTime(Ext.Date.parse(result.from, 'U')),
            till = Ext.Date.clearTime(Ext.Date.parse(result.until, 'U')),
            columns;

        this.dateFrom = from;
        this.dateTill = till;
        this.publicHolidays = result.publicHolidays;
        // generate html for weekends & public holidays just once per employee
        // and re-use it in row renderer
        this.employeeRowHtml = {};

        // load the stores
        Ext.suspendLayouts();

        this.storeCategories.getProxy().data = result.absenceCategories;
        this.storeCategories.load();

        if (result.employeesAsGroups === true) {
            store.group('group');
        } else {
            store.clearGrouping();
        }
        store.getProxy().data = result.employeeAbsences;
        store.load();

        columns = this.getCalendarColumns(from, till);
        columns.unshift(this.barColumnCfg);
        this.panelCalendar.reconfigure(store, columns);

        Ext.resumeLayouts(true);
    },

    renderEmployee: function(value, meta, record) {
        if (value) {
            return Ext.String.format('{0} ({1} {2})', value, record.get('first_name'), record.get('last_name'));
        }
        return '';
    },

    /**
     * Get column header configuration object according to the report's period
     */
    getCalendarColumns: function(from, till) {
        var months = [],
            weeks = [],
            days = [],
            defaultColAttributes = {
                align: 'center',
                sortable: false,
                resizable: false,
                menuDisabled: true
            };

        for (; from.getTime() <= till.getTime(); from = Ext.Date.add(from, Ext.Date.DAY, 1)) {
            // days
            days.push(
                Ext.apply({
                    text: from.getDate(),
                    width: this.self.colWidth
                }, defaultColAttributes)
            );
            // calendar weeks
            if (from.getDay() === 0 || from.getTime() === Ext.Date.getLastDateOfMonth(from).getTime() || from.getTime() === till.getTime()) {
                weeks.push(
                    Ext.apply({
                        text: BC.translate('calendarWeekShortcut') + ' ' + Ext.Date.format(from, 'W'),
                        columns: days
                    }, defaultColAttributes)
                );
                days = [];
            }
            // months
            if (from.getTime() === Ext.Date.getLastDateOfMonth(from).getTime() || from.getTime() === till.getTime()) {
                months.push(
                    Ext.apply({
                        text: Ext.Date.monthNames[from.getMonth()] + ' ' + from.getFullYear(),
                        columns: weeks
                    }, defaultColAttributes)
                );
                weeks = [];
            }
        }
        return months;
    },

    /**
     * Returns HTML code for week ends and public holidays for a location.
     * The generation is performed just once per location, all further calls
     * will return the stored result.
     */
    getEmployeeRowHtml: function(employeeId) {
        var from = this.dateFrom,
            till = this.dateTill,
            html = '',
            holidays;

        if (this.employeeRowHtml[employeeId] !== undefined) {
            return this.employeeRowHtml[employeeId];
        }

        holidays = this.publicHolidays[employeeId] || {};

        for (var d = from, ph; d <= till; d = Ext.Date.add(d, Ext.Date.DAY, 1)) {
            ph = holidays[Ext.Date.format(d, 'Y-m-d')];
            if (ph !== undefined) {
                html += Ext.String.format(
                    '<div class="absence-publicholiday" style="width:{0}px;left:{1}px;" data-qtip="{2}">{3}</div>',
                    this.self.colWidth,
                    this.getDays(d, from) * this.self.colWidth,
                    ph || '',
                    BC.translate('publicHolidayShortcut')
                );
            } else if (d.getDay() === 0 || d.getDay() === 6) {
                html += Ext.String.format(
                    '<div class="absence-weekend" style="width:{0}px;left:{1}px;">{2}</div>',
                    this.self.colWidth,
                    this.getDays(d, from) * this.self.colWidth,
                    Ext.Date.format(d, 'D').substr(0, 2)
                );
            }
        }

        this.employeeRowHtml[employeeId] = html;
        return html;
    },

    /**
     * Returns HTML code representation for an employee's absences,
     * i.e. bars with tooltips
     */
    renderBars: function(val, meta, record) {
        var absences = record.absences().getRange(),
            html = this.getEmployeeRowHtml(record.get('employee_id')),
            // loop variables
            tooltip, category;

        // render absence bars
        for (var i = 0; i < absences.length; i++) {
            tooltip = '';
            category = this.storeCategories.getById(absences[i].get('category_id'));

            if (category) {
                tooltip += '<b>' + category.get('name') + '</b>';
            }

            var startDate = Ext.Date.format(absences[i].get('start_date'), 'd.m.Y');
            var endDate = Ext.Date.format(absences[i].get('end_date'), 'd.m.Y')

            tooltip += "<br />" + startDate;

            if (startDate !== endDate) {
                tooltip += ' - ' + endDate;
            }

            var startTime = absences[i].get('start_time');
            var endTime = absences[i].get('end_time');

            if (startTime.length > 0 && endTime.length > 0) {
                tooltip += ' ' + startTime + ' - ' + endTime;
            }

            tooltip += "<br />" + Ext.String.htmlEncode(Ext.String.htmlEncode(absences[i].get('description')));
            html += Ext.String.format(
                '<div class="absence-bar {0}" style="width:{1}px;left:{2}px;background-color:#{3}; z-index:{5}"></div>' +
                '<div class="absence-bar-tooltip" style="width:{1}px;left:{2}px; z-index:{6}" data-qtip="{4}"></div>',
                category.get('striped') ? 'absence-bar-striped' : '',
                (this.getDays(absences[i].get('end_date'), absences[i].get('start_date')) + 1) * this.self.colWidth,
                this.getDays(absences[i].get('start_date'), this.dateFrom) * this.self.colWidth,
                category ? category.get('color') : 'FFFFFF',
                tooltip,
                category.get('priority'),
                category.get('priority') + 1
            );
        }
        return html;
    },

    /**
     * Get the number of days between two dates, rounded to full days
     */
    getDays: function(startDate, endDate) {
        return Math.round((startDate - endDate) / 86400000); // ms -> days (/1000 /60 /60 /24 = /86400000)
    }
});
