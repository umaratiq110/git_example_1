Ext.define('BC.view.report.RebookLog', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.RebookLog',

    mixins: ['BC.view.report.Abstract'],
    requires: ['BC.model.report.RebookLog', 'Ext.grid.feature.Grouping'],

    initComponent: function() {
        this.store = Ext.create('Ext.data.Store', {
            model: 'BC.model.report.RebookLog',
            groupField: 'rebooking_id',
            data: []
        });

        this.groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
            groupHeaderTpl: new Ext.XTemplate(
                '{[this.extract(values, "rebooking_date")]} ' + BC.translate('requestedBy')
                + ' {[this.extract(values, "requestor")]} ' + BC.translate('doneBy')
                + ' {[this.extract(values, "performer")]}, {[this.extract(values, "sum_of_modified_totals")]} '
                + BC.translate('rebookedHours') + ' ({[this.extract(values, "reason")]})',
                {
                    extract: function(values, key) {
                        return values.children[0].data[key];
                    }
                }
            )
        });

        this.isEmpty = function(val) {
            if (val !== 0) {
                return Ext.util.Format.number(val, '0.00');
            }
        };

        Ext.apply(this, {
            store: this.store,
            enableColumnMove: false,
            disableSelection: true,
            features: [this.groupingFeature],
            columns: [{
                text: BC.translate('day'),
                width: 100,
                dataIndex: 'booking_date',
                align: 'left',
                menuDisabled: true
            }, {
                text: BC.translate('empl'),
                width: 50,
                dataIndex: 'booking_employee',
                align: 'left',
                menuDisabled: true
            }, {
                text: BC.translate('ucHours'),
                width: 60,
                dataIndex: 'hours',
                align: 'right',
                renderer: this.isEmpty,
                menuDisabled: true
            }, {
                text: BC.translate('ccOld'),
                width: 250,
                dataIndex: 'cc_old',
                align: 'left',
                menuDisabled: true
            }, {
                text: BC.translate('ccNew'),
                width: 250,
                dataIndex: 'cc_new',
                align: 'left',
                menuDisabled: true
            }, {
                text: BC.translate('bookingTypeOld'),
                width: 100,
                dataIndex: 'old_booking_type',
                align: 'center',
                menuDisabled: true
            }, {
                text: BC.translate('bookingTypeNew'),
                width: 100,
                dataIndex: 'new_booking_type',
                align: 'center',
                menuDisabled: true
            }, {
                text: BC.translate('notePrevious'),
                flex: 1,
                dataIndex: 'old_note',
                align: 'left',
                menuDisabled: true
            }, {
                text: BC.translate('noteNew'),
                flex: 1,
                dataIndex: 'note',
                align: 'left',
                menuDisabled: true
            }, {
                itemId: 'rlRequestor',
                text: '',
                width: 0,
                dataIndex: 'requestor',
                menuDisabled: true,
                hidden: true
            }, {
                itemId: 'rlPerformer',
                text: '',
                width: 0,
                dataIndex: 'performer',
                menuDisabled: true,
                hidden: true
            }, {
                itemId: 'rlReason',
                text: '',
                width: 0,
                dataIndex: 'reason',
                menuDisabled: true,
                hidden: true
            }, {
                itemId: 'rlRebooking_date',
                text: '',
                width: 0,
                dataIndex: 'rebooking_date',
                menuDisabled: true,
                hidden: true
            }, {
                itemId: 'rlSum_of_modified_totals',
                text: '',
                width: 0,
                dataIndex: 'sum_of_modified_totals',
                menuDisabled: true,
                hidden: true
            }]
        });

        this.callParent(arguments);
    },

    init: function(result) {
        this.store.loadRecords(this.store.proxy.reader.readRecords(result.list).records);
        this.store.sort([
            {
                property: 'booking_date',
                direction: 'ASC'
            }, {
                property: 'booking_employee',
                direction: 'ASC'
            }
        ]);
    }
});
