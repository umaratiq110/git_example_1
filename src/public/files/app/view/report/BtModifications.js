Ext.define('BC.view.report.BtModifications', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.BtModifications',

    mixins: ['BC.view.report.Abstract'],
    requires: ['BC.model.BtModifications', 'BC.lib.tooltip.TreePath'],

    initComponent: function() {
        this.store = Ext.create('Ext.data.Store', {
            model: 'BC.model.BtModifications',
            data: []
        });

        Ext.apply(this, {
            store: this.store,
            enableColumnMove: false,
            disableSelection: true,
            columns: [{
                text: BC.translate('modification'),
                width: 75,
                dataIndex: 'modification',
                align: 'left',
                renderer: function modificationGrid(val) {
                    if (val === 'locked') {
                        return BC.translate('locked');
                    } else if (val === 'new') {
                        return BC.translate('neu');
                    }
                }
            }, {
                text: BC.translate('datum') + '/' + BC.translate('zeit'),
                width: 100,
                dataIndex: 'datetime',
                align: 'left'
            }, {
                text: BC.translate('nodeName'),
                flex: 1,
                dataIndex: 'node_name',
                align: 'left'
            }, {
                text: BC.translate('nodeType'),
                width: 75,
                dataIndex: 'node_type',
                align: 'right'
            }, {
                text: BC.translate('leaf'),
                width: 50,
                dataIndex: 'is_leaf',
                align: 'right',
                renderer: function leafGrid(val) {
                    if (val === 1) {
                        return BC.translate('ja');
                    } else if (val === 0) {
                        return BC.translate('nein');
                    }
                }
            }, {
                header: BC.translate('treepos'),
                xtype: 'actioncolumn',
                align: 'center',
                menuDisabled: true,
                items: [{
                    icon: 'files/images/wayUp.png',
                    handler: function(view, rowIndex, colIndex, item, e) {
                        var record = view.getRecord(view.getNode(rowIndex)),
                            pos = e.getXY();

                        pos[0] += 15;
                        pos[1] -= 15;

                        Ext.create('BC.lib.tooltip.TreePath', {
                            nodeId: record.get('node_id')
                        }).showAt(pos);
                    },
                    getClass: function(v, meta, record) {
                        if (record.get('isSet')) {
                            return 'x-hide-display';
                        }
                    }
                }]
            }, {
                text: BC.translate('authorizedBy'),
                width: 75,
                dataIndex: 'authorized',
                align: 'right'
            }, {
                text: BC.translate('sapKeyCostUnit'),
                width: 150,
                dataIndex: 'sap_key_cost_object',
                align: 'right'
            }, {
                text: BC.translate('sapKeyCostCenter'),
                width: 150,
                dataIndex: 'sap_key_cost_center',
                align: 'right'
            }],
            dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    text: BC.translate('export') + '-' + BC.translate('excel'),
                    iconCls: 'excel-ico',
                    action: 'downloadExcel'
                }]
            }]
        });

        this.callParent(arguments);
    },

    init: function(result) {
        this.store.loadRecords(this.store.proxy.reader.readRecords(result.list).records);
    }
});
