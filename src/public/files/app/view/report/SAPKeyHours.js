Ext.define('BC.view.report.SAPKeyHours', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.sapKeyHours',

    mixins: ['BC.view.report.Abstract'],
    requires: ['BC.model.SAPKeyHours'],

    cls: 'sapKeyHours',

    initComponent: function() {
        // Create booking types fields in model and as grid columns
        var bt = Ext.data.StoreManager.lookup('BookingTypes').getRange(),
            rebookModel = Ext.ModelManager.getModel('BC.model.SAPKeyHours'),
            gridBt = [];

        for (var i = 0; i < bt.length; ++i) {
            rebookModel.prototype.fields.add(new Ext.data.Field({
                name: bt[i].get('name').toUpperCase(),
                type: 'float',
                persist: false
            }));

            gridBt.push({
                text: bt[i].get('name').toUpperCase(),
                width: 100,
                dataIndex: bt[i].get('name').toUpperCase(),
                renderer: this.isEmpty,
                summaryType: 'sum',
                summaryRenderer: this.floatValueRenderer,
                align: 'right',
                menuDisabled: true
            });
        }

        this.store = Ext.create('Ext.data.Store', {
            model: 'BC.model.SAPKeyHours',
            data: []
        });

        this.floatValueRenderer = function(value) {
            return value.toFixed(2);
        };

        this.isEmpty = function(val) {
            if (val !== 0) {
                return Ext.util.Format.number(val, '0.00');
            }
        };

        this.isEmptyWithString = function(val) {
            if (val !== '') {
                return val;
            }
            return '---';
        };

        Ext.apply(this, {
            store: this.store,
            enableColumnMove: false,
            disableSelection: true,
            features: [{
                ftype: 'summary'
            }],
            columns: Ext.Array.merge([{
                text: BC.translate('sapKey'),
                width: 90,
                dataIndex: 'sapkey',
                align: 'left',
                renderer: this.isEmptyWithString,
                menuDisabled: true
            }, {
                text: BC.translate('nodeType'),
                width: 65,
                dataIndex: 'nodetype',
                align: 'left',
                renderer: this.isEmptyWithString,
                menuDisabled: true
            }, {
                text: BC.translate('name'),
                flex: 1,
                dataIndex: 'name',
                align: 'left',
                renderer: this.isEmptyWithString,
                menuDisabled: true
            }, {
                text: BC.translate('mitarbeiter'),
                width: 65,
                dataIndex: 'employee',
                align: 'left',
                menuDisabled: true
            }, {
                text: BC.translate('emplNo'),
                width: 80,
                dataIndex: 'employeenumber',
                align: 'right',
                summaryRenderer: function() {
                    return BC.translate('totals') + ':';
                },
                menuDisabled: true
            }], gridBt, [{
                text: BC.translate('total'),
                width: 100,
                dataIndex: 'total',
                renderer: Ext.util.Format.numberRenderer('0.00'),
                summaryType: 'sum',
                summaryRenderer: this.floatValueRenderer,
                align: 'right',
                menuDisabled: true
            }]),
            dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    text: BC.translate('export') + '-' + BC.translate('excel'),
                    iconCls: 'excel-ico',
                    action: 'downloadExcel'
                }]
            }]
        });

        this.callParent(arguments);
    },

    init: function(result) {
        this.store.loadRecords(this.store.proxy.reader.readRecords(result.liste).records);
    }
});
