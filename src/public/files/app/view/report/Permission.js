Ext.define('BC.view.report.Permission', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.permission',

    mixins: ['BC.view.report.Abstract'],
    uses: ['BC.lib.grid.FilterSearch', 'BC.lib.tooltip.TreePath'],

    cls: 'permission',

    searchedValue: '',
    sort: 'employeeName_ASC',

    initComponent: function() {
        this.clearBtn = Ext.create('Ext.button.Button', {
            text: BC.translate('resetUc'),
            icon: 'files/images/cancel.png',
            handler: function() {
                this.store.clearFilter();
                this.searchField.setValue('');
                this.searchedValue = this.searchField.getValue();
            },
            scope: this
        });

        this.searchField = Ext.create('Ext.form.field.Text', {
            name: 'searchField',
            hideLabel: true,
            width: 200
        });

        this.searchBtn = Ext.create('Ext.button.Button', {
            text: BC.translate('suche'),
            icon: 'files/images/search.png',
            handler: function() {
                this.liveSearch.onTextFieldChange(this.searchField.getValue(), false);
                this.searchedValue = this.searchField.getValue();
            },
            scope: this
        });

        Ext.apply(this, {
            store: Ext.create('BC.store.report.Permissions'),
            enableColumnMove: false,
            disableSelection: true,
            indexes: ['employeeName', 'permissionKind', 'value'],
            columns: [{
                text: BC.translate('mitarbeiter'),
                dataIndex: 'employeeName',
                width: 300,
                menuDisabled: true
            }, {
                text: BC.translate('permissionKind'),
                dataIndex: 'permissionKind',
                width: 300,
                menuDisabled: true
            }, {
                text: BC.translate('valueUC'),
                dataIndex: 'value',
                width: 300,
                menuDisabled: true
            }, {
                header: BC.translate('treepos'),
                xtype: 'actioncolumn',
                align: 'center',
                flex: 1,
                menuDisabled: true,
                items: [{
                    icon: 'files/images/wayUp.png',
                    handler: function(view, rowIndex, colIndex, item, e) {
                        var record = view.getRecord(view.getNode(rowIndex)),
                            pos = e.getXY();

                        pos[0] += 15;
                        pos[1] -= 15;

                        Ext.create('BC.lib.tooltip.TreePath', {
                            nodeId: record.data.nodeID
                        }).showAt(pos);
                    },
                    getClass: function(v, meta, record) {
                        if (record.data.nodeID === 0) {
                            return 'x-hide-display';
                        }
                    }
                }]
            }],
            tbar: [
                BC.translate('suche') + ':', this.searchField, this.searchBtn, this.clearBtn,
                '->',
                {
                    text: BC.translate('export') + '-' + BC.translate('excel'),
                    iconCls: 'excel-ico',
                    action: 'downloadExcel'
                }
            ]
        });

        this.liveSearch = Ext.create('BC.lib.grid.FilterSearch', {
            grid: this,
            store: this.store,
            searchField: this.searchField,
            indexes: this.indexes
        });

        this.callParent(arguments);
    },

    initEvents: function () {
        this.callParent(arguments);

        this.on({
            'sortchange': function(ct, column, direction) {
                this.liveSearch.onTextFieldChange(false, true);
                this.sort = column.dataIndex + '_' + direction;
            },
            scope: this
        });

        this.searchField.on({
            'specialkey': function(field, e) {
                if (e.getKey() === e.ENTER) {
                    this.liveSearch.onTextFieldChange(this.searchField.getValue(), false);
                    this.searchedValue = this.searchField.getValue();
                }
            },
            scope: this
        });
    },

    init: function(result) {
        Ext.suspendLayouts();

        this.store.getProxy().data = result.list;
        this.store.load();

        Ext.resumeLayouts(true);
    }
});
