Ext.define('BC.view.report.MonthEnd', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.monthEnd',

    mixins: ['BC.view.report.Abstract'],
    requires: ['BC.model.MonthEnd', 'Ext.grid.feature.Summary'],

    cls: 'monthEnd',

    initComponent: function() {
        this.floatValueRenderer = function(value) {
            return value ? Number(value).toFixed(2) : '0.00';
        };

        this.floatValueOneDecimalRenderer = function(value) {
            return value ? Number(value).toFixed(1) : '0.0';
        };

        this.percentageValueRenderer = function(value) {
            return value ? (Number(value) * 100).toFixed(0) + '%' : '0%';
        };

        this.floatNotEmptyValueRenderer = function(value) {
            return value ? Number(value).toFixed(2) : '';
        };

        this.intNotEmptyValueRenderer = function(value) {
            return value ? value : '';
        };

        this.integerRenderer = function(value) {
            if (value === parseInt(value, 10)) {
                return value;
            }
            return '';
        };

        this.reasonRenderer = function(value, metadata) {
            metadata.tdAttr = 'data-qtip="' + Ext.util.Format.htmlEncode(Ext.util.Format.htmlEncode(value)) + '"';

            return Ext.util.Format.htmlEncode(value);
        };

        this.store = Ext.create('Ext.data.Store', {
            model: 'BC.model.MonthEnd',
            data: []
        });

        var employeeCol = {
            text: BC.translate('mitarbeiter'),
            width: 65,
            dataIndex: 'shortcut',
            align: 'left'
        };

        //array with the id's of the visible extra columns
        var visibleExtraColsIds = this.record.get('additionalCols'),
            additionalColsStore = Ext.getStore('AdditionalCols'),
            visibleExtraCols = [],
            id,
            name,
            lastColumn;

        if (visibleExtraColsIds !== "") {
            for (var i = 0; i < visibleExtraColsIds.length; ++i) {
                id = visibleExtraColsIds[i];
                name = additionalColsStore.getById(id).get('name');

                switch (id) {
                    case 1:
                        visibleExtraCols.push({
                            text: name,
                            width: 55,
                            dataIndex: 'company',
                            align: 'left'
                        });
                        break;
                    case 2:
                        visibleExtraCols.push({
                            text: name,
                            width: 110,
                            dataIndex: 'division',
                            align: 'left'
                        });
                        break;
                    case 3:
                        visibleExtraCols.push({
                            text: name,
                            width: 110,
                            dataIndex: 'holidayManager',
                            align: 'left'
                        });
                        break;
                    case 4:
                        visibleExtraCols.push({
                            text: name,
                            width: 130,
                            dataIndex: 'staffManager',
                            align: 'left'
                        });
                        break;
                    case 5:
                        visibleExtraCols.push({
                            text: name,
                            width: 75,
                            dataIndex: 'hoursPerWeek',
                            align: 'right'
                        });
                        break;
                    case 6:
                        visibleExtraCols.push({
                            text: name,
                            width: 85,
                            dataIndex: 'holiday',
                            align: 'right'
                        });
                        break;
                    case 7:
                        visibleExtraCols.push({
                            text: name,
                            width: 85,
                            dataIndex: 'empLeavingDate',
                            align: 'left',
                            renderer: Ext.util.Format.dateRenderer('d.m.Y')
                        });
                        break;
                    case 8:
                        visibleExtraCols.push({
                            text: name,
                            width: 85,
                            dataIndex: 'empEntryDate',
                            align: 'left',
                            renderer: Ext.util.Format.dateRenderer('d.m.Y')
                        });
                        break;
                    case 9:
                        visibleExtraCols.push({
                            text: name,
                            width: 85,
                            dataIndex: 'status',
                            align: 'left'
                        });
                        break;
                    case 10:
                        visibleExtraCols.push({
                            id: 'monthEndColSickLeaveDays-' + this.timestamp,
                            text: name,
                            width: 85,
                            dataIndex: 'sickLeaveDays',
                            align: 'left',
                            renderer: this.floatValueOneDecimalRenderer
                        });
                        break;
                    case 11:
                        visibleExtraCols.push({
                            text: name,
                            width: 85,
                            dataIndex: 'takenHolidays',
                            align: 'left'
                        });
                        break;
                    case 12:
                        visibleExtraCols.push({
                            text: name,
                            width: 85,
                            dataIndex: 'acceptedHolidays',
                            align: 'left'
                        });
                        break;
                    case 19:
                        visibleExtraCols.push({
                            text: name,
                            width: 85,
                            dataIndex: 'actOvertime',
                            align: 'right',
                            renderer: this.floatValueRenderer
                        });
                        break;
                    case 20:
                        visibleExtraCols.push({
                            text: BC.translate('hours_accountable_monthend'),
                            width: 85,
                            dataIndex: 'accountableHours',
                            align: 'right',
                            renderer: this.floatValueRenderer
                        });
                        visibleExtraCols.push({
                            text: BC.translate('hours_not_accountable_monthend'),
                            width: 85,
                            dataIndex: 'notAccountableHours',
                            align: 'right',
                            renderer: this.floatValueRenderer
                        });
                        visibleExtraCols.push({
                            text: BC.translate('hours_accountable_percent_monthend'),
                            width: 85,
                            dataIndex: 'accountableHoursPerc',
                            align: 'right',
                            renderer: this.percentageValueRenderer
                        });
                        break;
                    case 21:
                        visibleExtraCols.push({
                            text: name,
                            width: 90,
                            renderer: this.integerRenderer,
                            dataIndex: 'businessKilometers',
                            align: 'right'
                        });
                        break;
                    case 23:
                        visibleExtraCols.push({
                            text: BC.translate('officeDays'),
                            width: 85,
                            dataIndex: 'officeDays',
                            renderer: this.integerRenderer,
                            align: 'right'
                        });
                        break;
                }
            }
            lastColumn = visibleExtraCols.slice(-1).pop();
            if (lastColumn) {
                //add sum label before the actWorkedHours column
                lastColumn.tdCls = 'summary-label';
                lastColumn.summaryRenderer = function() {
                    return BC.translate('summe') + ':';
                };
            }
        } else {
            employeeCol.tdCls = 'summary-label';
            employeeCol.summaryRenderer = function() {
                return BC.translate('summe') + ':';
            };
        }

        //contains all columns of the report which are displayed
        this.gridCols = Ext.Array.merge(
            [{
                text: BC.translate('month'),
                id: 'monthEndColId-' + this.timestamp,
                width: 60,
                dataIndex: 'month',
                align: 'left',
                hidden: true
                //renderer: Ext.util.Format.dateRenderer('Y/m')
            }, {
                text: BC.translate('lastName', 'ucfirst'),
                width: 110,
                dataIndex: 'lastname',
                align: 'left'
            }, {
                text: BC.translate('firstName'),
                width: 110,
                dataIndex: 'firstname',
                align: 'left'
            }],
            [employeeCol],
            visibleExtraCols,
            [{
                text: BC.translate('actualHrs'),
                width: 75,
                dataIndex: 'actWorkedHours',
                renderer: this.floatValueRenderer,
                summaryType: 'sum',
                summaryRenderer: this.floatValueRenderer,
                align: 'right'
            }, {
                text: BC.translate('payHrsRegular'),
                width: 110,
                dataIndex: 'regularHoursToPay',
                renderer: this.floatNotEmptyValueRenderer,
                summaryType: 'sum',
                summaryRenderer: this.floatValueRenderer,
                align: 'right'
            }, {
                text: BC.translate('payHrsNight'),
                width: 100,
                dataIndex: 'nightHoursToPay',
                renderer: this.floatNotEmptyValueRenderer,
                summaryType: 'sum',
                summaryRenderer: this.floatValueRenderer,
                align: 'right'
            }, {
                text: BC.translate('payHrsSunday'),
                width: 110,
                dataIndex: 'holidayHoursToPay',
                renderer: this.floatNotEmptyValueRenderer,
                summaryType: 'sum',
                summaryRenderer: this.floatValueRenderer,
                align: 'right'
            }, {
                text: BC.translate('payOvertimeSurcharge'),
                width: 100,
                dataIndex: 'payOvertimeSurcharge',
                align: 'right'
            }, {
                text: BC.translate('homeOfficeDays'),
                width: 100,
                dataIndex: 'homeOfficeDays',
                renderer: this.integerRenderer,
                summaryType: 'sum',
                summaryRenderer: this.integerRenderer,
                align: 'right'
            }, {
                text: BC.translate('hrsChange'),
                width: 80,
                dataIndex: 'hoursChange',
                hideMode: 'display',
                renderer: this.floatNotEmptyValueRenderer,
                align: 'right'
            }, {
                text: BC.translate('reason'),
                id: 'monthEndColHrsChangeReason-' + this.timestamp,
                width: 100,
                dataIndex: 'hoursChangeReason',
                hideMode: 'display',
                renderer: this.reasonRenderer,
                align: 'left'
            }, {
                text: BC.translate('holidayChange'),
                width: 95,
                dataIndex: 'holidayDaysChange',
                renderer: this.intNotEmptyValueRenderer,
                align: 'right'
            }, {
                text: BC.translate('reason'),
                id: 'monthEndColHolidayChangeReason-' + this.timestamp,
                width: 120,
                dataIndex: 'holidayDaysChangeReason',
                renderer: this.reasonRenderer,
                align: 'left'
            }, {
                text: BC.translate('holidayCurrentMonth'),
                width: 80,
                dataIndex: 'holidayDays',
                align: 'right'
            }, {
                id: 'monthEndColRemainingHours-' + this.timestamp,
                text: BC.translate('remainingHours'),
                width: 100,
                dataIndex: 'remainingHours',
                renderer: function (value) {
                    if (value === null) {
                        value = '---';
                    } else {
                        value = value ? Number(value).toFixed(2) : '0.00';
                    }

                    return value;
                },
                align: 'right'
            }, {
                text: BC.translate('remainingHoliday'),
                width: 100,
                dataIndex: 'remainingHolidayDays',
                renderer: function (value) {
                    if (value === null) {
                        value = '---';
                    }

                    return value;
                },
                align: 'right'
            }]
        );

        this.grid = Ext.create('Ext.grid.Panel', {
            store: this.store,
            enableColumnMove: false,
            disableSelection: true,
            margin: '0 0 0 0',
            cls: 'monthEnd-grid',
            features: [{
                ftype: 'summary'
            }],
            columns: this.gridCols,
            dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    text: BC.translate('export') + '-' + BC.translate('excel'),
                    iconCls: 'excel-ico',
                    action: 'downloadExcel'
                }]
            }]
        });

        this.items = [{
            xtype: 'panel',
            layout: 'fit',
            border: 0,
            height: '100%',
            width: '100%',
            margin: 0,
            padding: 0,
            items: [this.grid]
        }];

        this.callParent(arguments);
    },

    init: function(result) {
        var periodDate = Ext.Date.clearTime(new Date(result.from * 1000)),
            currDate = Ext.Date.getFirstDateOfMonth(Ext.Date.clearTime(new Date())),
            remainingCol = this.grid.headerCt.child('#monthEndColRemainingHours-' + this.timestamp),
            hrsChangeReasonCol = this.grid.headerCt.child('#monthEndColHrsChangeReason-' + this.timestamp),
            holidayChangeReasonCol = this.grid.headerCt.child('#monthEndColHolidayChangeReason-' + this.timestamp);

        if (result.summary.months > 1) {
            this.grid.columns[0].show();
        }

        //hide 'Remaining hours' column
        if (periodDate.getTime() >= currDate.getTime()) {
            remainingCol.hide();
        }
        if (Number(result.flags.oneLinePerEmployee)) {
            Ext.getCmp('monthEndColId-' + this.timestamp).setWidth(110);
            hrsChangeReasonCol.hide();
            holidayChangeReasonCol.hide();
        }
        this.store.loadRecords(this.store.proxy.reader.readRecords(result.bookings).records);
    }
});
