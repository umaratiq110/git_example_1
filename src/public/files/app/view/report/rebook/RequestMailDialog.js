Ext.define('BC.view.report.rebook.RequestMailDialog', {
    extend: 'Ext.window.Window',
    alias: 'widget.rebookrequestmaildialog',

    uses: [
        'BC.view.report.rebook.CostCentreCombo',
        'Ext.form.Panel',
        'Ext.layout.container.Form',
        'Ext.form.field.Hidden'
    ],

    width: 720,
    height: 510,
    border: false,
    modal: true,

    report: null,
    targetCc: '<Ziel-BKZ>',

    initComponent: function () {
        this.comboCostCentres = Ext.create('BC.view.report.rebook.CostCentreCombo', {
            fieldLabel: BC.translate('targetCC'),
            store: 'AllCostCentres',
            allowBlank: false
        });

        this.formPanel = Ext.create('Ext.form.Panel', {
            layout: 'form',
            url: 'report/sendrebookrequestmail',
            method: 'POST',
            bodyPadding: '5 5 0',
            defaults: {
                labelWidth: 75,
                allowBlank: false
            },
            items: [{
                xtype: 'textfield',
                fieldLabel: BC.translate('to'),
                readOnly: true,
                submitValue: false,
                value: global.bookingTreeMail
            }, {
                xtype: 'textfield',
                name: 'subject',
                fieldLabel: BC.translate('betreff'),
                value: "Bitte um Umbuchung"
            },
                this.comboCostCentres
            , {
                xtype: 'textarea',
                name: 'message',
                fieldLabel: BC.translate('message'),
                rows: 25 - (Ext.isGecko & 1) * 4 - ~~Ext.isIE,
                value: "Folgende Stunden sollen bitte umgebucht werden:\n\n" + this.getFormattedBookings()
            }, {
                xtype: 'hiddenfield',
                name: 'targetCC'
            }]
        });

        Ext.apply(this, {
            title: BC.translate('sendRebookingRequestMail'),
            layout: 'fit',
            items: [this.formPanel],
            buttons: [{
                text: BC.translate('send'),
                handler: function () {
                    if (!this.formPanel.isValid()) {
                        return;
                    }

                    this.formPanel.mask();

                    this.formPanel.submit({
                        success: function () {
                            this.close();
                        },
                        failure: function () {
                            Ext.Msg.show({
                                title: BC.translate('undefinederror_subject'),
                                msg: BC.translate('allgemeinerfehler'),
                                buttons: Ext.Msg.OK,
                                icon: Ext.Msg.ERROR
                            });
                            this.formPanel.unmask();
                        },
                        scope: this
                    });
                },
                scope: this
            }, {
                text: BC.translate('abbrechen'),
                handler: function () {
                    this.close();
                },
                scope: this
            }]
        });

        this.callParent();
    },

    initEvents: function () {
        this.callParent();

        this.formPanel.on({
            'beforeaction': function(form) {
                form.findField('targetCC').setValue(this.comboCostCentres.getRawValue());
            },
            scope: this
        });
    },

    getFormattedBookings: function () {
        var text = "", btText, note = "", newNote = "",
            rebookedHoursSum = 0;

        Ext.Array.each(this.report.getRebookingRecords(), function (booking) {
            var record = this.findRecord('bookingId', booking.id, false, false, true);
            btText = [];
            Ext.Object.each(booking, function (key, hours) {
                if (!Ext.Array.contains(['FP', 'CR', 'TM', 'MC', 'NA'], key) || hours === 0) {
                    return;
                }
                rebookedHoursSum += hours;
                note = Ext.String.trim(record.get('note'));
                newNote = Ext.String.trim(record.get('newNote'));

                btText.push(Ext.String.format(
                    "{0} Std von {1} vom {2} von BKZ '{3}'",
                    Ext.util.Format.number(hours, '0.00'),
                    record.get('employee'),
                    Ext.Date.format(record.get('date'), BC.translate('dateFormat')),
                    record.get('cc')
                ));

                if (note) {
                    btText.push(Ext.String.format(
                    "mit Notiz '{0}'",
                    note
                    ));
                }

                btText.push(Ext.String.format("auf Buchungstyp {0}", key));

                if (newNote && newNote !== note) {
                    btText.push(Ext.String.format("mit neuer Notiz '{0}'", newNote));
                }

                text += btText.join(" ") + ".\n";

            });
        }, this.report.getStore());

        text += "\nSumme der umzubuchenden Stunden: " + Ext.util.Format.number(rebookedHoursSum, '0.00');

        return text;
    }
});
