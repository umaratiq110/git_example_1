Ext.define('BC.view.report.rebook.CostCentreCombo', {
    extend: 'Ext.form.field.ComboBox',

    store: 'CostCentres',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'id',
    anyMatch: true,
    typeAhead: true,
    forceSelection: true,
    listConfig: {},

    initComponent: function () {
        Ext.applyIf(this.listConfig, {
            itemTpl: new Ext.XTemplate(
                '<span class="{[this.getClass(values)]}">',
                    '{name} ({[this.getBookingType(values.bookingTypeId)]})',
                    '<tpl if="notYetBookable">',
                        Ext.String.format(' -- ({0})', BC.translate('notYetBookableSuffix')),
                    '</tpl>',
                '</span>',
                {
                    storeBookingTypes: Ext.data.StoreManager.lookup('BookingTypes'),
                    getBookingType: function (id) {
                        return this.storeBookingTypes.getById(id).get('name');
                    },
                    getClass: function (values) {
                        var cls = [];
                        if (values.bookable === false) {
                            cls.push('locked-item');
                        }
                        return cls.join(" ");
                    }
                }
            )
        });

        this.callParent();
    }
});
