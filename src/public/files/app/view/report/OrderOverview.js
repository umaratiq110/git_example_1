Ext.define('BC.view.report.OrderOverview', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.orderOverview',

    mixins: ['BC.view.report.Abstract'],

    initComponent: function() {
        Ext.apply(this, {
            autoScroll: true,
            dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    text: BC.translate('export') + '-' + BC.translate('excel'),
                    iconCls: 'excel-ico',
                    action: 'downloadExcel'
                }]
            }]
        });

        this.callParent(arguments);
    },

    init: function(result) {
        this.update(result.view);
    }
});
