Ext.define('BC.view.report.Status', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.status',

    mixins: ['BC.view.report.Abstract'],

    initComponent: function() {
        Ext.apply(this, {
            autoScroll: true,
            dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    text: BC.translate('export') + '-' + BC.translate('excel'),
                    iconCls: 'excel-ico',
                    action: 'downloadExcel'
                }]
            }]
        });

        this.callParent(arguments);
    },

    init: function(result) {
        this.update(result.view);

        var header = this.getEl().down(".status-head"), body = this.getEl().down(".status-body");
        this.body.on('scroll', function() {
            header.setTop(this.body.dom.scrollTop);
        }, this);

        body.setTop(header.getHeight());
    }
});
