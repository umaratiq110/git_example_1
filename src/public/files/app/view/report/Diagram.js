Ext.define('BC.view.report.Diagram', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.diagram',

    mixins: ['BC.view.report.Abstract'],

    diagramImage: null,
    legendPosition: 'left',

    initComponent: function() {
        this.diagramImage = new Ext.Img({
            src: 'files/images/s.gif'
        });
        this.diagramImageLoader = new Ext.util.DelayedTask(this.reloadDiagramImage, this);

        this.items = [this.diagramImage];

        this.tbBtnSwitchLegendPosition = new Ext.Button({
            iconCls: 'switchlegend-ico',
            action: 'btnSwitchLegendPosition',
            text: BC.translate('positionlegendeaendern')
        });

        this.tbar = [this.tbBtnSwitchLegendPosition];

        this.callParent(arguments);
    },

    init: function() {
        this.reloadDiagramImage();
    },

    destroy: Ext.emptyFn,

    refresh: function () {
        this.diagramImageLoader.delay(1000);
    },

    reloadDiagramImage: function () {
        var me = this,
            bodyEl = me.body;

        me.diagramImage.setSrc(
            'index/image' +
            '/profile/' + me.record.get("id") +
            '/timestamp/' + me.timestamp +
            '/legendPosition/' + me.legendPosition +
            '/width/' + bodyEl.getWidth() +
            '/height/' +  bodyEl.getHeight()
        );
    },

    switchLegendPosition: function () {
        var me = this;
        me.legendPosition = Ext.String.toggle(me.legendPosition, 'left', 'right');
        me.refresh();
    }
});
