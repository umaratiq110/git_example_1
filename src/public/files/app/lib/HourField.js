/**
 * Class for creating a field for providing hour values
 *
 * @class BC.lib.HourField
 * @extends Ext.form.field.Text
 * @author Robert Lesniak <les@isogmbh.de>
 *     */
Ext.define('BC.lib.HourField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.hourfield',

    allowNegative: false,
    maskRe: /([0-9,\.])/,
    formatRe: /^([0-9]+([,\.]((25)|(00)|(50)|(5)|(0)|(75)))?)?$/,

    initComponent: function() {
        this.callParent(arguments);

        if (this.allowNegative === true) {
            this.maskRe = /([0-9,\.\-])/;
            this.formatRe = /^(\-?[0-9]+([,\.]((25)|(00)|(50)|(5)|(0)|(75)))?)?$/;
        }
    },

    processRawValue: function(value) {
        value = value.replace(',', '.');

        return this.callParent([value]);
    },

    getErrors: function(value) {
        var errors = this.callParent(arguments);

        if (this.formatRe.test(value) === false) {
            errors.push(this.invalidText);
        }

        return errors;
    }
});
