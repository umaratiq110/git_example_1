Ext.define('BC.lib.Multiselect', {
    extend: 'Ext.form.field.ComboBox',

    alias: 'widget.Multiselect',

    trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
    trigger2Cls: Ext.baseCSSPrefix + 'form-arrow-trigger',

    multiSelect: true,
    triggerAction: 'all',
    queryMode: 'local',

    onTrigger1Click: function() {
        this.reset();
    }
});
