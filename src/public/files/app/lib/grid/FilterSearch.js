/**
 * @class BC.lib.grid.FilterSearch
 * <p>A plugin for searching and filtering support. This plugin allowed to select the columns for the filter.</p>
 * requirements: store, grid, indexes, searchfield
 */
Ext.define('BC.lib.grid.FilterSearch', {
    /**
     * @private
     * search Field initialization
     */
    searchField: null,

    /**
     * @private
     * search value initialization
     */
    searchValue: null,

    /**
     * @private
     * grid for the searching
     */
    grid: null,

    /**
     * @private
     * store for the filtering
     */
    store: null,

    /**
     * @private
     * Columns from the grid used for the store filter
     */
    indexes: [],

    /**
     * @private
     * The row index of the first search, it could change if next or previous buttons are used.
     */
    currentIndex: null,

    /**
     * @private
     * The generated regular expression used for searching.
     */
    searchRegExpFilter: null,

    /**
     * @cfg {String} matchCls
     * The matched string css classe.
     */
    matchCls: 'x-livesearch-match',

    // detects html tag
    tagsRe: /<[^>]*>/gm,

    // DEL ASCII code
    tagsProtect: '\x0f',

    // detects regexp reserved word
    regExpProtect: /\\|\/|\+|\\|\.|\[|\]|\(|\)|\{|\}|\?|\$|\*|\^|\|/gm,

    constructor: function(config) {
        Ext.apply(this, config);
    },

    /**
     * In normal mode it returns the value with protected regexp characters.
     * In regular expression mode it returns the raw value except if the regexp is invalid.
     * @return {String} The value to process or null if the textfield value is blank or invalid.
     * @private
     */
    prepareSearchValue: function(searchValue) {
        var me = this;

        if (searchValue === '') {
            return null;
        }

        searchValue = searchValue.replace(me.regExpProtect, function(m) {
            return '\\' + m;
        });

        var length = searchValue.length, resultArray = [me.tagsProtect + '*'], i = 0, c;

        for (; i < length; i++) {
            c = searchValue.charAt(i);
            resultArray.push(c);
            if (c !== '\\') {
                resultArray.push(me.tagsProtect + '*');
            }
        }
        return resultArray.join('');
    },

    /**
     * Finds all strings that matches the searched value in each grid cells.
     * @private
     */
    onTextFieldChange: function(searchValue, sort) {
        var me = this,
            count = 0,
            indexes = me.indexes,
            idx,
            index;

        me.grid.view.refresh();

        if (searchValue !== false) {
            me.searchValue = me.prepareSearchValue(searchValue);
        }

        me.currentIndex = null;

        //---clear previous filter at search
        if (sort !== true) {
            me.store.clearFilter();
        }
        if (me.searchValue !== null) {
            //RegEx for mark the found matches(globl search)
            me.searchRegExpMark = new RegExp(me.searchValue, 'gi');
            //RegEx for filter the store
            me.searchRegExpFilter = new RegExp(me.searchValue, 'i');

            var regexp = me.searchRegExpFilter;
            if (sort !== true) {
                me.store.filterBy(function(obj) {
                    var test = false;

                    for (idx in indexes) {
                        index = indexes[idx];

                        test = test || regexp.test(obj.data[index]);
                    }
                    return test;
                });
            }

            me.store.each(function(record, recordIndex) {
                var td = Ext.fly(me.grid.view.getNode(recordIndex)).down('td'),
                    cell,
                    matches,
                    cellHTML,
                    generateCellHtml = function(m) {
                        count += 1;
                        if (me.currentIndex === null) {
                            me.currentIndex = recordIndex;
                        }
                        return '<span class="' + me.matchCls + '">' + m + '</span>';
                    },
                    replaceCellHtml = function(match) {
                        cellHTML = cellHTML.replace(me.tagsProtect, match);
                    };
                while (td) {
                    cell = td.down('.x-grid-cell-inner');
                    matches = cell.dom.innerHTML.match(me.tagsRe);
                    cellHTML = cell.dom.innerHTML.replace(me.tagsRe, me.tagsProtect);

                    // set currentIndex, and replace wrap matched string in a span
                    cellHTML = cellHTML.replace(me.searchRegExpMark, generateCellHtml);
                    // restore protected tags
                    Ext.each(matches, replaceCellHtml);
                    // update cell html
                    cell.dom.innerHTML = cellHTML;
                    td = td.next();
                }
            }, me);
        }

        // no results found
        if (me.currentIndex === null) {
            me.grid.getSelectionModel().deselectAll();

            // force textfield focus
            me.searchField.focus();
        }
    }
});
