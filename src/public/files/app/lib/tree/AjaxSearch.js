Ext.define('BC.lib.tree.AjaxSearch', {
    extend: 'BC.lib.tree.LiveSearch',

    alias: 'plugin.treeajaxsearch',

    preloader: {
        enabled: true,
        isLoading: false,
        queue: [],
        data: {}
    },

    init: function() {
        var me = this;

        me.callParent(arguments);

        me.tree.addEvents({
            searchcomplete: true,
            searchexpand: true
        });
    },

    search: function(searchValue) {
        var me = this;

        if (!searchValue && me.searchField !== null) {
            searchValue = me.searchField.getValue();
        }

        searchValue = Ext.String.trim(searchValue);
        if (searchValue === '') {
            return null;
        }

        me.searchValue = searchValue;

        me.indexes = [];
        me.currentIndex = null;

        Ext.Ajax.request({
            url: "tree/search",
            params: Ext.Object.toQueryString({
                criteria: me.nodeMatchField,
                phrase: searchValue
            }, true),
            success: function(response) {
                var respObj = Ext.JSON.decode(response.responseText);

                if (Ext.isArray(respObj) && respObj.length > 0) {
                    me.indexes = respObj;
                }

                me.tree.fireEvent('searchcomplete', me.tree, me, me.indexes);

                Ext.suspendLayouts();
                me.tree.collapseAll();

                if (me.indexes.length > 0) {
                    // results found
                    me.tree.getView().refresh();
                    me.highlightNodes();

                    me.tree.on('afteritemexpand', me.highlightNodes, me);

                    me.currentIndex = -1;
                    me.next();
                } else {
                    // no results found
                    me.tree.getSelectionModel().deselectAll();
                    me.refresh();
                }

                Ext.resumeLayouts(true);

                // force textfield focus
                if (me.searchField) {
                    me.searchField.focus();
                }
            },
            scope: this
        });
    },

    clear: function() {
        var me = this;
        me.callParent(arguments);
        me.tree.un('afteritemexpand', me.highlightNodes, me);
    },

    next: function() {
        var me = this;
        if (me.currentIndex !== null) {
            me.currentIndex = me.getNextIndex();
            me.expandNode(me.indexes[me.currentIndex]);
        }
    },

    previous: function() {
        var me = this;
        if (me.currentIndex !== null) {
            me.currentIndex = me.getPreviousIndex();
            me.expandNode(me.indexes[me.currentIndex]);
        }
    },

    getNextIndex: function() {
        var me = this,
            i = me.currentIndex;

        if (i !== null) {
            if (i + 1 === me.indexes.length) {
                i = -1;
            }
            return i + 1;
        }
    },

    getPreviousIndex: function() {
        var me = this,
            i = me.currentIndex;

        if (i !== null) {
            if (i === 0) {
                i = me.indexes.length;
            }
            return i - 1;
        }
    },

    expandNode: function(item) {
        var me = this,
            loadedNode;

        loadedNode = me.loadNode(item, function(node, children) {
            var store = me.tree.getStore();

            Ext.suspendLayouts();
            me.mergeTree(children, node);

            node = store.getNodeById('n' + item.ID);
            me.tree.selectPath(node.getPath());
            me.tree.scrollByDeltaY(20);
            me.refresh();
            Ext.resumeLayouts(true);

            me.tree.fireEvent('searchexpand', me.tree, me, node);

            if (this.preloader.enabled === true) {
                me.preloadNode(item);
            }
        }, this);

        // if function returns a node directly, the node was already loaded in
        // the tree, otherwise the callback function above will handle the node
        // data once it was returned by the AJAX request
        if (loadedNode instanceof Ext.data.Model) {
            me.tree.selectPath(loadedNode.getPath());
            me.tree.scrollByDeltaY(20);
            me.refresh();

            me.tree.fireEvent('searchexpand', me.tree, me, loadedNode);

            if (this.preloader.enabled === true) {
                me.preloadNode(item);
            }
        }
    },

    loadNode: function(item, callback, scope) {
        var me = this,
            store = me.tree.getStore(),
            preloadData,
            node,
            child,
            path;

        if (!scope) {
            scope = this;
        }

        // check if node already exists in store
        node = store.getNodeById('n' + item.ID);
        if (node) {
            return node;
        }

        // check if node already was preloaded
        preloadData = me.preloader.data[item.PATH];
        if (preloadData) {
            callback.apply(scope, preloadData);
            return undefined;
        }

        // load the node
        path = item.PATH.split('/');

        for (var i = 0; i < path.length; i++) {
            child = store.getNodeById('n' + path[i]);
            if (!child) {
                break;
            }
            node = child;
        }

        Ext.Ajax.request({
            url: "tree/gettree",
            params: {
                start: node.get('id'),
                end: item.ID
            },
            success: function(response) {
                var respObj = Ext.JSON.decode(response.responseText);

                if (respObj) {
                    callback.call(scope, node, respObj[0].children);
                }
            },
            scope: me
        });
    },

    preloadNode: function(item) {
        var me = this,
            store = me.tree.getStore(),
            items,
            node;

        // check current node
        node = store.getNodeById('n' + item.ID);
        if (!node) {
            // cannot preload if the current node was not loaded yet
            return false;
        }

        // check next and previous node
        items = [
            me.indexes[me.getNextIndex()],
            me.indexes[me.getPreviousIndex()]
        ];
        for (var i = 0; i < items.length; i++) {
            node = store.getNodeById('n' + items[i].ID);
            if (!node && !Ext.Array.contains(me.preloader.queue, items[i])) {
                // preload nodes
                me.preloader.queue.push(items[i]);
            }
        }

        me.preload();
    },

    /**
     * Preload items in the queue
     */
    preload: function(force) {
        var me = this,
            item,
            n,
            preLoadItemData;

        if (me.preloader.queue.length === 0
            || me.preloader.isLoading && !force
        ) {
            return;
        }

        preLoadItemData = function(node, children) {
            this.preloader.data[item.PATH] = [node, children];

            if (this.preloader.queue.length > 0) {
                return this.preload(true);
            }
            this.preloader.isLoading = false;
        };
        do {
            item = me.preloader.queue.shift();
            me.preloader.isLoading = true;
            n = me.loadNode(item, preLoadItemData, me);
        } while (n);
    },

    /**
     * Merge nodes to parent tree
     */
    mergeTree: function(nodes, parent) {
        var node,
            store = this.tree.getStore();

        for (var i = 0; i < nodes.length; i++) {
            node = store.getById(nodes[i].uid);
            if (node != null) {
                if (nodes[i].children !== undefined) {
                    this.mergeTree(nodes[i].children, node);
                }
            } else {
                if (parent === undefined) {
                    parent = store.getRootNode();
                }
                node = parent.createNode(nodes[i]);
                parent.appendChild(node);
            }
        }
    },

    highlightNodes: function() {
        var me = this,
            store = this.tree.getStore(),
            node;

        for (var i = 0; i < me.indexes.length; i++) {
            node = store.getById('n' + me.indexes[i].ID);
            if (node) {
                me.highlightNode(node);
            }
        }
    },

    enablePreloader: function () {
        this.preloader.enabled = true;
    },

    disablePreloader: function() {
        this.preloader.enabled = false;
    }
});
