/**
 * @class BC.lib.tree.NodeDisabled
 * @extends Ext.AbstractPlugin
 * @ptype nodedisabled
 *
 * A plugin that provides the ability to visually indicate to the user that a node is disabled.
 *
 *
 * Notes:
 *
 * - Compatible with Ext 4.x
 * - If the view already defines a getRowClass function, the original function will be called before this plugin.
 * - An Ext.data.Model must be defined for the store that includes the 'disabled' field.
        Ext.define('MyTreeModel', {
            extend: 'Ext.data.Model'
            ,fields: [
                {name: 'disabled', type:'bool', defaultValue:false}
                ...
            ]
        });
 *
 * Example usage:
        var tree = Ext.create('Ext.tree.Panel', {
            plugins: [{
                ptype: 'nodedisabled'
            }]
            ...
        });

 *
 * @author Phil Crawford
 * @license Licensed under the terms of the Open Source [LGPL 3.0 license](http://www.gnu.org/licenses/lgpl.html).
 * Commercial use is permitted to the extent that the code/component(s) do NOT become part of another Open Source
 * or Commercially licensed development library or toolkit without explicit permission.
 * @version 0.3 (February 2, 2012) Intercept 'onCheckChange' to cancel the event instead of overriding.
 * @constructor
 * @param {Object} config
 */
Ext.define('BC.lib.tree.NodeDisabled', {
    alias: 'plugin.nodedisabled',
    extend: 'Ext.AbstractPlugin',

    //configurables
    /**
     * @cfg {String} disabledCls
     * The CSS class applied when the {@link Ext.data.Model} of the node has a 'disabled' field with a true value.
     */
    disabledCls: 'tree-node-disabled',
    /**
     * @cfg {Boolean} preventSelection
     * True to prevent selection of a node that is disabled. Default true.
     */
    preventSelection: true,

    /**
     * @cfg {Function} disabledFn
     * A custom function which is passed each node in the Tree. Should return
     * `true` to disable a node.
     */
    disabledFn: null,

    //properties

    /**
     * @private
     * @param {Ext.tree.Panel} tree
     */
    init: function(tree) {
        var me = this,
            view = tree.getView(),
            origFn,
            origScope;

        me.callParent(arguments);

        origFn = view.getRowClass;
        if (origFn) {
            origScope = view.scope || me;
            Ext.apply(view, {
                //append our value to the original function's return value
                getRowClass: function() {
                    var v1, v2;
                    v1 = origFn.apply(origScope, arguments) || '';
                    v2 = me.getRowClass.apply(me, arguments) || '';
                    return v1 && v2 ? v1 + ' ' + v2 : v1 + v2;
                }
            });
        } else {
            Ext.apply(view, {
                getRowClass: Ext.Function.bind(me.getRowClass, me)
            });
        }

        Ext.apply(view, {
            //if our function returns false, the original function is not called
            onCheckChange: Ext.Function.createInterceptor(view.onCheckChange, me.onCheckChangeInterceptor, me)
        });

        if (me.preventSelection) {
            tree.getSelectionModel().on('beforeselect', me.onBeforeNodeSelect, me);
        }
    }, // eof init

    /**
     * Returns a properly typed result.
     * @return {Ext.tree.Panel}
     */
    getCmp: function() {
        return this.callParent(arguments);
    }, //eof getCmp

    /**
     * @private
     * @param {Ext.data.Model} record
     * @return {String}
     */
    getRowClass: function(record) {
        if (this.isNodeDisabled(record)) {
            return this.disabledCls;
        }

        return '';
    },//eof getRowClass

    /**
     * @private
     * @param {Ext.selection.TreeModel} sm
     * @param {Ext.data.Model} node
     * @return {Boolean}
     */
    onBeforeNodeSelect: function(sm, node) {
        if (this.isNodeDisabled(node)) {
            return false;
        }

        return true;
    },//eof onBeforeNodeSelect

    /**
     * @private
     * @param {Ext.data.Model} record
     */
    onCheckChangeInterceptor: function(record) {
        if (this.isNodeDisabled(record)) {
            return false;
        }

        return true;
    },//eof onCheckChange

    /**
     * @private
     * @param {Ext.data.Model} record
     */
    isNodeDisabled: function(record) {
        if (record.get('disabled')) {
            return true;
        }

        if (this.disabledFn !== null && typeof this.disabledFn === 'function') {
            return this.disabledFn(record);
        }

        return false;
    }//eof onCheckChange
});
