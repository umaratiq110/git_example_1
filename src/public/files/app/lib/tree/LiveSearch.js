
Ext.define('BC.lib.tree.LiveSearch', {
    extend: 'Ext.AbstractPlugin',

    alias: 'plugin.treelivesearch',

    tree: null,

    searchField: null,
    btnPrev: null,
    btnNext: null,
    statusField: null,

    defaultStatusText: '{0} of {1}',
    excludeFn: Ext.emptyFn,
    nodeMatchField: 'text',
    nodeMatchFn: Ext.emptyFn,
    searchValue: null,
    searchRegExp: null,
    caseSensitive: false,
    regExpMode: false,
    matchCls: 'livesearch-match',
    tagsRe: /<[^>]*>/gm,
    tagsProtect: '\x0f',
    regExpProtect: /\\|\/|\+|\-|\\|\.|\[|\]|\{|\}|\?|\$|\*|\^|\(|\)/gm,
    indexes: [],
    currentIndex: null,

    init: function (tree) {
        this.tree = tree;
        tree.liveSearch = this;

        // validate required attributes
        Ext.each(['searchField', 'btnNext', 'btnPrev', 'statusField'], function(attribute) {
            // try to take attribute from the tree if it's not given in the plugin
            if (!this[attribute] && this.tree[attribute]) {
                this[attribute] = this.tree[attribute];
            }
        }, this);
    },

    destroy: function() {
        // garbage collection
        this.tree.liveSearch = null;
        this.tree = null;
    },

    search: function(searchValue) {
        var me = this;

        if (!searchValue && me.searchField !== null) {
            searchValue = me.searchField.getValue();
        }

        searchValue = Ext.String.trim(searchValue);
        if (searchValue === '') {
            return null;
        }

        me.searchValue = searchValue;
        searchValue = me.escapeSearchValue(searchValue);

        me.indexes = [];
        me.currentIndex = null;
        if (searchValue !== null) {
            me.tree.view.refresh();

            me.searchRegExp = new RegExp(searchValue, 'g' + (me.caseSensitive ? '' : 'i'));

            me.tree.collapseAll();
            var rootNode = me.tree.getRootNode(),
                matchedNodes = [];

            rootNode.cascadeBy(function(record) {
                if (record.isRoot() && !me.tree.rootVisible) {
                    return;
                }

                if (me.excludeFn !== Ext.emptyFn) {
                    if (me.excludeFn(record)) {
                        return;
                    }
                }

                var nodePath = record.getPath();
                var matchCount = matchedNodes[nodePath] || 0;
                matchedNodes[nodePath] = matchCount;

                var nodeValue = me.nodeMatchField === 'text' ? record.get(me.nodeMatchField) : null,
                    matchResult = me.nodeMatchField === 'text' ? nodeValue.match(me.searchRegExp) : me.nodeMatchFn(record, me.nodeMatchField, me.searchValue);

                if (matchResult) {
                    matchCount = matchedNodes[nodePath] || 0;
                    matchedNodes[nodePath] = matchCount + 1;

                    me.indexes.push(nodePath);
                    me.currentIndex = 0;
                }
            }, this);

            var expandOrder = [];
            for (var i in matchedNodes) {
                if (matchedNodes[i] > 0) {
                    var path = i.split(/\//);
                    expandOrder.push({
                        path: i.replace(/\/\d+$/, ''),
                        pathLength: path.length - 1,
                        lastPathId: path.pop()
                    });
                }
            }

            //Sort by longest path
            var expandOrderSorted = Ext.Array.sort(expandOrder, function(first, second) {
                return second.pathLength - first.pathLength;
            });

            Ext.suspendLayouts();
            Ext.Array.each(expandOrderSorted, function(item) {
                me.tree.expandPath(item.path);

                var node = me.tree.getStore().getNodeById(item.lastPathId);

                me.highlightNode(node);
            });
            Ext.resumeLayouts(true);

            // Results found
            if (me.currentIndex !== null) {
                me.tree.selectPath(me.indexes[me.currentIndex]);
                me.tree.scrollByDeltaY(20);
                me.refresh();
            }

            // No results found
            if (me.currentIndex === null) {
                me.tree.getSelectionModel().deselectAll();
                me.refresh();
            }

            // Force textfield focus
            if (me.searchField) {
                me.searchField.focus();
            }
        }
    },

    highlightNode: function(node) {
        var me = this,
            domNode = me.tree.getView().getNode(node),
            td = Ext.fly(domNode),
            searchRegExpHTMLEncoded = new RegExp(Ext.htmlEncode(me.escapeSearchValue(me.searchValue)), 'g' + (me.caseSensitive ? '' : 'i')),
            cell,
            matches,
            cellHTML;

        // If search is not done by 'text' attribute mark whole node
        if (me.nodeMatchField !== 'text') {
            var nodeText = node.get('text').replace(me.regExpProtect, function(m) {
                return '\\' + m;
            });

            searchRegExpHTMLEncoded = new RegExp(Ext.htmlEncode(nodeText), 'gi');
        }

        if (td) {
            // make sure match class is not applied twice
            if (td.down('.' + me.matchCls) !== null) {
                return;
            }

            cell = td.down('.x-grid-cell-inner');
            matches = cell.dom.innerHTML.match(me.tagsRe);
            cellHTML = cell.dom.innerHTML.replace(me.tagsRe, me.tagsProtect);
            cellHTML = cellHTML.replace(searchRegExpHTMLEncoded, function(m) {
                return '<span class="' + me.matchCls + '">' + m + '</span>';
            });

            // Restore from HTMLElement
            Ext.each(matches, function(match) {
                cellHTML = cellHTML.replace(me.tagsProtect, match);
            });

            // Cell will appear with search results
            cell.dom.innerHTML = cellHTML;
        }
    },

    isModified: function() {
        var me = this;

        if (me.searchField) {
            var searchValue = Ext.String.trim(me.searchField.getValue());
            return me.searchValue !== searchValue;
        }
        return false;
    },

    clear: function(clearSearchField) {
        var me = this;

        clearSearchField = !!clearSearchField;

        me.tree.getView().refresh();
        if (me.tree.getRootNode().getChildAt(0)) {
            me.tree.selectPath(me.tree.getRootNode().getChildAt(0).getPath());
        }
        me.tree.getSelectionModel().deselectAll();

        me.searchValue = null;
        me.indexes = [];
        me.currentIndex = null;

        if (me.searchField && clearSearchField) {
            me.searchField.setValue('');
        }

        if (me.btnPrev) {
            me.btnPrev.disable();
        }

        if (me.btnNext) {
            me.btnNext.disable();
        }

        if (me.statusField) {
            me.statusField.update('');
        }
    },

    next: function() {
        var me = this;
        if (me.currentIndex !== null && me.indexes.length > 1) {
            if (me.indexes.length === me.currentIndex + 1) {
                me.currentIndex = -1;
            }
            me.tree.selectPath(me.indexes[++me.currentIndex]);
            me.tree.scrollByDeltaY(20);

            me.refresh();
        }
    },

    previous: function() {
        var me = this;
        if (me.currentIndex !== null) {
            if (me.currentIndex === 0) {
                me.currentIndex = me.indexes.length;
            }
            me.tree.selectPath(me.indexes[--me.currentIndex]);
            me.tree.scrollByDeltaY(20);

            me.refresh();
        }
    },

    refresh: function() {
        var me = this;

        if (me.btnPrev) {
            me.btnPrev.setDisabled(me.indexes.length < 2);
        }

        if (me.btnNext) {
            me.btnNext.setDisabled(me.indexes.length < 2);
        }

        if (me.statusField) {
            me.statusField.update(me.getStatusText());
        }
    },

    getTotalCount: function() {
        return this.indexes.length;
    },

    getCurrentIndex: function() {
        return this.currentIndex === null ? 0 : this.currentIndex + 1;
    },

    getStatusText: function() {
        var me = this;

        if (me.statusField === null) {
            return '';
        }

        return me.getTotalCount() > 0 ? Ext.String.format(me.defaultStatusText, me.getCurrentIndex(), me.getTotalCount()) : '';
    },

    escapeSearchValue: function(searchValue) {
        if (!this.regExpMode) {
            searchValue = Ext.String.escapeRegex(searchValue);
        } else {
            try {
                /*eslint-disable*/
                new RegExp(searchValue);
                /*eslint-enable*/
            } catch (e) {
                searchValue = Ext.String.escapeRegex(searchValue);
            }
        }

        return searchValue;
    }
});
