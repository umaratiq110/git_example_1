Ext.define('BC.lib.tooltip.ConfigInfo', {
    id: 'configIntoToolTip',
    tpl: new Ext.XTemplate(
        '<tpl for=".">',
            '<div><b>{key}:</b> {value}</div>',
        '</tpl>',
        {
            compiled: true,
            disableFormats: true
        }
    ),
    singleTpl: new Ext.XTemplate(
        '<tpl for=".">',
            '<div>{value}</div>',
        '</tpl>',
        {
            compiled: true,
            disableFormats: true
        }
    ),

    show: function(record, position) {
        var toolTip = this.getToolTipComponent(),
            reportType = record.get('type'),
            nodeStore = Ext.getStore('Nodes'),
            employeeStore = Ext.getStore('Employees'),
            btStore = Ext.getStore('BookingTypes'),
            additionalColsStore = Ext.getStore('AdditionalCols'),
            configOptionArr = [];

        // Nodes/Sets
        if (/^[DLTBNARSOFHV]$/.test(reportType) === true) {
            var nodes = this.getNodes(record, nodeStore, true),
                sets = this.getNodes(record, nodeStore, false);

            if (nodes.length > 0) {
                configOptionArr.push({key: BC.translate('nodes'), value: this.arrayToMultiline(nodes)});
            }

            if (sets.length > 0) {
                configOptionArr.push({key: BC.translate('sets'), value: this.arrayToMultiline(sets)});
            }
        }

        if (/^[I]$/.test(reportType) === true) {
            var debtor = this.getCheckedNodes(record),
                internal = this.getSingleNodes(record, BC.store.SingleNodeTypes.MIS_INTERNAL),
                sales = this.getSingleNodes(record, BC.store.SingleNodeTypes.MIS_SALES);

            if (debtor.length > 0) {
                configOptionArr.push({key: BC.translate('debtors'), value: this.arrayToMultiline(debtor)});
            }
            if (sales.length > 0) {
                configOptionArr.push({key: BC.translate('singleNodeSales'), value: this.arrayToMultiline(sales)});
            }
            if (internal.length > 0) {
                configOptionArr.push({key: BC.translate('singleNodeInternal'), value: this.arrayToMultiline(internal)});
            }
        }

        if (/^[K]$/.test(reportType) === true) {
            var project = this.getCheckedNodes(record),
                distinctRow = this.getDistinctRowNodes(record);
            if (project.length > 0) {
                configOptionArr.push({key: BC.translate('singleNodeStartPart'), value: this.arrayToMultiline(project)});
            }

            if (distinctRow.length > 0) {
                configOptionArr.push({key: BC.translate('singleNodeDistinctRow'), value: this.arrayToMultiline(distinctRow)});
            }
        }

        // Employees/Groups
        if (/^[DLTEBRMSPHCGAQY]$/.test(reportType) === true) {
            var employees = this.getEmployees(record, employeeStore, true),
                groups = this.getEmployees(record, employeeStore, false);

            if (employees.length > 0) {
                configOptionArr.push({key: BC.translate('employees'), value: this.arrayToMultiline(employees)});
            }

            if (groups.length > 0) {
                configOptionArr.push({key: BC.translate('groups'), value: this.arrayToMultiline(groups)});
            }
            if (/^[P]$/.test(reportType) === true) {
                if (groups.length === 0 && employees.length === 0) {
                    configOptionArr.push({key: BC.translate('groups'), value: "Alle"});
                }
            }
        }

        // Diagram type/Value type
        if (/^[D]$/.test(reportType) === true) {
            var valueType = record.get('diagramType').substring(0, 1), //T,M
                diagrammType = record.get('diagramType').substring(1, 2), //K,E
                startValue = record.get('startValue'), //J,N
                configValue;

            if (diagrammType === 'K') {
                if (startValue === 'J') {
                    configValue = BC.translate('cumulatedWithStarValue', 'ucfirst');
                } else {
                    configValue = BC.translate('cumulatedWithOutStarValue', 'ucfirst');
                }
            } else {
                configValue = BC.translate('einzeln', 'ucfirst');
            }

            configOptionArr.push({
                key: BC.translate('diagrammtyp'),
                value: configValue
            });
            configOptionArr.push({key: BC.translate('valueType'), value: valueType === 'T' ? BC.translate('tageswerte') : BC.translate('monatswerte')});
        }

        // CC with hour bookings/xAxis
        if (/^[T]$/.test(reportType) === true) {
            var ccWithHourBookings = record.get('ccWithHourBookings'), //1,0
                xAxis = record.get('xAxis'); //E,M

            configOptionArr.push({key: BC.translate('displayCCWithHourBookings'), value: ccWithHourBookings ? BC.translate('onlyBooked') : BC.translate('allCCs')});
            configOptionArr.push({key: BC.translate('xAxis'), value: xAxis === 'E' ? BC.translate('employees') : BC.translate('monatswerte')});
        }

        // Booking note
        if (/^[DLTBR]$/.test(reportType) === true) {
            var note = record.get('hoursNote');
            if (note !== '') {
                configOptionArr.push({key: BC.translate('bc_notiztext'), value: record.get('hoursNote')});
            }
        }

        // Additional columns
        if (/^[LM]$/.test(reportType) === true) {
            var values = [];
            Ext.Array.each(record.get('additionalCols'), function(extraCol) {
                var name = additionalColsStore.getById(extraCol).get('name');
                if (Ext.Array.contains(values, name) === false) {
                    values.push(name);
                }
            });
            if (values.length > 0) {
                configOptionArr.push({key: BC.translate('extraCols'), value: this.arrayToMultiline(values)});
            }
        }

        if (reportType === 'B') {
            var btArray = [];
            Ext.each(record.get('bookingTypes'), function(inf) {
                Ext.getStore('BookingTypes').each(function(bt) {
                    if (bt.get('id') === inf) {
                        btArray.push(bt.get('displayInformation'));
                    }
                });
            });

            if (record.get('displayOnlySubtrees') === 1) {
                configOptionArr.push({key: BC.translate('displayOnlySubtrees'), value: this.arrayToMultiline(btArray)});
            }
            var infArray = [];

            if (record.get('showEmpBookings') === 1) {
                infArray.push(BC.translate('showEmpBookings'));
            }
            if (record.get('showCostAndRevenue') === 1) {
                infArray.push(BC.translate('showCostAndRevenue'));
            }
            if (record.get('showBTHours') === 1) {
                infArray.push(BC.translate('showBTHours'));
            }
            if (record.get('showTargetHours') === 1) {
                infArray.push(BC.translate('targetRemainingEffort'));
            }
            if (record.get('showWithLockedNodes') === BC.store.status.LockedNodes.WITHOUT_LOCKED_NODES) {
                infArray.push(BC.translate('withoutLockedNodes'));
            }
            if (infArray.length) {
                configOptionArr.push({key: BC.translate('configInformationToBeDisplayed'), value: this.arrayToMultiline(infArray)});
            }

        }

        // List only with begin/end or remote work days
        if (reportType === 'L') {
            var onlyWithBeginEnd = record.get('showBookingsWithBeginEnd');
            configOptionArr.push({key: BC.translate('onlyHourBookingsWithBeginEndShort'), value: onlyWithBeginEnd === 1 ? BC.translate('ja') : BC.translate('nein')});

            var onlyRemoteWorkDays = record.get('showRemoteWorkDays');
            configOptionArr.push({key: BC.translate('onlyRemoteWorkDays'), value: onlyRemoteWorkDays === 1 ? BC.translate('ja') : BC.translate('nein')});
        }

        // MonthEnd zero hours per week flag
        if (reportType === 'M') {
            var zeroHoursPerWeek = record.get('showZeroHoursPerWeek'),
                oneLinePerEmployee = record.get('showOneLinePerEmployee'),
                oneTabPerCompany = record.get('showOneTabPerCompany');
            configOptionArr.push({key: BC.translate('configIconZeroHoursPerWeek'), value: zeroHoursPerWeek === 1 ? BC.translate('ja') : BC.translate('nein')});
            configOptionArr.push({key: BC.translate('configOneLinePerEmployee'), value: oneLinePerEmployee === 1 ? BC.translate('ja') : BC.translate('nein')});
            configOptionArr.push({key: BC.translate('configOneTabPerCompany'), value: oneTabPerCompany === 1 ? BC.translate('ja') : BC.translate('nein')});
        }

        // Booking type
        if (/^[R]$/.test(reportType) === true) {
            values = [];
            Ext.Array.each(record.get('bookingTypes'), function(bookingType) {
                var name = btStore.getById(bookingType).get('longName');
                if (Ext.Array.contains(values, name) === false) {
                    values.push(name);
                }
            });

            if (values.length > 0) {
                configOptionArr.push({key: BC.translate('bookingType'), value: this.arrayToMultiline(values)});
            }
        }

        // Mis division
        if (reportType === 'I') {
            // division
            var divisionRecord = Ext.StoreMgr.lookup('Divisions').getById(record.get('divisionId'));
            if (divisionRecord !== null) {
                configOptionArr.push({key: BC.translate('division'), value: divisionRecord.get('displayName')});
            }

            // target hourly rate
            configOptionArr.push({key: BC.translate('targethourlyrate'), value: record.get('targetHourlyRate')});
        }

        // Status
        if (reportType === 'K') {
            // information to be displayed
            var informationArray = [];

            Ext.each(record.get('bookingTypes'), function(inf) {
                Ext.getStore('BookingTypes').each(function(bt) {
                    if (bt.get('id') === inf) {
                        informationArray.push(bt.get('displayInformation'));
                    }
                });
            });
            configOptionArr.push({key: BC.translate('configInformationToBeDisplayed'), value: this.arrayToMultiline(informationArray)});

            //display lines
            var displayLines;
            if (record.get('displayLines') === BC.store.status.DisplayLines.ONLY_IF_HOURS_AMOUNTS_IN_SELECTED_GENERAL_PERIOD) {
                displayLines = BC.translate('onlyIfHoursAmountsInSelectedGeneralPeriod_short');
            } else if (record.get('displayLines') === BC.store.status.DisplayLines.ONLY_IF_HOURS_AMOUNTS_IN_SELECTED_PERIOD) {
                displayLines = BC.translate('onlyIfHoursAmountsInSelectedPeriod_short');
            } else {
                displayLines = BC.translate('onlyIfHoursInSelectedPeriod_short');
            }
            configOptionArr.push({key: BC.translate('configDisplayLines'), value: '<br />' + displayLines});

            // display figures
            if (record.get('displayFigures') === BC.store.status.DisplayFigures.PER_MONTH) {
                configOptionArr.push({key: BC.translate('configDisplayFigures'), value: '<br />' + BC.translate('displayFiguresPerMonth')});
            } else {
                configOptionArr.push({key: BC.translate('configDisplayFigures'), value: '<br />' + BC.translate('displayFiguresOncePerPeriod')});
            }

            // with locked nodes
            if (record.get('showWithLockedNodes') === BC.store.status.LockedNodes.WITH_LOCKED_NODES) {
                configOptionArr.push({key: BC.translate('configLockedNodes'), value: '<br />' + BC.translate('withLockedNodes')});
            } else {
                configOptionArr.push({key: BC.translate('configLockedNodes'), value: '<br />' + BC.translate('withoutLockedNodes')});
            }
        }

        // show with closed positions of orders which relates to the configured period
        if (reportType === 'V') {
            let closedOrders = record.get('showClosedOrders'),
                ordersAfterPeriod = record.get('showOrdersAfterPeriod'),
                withoutOrdersAfterPeriod = record.get('showWithoutOrdersAfterPeriod'),
                withHigherLevelPosition = record.get('showWithHigherLevelPosition'),
                materialFilterIds = record.get('materialFilter');

            configOptionArr.push({key: BC.translate('closedOrdersShort'), value: closedOrders === 1 ? BC.translate('ja') : BC.translate('nein')});
            configOptionArr.push({key: BC.translate('ordersAfterPeriodShort'), value: ordersAfterPeriod === 1 ? BC.translate('ja') : BC.translate('nein')});
            configOptionArr.push({key: BC.translate('withoutOrdersAfterPeriodShort'), value: withoutOrdersAfterPeriod === 1 ? BC.translate('ja') : BC.translate('nein')});
            configOptionArr.push({key: BC.translate('withHigherLevelPositionShort'), value: withHigherLevelPosition === 1 ? BC.translate('ja') : BC.translate('nein')});

            if (materialFilterIds.length > 0) {
                let materialFilterStore = Ext.getStore('MaterialFilter'),
                    materialFilterNames = [];
                Ext.Array.each(materialFilterIds, function(id) {
                    let rec = materialFilterStore.getById(id);
                    materialFilterNames.push(rec.get('name'));
                });
                configOptionArr.push({
                    key: BC.translate('filteredBy'),
                    value: this.arrayToMultiline(materialFilterNames)
                });
            }
        }

        if (reportType === 'Q') {
            configOptionArr.push({key: BC.translate('externalEmployees'), value: record.get('showExternalEmployees') === 1 ? BC.translate('ja') : BC.translate('nein')});
        }

        if (configOptionArr.length > 0) {
            toolTip.update(this.tpl.applyTemplate(configOptionArr));
        } else {
            toolTip.update(BC.translate('noDetails'));
        }

        toolTip.showAt(position);
    },

    showNodes: function(record, position) {
        var toolTip = this.getToolTipComponent(),
            nodeStore = Ext.data.StoreManager.lookup('Nodes'),
            nodes = this.getNodes(record, nodeStore, true),
            sets = this.getNodes(record, nodeStore, false),
            output = '';

        if (nodes.length > 0) {
            output = nodes.join(',');
        }

        if (sets.length > 0) {
            if (output !== '') {
                output += ',';
            }
            output += sets.join(',');
        }

        if (output !== '') {
            toolTip.update(this.singleTpl.applyTemplate({value: output}));
        } else {
            toolTip.update(BC.translate('noDetails'));
        }
        toolTip.showAt(position);
    },

    getToolTipComponent: function() {
        var toolTip = Ext.getCmp(this.id);

        if (toolTip instanceof Ext.tip.ToolTip === false) {
            toolTip = Ext.create('Ext.tip.ToolTip', {
                id: this.id,
                autoHide: false,
                trackMouse: true,
                width: 250,
                dismissDelay: 20000
            });
        }
        return toolTip;
    },

    arrayToMultiline: function(arr) {
        return '<br />' + arr.join('<br />');
    },

    getCheckedNodes: function(record) {
        var items = record.get('nodes'),
            values = [];
        for (var i = 0; i < items.length; ++i) {
            if (items[i].checked) {
                values.push(items[i].name);
            }
        }
        return values;
    },

    getSingleNodes: function(record, singleNodeType) {
        var items = record.get('nodes'),
            values = [];
        for (var i = 0; i < items.length; ++i) {
            if ((items[i].singleNodeTypes & singleNodeType) > 0 && items[i].name) {
                values.push(items[i].name);
            }
        }
        return values;
    },

    getDistinctRowNodes: function(record) {
        var items = record.get('nodes'),
            values = [];
        for (var i = 0; i < items.length; ++i) {
            if ((items[i].singleNodeTypes & BC.store.SingleNodeTypes.STATUS_DISTINCTROW) > 0 && items[i].name) {
                var name = items[i].name;
                if ((items[i].singleNodeTypes & BC.store.SingleNodeTypes.STATUS_WITHNEXTLEVEL) > 0) {
                    name += ' ' + BC.translate('withNextLevelInfo');
                }
                values.push(name);
            }
        }
        return values;
    },

    getNodes: function(record, store, nodes) {
        var items = record.get('nodes'),
            values = [];

        for (var i = 0; i < items.length; ++i) {
            if (!nodes === items[i].isSet) {
                values.push(items[i].name);
            }
        }
        values.sort();

        return values;
    },

    getEmployees: function(record, store, employees) {
        var items = record.get('employees'),
            values = [];

        for (var i = 0; i < items.length; ++i) {
            if (employees && items[i].shortcut) {
                values.push(Ext.String.format(
                    '{2}, {1} ({0})',
                    items[i].shortcut,
                    items[i].first_name,
                    items[i].last_name
                ));
            } else if (!employees && items[i].name) {
                values.push(items[i].name);
            }
        }
        values.sort();

        return values;
    }
});
