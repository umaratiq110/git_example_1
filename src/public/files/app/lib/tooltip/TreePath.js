Ext.define('BC.lib.tooltip.TreePath', {
    extend: 'Ext.tip.ToolTip',

    autoHide: false,

    nodeId: null,
    isLoading: false,
    isLoaded: false,

    initComponent: function() {
        this.tree = Ext.create('Ext.tree.Panel', {
            rootVisible: false,
            bodyBorder: false,
            border: false,
            bodyStyle: 'background-color: #E9F2FF;',
            scroll: false,
            root: {
                text: "ToolTip Tree Root",
                expanded: true,
                children: []
            }
        });

        this.callParent(arguments);
    },

    show: function() {
        if (this.isLoaded) {
            this.callParent(arguments);
        } else if (!this.isLoading) {
            this.loadPath(Ext.Function.bind(this.show, this, arguments));
        }
    },

    showAt: function() {
        if (this.isLoaded) {
            this.callParent(arguments);
        } else if (!this.isLoading) {
            this.loadPath(Ext.Function.bind(this.showAt, this, arguments));
        }
    },

    loadPath: function(callback) {
        if (this.isLoading) {
            return false;
        }

        this.isLoaded = false;
        this.isLoading = true;

        Ext.Ajax.request({
            url: "tree/getpath",
            params: {
                nodeId: this.nodeId
            },
            scope: this,
            success: function(response) {
                if (response && response.responseText) {
                    var result = Ext.JSON.decode(response.responseText),
                        node = this.tree.getRootNode(),
                        i;

                    // remove all children (if any)
                    node.removeAll(true);

                    // append children
                    for (i = 0; i < result.length; i++) {
                        node = node.appendChild(Ext.apply(result[i], {expanded: true}));
                    }

                    this.add(this.tree);

                    var maxLineWidth = 0;
                    for (i = 0; i < result.length; ++i) {
                        var textMetrics = new Ext.util.TextMetrics(this.tree.getView().getNode(this.tree.getRootNode())),
                            textWidth = textMetrics.getWidth(result[i].text),
                            lineWidth = textWidth + i * 16 + 32 + 5;

                        if (maxLineWidth < lineWidth) {
                            maxLineWidth = lineWidth;
                        }

                        textMetrics.destroy();
                    }

                    this.width = maxLineWidth + 10;
                    this.height = result.length * 20 + 10;
                    this.setWidth(this.width);
                    this.setHeight(this.height);

                    this.isLoading = false;
                    this.isLoaded = true;

                    if (Ext.isFunction(callback)) {
                        callback();
                    }
                }
            },

            failure: function() {
                Ext.MessageBox.alert(BC.translate('undefinederror_subject'), BC.translate('allgemeinerfehler'));
            }
        });
    }
});
