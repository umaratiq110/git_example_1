/**
 * Library overrides
 */
Ext.define('BC.lib.Override', {
    requires: [
        'Ext.picker.Date',
        'Ext.form.field.Date',
        'Ext.data.NodeInterface',
        'Ext.grid.column.Column',
        'Ext.tree.Column'
    ]
}, function() {

    /**
     * Ext.picker.Date Override
     * Sets first day in date pickers to Monday
     */
    Ext.override(Ext.picker.Date, {
        startDay: 1
    });

    /**
     * Ext.state.Stateful Override
     * Sets first day in date pickers to Monday
     */
    Ext.override(Ext.form.field.Date, {
        startDay: 1
    });

    /**
     * Ext.data.NodeInterface Override
     *
     * Provides proper getPath method
     */
    var prototypeBodyObj = Ext.data.NodeInterface.getPrototypeBody();
    Ext.data.NodeInterface.addStatics({
        getPrototypeBody: function() {
            /**
             * Gets the hierarchical path from the root of the current node.
             * @param {String} [field] The field to construct the path from. Defaults to the model idProperty.
             * @param {String} [separator="/"] A separator to use.
             * @return {String} The node path
             */
            prototypeBodyObj.getPath = function(field, separator) {
                field = field || this.idProperty;
                separator = separator || '/';

                var path = [this.get(field)],
                    parent = this.parentNode;

                while (parent) {
                    path.unshift(parent.get(field));
                    parent = parent.parentNode;
                }
                return separator + path.join(separator);
            };

            return prototypeBodyObj;
        }
    });

    /**
     * Ext.grid.column.Column Overrides
     *
     * Encodes HTML tags in a default rendered grid columns (as a text).
     * If renderer is provided by user then the user is repsonsible for applying encoding.
     *
     * @author Robert Lesniak <les@isogmbh.de>
     */
    Ext.override(Ext.grid.column.Column, {
        constructor: function() {
            this.callOverridden(arguments);
            if (this.xtype === 'gridcolumn' && this.renderer === false) {
                var rendererFn = function(value) {
                    return Ext.util.Format.htmlEncode(value);
                };

                this.renderer = rendererFn;
            }
        }
    });

    /**
     * Ext.tree.Column Overrides
     *
     * Encodes HTML tags in a tree column in a 'text' field of a node.
     *
     * @author Robert Lesniak <les@isogmbh.de>
     */
    Ext.override(Ext.tree.Column, {
        defaultRenderer: function(value) {
            return Ext.util.Format.htmlEncode(value);
        }
    });
});
