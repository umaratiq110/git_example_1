var global = {
    "user": "",
    "prevUser": {
        "name": "",
        "checksum": ""
    },
    "privileges": {
        "adminRights": true,
        "costAndRevenue": true,
        "monthEnd": true,
        "accounting": true,
        "rebook": true,
        "rebookAllBookings": true,
        "rebookLog": true,
        "permission": true,
        "sapKeyHours": true,
        "standby": true,
        "absenceCalendar": true,
        "misDivisionOverall": true,
        "projectStatus": true,
        "accountableHours": true,
        "orderOverview": true
    },
    "tree": {
        "nodes": [],
        "employees": []
    },
    "costcentres": {
        "all": [],
        "user": []
    },
    "nodeTypes": [],
    "structureUnitTypes": [],
    "bookingTypes": [],
    "singleNodeTypes": [],
    "divisions": [],
    "additionalCols": [],
    "bcUsers": [],
    "employees": [],
    "bcPrivilegeMail": "",
    "bookingTreeMail": "",
    "jiraUrl": "",
    "displayFigures": [],
    "lockedNodes": [],
    "displayLines": [],
    "accountingPath": "",
    "ableToShareEmployees": []
};
