<?php
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
$environmentFile = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'ENVIRONMENT';
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', file_exists($environmentFile) ? file_get_contents($environmentFile) : "development");

require_once APPLICATION_PATH . '/../../vendor/autoload.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/config/config.ini'
);

$application->bootstrap()
            ->run();
