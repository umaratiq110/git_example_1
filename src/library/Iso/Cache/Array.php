<?php
/**
 * Class Iso_Cache_Array
 *
 * @category Iso
 * @package Iso_Cache
 */
class Iso_Cache_Array extends Zend_Cache_Core
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @param string $id
     * @param bool|false $doNotTestCacheValidity
     * @param bool|false $doNotUnserialize
     * @return bool
     */
    public function load($id, $doNotTestCacheValidity = false, $doNotUnserialize = false)
    {
        if (isset($this->data[$id])) {
            return $this->data[$id];
        }
        return false;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function test($id)
    {
        return isset($this->data[$id]);
    }

    /**
     * @param string $data
     * @param string $id
     * @return bool
     */
    public function save($data, $id = null, $tags = [], $specificLifetime = false, $priority = 8)
    {
        $this->data[$id] = $data;
        return true;
    }

    /**
     * @param string $id
     * @return void
     */
    public function remove($id)
    {
        if (isset($this->data[$id])) {
            unset($this->data[$id]);
        }
    }
}
