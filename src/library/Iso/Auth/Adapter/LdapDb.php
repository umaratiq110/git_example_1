<?php

/**
 * @category   Iso
 * @package    Iso_Auth
 * @subpackage Iso_Auth_Adapter
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Iso_Auth_Adapter_LdapDb extends Zend_Auth_Adapter_Ldap
{
    /**
     * Database Connection
     *
     * @var Zend_Db_Adapter_Abstract
     */
    protected $zendDb = null;

    /**
     * $authenticateResultInfo
     *
     * @var array
     */
    protected $authenticateResultInfo = null;

    /**
     * $identityColumn - the column to use as the identity
     *
     * @var string
     */
    protected $identityColumn = null;

    /**
     * $tableName - the table name to check
     *
     * @var string
     */
    protected $tableName = null;

    /**
     * resultRow - result of the row
     *
     * @var string
     */
    protected $resultRow = null;


    /**
     * setDb() - set the database
     *
     * @param  string $db
     * @return Zend_Auth_Adapter_DbTable Provides a fluent interface
     */
    public function setDb($db)
    {
        $this->zendDb = $db;
        return $this;
    }

    /**
     * setTableName() - set the table name to be used in the select query
     *
     * @param  string $tableName
     * @return Zend_Auth_Adapter_DbTable Provides a fluent interface
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * setIdentityColumn() - set the column name to be used as the identity column
     *
     * @param  string $identityColumn
     * @return Zend_Auth_Adapter_DbTable Provides a fluent interface
     */
    public function setIdentityColumn($identityColumn)
    {
        $this->identityColumn = $identityColumn;
        return $this;
    }

    /**
     * Authenticate the user
     *
     * @throws Zend_Auth_Adapter_Exception
     * @return Zend_Auth_Result
     */
    public function authenticate()
    {
        $resultLdap = parent::authenticate();

        if ($resultLdap->isValid()) {
            $this->authenticateSetup();
            $dbSelect = $this->authenticateCreateSelect();
            $resultIdentities = $this->authenticateQuerySelect($dbSelect);

            if (($authResult = $this->authenticateValidateResultSet($resultIdentities)) instanceof Zend_Auth_Result) {
                return $authResult;
            }

            $authResult = $this->authenticateValidateResult(array_shift($resultIdentities));
            return $authResult;
        } else {
            return $resultLdap;
        }
    }

    /**
     * _authenticateSetup() - This method abstracts the steps involved with making sure
     * that this adapter was indeed setup properly with all required peices of information.
     *
     * @throws Zend_Auth_Adapter_Exception - in the event that setup was not done properly
     * @return true
     */
    protected function authenticateSetup()
    {
        $exception = null;

        if ($this->tableName == '') {
            $exception = 'A table must be supplied for the Zend_Auth_Adapter_LdapDb authentication adapter.';
        } elseif ($this->identityColumn == '') {
            $exception = 'An identity column must be supplied for the Zend_Auth_Adapter_LdapDb authentication adapter.';
            // Ignored because used in Zend class
            // @codingStandardsIgnoreStart
        } elseif ($this->_username == '') {
            // @codingStandardsIgnoreEnd
            $exception =
                'A value for the identity was not provided prior to authentication with Zend_Auth_Adapter_LdapDb.';
        }

        if (null !== $exception) {
            throw new Zend_Auth_Adapter_Exception($exception);
        }

        $this->authenticateResultInfo = array(
            'code'     => Zend_Auth_Result::FAILURE,
            // Ignored because used in Zend class
            // @codingStandardsIgnoreStart
            'identity' => $this->_username,
            // @codingStandardsIgnoreEnd
            'messages' => array()
        );

        return true;
    }

    /**
     * _authenticateCreateSelect() - This method creates a Zend_Db_Select object that
     * is completely configured to be queried against the database.
     *
     * @return Zend_Db_Select
     */
    protected function authenticateCreateSelect()
    {
        // get select
        // Ignored because used in Zend class
        // @codingStandardsIgnoreStart
        $dbSelect = $this->zendDb->select();
        $dbSelect->from($this->tableName)
            ->where($this->zendDb->quoteIdentifier($this->identityColumn, true) . ' = ?', $this->_username)
            ->where('ID_FIRMA = ?', '1');
        // @codingStandardsIgnoreEnd
        return $dbSelect;
    }

    /**
     * _authenticateQuerySelect() - This method accepts a Zend_Db_Select object and
     * performs a query against the database with that object.
     *
     * @param Zend_Db_Select $dbSelect
     * @throws Zend_Auth_Adapter_Exception - when a invalid select object is encoutered
     * @return array
     */
    protected function authenticateQuerySelect(Zend_Db_Select $dbSelect)
    {
        try {
            if ($this->zendDb->getFetchMode() != Zend_DB::FETCH_ASSOC) {
                $origDbFetchMode = $this->zendDb->getFetchMode();
                $this->zendDb->setFetchMode(Zend_DB::FETCH_ASSOC);
            }
            $resultIdentities = $this->zendDb->fetchAll($dbSelect->__toString());
            if (isset($origDbFetchMode)) {
                $this->zendDb->setFetchMode($origDbFetchMode);
                unset($origDbFetchMode);
            }
        } catch (Exception $e) {
            throw new Zend_Auth_Adapter_Exception('The supplied parameters to Zend_Auth_Adapter_LdapDb failed to '
                . 'produce a valid sql statement, please check table and column names '
                . 'for validity.');
        }
        return $resultIdentities;
    }

    /**
     * _authenticateValidateResultSet() - This method attempts to make certian that only one
     * record was returned in the result set
     *
     * @param array $resultIdentities
     * @return true|Zend_Auth_Result
     */
    protected function authenticateValidateResultSet(array $resultIdentities)
    {
        if (count($resultIdentities) < 1) {
            $this->authenticateResultInfo['code'] = Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND;
            $this->authenticateResultInfo['messages'][] = 'A record with the supplied identity could not be found.';
            return $this->authenticateCreateAuthResult();
        } elseif (count($resultIdentities) > 1) {
            $this->authenticateResultInfo['code'] = Zend_Auth_Result::FAILURE_IDENTITY_AMBIGUOUS;
            $this->authenticateResultInfo['messages'][] = 'More than one record matches the supplied identity.';
            return $this->authenticateCreateAuthResult();
        }

        return true;
    }

    /**
     * _authenticateValidateResult() - This method attempts to validate that the record in the
     * result set is indeed a record that matched the identity provided to this adapter.
     *
     * @param array $resultIdentity
     * @return Zend_Auth_Result
     */
    protected function authenticateValidateResult($resultIdentity)
    {
        $this->resultRow = $resultIdentity;

        $this->authenticateResultInfo['code'] = Zend_Auth_Result::SUCCESS;
        $this->authenticateResultInfo['messages'][] = 'Authentication successful.';
        return $this->authenticateCreateAuthResult();
    }

    /**
     * _authenticateCreateAuthResult() - This method creates a Zend_Auth_Result object
     * from the information that has been collected during the authenticate() attempt.
     *
     * @return Zend_Auth_Result
     */
    protected function authenticateCreateAuthResult()
    {
        return new Zend_Auth_Result(
            $this->authenticateResultInfo['code'],
            $this->authenticateResultInfo['identity'],
            $this->authenticateResultInfo['messages']
        );
    }
}
