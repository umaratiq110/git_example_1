<?php

/**
 * @category   Iso
 * @package    Iso_Auth
 * @subpackage Iso_Auth_Adapter
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Iso_Auth_Adapter_Normal implements Zend_Auth_Adapter_Interface
{
    /**
     * The username of the account being authenticated.
     *
     * @var string
     */
    protected $username = null;
    
    
    /**
     * Constructor
     *
     * @param  string $username The username of the account being authenticated
     * @return void
     */
    public function __construct($username = null)
    {
        if ($username !== null) {
            $this->username = (string) $username;
        }
    }
   
    
    /**
     * Authenticate the user
     *
     * @throws Zend_Auth_Adapter_Exception
     * @return Zend_Auth_Result
     */
    public function authenticate()
    {
        if ($this->username == null || $this->username == '') {
            $exception =
                'A value for the identity was not provided prior to authentication with Iso_Auth_Adapter_Normal.';

            throw new Zend_Auth_Adapter_Exception($exception);
        }
        
        return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $this->username, array());
    }
}
