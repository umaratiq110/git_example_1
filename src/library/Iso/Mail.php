<?php
/**
 * @category Iso
 * @package Iso_Mail
 * @author Matthias Zeug <zeu@isogmbh.de>
 */
class Iso_Mail extends Zend_Mail
{
    protected static $savePath;
    protected static $saveTransport;

    protected static $defaultRedirectRecipient;

    /**
     * Sets the save path for email files
     *
     * @param string $path Directory where emails should be saved
     * @throws Zend_Mail_Exception
     */
    public static function setSavePath($path)
    {
        if (true === is_dir($path)) {
            self::$saveTransport = new Zend_Mail_Transport_File(array(
                'path'        => $path,
                'callback'    => array('Iso_Mail', 'generateFilename')
            ));
        } else {
            throw new Zend_Mail_Exception('Mail save path must be a directory');
        }
    }

    /**
     * Sets the default recipient of redirected mails which is used in case
     * no recipient can be detected from the session
     *
     * @param string $mail
     */
    public static function setDefaultRedirectRecipient($mail)
    {
        self::$defaultRedirectRecipient = $mail;
    }
    
    /**
     * Generates a filename for an email to be stored
     *
     * @param Zend_Mail_Transport_File
     * @return string The file name
     */
    public static function generateFilename($transport)
    {
        return date('YmdHis') . '_' . $transport->recipients . '.txt';
    }

    /**
     * Overwrites the send method to
     * a) store all emails as files before sending them
     * b) overwrite recipients and subject in case we are not in production
     *
     * @param mixed $transport
     * @return Zend_Mail
     * @throws Zend_Exception
     * @throws Zend_Mail_Exception
     */
    public function send($transport = null)
    {
        // if saveTransport is set, save email first
        if (self::$saveTransport instanceof Zend_Mail_Transport_File) {
            parent::send(self::$saveTransport);
        }

        if (APPLICATION_ENV == 'prod') {
            return parent::send($transport);
        }

        $email = self::$defaultRedirectRecipient;
        try {
            $session = new Zend_Session_Namespace('bkzcockpit');
            if (isset($session)) {
                // respect switched users
                while (isset($session->prevUser)) {
                    $session = $session->prevUser;
                }

                if (isset($session->user)) {
                    $email = $session->user->getUserMail();
                }
            }
        } catch (Exception $e) {
        }
        // save & overwrite recipients
        $headers = $this->_headers;
        $recipients = $this->getRecipients();
        $this->clearRecipients();
        $this->addTo($email);
        
        // save & overwrite subject
        $subject = $this->getSubject();
        $this->clearSubject();
        $this->setSubject($subject . ' (' . APPLICATION_ENV . ')' . ' [' . implode(';', $recipients) . ']');
        
        parent::send($transport);
        
        // restore original recipients
        foreach ($headers as $t => $arr) {
            if ($t == 'To' || $t == 'Cc' || $t == 'Bcc') {
                $fn = 'add' . $t;
                foreach ($arr as $key => $val) {
                    if (is_string($val)) {
                        $this->$fn($val);
                    }
                }
            }
        }

        // restore original subject
        $this->clearSubject();
        $this->setSubject($subject);
    }
}
