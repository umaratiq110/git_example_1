<?php
class Iso_Login extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart

    /**
    * Table name
    * @var string
    */
    protected $_name = 'LOGIN';

    /**
    * Primary key
    * @var string
    */
    protected $_primary = 'ID';

    /**
    * Sequence
    * @var string
    */
    protected $_sequence = 'SEQ_LOGIN';

    // @codingStandardsIgnoreEnd
}
