<?php
use JpGraph\JpGraph;

abstract class Iso_Diagramm
{
    /**
     * Ein Array, dass je nach Diagrammtyp die Limit-Werte kumuliert oder einzeln enthält, z. B.:
     * - einzeln: [200, 200, 200, 200]
     * - kumuliert: [200, 400, 600, 800];
     *
     * @var int[]
     */
    protected $limit;
    
    /**
     * Ein Array, das die Werte der einzelnen BKZs enthält, z. B.:
     * - zwei BKZs a vier Werte: [ [100, 300, 400, 700], [50, 270, 450, 700] ]
     *
     * @var array
     */
    protected $data;
    
    /**
     * Ein Array, das die Bezeichnungen der einzelnen BKZs enthält, z. B.:
     * - zwei BKZs: ['BKZ1', 'BKZ2']
     *
     * @var string[]
     */
    protected $ccs;
    
    /**
     * Ein Array, das die Summierten Aufwände der einzelnen BKZs über den kompletten Zeitraum enthält, z. B.:
     * - zwei BKZs: [10, 3]
     *
     * @var float[]
     */
    protected $sum;
    
    /**
     * Ein Array, das die Beschriftung der X-Achsel enthält, z. B.:
     * - vier Monate: ['2008/03', '2008/04', '2008/05', '2008/06']
     * - drei Tage: ['1', '2', '3']
     *
     * @var string[]
     */
    protected $labelx;

    /**
     * @var string
     */
    protected $bgimage = 'files/images/ISO_Gruppe_logo.jpg';

    /**
     * @var int
     */
    protected $ttfont;

    /**
     * @var int
     */
    protected $sizex = 1280;

    /**
     * @var int
     */
    protected $sizey = 1024;

    /**
     * @var string
     */
    protected $legendPosition = 'left';
    
    /**
     * @var string[]
     */
    protected $colors = [
        'chocolate4',
        'azure4',
        'chartreuse4',
        'cornflowerblue',
        'darkgoldenrod2',
        'darkred',
        'dodgerblue4',
        'gold',
        'hotpink4',
        'khaki',
        'lightblue4',
        'olivedrab'
    ];

    /**
     * @var Zend_Translate_Adapter
     */
    protected $translate;

    /**
     * @param Zend_Translate_Adapter|Zend_View_Helper_Translate $translate
     */
    public function __construct($translate)
    {
        if ($translate instanceof Zend_View_Helper_Translate) {
            $translate = $translate->getTranslator();
        }
        $this->translate = $translate;

        $this->initJpGraph();
    }

    /**
     * Invoked upon deserialization
     */
    public function __wakeup()
    {
        $this->initJpGraph();
    }

    public function setData($data, $ccs, $summen)
    {
        $this->data = $data;
        $this->ccs  = $ccs;
        $this->sum  = $summen;
    }

    /**
     * @param int $sizex
     * @return Iso_Diagramm $this
     */
    public function setSizeX($sizex)
    {
        $this->sizex = $sizex;
        return $this;
    }

    /**
     * @param int $sizey
     * @return Iso_Diagramm $this
     */
    public function setSizeY($sizey)
    {
        $this->sizey = $sizey;
        return $this;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function setXLabels($labels)
    {
        $this->labelx = $labels;
    }

    public function setLegendPosition($position)
    {
        if (!in_array($position, ['left', 'right'])) {
            throw new Iso_Exception("Legend position must be either 'left' or 'right'");
        }
        $this->legendPosition = $position;
    }

    /**
     * @return mixed
     */
    abstract public function generate();

    /**
     * Initialize JpGraph library
     */
    protected function initJpGraph()
    {
        JpGraph::load();

        $this->ttfont = FF_DV_SANSSERIF;
    }

    /**
     * @return Graph
     */
    protected function initGraph()
    {
        $graph = new Graph($this->sizex, $this->sizey, "auto");

        $graph->SetScale("textlin");
        $graph->SetMarginColor('white');
        $graph->SetMargin(50, 5, 25, 50);
        $graph->SetFrame(false);
        $graph->SetShadow();

        $this->initGrid($graph);
        $this->initAxis($graph);
        $this->initBackgroundImage($graph);
        $this->initLegend($graph);

        return $graph;
    }

    /**
     * @param Graph $graph
     */
    protected function initGrid(Graph $graph)
    {
        $graph->xgrid->SetColor('#FFDD99');
        $graph->ygrid->SetColor('#FFDD99');
        $graph->ygrid->SetFill(true, '#EFEFEF@0.7', '#FFCC66@0.7');
        $graph->xgrid->Show();
    }

    /**
     * @param Graph $graph
     */
    protected function initAxis(Graph $graph)
    {
        $graph->xaxis->SetFont($this->ttfont, FS_NORMAL, 12);
        $graph->xaxis->SetLabelMargin(5);

        $graph->yaxis->SetFont($this->ttfont, FS_NORMAL, 12);
        $graph->yaxis->HideZeroLabel();

        $graph->xaxis->SetTickLabels($this->labelx);

        // adjust label presentation on x-axis depending on the number of labels
        $labelCount = sizeof($this->labelx);
        if ($labelCount <= 15) {
            $graph->xaxis->SetLabelAngle(0);
        } else {
            $graph->xaxis->SetLabelAngle(45);
            if ($labelCount > 31) {
                $graph->xaxis->SetFont($this->ttfont, FS_NORMAL, 10);
            }
        }
    }

    /**
     * @param Graph $graph
     */
    protected function initBackgroundImage(Graph $graph)
    {
        $graph->SetBackgroundImage($this->bgimage, BGIMG_CENTER);
        $graph->SetBackgroundImageMix(40);
    }

    /**
     * @param Graph $graph
     */
    protected function initLegend(Graph $graph)
    {
        $graph->legend->SetLayout(LEGEND_VERT);
        $graph->legend->SetFont($this->ttfont, FS_NORMAL, 12);
        $graph->legend->SetShadow('gray@0.5', 0);

        if ($this->legendPosition == 'left') {
            $graph->legend->SetPos(0.05, 0.01, $this->legendPosition, 'top');
        } else {
            $graph->legend->SetPos(0.01, 0.01, $this->legendPosition, 'top');
        }
    }
}
