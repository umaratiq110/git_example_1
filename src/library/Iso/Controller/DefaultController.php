<?php
class Iso_Controller_DefaultController extends Zend_Controller_Action
{
    protected $languageFile = null;
    protected $languageType;

    /**
     * @var Zend_Db_Adapter_Abstract
     */
    protected $db;

    /**
     * @var Zend_Config
     */
    protected $config;

    /**
     * @var Zend_Log
     */
    protected $log;

    /**
     * @var Zend_Session_Namespace
     */
    protected $session;

    /**
     * @var Zend_Auth_Result
     */
    protected $auth;

    /**
     * @var Iso_User
     */
    protected $user;

    /**
     * @var Zend_Translate|Zend_View_Helper_Translate
     */
    protected $translate;

    /**
     * @var Iso_Log_StopWatch
     *
     */
    private $pLogger;

    public function init()
    {
        $this->pLogger = Zend_Registry::get('performanceLog');
        $this->pLogger->logStage('controller init start');
        $bootstrap = $this->getInvokeArg('bootstrap');

        $this->db = $bootstrap->getResource('db');
        $this->config = $bootstrap->getResource('config');
        $this->log = $bootstrap->getResource('log');
        $this->session = $bootstrap->getResource('sessionNamespace');
        
        // load translations
        $this->setLanguage();
        
        if ($this->languageFile != null) {
            $adapter = new Iso_Translate_DbTranslate('database', $this->languageFile, $this->languageType);
            Zend_Registry::set('Zend_Translate', $adapter);
            
            $this->translate = new Zend_View_Helper_Translate($adapter);
        }

        if (PHP_SAPI === 'cli') {
            return;
        }
        
        // set user
        if (isset($this->session->user)) {
            $this->user = $this->session->user;
        } elseif ($this->session->auth != null) {
            $this->user = new Iso_User($this->config->company->ownId, $this->session->auth->getIdentity());
        }

        // check authentication
        $this->auth = $this->session->auth;

        // Set headers in order to prevent caching
        $response = $this->getResponse();
        $response->setHeader('Cache-Control', 'private, no-store, no-cache, must-revalidate', true);
        $response->setHeader('Expires', '-1', true);

        $request = $this->getRequest();

        if (!in_array($request->getControllerName(), ['login', 'error']) &&
            ($this->auth == null || !$this->auth->isValid())
        ) {
            if (!$request->isXmlHttpRequest()) {
                $this->redirect('/login', array('prependBase' => true));
            } else {
                header("HTTP/1.0 403 Forbidden");
                exit();
            }
        }
        $this->pLogger->logStage('controller init end');
    }

    public function preDispatch()
    {
        $this->pLogger->logStage('preDispatch start');
        parent::preDispatch();
        $this->pLogger->logStage('preDispatch end');
    }

    public function dispatch($action)
    {
        $this->pLogger->logStage('dispatch start');
        parent::dispatch($action);
        $this->pLogger->logStage('dispatch end');
    }

    public function postDispatch()
    {
        $this->pLogger->logStage('postDispatch start');
        parent::postDispatch();
        $this->pLogger->logStage('postDispatch end');
    }

    /**
     * Adds helpers for view.
     */
    public function initView()
    {
        $view = parent::initView();
        $view->addHelperPath(APPLICATION_PATH . '/../library/Iso/View/Helper/', 'Iso_View_Helper_');
        return $view;
    }

    /**
     * Set user language
     */
    protected function setLanguage($language = '')
    {
        if (empty($language)) {
            if ($this->session->user != null) {
                $this->languageType = $this->checkLanguage($this->session->user->getSprache());
            } else {
                try {
                    $locale = new Zend_Locale(Zend_Locale::BROWSER);
                    $lang = $locale->getLanguage();
                } catch (Zend_Locale_Exception $zle) {
                    $lang = 'en-GB';
                }
                
                $this->languageType = $this->checkLanguage($lang);
            }
        } else {
            $this->languageType = $this->checkLanguage($language);
        }
    }
    
    /**
     * Check language and return correct 2-letter shortcut
     */
    public function checkLanguage($language)
    {
        switch ($language) {
            case 'german':
            case 'de-DE':
            case 'de_DE':
            case 'de':
                return 'de';
                break;
            case 'english':
            case 'en-GB':
            case 'en_GB':
            case 'en':
                return 'en';
                break;
            default:
                return $this->config->language->default;
                break;
        }
    }
    
    public function getUser()
    {
        return $this->user;
    }
}
