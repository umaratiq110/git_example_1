<?php
/**
 * Class Iso_Controller_Router_Cli
 *
 * @category   Iso
 * @package    Iso_Controller
 * @subpackage Iso_Controller_Router
 */
class Iso_Controller_Router_Cli extends Zend_Controller_Router_Abstract
{
    /**
     * @param Zend_Controller_Request_Abstract $dispatcher
     * @return Zend_Controller_Request_Abstract|boolean
     */
    public function route(Zend_Controller_Request_Abstract $dispatcher)
    {
        $frontController = $this->getFrontController();

        $module = $dispatcher->getParam($dispatcher->getModuleKey(), $frontController->getDefaultModule());
        $controller = $dispatcher->getParam($dispatcher->getControllerKey(), $frontController
            ->getDefaultControllerName());
        $action = $dispatcher->getParam($dispatcher->getActionKey(), $frontController->getDefaultAction());

        $dispatcher->setModuleName($module);
        $dispatcher->setControllerName($controller);
        $dispatcher->setActionName($action);

        return $dispatcher;
    }

    /**
     * @param array $userParams
     * @param null $name
     * @param bool|false $reset
     * @param bool|true $encode
     * @return string
     */
    public function assemble($userParams, $name = null, $reset = false, $encode = true)
    {
        return '';
    }
}
