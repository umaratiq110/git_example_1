<?php
/**
 * Class Iso_Controller_Request_Cli
 *
 * @category   Iso
 * @package    Iso_Controller
 * @subpackage Iso_Controller_Request
 */
class Iso_Controller_Request_Cli extends Zend_Controller_Request_Abstract
{
    public function __construct()
    {
        $opts = new Zend_Console_Getopt(
            array(
                'module|m-s'        => 'Module name',
                'controller|c-s'    => 'Controller name',
                'action|a-s'        => 'Action name'
            )
        );
        $opts->parse();

        foreach ($opts->getOptions() as $option) {
            $this->setParam($option, $opts->getOption($option));
        }

        foreach ($opts->getRemainingArgs() as $arg) {
            $arg = explode(':', $arg, 2);

            if (sizeof($arg) === 1) {
                $arg[1] = true;
            }
            $this->setParam($arg[0], $arg[1]);
        }
    }
}
