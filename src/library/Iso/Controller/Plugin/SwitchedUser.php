<?php

/**
 * Handle exceptions that bubble up based on missing controllers, actions, or
 * application errors, and forward to an error handler.
 *
 * @category   Iso
 * @package    Iso_Controller
 * @subpackage Iso_Controller_Plugin
 * @author Matthias Zeug <zeu@isogmbh.de>
 * @author Krzysztof Atlasik<atk@isogmbh.de>
 */
class Iso_Controller_Plugin_SwitchedUser extends Zend_Controller_Plugin_Abstract
{
    protected $session;
    
    protected $dbAdapter;
    protected $dbTable = 'BC_LOG_USER_SWITCH';
    
    /**
     * Constructor
     *
     * @param  Array $options
     * @return void
     */
    public function __construct($session, Zend_Db_Adapter_Abstract $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
        $this->session = $session;
    }
    
    /**
     * preDispatch() plugin hook -- check for exceptions and dispatch error
     * handler if necessary
     *
     * @access public
     * @author Thomas Heinloth <hen@isogmbh.de>
     * @author Matthias Zeug <zeu@isogmbh.de>
     * @author Krzysztof Atlasik<atk@isogmbh.de>
     * @param  Zend_Controller_Request_Abstract $request
     * @return void
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        // skip if not switched to another user or it is not an ajax request
        if (!isset($this->session->prevUser) ||
            !($request instanceof Zend_Controller_Request_Http) ||
            true !== $request->isXmlHttpRequest() // comment this condition out for debugging purposes
        ) {
            return;
        }
        
        if ($request->getActionName() == "timeoutcheck") {
            return;
        }
        
        $frontController = Zend_Controller_Front::getInstance();
        
        // if checksum is missing, redirect to error controller w/ ACL error message
        if ($request->getParam('checksum') != $this->session->checksum) {
            $this->getResponse()->setHttpResponseCode(403);
            $this->getResponse()->setBody(
                Zend_Json::encode(
                    array(
                        'code' => 'missingchecksum',
                        'message' =>
                            'SwitchToUser: Trying to call an AJAX request w/o checksum when switched to another user'
                    )
                )
            );
             $this->getResponse()->sendResponse();
             die();
        }
        
        // determine request parameters (remove irrelevant ones)
        $parameters = array_diff($request->getParams(), $request->getUserParams());
        
        unset($parameters['checksum']);
        if (isset($parameters['_dc'])) {
            unset($parameters['_dc']);
        }
    
        
        // insert entry into logging table
        try {
            $this->dbAdapter->insert($this->dbTable, [
                'ID'                => $this->dbAdapter->nextSequenceId('SEQ_LOG_BC_USER_SWITCH'),
                'SWITCH_DATE'        => new Zend_Db_Expr("current_date"),
                'USER_ID'            => $this->session->prevUser->getId(),
                'SWITCHED_USER_ID'  => $this->session->user->getId(),
                'CONTROLLER'        => $request->getControllerName(),
                'ACTION'            => $request->getActionName(),
                'PARAMETERS'        => Zend_Json::encode($parameters)
            ]);
        } catch (Zend_Db_Exception $e) {
            // silently ignore any exceptions here
        }
    }
}
