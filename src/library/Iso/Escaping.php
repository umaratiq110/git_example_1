<?php
class Iso_Escaping
{
    public function __construct()
    {
    }
    
    public static function html($value)
    {
        $result = htmlentities($value);
        $result = nl2br($result);
        
        return $result;
    }
    
    public static function csv($value)
    {
        $result = str_replace('"', '""', $value);
        $result = str_replace("\r\n", "", $result);
        $result = str_replace("<br/>", " ", $result);
        return '"' . trim($result) . '"';
    }
    
    public static function excel($value)
    {
        $result = html_entity_decode($value);
        $result = str_replace('<br/>', chr(10), $result);
        return $result;
    }

    public static function fileNames($value)
    {
        $searchString = "/[^a-zA-Z0-9ÖÄÜßöäü_+\-()\[\]. ]/ ";
        $replacement = "";
        return preg_replace($searchString, $replacement, $value);
    }
}
