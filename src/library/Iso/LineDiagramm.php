<?php
use JpGraph\JpGraph;

class Iso_LineDiagramm extends Iso_Diagramm
{
    /*
     * Wenn dieser Wert gesetzt wird, wird an der Stelle eine
     * schwarze vertikale Linie gezeichnet.
     * Diese soll den aktuellen Monat symbolisieren
     *
     * @var int
     */
    protected $currentLine = null;

    /**
     * @param int $value
     */
    public function setCurrentLine($value)
    {
        $this->currentLine = $value;
    }

    /**
     * @return void
     */
    public function generate()
    {
        $graph = $this->initGraph();

        // add plotline(s)
        for ($loop = 0; $loop < sizeof($this->data); $loop++) {
            $line = @new LinePlot($this->data[$loop]);

            $color = $loop % sizeof($this->colors);

            $line->SetColor($this->colors[$color]);
            $line->SetWeight(4);

            $line->SetLegend($this->ccs[$loop] . ' (' . $this->sum[$loop] . ' ' . $this->translate->translate('std') . ')');

            $graph->Add($line);
        }

        // add limit line
        if (isset($this->limit) && is_array($this->limit) && sizeof($this->limit) > 0) {
            $limit_line = @new LinePlot($this->limit);
            $limit_line->SetColor("red");
            $limit_line->SetWeight(4);
            $limit_line->SetLegend("Limit");

            $graph->Add($limit_line);
        }

        // add vertical line at current day/month
        if ($this->currentLine !== null) {
            $sline = new PlotLine(VERTICAL, $this->currentLine, "black", 2);
            $sline->setLegend('current');
            $graph->Add($sline);
        }

        // render and return graph data
        return @$graph->Stroke();
    }

    /**
     * Load additional JpGraph modules
     */
    protected function initJpGraph()
    {
        parent::initJpGraph();

        @JpGraph::module('line');
        JpGraph::module('plotline');
        JpGraph::module('plotmark.inc');
    }

    /**
     * @param Graph $graph
     */
    protected function initAxis(Graph $graph)
    {
        parent::initAxis($graph);

        $graph->xaxis->SetLabelAlign('right');
    }

    /**
     * @param Graph $graph
     */
    protected function initLegend(Graph $graph)
    {
        parent::initLegend($graph);

        $graph->legend->SetLineWeight(14);
    }
}
