<?php

class Iso_Log_StopWatch
{
    /**
     * @var float
     */
    private $requestStart;

    /**
     * @var Zend_Db_Profiler
     */
    private $dbProfiler;

    /**
     * @var false|resource
     */
    private $fileHandler;
    /**
     * @var bool
     */
    private $isEnabled;

    /**
     * @var float
     */
    private $timeLastStage;

    public function __construct(Zend_Config $config, Zend_Db_Adapter_Abstract $db)
    {
        $this->isEnabled = (bool)$config->performance->profiling->enabled;
        $this->requestStart = $this->timeLastStage = $_SERVER['REQUEST_TIME_FLOAT'];
        $this->dbProfiler = $db->getProfiler();

        if ($this->isEnabled) {
            $this->fileHandler = fopen($config->performance->profiling->log->file, 'a');
            $this->write(sprintf('####################### REQUEST START - %s #########################', $_SERVER['REQUEST_URI']));
        }
    }

    public function logStage(string $stage)
    {
        if (!$this->isEnabled) {
            return;
        }
        $text = sprintf(
            "----------%s%s: %s sec. - since last stage: %s sec. %sDB-time: %s%sMemory: %sMB%s-----------",
            PHP_EOL,
            $stage,
            (microtime(true) - $this->requestStart),
            (microtime(true) - $this->timeLastStage),
            PHP_EOL,
            $this->dbProfiler->getTotalElapsedSecs(),
            PHP_EOL,
            round(memory_get_usage(true)/1048576,2),
            PHP_EOL
        );
        $this->write($text);
        $this->timeLastStage = microtime(true);
    }

    private function write(string $text)
    {
        fwrite($this->fileHandler, $text . PHP_EOL);
    }

    public function __destruct()
    {
        if (is_resource($this->fileHandler)) {
            $this->write(sprintf('Total time: %s%s', (microtime(true) - $this->requestStart), PHP_EOL));
            $this->write(
                sprintf('####################### REQUEST END - %s #########################%s', $_SERVER['REQUEST_URI'], PHP_EOL.PHP_EOL)
            );
            fclose($this->fileHandler);
        }
    }
}