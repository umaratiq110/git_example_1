<?php
/**
 * View helper adding cache control params to http resources
 *
 * @category   Iso
 * @package    Iso_View
 * @subpackage Iso_View_Helper
 */
class Iso_View_Helper_CacheControl extends Zend_View_Helper_Abstract
{
    /**
     * Add a md5 fingerprint based on the file's content to the filename
     *
     * @param string $file
     * @return string|false
     */
    public function cacheControl($file)
    {
        if (file_exists($file)) {
            $fileInfo = new SplFileInfo($file);
            $extension = '.' . $fileInfo->getExtension();

            return $fileInfo->getPath() . '/' . $fileInfo->getBasename($extension) . '_' . md5_file($file) . $extension;
        }
        return false;
    }
}
