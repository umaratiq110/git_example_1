<?php
/**
 * @category Iso
 * @package Iso_Date
 * @author Thomas Heinloth <hen@isogmbh.de>
 */
class Iso_Date extends Zend_Date
{
    const ORACLE_DATE = 'yyyy-MM-dd';
    const ORACLE_TIMESTAMP = 'yyyy-MM-dd mm:hh:ss';
}
