<?php
class Iso_Model_IsoModel extends Zend_Db_Table_Abstract
{
    /**
     * @var Iso_Log_StopWatch
     */
    private static $profiler;

    public function __construct($config = array())
    {
        parent::__construct($config);
    }

    /**
     * @return Iso_Log_StopWatch
     * @throws Zend_Exception
     */
    protected static function getProfiler()
    {
        if (self::$profiler === null) {
            self::$profiler = Zend_Registry::get('performanceLog');
        }
        return self::$profiler;
    }
}
