<?php
use JpGraph\JpGraph;

class Iso_BarDiagramm extends Iso_Diagramm
{
    /**
     * @return void
     */
    public function generate()
    {
        $graph = $this->initGraph();

        // create bar(s)
        $bars = [];
        for ($loop = 0; $loop < sizeof($this->data); $loop++) {
            $bars[$loop] = new BarPlot($this->data[$loop]);
            $bars[$loop]->SetFillColor($this->colors[$loop]);
            $bars[$loop]->SetLegend($this->ccs[$loop] . ' (' . $this->sum[$loop] . ' ' . $this->translate->translate('std') . ')');
        }

        // add bar group
        $plot = new GroupBarPlot($bars);
        $graph->Add($plot);

        // add limit line
        if (isset($this->limit) && is_numeric($this->limit) && $this->limit > 0) {
            $limit_line = new PlotLine(HORIZONTAL, $this->limit, "red", 4);
            $limit_line->setLegend('Limit');

            $graph->Add($limit_line);
        }

        // render and return graph data
        return @$graph->Stroke();
    }

    /**
     * Load additional JpGraph modules
     */
    protected function initJpGraph()
    {
        parent::initJpGraph();

        @JpGraph::module('bar');
        JpGraph::module('plotline');
        JpGraph::module('plotmark.inc');
    }

    /**
     * @param Graph $graph
     */
    protected function initLegend(Graph $graph)
    {
        parent::initLegend($graph);

        $graph->legend->SetLineWeight(8);
    }
}
