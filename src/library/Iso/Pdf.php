<?php

/**
 * @category Iso
 * @package Iso_Pdf
 * @author Niels Genau <gen@isogmbh.de>
 */
class Iso_Pdf extends Zend_Pdf
{
    private $actualPage = 0;
    private $y = 775;
    private $x = 30;
    private $maxNoticeLength = 54;
    private $zeile = 0;
    
    public function setActualPage($value)
    {
        $this->pages[$value] = new Zend_Pdf_page(Zend_Pdf_Page::SIZE_A4);
        $this->actualPage = $value;
    }
    
    public function getActualPage()
    {
        return $this->actualPage;
    }
    
    public function nextPage()
    {
        $this->setActualPage($this->getActualPage() + 1);
        $this->zeile = 1;
        $this->y = 775;
    }
    
    public function addChar($text, $whitespaces, $zeichen = " ")
    {
        $resultText = $text;
        $textLength = strlen($text);
        $neededWhitespaces = $whitespaces - $textLength;
        for ($i = 0; $i < $neededWhitespaces; $i++) {
            $resultText .= $zeichen;
        }
        return $resultText;
    }
    
    public function splitNotice($text)
    {
        $arrText = array();
        $i = 0;
        $nextZeilenStart = 0;
        $text = trim($text);
        
        while (strlen($text) > 0) {
            $textZeile = substr($text, 0, $this->maxNoticeLength);
            if (strlen($text) > strlen($textZeile)) {
                $lastWhiteSpacePos = strrpos($textZeile, " ");
                if ($lastWhiteSpacePos != false) {
                    $textZeile = substr($textZeile, 0, $lastWhiteSpacePos);
                    $nextZeilenStart = $lastWhiteSpacePos;
                } else {
                    $nextZeilenStart = $this->maxNoticeLength;
                }
            } else {
                $nextZeilenStart = $this->maxNoticeLength;
            }
            $arrText[$i++] = trim($textZeile);
            
            $text = trim(substr($text, $nextZeilenStart, strlen($text)));
        }
        
        return $arrText;
    }
    
    public function pushZeile($value)
    {
        $this->zeile += $value;
        
        if ($this->zeile > 80) {
            $this->nextPage();
        }
    }
    
    public function drawHeader($arrText, $i)
    {
        for ($o = 0; $o < count($arrText); $o++) {
            $this->pages[$i]
                ->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_COURIER), 8)
                ->setFillColor(Zend_Pdf_Color_Html::color('#000000'))
                ->drawText($arrText[$o], $this->x, 800 - ($o * 5), 'UTF-8');
        }
    }
    
    public function formatHours($stunden)
    {
        if ($stunden == 0) {
            return "";
        }
        $betterStunden = number_format($stunden, 2, ',', ' ');
        $neededWhiteSpaces = 7 - strlen($betterStunden);
        for ($i = 0; $i < $neededWhiteSpaces; $i++) {
            $betterStunden = " " . $betterStunden;
        }
        return $betterStunden;
    }
    
    public function row($text, $x, $same_line = false)
    {
        $white_spaces = "";
        for ($i = 0; $i < $x; $i++) {
            $white_spaces .= " ";
        }
        
        //if ($same_line == false){
            $this->pushZeile(1);
        $this->y -= 9;
        //}
        
        $this->pages[$this->getActualPage()]
            ->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_COURIER), 8)
            ->setFillColor(Zend_Pdf_Color_Html::color('#000000'))
            ->drawText($white_spaces . $text, $this->x, $this->y, 'UTF-8');
    }
}
