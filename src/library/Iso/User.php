<?php

class Iso_User
{
    /**
     * @var Zend_Db_Adapter_Abstract
     */
    protected $db;

    private $id = '';
    // nur ISO-Mitarbeiter!!
    private $firma = 0;
    private $kuerzel = '';
    private $vorname = '';
    private $nachname = '';
    private $userMail = '';
    private $berechtigung = 0;
    private $seeAll = false;
    private $sprache = '';
    private $mail = '';
    private $status = 'inaktiv';

    const RIGHT_TO_SEE_ALL_ENTRIES = 2048;

    // NOTE: changes to this keys requires also changes in PermissionReport.php
    const RIGHT_TO_OPEN_BKZ_COCKPIT             = 0b1;
    const RIGHT_TO_SEE_COST_AND_REVENUE         = 0b100;
    const RIGHT_TO_MONTH_END                    = 0b1000;
    const RIGHT_TO_ACCOUNTING                   = 0b10000;
    const RIGHT_TO_INTERNAL_GROUP_TRANSFER      = 0b100000;
    const RIGHT_TO_REBOOK                       = 0b1000000;
    const RIGHT_TO_SEE_PERMISSION               = 0b10000000;
    const RIGHT_TO_SEE_SAP_KEY_HOURS            = 0b100000000;
    const RIGHT_TO_SEE_REBOOK_LOG               = 0b1000000000;
    const RIGHT_TO_STANDBY                      = 0b10000000000;
    const RIGHT_TO_ABSENCE_CALENDAR             = 0b100000000000;
    const RIGHT_TO_MIS_DIVISION_OVERALL         = 0b1000000000000;
    const RIGHT_TO_PROJECT_STATUS               = 0b10000000000000;
    const RIGHT_TO_REBOOK_ALL_BOOKINGS          = 0b100000000000000;
    const RIGHT_TO_ACCOUNTABLE_HOURS            = 0b1000000000000000;
    const RIGHT_TO_ORDER_OVERVIEW               = 0b10000000000000000;
    const RIGHT_TO_CONTINGENT_RANGE             = 0b100000000000000000;
    const RIGHT_TO_CONTINGENT_DATA              = 0b1000000000000000000;
    const RIGHT_TO_CONTINGENT_SAP_INVOICE_CMP   = 0b10000000000000000000;
    const RIGHT_TO_PRISMA_VALUES                = 0b100000000000000000000;

    private $config;
    private $log;

    public function __construct($firma, $user)
    {
        if (empty($firma) || empty($user)) {
            throw new Exception('No input params: Firma: ' . $firma . ' User: ' . $user);
        }

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $this->config = $bootstrap->getResource('config');
        $this->db = $bootstrap->getResource('db');
        $this->log = $bootstrap->getResource('log');

        if (is_numeric($user)) {
            $stmt = $this->db->prepare(
                "SELECT
                  firma.id as firma_id,
                  mitarbeiter.ID,
                  mitarbeiter.KUERZEL,
                  mitarbeiter.EMAIL,
                  mitarbeiter.STATUS,
                  mitarbeiter.BKZCOCKPIT_MAIL,
                  mitarbeiter.VORNAME,
                  mitarbeiter.NACHNAME,
                  mitarbeiter.BKZCOCKPIT_RECHTE,
                  mitarbeiter.PFLEGE,
                  mitarbeiter.SPRACHE
                  FROM MITARBEITER INNER JOIN FIRMA ON MITARBEITER.ID_FIRMA = FIRMA.ID
                  WHERE FIRMA.KUERZEL = :FIRMA AND mitarbeiter.id = :ID"
            );
            $stmt->bindValue(':ID', $user);
        } else {
            $username = $this->extractUserName($user);

            if ($username != '') {
                $stmt = $this->db->prepare(
                    "SELECT
                      firma.id as firma_id,
                      mitarbeiter.ID,
                      mitarbeiter.KUERZEL,
                      mitarbeiter.EMAIL,
                      mitarbeiter.STATUS,
                      mitarbeiter.BKZCOCKPIT_MAIL,
                      mitarbeiter.VORNAME,
                      mitarbeiter.NACHNAME,
                      mitarbeiter.BKZCOCKPIT_RECHTE,
                      mitarbeiter.PFLEGE,
                      mitarbeiter.SPRACHE
                      FROM MITARBEITER INNER JOIN FIRMA ON MITARBEITER.ID_FIRMA = FIRMA.ID
                      WHERE FIRMA.KUERZEL = :FIRMA AND lower(mitarbeiter.KUERZEL) = :KUERZEL"
                );
                $stmt->bindValue(':KUERZEL', strtolower($username));
            } else {
                throw new Exception('No valid username: ' . $user);
            }
        }

        $stmt->bindValue(':FIRMA', strtolower($firma));

        if ($stmt->execute() && $row = $stmt->fetch()) {
            $this->id            = $row['ID'];
            $this->kuerzel        = $row['KUERZEL'];
            $this->vorname        = $row['VORNAME'];
            $this->nachname    = $row['NACHNAME'];
            $this->userMail        = $row['EMAIL'];
            $this->berechtigung = $row['BKZCOCKPIT_RECHTE'];
            $this->seeAll        = ($row['PFLEGE'] & self::RIGHT_TO_SEE_ALL_ENTRIES) > 0 ? true : false;
            $this->sprache        = $row['SPRACHE'];
            $this->status        = $row['STATUS'];
            $this->firma        = $row['FIRMA_ID'];
        }
    }

    /*
     * extracts the "shortcut" from an KERBEROS username (probably something like abc@isoad.isogmbh.de or abc\isoad.isogmbh.de)
     */
    private function extractUserName($username)
    {
        $username = strtoupper($username);
        $domain = $this->config->auth->http->domain;
        $pattern = sprintf("/(^%s\\\\|@%s$)/i", $domain, $domain);
        return preg_replace($pattern, '', $username);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getKuerzel()
    {
        return $this->kuerzel;
    }

    public function getVorname()
    {
        return $this->vorname;
    }

    public function getNachname()
    {
        return $this->nachname;
    }

    public function getUserMail()
    {
        return $this->userMail;
    }

    public function getSprache()
    {
        return $this->sprache;
    }

    public function setSprache($sprache)
    {
        switch ($sprache) {
            case 'german':
            case 'de':
            case 'de_DE':
            case 'de-DE':
                $this->sprache = 'de-DE';
                break;
            case 'english':
            case 'en':
            case 'en_GB':
            case 'en-GB':
                $this->sprache = 'en-GB';
                break;
            default:
                $this->sprache = 'en-GB';
                break;
        }

        $stmt = $this->db->prepare('UPDATE MITARBEITER SET SPRACHE = :SPRACHE WHERE ID = :MITARBEITER');
        $stmt->bindValue(':SPRACHE', $this->sprache);
        $stmt->bindValue(':MITARBEITER', $this->id);

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function logLogin()
    {
        //user tooladmin should not be logged
        $tooladmin_id = $this->db->fetchOne("SELECT id FROM mitarbeiter WHERE kuerzel = 'tooladmin'");
        if ($tooladmin_id == $this->id) {
            return;
        }

        $data = array(
            'MITARBEITER_ID'    => $this->id,
            'TIMESTAMP'         => new Zend_Db_Expr('localtimestamp'),
            'ISOPMTOOL_ID'      => new Zend_Db_Expr("(SELECT id FROM isopmtool WHERE name = 'BC')"),
            'USER_AGENT'        => $_SERVER['HTTP_USER_AGENT']
        );

        try {
            $table = new Iso_Login();
            $table->insert($data);
        } catch (Exception $e) {
            $this->log->err($e);
        }
    }

    /**
     * @return bool
     */
    public function isExistant()
    {
        return !($this->vorname == '' && $this->nachname == '');
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 'aktiv';
    }

    /**
     * @return bool
     */
    public function hasAdminRights()
    {
        $stmt = $this->db->prepare("
            SELECT 1
            FROM tusergroups
            JOIN tusergroups_x_employee ON tusergroups_x_employee.group_id = tusergroups.cid
            JOIN employee ON employee.id = tusergroups_x_employee.employee_id
            WHERE lower(employee.shortcut) = :SHORTCUT
            AND tusergroups.ctitle = 'admin'
        ");

        $stmt->bindValue(':SHORTCUT', $this->kuerzel);
        $stmt->execute();

        return $stmt->fetch(Zend_Db::FETCH_COLUMN) !== false;
    }

    /**
     * @return bool
     */
    public function isAllowedToUseBKZCockpit()
    {
        return ($this->berechtigung & self::RIGHT_TO_OPEN_BKZ_COCKPIT) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToProjectStatus()
    {
        return ($this->berechtigung & self::RIGHT_TO_PROJECT_STATUS) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToSeeCostAndRevenue()
    {
        return ($this->berechtigung & self::RIGHT_TO_SEE_COST_AND_REVENUE) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToSeeAccounting()
    {
        return ($this->berechtigung & self::RIGHT_TO_ACCOUNTING) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToMonthEnd()
    {
        return ($this->berechtigung & self::RIGHT_TO_MONTH_END) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToRebookAllBookings()
    {
        return ($this->berechtigung &  self::RIGHT_TO_REBOOK_ALL_BOOKINGS) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToRebook()
    {
        return ($this->berechtigung & self::RIGHT_TO_REBOOK) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToSeePermission()
    {
        return ($this->berechtigung & self::RIGHT_TO_SEE_PERMISSION) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToSeeSAPKeyHours()
    {
        return ($this->berechtigung & self::RIGHT_TO_SEE_SAP_KEY_HOURS) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToSeeRebookLog()
    {
        return ($this->berechtigung & self::RIGHT_TO_SEE_REBOOK_LOG) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToStandby()
    {
        return ($this->berechtigung & self::RIGHT_TO_STANDBY) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToAbsenceCalendar()
    {
        return ($this->berechtigung & self::RIGHT_TO_ABSENCE_CALENDAR) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToMisDivisionOverall()
    {
        return ($this->berechtigung & self::RIGHT_TO_MIS_DIVISION_OVERALL) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToSeeEverything()
    {
        return $this->seeAll;
    }

    /**
     * @return bool
     */
    public function isAllowedToAccountableHours()
    {
        return ($this->berechtigung & self::RIGHT_TO_ACCOUNTABLE_HOURS) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToOrderOverview()
    {
        return ($this->berechtigung & self::RIGHT_TO_ORDER_OVERVIEW) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToContingentRange()
    {
        return ($this->berechtigung & self::RIGHT_TO_CONTINGENT_RANGE) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToContingentData()
    {
        return ($this->berechtigung & self::RIGHT_TO_CONTINGENT_DATA) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToContingentSapInvoiceComparison(): bool
    {
        return ($this->berechtigung & self::RIGHT_TO_CONTINGENT_SAP_INVOICE_CMP) > 0;
    }

    /**
     * @return bool
     */
    public function isAllowedToPrismaValues(): bool
    {
        return ($this->berechtigung & self::RIGHT_TO_PRISMA_VALUES) > 0;
    }
}
