<?php

/**
 * Quick and dirty normalization for values used from Zend_Json::decode without further sanitation
 */
class Iso_Serializer_Denormalizer
{
    /**
     * @param $values
     * @return array|mixed|StdClass
     * @throws Exception
     */
    public static function denormalize($values)
    {
        foreach ($values as $key => $value) {
            if (!is_scalar($value) && !is_null($value)) {
                $value = self::denormalize($value);
            }
            if ($values instanceof StdClass) {
                $values->$key = self::sanitize($value);
            }
            elseif (is_array($values)) {
                $values[$key] = self::sanitize($value);
            }
            else {
                throw new Exception('unsupported value type');
            }
        }
        return $values;
    }

    /**
     * convert empty strings to null
     *
     * @param $value
     * @return mixed|null
     */
    public static function sanitize($value)
    {
        if ($value === '') {
            # empty strings handling as done in oracle
            return null;
        }
        return $value;
    }
}