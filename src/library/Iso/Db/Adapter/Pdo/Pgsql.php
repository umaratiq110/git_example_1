<?php



class Iso_Db_Adapter_Pdo_Pgsql extends Zend_Db_Adapter_Pdo_Pgsql
{
    /**
     * override to allow quoted identifiers (reserved words in column names) without breaking by usage of upper case column naming...
     * @param array|string|Zend_Db_Expr $ident
     * @param null $alias
     * @param false $auto
     * @param string $as
     * @return string
     * @throws Exception
     */
    protected function _quoteIdentifierAs($ident, $alias = null, $auto = false, $as = ' AS ')
    {
        $ident = $this->quoteIdent($ident);
        $alias = $this->quoteIdent($alias);
        return parent::_quoteIdentifierAs($ident, $alias, $auto, $as);
    }

    /**
     * Override due to Zend Db not working with postgres versions > 9
     *
     * @param string $tableName
     * @param null $schemaName
     * @return array
     * @throws Zend_Db_Adapter_Exception
     * @throws Zend_Db_Statement_Exception
     */
    public function describeTable($tableName, $schemaName = null)
    {
        $tableName = strtolower($tableName);
        $schemaName = strtolower($schemaName);
        $sql = "SELECT
                a.attnum,
                n.nspname,
                c.relname,
                a.attname AS colname,
                t.typname AS type,
                a.atttypmod,
                FORMAT_TYPE(a.atttypid, a.atttypmod) AS complete_type,
                pg_get_expr(d.adbin, d.adrelid) AS default_value,
                a.attnotnull AS notnull,
                a.attlen AS length,
                co.contype,
                ARRAY_TO_STRING(co.conkey, ',') AS conkey
            FROM pg_attribute AS a
                JOIN pg_class AS c ON a.attrelid = c.oid
                JOIN pg_namespace AS n ON c.relnamespace = n.oid
                JOIN pg_type AS t ON a.atttypid = t.oid
                LEFT OUTER JOIN pg_constraint AS co ON (co.conrelid = c.oid
                    AND a.attnum = ANY(co.conkey) AND co.contype = 'p')
                LEFT OUTER JOIN pg_attrdef AS d ON d.adrelid = c.oid AND d.adnum = a.attnum
            WHERE a.attnum > 0 AND c.relname = ".$this->quote($tableName);
        if ($schemaName) {
            $sql .= " AND n.nspname = ".$this->quote($schemaName);
        }
        $sql .= ' ORDER BY a.attnum';

        $stmt = $this->query($sql);

        // Use FETCH_NUM so we are not dependent on the CASE attribute of the PDO connection
        $result = $stmt->fetchAll(Zend_Db::FETCH_NUM);

        $attnum        = 0;
        $nspname       = 1;
        $relname       = 2;
        $colname       = 3;
        $type          = 4;
        $atttypemod    = 5;
        $complete_type = 6;
        $default_value = 7;
        $notnull       = 8;
        $length        = 9;
        $contype       = 10;
        $conkey        = 11;

        $desc = array();
        foreach ($result as $key => $row) {
            $defaultValue = $row[$default_value];
            if ($row[$type] == 'varchar' || $row[$type] == 'bpchar' ) {
                if (preg_match('/character(?: varying)?(?:\((\d+)\))?/', $row[$complete_type], $matches)) {
                    if (isset($matches[1])) {
                        $row[$length] = $matches[1];
                    } else {
                        $row[$length] = null; // unlimited
                    }
                }
                if (preg_match("/^'(.*?)'::(?:character varying|bpchar)$/", $defaultValue, $matches)) {
                    $defaultValue = $matches[1];
                }
            }
            list($primary, $primaryPosition, $identity) = array(false, null, false);
            if ($row[$contype] == 'p') {
                $primary = true;
                $primaryPosition = array_search($row[$attnum], explode(',', $row[$conkey])) + 1;
                $identity = (bool) (preg_match('/^nextval/', $row[$default_value]));
            }
            $desc[$this->foldCase($row[$colname])] = array(
                'SCHEMA_NAME'      => $this->foldCase($row[$nspname]),
                'TABLE_NAME'       => $this->foldCase($row[$relname]),
                'COLUMN_NAME'      => $this->foldCase($row[$colname]),
                'COLUMN_POSITION'  => $row[$attnum],
                'DATA_TYPE'        => $row[$type],
                'DEFAULT'          => $defaultValue,
                'NULLABLE'         => (bool) ($row[$notnull] != 't'),
                'LENGTH'           => $row[$length],
                'SCALE'            => null, // @todo
                'PRECISION'        => null, // @todo
                'UNSIGNED'         => null, // @todo
                'PRIMARY'          => $primary,
                'PRIMARY_POSITION' => $primaryPosition,
                'IDENTITY'         => $identity
            );
        }
        return $desc;
    }

    /**
     * recursively lower cases idents (except Zend_Db_Expr)
     *
     * @param $ident
     * @return array|string|Zend_Db_Expr
     * @throws Exception
     */
    private function quoteIdent($ident)
    {
        if (is_string($ident)) {
            return strtolower($ident);
        }
        elseif (is_array($ident)) {
            foreach ($ident as $i => $item) {
                $ident[$i] = $this->quoteIdent($item);
            }
            return $ident;
        }
        elseif ($ident instanceof Zend_Db_Expr) {
            return $ident;
        }
        elseif ($ident === null) {
            return null;
        }
        else {
            throw new Exception('ident type not implemented yet');
        }
    }
}