<?php

/**
 * Class Iso_Db_Profiler_File
 *
 * @category Iso
 * @package Iso_Db
 * @subpackage Iso_Db_Profiler
 */
class Iso_Db_Profiler_File extends Zend_Db_Profiler
{
    /**
     * @var Zend_Logwriter_Stream
     */
    protected $writer = null;

    /**
     * @var Zend_Log
     */
    protected $logger = null;

    /**
     * @var string
     */
    protected static $logFile = null;

    /*
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if (self::$logFile === null) {
            throw new Iso_Exception("No logfile was configured");
        }

        try {
            $this->writer = new Zend_Log_Writer_Stream(self::$logFile);
            $this->logger = new Zend_Log($this->writer);
        } catch (Exception $ex) {
            $this->logger = null;
            $this->writer = null;
        }
    }

    /**
     * Intercept the query end and log the profiling data.
     *
     * @param  integer $queryId
     * @throws Zend_Db_Profiler_Exception
     * @return void
     */
    public function queryEnd($queryId)
    {
        parent::queryEnd($queryId);

        return;
        // If the profiler is not enabled then do nothing
        if (!$this->_enabled) {
            return;
        }

        // Query-Profile laden
        $profile = $this->getQueryProfile($queryId);
        $text = $this->buildLogText($profile);
        $this->writeToFile($text);
    }

    /**
     * @param string $text
     */
    protected function writeToFile($text)
    {
        if ($this->logger != null && $text != null) {
            $this->logger->info($text);
        }
    }

    /**
     * @param Zend_Db_Profiler_Query $profile
     * @return string
     */
    private function buildLogText(Zend_Db_Profiler_Query $profile)
    {
        $text = '';

        if ($profile != null) {
            $params = $profile->getQueryParams();

            $text .= PHP_EOL;
            $text .= '--------------------------------------' . PHP_EOL;
            $text .= 'Query  : ' . $profile->getQuery()       . PHP_EOL;
            $text .= 'Type   : ' . $profile->getQueryType()   . PHP_EOL;
            $text .= 'Time   : ' . $profile->getElapsedSecs() . PHP_EOL;
            $text .= 'Params : ' . sizeof($params)            . PHP_EOL;

            if (is_array($params) && sizeof($params) > 0) {
                foreach ($params as $param) {
                    $text .= 'Param : ' . $param . PHP_EOL;
                }
            }

            $text .= '--------------------------------------' . PHP_EOL;
        }

        return $text;
    }

    /**
     * @param string $filePath
     */
    public static function setLogFile($filePath)
    {
        self::$logFile = $filePath;
    }
}
