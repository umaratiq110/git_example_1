<?php

/**
 * Datenbank Adapter für die Translate Klasse aus dem Zend Framework
 * @author wap
 * @since 04.03.2008
 */
class Iso_Translate_Adapter_Database extends Zend_Translate_Adapter
{
    
    //Speichert das Ergebnis der Language Anfrage, also die gefundenen Einträge zu der Seite mit entweder
    // deustcher oder englischer Bezeichung, je nach MA Sprache
    private $result;
    private $language;

    /**
     * Load translation data
     *
     * @param  string|array  $data //Gibt an von wo die Daten kommen
     * @param  string        $locale  //gibt an welche Seite aufgerufen wird
     * @param  array         $options OPTIONAL Options to use
     */
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected function _loadTranslationData($dialog, $locale, array $options = array())
    {
        // @codingStandardsIgnoreEnd
        //Wandelt die Sprache für den SQL um
        switch ($locale) {
            case 'de':
            case 'de_DE':
                $language = 'SPRACHE1';
                $this->language = $language;
                break;
            case 'en':
            default:
                $language = 'SPRACHE2';
                $this->language = $language;
                break;
        }
        
        $options = array_merge($this->_options, $options);
        if (($options['clear'] == true) || !isset($this->_translate[$locale])) {
            $this->_translate[$locale] = array();
        }
        
        $data = array();
        
        /*
         * Datenbankstatement bereitstellen und ausführen
         */
        $db = Zend_Registry::get('db');
         
        $stmt = $db->prepare(
            "SELECT BEZEICHNUNG," . $language . " AS Text FROM LANGUAGE WHERE DIALOGLISTE LIKE '%' || :dialog || '%'"
        );
        $stmt->setFetchMode(Zend_Db::FETCH_OBJ);

        $stmt->bindValue(':dialog', $dialog);
        
        $stmt->execute();
        $rows = $stmt->fetchAll();
        
        foreach ($rows as $row) {
            $data[$row->BEZEICHNUNG] = $row->TEXT;
        }
        
        $this->result = $rows;
        
        $this->_translate[$locale] = array_merge($this->_translate[$locale], $data);
    }
    
    /**
     * Gibt das Translate Array für den Datastore aus
     */
    public function getTranslateArray()
    {
        $lang = [];
        foreach ($this->result as $item) {
            $lang[] = array(
                'bezeichnung' => $item->BEZEICHNUNG,
                'sprache' => $item->TEXT
            );
        }
        
        return $lang;
    }
    
    /**
     * returns the adapters name
     *
     * @return string
     */
    public function toString()
    {
        return "Database";
    }
}
