<?php

/**
 * @category   Zend
 * @package    Zend_Translate
 * @copyright  Copyright (c) 2005-2007 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Iso_Translate_DbTranslate extends Zend_Translate
{
    protected $adapter = null;

    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * Overriding Zend-Function,
     * see comment for difference.
     *
     */
    public function setAdapter($options = array())
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        } elseif (func_num_args() > 1) {
            $args               = func_get_args();
            $options            = array();
            $options['adapter'] = array_shift($args);
            if (!empty($args)) {
                $options['content'] = array_shift($args);
            }

            if (!empty($args)) {
                $options['locale'] = array_shift($args);
            }

            if (!empty($args)) {
                $opt     = array_shift($args);
                $options = array_merge($opt, $options);
            }
        } elseif (!is_array($options)) {
            $options = array('adapter' => $options);
        }

        $adapter = $options['adapter'];

        // This is the only differnce to the super-method
        if (strtolower($adapter) == 'database') {
            $adapter = 'Iso_Translate_Adapter_Database';
            $options['adapter'] = 'Iso_Translate_Adapter_Database';
        }

        if (Zend_Loader::isReadable('Zend/Translate/Adapter/' . ucfirst($options['adapter']) . '.php')) {
            $options['adapter'] = 'Zend_Translate_Adapter_' . ucfirst($options['adapter']);
        }

        if (!class_exists($options['adapter'])) {
            Zend_Loader::loadClass($options['adapter']);
        }

        if (array_key_exists('cache', $options)) {
            Zend_Translate_Adapter::setCache($options['cache']);
        }

        unset($options['adapter']);

        $this->adapter = new $adapter($options);

        if (!$this->adapter instanceof Zend_Translate_Adapter) {
            throw new Zend_Translate_Exception("Adapter " . $adapter . " does not extend Zend_Translate_Adapter");
        }
    }
}
