<?php
declare(strict_types=1);

/**
 * Handles request result for jira issue requests
 */
class Result
{
    /**
     * @var Zend_Http_Response|null
     */
    private $response;

    /**
     * @var Zend_Http_Client_Exception|null
     */
    private $exception;

    /**
     * @param Zend_Http_Response|null $response
     */
    public function setResponse(Zend_Http_Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return Zend_Http_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return Zend_Http_Client_Exception
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * @param Zend_Http_Client_Exception $exception
     */
    public function setException(Zend_Http_Client_Exception $exception)
    {
        $this->exception = $exception;
    }

    /**
     * @return bool
     */
    public function hasError(): bool
    {
        return $this->exception !== null;
    }

    /**
     * @return string|null
     */
    public function getErrorMessage()
    {
        if ($this->hasError()) {
            return $this->exception->getMessage();
        }
        return null;
    }
}
