<?php
declare(strict_types=1);

/**
 * Simple DTO for passing issue data
 */
class Jira_IssueDTO
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $summary;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $classification;

    /**
     * @var string
     */
    private $projectName;

    /**
     * IssueDTO constructor.
     * @param string $url
     * @param string $summary
     * @param string $status
     * @param string $classification
     * @param string $projectName
     */
    public function __construct(string $url, string $summary, string $status, string $classification, string $projectName)
    {
        $this->url = $url;
        $this->summary = $summary;
        $this->status = $status;
        $this->classification = $classification;
        $this->projectName = $projectName;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getSummary(): string
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     */
    public function setSummary(string $summary)
    {
        $this->summary = $summary;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getClassification(): string
    {
        return $this->classification;
    }

    /**
     * @param string $classfication
     */
    public function setClassification(string $classification)
    {
        $this->classification = $classification;
    }

    /**
     * @return string
     */
    public function getProjectName(): string
    {
        return $this->projectName;
    }

    /**
     * @param string $projectName
     */
    public function setProjectName(string $projectName)
    {
        $this->projectName = $projectName;
    }
}
