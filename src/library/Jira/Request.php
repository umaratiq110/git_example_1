<?php

class Jira_Request
{
    /**
     * @var Zend_Config
     */
    protected $config;

    /**
     * @var Zend_Http_Client
     */
    protected $client;

    /**
     * Request constructor.
     * @param Zend_Config $config
     */
    public function __construct(Zend_Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param bool $fresh
     * @return Zend_Http_Client
     * @throws Zend_Http_Client_Exception
     */
    protected function getClient(bool $fresh = false): Zend_Http_Client
    {
        if (!$fresh && $this->client !== null) {
            return $this->client;
        }
        $curlOpts = [
            CURLOPT_PROXY => $this->config->http->client->proxy->host,
            CURLOPT_PROXYPORT => $this->config->http->client->proxy->port
        ];
        if (APPLICATION_ENV !== 'prod') {
            //due to self-signed certs on dev and test-systems, we need to disable ssl verification
            $curlOpts[CURLOPT_SSL_VERIFYPEER] = false;
        }
        $this->client = new Zend_Http_Client(null, [
            'adapter' => Zend_Http_Client_Adapter_Curl::class,
            'curloptions' => $curlOpts
        ]);
        $this->client->setAuth($this->config->jira->rest->api->username, $this->config->jira->rest->api->password);

        return $this->client;
    }

    /**
     * @param $url string
     * @param $postData array|null
     * @return Jira_Result
     */
    protected function getResult($url, $postData)
    {
        $result = new Jira_Result();
        try {
            $client = $this->getClient();
            $client->setUri($url);
            $client->setHeaders('Content-type', 'application/json');
            if (isset($postData)) {
                $client->setRawData(json_encode($postData));
                $response = $client->request('POST');
            } else {
                $response = $client->request('GET');
            }
            $result->setResponse($response);
        } catch (Zend_Http_Client_Exception $e) {
            $result->setException($e);
        }
        return $result;
    }
}
