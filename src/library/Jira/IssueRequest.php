<?php
declare(strict_types=1);

/**
 * Requests basic jira issue data via jira api
 */
class Jira_IssueRequest extends Jira_Request
{
    /**
     * @param array $issueKeys
     * @return Jira_Result
     */
    public function fetchTicketData(string $issueKey): Jira_Result
    {
        $url = sprintf('%s/issue/' . $issueKey, $this->config->jira->rest->api->url);
        $result = new Jira_Result();
        try {
            $client = $this->getClient();
            $client->setUri($url);
            $response = $client->request('GET');
            $result->setResponse($response);
        } catch (Zend_Http_Client_Exception $e) {
            $result->setException($e);
        }
        return $result;
    }
}
