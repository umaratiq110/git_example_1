<?php
declare(strict_types=1);

/**
 * Service for fetching jira role data via jira rest api
 */
class Jira_FetchRoles
{
    /**
     * @var Zend_Config
     */
    private $config;

    /**
     * @var Zend_Log
     */
    private $log;

    /**
     * Constructor.
     * @param Zend_Config $config
     * @param Zend_Log $log
     */
    public function __construct(Zend_Config $config, Zend_Log $log)
    {
        $this->config = $config;
        $this->log = $log;
    }

    /**
     * @param string $userToken
     * @return Jira_Roles|null
     */
    public function fetch(string $userToken)
    {
        $req = new Jira_RolesRequest($this->config);
        $result = $req->fetch($userToken);
        if ($result->hasError()) {
            $this->log->err((string)$result->getException());
            return null;
        }
        $roles = new Jira_Roles($this->config, $result->getResponse());
        if ($roles->hasErrors()) {
            $this->log->err(implode('; ', $roles->getErrors()));
            return null;
        }
        return $roles;
    }
}
