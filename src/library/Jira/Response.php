<?php

abstract class Jira_Response
{
    /**
     * @var array
     */
    private $errors = [];

    /**
     * Issues constructor.
     * @param Zend_Config $config
     * @param Zend_Http_Response $response
     */
    public function __construct(Zend_Config $config, Zend_Http_Response $response)
    {
        $this->parseResponse($config, $response);
    }

    abstract protected function parseResponse(Zend_Config $config, Zend_Http_Response $response);

    /**
     * @param Zend_Http_Response $response
     * @param $emptyArrayNameForError string|null
     * @return bool
     */
    protected function validateResponse(Zend_Http_Response $response): bool
    {
        if ($response->isError()) {
            $this->errors[] = sprintf('response (%d) has error: %s', $response->getStatus(), $response->getMessage());
            return false;
        }
        $rawIssueData = json_decode($response->getBody(), true);
        if (isset($rawIssueData['errorMessages'])) {
            $this->errors = $rawIssueData['errorMessages'];
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return count($this->errors) !== 0;
    }

    /**
     * @return string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
