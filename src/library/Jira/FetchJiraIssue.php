<?php
declare(strict_types=1);

/**
 * Service for fetching jira issue data via jira rest api
 */
class Jira_FetchJiraIssue
{
    /**
     * @var Zend_Config
     */
    private $config;

    /**
     * @var Zend_Log
     */
    private $log;

    /**
     * JiraData constructor.
     * @param Zend_Config $config
     * @param Zend_Log $log
     */
    public function __construct(Zend_Config $config, Zend_Log $log)
    {
        $this->config = $config;
        $this->log = $log;
    }

    /**
     * @param array $issueKeys
     * @return Jira_Issue|null
     */
    public function fetch(string $issueKey)
    {
        $req = new Jira_IssueRequest($this->config);
        $result = $req->fetchTicketData($issueKey);
        if ($result->hasError()) {
            $this->log->err((string)$result->getException());
            return null;
        }
        $issue = new Jira_Issue($this->config, $result->getResponse());
        if ($issue->hasErrors()) {
            $this->log->err(implode('; ', $issue->getErrors()));
            return null;
        }
        return $issue;
    }
}
