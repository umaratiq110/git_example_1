<?php
declare(strict_types=1);

/**
 * Requests basic jira project data via jira api
 */
class ProjectsRequest extends Request
{
    /**
     * @return Result
     */
    public function fetch(): Result
    {
        $url = sprintf('%s/rest/api/2/project', $this->config->jira->base->url);

        return $this->getResult($url, null);
    }
}
