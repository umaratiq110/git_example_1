<?php
declare(strict_types=1);

/**
 * Requests basic jira role data via jira api
 */
class Jira_RolesRequest extends Jira_Request
{
    /**
     * @param string $userToken
     * @return Jira_Result
     */
    public function fetch(string $userToken): Jira_Result
    {
        $url = sprintf('%s/rest/jiraaction/latest/user/%s/projectRole', $this->config->jira->base->url, $userToken);

        return $this->getResult($url, null);
    }
}
