<?php
declare(strict_types=1);

/**
 * Jira IssueDTO provider
 * parses response from jira rest api
 */
class Jira_Issue extends Jira_Response
{
    /**
     * @var Jira_IssueDTO
     */
    private $issue;

    /**
     * parses response
     * @param Zend_Config $config
     * @param Zend_Http_Response $response
     */
    protected function parseResponse(Zend_Config $config, Zend_Http_Response $response)
    {
        if (!$this->validateResponse($response)) {
            return;
        }
        $issueData = json_decode($response->getBody(), true);

        $url = sprintf(
            '%s/browse/%s',
            $config->jira->base->url,
            $issueData['key']
        );
        $summary = $issueData['fields']['summary'];
        $status  = $issueData['fields']['status']['name'];
        $projectName = $issueData['fields']['project']['name'];
        $categoryCustomFieldId = sprintf('customfield_%s', $config->jira->category->customfield->id);
        $classification = '-';

        if (array_key_exists($categoryCustomFieldId, $issueData['fields'])) {
            $value = $issueData['fields'][$categoryCustomFieldId];

            if ($value !== null) {
                $classification = $value['value'];
            }
        }

        $this->issue = new Jira_IssueDTO($url, $summary, $status, $classification, $projectName);
    }

    /**
     * @return Jira_IssueDTO
     */
    public function getIssue(): Jira_IssueDTO
    {
        return $this->issue;
    }
}
