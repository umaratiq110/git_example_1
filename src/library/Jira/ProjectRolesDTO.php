<?php
declare(strict_types=1);

/**
 * Simple DTO for passing project role data
 */
class Jira_ProjectRolesDTO
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $userRoles;

    /**
     * ProjectRolesDTO constructor.
     * @param int $id
     * @param string $key
     * @param string $name
     * @param array $userRoles
     */
    public function __construct(int $id, string $key, string $name, array $userRoles)
    {
        $this->id = $id;
        $this->key = $key;
        $this->name = $name;
        $this->userRoles = $userRoles;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey(string $key)
    {
        $this->key = $key;
    }

    /**
     * @return Jira_UserRoleDTO[]
     */
    public function getUserRoles(): array
    {
        return $this->userRoles;
    }

    /**
     * @param array $userRoles
     */
    public function setUserRoles(array $userRoles)
    {
        $this->userRoles = $userRoles;
    }
}
