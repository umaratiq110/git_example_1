<?php
declare(strict_types=1);

/**
 * Jira RoleDTO provider
 * parses response from jira rest api
 */
class Jira_Roles extends Jira_Response
{
    /**
     * @var array
     */
    private $roles = [];

    /**
     * parses response
     * @param Zend_Config $config
     * @param Zend_Http_Response $response
     */
    protected function parseResponse(Zend_Config $config, Zend_Http_Response $response)
    {
        if (!$this->validateResponse($response)) {
            return;
        }
        $rawProjectRolesData = json_decode($response->getBody(), true);

        foreach ($rawProjectRolesData as $projectRole) {
            $userRoles = [];

            foreach ($projectRole['userRoles'] as $userRole) {
                $userRoles[] = new Jira_UserRoleDTO($userRole['id'], $userRole['name']);
            }

            $this->roles[] = new Jira_ProjectRolesDTO($projectRole['id'], $projectRole['key'], $projectRole['name'], $userRoles);
        }
    }

    /**
     * @return Jira_ProjectRolesDTO[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }
}
