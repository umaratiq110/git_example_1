<?php
/*
 * IndexController.php
 *
 * Handelt die Actions im BKZ-Cockpit
 */
class IndexController extends Iso_Controller_DefaultController
{
    protected $languageFile = 'BC_index.php';

    private $chartprofil  = null;

    public function indexAction()
    {
        if (preg_match('/MSIE [5-8]/', $_SERVER['HTTP_USER_AGENT']) > 0) {
            $this->initView();
            $this->render('unsupportedbrowser');
        } else {
            // Session ID erneuern
            Zend_Session::regenerateId();

            $this->initView();
            $this->view->languageType = $this->languageType;

            // zum Escapen der Daten in der View die Klasse Iso_Escaping verwenden
            $this->view->setEscape(array('Iso_Escaping', 'html'));

            // make sure default profile exists and is stored in session
            // somehow it is possible to get here without an active session
            // so check if the default profile exists or could be created
            if ($this->loadDefaultProfile() === false) {
                # otherwise logout to ensure a functional session
                $this->redirect('/login/logout');
            }

            //Sprache
            if ($this->languageType == 'de') {
                $this->view->languageparameter = 'language/english';
            } else {
                $this->view->languageparameter = 'language/german';
            }

            $this->render('index');
        }
    }

    public function getinitialdataAction()
    {
        $this->getResponse()->setHeader('Content-Type', 'application/javascript');

        $this->initView();

        // Load initial data
        $this->view->userdata = Zend_Json::encode($this->getInitialData());
        $this->view->translations = str_replace(
            '\n',
            '<br/>',
            Zend_Json::encode(
                array('translations' => $this->translate->getTranslator()->getTranslateArray())
            )
        );
        $this->viewSuffix = 'pjs';
        $this->render('initialdata');
    }


    public function assignedemployeesAction()
    {
        /**
         * Mitarbeiter auf die eingeschraenkt werden darf laden
         */
        $mitarbeiter = new Mitarbeiter();
        $employees = $mitarbeiter->getRelatedEmployees($this->user->getId(), $this->config->company->ownId);

        echo Zend_Json::encode($employees);
    }

    public function timeoutcheckAction()
    {
        echo 1;
    }


    public function norouteAction()
    {
        $this->indexAction();
    }

    /**
     * Delivers the help file for general BC usage or a specific report type
     */
    public function helpAction()
    {
        $reportTypeShortcut = $this->getRequest()->getParam('report');
        $sql = 'SELECT helppage_hash
                FROM bc_report_type 
                WHERE shortcut = :REPORT_TYPE_SHORTCUT';

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':REPORT_TYPE_SHORTCUT', $reportTypeShortcut);
        $stmt->execute();
        $helpPageHash = $stmt->fetchColumn(0);

        if (empty($helpPageHash)) {
            $helpPageHash = $this->config->confluence->centralHelpPage->hash;
        }

        $helpUrl = $this->config->confluence->base->url . '/x/' . $helpPageHash;

        $this->redirect($helpUrl);
    }

    /**
     * Validates if profile data is consistent
     */
    public function validateAction()
    {
        $chartprofil = $this->getRequest()->getParam('chartprofil');
        $check = new Zend_Filter_Input(
            array('*' => 'StringTrim'),
            array('chartprofil' => 'Digits'),
            array('chartprofil' => $chartprofil)
        );

        if ($check->isValid() && $check->chartprofil > 0) {
            $chartprofil = new ChartProfil();
            $profile = $chartprofil->find($check->chartprofil)->current();
            $validator = new ReportValidator($this->translate);
            $result = $validator->validate($profile);
            if (isset($result['error']) && true === $result['error']) {
                $response = array_merge(array('success' => false), $result);
            } else {
                $response = array('success' => true, 'hint' => $result['hint']);
            }

            echo Zend_Json::encode($response);
        }
    }

    /**
     * Ausgabe generieren
     */
    public function showAction()
    {
        $request = $this->getRequest();
        $profileId = $request->getParam('chartprofil');
        $timestamp = $request->getParam('timestamp');

        $check = new Zend_Filter_Input(
            array('*' => 'StringTrim'),
            array('chartprofil' => 'Digits'),
            array('chartprofil' => $profileId)
        );

        if ($check->isValid() && $check->chartprofil > 0) {
            $chartprofil = new ChartProfil();
            $profiles = $chartprofil->find($check->chartprofil);

            $report = new ReportRenderer($this->session, $this->user, $this->translate);
            if ($profil = $profiles->current()) {
                // If report belongs to another user skip making this profile default
                if ($profil->MITARBEITER_ID == $this->user->getId()) {
                    ChartProfil::copyChartProfileToDefault($check->chartprofil, $this->session, $this->db);
                }

                if (in_array($profil->ANZEIGE, ['F', 'I', 'J', 'G', 'Q', 'Z', 'Y'])) {
                    $result = $report->renderToDownload($this, $check->chartprofil, $timestamp);
                } elseif ($profil->ANZEIGE == 'A') {
                    $result = $report->saveAccounting($this, $check->chartprofil, $timestamp);
                } else {
                    $result = $report->render($this, $check->chartprofil, $timestamp, $this->user->getSprache());
                }

                echo Zend_Json::encode($result);
            }
        }
    }

    /**
     * Renders direct reports
     */
    public function showdirectAction()
    {
        $request = $this->getRequest();
        $type = $request->getParam('type');
        $timestamp = $request->getParam('timestamp');

        // get contents of report file, which was already generated from download folder
        $path = $this->config->main->download->save_path . $type . '_' . $timestamp;

        if (!file_exists($path)) {
            return;
        }

        $content = file_get_contents($path);
        unlink($path);

        // get filename and set response headers according to report type
        switch ($type) {
            case 'F':
                $sessionKey = 'btOrder';
                break;
            case 'I':
                $sessionKey = 'misDivision';
                break;
            case 'J':
                $sessionKey = 'misOverall';
                break;
            case 'G':
                $sessionKey = 'accountableHours';
                break;
            case 'Q':
                $sessionKey = 'contingentRange';
                break;
            case 'Z':
                $sessionKey = 'contingentData';
                break;
            case 'Y':
                $sessionKey = 'prismaValues';
                break;
            default:
                throw new Iso_Exception(sprintf("Report type '%s' does not support direct download generation", $type));
        }

        $sessionData = $this->session->{$sessionKey}[$timestamp];
        unset($this->session->{$sessionKey}[$timestamp]);

        $this->getResponse()
            ->setHeader('Content-Type', $sessionData['contentType'])
            ->setHeader('Content-Disposition', 'attachment; filename=' . $sessionData['filename']);

        echo $content;
    }

    /**
     * Chart-Profil löschen
     */
    public function deletechartprofileAction()
    {
        $chartprofil_param = $this->getRequest()->getParam('chartprofil');

        $check = new Zend_Filter_Input(
            array('*' => 'StringTrim', ),
            array('chartprofil' => 'Digits'),
            array('chartprofil' => $chartprofil_param)
        );

        $result = false;

        if ($check->isValid()) {
            $chartprofil = new ChartProfil();
            $rows = $chartprofil->find($check->chartprofil);

            if ($rows->count() > 0) {
                $row = $rows->current();

                if ($row->MITARBEITER_ID == $this->user->getId()) {
                    $result = ChartProfil::removeProfile($row->ID);
                }
            }
        }

        echo Zend_Json::encode(array('result' => $result));
    }

    /**
     * Chart-Profil speichern
     */
    public function savechartprofileAction()
    {
        $result = false;

        $name = trim($this->getRequest()->getParam('bezeichnung'));
        $configuration = Zend_Json::decode($this->getRequest()->getParam('configuration'), Zend_Json::TYPE_OBJECT);
        $params = Zend_Json::decode($this->getRequest()->getParam('params'), Zend_Json::TYPE_OBJECT);
        $check = new Zend_Filter_Input(
            array('*' => 'StringTrim'),
            array('name' => new Zend_Validate_Regex('/^[a-zA-Z0-9ÖÄÜßöäü_+\-()\[\]. ]+$/')),
            array('name' => $name)
        );
        if ($check->isValid() || empty($name)) {
            $result = ChartProfil::saveChartProfile(
                substr($check->getUnescaped('name'), 0, 100),
                Iso_Serializer_Denormalizer::denormalize($configuration),
                $params,
                $this->user,
                $this->db
            );
        }
        echo Zend_Json::encode(array('result' => $result));
    }

    /**
     * Loads profiles
     */
    public function loadprofilesAction()
    {
        $sql = "
            WITH chpshraedemp AS (" . ChartProfil::getChartProfileSharedEmployeesSql(). ")

            SELECT
                chp.*,
                to_char(chp.zeitraum_von, 'YYYYMMDD') AS sort_zeitraum_von,
                to_char(chp.zeitraum_bis, 'YYYYMMDD') AS sort_zeitraum_bis,
                m.kuerzel as shared_with,
                chpxae.add_emp_id,
                chpxn.set_id,
                chpxn.set_name,
                chpxn.node_id,
                chpxn.node_type_id,
                chpxn.structure_unit_type_id,
                chpxn.single_node_id,
                chpxn.single_node_types,
                chpxn.node_name,
                chpxn.node_path_id,
                chpxn.node_path_name,
                chpxe.employee_id,
                chpxe.employee_shortcut,
                chpxe.employee_first_name,
                chpxe.employee_last_name,
                chpxe.employee_group_id,
                chpxe.employee_group_name,
                chpxbt.booking_type_id,
                chpxbt.sort_order AS booking_type_sort_order,
                chpxaddc.bc_additional_col_id,
                chpxaddc.bc_additional_col_sort_order,
                chpshraedemp.share_emp_id,
                CASE WHEN :EMPLOYEE_ID IN (
                    SELECT mitarbeiter_id
                    FROM bc_chartp_x_shared_emp
                    WHERE bc_chart_profil_id = chp.id
                )
                THEN
                    1
                ELSE
                    0
                END AS shared_profile,
                CASE WHEN :EMPLOYEE_ID IN (
                    SELECT mitarbeiter.id
                    FROM mitarbeiter
                    INNER JOIN employee emp ON lower(mitarbeiter.kuerzel) = lower(emp.shortcut)
                    INNER JOIN employee_node_permission ON employee_node_permission.employee_id = emp.id
                    WHERE mitarbeiter.id = :EMPLOYEE_ID
                    AND employee_node_permission.node_id = 1
                )
                THEN
                    1
                ELSE
                    0
                END AS has_root_node_permission,
                material_filter.material_filter_id
            FROM bc_chart_profil chp
            INNER JOIN mitarbeiter m ON chp.mitarbeiter_id = m.id
            LEFT JOIN (
                SELECT
                    bc_chart_profil_id,
                    m.id AS add_emp_id
                FROM bc_chartp_x_addemp
                INNER JOIN mitarbeiter m ON bc_chartp_x_addemp.mitarbeiter_id = m.id
                INNER JOIN employee emp ON lower(m.kuerzel) = lower(emp.shortcut)
                INNER JOIN employee_state ON employee_state.employee_id = emp.id
                WHERE m.status = 'aktiv'
                AND employee_state.is_active = 1
                AND m.bkzcockpit_rechte > 0
                AND m.bkzcockpit_rechte & 1 > 0
            ) chpxae on chp.id = chpxae.bc_chart_profil_id
            LEFT JOIN chpshraedemp on chp.id = chpshraedemp.bc_chart_profil_id
            LEFT JOIN (
                SELECT
                    bc_chart_profil_x_node.*,
                    CASE
                      WHEN node_version.long_name IS NOT NULL THEN node_version.name || ' [' || node_version.long_name || ']'
                      WHEN node_version.customer_name IS NOT NULL THEN node_version.name || ' [' || node_version.customer_name || ']'
                      ELSE node_version.name
                    END AS node_name,
                    node.node_type_id,
                    node_version.structure_unit_type_id,
                    node_path.path_id AS node_path_id,
                    node_path.path_name AS node_path_name,
                    nodeset.name AS set_name
                FROM bc_chart_profil_x_node
                LEFT JOIN node ON node.id = coalesce(bc_chart_profil_x_node.node_id, bc_chart_profil_x_node.single_node_id)
                LEFT JOIN node_version ON node_version.node_id = node.id AND node_version.version_state_id = 1
                LEFT JOIN node_path ON node_path.id = node.id
                LEFT JOIN nodeset ON bc_chart_profil_x_node.set_id = nodeset.id
            ) chpxn on chp.id = chpxn.bc_chart_profil_id
            LEFT JOIN (
                SELECT
                    bc_chart_profil_x_employee.*,
                    employee.shortcut AS employee_shortcut,
                    employee.first_name AS employee_first_name,
                    employee.last_name AS employee_last_name,
                    employee_group.name AS employee_group_name
                FROM bc_chart_profil_x_employee
                LEFT JOIN employee ON employee.id = bc_chart_profil_x_employee.employee_id
                LEFT JOIN employee_group ON employee_group.id = bc_chart_profil_x_employee.employee_group_id
            ) chpxe
                ON chp.id = chpxe.bc_chart_profil_id
            LEFT JOIN (
                SELECT
                    bc_chart_profil_x_bt.*,
                    coalesce(booking_type.sort_order, 0) AS sort_order
                FROM bc_chart_profil_x_bt
                LEFT JOIN booking_type ON booking_type.id = bc_chart_profil_x_bt.booking_type_id
            ) chpxbt
                ON chp.id = chpxbt.bc_chart_profil_id
            LEFT JOIN (
                SELECT
                    bc_chart_profil_id,
                    bc_additional_col_id,
                    sort_order AS bc_additional_col_sort_order
                FROM bc_chartp_x_additional_col
                INNER JOIN bc_additional_col ON bc_chartp_x_additional_col.bc_additional_col_id = bc_additional_col.id
            ) chpxaddc
                ON chp.id = chpxaddc.bc_chart_profil_id
            LEFT JOIN (
                SELECT
                    bc_chart_profil_id,
                    material_filter_id
                FROM bc_chartp_x_mat_filter
            ) material_filter
                ON chp.id = material_filter.bc_chart_profil_id
            WHERE chp.mitarbeiter_id = :EMPLOYEE_ID
            OR chp.id IN (
                SELECT bc_chart_profil_id
                FROM bc_chartp_x_addemp
                WHERE bc_chartp_x_addemp.mitarbeiter_id = :EMPLOYEE_ID
                
                UNION
                
                SELECT bc_chart_profil_id 
                FROM chpshraedemp 
                WHERE bc_chartp_mitarbeiter_id = :EMPLOYEE_ID
            )
            ORDER BY chp.bezeichnung, chpxbt.sort_order, chpxaddc.bc_additional_col_sort_order asc
        ";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':EMPLOYEE_ID', $this->user->getId());
        $stmt->execute();

        $data = [];
        $idx = -1;

        $profiles = $stmt->fetchAll();

        foreach ($profiles as $row) {
            $separateProfile = ($idx == -1 || ($idx >= 0 && $row['ID'] != $data[$idx]['id']));
            $ownProfile = $row['MITARBEITER_ID'] == $this->user->getId();

            // Skip default profiles of other users
            if (empty($row['BEZEICHNUNG']) && false === $ownProfile) {
                continue;
            }

            if ($separateProfile) {
                $idx += 1;

                $data[] = [
                    'id'                                => $row['ID'],
                    'name'                              => $ownProfile ? $row['BEZEICHNUNG'] : $row['BEZEICHNUNG'] . ' (' . $row['SHARED_WITH'] . ')',
                    'limit'                             => General::toFloat($row['LIMIT']),
                    'act'                               => $row['IST'],
                    'from'                              => $row['ZEITRAUM_VON'],
                    'to'                                => $row['ZEITRAUM_BIS'],
                    'dynamicPeriod'                     => $row['DYNAMIC_PERIOD'],
                    'displayPeriod' => [
                        'from'          => $row['SORT_ZEITRAUM_VON'],
                        'to'            => $row['SORT_ZEITRAUM_BIS'],
                        'dynamicPeriod' => $row['DYNAMIC_PERIOD']
                    ],
                    'startvalue'                        => General::toFloat($row['STARTWERT']),
                    'hoursNote'                         => $row['NOTIZTEXT'],
                    'type'                              => $row['ANZEIGE'],
                    'mail'                              => $row['MAILTYPE'],
                    'reportMail'                        => $row['REPORT_MAIL'],
                    'diagramType'                       => $row['DIAGRAMM_TYP'],
                    'startValue'                        => $row['STARTWERT'],
                    'own'                               => $ownProfile,
                    'limitexceeded'                     => $row['LIMITUEBERSCHREITUNG'] == 'J',
                    'aktiv'                             => $row['AKTIV'] == 'J',
                    'showEmpBookings'                   => $row['EMPLOYEE_BOOKING'],
                    'showCostAndRevenue'                => $row['COST_AND_REVENUE'],
                    'showBTHours'                       => $row['BOOKING_TYPE_HOURS'],
                    'showTargetHours'                   => $row['TARGET_HOURS'],
                    'showBookingsWithBeginEnd'          => $row['BOOKINGS_WITH_BEGIN_END'],
                    'showRemoteWorkDays'                => $row['ONLY_REMOTE_WORK_DAYS'],
                    'showEmployeesWithoutBCPermission'  => $row['EMP_WITHOUT_BC_PERMISSION'],
                    'showEmployeesAsGroups'             => $row['EMPLOYEES_AS_GROUPS'],
                    'showEmployeesWithAbsences'         => $row['EMPLOYEES_WITH_ABSENCES'],
                    'showPerformedOrBookingDate'        => $row['PERFORMED_OR_BOOKING_DATE'],
                    'showWithLockedNodes'               => $row['WITH_LOCKED_NODES'],
                    'ccWithHourBookings'                => $row['CC_WITH_HOUR_BOOKINGS'],
                    'xAxis'                             => $row['X_AXIS'],
                    'divisionId'                        => $row['DIVISION_ID'],
                    'targetHourlyRate'                  => $row['TARGET_HOURLY_RATE'],
                    'showToUsers'                       => [],
                    'shareWithUsers'                    => [],
                    'monitoredUsers'                    => '',
                    'nodes'                             => [],
                    'employees'                         => [],
                    'bookingTypes'                      => [],
                    'additionalCols'                    => [],
                    'materialFilter'                    => [],
                    'showZeroHoursPerWeek'              => $row['ZERO_HOURS_PER_WEEK'],
                    'showOneLinePerEmployee'            => $row['ONE_LINE_PER_EMPLOYEE'],
                    'showOneTabPerCompany'              => $row['ONE_TAB_PER_COMPANY'],
                    'displayFigures'                    => $row['DISPLAY_FIGURES'],
                    'displayLines'                      => $row['DISPLAY_LINES'],
                    'displayOnlySubtrees'               => $row['DISPLAY_ONLY_SUBTREES'],
                    'showClosedOrders'                  => $row['CLOSED_ORDERS'],
                    'showOrdersAfterPeriod'             => $row['ORDER_AFTER_PERIOD'],
                    'showWithoutOrdersAfterPeriod'      => $row['WITHOUT_ORDERS_AFTER_PERIOD'],
                    'showWithHigherLevelPosition'       => $row['WITH_HIGHER_LEVEL_POSITION'],
                    'showExternalEmployees'             => $row['WITH_EXTERNAL_EMPLOYEES'],
                    'contingentDataReportType'          => $row['CONTINGENT_DATA_REPORT_TYPE'],
                    'sharedProfile'                     => $row['SHARED_PROFILE'],
                    'hasRootNodePermission'             => $row['HAS_ROOT_NODE_PERMISSION']
                ];
            }
            $values = &$data[$idx];

            if (($values['own'] || $values['sharedProfile'] == true) && false == empty($row['ADD_EMP_ID'])) {
                if (false === array_search($row['ADD_EMP_ID'], $values['showToUsers'])) {
                    array_push($values['showToUsers'], $row['ADD_EMP_ID']);
                }
            }

            if (($values['own'] || $values['sharedProfile'] == true) && empty($row['SHARE_EMP_ID']) == false) {
                if (false === array_search($row['SHARE_EMP_ID'], $values['shareWithUsers'])) {
                    array_push($values['shareWithUsers'], $row['SHARE_EMP_ID']);
                }
            }

            if (false == (empty($row['SET_ID']) && empty($row['NODE_ID']) && empty($row['SINGLE_NODE_ID']))) {
                $item = ChartProfileNode::createNode($row);
                if (false === array_search($item, $values['nodes'])) {
                    array_push($values['nodes'], $item);
                }
            }

            if (false == (empty($row['EMPLOYEE_ID']) && empty($row['EMPLOYEE_GROUP_ID']))) {
                $item = ChartProfileEmployee::createEmployee($row);
                if (false === array_search($item, $values['employees'])) {
                    array_push($values['employees'], $item);
                }
            }

            if ((false == empty($row['BOOKING_TYPE_ID']) && false === array_search((int)$row['BOOKING_TYPE_ID'], $values['bookingTypes'])) ||
                $row['BOOKING_TYPE_ID'] == 0
            ) {
                $values['bookingTypes'][] = (int)$row['BOOKING_TYPE_ID'];
            }

            if (false == empty($row['BC_ADDITIONAL_COL_ID'])
                && false === array_search((int)$row['BC_ADDITIONAL_COL_ID'], $values['additionalCols'])
            ) {
                $values['additionalCols'][] = (int)$row['BC_ADDITIONAL_COL_ID'];
            }

            if (!empty($row['MATERIAL_FILTER_ID'])) {
                $values['materialFilter'][] = (int)$row['MATERIAL_FILTER_ID'];
            }
        }

        echo Zend_Json::encode(['profiles' => $data]);
    }

    /**
     * Generiert das Diagramm anhand des Objektes aus der Session
     */
    public function imageAction()
    {
        $request = $this->getRequest();

        $cacheId = $request->getParam('profile') . '_' . $request->getParam('timestamp');
        $chart = $this->getCache()->load($cacheId);

        if ($chart instanceof Iso_Diagramm) {
            $width = $request->getParam('width');
            $height = $request->getParam('height');
            if ($width !== null && $height !== null) {
                $chart->setSizeX($width);
                $chart->setSizeY($height);
            }

            $legendPosition = $request->getParam('legendPosition');
            if ($legendPosition !== null) {
                $chart->setLegendPosition($legendPosition);
            }

            header('Content-Type: image/png');
            $chart->generate();
        }
    }

    /**
     * wap, 2009.08.24 iso-7673-i: Sortiert die Listendaten neu
     */
    public function sortlistAction()
    {
        static $colMap = array(
            'company'        => 'COMPANY_NAME',
            'division'        => 'DIVISION_NAME',
            'presenceFrom'    => 'PRESENCE_FROM',
            'presenceTo'    => 'PRESENCE_TO',
            'date'            => 'CDATE',
            'dayType'        => 'DAY_TYPE',
            'employee'        => 'SHORTCUT',
            'employeeName'    => 'LAST_NAME',
            'costcentre'    => 'NAME',
            'hours'            => 'CHOURS',
            'bookingType'    => 'BOOKING_TYPE',
            'bookingBegin'    => 'BOOKING_BEGIN',
            'bookingEnd'    => 'BOOKING_END',
            'note'            => 'CNOTE',
            'pdbIds'        => 'PDBIDS',
            'break'         => 'BREAK'
        );

        $column = $this->getRequest()->getParam('column');
        $direction = $this->getRequest()->getParam('direction');

        if (!isset($colMap[$column])) {
            return;
        }
        $column = $colMap[$column];

        $profileId = $this->getRequest()->getParam('profileId');
        $timestamp = $this->getRequest()->getParam('timestamp');

        $cache = $this->getCache();
        $cacheId = $profileId . '_' . $timestamp;

        if (!$cache->test($cacheId)) {
            return;
        }
        $list_data = $cache->load($cacheId);

        if ($column != "" || !array_key_exists($column, $list_data['DATA'][0])) {
            /**
             * PHP array_multisort
             * Wir haben ein Array von Zeilen, aber array_multisort() benötigt ein Array von Spalten,
             * daher benutzen wir den Code unten, um die Spalten zu bekommen und dann die Sortierung durchzuführen.
             */
            $indizes = array();
            $datum_indizes = array();
            $bkz_indizes = array();
            $ma_indizes = array();
            foreach ($list_data['DATA'] as $key => $row) {
                if ($column == 'CNOTE') {
                    $indizes[$key] = strtolower(trim(html_entity_decode($row[$column])));
                } else {
                    if (!is_array($row[$column])) {
                        $indizes[$key] = strtolower($row[$column]);
                    } else {
                        $indizes[$key] = $row[$column];
                    }
                }
                $datum_indizes[$key] = $row['CDATE'];
                $bkz_indizes[$key] = $row['NAME'];
                $ma_indizes[$key] = $row['SHORTCUT'];
            }

            $order = strtolower($direction) == 'desc' ? SORT_DESC : SORT_ASC;

            //Sortierung je nach Spalte anpassen
            if ($column == 'PDBIDS') {
                //Da die einzelnen PDBIDs in Arrays gespeichert werden, müssen diese erst in
                //	Kommastrings umgewandelt werden, damit die Sortierungen erfolgreich verläuft
                foreach ($indizes as $key => $values) {
                    if (is_array($values)) {
                        $indizes[$key] = implode(',', $values);
                    }
                }
            }

            if ($column == 'CNOTE') {
                array_multisort(
                    $indizes,
                    $order,
                    SORT_STRING,
                    $datum_indizes,
                    SORT_ASC,
                    $ma_indizes,
                    SORT_ASC,
                    SORT_STRING,
                    $bkz_indizes,
                    SORT_ASC,
                    $list_data['DATA']
                );
            } else {
                array_multisort(
                    $indizes,
                    $order,
                    $datum_indizes,
                    SORT_ASC,
                    $ma_indizes,
                    SORT_ASC,
                    SORT_STRING,
                    $bkz_indizes,
                    SORT_ASC,
                    $list_data['DATA']
                );
            }

            $cache->save(array(
                'REPORT_INFO' => $list_data['REPORT_INFO'],
                'DATA' => $list_data['DATA'],
                'ADDITIONAL_COLS' => $list_data['ADDITIONAL_COLS'],
                'order' => $order,
                'sort_column' => $column
            ), $cacheId);
        }
    }

    private function getPaymentPackageInfo($issueKey)
    {
        $issueValues = explode('-', $issueKey);
        $projectName = strtolower($issueValues[0]);
        $issueNumber = $issueValues[1];

        $sql = "SELECT zahlungspaket.bezeichnung,
                zahlungspaketstatus.status_kuerzel
                FROM zahlungspaket
                LEFT JOIN zahlungspaketstatus 
                ON zahlungspaket.zahlungspaketstatus_id = zahlungspaketstatus.id
                INNER JOIN jira_issue_id issue 
                ON zahlungspaket.issue_id = issue.id
                INNER JOIN jira_project_name jira_project 
                ON issue.project_id = jira_project.id
                WHERE zahlungspaket.next_id IS NULL
                AND LOWER(jira_project.name) = :PROJECT_NAME
                AND issue.issue_number = :ISSUE_NUMBER";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':PROJECT_NAME', $projectName);
        $stmt->bindValue(':ISSUE_NUMBER', $issueNumber);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        $paymentPackageInfo = '-';

        if ($rows) {
            $paymentPackageInfo = $rows;
        }

        return $paymentPackageInfo;
    }

    /**
     * ten, 2009.06.09 iso-7465-i:
     * die Infos fuer eine PDB-Meldung ermitteln und an die View zurueckliefern
     */
    public function pdbinfoAction()
    {
        /* MeldungsID auslesen, für das Element, für das die Daten geladen werden sollen */
        $issueKey = $this->getRequest()->getParam('jiraid');
        $jiraDataRetriever = new Jira_FetchJiraIssue($this->config, $this->log);
        $jiraIssue = $jiraDataRetriever->fetch($issueKey);
        $issue = null;

        if ($jiraIssue !== null) {
            $issue = $jiraIssue->getIssue();
        }

        if ($issue !== null) {
            $infos = [];
            $infos['ISSUEKEY'] = $issueKey;
            $infos['TITLE'] = $issue->getSummary();
            $infos['STATE'] = $issue->getStatus();
            $infos['CLASSIFICATION'] = $issue->getClassification();
            $infos['PAYMENTPACKAGES'] = $this->getPaymentPackageInfo($issueKey);
            $infos['PROJECT'] = $issue->getProjectName();

            $this->initView();
            $this->view->locale = new Zend_Locale($this->user->getSprache());
            $this->view->data = $infos;
            echo $this->render('jirainfo');
        } else {
            /* Wenn MeldungsID nicht gefunden wurde, dann Fehlertext liefern */
            echo $this->translate->translate('keineinfosverfuegbar');
        }
    }

    /**
     * Calculates the Act hours for the given chart profile.
     *
     * @param int $profileId
     */
    public function calculateacthoursAction()
    {
        $profileid = $this->getRequest()->getParam('profileId');
        ChartProfil::calculateChartProfile($profileid, $this->db);

        echo(1);
    }

    /**
     * Calculates a the act value for the given profile
     */
    public function calcactvalAction()
    {
        $profileId = $this->getRequest()->getParam('profileId');

        $statement = $this->db->prepare('CALL PACK_BKZCOCKPIT.calculateChartProfile(:PROFILE_ID)');
        $statement->bindValue(':PROFILE_ID', $profileId);
        $statement->execute();

        echo true;
    }

    /**
     * Determine the chart profile's period
     */
    public function determineperiodAction()
    {
        $profileId = $this->getRequest()->getParam('profileId');

        $dateFrom = null;
        $dateUntil = null;

        Date::determinePeriod($profileId, $dateFrom, $dateUntil, $this->db);

        if ($dateFrom !== null && $dateUntil !== null) {
            echo $dateFrom->get(Zend_Date::TIMESTAMP) . "-" . $dateUntil->get(Zend_Date::TIMESTAMP);
        } else {
            echo 'false';
        }
    }

    /**
     * Determine the chart profile's period
     * @throws Zend_Date_Exception
     */
    public function determineperiodfromvaluesAction()
    {
        $from = substr($this->getRequest()->getParam('from'), 0, 10);
        $until = substr($this->getRequest()->getParam('until'), 0, 10);
        $dynamicPeriod = $this->getRequest()->getParam('dynamicPeriod');
        $profileType = $this->getRequest()->getParam('profileType');

        $sql = "
            CALL
                PACK_BKZCOCKPIT.determinePeriod(
                    to_date(:PERIOD_FROM, 'YYYY-MM-DD'),
                    to_date(:PERIOD_UNTIL, 'YYYY-MM-DD'),
                    :DYNAMIC_PERIOD,
                    :PROFILE_TYPE,
                    null,
                    null
                )
        ";

        $statement = $this->db->prepare($sql);
        $statement->bindValue(':PERIOD_FROM', $from);
        $statement->bindValue(':PERIOD_UNTIL', $until);
        $statement->bindValue(':PROFILE_TYPE', $profileType, PDO::PARAM_STR, 1);
        $statement->bindValue(':DYNAMIC_PERIOD', $dynamicPeriod);
        $statement->execute();
        $result = $statement->fetch();

        $dateFrom = new Zend_Date($result['P_FROM'], 'yyyy-MM-dd');
        $dateUntil = new Zend_Date($result['P_UNTIL'], 'yyyy-MM-dd');

        echo $dateFrom->get(Zend_Date::TIMESTAMP) . "-" . $dateUntil->get(Zend_Date::TIMESTAMP);
    }

    /**
     * Admin action to switch user.
     *
     */
    public function switchtouserAction()
    {
        $user = $this->getRequest()->getParam('user');
        if ($user && $this->user->hasAdminRights()) {
            $authAdapter = new Iso_Auth_Adapter_Normal($user);
            $auth = Zend_Auth::getInstance()->authenticate($authAdapter);

            $userObj = new Iso_User($this->config->company->ownId, $auth->getIdentity());
            $this->session->prevUser = $this->session->user;
            $this->session->user = $userObj;
            $this->session->checksum = md5((string)time() . $user);
        }
        $this->redirect('/index', array('prependBase' => true));
    }

    /**
     * Admin action to switchback to original user.
     *
     */
    public function switchbackAction()
    {
        $prevUser = $this->session->prevUser;

        if ($prevUser) {
            unset($this->session->prevUser);
            unset($this->session->checksum);
            $this->session->user = $prevUser;
        }
        $this->redirect('/index', array('prependBase' => true));
    }

    /**
     * Returns the id of the default profile of the user. If no default profile
     * exists yet, it is created
     *
     * @return int|false
     */
    protected function loadDefaultProfile()
    {
        # check against empty string as this is the default value for the userId...
        if ($this->user->getId() === '') {
            return false;
        }
        $chartprofil = new ChartProfil();
        $row = $chartprofil->fetchRow(array(
            'bezeichnung IS null',
            $chartprofil->getAdapter()->quoteInto('MITARBEITER_ID = ?', (int)$this->user->getId())
        ));

        if ($row) {
            $id = $row->ID;
        } else {
            // create new default profile
            $data = array(
                'MITARBEITER_ID' => (int)$this->user->getId(),
                'BEZEICHNUNG'    => null,
                'DIAGRAMM_TYP'   => 'TE',
                'ZEITRAUM_VON'   => new Zend_Db_Expr('current_date')
            );

            $id = $chartprofil->insert($data);
        }
        $this->session->defaultprofil = $id;
        return $id;
    }

    /**
     * @return Zend_Cache_Core
     */
    protected function getCache()
    {
        return $this->getInvokeArg('bootstrap')->getResource('cache')->getCache('report');
    }

    /**
     * @return array
     * @throws Zend_Db_Statement_Exception
     */
    protected function getInitialData()
    {
        $user = $this->user;
        $userShortcut = $user->getKuerzel();

        $prevUser = $this->session->prevUser ? $this->session->prevUser->getKuerzel() : "";
        $checksum = $this->session->checksum ? $this->session->checksum : "";
        $employee = EmployeeMapper::pdbEmployee2PrismaEmployee($userShortcut);
        $nodeObj = new Node($this->translate);
        $nodeTypeObj = new NodeType();
        $structureUnitTypeObj = new StructureUnitType();
        $bookingTypeObj = new BookingType();
        $divisionObj = new Division();
        $additionalColsObj = new AdditionalCol();
        $mitarbeiterObj = new Mitarbeiter();
        $employeeObj = new Employee();

        $singleNodeTypes = [];
        foreach (SingleNodeType::getAll() as $key => $val) {
            $singleNodeTypes[] = ['NAME' => $key, 'VALUE' => $val];
        }

        $displayFigures = [];
        foreach (DisplayFigure::getAll() as $key => $val) {
            $displayFigures[] = ['NAME' => $key, 'VALUE' => $val];
        }

        $lockedNodes = [];
        foreach (LockedNodes::getAll() as $key => $val) {
            $lockedNodes[] = ['NAME' => $key, 'VALUE' => $val];
        }

        $displayLines = [];
        foreach (DisplayLines::getAll() as $key => $val) {
            $displayLines[] = ['NAME' => $key, 'VALUE' => $val];
        }

        return [
            'user' => strtoupper($userShortcut),
            'prevUser' => [
                'name' => $prevUser,
                'checksum' => $checksum
            ],
            'privileges' => [
                'adminRights' => $user->hasAdminRights(),
                'costAndRevenue' => $user->isAllowedToSeeCostAndRevenue(),
                'monthEnd' => $user->isAllowedToMonthEnd(),
                'accounting' => $user->isAllowedToSeeAccounting(),
                'rebook' => $user->isAllowedToRebook(),
                'rebookAllBookings' => $user->isAllowedToRebookAllBookings(),
                'rebookLog' => $user->isAllowedToSeeRebookLog(),
                'permission' => $user->isAllowedToSeePermission(),
                'sapKeyHours' => $user->isAllowedToSeeSAPKeyHours(),
                'standby' => $user->isAllowedToStandby(),
                'absenceCalendar' => $user->isAllowedToAbsenceCalendar(),
                'misDivisionOverall' => $user->isAllowedToMisDivisionOverall(),
                'projectStatus' => $user->isAllowedToProjectStatus(),
                'accountableHours' => $user->isAllowedToAccountableHours(),
                'orderOverview' => $user->isAllowedToOrderOverview(),
                'contingentRange' => $user->isAllowedToContingentRange(),
                'contingentData' => $user->isAllowedToContingentData(),
                'contingentSapInvoiceComparison' => $user->isAllowedToContingentSapInvoiceComparison(),
                'prismaValues' => $user->isAllowedToPrismaValues()
            ],
            'tree' => [
                'nodes' => Tree::getTree(true, $employee, $this->translate),
                'employees' => Tree::getTree(false, $employee, $this->translate)
            ],
            'costcentres' => [
                'all' => $nodeObj->getCostcentres(),
                'user' => $nodeObj->getEmployeeCostcentres($employee)
            ],
            'nodeTypes' => $nodeTypeObj->getNodeTypes($this->languageType),
            'structureUnitTypes' => $structureUnitTypeObj->getStructureUnitTypes($this->languageType),
            'bookingTypes' => $bookingTypeObj->getBookingTypes($this->languageType),
            'singleNodeTypes' => $singleNodeTypes,
            'divisions' => $divisionObj->getDivisions(),
            'additionalCols' => $additionalColsObj->getAdditionalCols($this->languageType),
            'bcUsers' => $mitarbeiterObj->getAllBCUsers($this->user->getId(), $this->config->company->ownId),
            'employees' => $employeeObj->getAllEmployees(),
            'bcPrivilegeMail' => $this->config->environment->privilege_mail,
            'bookingTreeMail' => $this->config->environment->bookingtree_mail,
            'jiraUrl' => $this->config->jira->base->url,
            'displayFigures' => $displayFigures,
            'lockedNodes' => $lockedNodes,
            'displayLines' => $displayLines,
            'accountingPath' => $this->config->main->accounting->samba_path,
            'ableToShareEmployees' => $mitarbeiterObj->getAllAbleToShareBcUsers($this->user->getId(), $this->config->company->ownId)
        ];
    }

    /**
     * Temporary action to test mail functionality
     *
     * If you do not know what this is, remove it:-)
     *
     * @throws Zend_Exception
     * @throws Zend_Mail_Exception
     */
    public function sendtestmailAction()
    {
        $recipient = 'david.hoelzel@iso-gruppe.com';
        $subject = 'BKZ Cockpit Test Mail';
        $body = 'Test Content';
        if (PHP_SAPI !== 'cli') {
            throw new RuntimeException('Calling only from CLI allowed');
        }
        if (!$recipient) {
            throw new RuntimeException('No Recipient given');
        }
        $mail = new Iso_Mail('utf-8');
        $mail->addTo($recipient)
            ->setSubject($subject)
            ->setBodyText($body);

        try {
            $mail->send();
        }
        catch (Exception $e) {
            echo $e;
        }

    }
}
