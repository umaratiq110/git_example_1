<?php
class LoginController extends Iso_Controller_DefaultController
{
    /*
     *  $languageFile
     *  Legt fest, welches Language-Einträge geholt werden sollen
     */
    protected $languageFile = 'BC_login.php';
    
    private $release;

    public function indexAction()
    {
        $this->doLogin();
    }

    /**
     * Löscht die Session Daten des Users und leitet ihn durch die EXT JS wieder zur Login Seite zurück
     * wap, iso-7457-i: Nach dem Logout, soll ein Hinweis erscheinen.
     */
    public function logoutAction()
    {
        $this->initView();
      
        // view parameters
        $this->view->prodkenn         = $this->release;
        $this->view->toolteammail     = $this->config->environment->mail;

        // destroy session
        Zend_Session::destroy();
  
        $this->render();
    }

    public function languageAction()
    {
        $language = $this->getRequest()->getParam('language');

        if (!is_null($this->auth) && $this->auth->isValid() && !is_null($this->user)) {
            $this->user->setSprache($language);
            $this->redirect('/index');
        } else {
            $this->doLogin($language);
        }
    }

    public function norouteAction()
    {
        $this->indexAction();
    }
    
    public function errorAction($error = 'UndefinedError')
    {
        $this->session->auth = null;
        $this->session->user = null;

        $this->initView();

        $this->view->errorLanguageEntry = strtolower($error);
        $this->view->toolteam_mail = '<a href="mailto:' . $this->config->environment->mail . '">Webteam</a>';

        // Email versenden
        $mail = new Iso_Mail('utf-8');
        $mail->addTo($this->config->environment->mail);
        $mail->setSubject('Fehler im BKZCockpit (' . APPLICATION_ENV . ')');

        $text  = 'Es trat ein Problem beim Zugriff eines Users auf BKZCockpit auf: ' . "\r\n";
        $text .= 'IP-Adresse: ' . $_SERVER['REMOTE_ADDR'] . "\r\n";
        $text .= 'User: ' . $_SERVER['REMOTE_USER'] . "\r\n";
        $text .= 'Datum: ' . new Zend_Date() . "\r\n";
        $text .= "\r\n";
        $text .= 'Fehler: ' . $error . "\r\n";
        $text .= 'Fehler: ' . $this->view->translate(strtolower($error) . '_subject');

        $mail->setBodyText($text);

        try {
            // E-Mail nur noch bei undefinierten Fehlern versenden
            if ($error == 'UndefinedError') {
                $mail->send();
            }
            $this->log->err($text);
        } catch (Zend_Mail_Exception $zme) {
            $this->log->err($text);
            $this->log->err($zme->toString());
        }

        $this->render('error');
    }

    /**
     * @param null $language
     * @return void|null
     * @throws Exception
     */
    protected function doLogin($language = null)
    {
        # true is not converted to a boolean in Zend...
        if ($this->config->auth->http->enable === '1') {
            $authResult = $this->handleHttpLogin();
            if (!$authResult instanceof Zend_Auth_Result) {
                return null;
            }
        }
        elseif ($this->config->auth->ldap->enable === '1') {
            $this->redirect('/login/form');
            return null;
        }
        else {
            return $this->errorAction();
        }

        $user = new Iso_User($this->config->company->ownId, Zend_Auth::getInstance()->getIdentity());

        $this->postLogin($user, $authResult, $language);

        return null;
    }

    /**
     * @return void|null
     */
    public function formAction()
    {
        if ($this->config->auth->ldap->enable !== '1') {
            # form login not enabled
            $this->redirect('/index');
            # return null to make clear that this function is done (redirect implicitly exists...)
            return null;
        }
        $errors = [];
        if ($this->getRequest()->isPost()) {
            $ldapLogin = new Authentication_LdapLogin();
            try {
                $result = $ldapLogin->authenticate($this->getRequest()->getParam('username'), $this->getRequest()->getParam('password'));
                $user = new Iso_User($this->config->company->ownId, Zend_Auth::getInstance()->getIdentity());
                $this->checkUser($user);
                $this->postLogin($user, $result);
            }
            catch (Authentication_Exception_AuthenticationFailedException $e) {
                $this->log->warn($e);
                $errors[] = 'passwortoderuserfalsch';
            }
            catch (Exception $e) {
                return $this->errorAction();
            }
        }
        $this->initView();
        $this->view->loginErrors = $errors;
        $this->render('login-form');
        return null;
    }

    /**
     * @param Zend_Auth_Result $result
     * @throws Zend_Session_Exception
     */
    private function saveAuthenticationToSession(Zend_Auth_Result $result)
    {
        $this->session->auth = $result;

        // session lifetime
        $this->session->setExpirationSeconds($this->config->resources->session->remember_me_seconds);
    }

    /**
     * Dirty check function - code is aborted in errorAction, so we do not return anything
     * @param Iso_User $user
     */
    private function checkUser(Iso_User $user)
    {
        if (!$user->isExistant()) {
            $this->errorAction('UserDoesNotExist');
        }

        if (!$user->isActive()) {
            $this->errorAction('UserIsInactive');
        }

        if (!$user->isAllowedToUseBKZCockpit()) {
            $this->errorAction('UserIsNotAllowed');
        }
    }

    /**
     * @param Iso_User $user
     * @param Zend_Auth_Result $authResult
     * @param string|null $language
     * @throws Exception
     */
    private function postLogin(Iso_User $user, Zend_Auth_Result $authResult, string $language = null)
    {
        if (!$authResult->isValid()) {
            throw new Exception(sprintf('auth result not valid, cannot do post login actions'));
        }
        // overwrites auth object temporarily with a auth result object (probably by mistake, who knows...)
        $this->auth = $authResult;

        if ($language !== null) {
            $user->setSprache($language);
        }

        $user->logLogin();

        $this->session->user = $user;

        $this->saveAuthenticationToSession($authResult);

        $this->redirect('/index');
    }

    /**
     * @return void|Zend_Auth_Result|null
     */
    private function handleHttpLogin()
    {
        # try http header login
        try {
            $httpLogin = new Authentication_HttpLogin();
            return $httpLogin->authenticate();
        }
        catch (Authentication_Exception_AuthenticationFailedException $e) {
            # normally impossible to reach, but let´s handle it
            return $this->errorAction('UserIsNotAuthenticated');
        }
        catch (Authentication_Exception_UsernameMissingException $e) {
            if ($this->config->auth->ldap->enable === '1') {
                # redirect to login form
                $this->redirect('/login/form');
                return null;
            }
            return $this->errorAction();
        }
    }
}
