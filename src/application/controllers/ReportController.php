<?php
/*
 *	ReportController
 */
class ReportController extends Iso_Controller_DefaultController
{
    protected $languageFile = 'BC_report.php';
    
    /**
     * Rebooking action
     */
    public function rebookAction()
    {
        $profile = $this->getRequest()->getParam('profile');
        $bookings = Zend_Json::decode($this->getRequest()->getParam('bookings'));
        $targetCC = $this->getRequest()->getParam('targetCC');
        $requestor = $this->getRequest()->getParam('requestor');
        $reason = $this->getRequest()->getParam('reason');
        $total = $this->getRequest()->getParam('total');
        
        $wh = new TPWWorkingHours($this->user->isAllowedToRebookAllBookings());
        $result = $wh->rebook($bookings, $targetCC);
        
        if (is_array($result)) {
            $ra = new RebookingAction();
            $result = $ra->log(
                $profile,
                $requestor,
                EmployeeMapper::pdbEmployee2PrismaEmployee((int)$this->user->getId()),
                $reason,
                $targetCC,
                $total,
                $result
            );
        }
        
        echo json_encode(array('result' => $result));
    }

    public function sendrebookrequestmailAction()
    {
        $subject = $this->getRequest()->getParam('subject');
        $message = $this->getRequest()->getParam('message');
        $targetCC = $this->getRequest()->getParam('targetCC');
        $message  = "Ziel-BKZ: ".$targetCC ."\n\n". $message;
        $userMail = $this->user->getUserMail();

        $success = true;
        try {
            $mail = new Iso_Mail('utf-8');
            $mail->setFrom($userMail)
                ->addTo($this->config->environment->bookingtree_mail)
                ->addCc($userMail)
                ->setSubject($subject)
                ->setBodyText($message)
                ->send();
        } catch (Exception $e) {
            $success = false;
        }

        echo json_encode(array('success' => $success));
    }

    public function cleanaccountingfolderAction()
    {

        $dir = $this->config->main->accounting->save_path;
        $dirDate = "";

        $date = new Zend_Date();
        $date->set('1', Zend_Date::DAY);
        $date->sub('1', Zend_Date::MONTH);
        $date->sub('1', Zend_Date::DAY);
        $dayBeforeLastMonth = $date->toString('yyyy-MM-dd');

        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($iterator as $path) {
            if (!$path->isDir()) {
                $dirDate = preg_split("/[0-9]{4}-[0-9]{2}-[0-9]{2}-/", $path->getPath());
            }

            if (isset($dirDate[1]) && $dirDate[1] < $dayBeforeLastMonth) {
                $path->isDir() && !$path->isLink() ? rmdir($path->getPathname()) : unlink($path->getPathname());
            }
        }
    }
}
