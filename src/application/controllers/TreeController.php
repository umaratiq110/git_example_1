<?php
/*
 *	TreeController
 */
class TreeController extends Iso_Controller_DefaultController
{
    protected $languageFile = 'BC_index.php';

    /**
     * @var Node
     */
    protected $nodeModel;
    
    /**
     * Controller initialization
     */
    public function init()
    {
        parent::init();
        
        $this->nodeModel = new Node($this->translate);
    }
    
    /**
     * Get the children of a node
     */
    public function getnodeAction()
    {
        $node = $this->getRequest()->getParam('node');
        if (null === $node) {
            throw new Exception("Parameter 'node' must be set");
        }
        
        $type = substr($node, 0, 1);
        $nodeId = substr($node, !is_numeric($type) ? 1 : 0);
        
        $employeeId = EmployeeMapper::pdbEmployee2PrismaEmployee($this->user->getKuerzel());
        $result = $this->nodeModel->getTree($employeeId, $nodeId);
        
        $this->_helper->json($result);
    }
    
    /**
     * Get the subtree between 'start' and 'end' node, or the direct children
     * if 'end' is not specified
     */
    public function gettreeAction()
    {
        $startId = $this->getRequest()->getParam('start');
        if (!isset($startId)) {
            $startId = 1;
        }
        if (!is_numeric($startId)) {
            throw new Exception("Parameter 'start' must be numeric");
        }
        $endId = $this->getRequest()->getParam('end');
        if (null !== $endId && !is_numeric($endId)) {
            throw new Exception("Parameter 'end' must be numeric");
        }
        if ($startId == $endId) {
            $endId = null;
        }
        
        $employeeId = EmployeeMapper::pdbEmployee2PrismaEmployee($this->user->getKuerzel());
        $result = $this->nodeModel->getTree($employeeId, $startId, $endId);
        
        $this->_helper->json($result);
    }
    
    /**
     * Find nodes according to a search phrase
     */
    public function searchAction()
    {
        $criteria = $this->getRequest()->getParam('criteria');
        $searchPhrase = $this->getRequest()->getParam('phrase');
        
        $employeeId = EmployeeMapper::pdbEmployee2PrismaEmployee($this->user->getKuerzel());
        $result = $this->nodeModel->search($employeeId, $criteria, $searchPhrase);
        
        $this->_helper->json($result);
    }
    
    /**
     * Returns the path back to the root node for the give node
     *
     * @param int $nodeId
     */
    public function getpathAction()
    {
        $nodeId = $this->getRequest()->getParam('nodeId');
        
        $result = $this->nodeModel->getPath($nodeId);
        
        $this->_helper->json($result);
    }
}
