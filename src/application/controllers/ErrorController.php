<?php
/**
 * Controller for handling exceptions in a tasteful way - yummy
 */
class ErrorController extends Iso_Controller_DefaultController
{
    /**
     * @var string
     */
    protected $languageFile = 'BC_error.php';

    /**
     * @throws Zend_Controller_Response_Exception
     * @throws Zend_Exception
     */
    public function errorAction()
    {
        $this->initView();

        $log = $this->log;

        $errorHandler = $this->_getParam('error_handler');
        $exception = $errorHandler->exception;

        // prepare log message
        $logMsg = $exception->getMessage() . " in " .  $exception->getFile() . " line " . $exception->getLine();

        switch ($errorHandler->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                $log->warn($logMsg);
                $httpResponseCode = 404;
                break;
            default:
                $log->err($logMsg . PHP_EOL . $exception->getTraceAsString());
                $this->sendErrorMail($exception);
                $httpResponseCode = 500;
        }

        if (PHP_SAPI === 'cli') {
            echo $exception;
            exit($exception->getCode());
        }

        $this->getResponse()->setHttpResponseCode($httpResponseCode);

        if ($this->_request->isXmlHttpRequest()) {
            $error = array(
                'success' => false,
                'exception' => array(
                    'message' => $exception->getMessage(),
                    'code' => $exception->getCode(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'trace' => $exception->getTraceAsString()
                )
            );

            $this->_helper->json($error);
        } else {
            $this->view->errorHandler = $errorHandler;
            $this->render((string)$httpResponseCode);
        }
    }

    /**
     * Sends an error email
     *
     * @param Exception $exception
     */
    protected function sendErrorMail(Exception $exception)
    {
        $frontController = $this->getFrontController();
        $request = $this->getRequest();

        $view = new Zend_View();
        $view->setBasePath($frontController->getModuleDirectory() . '/views');

        $view->user = $this->session->user instanceof Iso_User ? $this->session->user->getKuerzel() : '---';
        $view->date = date('d.m.Y H:i:s');
        $view->module = $request->getParam('module');
        $view->controller = $request->getParam('controller');
        $view->action = $request->getParam('action');
        $view->exception = $exception;

        $body = $view->render('error/mail.ptxt');

        $mail = new Iso_Mail('utf-8');
        $mail->addTo($this->config->environment->mail)
             ->setSubject('Uncaught error in the BKZ-Cockpit')
             ->setBodyText($body)
             ->send();
    }
}
