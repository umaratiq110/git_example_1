<?php

class ConfigController extends Iso_Controller_DefaultController
{
    /**
     * returns all available material filters with translated name
     */
    public function getmaterialfilterAction()
    {
        $materialFilter = new MaterialFilter();
        $this->_helper->json($materialFilter->getMaterialFilter($this->languageType));
    }
}