<?php
class ExportController extends Iso_Controller_DefaultController
{
    public $filename_suffix = '';
    public $filter_text = '';
    public $filter_column = '';

    /**
     * @var Zend_Cache_Core
     */
    protected $cache;

    /**
     * @var string
     *
     * Legt fest, welches Language-Eintraege geholt werden sollen
     */
    protected $languageFile = 'BC_Export.php';

    /**
     * Init controller
     */
    public function init()
    {
        parent::init();

        $this->cache = $this->getInvokeArg('bootstrap')->getResource('cache')->getCache('report');
    }

    /**
     * Sends reports as mail attachments
     */
    public function reportmailAction()
    {
        $userShortCut = $this->getRequest()->getParam('shortcut');

        $reportMail = new ReportMail($this);
        $reportMail->send($userShortCut);
    }

    /**
     * Generate report
     */
    public function generateAction()
    {
        $type = $this->getRequest()->getParam('type');
        $timestamp = $this->getRequest()->getParam('timestamp');
        $cacheId = $this->getRequest()->getParam('profile') . '_' . $timestamp;
        $data = $this->cache->load($cacheId);

        $this->initView();
        $this->view->data = $data;
        $this->view->user = $this->user->getKuerzel();

        $name = $type . $this->filename_suffix . '_' . $timestamp;

        // standard escape and extension is for excel exports
        $this->view->setEscape(array('Iso_Escaping', 'excel'));
        $fileExt = '.xlsx';
        $exportParams = array();

        switch ($type) {
            case "status":
                $exportFn = 'exportStatus';
                break;
            case "standby":
                $exportFn = 'exportStandby';
                break;
            case "sapKeyHours":
                $exportFn = 'exportSAPKeyHours';
                break;
            case "permission":
                $sort = explode('_', $this->getRequest()->getParam('sort'));
                $this->view->sort = $sort;

                // data with the filtered
                if ($data['DATA'] != null) {
                    $this->view->data['DATA'] = $this->filterData($data['DATA']);
                }

                $exportFn = 'exportPermission';
                break;
            case 'list':
                $this->view->data['DATA'] = $this->filterData($data['DATA']);
                $this->view->sort_column = $data['sort_column'];
                $exportFn = 'exportList';
                break;
            case 'pdbinfosliste':
                $this->view->data['DATA'] = $this->pdbinfos($this->filterData($data['DATA']));
                $exportFn = 'exportPdbInfos';
                break;
            case 'bookingsWith':
                $exportFn = 'exportBookings';
                $exportParams[] = true;
                break;
            case 'bookingsWithout':
                $exportFn = 'exportBookings';
                $exportParams[] = false;
                break;
            case 'monthEnd':
                $exportFn = 'exportMonthEnd';
                break;
            case 'btModifications':
                $exportFn = 'exportBtModifications';
                break;
            case 'absenceCalendar':
                $exportFn = 'exportAbsenceCalendar';
                break;
            case 'table':
                $this->view->alhours = $this->getRequest()->getParam('alhours');
                $this->view->percent = $this->getRequest()->getParam('percent');
                $this->view->targetHours = $this->getRequest()->getParam('targetHours');
                $this->view->sapKey = $this->getRequest()->getParam('sapKey');

                $exportFn = 'exportTable';
                break;
            case 'orderOverview':
                $exportFn = 'exportOrderOverview';
                break;
            default:
                throw new Exception("Unknown report type: " . $type);
        }

        if (isset($exportFn)) {
            $exportObj = new Export($this->view, $this->translate, $this->config->main->export->save_path, $type);
            $content = call_user_func_array(array($exportObj, $exportFn), $exportParams);
        } elseif (isset($exportView)) {
            $content = $this->view->render($exportView);
        }

        $name .= $fileExt;
        $path = $this->config->main->download->save_path . $name;
        file_put_contents($path, $content);

        echo $name;
    }

    /**
     * Download report
     */
    public function downloadAction()
    {
        $name = $this->getRequest()->getParam('name');

        $path = $this->config->main->download->save_path . $name;
        if (!file_exists($path)) {
            echo false;
            exit;
        }

        $report = file_get_contents($path);
        unlink($path);

        $response = $this->getResponse();
        $parts = explode('.', $name);
        $ext = end($parts);

        switch ($ext) {
            case 'csv':
                $response->setHeader('Content-Type', 'text/csv');
                break;
            case 'xls':
                $response->setHeader('Content-Type', 'application/vnd.ms-excel');
                break;
            case 'xlsx':
                $response->setHeader(
                    'Content-Type',
                    'application/vnd.openXMLformats-officedocument.spreadsheetml.sheet'
                );
                break;
            default:
                throw new Exception("Unknown format in ExportController:" . $ext);
        }

        $response->setHeader('Content-Disposition', 'attachment; filename=' . $name . '');

        echo $report;
    }

    /**
     * Filters data according to search criteria
     */
    protected function filterData($data)
    {
        //Array in das die Zeilen mit übereinstimmungen geschrieben werden
        $new_data = array();

        $orig_filter_text = $filter_text = trim($this->getRequest()->getParam('text'), '');

        if ($this->getRequest()->getParam('column')) {
            $filter_column = $this->getRequest()->getParam('column');
        } else {
            $filter_column = 'alle';
            $withoutNote = true;
        }
        //Wenn Filtertext leer ist Daten unverändert zurückgeben
        if ($filter_text == '') {
            return $data;
        }

        //Zu untersuchende Indizes
        $filter_keys = array();

        //Escaping
        $filter_text = preg_quote($filter_text);
        $filter_text = preg_replace('/\//', '\/', $filter_text);
        $filter_text = preg_replace('/\&/', '\&', $filter_text);
        $filter_text = preg_replace('/\§/', '\§', $filter_text);
        $regex  = '/' . $filter_text . '/i';

        switch ($filter_column) {
            case 'alle':
                if (is_array($data) && !empty($data)) {
                    $filter_keys = array_keys($data[0]);
                    if (!isset($withoutNote)) {
                        //Es werden nicht die echten Indizes der Notiz verwendet,
                        //da die Notiz so zusammengestellt werden muss wie in der Liste
                        $filter_keys[] = 'notiz';
                    }
                } else {
                    // return unfiltered data
                    return $data;
                }
                break;
            case 'companyName':
                $filter_keys[] = 'COMPANY_NAME';
                break;
            case 'divisionName':
                $filter_keys[] = 'DIVISION_NAME';
                break;
            case 'costcentre':
                $filter_keys[] = 'NAME';
                break;
            case 'employee':
                $filter_keys[] = 'SHORTCUT';
                break;
            case 'note':
                //Es werden nicht die echten Indizes der Notiz verwendet,
                // da die Notiz so zusammengestellt werden muss wie in der Liste
                $filter_keys[] = 'notiz';
                break;
            case 'bookingType':
                $filter_keys[] = 'BOOKING_TYPE';
                break;
        }

        //Alle zeilen durchgehen
        $count_rows = count($data);
        for ($i = 0; $i < $count_rows; $i++) {
            //Indizes werden ermittelt

            //Alle Werte prüfen
            $keys_count = count($filter_keys);
            for ($j = 0; $j < $keys_count; $j++) {
                /*
                 * Note muss extra geprüft und zusammengestellt werden
                 */
                if ($filter_keys[$j] == 'notiz') {
                    $note = '';
                    $note = $data[$i]['CNOTE'];
                    $value = $note;
                } else {
                    if (isset($data[$i][$filter_keys[$j]])) {
                        $value = $data[$i][$filter_keys[$j]];
                    }
                }

                if (!is_array($value)) {
                    $reg_erg = preg_match($regex, html_entity_decode($value));
                } else {
                    $reg_erg = false;
                }
                //Wert prüfen
                if ($reg_erg) {
                    //Wenn Suchtext gefunden in Daten in neues Array schreiben und aus der Schleife gehen
                    $new_data[] =  $data[$i];
                    break;
                }
            }
        }

        $this->filter_text   = $orig_filter_text;

        if ($filter_column == 'bkz') {
            $this->filter_column = 'abkbkz';
        } else {
            $this->filter_column = $filter_column;
        }

        return $new_data;
    }

    /**
     * Ermitteln der PDB-Infos pro Meldung einmal
     */
    private function pdbinfos($rows)
    {
        $fetchedPdbs = null;

        for ($loop = 0; $loop < count($rows); $loop++) {
            $pdbids = $rows[$loop]['PDBIDS'];

            for ($loop2 = 0; $loop2 < count($pdbids); $loop2++) {
                /*
                 * Logik nur dann ausführen, wenn Daten für Meldung noch nicht geladen wurden
                 */
                if (!is_array($fetchedPdbs) ||
                    (is_array($fetchedPdbs) && count($fetchedPdbs) == 0) ||
                    (is_array($fetchedPdbs) && !array_key_exists(strToLower($pdbids[$loop2]), $fetchedPdbs))
                ) {
                    $pdb = new PDBEntry(strToLower($pdbids[$loop2]));
                    if ($pdb->hasInfos()) {
                        $fetchedPdbs[strToLower($pdbids[$loop2])] = $pdb->getInfos($this->user->getSprache());
                    }
                    $pdb = null;
                }
            }
        }

        return $fetchedPdbs;
    }

    /**
     * Daten für den Header der View laden und bereitstellen
     */
    private function setHeader($permissionReport = false)
    {
        $chartprofil = new ChartProfil();

        if ($permissionReport == false) {
            $chartprofil_data = $chartprofil->fetchRow(
                array(
                    "BEZEICHNUNG IS NULL",
                    "MITARBEITER_ID = '" . $this->user->getId() . "'"
                )
            );
        } else {
            $chartprofil_data = (object) array("ID" => $this->user->getId(), "NOTIZTEXT" => null, "ANZEIGE" => 'P');
        }
        Report::setExportHeaderData($this, $chartprofil_data, $this->user);
    }
}
