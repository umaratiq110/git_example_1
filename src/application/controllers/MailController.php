<?php
declare(strict_types=1);

class MailController extends Iso_Controller_DefaultController
{
    /**
     * @throws Zend_Db_Statement_Exception
     */
    public function sendlimitandhoursmailAction()
    {
        $service = new LimitAndHoursMail();
        $service->sendMails();
    }
}
