<?php
class InfoseiteController extends Iso_Controller_DefaultController
{
    /*
     *  $languageFile
     *  Legt fest, welches Language-Einträge geholt werden sollen
     */
    protected $languageFile = 'BC_Infoseite.php';

    public function indexAction()
    {
        $this->redirect('/index', array('prependBase' => true));
    }

    public function informationAction()
    {
        $this->initView();
        $this->render('information');
    }
}
