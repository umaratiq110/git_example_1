<?php
declare(strict_types=1);

/**
 * Class Excel
 */
abstract class Excel
{
    /**
     * provide when a specific width is needed
     */
    const COLUMN_WIDTH = 'width';

    /**
     * mandatory for showing anything in the column
     */
    const COLUMN_DATA_INDEX = 'dataIndex';

    /**
     * Title for the column
     */
    const COLUMN_TITLE = 'title';

    /**
     * Position of column (integer 0 - x)
     */
    const COLUMN_POSITION = 'position';

    /**
     * provide a public method in your implementation to convert the value to something else; e.g. format date
     */
    const COLUMN_CONVERTER = 'converter';

    /**
     * style config supported by PhpExcel
     */
    const COLUMN_STYLE = 'style';

    /**
     * Indicator if value is a date
     */
    const DATE_VALUE = 'dateValue';

    /**
     * @var PHPExcel_Worksheet
     */
    protected $worksheet;

    /**
     * chars for building excel columns (A - ZZ)
     * @var string
     */
    private $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    /**
     * @var array
     */
    private $columns = [];

    /**
     * @var array
     */
    private $headerStyleConfig = [];

    /**
     * @var array
     */
    private $borderStyleConfig = [];

    /**
     * @var array
     */
    private $reportStyleConfig = [];

    /**
     * @var int
     */
    private $headerRowIndex = 1;

    /**
     * true means mandatory, false is optional
     * @var array
     */
    private static $applicableColumnConfig = [
        self::COLUMN_TITLE => true,
        self::COLUMN_DATA_INDEX => true,
        self::COLUMN_POSITION => false,
        self::COLUMN_CONVERTER => false,
        self::COLUMN_STYLE => false,
        self::COLUMN_WIDTH => false,
        self::DATE_VALUE => false
    ];

    /**
     * Provide a method that builds a usable
     * @return array
     */
    abstract protected function getColumnConfig(): array;

    /**
     * Excel_ContingentData constructor.
     * @param PHPExcel_Worksheet $worksheet
     */
    public function __construct(PHPExcel_Worksheet $worksheet)
    {
        $this->worksheet = $worksheet;

        $this->buildColumnConfig();
    }

    /**
     *
     */
    private function buildColumnConfig()
    {
        $columns = $this->getColumnConfig();
        foreach ($columns as $cfg) {
            $this->addColumn($cfg);
        }

        # sort columns by excel column name (A-ZZ)
        uksort($this->columns, static function ($a, $b) {
            $aLength = strlen($a);
            $bLength = strlen($b);
            if ($aLength === $bLength) {
                return strcmp($a, $b);
            }
            if ($aLength < $bLength) {
                return -1;
            }
            if ($bLength < $aLength) {
                return 1;
            }
            return 0;
        });
    }

    /**
     * @param array $config
     */
    private function addColumn(array $config)
    {
        foreach (self::$applicableColumnConfig as $key => $mandatory) {
            if ($mandatory && !isset($config[$key])) {
                throw new InvalidArgumentException(sprintf('mandatory column config: %s missing', $key));
            }
        }

        foreach ($config as $k => $v) {
            if (!isset(self::$applicableColumnConfig[$k])) {
                throw new InvalidArgumentException(sprintf('config: %s is not supported', $k));
            }
            if ($k === self::COLUMN_CONVERTER && !method_exists($this, $v)) {
                throw new InvalidArgumentException(sprintf('converter method: %s not found', $k));
            }
        }
        $position = $config['position'] ?? count($this->columns);

        $char = $this->getCharForPosition($position);
        $this->columns[$char] = $config;
    }

    /**
     * @param int $position
     * @return string
     */
    protected function getCharForPosition(int $position): string
    {
        if ($position < strlen($this->chars)) {
            return substr($this->chars, $position, 1);
        }
        $firstCharPosition = (int)floor($position / strlen($this->chars)) - 1;
        $offset = ($position - strlen($this->chars)) - ($firstCharPosition * strlen($this->chars));

        $char = sprintf(
            '%s%s',
            substr($this->chars, $firstCharPosition, 1),
            substr($this->chars, $offset, 1)
        );
        if ($char === false) {
            throw new InvalidArgumentException(sprintf('No character available for position: %d', $position));
        }
        return $char;
    }

    /**
     * @param array $data
     * @throws PHPExcel_Exception
     */
    public function generate(array $data)
    {
        $this->generateHeader();

        $rowIdx = $rowIdxStart = $this->headerRowIndex + 1;

        foreach ($data as $row) {
            $colIdx = 0;
            $this->worksheet->getStyle($this->getCoordinateRange($rowIdx))->applyFromArray($this->borderStyleConfig);

            foreach ($this->columns as $column => $cfg) {
                $value = $row[$cfg['dataIndex']];
                if (isset($cfg['converter']) && method_exists($this, $cfg['converter'])) {
                    $method = $cfg['converter'];
                    $value = $this->$method($value);
                }

                if (array_key_exists(Excel::DATE_VALUE, $cfg) && $cfg[Excel::DATE_VALUE]) {
                    $this->worksheet->getCellByColumnAndRow($colIdx, $rowIdx)->getStyle()->getNumberFormat()->setFormatCode(Export::Date_FORMAT);
                    $this->worksheet->setCellValueByColumnAndRow($colIdx, $rowIdx, $value !== null ? PHPExcel_Shared_Date::PHPToExcel(DateTime::createFromFormat('d.m.Y', $value)): null);
                } else {
                    $this->worksheet->setCellValueByColumnAndRow($colIdx, $rowIdx, $value);
                }
                $colIdx++;
            }
            $rowIdx++;
        }
        $this->applyStyles($rowIdxStart, $rowIdx - 1);
    }

    /**
     * @param int $rowIdxStart
     * @param int $rowIdxEnd
     * @throws PHPExcel_Exception
     */
    protected function applyStyles(int $rowIdxStart, int $rowIdxEnd)
    {
        // apply column styles (width does not belong to styling in excel...)
        foreach ($this->columns as $column => $cfg) {
            if (isset($cfg['style'])) {
                $coordinateRange = sprintf('%s%d:%s%d', $column, $rowIdxStart, $column, $rowIdxEnd);
                $this->worksheet->getStyle($coordinateRange)->applyFromArray($cfg['style']);
            }
            if (isset($cfg['width'])) {
                $this->worksheet->getColumnDimension($column)->setWidth($cfg['width']);
            }
        }
    }

    /**
     * @param int $rowIndex
     * @return string
     */
    protected function getCoordinateRange(int $rowIndex): string
    {
        $keys = array_keys($this->columns);
        return sprintf('%s%d:%s%d', reset($keys), $rowIndex, end($keys), $rowIndex);
    }

    /**
     * @throws PHPExcel_Exception
     */
    protected function generateHeader()
    {
        $colIdx = 0;
        $coordinateRange = $this->getCoordinateRange($this->headerRowIndex);
        $this->worksheet->getStyle($coordinateRange)->applyFromArray($this->headerStyleConfig);
        foreach ($this->columns as $col => $cfg) {
            $this->worksheet->getColumnDimensionByColumn($colIdx)->setAutoSize(true);
            $this->worksheet->setCellValueByColumnAndRow(
                $colIdx,
                $this->headerRowIndex,
                $cfg['title']
            );
            $colIdx++;
        }
    }

    /**
     * @param array $styleConfig
     */
    public function setHeaderStyle(array $styleConfig)
    {
        $this->headerStyleConfig = $styleConfig;
    }

    /**
     * @param array $styleConfig
     */
    public function setReportStyle(array $styleConfig)
    {
        $this->reportStyleConfig = $styleConfig;
    }

    /**
     * @param array $styleConfig
     */
    public function setBorderStyle(array $styleConfig)
    {
        $this->borderStyleConfig = $styleConfig;
    }

    /**
     * @return array
     */
    public function getApplicableColumnConfig(): array
    {
        return self::$applicableColumnConfig;
    }
}
