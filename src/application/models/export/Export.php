<?php
/**
 * Export class
 *
 * @author les
 * @since 08.03.2011
 */
class Export
{
    private $view;
    /**
     * @var Zend_View_Helper_Translate
     */
    private $translate;
    private $dir;
    private $filename;
    private $objPHPExcel;
    /**
     * @var PHPExcel_Worksheet
     */
    private $activeSheet;

    const Date_FORMAT = 'dd.mm.yyyy';
    private $timeFormat = 'hh:mm';
    private $dateTimeFormat = 'dd.mmm.yyyy hh:mm:ss';
    private $beginOf1900Date;
    private $numberFormat = PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1;
    private $percentFormat = PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00;

    private $orderOverviewOrderColumns;
    private $orderOverviewOrderPositionsColumns;
    private $orderOverviewInvoicePlanElementColumns;
    private $orderOverviewSumsColumns;

    const GREEN = 'FF92D050';
    const LIGHTRED = 'FF7673';
    const DAY_IN_SECONDS = 86400;

    public function __construct($view, $translate, $dir, $filename)
    {
        $this->view = $view;
        $this->translate = $translate;
        $this->dir = $dir;
        $this->filename = $filename;

        if (!file_exists($this->dir)) {
            mkdir($this->dir, 0777);
        }

        $this->objPHPExcel = new PHPExcel();
        $this->activeSheet = $this->objPHPExcel->setActiveSheetIndex(0);

        // default style for workbook
        $this->objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
        $this->objPHPExcel->getDefaultStyle()->getFont()->setSize(10);

        $this->beginOf1900Date = new Zend_Date('1900-01-01', 'YYYY-MM-dd');
    }

    public function __destruct()
    {
        // gargabe collection
        $this->objPHPExcel->disconnectWorksheets();
        unset($this->objPHPExcel);
    }

    private function getDateCellValue(Zend_Date $date)
    {
        $dateCellValue = 0;

        if (isset($date)) {
            // number of days since beginning of the 20th century, float if other time than 0 o'clock
            $dateCellValue = $date->sub($this->beginOf1900Date)->toValue() / self::DAY_IN_SECONDS;
            // the result is the period of time, add begin and end day
            $dateCellValue += 2;
        }

        return $dateCellValue;
    }

    private function getReportConfigStyle()
    {
        return array(
            'font' => array(
                'bold' => true
            ),
            'alignment' => array(
                'horizontal'    => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical'        => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
    }

    private function getStyleSummary() {
        return array(
            'font' => array(
                'bold' => true
            ),
            'alignment' => array(
                'horizontal'    => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                'vertical'      => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
    }

    private function getBorderTableStyle()
    {
        return array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
            ),
        );
    }

    private function getContentHeaderStyle()
    {
        return array(
            'font' => array(
                'bold' => true
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical'    => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap'        => true
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                )
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => 'FFDDDDDD'
                )
            )
        );
    }

    /**
     * Gets appropariate node style
     *
     * @param $nodeType
     * @param $structureUnitType
     */
    private function getNodeStyle($nodeType, $structureUnitType)
    {
        $font = array();
        $fill = array();

        switch ($nodeType) {
            case NodeType::STRUCTURE_UNIT:
                switch ($structureUnitType) {
                    case StructureUnitType::COMPANY:
                        $font = array(
                            'bold' => true,
                        );
                        $fill = array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array(
                                'argb' => 'FFD6D6D6'
                            )
                        );
                        break;
                    case StructureUnitType::DIVISION:
                        $font = array(
                            'bold' => true,
                        );
                        break;
                }
                break;
            case NodeType::DEBTOR:
                $font = array(
                    'bold' => true,
                    'italic' => true
                );
                break;
        }

        return array('font' => $font, 'fill' => $fill);
    }

    private function setUpPageSetup()
    {
        $objSheet = $this->objPHPExcel->getActiveSheet();
        $objSheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objSheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $objSheet->getPageSetup()->setFitToPage(true);
        $objSheet->getPageSetup()->setFitToWidth(1);
        $objSheet->getPageSetup()->setFitToHeight(0);
    }

    private function trim($value)
    {
        return trim($value, ' ' . chr(0xC2) . chr(0xA0));
    }

    private function escapeSheetTitle($value)
    {
        return substr(str_replace(array('*', ':', '/', '\\', '?', '[', ']'), '', $value), 0, 31);
    }

    private function getContent()
    {
        $filename_path = $this->dir . $this->filename;
        $objWriterExcel5 = new PHPExcel_Writer_Excel5($this->objPHPExcel);
        $objWriterExcel5->save($filename_path);

        $content = file_get_contents($filename_path);
        unlink($filename_path);

        return $content;
    }

    private function getContentXlsx()
    {
        $filename_path = $this->dir . $this->filename;

        $objWriter2007 = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
        $objWriter2007->setPreCalculateFormulas(false);
        $objWriter2007->save($filename_path);

        $content = file_get_contents($filename_path);
        unlink($filename_path);

        return $content;
    }

    private function createHeader($name)
    {
        $configStyle = $this->getReportConfigStyle();

        $this->objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
        $this->objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(40);

        // Report config
        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, 1)->applyFromArray($configStyle);
        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, 1)->getFont()->setSize(12);
        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1, 1)->getFont()->setSize(12);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'Report:');
        $this->objPHPExcel->getActiveSheet()
            ->setCellValueByColumnAndRow(1, 1, $this->view->escape($this->translate->translate("$name")));

        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, 3)->applyFromArray($configStyle);
        $this->objPHPExcel->getActiveSheet()
            ->setCellValueByColumnAndRow(0, 3, $this->view->escape($this->translate->translate('erstellungvon')) . ":");
        $this->objPHPExcel->getActiveSheet()
            ->setCellValueByColumnAndRow(1, 3, $this->view->escape($this->view->user));

        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, 4)->applyFromArray($configStyle);
        $this->objPHPExcel->getActiveSheet()
            ->setCellValueByColumnAndRow(0, 4, $this->
                view->escape($this->translate->translate('erstellungdatum')) . ":");

        $date = new Zend_Date();
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(
            1,
            4,
            $this->view->escape($date->toString('dd.MMM yyyy HH:mm:ss', $this->view->locale))
        );
        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1, 4)->getNumberFormat()
            ->setFormatCode($this->dateTimeFormat);

        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, 6)->applyFromArray($configStyle);
        $this->objPHPExcel->getActiveSheet()
            ->setCellValueByColumnAndRow(0, 6, $this->view->escape($this->translate->translate('filterOpts')));

        $row = 7;

        if (isset($this->view->data['REPORT_INFO']['PERIODFROM'])) {
            $dateFrom = date('d.m.Y', $this->view->data['REPORT_INFO']['PERIODFROM']);
            $dateTill = date('d.m.Y', $this->view->data['REPORT_INFO']['PERIODUNTIL']);

            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $row)->applyFromArray($configStyle);
            $this->objPHPExcel->getActiveSheet()
                ->setCellValueByColumnAndRow(0, $row, $this->view->escape($this
                        ->translate->translate('zeitraumal')) . ":");
            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $dateFrom . ' - ' . $dateTill);

            ++$row;
        }

        if ($this->view->data['REPORT_INFO']['TYPE'] === 'G') {
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, 9)->applyFromArray($configStyle);
            $this->objPHPExcel->getActiveSheet()
                ->setCellValueByColumnAndRow(0, 9, $this->view->escape($this->translate->translate('hint')) . ":");
            $this->objPHPExcel->getActiveSheet()
                ->setCellValueByColumnAndRow(1, 9, $this->view->escape($this->translate->translate('AccountableHoursHintMessage')));
        }

        $iter = 0;

        if (isset($this->view->data['REPORT_INFO']['NODES'])
            && !in_array($name, array('misDivision', 'projectStatus'))
        ) {
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $row)->applyFromArray($configStyle);
            $this->objPHPExcel->getActiveSheet()
                ->setCellValueByColumnAndRow(0, $row, $this->translate->translate('treeNodes') . ":");
            if (count($this->view->data['REPORT_INFO']['NODES']) === 0) {
                $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "---");
                $iter++;
            } else {
                foreach ($this->view->data['REPORT_INFO']['NODES'] as $item) {
                    if (false == $item['set']) {
                        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(
                            1,
                            $row + $iter,
                            $this->view->nodeTypes[$item['type']] . ': ' . $item['name'] . ' (' . $this
                                ->translate->translate('node') . ')'
                        );
                    } else {
                        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(
                            1,
                            $row + $iter,
                            $this->view->escape($item['name'] . ' (' . $this
                                    ->translate->translate('set') . ')')
                        );
                    }
                    $iter++;
                }
            }
        }

        $idx = $row + $iter;
        if (isset($this->view->data['REPORT_INFO']['EMPLOYEES'])) {
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $idx)->applyFromArray($configStyle);
            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $this->view->escape($this
                    ->translate->translate('employees')) . ":");
            $count = count($this->view->data['REPORT_INFO']['EMPLOYEES']['employees']) +
                count($this->view->data['REPORT_INFO']['EMPLOYEES']['groups']);
            foreach ($this->view->data['REPORT_INFO']['EMPLOYEES']['groups'] as $group) {
                $count += count($group['members']);
            }

            if ($count <= 10) {
                $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx, $this
                    ->renderEmployeesAndGroups($this->view->data['REPORT_INFO']['EMPLOYEES']));
            } else {
                $this->objPHPExcel->getActiveSheet()->getCommentByColumnAndRow(1, $idx)
                    ->setWidth(150 + $count * 10)->getText()->createTextRun($this->renderEmployeesAndGroups($this
                        ->view->data['REPORT_INFO']['EMPLOYEES']));
                $this->objPHPExcel->getActiveSheet()
                    ->setCellValueByColumnAndRow(1, $idx, $this->translate->translate('seeComment'));
            }
        }

        if (in_array($name, array('orderOverview'))) {
            $filters = $this->view->data['REPORT_INFO']['CLOSED_ORDERS']
                + $this->view->data['REPORT_INFO']['ORDER_AFTER_PERIOD']
                + $this->view->data['REPORT_INFO']['WITHOUT_ORDERS_AFTER_PERIOD']
                + $this->view->data['REPORT_INFO']['WITH_HIGHER_LEVEL_POSITION'];

            if ($filters) {
                $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $idx)->applyFromArray($configStyle);

                $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $this->view->escape($this
                        ->translate->translate('displayedInfo')) . ":");

                if ($this->view->data['REPORT_INFO']['CLOSED_ORDERS']) {
                    $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx++, $this->view->escape($this->translate->translate('closedOrdersShort')));
                }

                if ($this->view->data['REPORT_INFO']['ORDER_AFTER_PERIOD']) {
                    $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx++, $this->view->escape($this->translate->translate('ordersAfterPeriodShort')));
                }

                if ($this->view->data['REPORT_INFO']['WITHOUT_ORDERS_AFTER_PERIOD']) {
                    $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx++, $this->view->escape($this->translate->translate('withoutOrdersAfterPeriodShort')));
                }

                if ($this->view->data['REPORT_INFO']['WITH_HIGHER_LEVEL_POSITION']) {
                    $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx++, $this->view->escape($this->translate->translate('withHigherLevelPositionShort')));
                }
            }
            if ($this->view->data['REPORT_INFO']['MATERIAL_FILTER']) {
                $materialFilters = [];
                foreach ($this->view->data['REPORT_INFO']['MATERIAL_FILTER'] as $filter) {
                    $materialFilters[] = $filter['NAME_' . strtoupper($this->translate->getLocale())];
                }
                $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $idx)->applyFromArray($configStyle);
                $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(
                    0,
                    $idx,
                    $this->view->escape($this->translate->translate('filteredBy')) . ":");
                $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1, $idx)->getAlignment()->setWrapText(true);
                $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx++, implode("\n", $materialFilters));
            }
        }

        return $idx + 2;
    }

    /**
     * Converts an employee array from structure
     *  array(
     *    'employees' => array(...),
     *      'groups' => array(
     *         array('name' => '', 'members' => array(...)),
     *         ...
     *     )
     *  )
     * to string "[group1[ (member1, ..., memberN)],...,groupN] - [emp1, ..., empN]"
     * e.g.: "ALLE, ISS, WEBTEAM (gen, gla, les, pov, zeu) - zeu, gen"
     *
     * @param array $data
     * @return string
     */
    private function renderEmployeesAndGroups($data)
    {
        if (!is_array($data)) {
            return '';
        }
        if (empty($data['employees']) && empty($data['groups'])) {
            return '---';
        }

        $groups = array();
        if (is_array($data['groups'])) {
            foreach ($data['groups'] as $group) {
                $g = $group['name'];
                if (is_array($group['members']) && !empty($group['members'])) {
                    $g .= ' (' . implode(', ', $group['members']) . ')';
                }
                $groups[] = $g;
            }
        }

        $output = array();
        if (!empty($groups)) {
            $output[] = implode(', ', $groups);
        }
        if (is_array($data['employees']) && !empty($data['employees'])) {
            $output[] = implode(', ', $data['employees']);
        }

        return implode(' - ', $output);
    }

    private function createListHeader()
    {
        $idx = $this->createHeader('liste');
        $idx = $idx - 2;

        $configStyle = $this->getReportConfigStyle();

        if (isset($this->view->filter_text) && $this->view->filter_text != '') {
            $idx++;
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $idx)->applyFromArray($configStyle);
            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $this->view
                ->escape($this->translate->translate('suche')));

            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx, $this->view
                ->escape($this->view->filter_text));

            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(2, $idx)->applyFromArray($configStyle);
            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idx, $this->view->escape($this
                    ->translate->translate('in') . ' ' . $this->translate->translate('spalte')));

            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idx, $this->view
                ->escape($this->translate->translate($this->view->filter_column)));
        }

        if ($this->view->notiztext != '') {
            $idx++;
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $idx)->applyFromArray($configStyle);
            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $this->view
                    ->escape($this->translate->translate('bc_notiztext')) . ":");

            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx, $this->view
                ->escape($this->view->notiztext));
        }

        return $idx + 2;
    }

    private function createPermissionHeader()
    {
        $idx = $this->createHeader('permissionReport');
        $idx = $idx - 2;

        $configStyle = $this->getReportConfigStyle();

        if (isset($this->view->filter_text) && $this->view->filter_text != '') {
            $idx++;
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $idx)->applyFromArray($configStyle);
            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $this->view
                    ->escape($this->translate->translate('suche')) . ':');

            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx, $this->view
                ->escape($this->view->filter_text));
        }

        return $idx + 2;
    }

    private function createBookingOverviewHeader($profileBookingTypes, $displayOnlySubtrees)
    {
        $idx = $this->createHeader('bookingReport');
        $idx = $idx - 2;

        $configStyle = $this->getReportConfigStyle();

        $idx++;
        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $idx)->applyFromArray($configStyle);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $this->view
                ->escape($this->translate->translate('withEmpBookings')) . ":");
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx, $this->view
            ->escape($this->view->data['REPORT_INFO']['EMP_BOOKINGS'] == 1 ? $this
                ->translate->translate('ja') : $this->translate->translate('nein')));

        $idx++;
        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $idx)->applyFromArray($configStyle);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $this
                ->view->escape($this->translate->translate('withCostRevenue')) . ":");
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx, $this->view
            ->escape($this->view->data['REPORT_INFO']['COSTREVENUE'] == 1 ? $this
                ->translate->translate('ja') : $this->translate->translate('nein')));

        $idx++;
        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $idx)->applyFromArray($configStyle);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $this->view
                ->escape($this->translate->translate('withTargetHours')) . ":");
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx, $this->view
            ->escape($this->view->data['REPORT_INFO']['TARGET_HOURS'] == 1 ? $this->translate->translate('ja') : $this
                ->translate->translate('nein')));

        if ($displayOnlySubtrees) {
            $idx++;
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $idx)->applyFromArray($configStyle);
            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $this->view
                    ->escape($this->translate->translate('bookingType')) . ":");
            $this->objPHPExcel->getActiveSheet()
                ->setCellValueByColumnAndRow(1, $idx, implode(', ', $profileBookingTypes));
        }

        return $idx + 2;
    }

    public function exportList()
    {
        $data = $this->view->data['DATA'];
        $reportInfo = $this->view->data['REPORT_INFO'];
        $rowsCount = count($data);

        $additionalCols = $this->view->data['ADDITIONAL_COLS'];
        $remoteWorkReport = $reportInfo['REMOTE_WORK_REPORT'];

        $objSheet = $this->objPHPExcel->getActiveSheet();

        //Set up page setup
        $this->setUpPageSetup();
        // Report config
        $idx = $this->createListHeader();

        // Table header
        $headerContentTableStyle = $this->getContentHeaderStyle();

        // columns definitions
        $columns = [
            'company'   => null,
            'division'  => null
        ];

        if (!$remoteWorkReport) {
            $columns['presenceFrom'] = null;
            $columns[ 'presenceTo'] = null;
        }

        $columns['date'] = [
            'title' => $this->translate->translate('datum'),
            'width' => null,
            'show' => true,
            'format' => self::Date_FORMAT
        ];

        $columns['dayType'] = null;
        $columns['employeeShortcut'] = [
            'title' => $this->translate->translate('mitarbeiter'),
            'width' => 18,
            'show' => true
        ];
        $columns['employeeName'] = null;

        if (!$remoteWorkReport) {
            $columns['costcentre'] = [
                'title' => $this->translate->translate('abkbkz'),
                'width' => -1,
                'show' => true
            ];
            $columns['hours'] = [
                'title' => $this->translate->translate('stunden'),
                'width' => null, 'show' => true,
                'format' => $this->numberFormat
            ];
            $columns['bookingType'] = [
                'title' => $this->translate->translate('bookingType'),
                'width' => 13,
                'show' => true
            ];
            $columns['bookingBegin'] = null;
            $columns['bookingEnd'] = null;
            $columns['break'] = null;
        }

        $noteTextKey = $remoteWorkReport ? 'remoteWorkMarker' : 'notiz';

        $columns['note'] = [
            'title' => $this->translate->translate($noteTextKey),
            'width' => 36,
            'show' => true
        ];

        if (!$remoteWorkReport) {
            $columns['entryIds'] = [
                'title' => $this->translate->translate('abkbearbeitungsnr'),
                'width' => 15,
                'show' => true
            ];
        }
        if (isset($additionalCols[AdditionalCol::LIST_COMPANY])) {
            $columns['company'] = array('title' => $additionalCols[AdditionalCol::LIST_COMPANY]['NAME']);
        }
        if (isset($additionalCols[AdditionalCol::LIST_DIVISION])) {
            $columns['division'] = array('title' => $additionalCols[AdditionalCol::LIST_DIVISION]['NAME']);
        }
        if (isset($additionalCols[AdditionalCol::LIST_FROM_TILL])) {
            $columns['presenceFrom'] = array('title' => $this->translate->translate('fromDate'), 'format' => $this
                ->timeFormat);
            $columns['presenceTo'] = array('title' => $this->translate->translate('toDate'), 'format' => $this
                ->timeFormat);
        }
        if (isset($additionalCols[AdditionalCol::LIST_DAY_TYPE])) {
            $columns['dayType'] = array(
                'title' => $additionalCols[AdditionalCol::LIST_DAY_TYPE]['NAME'],
                'width' => -1
            );
        }
        if (isset($additionalCols[AdditionalCol::LIST_EMPLOYEE_NAME])) {
            $columns['employeeName'] = array(
                'title' => $additionalCols[AdditionalCol::LIST_EMPLOYEE_NAME]['NAME'],
                'width' => 15
            );
        }
        if (isset($additionalCols[AdditionalCol::LIST_BEGIN_END])) {
            $columns['bookingBegin'] = array('title' => $this->translate->translate('begin'));
            $columns['bookingEnd'] = array('title' => $this->translate->translate('end'));
        }

        if (isset($additionalCols[AdditionalCol::LIST_BREAK])) {
            $columns['break'] = array('title' => $this->translate->translate('break'));
        }

        // create column headers
        $colIdx = 0;
        foreach ($columns as &$colHeader) {
            if (!is_array($colHeader) || isset($colHeader['show']) && true !== $colHeader['show']) {
                continue;
            }

            $objSheet->getStyleByColumnAndRow($colIdx, $idx)->applyFromArray($headerContentTableStyle);
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $colHeader['title']);

            $colHeader['i'] = $colIdx;
            $colHeader['c'] = PHPExcel_Cell::stringFromColumnIndex($colIdx++);

            // set column width
            if (isset($colHeader['width'])) {
                if (-1 === $colHeader['width']) {
                    $objSheet->getColumnDimension($colHeader['c'])->setAutoSize(true);
                } else {
                    $objSheet->getColumnDimension($colHeader['c'])->setWidth($colHeader['width']);
                }
            }
            // set column format
            if (isset($colHeader['format'])) {
                // we cannot simply adders the whole column here since those styles
                // can be overridden when other styles are applied to the cell
                $objSheet->getStyle($colHeader['c'] . ($idx + 1) . ":" . $colHeader['c'] . ($idx + 1 + $rowsCount))
                    ->getNumberFormat()->setFormatCode($colHeader['format']);
            }
        }

        // apply sort column style
        $sortIndexes = array(
            'COMPANY_NAME'    => 'company',
            'DIVISION_NAME'    => 'division',
            'PRESENCE_FROM' => 'presenceFrom',
            'PRESENCE_TO'    => 'presenceTo',
            'CDATE'            => 'date',
            'DAY_TYPE'        => 'dayType',
            'SHORTCUT'        => 'employeeShortcut',
            'NAME'            => 'costcentre',
            'CHOURS'        => 'hours',
            'BOOKING_TYPE'    => 'bookingType',
            'BOOKING_BEGIN'    => 'bookingBegin',
            'BOOKING_END'    => 'bookingEnd',
            'BREAK'         => 'break',
            'CNOTE'            => 'note',
            'PDBIDS'        => 'entryIds'
        );
        if (isset($sortIndexes[$this->view->sort_column])) {
            $colIdx = $columns[$sortIndexes[$this->view->sort_column]]['i'];
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($colIdx, $idx)->applyFromArray(array(
                'font' => array(
                    'color' => array(
                        'argb' => 'FF0066FF'
                    ),
                    'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE
                )
            ));
        }

        if (empty($data) || false == is_array($data)) {
            return $this->getContentXlsx();
        }

        $idx++;
        $contentStyle = array(
            'alignment' => array(
                'vertical'    => PHPExcel_Style_Alignment::VERTICAL_TOP
            )
        );

        $date = new Zend_Date();
        $lastColumn = $remoteWorkReport ? 'note' : 'entryIds';

        $rangeStart = PHPExcel_Cell::stringFromColumnIndex($columns['company']['i']) . $idx;
        $rangeStartWrap = PHPExcel_Cell::stringFromColumnIndex($columns['note']['i']) . $idx;
        $rangeEnd = PHPExcel_Cell::stringFromColumnIndex($columns[$lastColumn]['i']) . ($idx + $rowsCount - 1);

        $objSheet->getStyle($rangeStart . ":" . $rangeEnd)->applyFromArray($contentStyle);
        $objSheet->getStyle($rangeStartWrap . ":" . $rangeEnd)->getAlignment()->setWrapText(true);

        foreach ($data as &$row) {
            if (isset($additionalCols[AdditionalCol::LIST_COMPANY])) {
                $objSheet->setCellValueByColumnAndRow($columns['company']['i'], $idx, $row['COMPANY_NAME']);
            }
            if (isset($additionalCols[AdditionalCol::LIST_DIVISION])) {
                $objSheet->setCellValueByColumnAndRow($columns['division']['i'], $idx, $row['DIVISION_NAME']);
            }

            if (isset($additionalCols[AdditionalCol::LIST_FROM_TILL])) {
                if ($row['PRESENCE_FROM']) {
                    $date->set($row['PRESENCE_FROM'], 'HH:mm');
                    $objSheet->setCellValueByColumnAndRow($columns['presenceFrom']['i'], $idx, $date
                        ->toString('HH:mm'));
                }

                if ($row['PRESENCE_TO']) {
                    $date->set($row['PRESENCE_TO'], 'HH:mm');
                    $objSheet->setCellValueByColumnAndRow($columns['presenceTo']['i'], $idx, $date->toString('HH:mm'));
                }
            }

            $date->set($row['CDATE'], 'dd.MM.yy');
            $objSheet->setCellValueByColumnAndRow($columns['date']['i'], $idx, $this->getDateCellValue($date));

            if (isset($additionalCols[AdditionalCol::LIST_DAY_TYPE])) {
                $objSheet->setCellValueByColumnAndRow($columns['dayType']['i'], $idx, $this->translate
                    ->translate('dayType_' . $row['DAY_TYPE']));
            }

            $objSheet->setCellValueByColumnAndRow($columns['employeeShortcut']['i'], $idx, $row['SHORTCUT']);

            if (isset($additionalCols[AdditionalCol::LIST_EMPLOYEE_NAME])) {
                $objSheet
                    ->setCellValueByColumnAndRow($columns['employeeName']['i'], $idx, $row['FIRST_NAME'] . ' ' .
                        $row['LAST_NAME']);
            }

            if (!$remoteWorkReport) {
                $objSheet->setCellValueByColumnAndRow($columns['costcentre']['i'], $idx, html_entity_decode($row['BKZ']));

                $value = $this->trim($row['CHOURS']);
                $objSheet->setCellValueByColumnAndRow($columns['hours']['i'], $idx, floatval($value));

                $objSheet->setCellValueByColumnAndRow($columns['bookingType']['i'], $idx, $row['BOOKING_TYPE']);
            }

            if (isset($additionalCols[AdditionalCol::LIST_BEGIN_END])) {
                $objSheet->setCellValueByColumnAndRow($columns['bookingBegin']['i'], $idx, $row['BOOKING_BEGIN']);
                $objSheet->setCellValueByColumnAndRow($columns['bookingEnd']['i'], $idx, $row['BOOKING_END']);
            }

            if (isset($additionalCols[AdditionalCol::LIST_BREAK])) {
                $objSheet->setCellValueByColumnAndRow($columns['break']['i'], $idx, $row['BREAK']);
            }

            $objSheet->setCellValueExplicitByColumnAndRow($columns['note']['i'], $idx, $row['CNOTE']);

            if (!$remoteWorkReport) {
                if ($row['PDBIDS'] != null) {
                    $pdbids = strtolower(implode($row['PDBIDS'], ','));
                } else {
                    $pdbids = '';
                }
                $objSheet->setCellValueByColumnAndRow($columns['entryIds']['i'], $idx, $pdbids);
            }

            $idx++;
        }

        return $this->getContentXlsx();
    }

    public function exportPdbInfos()
    {

        // Report config
        $idx = $this->createListHeader();

        // Table header
        $headerContentTableStyle = $this->getContentHeaderStyle();

        //Set up page setup
        $this->setUpPageSetup();

        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $idx)->applyFromArray($headerContentTableStyle);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $this->view->escape($this->translate
            ->translate('abkbearbeitungsnr')));

        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1, $idx)->applyFromArray($headerContentTableStyle);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx, $this->view->escape($this->translate
            ->translate('titel')));

        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(2, $idx)->applyFromArray($headerContentTableStyle);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idx, $this->view->escape($this->translate
            ->translate('status')));
        $this->objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);

        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(3, $idx)->applyFromArray($headerContentTableStyle);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idx, $this->view
            ->escape(ucfirst($this->translate->translate('klassifikation'))));
        $this->objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);

        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(4, $idx)->applyFromArray($headerContentTableStyle);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idx, $this->view->escape($this->translate
            ->translate('produkt')));
        $this->objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setAutoSize(true);

        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(5, $idx)->applyFromArray($headerContentTableStyle);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idx, $this->view->escape($this->translate
            ->translate('projekt')));
        $this->objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setAutoSize(true);

        $data = $this->view->data['DATA'];
        if (empty($data) || false == is_array($data)) {
            return $this->getContentXlsx();
        }

        $idx++;
        $contentStyle = array(
            'borders' => array(
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'vertical'        => PHPExcel_Style_Alignment::VERTICAL_TOP
            )
        );

        foreach ($data as $pdbentry) {
            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $this->view
                ->escape($pdbentry['PDBID']));
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $idx)->applyFromArray($contentStyle);

            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx, $this->view
                ->escape(htmlentities($pdbentry['TITLE'])));
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1, $idx)->applyFromArray($contentStyle);
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1, $idx)->getAlignment()->setWrapText(true);

            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idx, $this->view
                ->escape($pdbentry['STATE']));
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(2, $idx)->applyFromArray($contentStyle);

            $limit = count($pdbentry['CLASSIFICATION']);

            $value = '';
            for ($loop = 0; $loop < $limit; $loop++) {
                $date = new Zend_Date($pdbentry['CLASSIFICATION'][$loop]['DATE'], 'dd.MM.yy');
                $value .= $this->view->escape($date->toString('dd.MMM yyyy', $this->view->locale));
                $value .= ' ' . $this->view->escape($pdbentry['CLASSIFICATION'][$loop]['CLASSIFICATION']);
                $value .= ' (';
                switch ($pdbentry['CLASSIFICATION'][$loop]['TYP']) {
                    case 0:
                        $value .= 'A';
                        break;
                    case 1:
                        $value .= 'N';
                        break;
                    case -1:
                        $value .= 'S';
                        break;
                };
                $value .= ')';

                if ($limit > 1 && $loop <= ($limit - 2)) {
                    $value .= chr(10);
                }
            }

            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idx, $value);
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(3, $idx)->applyFromArray($contentStyle);
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(3, $idx)->getAlignment()->setWrapText(true);

            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idx, $this->view
                ->escape($pdbentry['PRODUCT']));
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(4, $idx)->applyFromArray($contentStyle);

            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idx, $this->view
                ->escape($pdbentry['PROJECT']));
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(5, $idx)->applyFromArray($contentStyle);

            $idx++;
        }

        return $this->getContentXlsx();
    }

    public function exportBookings($showEmpHours)
    {
        $showEmpBookings = $this->view->data['REPORT_INFO']['EMP_BOOKINGS'] == 1 ? true : false;
        $showCostAndRevenue = $this->view->data['REPORT_INFO']['COSTREVENUE'] == 1 ? true : false;
        $showBtHours = $this->view->data['REPORT_INFO']['BOOKING_TYPE_HOURS'] == 1 ? true : false;
        $showTargetHours = $this->view->data['REPORT_INFO']['TARGET_HOURS'] == 1 ? true : false;
        $displayOnlySubtrees = $this->view->data['REPORT_INFO']['DISPLAY_ONLY_SUBTREES'] == 1 ? true : false;
        $profileBookingTypes = $this->view->data['REPORT_INFO']['PROFILE_BOOKING_TYPES'];

        $availableCols = array();

        $btObj = new BookingType();
        $bookingTypes = array();

        foreach ($btObj->getBookingTypesAsArray() as $key => $val) {
            if (in_array($val, $profileBookingTypes) || !$displayOnlySubtrees) {
                $bookingTypes[$key] = $val;
            }
        }

        $nodeTypeObj = new NodeType();
        $nodeTypes = array();
        foreach ($nodeTypeObj->getNodeTypes($this->translate->getLocale()) as $nodeType) {
            $nodeTypes[$nodeType['ID']] = $nodeType['SHORTCUT'];
        }

        if ($showBtHours) {
            foreach ($bookingTypes as $bookingType) {
                $availableCols[$bookingType . 'Hours'] = array('translation' => $bookingType,
                    'width' => 10);
            }
        }

        $columns = array(
            'nodeType' => array(
                'translation' => $this->view->escape($this->translate->translate('nodeType')),
                'width' => $showBtHours ? 12 : 35),
            'number' => array(
                'translation' => $this->view->escape($this->translate->translate('number')),
                'width' => $showBtHours ? 10 : 17),
            'targetHours' => array(
                'translation' => ucfirst($this->view->escape($this->translate->translate('abbrTargetHours'))),
                'comment' => $this->translate->translate('targetHoursAggregatedValuesTooltip'),
                'width' => 14),
            'remainingHours' => array(
                'translation' => ucfirst($this->view->escape($this->translate->translate('abbrRemainingHours'))),
                'comment' => $this->translate->translate('remainingHoursAggregatedValuesTooltip'),
                'width' => 16),
            'remainingHoursMonth' => array(
                'translation' => ucfirst($this->view->escape($this->translate->translate('abbrRemainingHoursMonth'))),
                'width' => 16,
                'alignment' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
            'hours' => array(
                'translation' => ucfirst($this->view->escape($this->translate->translate('stunden'))),
                'width' => 10),
            'bookingTree' => array(
                'translation' => $this->view->escape($this->translate->translate('bookingTree'))),
            'empBookings' => array(
                'translation' => $this->view->escape($this->translate->translate('empBookings')),
                'width' => 13),
            'singleBookings' => array(
                'translation' => $this->view->escape($this->translate->translate('singleBookings')),
                'width' => 45),
            'revenue' => array(
                'translation' => str_replace(chr(0xC2) . chr(0x80), chr(0xE2) . chr(0x82) . chr(0xAC), $this
                    ->view->escape(str_replace(' (&euro;)', '', $this->translate->translate('revenue')))),
                'width' => 10),
            'cost' => array(
                'translation' => str_replace(chr(0xC2) . chr(0x80), chr(0xE2) . chr(0x82) . chr(0xAC), $this
                    ->view->escape(str_replace(' (&euro;)', '', $this->translate->translate('cost')))),
                'width' => 10),
            'sapKeyCostUnit' => array(
                'translation' => $this->view->escape($this->translate->translate('sapKeyCostUnit')),
                'width' => 20),
            'sapKeyCostCenter' => array(
                'translation' => $this->view->escape($this->translate->translate('sapKeyCostCenter')),
                'width' => 23)
        );

        $availableCols = array_merge($availableCols, $columns);

        if (false == $showEmpBookings) {
            unset($availableCols['empBookings']);
            unset($availableCols['singleBookings']);
        }

        if (false == $showEmpHours) {
            unset($availableCols['singleBookings']);
        }

        if (false == $showCostAndRevenue) {
            unset($availableCols['revenue']);
            unset($availableCols['cost']);
        }

        if (false == $showTargetHours) {
            unset($availableCols['targetHours']);
            unset($availableCols['remainingHours']);
            unset($availableCols['remainingHoursMonth']);
        }

        //Report config
        $idx = $this->createBookingOverviewHeader($profileBookingTypes, $displayOnlySubtrees);

        //Set up page setup
        $this->setUpPageSetup();

        //Table header
        $styleTableHead = $this->getContentHeaderStyle();
        $colIdx = 0;
        foreach ($availableCols as $key => $column) {
            if ($key != 'bookingTree') {
                $this->objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colIdx)->setWidth($column['width']);
            } else {
                $this->objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colIdx)->setAutoSize(true);
            }
            $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($colIdx, $idx)
                ->applyFromArray($styleTableHead);
            if (isset($column['alignment'])) {
                $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($colIdx, $idx)->getAlignment()
                    ->setHorizontal($column['alignment']);
            }
            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colIdx, $idx, $column['translation']);

            if (isset($column['comment'])) {
                $this->objPHPExcel->getActiveSheet()->getCommentByColumnAndRow($colIdx, $idx)->setWidth(300)
                    ->setHeight(150)->getText()->createTextRun($column['comment']);
            }

            $colIdx++;
        }

        if (false == empty($this->view->data)) {
            //data for recursiveTree()
            $tree = $this->view->data['TREE'];

            $regEx = "@<span[^>]*><i>(.*)</i></span>@";
            $tab = -1;

            if (false == empty($tree)) {
                $globalRow = $idx;
                foreach ($tree as $element) {
                    $globalRow = $this->recursiveTree($element, $globalRow, $tab, $regEx, $this->view
                        ->data['EMP_BOOKINGS'], $availableCols, $showEmpHours, $showBtHours, $nodeTypes, $bookingTypes);
                }
            }
        }
        return $this->getContentXlsx();
    }

    private function recursiveTree(
        $node,
        $globalRow,
        $tab,
        $regEx,
        $emp_bookings,
        $availableCols,
        $showEmpHours,
        $showBtHours,
        $nodeTypes,
        $bookingTypes
    ) {
        $globalRow++;
        $tab++;
        $indentation = '';
        for ($z = 0; $z < $tab; $z++) {
            $indentation = $indentation . '     ';
        }

        $colIdx = 0;
        $empBookingsColIdx = 0;
        $singleBookingsColIdx = 0;
        foreach ($availableCols as $key => $column) {
            $value = null;
            $style = null;
            $numberFormat = null;
            $italics = null;
            switch ($key) {
                case 'nodeType':
                    $value = $node['nodeType'] ? $nodeTypes[$node['nodeType']] : '';
                    break;
                case 'number':
                    $value = $node['debtor'];
                    break;
                case 'targetHours':
                    $value = isset($node['target_effort']) && $node['target_effort'] > 0 ? $node['target_effort'] : '';
                    if (isset($node['target_effort_is_aggregated']) && $node['target_effort_is_aggregated'] > 0) {
                        $italics = true;
                    }
                    $numberFormat = $this->numberFormat;
                    break;
                case 'remainingHours':
                    $value = isset($node['remaining_effort'])? $node['remaining_effort'] : '';
                    if (isset($node['remaining_effort_is_aggregated']) && $node['remaining_effort_is_aggregated'] > 0) {
                        $italics = true;
                    }
                    $numberFormat = $this->numberFormat;
                    break;
                case 'remainingHoursMonth':
                    $value = isset($node['remaining_effort_month'])? $node['remaining_effort_month'] : '';
                    break;
                case 'hours':
                    $value = $node['hours'];
                    $numberFormat = $this->numberFormat;
                    break;
                case 'bookingTree':
                    $value = (preg_match_all($regEx, $node['node'], $matches)) ?
                        $indentation . $matches[1][0] :
                        $indentation . $node['node'];
                    $style = $this->getNodeStyle($node['nodeType'], $node['suType']);
                    break;
                case 'empBookings': // Values are set below
                    $empBookingsColIdx = $colIdx;
                    break;
                case 'singleBookings': // Values are set below
                    $singleBookingsColIdx = $colIdx;
                    break;
                case 'revenue':
                    $value = $node['revenue'];
                    $numberFormat = $this->numberFormat;
                    break;
                case 'cost':
                    $value = $node['cost'];
                    $numberFormat = $this->numberFormat;
                    break;
                case 'sapKeyCostUnit':
                    $value = $node['sapKeyCostUnit'];
                    break;
                case 'sapKeyCostCenter':
                    $value = $node['sapKeyCostCenter'];
                    break;
            }

            if ($showBtHours) {
                //If variable $key is a Booking type
                foreach ($bookingTypes as $bookingType) {
                    if ($key == ($bookingType . 'Hours')) {
                        $valueName = $bookingType . 'Hours';
                        $value = $node[$valueName];
                        $numberFormat = $this->numberFormat;
                        break;
                    }
                }
            }

            if ($italics) {
                $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($colIdx, $globalRow)->getFont()
                    ->setItalic(true);
            }
            if (!is_null($value)) {
                $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colIdx, $globalRow, $value);
            }
            if ($style) {
                $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($colIdx, $globalRow)
                    ->applyFromArray($style);
            }
            if ($numberFormat) {
                $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($colIdx, $globalRow)->getNumberFormat()
                    ->setFormatCode($numberFormat);
            }


            $colIdx++;
        }

        if ($node['leaf'] == true && count($emp_bookings) > 0 && isset($emp_bookings[$node['id']])) {
            $bookingInfo = $emp_bookings[$node['id']];

            // iterate over all employees that booked on that node (and add a row for each)
            foreach ($bookingInfo as $empBookingEntries) {
                $globalRow++;

                $EmployeeSummary = number_format($empBookingEntries['Hours'], 2, ',', '.') . ' h ' .
                    $empBookingEntries['Employee'];
                $this->objPHPExcel->getActiveSheet()
                    ->setCellValueByColumnAndRow($empBookingsColIdx, $globalRow, $EmployeeSummary);

                if ($showEmpHours) {
                    // iterate over all single bookings of one employee (and add a row for each)
                    $count = count($empBookingEntries['Bookings']);
                    for ($i = 0; $i < $count; $i++) {
                        $globalRow++;

                        $bookingDate = new Zend_Date($empBookingEntries['Bookings'][$i]['Date'], 'yyyy-MM-dd');
                        $bookingDate = $bookingDate->get('dd.MM.yyyy');

                        $singleBooking = number_format($empBookingEntries['Bookings'][$i]['Hours'], 2, ',', '.') .
                            ' h ' . $empBookingEntries['Bookings'][$i]['BT'] . ' ' . $bookingDate . ' ' .
                            $empBookingEntries['Bookings'][$i]['Note'];
                        $this->objPHPExcel->getActiveSheet()
                            ->setCellValueByColumnAndRow($singleBookingsColIdx, $globalRow, $singleBooking);
                        $this->objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($singleBookingsColIdx, $globalRow)
                            ->getAlignment()->setWrapText(true);
                    }
                }
            }

            return $globalRow;
        } else {
            $children = $node['children'];
            $count = count($children);
            for ($i = 0; $i < $count; $i++) {
                $childnode = $children[$i];
                $globalRow = $this->recursiveTree(
                    $childnode,
                    $globalRow,
                    $tab,
                    $regEx,
                    $emp_bookings,
                    $availableCols,
                    $showEmpHours,
                    $showBtHours,
                    $nodeTypes,
                    $bookingTypes
                );
            }
            return $globalRow;
        }
    }

    public function exportTable()
    {
        //Get the active sheet
        $objSheet = $this->objPHPExcel->getActiveSheet();

        //Set up page setup
        $this->setUpPageSetup();

        // Report config
        $idx = $this->createHeader('tabelle');

        $data = $this->view->data['DATA'];
        if (empty($data) || false == is_array($data)) {
            return $this->getContentXlsx();
        }

        $sapKeyCol = ($this->view->sapKey == 'skshow');
        $targetHoursCol = ($this->view->targetHours == 'thshow');
        $percentCol = ($this->view->percent == 'pshow');
        $showALHours = ($this->view->alhours == 'show');

        // Table header
        $headerContentTableStyle = $this->getContentHeaderStyle();

        $objSheet->getStyleByColumnAndRow(0, $idx)->applyFromArray($headerContentTableStyle);
        $objSheet->getColumnDimensionByColumn(0)->setAutoSize(true);

        foreach ($data as $item) {
            $isHeader = isset($item['isHeader']) && $item['isHeader'];

            $colIdx = 0;
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($item['name']));
            $colIdx++;

            if ($sapKeyCol) {
                if ($isHeader) {
                    $objSheet->getStyleByColumnAndRow($colIdx, $idx)->applyFromArray($headerContentTableStyle);
                    $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getFont()->setSize(8);
                }
                $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($item['sapKey']));
                $colIdx++;
            }

            if ($isHeader) {
                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->applyFromArray($headerContentTableStyle);
                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getFont()->setSize(8);
                $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($item['total']));
            } else {
                $value = $this->formatTableHourValue($item['total'], $showALHours);
                if ($showALHours && $value != "") {
                    $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($value));
                } else {
                    if ($value != "") {
                        $objSheet->setCellValueByColumnAndRow($colIdx, $idx, floatval($value));
                    }
                    $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getNumberFormat()->setFormatCode($this
                        ->numberFormat);
                }
            }
            $colIdx++;
            if ($targetHoursCol) {
                if ($isHeader) {
                    $objSheet->getStyleByColumnAndRow($colIdx, $idx)->applyFromArray($headerContentTableStyle);
                    $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getFont()->setSize(8);
                    $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($item['targetEffort']));
                } else {
                    if ($item['targetEffort'] > 0) {
                        $objSheet->setCellValueByColumnAndRow($colIdx, $idx, floatval($item['targetEffort']));
                    }
                    $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getNumberFormat()->setFormatCode($this
                        ->numberFormat);
                }
                $colIdx++;
            }
            if ($percentCol) {
                if ($isHeader) {
                    $objSheet->getStyleByColumnAndRow($colIdx, $idx)->applyFromArray($headerContentTableStyle);
                    $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getFont()->setSize(8);
                    $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($item['percent']));
                } else {
                    $objSheet->setCellValueByColumnAndRow($colIdx, $idx, floatval($item['percent']) / 100);
                    $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getNumberFormat()->setFormatCode($this
                        ->percentFormat);
                }
                $colIdx++;
            }

            // Iterate through x-axis data
            foreach ($item['xAxisData'] as $xAxisItem) {
                if ($isHeader) {
                    $objSheet->getStyleByColumnAndRow($colIdx, $idx)->applyFromArray($headerContentTableStyle);
                    $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getFont()->setSize(8);
                    $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getAlignment()->setWrapText(true);
                    $objSheet->getColumnDimensionByColumn($colIdx)->setAutoSize(true);
                    $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($this
                        ->formatTableHeaderValue($xAxisItem)));
                } else {
                    $value = $this->formatTableHourValue($xAxisItem, $showALHours);
                    if ($showALHours && $value != "") {
                        $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($value));
                    } else {
                        if ($value != "") {
                            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, floatval($value));
                        }
                        $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getNumberFormat()->setFormatCode($this
                            ->numberFormat);
                    }
                }
                $colIdx++;
            }

            $idx++;
        }

        return $this->getContentXlsx();
    }

    public function exportMonthEnd()
    {
        if ($this->view->data['REPORT_INFO']['FLAGS']['ONE_TAB_PER_COMPANY'] === '1' && !empty($this->view->data['DATA'])) {
            $companyData = [];

            foreach ($this->view->data['DATA'] as $employeeData) {
                $companyData[$employeeData['COMPANY']][] = $employeeData;
            }
            ksort($companyData);

            //removes automatically created default excel worksheet
            $this->objPHPExcel->removeSheetByIndex(0);

            foreach ($companyData as $company => $data) {
                $activeSheet = $this->objPHPExcel->createSheet();
                $activeSheet->setTitle($company);
                $this->objPHPExcel->setActiveSheetIndexByName($company);

                $this->generateMonthEndExport($activeSheet, $data);
            }
        } else {
            $this->generateMonthEndExport($this->objPHPExcel->getActiveSheet(), $this->view->data['DATA']);
        }

        return $this->getContentXlsx();
    }

    private function generateMonthEndExport($activeSheet, $data)
    {
        $styleBorder = $this->getBorderTableStyle();
        $styleSummary = $this->getStyleSummary();

        // Report Config
        $idx = $this->createHeader('monthEndReport');
        $tableHeaderIdx = $idx;

        $periodDate = new Zend_Date($this->view->data['REPORT_INFO']['PERIODFROM']);
        $currDate = new Zend_Date();
        $currDate->setDay(1);

        $extraCols = $this->view->data['REPORT_INFO']['EXTRA_COLS'];
        $extraColsTrans = $this->view->data['REPORT_INFO']['EXTRA_COLS_TRANSLATIONS'];

        $flags = $this->view->data['REPORT_INFO']['FLAGS'];

        $showMonths = $this->view->data['REPORT_INFO']['MONTHS'] > 1;
        $currOrFutureMonth = $periodDate->compareDate($currDate, 'dd.MM.yyyy') == -1 ? false : true;

        $keyToColIdx = array();
        $colIdx = 0;

        //Set up page setup
        $this->setUpPageSetup();

        if ($showMonths) {
            $keyToColIdx['month'] = $colIdx;
            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, ucfirst($this->view
                ->escape($this->translate->translate('month'))));
        }

        $keyToColIdx['lastName'] = $colIdx;
        $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, ucfirst($this->view
            ->escape($this->translate->translate('lastName'))));

        $keyToColIdx['firstName'] = $colIdx;
        $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('firstName')));

        $keyToColIdx['mitarbeiter'] = $colIdx;
        $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('mitarbeiter')));

        //Header for extra columns
        foreach ($extraColsTrans as $column) {
            $keyToColIdx[$column["NAME"]] = $colIdx;
            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
                ->escape($column["NAME"]));
        }

        $keyToColIdx['actualHrs'] = $colIdx;
        $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('actualHrs')));

        $keyToColIdx['payHrsRegular'] = $colIdx;
        $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('payHrsRegular')));

        $keyToColIdx['payHrsNight'] = $colIdx;
        $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('payHrsNight')));

        $keyToColIdx['payHrsSunday'] = $colIdx;
        $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('payHrsSunday')));

        $keyToColIdx['payOvertimeSurcharge'] = $colIdx;
        $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx,
            $this->view->escape($this->translate->translate('payOvertimeSurcharge')));

        $keyToColIdx['homeOfficeDays'] = $colIdx;
        $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx,
            $this->view->escape($this->translate->translate('homeOfficeDays')));

        $keyToColIdx['hrsChange'] = $colIdx;
        $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('hrsChange')));

        if ($flags['ONE_LINE_PER_EMPLOYEE'] != 1) {
            $keyToColIdx['hrsChangeReason'] = $colIdx;
            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view->escape($this->translate
                    ->translate('reason')));
        }

        $keyToColIdx['holidayChange'] = $colIdx;
        $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('holidayChange')));

        if ($flags['ONE_LINE_PER_EMPLOYEE'] != 1) {
            $keyToColIdx['holidayChangeReason'] = $colIdx;
            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view->escape($this->translate
                    ->translate('reason')));
        }

        $keyToColIdx['holidayCurrentMonth'] = $colIdx;
        $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('holidayCurrentMonth')));

        if (false == $currOrFutureMonth) {
            $keyToColIdx['remainingHours'] = $colIdx;
            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view->escape($this->translate
                    ->translate('remainingHours')));
        }

        $keyToColIdx['remainingHoliday'] = $colIdx;
        $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('remainingHoliday')));

        $keyToColIdx['comment'] = $colIdx;
        $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('comment')));

        // Columns width
        foreach ($keyToColIdx as $colIdx) {
            $activeSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx))
                ->setWidth(20);
        }

        $colsLength = count($keyToColIdx);
        $lastColumnIndex = $colsLength - 1;
        // Table header
        $styleTableHead = $this->getContentHeaderStyle();
        $activeSheet->getStyle('A' . $tableHeaderIdx . ':' . PHPExcel_Cell::stringFromColumnIndex($lastColumnIndex) .
                $tableHeaderIdx)
            ->applyFromArray($styleTableHead);

        $idx++;
        $dataIdx = $idx;

        if (empty($data) || false == is_array($data)) {
            return $this->getContentXlsx();
        }

        foreach ($data as $item) {
            $activeSheet->getStyle('A' . $idx . ':' .
                PHPExcel_Cell::stringFromColumnIndex($lastColumnIndex) . $idx)->applyFromArray($styleBorder);

            $colIdx = 0;
            if ($showMonths) {
                $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
                    ->escape($item['MONTH']));
            }

            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
                ->escape($item['LAST_NAME']));

            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
                ->escape($item['FIRST_NAME']));

            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
                ->escape($item['SHORTCUT']));

            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this
                ->view->escape(isset($item['COMPANY']) ? $item['COMPANY'] : ""));

            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this
                ->view->escape(isset($item['DIVISION']) ? $item['DIVISION'] : ""));

            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this
                ->view->escape(isset($item['HOLIDAY_MANAGER']) ? $item['HOLIDAY_MANAGER'] : ""));

            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this
                ->view->escape(isset($item['STAFF_MANAGER']) ? $item['STAFF_MANAGER'] : ""));

            $activeSheet->setCellValueByColumnAndRow($colIdx, $idx, str_replace('.', ',', $item['HOURS_PER_WEEK'] ?? ""));
            $hoursPerWeekColIdx = $colIdx++;
            $activeSheet->getStyleByColumnAndRow($hoursPerWeekColIdx, $idx)->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $activeSheet->getStyleByColumnAndRow($hoursPerWeekColIdx, $idx)->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

            $activeSheet->setCellValueByColumnAndRow(
                $colIdx,
                $idx,
                isset($item['HOLIDAY']) ? intval($item['HOLIDAY']) : ""
            );
            $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

            $activeSheet->getStyleByColumnAndRow($colIdx, $idx)->getNumberFormat()
                ->setFormatCode(self::Date_FORMAT);
            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
                ->escape(isset($item['EMP_ENTRY_DATE']) ? $this->getDateCellValue(new Zend_Date($item['EMP_ENTRY_DATE'], 'dd.MM.YYYY')) : ''));
            $activeSheet->getStyleByColumnAndRow($colIdx, $idx)->getNumberFormat()
                ->setFormatCode(self::Date_FORMAT);
            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
                ->escape(isset($item['EMP_LEAVING_DATE']) ? $this->getDateCellValue(new Zend_Date($item['EMP_LEAVING_DATE'], 'dd.MM.YYYY')) : ''));
            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
                ->escape(isset($item['STATUS']) ? $item['STATUS'] : ""));
            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
                ->escape(isset($item['SICK_LEAVE_DAYS']) ? $item['SICK_LEAVE_DAYS'] : ""));
            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
                ->escape(isset($item['TAKEN_HOLIDAYS']) ? $item['TAKEN_HOLIDAYS'] : ""));
            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
                ->escape(isset($item['ACCEPTED_HOLIDAYS']) ? $item['ACCEPTED_HOLIDAYS'] : ""));
            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
                ->escape(isset($item['BUSINESS_KILOMETERS']) ? $item['BUSINESS_KILOMETERS'] : ""));
            $activeSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view
                ->escape(isset($item['ACT_OVERTIME']) ? $item['ACT_OVERTIME'] : ""));
            $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                ->setFormatCode($this->numberFormat);
            $activeSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
                ->escape(isset($item['OFFICE_DAYS']) ? $item['OFFICE_DAYS'] : ""));


            $activeSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view
                ->escape(isset($item['ACCOUNTABLE_HOURS']) ? $item['ACCOUNTABLE_HOURS'] : ""));
            $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                ->setFormatCode($this->numberFormat);

            $activeSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view
                ->escape(isset($item['NOT_ACCOUNTABLE_HOURS']) ? $item['NOT_ACCOUNTABLE_HOURS'] : ""));
            $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                ->setFormatCode($this->numberFormat);

            $activeSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view
                ->escape(isset($item['ACCOUNTABLE_HOURS_PERC']) ? $item['ACCOUNTABLE_HOURS_PERC'] : ""));
            $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);

            $activeSheet->setCellValueByColumnAndRow($colIdx, $idx, $item['ACT_WORKED_HOURS']);
            $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                ->setFormatCode($this->numberFormat);

            $activeSheet->setCellValueByColumnAndRow(
                $colIdx,
                $idx,
                ($item['REGULAR_HOURS_TO_PAY'] > 0 ? floatval($item['REGULAR_HOURS_TO_PAY']) : '')
            );
            $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                ->setFormatCode($this->numberFormat);

            $activeSheet->setCellValueByColumnAndRow(
                $colIdx,
                $idx,
                ($item['NIGHT_HOURS_TO_PAY'] > 0 ? floatval($item['NIGHT_HOURS_TO_PAY']) : '')
            );
            $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                ->setFormatCode($this->numberFormat);

            $activeSheet->setCellValueByColumnAndRow(
                $colIdx,
                $idx,
                ($item['HOLIDAY_HOURS_TO_PAY'] > 0 ? floatval($item['HOLIDAY_HOURS_TO_PAY']) : '')
            );
            $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                ->setFormatCode($this->numberFormat);

            $activeSheet->setCellValueByColumnAndRow(
                $colIdx,
                $idx,
                $this->view->escape(isset($item['PAY_OVERTIME_SURCHARGE']) ? $item['PAY_OVERTIME_SURCHARGE'] : "")
            );
            $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

            $activeSheet->setCellValueByColumnAndRow(
                $colIdx,
                $idx,
                $this->view->escape(isset($item['HOME_OFFICE_DAYS']) ? $item['HOME_OFFICE_DAYS'] : "")
            );
            $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

            $activeSheet->setCellValueByColumnAndRow(
                $colIdx,
                $idx,
                ($item['CURRENT_HOURS_MONTH_CHANGE'] != 0 ? floatval($item['CURRENT_HOURS_MONTH_CHANGE']) : '')
            );
            $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                ->setFormatCode($this->numberFormat);

            if ($flags['ONE_LINE_PER_EMPLOYEE'] != 1) {
                $activeSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view
                    ->escape($item['HOURS_CHANGE_REASON']));
                $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getAlignment()
                    ->setWrapText(true);
            }

            // if-condition is necessary for a correct presentation
            if ($item['CURRENT_HOLIDAY_MONTH_CHANGE'] != 0) {
                $activeSheet->setCellValueByColumnAndRow(
                    $colIdx,
                    $idx,
                    intval($item['CURRENT_HOLIDAY_MONTH_CHANGE'])
                );
                $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            } else {
                $colIdx++;
            }

            if ($flags['ONE_LINE_PER_EMPLOYEE'] != 1) {
                $activeSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view
                    ->escape($item['HOLIDAY_CHANGE_REASON']));
                $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getAlignment()
                    ->setWrapText(true);
            }


            $activeSheet->setCellValueByColumnAndRow($colIdx, $idx, floatval($item['HOLIDAY_DAYS']));
            $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

            if (false == $currOrFutureMonth) {
                $activeSheet->setCellValueByColumnAndRow($colIdx, $idx, floatval($item['REMAINING_HOURS']));
                $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                    ->setFormatCode($this->numberFormat);
            }

            $activeSheet->setCellValueByColumnAndRow($colIdx, $idx, floatval($item['REMAINING_HOLIDAY_DAYS']));
            $activeSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()
                ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

            $idx++;
        }

        $startPosExtraCols = $keyToColIdx['mitarbeiter'] + 1;

        $sumLabelColCounter = 0;
        $sumLabelCol = 0;

        if ($flags['ONE_TAB_PER_COMPANY'] === '1') {
            $extraCols[] = AdditionalCol::MONTH_END_COMPANY;
        }

        //hide extra columns if they are not selected and determine last visible additional column
        foreach ($extraColsTrans as $i => $col) {
            $sumLabelColCounter++;
            if (!in_array($col['ID'], $extraCols)) {
                $activeSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($startPosExtraCols + $i))
                    ->setVisible(false);
            } else {
                $sumLabelCol = $sumLabelColCounter;
            }
        }

        $activeSheet->setCellValueByColumnAndRow($sumLabelCol + $startPosExtraCols - 1, $idx, $this->view->escape($this
                    ->translate->translate('monthEndSum') . ':'));
        $activeSheet->getStyleByColumnAndRow($sumLabelCol + $startPosExtraCols - 1, $idx)
            ->applyFromArray($styleSummary);

        $sumCols = array('actualHrs', 'payHrsRegular', 'payHrsNight', 'payHrsSunday', 'homeOfficeDays');

        foreach ($sumCols as $sumColIdx) {
            $colChar = PHPExcel_Cell::stringFromColumnIndex($keyToColIdx[$sumColIdx]);

            $activeSheet
                ->setCellValue($colChar . $idx, '=SUM(' . $colChar . $dataIdx . ':' . $colChar . ($idx - 1) . ')');
            $activeSheet->getStyleByColumnAndRow($keyToColIdx[$sumColIdx], $idx)->getNumberFormat()
                ->setFormatCode($this->numberFormat);
        }

        return;
    }

    public function exportBtModifications()
    {
        $styleBorder = $this->getBorderTableStyle();

        //Get the active sheet
        $objSheet = $this->objPHPExcel->getActiveSheet();

        //Set up page setup
        $this->setUpPageSetup();

        // Report Config
        $idx = $this->createHeader('btModifications');

        // Columns width
        $objSheet->getColumnDimension('C')->setWidth(65);
        $objSheet->getColumnDimension('D')->setWidth(10);
        $objSheet->getColumnDimension('E')->setWidth(6);
        $objSheet->getColumnDimension('F')->setWidth(15);
        $objSheet->getColumnDimension('G')->setWidth(22);
        $objSheet->getColumnDimension('H')->setWidth(20);

        // Table header
        $styleTableHead = $this->getContentHeaderStyle();
        $objSheet->getStyle('A' . $idx . ':H' . $idx)->applyFromArray($styleTableHead);

        $objSheet->setCellValueByColumnAndRow(0, $idx, $this->view->escape($this->translate
            ->translate('modification')));

        $objSheet->setCellValueByColumnAndRow(1, $idx, $this->view->escape($this->translate
                ->translate('zeit') . '/' . $this->translate->translate('datum')));

        $objSheet->setCellValueByColumnAndRow(2, $idx, $this->view->escape($this->translate
            ->translate('nodeName')));

        $objSheet->setCellValueByColumnAndRow(3, $idx, $this->view->escape($this->translate
            ->translate('nodeType')));

        $objSheet->setCellValueByColumnAndRow(4, $idx, $this->view->escape($this->translate
            ->translate('leaf')));

        $objSheet->setCellValueByColumnAndRow(5, $idx, $this->view->escape($this->translate
            ->translate('authorizedBy')));

        $objSheet->setCellValueByColumnAndRow(6, $idx, $this->view->escape($this->translate
            ->translate('sapKeyCostUnit')));

        $objSheet->setCellValueByColumnAndRow(7, $idx, $this->view->escape($this->translate
            ->translate('sapKeyCostCenter')));

        $idx++;

        $data = $this->view->data['DATA'];
        if (empty($data) || false == is_array($data)) {
            return $this->getContentXlsx();
        }

        foreach ($data as $item) {
            $objSheet->getStyle('A' . $idx . ':H' . $idx)->applyFromArray($styleBorder);
            $objSheet->setCellValueByColumnAndRow(0, $idx, $this->view
                ->escape($item['MODIFICATION'] == 'locked' ? $this->translate->translate('locked'): $this->translate
                    ->translate('neu')));

            $objSheet->setCellValueByColumnAndRow(1, $idx, $this->view->escape($item['DATETIME']));

            $objSheet->setCellValueByColumnAndRow(2, $idx, $this->view->escape($item['NODE_NAME']));

            $objSheet->setCellValueByColumnAndRow(3, $idx, $item['NODE_TYPE']);

            $objSheet->setCellValueByColumnAndRow(4, $idx, $this->view->escape($item['IS_LEAF'] == 1 ? $this->translate
                ->translate('ja'): $this->translate->translate('nein')));

            $objSheet->setCellValueByColumnAndRow(5, $idx, $item['AUTHORIZED']);

            $objSheet->setCellValueByColumnAndRow(6, $idx, $item['SAP_KEY_COST_OBJECT']);

            $objSheet->setCellValueByColumnAndRow(7, $idx, $item['SAP_KEY_COST_CENTER']);

            $idx++;
        }

        return $this->getContentXlsx();
    }

    /**
     *  Export 'Absence Calendar' report.
     *
     * @author Steffen Glatzel <gla@isogmbh.de>
     */
    public function exportAbsenceCalendar()
    {
        // Get the active sheet
        $objSheet = $this->objPHPExcel->getActiveSheet();

        //Set up page setup
        $this->setUpPageSetup();

        // Report Config
        $idx = $this->createHeader('absenceCalendar');
        $dateFrom = $this->view->data['REPORT_INFO']['PERIODFROM'];
        $dateUntil = $this->view->data['REPORT_INFO']['PERIODUNTIL'];
        $employeeAsGroups = $this->view->data['REPORT_INFO']['EMPLOYEES_AS_GROUPS'];
        $daysBetweenDates = $this->getElapsed($dateFrom, $dateUntil);
        $totalDays = $daysBetweenDates + 1;
        $daysForCW = $this->getDaysForRenderCalenderWeek($dateFrom, $totalDays);

        $data = $this->view->data['DATA']['employeeAbsences'];
        $publicHolidays = $this->view->data['DATA']['publicHolidays'];
        $absenceCategories = array();
        foreach ($this->view->data['DATA']['absenceCategories'] as $category) {
            $absenceCategories[$category['ID']] = $category;
        }

        $startColIndex = 1; //Start Column for render absences
        $colIndexForCW = array();

        // Styles for cells
        $tableHeaderStyle = array(
            'font' => array(
                'bold' => true
            ),
            'alignment' => array(
                'horizontal'    => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical'        => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        // Styles for the table body and day numbers
        $tableBodyStyle = array(
            'font' => array(
                'size' => 8
            ),
            'alignment' => array(
                'horizontal'    => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical'        => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        //Array which contains the cells attributes for the short legend
        $legendShort = array(
            array('column' => 0, 'row' => $idx, 'text' => $this->view->escape($this->translate
                    ->translate('legendShort')) . ': ', 'color' => '', 'bold' => true),
            array('column' => 1, 'row' => $idx, 'text' => $this->view->escape($this->translate
                ->translate('publicHolidayShortcut')), 'color' => '', 'bold' => true),
            array('column' => 3, 'row' => $idx, 'text' => $this->view->escape($this->translate
                ->translate('publicHoliday')), 'color' => '', 'bold' => false),
            array(
                'column' => 9,
                'row' => $idx,
                'text' => '', 'color' => $absenceCategories[2]['COLOR'],
                'bold' => false
            ),
            array(
                'column' => 10,
                'row' => $idx,
                'text' => $this->view->escape($absenceCategories[2]['NAME']), 'color' => '',
                'bold' => false
            ),
            array(
                'column' => 14,
                'row' => $idx,
                'text' => '', 'color' => $absenceCategories[5]['COLOR'],
                'bold' => false
            ),
            array(
                'column' => 15,
                'row' => $idx,
                'text' => $this->view->escape($absenceCategories[5]['NAME']), 'color' => '',
                'bold' => false
            ),
            array(
                'column' => 21,
                'row' => $idx,
                'text' => '', 'color' => $absenceCategories[1]['COLOR'],
                'bold' => false
            ),
            array(
                'column' => 22,
                'row' => $idx,
                'text' => $this->view->escape($absenceCategories[1]['NAME']), 'color' => '',
                'bold' => false
            ),
        );

        $this->renderCellsWithStyleOptions($legendShort, $objSheet);

        $idx += 2;

        //background color for table header
        $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex(0) . $idx . ':'
            .  PHPExcel_Cell::stringFromColumnIndex($totalDays) . ($idx + 2))
            ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()->setARGB('C3C3C3');


        $colMonth = new Zend_Date($dateFrom);
        $cwIndex = 1;

        //First border of calendar week column at the left side
        $objSheet->setCellValueByColumnAndRow(0, $idx, $this->view->escape($this->translate->translate('month')));
        $objSheet->getStyleByColumnAndRow(0, $idx)->getFont()->setBold(true);
        $objSheet->getStyleByColumnAndRow($cwIndex, $idx)->getBorders()->getLeft()
            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //render month columns for table header
        do {
            if ($cwIndex != 1) {
                $firstDayofMonth = $colMonth->get(Zend_Date::TIMESTAMP);
            }

            $daysInMonth = $this->getDaysInMonth($colMonth->toString(Zend_Date::MONTH_SHORT), $colMonth
                ->toString(Zend_Date::YEAR_SHORT));
            $currentDayNumber = $colMonth->toString(Zend_Date::DAY);

            $colMonth->set($daysInMonth, Zend_Date::DAY);
            if ($cwIndex == 1) {
                $elapsedDays = $this->getElapsedWithValidDatePeriod($dateFrom, $colMonth
                    ->toString(Zend_Date::TIMESTAMP), $dateFrom, $dateUntil);
            } else {
                $elapsedDays = $this->getElapsedWithValidDatePeriod($firstDayofMonth, $colMonth
                    ->toString(Zend_Date::TIMESTAMP), $dateFrom, $dateUntil);
            }
            //set month value
            $objSheet->setCellValueByColumnAndRow($cwIndex, $idx, $colMonth->toString('MMM yyyy', $this->view->locale));

            //merge cells of month
            $objSheet->mergeCellsByColumnAndRow($cwIndex, $idx, $cwIndex + $elapsedDays, $idx)
                ->getStyleByColumnAndRow($cwIndex + $elapsedDays)->getBorders()->getRight()
                ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            //styles
            $objSheet->getStyleByColumnAndRow($cwIndex, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow($cwIndex, $idx)->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($cwIndex) . $idx . ':'
                .  PHPExcel_Cell::stringFromColumnIndex($cwIndex + $elapsedDays) . $idx)
                ->getBorders()->getRight()
                ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);


            $cwIndex = $cwIndex + $elapsedDays + 1;
            $colMonth->add(1, Zend_Date::DAY);
        } while ($colMonth->toString(Zend_Date::TIMESTAMP) <= $dateUntil);

        ++$idx;
        $startRowIndexCW = $idx;

        //render week columns for table header
        $dateForColumn = new Zend_Date($dateFrom);
        $cwIndex = 1;

        $objSheet->setCellValueByColumnAndRow(0, $idx, $this->view->escape($this->translate
            ->translate('calendarWeekShortcut')));
        $objSheet->getStyleByColumnAndRow(0, $idx)->getFont()->setBold(true);

        for ($i = 0; $i < 3; $i++) {
            if (!empty($daysForCW[$i])) {
                for ($k = 0; $k < $daysForCW[$i]; $k++) {
                    $objSheet->setCellValueByColumnAndRow($cwIndex, $idx, $this->view->escape($this->translate
                            ->translate('calendarWeekShortcut'))
                        . ' ' . $dateForColumn->toString(Zend_Date::WEEK));
                    $objSheet->getStyleByColumnAndRow($cwIndex, $idx)->applyFromArray($tableHeaderStyle);

                    if ($i == 0) {
                        $colIndex = $cwIndex + $daysForCW[$i] - 1;
                    } elseif ($i == 1) {
                        $colIndex = $cwIndex + 6;
                    } elseif ($i == 2) {
                        $colIndex = $cwIndex + $daysForCW[$i] - 1;
                    }

                    $objSheet->mergeCellsByColumnAndRow($cwIndex, $idx, $colIndex, $idx);

                    if ($i == 0) {
                        $cwIndex += $daysForCW[0];
                    } elseif ($i == 1) {
                        $cwIndex += 7;
                    }

                    //store the column index for the calender week borders.
                    // After the data was rendered the cw borders wil be displayed
                    $colIndexForCW[] = $colIndex;

                    $dateForColumn = $dateForColumn->add('1', Zend_Date::WEEK);
                    //the first and last array field runs only one time, because the values of these fields are days
                    if ($i != 1) {
                        break;
                    }
                }
            }
        }

        //render day as number for table header
        $dateString = new Zend_Date($dateFrom);
        $idx++;

        $objSheet->setCellValueByColumnAndRow(0, $idx, $this->view->escape($this->translate->translate('day')));
        $objSheet->getStyleByColumnAndRow(0, $idx)->getFont()->setBold(true);

        for ($i = 0; $i <= $daysBetweenDates; ++$i) {
            $objSheet->setCellValueByColumnAndRow($i + 1, $idx, $dateString->toString('dd'));
            $objSheet->getColumnDimensionByColumn($i + 1)->setWidth(2.5);
            $dateString = $dateString->add('1', Zend_Date::DAY);
            $objSheet->getStyleByColumnAndRow($i + 1, $idx)->applyFromArray($tableBodyStyle);
        }

        $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($startColIndex - 1) . $idx . ':'
            . PHPExcel_Cell::stringFromColumnIndex($totalDays)
            . $idx)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        $idx++;

        // return only header and calendar without data, if there aren't data
        if (empty($data) || false == is_array($data)) {
            //render calendar week border
            foreach ($colIndexForCW as $cw) {
                $columnIndexForCwBorder = PHPExcel_Cell::stringFromColumnIndex($cw);
                $objSheet->getStyle($columnIndexForCwBorder . $startRowIndexCW . ':' . $columnIndexForCwBorder
                    . ($idx - 1))->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            }

            $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($startColIndex) . $startRowIndexCW . ':'
                . PHPExcel_Cell::stringFromColumnIndex($startColIndex)
                . ($idx - 1))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            return $this->getContentXlsx();
        }

        // render absences
        $idx = $this->renderAbsencesOfEmployees(
            $data,
            $idx,
            $dateFrom,
            $dateUntil,
            $totalDays,
            $startColIndex,
            $objSheet,
            $tableBodyStyle,
            $employeeAsGroups,
            $publicHolidays,
            $absenceCategories
        );
        $idx += 2;

        //render calendar week border
        foreach ($colIndexForCW as $cw) {
            $columnIndexForCwBorder = PHPExcel_Cell::stringFromColumnIndex($cw);
            $objSheet->getStyle($columnIndexForCwBorder . $startRowIndexCW . ':' . $columnIndexForCwBorder
                . ($idx - 2))->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        }

        $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($startColIndex) . $startRowIndexCW . ':'
            . PHPExcel_Cell::stringFromColumnIndex($startColIndex)
            . ($idx - 2))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //render after last employee a horizontal border
        $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($startColIndex - 1) . ($idx - 2) . ':'
            . PHPExcel_Cell::stringFromColumnIndex($totalDays)
            . ($idx - 2))->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //Array which contains the cells attributes for the complete legend
        $legendComplete = array(
            array('column' => 0, 'row' => $idx,   'text' => $this->view->escape($this->translate
                    ->translate('legendComplete')) . ': ', 'color' => '', 'bold' => true),
            array('column' => 1, 'row' => $idx,   'text' => $this->view->escape($this->translate
                ->translate('publicHolidayShortcut')), 'color' => '', 'bold' => true),
            array('column' => 3, 'row' => $idx++, 'text' => $this->view->escape($this->translate
                ->translate('publicHoliday')), 'color' => '', 'bold' => false)
        );

        foreach ($absenceCategories as $category) {
            $legendComplete[] = array(
                'column' => 1,
                'row' => $idx,
                'text' => '',
                'color' => $category['COLOR'],
                'striped' => $category['STRIPED'] == 1,
                'bold' => false
            );
            $legendComplete[] = array(
                'column' => 3,
                'row' => $idx++,
                'text' => $category['NAME'],
                'color' => '',
                'bold' => false
            );
        }

        $this->renderCellsWithStyleOptions($legendComplete, $objSheet);

        return $this->getContentXlsx();
    }

    /**
     *  Export 'AccountableHours' report.
     *
     * @author Krzysztof Atlasik <atk@isogmbh.de>
     */
    public function exportAccountableHours()
    {
        $data = $this->view->data;

        $sheet = 0;

        $this->objPHPExcel->setActiveSheetIndex($sheet++)->setTitle($this->translate->translate('overview'));

        //Get the active sheet
        $objSheet = $this->objPHPExcel->getActiveSheet();

        //Set up page setup
        $this->setUpPageSetup();

        $color = 'C3C3C3';

        $idx = $this->createHeader('accountableHours') - 2;

        if (!empty($data["DATA"]['EMPLOYEES'])) {
            //employees
            $this->objPHPExcel->createSheet($sheet);
            $this->objPHPExcel->setActiveSheetIndex($sheet++)->setTitle($this->translate->translate('mitarbeiter'));
            $objSheet = $this->objPHPExcel->getActiveSheet();
            $idx = 1;

            $objSheet->setCellValueByColumnAndRow(0, $idx, $this->translate->translate('shortcut'));
            $objSheet->getStyleByColumnAndRow(0, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(0, $idx)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(1, $idx, $this->translate->translate('name'));
            $objSheet->getStyleByColumnAndRow(1, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(1, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(2, $idx, $this->translate->translate('actual_division'));
            $objSheet->getStyleByColumnAndRow(2, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(2, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(3, $idx, ucfirst($this->translate->translate('active')));
            $objSheet->getStyleByColumnAndRow(3, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(3, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(4, $idx, $this->translate->translate('hours_total'));
            $objSheet->getStyleByColumnAndRow(4, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(4, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(5, $idx, $this->translate->translate('hours_holiday'));
            $objSheet->getStyleByColumnAndRow(5, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(5, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(6, $idx, $this->translate->translate('hours_illness'));
            $objSheet->getStyleByColumnAndRow(6, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(6, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(7, $idx, $this->translate->translate('hours_short_time_work'));
            $objSheet->getStyleByColumnAndRow(7, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(7, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(8, $idx, $this->translate->translate('hours_not_accountable'));
            $objSheet->getStyleByColumnAndRow(8, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(8, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(9, $idx, $this->translate->translate('hours_accountable'));
            $objSheet->getStyleByColumnAndRow(9, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(9, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(10, $idx, $this->translate->translate('hours_accountable_percent'));
            $objSheet->getStyleByColumnAndRow(10, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(10, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);

            $objSheet->getColumnDimension("A")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("B")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("C")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("D")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("E")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("F")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("G")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("H")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("I")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("J")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("K")
                ->setAutoSize(true);
            $idx++;

            foreach ($data["DATA"]['EMPLOYEES'] as $employee) {
                $objSheet->setCellValueByColumnAndRow(0, $idx, $employee['MA_KUERZEL']);
                $objSheet->setCellValueByColumnAndRow(1, $idx, $employee['NAME']);
                $objSheet->setCellValueByColumnAndRow(2, $idx, $employee['AKTUELLER_BEREICH']);
                $objSheet->setCellValueByColumnAndRow(3, $idx, $employee['AKTIV'] == 1 ? $this->translate
                    ->translate('ja') : $this->translate->translate('nein'));
                $objSheet->getStyleByColumnAndRow(4, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(4, $idx, $employee['STD_GESAMT']);
                $objSheet->getStyleByColumnAndRow(5, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(5, $idx, $employee['STD_URLAUB']);
                $objSheet->getStyleByColumnAndRow(6, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(6, $idx, $employee['STD_KRANKHEIT']);
                $objSheet->getStyleByColumnAndRow(7, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(7, $idx, $employee['STD_KURZARBEIT']);
                $objSheet->getStyleByColumnAndRow(8, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(8, $idx, $employee['STD_NICHT_ABRECHENBAR']);
                $objSheet->getStyleByColumnAndRow(9, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(9, $idx, $employee['STD_ABRECHENBAR']);
                $objSheet->setCellValueByColumnAndRow(10, $idx, $employee['PROZENT_STD_ABRECHENBAR']);
                $idx++;
            }
        }

        if (!empty($data["DATA"]['DIVISIONS'])) {
            //employees
            $this->objPHPExcel->createSheet($sheet);
            $this->objPHPExcel->setActiveSheetIndex($sheet++)->setTitle($this->translate->translate('division'));
            $objSheet = $this->objPHPExcel->getActiveSheet();
            $idx = 1;

            $objSheet->setCellValueByColumnAndRow(0, $idx, $this->translate->translate('division'));
            $objSheet->getStyleByColumnAndRow(0, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(0, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(1, $idx, $this->translate->translate('hours_total'));
            $objSheet->getStyleByColumnAndRow(1, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(1, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(2, $idx, $this->translate->translate('hours_holiday'));
            $objSheet->getStyleByColumnAndRow(2, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(2, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(3, $idx, $this->translate->translate('hours_illness'));
            $objSheet->getStyleByColumnAndRow(3, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(3, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(4, $idx, $this->translate->translate('hours_short_time_work'));
            $objSheet->getStyleByColumnAndRow(4, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(4, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(5, $idx, $this->translate->translate('hours_not_accountable'));
            $objSheet->getStyleByColumnAndRow(5, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(5, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(6, $idx, $this->translate->translate('hours_accountable'));
            $objSheet->getStyleByColumnAndRow(6, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(6, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(7, $idx, $this->translate->translate('hours_accountable_percent'));
            $objSheet->getStyleByColumnAndRow(7, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(7, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);

            $objSheet->getColumnDimension("A")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("B")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("C")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("D")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("E")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("F")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("G")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("H")
                ->setAutoSize(true);
            $idx++;
            foreach ($data["DATA"]['DIVISIONS'] as $employee) {
                $objSheet->setCellValueByColumnAndRow(0, $idx, $employee['BEREICH']);
                $objSheet->getStyleByColumnAndRow(1, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(1, $idx, $employee['STD_GESAMT']);
                $objSheet->getStyleByColumnAndRow(2, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(2, $idx, $employee['STD_URLAUB']);
                $objSheet->getStyleByColumnAndRow(3, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(3, $idx, $employee['STD_KRANKHEIT']);
                $objSheet->getStyleByColumnAndRow(4, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(4, $idx, $employee['STD_KURZARBEIT']);
                $objSheet->getStyleByColumnAndRow(5, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(5, $idx, $employee['STD_NICHT_ABRECHENBAR']);
                $objSheet->getStyleByColumnAndRow(6, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(6, $idx, $employee['STD_ABRECHENBAR']);
                $objSheet->setCellValueByColumnAndRow(7, $idx, $employee['PROZENT_STD_ABRECHENBAR']);
                $idx++;
            }
        }

        if (!empty($data["DATA"]['COMPANIES'])) {
            //employees
            $this->objPHPExcel->createSheet($sheet);
            $this->objPHPExcel->setActiveSheetIndex($sheet++)->setTitle($this->translate->translate('firma'));
            $objSheet = $this->objPHPExcel->getActiveSheet();
            $idx = 1;

            $objSheet->setCellValueByColumnAndRow(0, $idx, $this->translate->translate('shortcut'));
            $objSheet->getStyleByColumnAndRow(0, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(0, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(1, $idx, $this->translate->translate('firma'));
            $objSheet->getStyleByColumnAndRow(1, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(1, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(2, $idx, $this->translate->translate('hours_total'));
            $objSheet->getStyleByColumnAndRow(2, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(2, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(3, $idx, $this->translate->translate('hours_holiday'));
            $objSheet->getStyleByColumnAndRow(3, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(3, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(4, $idx, $this->translate->translate('hours_illness'));
            $objSheet->getStyleByColumnAndRow(4, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(4, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(5, $idx, $this->translate->translate('hours_short_time_work'));
            $objSheet->getStyleByColumnAndRow(5, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(5, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(6, $idx, $this->translate->translate('hours_not_accountable'));
            $objSheet->getStyleByColumnAndRow(6, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(6, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(7, $idx, $this->translate->translate('hours_accountable'));
            $objSheet->getStyleByColumnAndRow(7, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(7, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);
            $objSheet->setCellValueByColumnAndRow(8, $idx, $this->translate->translate('hours_accountable_percent'));
            $objSheet->getStyleByColumnAndRow(8, $idx)->getFont()->setBold(true);
            $objSheet->getStyleByColumnAndRow(8, $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB($color);

            $objSheet->getColumnDimension("A")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("B")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("C")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("D")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("E")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("F")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("G")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("H")
                ->setAutoSize(true);
            $objSheet->getColumnDimension("I")
                ->setAutoSize(true);
            $idx++;

            foreach ($data["DATA"]['COMPANIES'] as $employee) {
                $objSheet->setCellValueByColumnAndRow(0, $idx, $employee['FIRMENKUERZEL']);
                $objSheet->setCellValueByColumnAndRow(1, $idx, $employee['FIRMA']);
                $objSheet->getStyleByColumnAndRow(2, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(2, $idx, $employee['STD_GESAMT']);
                $objSheet->getStyleByColumnAndRow(3, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(3, $idx, $employee['STD_URLAUB']);
                $objSheet->getStyleByColumnAndRow(4, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(4, $idx, $employee['STD_KRANKHEIT']);
                $objSheet->getStyleByColumnAndRow(5, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(5, $idx, $employee['STD_KURZARBEIT']);
                $objSheet->getStyleByColumnAndRow(6, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(6, $idx, $employee['STD_NICHT_ABRECHENBAR']);
                $objSheet->getStyleByColumnAndRow(7, $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objSheet->setCellValueByColumnAndRow(7, $idx, $employee['STD_ABRECHENBAR']);
                $objSheet->setCellValueByColumnAndRow(8, $idx, $employee['PROZENT_STD_ABRECHENBAR']);
                $idx++;
            }
        }

        $this->objPHPExcel->setActiveSheetIndex(0);

        return $this->getContentXlsx();
    }

    /**
     *  Export 'Status' report.
     *
     * @author Krzysztof Atlasik <atk@isogmbh.de>
     */
    public function exportStatus()
    {
        $objSheet = $this->objPHPExcel->getActiveSheet();

        //Set up page setup
        $this->setUpPageSetup();

        $lang = $this->translate;

        // prepare style arrays
        $headerStyle = $this->getReportConfigStyle();
        $tableHeaderStyle = $this->getContentHeaderStyle();
        $tableHeaderStyle['alignment']['horizontal'] = PHPExcel_Style_Alignment::HORIZONTAL_CENTER;

        $bordersStyle = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                )
            )
        );

        $bordersOverlayStyle = array(
            'borders' => array(
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
                'left' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
                'right' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                )
            )
        );

        $thickBordersStyle = array(
            'borders' => array(
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array('argb' => '00000000'),
                ),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array('argb' => '00000000'),
                ),
                'left' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array('argb' => '00000000'),
                ),
                'right' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array('argb' => '00000000'),
                )
            )

        );

        $darkHeaderStyle = array_merge($bordersStyle, array(
            'font' => array(
                'bold' => true,
                'color' => array(
                    'argb' => '00000000'
                )
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => '93939393'
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        ));

        $bigDarkHeaderStyle =  array_merge($darkHeaderStyle, array(
            'font'    => array(
                'size'      => 14,
                'bold'      => true,
            )
        ));

        $bold = array(
            'font'    => array(
                'bold'      => true,
            )
        );

        $headerStyle = array_merge($bordersStyle, array(
            'font' => array(
                'bold' => true,
                'color' => array(
                    'argb' => '00000000'
                )
            ), 'alignment' => array(
                'vertical' => 'top',
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap' => true
            )
        ));

        $columnHeaderStyle =  array_merge($bordersStyle, array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => 'D3D3D3D3'
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical'    => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap'        => true
            ),
            'font' => array(
                'bold' => true
            )
        ));

        //Creating report header
        $idx = $this->createHeader('projectStatus') - 2;

        $nodes = $this->view->data['REPORT_INFO']['NODES']['start'];
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $lang
                ->translate('singleNodeStartPart') . ":");
        $objSheet->getStyle('A8')->getFont()->setBold(true);
        $objSheet->getStyle('A9')->getFont()->setBold(true);
        $objSheet->getStyle('A10')->getFont()->setBold(true);
        $objSheet->getStyle('A11')->getFont()->setBold(true);
        $objSheet->getStyle('A12')->getFont()->setBold(true);
        $objSheet->getStyle('A13')->getFont()->setBold(true);
        $this->objPHPExcel->getActiveSheet()
            ->setCellValueByColumnAndRow(1, $idx++, !empty($nodes) ? join($nodes, ', ') : $lang
                ->translate('notSelected'));

        $projects = array();

        foreach ($this->view->data['REPORT_INFO']['NODES']['project'] as $project) {
            $name = $project['name'];
            if ($project['with_next_lvl'] === true) {
                $name .= ' (' . ($lang->translate('singleNodeAtNextLevel')) . ')';
            }
            array_push($projects, $name);
        }

        $informationArray = $this->view->data['INFORMATION'];
        $usedInformationArray = $this->view->data['REPORT_INFO']['USED_INFORMATION'];

        if ($this->view->data['REPORT_INFO']['DISPLAY_FIGURES'] == 0) {
            $displayFigures = $lang->translate('displayFiguresOncePerPeriod');
        } else {
            $displayFigures = $lang->translate('displayFiguresPerMonth');
        }

        if ($this->view->data['REPORT_INFO']['DISPLAY_LINES'] == 0) {
            $displayLines = $lang->translate('onlyIfHoursAmountsInSelectedGeneralPeriod_short');
        } elseif ($this->view->data['REPORT_INFO']['DISPLAY_LINES'] == 1) {
            $displayLines = $lang->translate('onlyIfHoursAmountsInSelectedPeriod_short');
        } else {
            $displayLines = $lang->translate('onlyIfHoursInSelectedPeriod_short');
        }

        if ($this->view->data['REPORT_INFO']['WITH_LOCKED_NODES'] == LockedNodes::WITH_LOCKED_NODES) {
            $withLockedNodes = $lang->translate('withLockedNodes');
        } else {
            $withLockedNodes = $lang->translate('withoutLockedNodes');
        }

        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $lang
                ->translate('singleNodeDistinctRow') . ":");
        $this->objPHPExcel->getActiveSheet()
            ->setCellValueByColumnAndRow(1, $idx++, !empty($projects) ? join($projects, ', ') : $lang
                ->translate('notSelected'));

        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $lang
                ->translate('configInformationToBeDisplayed') . ":");
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(
            1,
            $idx++,
            !empty($usedInformationArray) ? join($usedInformationArray, ', ') : $lang->translate('notSelected')
        );

        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $lang
                ->translate('configDisplayFigures') . ":");
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx++, $displayFigures);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $lang
                ->translate('configDisplayLines') . ":");
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx++, $displayLines);

        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx, $lang
                ->translate('configLockedNodes') . ":");
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx++, $withLockedNodes);

        //Left side of nodes generations

        $idx += 1;

        $objSheet->mergeCellsByColumnAndRow(0, $idx, 0, $idx + 4);
        $objSheet->getStyle('A' . $idx . ':A' . ($idx + 4))->applyFromArray($bigDarkHeaderStyle);
        $objSheet->setCellValueByColumnAndRow(0, $idx, $lang->translate('nodes'));
        $objSheet->getColumnDimension('A')->setAutoSize(false);
        $objSheet->getColumnDimension('A')->setWidth(34);

        $objSheet->mergeCellsByColumnAndRow(1, $idx, 1, $idx + 4);
        $objSheet->getStyle('B' . $idx . ':B' . ($idx + 4))->applyFromArray($bigDarkHeaderStyle);
        $objSheet->setCellValueByColumnAndRow(1, $idx, $lang->translate('information'));
        $objSheet->getColumnDimension('B')->setWidth(22);

        $objSheet->mergeCellsByColumnAndRow(2, $idx, 2, $idx + 4);
        $objSheet->getStyle('C' . $idx . ':C' . ($idx + 4))
            ->applyFromArray($darkHeaderStyle);
        $objSheet->setCellValueByColumnAndRow(2, $idx, $lang->translate('company'));
        $objSheet->getColumnDimension('C')->setWidth(10);

        $objSheet->mergeCellsByColumnAndRow(3, $idx, 3, $idx + 4);
        $objSheet->getStyle('D' . $idx . ':D' . ($idx + 4))->applyFromArray($darkHeaderStyle);
        $objSheet->setCellValueByColumnAndRow(3, $idx, $lang->translate('plannedEffort'));
        $objSheet->getColumnDimension('D')->setWidth(14);
        $objSheet->getColumnDimension('D')->setVisible(false);

        $objSheet->mergeCellsByColumnAndRow(4, $idx, 4, $idx + 4);
        $objSheet->getStyle('E' . $idx . ':E' . ($idx + 4))->applyFromArray($darkHeaderStyle);
        $objSheet->getStyle('E' . $idx . ':E' . ($idx + 4))->getAlignment()->setWrapText(true);
        $objSheet->setCellValueByColumnAndRow(4, $idx, str_replace('\n', PHP_EOL, $lang->translate('plannedEffort')));
        $objSheet->getColumnDimension('E')->setWidth(20);
        $objSheet->getCommentByColumnAndRow(4, $idx)->setWidth(280)->getText()
            ->createTextRun(str_replace('\n', PHP_EOL, $lang->translate("plannedEffortComment")));


        $idx += 5;
        $offset = 8; //number of columns in months headers
        $nodesStartIdx = $idx; //remember position where dumping nodes started
        $usedMonths = $this->view->data["MONTHS"];

        //creating months headers
        $vertical = 0;
        foreach ($usedMonths as $month) {
            $objSheet->mergeCellsByColumnAndRow(
                5 + $offset * $vertical,
                ($nodesStartIdx - 5),
                12 + $offset * $vertical,
                ($nodesStartIdx - 5)
            );
            $objSheet->setCellValueByColumnAndRow(5 + $offset * ($vertical), ($nodesStartIdx - 5), $month);
            $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex(5 + $offset * $vertical) . ($nodesStartIdx - 5) .
                ':' . PHPExcel_Cell::stringFromColumnIndex(12 + $offset * $vertical) . ($nodesStartIdx - 5))
                ->applyFromArray($bigDarkHeaderStyle);

            $objSheet->mergeCellsByColumnAndRow(
                5 + $offset * $vertical,
                ($nodesStartIdx - 4),
                5 + $offset * $vertical,
                ($nodesStartIdx - 1)
            );
            $objSheet->getStyle(
                PHPExcel_Cell::stringFromColumnIndex(5 + $offset * ($vertical)) . ($nodesStartIdx - 4) . ':' .
                PHPExcel_Cell::stringFromColumnIndex(5 + $offset * ($vertical)) . ($nodesStartIdx - 1)
            )->applyFromArray($columnHeaderStyle);
            $objSheet->setCellValueByColumnAndRow(
                5 + $offset * ($vertical),
                ($nodesStartIdx - 4),
                str_replace('\n', PHP_EOL, $lang->translate('acctualEffortInPeriod'))
            );
            $objSheet
                ->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(5 + $offset * ($vertical)))->setWidth(12);

            $objSheet->mergeCellsByColumnAndRow(
                6 + $offset * $vertical,
                ($nodesStartIdx - 4),
                6 + $offset * $vertical,
                ($nodesStartIdx - 1)
            );
            $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex(6 + $offset * ($vertical)) . ($nodesStartIdx - 4) .
                ':' . PHPExcel_Cell::stringFromColumnIndex(6 + $offset * ($vertical)) . ($nodesStartIdx - 1))
                ->applyFromArray($columnHeaderStyle);
            $objSheet->setCellValueByColumnAndRow(
                6 + $offset * ($vertical),
                ($nodesStartIdx - 4),
                str_replace('\n', PHP_EOL, $lang->translate('billedInPeriod'))
            );
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(6 + $offset * ($vertical)))
                ->setWidth(13);

            $objSheet->mergeCellsByColumnAndRow(
                7 + $offset * $vertical,
                ($nodesStartIdx - 4),
                7 + $offset * $vertical,
                ($nodesStartIdx - 1)
            );
            $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex(7 + $offset * ($vertical)) . ($nodesStartIdx - 4) . ':' . PHPExcel_Cell::stringFromColumnIndex(7 + $offset * ($vertical)) . ($nodesStartIdx - 1))->applyFromArray($columnHeaderStyle);
            $objSheet->setCellValueByColumnAndRow(7 + $offset * ($vertical), ($nodesStartIdx - 4), str_replace('\n', PHP_EOL, $lang->translate('acctualEffortInYear')));
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(7 + $offset * ($vertical)))->setWidth(15);
            $objSheet->getCommentByColumnAndRow(7 + $offset * $vertical, ($nodesStartIdx - 4))->setWidth(250)->getText()->createTextRun(sprintf(str_replace('\n', PHP_EOL, $lang->translate('acctualEffortInYearComment')), strtoupper($informationArray['FP'])));

            $objSheet->mergeCellsByColumnAndRow(8 + $offset * $vertical, ($nodesStartIdx - 4), 8 + $offset * $vertical, ($nodesStartIdx - 1));
            $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex(8 + $offset * ($vertical)) . ($nodesStartIdx - 4) . ':' . PHPExcel_Cell::stringFromColumnIndex(8 + $offset * ($vertical)) . ($nodesStartIdx - 1))->applyFromArray($columnHeaderStyle);
            $objSheet->setCellValueByColumnAndRow(8 + $offset * ($vertical), ($nodesStartIdx - 4), str_replace('\n', PHP_EOL, $lang->translate('billedInYear')));
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(8 + $offset * ($vertical)))->setWidth(13);

            $objSheet->mergeCellsByColumnAndRow(9 + $offset * $vertical, ($nodesStartIdx - 4), 9 + $offset * $vertical, ($nodesStartIdx - 1));
            $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex(9 + $offset * ($vertical)) . ($nodesStartIdx - 4) . ':' . PHPExcel_Cell::stringFromColumnIndex(9 + $offset * ($vertical)) . ($nodesStartIdx - 1))->applyFromArray($columnHeaderStyle);
            $objSheet->setCellValueByColumnAndRow(9 + $offset * ($vertical), ($nodesStartIdx - 4), str_replace('\n', PHP_EOL, $lang->translate('remainingOrPendingEffort')));
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(9 + $offset * ($vertical)))->setWidth(14);

            $objSheet->mergeCellsByColumnAndRow(10 + $offset * $vertical, ($nodesStartIdx - 4), 10 + $offset * $vertical, ($nodesStartIdx - 1));
            $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex(10 + $offset * ($vertical)) . ($nodesStartIdx - 4) . ':' . PHPExcel_Cell::stringFromColumnIndex(10 + $offset * ($vertical)) . ($nodesStartIdx - 1))->applyFromArray($columnHeaderStyle);
            $objSheet->setCellValueByColumnAndRow(10 + $offset * ($vertical), ($nodesStartIdx - 4), str_replace('\n', PHP_EOL, $lang->translate('discrepancy')));
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(10 + $offset * ($vertical)))->setWidth(12);
            $objSheet->getCommentByColumnAndRow(10 + $offset * $vertical, ($nodesStartIdx - 4))->setWidth(450)->getText()->createTextRun(str_replace('\n', PHP_EOL, $lang->translate('discrepancyComment')));

            $objSheet->mergeCellsByColumnAndRow(11 + $offset * $vertical, ($nodesStartIdx - 4), 11 + $offset * $vertical, ($nodesStartIdx - 1));
            $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex(11 + $offset * ($vertical)) . ($nodesStartIdx - 4) . ':' . PHPExcel_Cell::stringFromColumnIndex(11 + $offset * ($vertical)) . ($nodesStartIdx - 1))->applyFromArray($columnHeaderStyle);
            $objSheet->setCellValueByColumnAndRow(11 + $offset * ($vertical), ($nodesStartIdx - 4), str_replace('\n', PHP_EOL, $lang->translate('billedInPeriodEUR')));
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(11 + $offset * ($vertical)))->setWidth(13);

            $objSheet->mergeCellsByColumnAndRow(12 + $offset * $vertical, ($nodesStartIdx - 4), 12 + $offset * $vertical, ($nodesStartIdx - 1));
            $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex(11 + $offset * ($vertical)) . ($nodesStartIdx - 4) . ':' . PHPExcel_Cell::stringFromColumnIndex(12 + $offset * ($vertical)) . ($nodesStartIdx - 1))->applyFromArray($columnHeaderStyle);
            $objSheet->setCellValueByColumnAndRow(12 + $offset * ($vertical), ($nodesStartIdx - 4), str_replace('\n', PHP_EOL, $lang->translate('billedInYearEUR')));
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(12 + $offset * ($vertical)))->setWidth(15);
            $objSheet->getCommentByColumnAndRow(12 + $offset * $vertical, ($nodesStartIdx - 4))->setWidth(250)->getText()->createTextRun(sprintf(str_replace('\n', PHP_EOL, $lang->translate('billedInYearEURComment')), strtoupper($informationArray['FP'])));

            $vertical++;
        }

        $dataNodes = $this->view->data["DATA"];

        // return early if no data is given at all
        if (empty($dataNodes)) {
            return $this->getContentXlsx();
        }

        // extract orphan project nodes
        if (isset($dataNodes[-1])) {
            $orphanNodes = $dataNodes[-1];
            unset($dataNodes[-1]);
        } else {
            $orphanNodes = array();
        }

        $fixedPrice = array();

        $i = 0;
        foreach ($dataNodes as $node) {
            $nodeName = $node['node_name'];
            $nodeShortcut = $node['node_type_name'];
            $sapKey = $node['sap_key'];
            $targetEffort = $node['target_effort'];
            $maintenancePart = $node['maintenance_part'];
            $hourlyRate = $node['hourly_rate'] ?: 130;
            $company = $node['company'];

            if ($i++ > 0 && count($usedInformationArray) > 1) {
                $this->createHorizontalRule($objSheet, $idx, $usedMonths);
            }

            //// Projects
            if (isset($node["sectionData"][BookingType::FIXED_PRICE])) {
                $fixedPrice = $node["sectionData"][BookingType::FIXED_PRICE];
                $objSheet->setCellValueByColumnAndRow(0, $idx, $nodeName);
                $nameComment = $nodeName . ' (' . $nodeShortcut . ')' . ($sapKey?(' --- ' . $sapKey):'');
                $objSheet->getComment('A' . $idx)->setWidth(500)->setHeight(30)->getText()->createTextRun($nameComment);
                $objSheet->getStyle('A' . $idx)->applyFromArray($bordersStyle);
                $objSheet->getStyle('A' . $idx)->getAlignment()->setWrapText(true);
                $objSheet->setCellValueByColumnAndRow(1, $idx, strtoupper($informationArray['FP']));
                $objSheet->getStyle('B' . $idx)->applyFromArray($bold);
                $objSheet->setCellValueByColumnAndRow(2, $idx, $company);
                $objSheet->getStyle('C' . $idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objSheet->setCellValueByColumnAndRow(3, $idx, $maintenancePart);
                $objSheet->setCellValueByColumnAndRow(4, $idx, $targetEffort);
                $objSheet->getStyleByColumnAndRow(4, $idx)->getNumberFormat()->setFormatCode('#,##0.00');

                $vertical = 0;
                foreach ($usedMonths as $month) {
                    //Ist-Aufwand im Zeitraum (Std)
                    $hoursBtMonth = $fixedPrice[$month]['hours_bt_month'];
                    $objSheet->setCellValueByColumnAndRow(5 + $offset * ($vertical), $idx, $hoursBtMonth);
                    $objSheet->getStyleByColumnAndRow(5 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abgerechnet im Zeitraum
                    $objSheet->setCellValueByColumnAndRow(6 + $offset * ($vertical), $idx, null);
                    $objSheet->getStyleByColumnAndRow(6 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Ist-Aufwand seit (Jahres-)Beginn
                    $hoursBtAll = $fixedPrice[$month]['hours_bt_all'];
                    $objSheet->setCellValueByColumnAndRow(7 + $offset * ($vertical), $idx, $hoursBtAll);
                    $objSheet->getStyleByColumnAndRow(7 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abgerechnet seit 1.1.
                    $objSheet->setCellValueByColumnAndRow(8 + $offset * ($vertical), $idx, null);
                    $objSheet->getStyleByColumnAndRow(8 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Rest-Aufwand
                    $objSheet->setCellValueByColumnAndRow(9 + $offset * ($vertical), $idx, null);
                    $objSheet->getStyleByColumnAndRow(9 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abweichung
                    $objSheet->setCellValueByColumnAndRow(
                        10 + $offset * ($vertical),
                        $idx,
                        "=IF(AND(" . PHPExcel_Cell::stringFromColumnIndex(4) . $idx . "<>0,NOT(ISBLANK(" .
                        PHPExcel_Cell::stringFromColumnIndex(9 + $offset * ($vertical)) . $idx . "))),((" .
                        PHPExcel_Cell::stringFromColumnIndex(7 + $offset * ($vertical)) . $idx . "+" .
                        PHPExcel_Cell::stringFromColumnIndex(9 + $offset * ($vertical)) . $idx . "-" .
                        PHPExcel_Cell::stringFromColumnIndex(4) . $idx . ")/" .
                        PHPExcel_Cell::stringFromColumnIndex(4) . $idx . "),\"\")"
                    );
                    $objSheet->getStyleByColumnAndRow(10 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abgerechnet im Zeitraum (EUR)
                    $revenueMonth = $fixedPrice[$month]['revenue_month'] + $fixedPrice[$month]['revenue_license_month'];
                    $objSheet->setCellValueByColumnAndRow(11 + $offset * ($vertical), $idx, $revenueMonth);
                    $objSheet->getStyleByColumnAndRow(11 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00_-[$€ ]');
                    if (0 != $fixedPrice[$month]['revenue_license_month']) {
                        $objSheet->getCommentByColumnAndRow(11 + $offset * ($vertical), $idx)
                            ->setWidth(250)
                            ->setHeight(75)
                            ->getText()
                            ->createTextRun(sprintf(
                                $lang->translate("licenceRevenue") . ": %s €\n" . $lang->translate("otherRevenue") . ": %s €",
                                number_format($fixedPrice[$month]['revenue_license_month'], 2, ',', '.'),
                                number_format($fixedPrice[$month]['revenue_month'], 2, ',', '.')
                            ));
                    }
                    //Abgerechnet seit (Jahres-)Beginn
                    $revenueAll = $fixedPrice[$month]['revenue_all'] + $fixedPrice[$month]['revenue_license_all'];
                    $objSheet->setCellValueByColumnAndRow(12 + $offset * ($vertical), $idx, $revenueAll);
                    $objSheet->getStyleByColumnAndRow(12 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00_-[$€ ]');
                    if (0 != $fixedPrice[$month]['revenue_license_all']) {
                        $objSheet->getCommentByColumnAndRow(12 + $offset * ($vertical), $idx)
                            ->setWidth(250)
                            ->setHeight(75)
                            ->getText()
                            ->createTextRun(sprintf(
                                $lang->translate("licenceRevenue") . ": %s €\n" . $lang->translate("otherRevenue") . ": %s €",
                                number_format($fixedPrice[$month]['revenue_license_all'], 2, ',', '.'),
                                number_format($fixedPrice[$month]['revenue_all'], 2, ',', '.')
                            ));
                    }
                    $vertical++;
                }

                $idx++;
            }

            //// Change requests
            if (isset($node["sectionData"][BookingType::CHANGE_REQUEST])) {
                $changeRequests = $node["sectionData"][BookingType::CHANGE_REQUEST];
                $objSheet->setCellValueByColumnAndRow(0, $idx, $nodeName);
                $nameComment = $nodeName . ' (' . $nodeShortcut . ')' . ($sapKey?(' --- ' . $sapKey):'');
                $objSheet->getComment('A' . $idx)->setWidth(500)->setHeight(30)->getText()->createTextRun($nameComment);
                $objSheet->getStyle('A' . $idx)->applyFromArray($bordersStyle);
                $objSheet->getStyle('A' . $idx)->getAlignment()->setWrapText(true);
                $objSheet->setCellValueByColumnAndRow(1, $idx, strtoupper($informationArray['CR']));
                $objSheet->getStyle('B' . $idx)->applyFromArray($bold);
                $objSheet->setCellValueByColumnAndRow(2, $idx, $company);
                $objSheet->getStyle('C' . $idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objSheet->setCellValueByColumnAndRow(3, $idx, $maintenancePart);
                //Plan-Aufwand
                $objSheet->getStyleByColumnAndRow(4, $idx)->getNumberFormat()->setFormatCode('#,##0.00_-[$€ ]');
                $objSheet->setCellValueByColumnAndRow(4, $idx, $hourlyRate);


                $vertical = 0;
                foreach ($usedMonths as $month) {
                    //Ist-Aufwand im Zeitraum
                    $objSheet->setCellValueByColumnAndRow(5 + $offset * ($vertical), $idx, $changeRequests[$month]['hours_bt_month']);
                    $objSheet->getStyleByColumnAndRow(5 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abgerechnet im Zeitraum
                    $objSheet->setCellValueByColumnAndRow(6 + $offset * ($vertical), $idx, "=(" . PHPExcel_Cell::stringFromColumnIndex(11 + $offset * ($vertical)) . $idx . "/" . PHPExcel_Cell::stringFromColumnIndex(4) . $idx . ")");
                    $objSheet->getStyleByColumnAndRow(6 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Ist-Aufwand seit (Jahres-)Beginn
                    $objSheet->setCellValueByColumnAndRow(7 + $offset * ($vertical), $idx, $changeRequests[$month]['hours_bt_year']);
                    $objSheet->getStyleByColumnAndRow(7 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abgerechnet seit 1.1.
                    $objSheet->setCellValueByColumnAndRow(8 + $offset * ($vertical), $idx, "=(" . PHPExcel_Cell::stringFromColumnIndex(12 + $offset * ($vertical)) . $idx . "/" . PHPExcel_Cell::stringFromColumnIndex(4) . $idx . ")");
                    $objSheet->getStyleByColumnAndRow(8 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Rest-Aufwand
                    $objSheet->setCellValueByColumnAndRow(9 + $offset * ($vertical), $idx, null);
                    $objSheet->getStyleByColumnAndRow(9 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abweichung
                    $objSheet->setCellValueByColumnAndRow(
                        10 + $offset * ($vertical),
                        $idx,
                        "=(" . PHPExcel_Cell::stringFromColumnIndex(8 + $offset * ($vertical)) . $idx .
                        "-" . PHPExcel_Cell::stringFromColumnIndex(7 + $offset * ($vertical)) . $idx . ")"
                    );
                    $objSheet->getStyleByColumnAndRow(10 + $offset * ($vertical), $idx)->getNumberFormat()
                        ->setFormatCode('#,##0.00');
                    //Abgerechnet im Zeitraum
                    $objSheet->setCellValueByColumnAndRow(
                        11 + $offset * ($vertical),
                        $idx,
                        $changeRequests[$month]['revenue_month']
                    );
                    $objSheet->getStyleByColumnAndRow(11 + $offset * ($vertical), $idx)->getNumberFormat()
                        ->setFormatCode('#,##0.00_-[$€ ]');
                    //Abgerechnet seit (Jahres-)Beginn
                    $objSheet->setCellValueByColumnAndRow(
                        12 + $offset * ($vertical),
                        $idx,
                        $changeRequests[$month]['revenue_year']
                    );
                    $objSheet->getStyleByColumnAndRow(12 + $offset * ($vertical), $idx)->getNumberFormat()
                        ->setFormatCode('#,##0.00_-[$€ ]');
                    $vertical++;
                }

                $idx++;
            }

            //// Time and material
            if (isset($node["sectionData"][BookingType::TIME_AND_MATERIAL])) {
                $timeAndMaterial = $node["sectionData"][BookingType::TIME_AND_MATERIAL];
                $objSheet->setCellValueByColumnAndRow(0, $idx, $nodeName);
                $nameComment = $nodeName . ' (' . $nodeShortcut . ')' . ($sapKey?(' --- ' . $sapKey):'');
                $objSheet->getComment('A' . $idx)->setWidth(500)->setHeight(30)->getText()->createTextRun($nameComment);
                $objSheet->getStyle('A' . $idx)->applyFromArray($bordersStyle);
                $objSheet->getStyle('A' . $idx)->getAlignment()->setWrapText(true);
                $objSheet->setCellValueByColumnAndRow(1, $idx, strtoupper($informationArray['TM']));
                $objSheet->getStyle('B' . $idx)->applyFromArray($bold);
                $objSheet->setCellValueByColumnAndRow(2, $idx, $company);
                $objSheet->getStyle('C' . $idx)->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objSheet->setCellValueByColumnAndRow(3, $idx, $maintenancePart);
                //Plan-Aufwand
                $objSheet->getStyleByColumnAndRow(4, $idx)->getNumberFormat()->setFormatCode('#,##0.00_-[$€ ]');
                $objSheet->setCellValueByColumnAndRow(4, $idx, $hourlyRate);

                $vertical = 0;
                foreach ($usedMonths as $month) {
                    //Ist-Aufwand im Zeitraum
                    $objSheet->setCellValueByColumnAndRow(5 + $offset * ($vertical), $idx, $timeAndMaterial[$month]['hours_bt_month']);
                    $objSheet->getStyleByColumnAndRow(5 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abgerechnet im Zeitraum
                    $objSheet->setCellValueByColumnAndRow(6 + $offset * ($vertical), $idx, "=(" . PHPExcel_Cell::stringFromColumnIndex(11 + $offset * ($vertical)) . $idx . "/" . PHPExcel_Cell::stringFromColumnIndex(4) . $idx . ")");
                    $objSheet->getStyleByColumnAndRow(6 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Ist-Aufwand seit (Jahres-)Beginn
                    $objSheet->setCellValueByColumnAndRow(7 + $offset * ($vertical), $idx, $timeAndMaterial[$month]['hours_bt_year']);
                    $objSheet->getStyleByColumnAndRow(7 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abgerechnet seit 1.1.
                    $objSheet->setCellValueByColumnAndRow(8 + $offset * ($vertical), $idx, "=(" . PHPExcel_Cell::stringFromColumnIndex(12 + $offset * ($vertical)) . $idx . "/" . PHPExcel_Cell::stringFromColumnIndex(4) . $idx . ")");
                    $objSheet->getStyleByColumnAndRow(8 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Rest-Aufwand
                    if ($timeAndMaterial[$month]['eab_pending_hours'] > 0) {
                        $objSheet->getStyleByColumnAndRow(9 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                        $objSheet->setCellValueByColumnAndRow(9 + $offset * ($vertical), $idx, $timeAndMaterial[$month]['eab_pending_hours']);
                        $objSheet->getCommentByColumnAndRow(9 + $offset * ($vertical), $idx)
                            ->setWidth(275)
                            ->setHeight(25)
                            ->getText()
                            ->createTextRun(sprintf($lang->translate('eabComment'), $timeAndMaterial[$month]['eab_month']));
                    }
                    //Abweichung
                    $objSheet->setCellValueByColumnAndRow(
                        10 + $offset * ($vertical),
                        $idx,
                        "=(" . PHPExcel_Cell::stringFromColumnIndex(8 + $offset * ($vertical)) . $idx .
                        "-" . PHPExcel_Cell::stringFromColumnIndex(7 + $offset * ($vertical)) . $idx . ")"
                    );
                    $objSheet->getStyleByColumnAndRow(10 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abgerechnet im Zeitraum
                    $objSheet->setCellValueByColumnAndRow(11 + $offset * ($vertical), $idx, $timeAndMaterial[$month]['revenue_month']);
                    $objSheet->getStyleByColumnAndRow(11 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00_-[$€ ]');
                    //Abgerechnet seit (Jahres-)Beginn
                    $objSheet->setCellValueByColumnAndRow(12 + $offset * ($vertical), $idx, $timeAndMaterial[$month]['revenue_year']);
                    $objSheet->getStyleByColumnAndRow(12 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00_-[$€ ]');
                    $vertical++;
                }

                $idx++;
            }

            //// Maintenance
            if (isset($node["sectionData"][BookingType::MAINTENANCE])) {
                $maintenance = $node["sectionData"][BookingType::MAINTENANCE];
                $objSheet->setCellValueByColumnAndRow(0, $idx, $nodeName);
                $nameComment = $nodeName . ' (' . $nodeShortcut . ')' . ($sapKey?(' --- ' . $sapKey):'');
                $objSheet->getComment('A' . $idx)->setWidth(500)->setHeight(30)->getText()->createTextRun($nameComment);
                $objSheet->getStyle('A' . $idx)->applyFromArray($bordersStyle);
                $objSheet->getStyle('A' . $idx)->getAlignment()->setWrapText(true);
                $objSheet->setCellValueByColumnAndRow(1, $idx, strtoupper($informationArray['MC']));
                $objSheet->getStyle('B' . $idx)->applyFromArray($bold);
                if ($maintenancePart) {
                    $infComment = sprintf($lang->translate('maintenanceComment'), $maintenancePart);
                    $objSheet->getComment('B' . $idx)->setWidth(350)->setHeight(50)->getText()->createTextRun($infComment);
                }
                $objSheet->setCellValueByColumnAndRow(2, $idx, $company);
                $objSheet->getStyle('C' . $idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objSheet->setCellValueByColumnAndRow(3, $idx, $maintenancePart);
                //Plan-Aufwand
                $objSheet->getStyleByColumnAndRow(4, $idx)->getNumberFormat()->setFormatCode('#,##0.00_-[$€ ]');
                $objSheet->setCellValueByColumnAndRow(4, $idx, $hourlyRate);


                $vertical = 0;
                foreach ($usedMonths as $month) {
                    //Ist-Aufwand im Zeitraum
                    $objSheet->setCellValueByColumnAndRow(5 + $offset * ($vertical), $idx, $maintenance[$month]['hours_bt_month']);
                    $objSheet->getStyleByColumnAndRow(5 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abgerechnet im Zeitraum
                    $objSheet->setCellValueByColumnAndRow(6 + $offset * ($vertical), $idx, "=((" . PHPExcel_Cell::stringFromColumnIndex(11 + $offset * ($vertical)) . $idx .
                        "*" . PHPExcel_Cell::stringFromColumnIndex(3) . $idx . "/100)/" . PHPExcel_Cell::stringFromColumnIndex(4) . $idx . ")");
                    $objSheet->getStyleByColumnAndRow(6 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Ist-Aufwand seit (Jahres-)Beginn
                    $objSheet->setCellValueByColumnAndRow(7 + $offset * ($vertical), $idx, $maintenance[$month]['hours_bt_year']);
                    $objSheet->getStyleByColumnAndRow(7 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abgerechnet seit 1.1.
                    $objSheet->setCellValueByColumnAndRow(8 + $offset * ($vertical), $idx, "=((" . PHPExcel_Cell::stringFromColumnIndex(12 + $offset * ($vertical)) . $idx .
                        "*" . PHPExcel_Cell::stringFromColumnIndex(3) . $idx . "/100)/" . PHPExcel_Cell::stringFromColumnIndex(4) . $idx . ")");
                    $objSheet->getStyleByColumnAndRow(8 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Rest-Aufwand
                    $objSheet->setCellValueByColumnAndRow(9 + $offset * ($vertical), $idx, null);
                    $objSheet->getStyleByColumnAndRow(9 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abweichung
                    $objSheet->setCellValueByColumnAndRow(
                        10 + $offset * ($vertical),
                        $idx,
                        "=(" . PHPExcel_Cell::stringFromColumnIndex(8 + $offset * ($vertical)) . $idx .
                        "-" . PHPExcel_Cell::stringFromColumnIndex(7 + $offset * ($vertical)) . $idx . ")"
                    );
                    $objSheet->getStyleByColumnAndRow(10 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abgerechnet im Zeitraum (EUR)
                    $objSheet->setCellValueByColumnAndRow(11 + $offset * ($vertical), $idx, $maintenance[$month]['revenue_month'] + $maintenance[$month]['revenue_pax_month']);
                    $objSheet->getStyleByColumnAndRow(11 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00_-[$€ ]');
                    if (0 != $maintenance[$month]['revenue_pax_month']) {
                        $objSheet->getCommentByColumnAndRow(11 + $offset * ($vertical), $idx)
                            ->setWidth(250)
                            ->setHeight(75)
                            ->getText()
                            ->createTextRun(sprintf(
                                $lang->translate("paxRevenue") . ": %s €\n" . $lang->translate("otherRevenue") . ": %s €",
                                number_format($maintenance[$month]['revenue_pax_month'], 2, ',', '.'),
                                number_format($maintenance[$month]['revenue_month'], 2, ',', '.')
                            ));
                    }
                    //Abgerechnet seit (Jahres-)Beginn (EUR)
                    $objSheet->setCellValueByColumnAndRow(12 + $offset * ($vertical), $idx, $maintenance[$month]['revenue_year'] + $maintenance[$month]['revenue_pax_year']);
                    $objSheet->getStyleByColumnAndRow(12 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00_-[$€ ]');
                    if (0 != $maintenance[$month]['revenue_pax_year']) {
                        $objSheet->getCommentByColumnAndRow(12 + $offset * ($vertical), $idx)
                            ->setWidth(250)
                            ->setHeight(75)
                            ->getText()
                            ->createTextRun(sprintf(
                                $lang->translate("paxRevenue") . ": %s €\n" . $lang->translate("otherRevenue") . ": %s €",
                                number_format($maintenance[$month]['revenue_pax_year'], 2, ',', '.'),
                                number_format($maintenance[$month]['revenue_year'], 2, ',', '.')
                            ));
                    }
                    $vertical++;
                }

                $idx++;
            }

            //// Not accountable
            if (isset($node["sectionData"][BookingType::NOT_ACCOUNTABLE])) {
                $notaccountable = $node["sectionData"][BookingType::NOT_ACCOUNTABLE];
                $objSheet->setCellValueByColumnAndRow(0, $idx, $nodeName);
                $objSheet->getStyle('A' . $idx)->applyFromArray($bordersStyle);
                $objSheet->getStyle('A' . $idx)->getAlignment()->setWrapText(true);
                $objSheet->setCellValueByColumnAndRow(1, $idx, strtoupper($informationArray['NA']));
                $objSheet->getStyle('B' . $idx)->applyFromArray($bold);
                $objSheet->getStyle('C' . $idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objSheet->setCellValueByColumnAndRow(2, $idx, $company);
                $objSheet->setCellValueByColumnAndRow(3, $idx, $maintenancePart);
                $nameComment = $nodeName . ' (' . $nodeShortcut . ')' . ($sapKey?(' --- ' . $sapKey):'');
                $objSheet->getComment('A' . $idx)->setWidth(500)->setHeight(30)->getText()->createTextRun($nameComment);


                $vertical = 0;
                foreach ($usedMonths as $month) {
                    //Ist-Aufwand im Zeitraum
                    $objSheet->setCellValueByColumnAndRow(5 + $offset * ($vertical), $idx, $notaccountable[$month]['hours_bt_month']);
                    $objSheet->getStyleByColumnAndRow(5 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abgerechnet im Zeitraum
                    $objSheet->setCellValueByColumnAndRow(6 + $offset * ($vertical), $idx, null);
                    $objSheet->getStyleByColumnAndRow(6 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Ist-Aufwand seit (Jahres-)Beginn
                    $objSheet->setCellValueByColumnAndRow(7 + $offset * ($vertical), $idx, $notaccountable[$month]['hours_bt_year']);
                    $objSheet->getStyleByColumnAndRow(7 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abgerechnet seit 1.1.
                    $objSheet->setCellValueByColumnAndRow(8 + $offset * ($vertical), $idx, null);
                    $objSheet->getStyleByColumnAndRow(8 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Rest-Aufwand
                    $objSheet->setCellValueByColumnAndRow(9 + $offset * ($vertical), $idx, null);
                    $objSheet->getStyleByColumnAndRow(9 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abweichung
                    $objSheet->setCellValueByColumnAndRow(10 + $offset * ($vertical), $idx, null);
                    $objSheet->getStyleByColumnAndRow(10 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    //Abgerechnet im Zeitraum
                    $objSheet->setCellValueByColumnAndRow(11 + $offset * ($vertical), $idx, null);
                    $objSheet->getStyleByColumnAndRow(11 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00_-[$€ ]');
                    //Abgerechnet seit (Jahres-)Beginn
                    $objSheet->setCellValueByColumnAndRow(12 + $offset * ($vertical), $idx, null);
                    $objSheet->getStyleByColumnAndRow(12 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00_-[$€ ]');
                    $vertical++;
                }

                $idx++;
            }

            //Indented nodes
            $projectData = $node["projectData"];
            if (!empty($projectData)) {
                $this->createIndentedNodes($objSheet, $usedMonths, $bold, $idx, $projectData);

                if (count($usedInformationArray) == 1) {
                    $this->createHorizontalRule($objSheet, $idx, $usedMonths);
                }
            }
        }

        //Indented orphan nodes
        if (!empty($orphanNodes)) {
            $this->createIndentedNodes($objSheet, $usedMonths, $bold, $idx, $orphanNodes);
        }

        //borders
        $vertical = 0;

        $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex(0) . ($nodesStartIdx - 5) . ':' . PHPExcel_Cell::stringFromColumnIndex(5) . ($idx - 1))->applyFromArray($bordersStyle);
        foreach ($usedMonths as $month) { //add borders
            $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex(5 + $offset * ($vertical)) . ($nodesStartIdx - 5) . ':' . PHPExcel_Cell::stringFromColumnIndex(12 + $offset * ($vertical)) . ($idx - 1))->applyFromArray($bordersStyle);
            $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex(5 + $offset * ($vertical)) . ($nodesStartIdx - 5) . ':' . PHPExcel_Cell::stringFromColumnIndex(12 + $offset * ($vertical)) . ($idx - 1))->applyFromArray($thickBordersStyle);
            $vertical++;
        }

        //Sums
        $vertical = 0;

        if (empty($this->view->data['REPORT_INFO']['NODES']['project']) && (!isset($usedInformationArray['FP']) || count($usedInformationArray) == 1)) {
            $objSheet->setCellValueByColumnAndRow(4, $idx, "Summen:");
            $objSheet->getStyleByColumnAndRow(4, $idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objSheet->getStyleByColumnAndRow(4, $idx)->applyFromArray($bold);
            foreach ($usedMonths as $month) { //add borders
                //Ist-Aufwand im Zeitraum
                $objSheet->setCellValueByColumnAndRow(
                    5 + $offset * ($vertical),
                    $idx,
                    '=(SUM(' . PHPExcel_Cell::stringFromColumnIndex(5 + $offset * ($vertical)) . $nodesStartIdx . ':' . PHPExcel_Cell::stringFromColumnIndex(5 + $offset * ($vertical)) . ($idx - 1) . ') )'
                );
                $objSheet->getStyleByColumnAndRow(5 + $offset * ($vertical), $idx)->getNumberFormat()
                    ->setFormatCode('#,##0.00');
                //Abgerechnet im Zeitraum
                $objSheet->setCellValueByColumnAndRow(
                    6 + $offset * ($vertical),
                    $idx,
                    '=(SUM(' . PHPExcel_Cell::stringFromColumnIndex(6 + $offset * ($vertical)) . $nodesStartIdx . ':' . PHPExcel_Cell::stringFromColumnIndex(6 + $offset * ($vertical)) . ($idx - 1) . ') )'
                );
                $objSheet->getStyleByColumnAndRow(6 + $offset * ($vertical), $idx)->getNumberFormat()
                    ->setFormatCode('#,##0.00');
                //Ist-Aufwand seit (Jahres-)Beginn
                $objSheet->setCellValueByColumnAndRow(
                    7 + $offset * ($vertical),
                    $idx,
                    '=(SUM(' . PHPExcel_Cell::stringFromColumnIndex(7 + $offset * ($vertical)) . $nodesStartIdx . ':' . PHPExcel_Cell::stringFromColumnIndex(7 + $offset * ($vertical)) . ($idx - 1) . ') )'
                );
                $objSheet->getStyleByColumnAndRow(7 + $offset * ($vertical), $idx)->getNumberFormat()
                    ->setFormatCode('#,##0.00');
                //Abgerechnet seit 1.1.
                $objSheet->setCellValueByColumnAndRow(
                    8 + $offset * ($vertical),
                    $idx,
                    '=(SUM(' . PHPExcel_Cell::stringFromColumnIndex(8 + $offset * ($vertical)) . $nodesStartIdx . ':' . PHPExcel_Cell::stringFromColumnIndex(8 + $offset * ($vertical)) . ($idx - 1) . ') )'
                );
                $objSheet->getStyleByColumnAndRow(8 + $offset * ($vertical), $idx)->getNumberFormat()
                    ->setFormatCode('#,##0.00');
                //Rest-Aufwand
                //Abweichung
                $objSheet->setCellValueByColumnAndRow(
                    10 + $offset * ($vertical),
                    $idx,
                    '=(SUM(' . PHPExcel_Cell::stringFromColumnIndex(10 + $offset * ($vertical)) . $nodesStartIdx . ':' . PHPExcel_Cell::stringFromColumnIndex(10 + $offset * ($vertical)) . ($idx - 1) . ') )'
                );
                $objSheet->getStyleByColumnAndRow(10 + $offset * ($vertical), $idx)->getNumberFormat()
                    ->setFormatCode('#,##0.00');
                //Abgerechnet im Zeitraum
                $objSheet->setCellValueByColumnAndRow(
                    11 + $offset * ($vertical),
                    $idx,
                    '=(SUM(' . PHPExcel_Cell::stringFromColumnIndex(11 + $offset * ($vertical)) . $nodesStartIdx . ':' . PHPExcel_Cell::stringFromColumnIndex(11 + $offset * ($vertical)) . ($idx - 1) . ') )'
                );
                $objSheet->getStyleByColumnAndRow(11 + $offset * ($vertical), $idx)->getNumberFormat()
                    ->setFormatCode('#,##0.00_-[$€ ]');
                //Abgerechnet seit (Jahres-)Beginn
                $objSheet->setCellValueByColumnAndRow(
                    12 + $offset * ($vertical),
                    $idx,
                    '=(SUM(' . PHPExcel_Cell::stringFromColumnIndex(12 + $offset * ($vertical)) . $nodesStartIdx . ':' . PHPExcel_Cell::stringFromColumnIndex(12 + $offset * ($vertical)) . ($idx - 1) . ') )'
                );
                $objSheet->getStyleByColumnAndRow(12 + $offset * ($vertical), $idx)->getNumberFormat()
                    ->setFormatCode('#,##0.00_-[$€ ]');
                $vertical++;
            }
        }

        return $this->getContentXlsx();
    }

    public function exportOrderOverview()
    {
        $lang = &$this->translate;
        $objSheet = $this->activeSheet;
        $defaultNumberFormat = '#,##0.00_-[$€ ]';

        // prepare data
        $tsFrom = $this->view->data['REPORT_INFO']['PERIODFROM'];
        $dateFrom = new Zend_Date();
        $dateFrom->setTimestamp($tsFrom);

        $tsUntil = $this->view->data['REPORT_INFO']['PERIODUNTIL'];
        $dateUntil = new Zend_Date();
        $dateUntil->setTimestamp($tsUntil);

        // general sheet config
        $this->setUpPageSetup();
        $objSheet->getPageSetup()->setFitToHeight(0);

        // create report header
        $rowIndex = $this->createHeader('orderOverview');

        // create report body
        $data = $this->view->data['DATA'];
        $orders = $data['ORDERS'];

        $colStyleConfigs = array(
            'A' => [
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
                    ]
                ],
                'width' => 30
            ],
            'B' => [
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
                    ]
                ],
                'width' => 22
            ],
            'C' => [
                'width' => 17
            ],
            'D' => [
                'width' => 14
            ],
            'E' => [
                'width' => 24
            ],
            'F' => [
                'width' => 23
            ],
            'G' => [
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
                    ]
                ],
                'width' => 14
            ],
            'H' => [
                'width' => 10
            ],
            'I' => [
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
                    ],
                    'numberformat' => [
                        'code' => $defaultNumberFormat
                    ]
                ],
                'width' => 14
            ],
            'J' => [
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
                    ],
                    'numberformat' => [
                        'code' => $defaultNumberFormat
                    ]
                ],
                'width' => 14
            ],
            'K' => [
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
                    ],
                    'numberformat' => [
                        'code' => $defaultNumberFormat
                    ]
                ],
                'width' => 15
            ],
            'L' => [
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
                    ],
                    'numberformat' => [
                        'code' => '#,##0.00'
                    ]
                ],
                'width' => 10
            ],
            'M' => [
                'width' => 7
            ],
            'N' => [
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
                    ],
                    'numberformat' => [
                        'code' => $defaultNumberFormat
                    ]
                ],
                'width' => 15
            ]
        );

        $this->orderOverviewOrderColumns = array(
            'DEBTOR_NUMBER' => array(
                'title' => $lang->translate('orderOverviewDebtorNumber')
            ),
            'INVOICE_RECEIVER_NUMBER' => array(
                'title' => $lang->translate('orderOverviewInvoiceRecipient')
            ),
            'STATUS' => array(
                'title' => $lang->translate('orderOverviewStatus'),
                'renderer' => 'statusRenderer'
            ),
            'ORDER_NUMBER'  => array(
                'title' => $lang->translate('orderOverviewSapOrder')
            ),
            'CUSTOMER_NAME' => array(
                'title' => $lang->translate('orderOverviewCustomerName')
            ),
            'CUSTOMER_ORDER_NUMBER' => array(
                'title' => $lang->translate('orderOverviewOrderNumber')
            ),
            'ORDER_KIND' => array(
                'title' => $lang->translate('orderOverviewKindOfOrder'),
                'renderer' => 'orderKindRenderer'
            ),
            'BEGIN' => array(
                'title' => $lang->translate('orderOverviewBegin')
            ),
            'END' => array(
                'title' => $lang->translate('orderOverviewEnd')
            ),
            'COMMISSIONED' => array(
                'title' => $lang->translate('orderOverviewOrderedInEuro')
            ),
            'INVOICED' => array(
                'title' => $lang->translate('orderOverviewInvoicedInEuro'),
                'displayZeroValues' => true
            ),
            'OUTSTANDING' => array(
                'title' => $lang->translate('orderOverviewOpenInEuro'),
                'colOffset' => 2
            )
        );

        $this->orderOverviewOrderPositionsColumns = array(
            'SAP_KEY' => array(
                'title' => $lang->translate('orderOverviewSapKeyType'),
                'renderer' => 'sapKeyRenderer'
            ),
            'POSITION_NUMBER' => array(
                'title' => $lang->translate('orderOverviewPosition')
            ),
            'STATUS' => array(
                'title' => $lang->translate('orderOverviewStatus'),
                'renderer' => 'statusRenderer'
            ),
            'MATERIAL' => array(
                'title' => $lang->translate('orderOverviewMaterial'),
            ),
            'DESCRIPTION' => array(
                'title' => $lang->translate('orderOverviewDescription'),
            ),
            'MATERIAL_TEXT' => array(
                'title' => $lang->translate('orderOverviewAdditionalDescription')
            ),
            'QUANTITY' => array(
                'title' => $lang->translate('orderOverviewAmount'),
                'styles' => array(
                    'headerTextAlign' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
                )
            ),
            'UNIT' => array(
                'title' => $lang->translate('orderOverviewUnit')
            ),
            'UNIT_PRICE' => array(
                'title' => $lang->translate('orderOverviewUnitPrice'),
                'styles' => array(
                    'headerTextAlign' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
                )
            ),
            'NET_VALUE' => array(
                'title' => $lang->translate('orderOverviewOrderedInEuro'),
            ),
            'SUM_BILLED_AMOUNT' => array(
                'title' => $lang->translate('orderOverviewInvoicedInEuro'),
                'displayZeroValues' => true
            ),
            'OUTSTANDING' => array(
                'title' => $lang->translate('orderOverviewOpenIn')
            ),
            'BT_BS' => array(
                'title' => $lang->translate('orderOverviewUnitBtBs'),
                'renderer' => 'btBsRenderer'
            ),
            'OUTSTANDING_IN_EURO' => array(
                'title' => $lang->translate('orderOverviewOpenInEuro')
            )
        );

        $this->orderOverviewInvoicePlanElementColumns = array(
            'ELEMENT_NUMBER' => array(
                'title' => $lang->translate('orderOverviewMilestone'),
                'colOffset' => 1
            ),
            'STATUS' => array(
                'title' => $lang->translate('orderOverviewStatus'),
                'renderer' => 'statusRenderer'
            ),
            'TEXT' => array(
                'title' => $lang->translate('orderOverviewMilestoneText')
            ),
            'INVOICE_PERIOD' => array(
                'title' => $lang->translate('orderOverviewInvoicePeriod'),
                'colOffset' => 1
            ),
            'INVOICE_DATE' => array(
                'title' => $lang->translate('orderOverviewInvoiceAcceptanceDate'),
                'colOffset' => 1
            ),
            'COMMISSIONED' => array(
                'title' => $lang->translate('orderOverviewOrderedInEuro'),
                'colOffset' => 1
            ),
            'INVOICED' => array(
                'title' => $lang->translate('orderOverviewInvoicedInEuro'),
                'displayZeroValues' => true
            ),
            'OUTSTANDING_IN_EURO' => array(
                'title' => $lang->translate('orderOverviewOpenInEuro'),
                'colOffset' => 2
            )
        );

        $this->orderOverviewSumsColumns = array(
            'SUMS' => array(
                'title' => $lang->translate('orderOverviewSum')
            ),
            'ORDERS' => array(
                'title' => $lang->translate('orderOverviewOrders') . ':',
                'styles' => array(
                    'headerTextAlign' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
                )
            ),
            'ORDERS_COMPUTED' => array(
                'title' => $lang->translate('orderOverviewTotal'),
                'colOffset' => 1,
                'styles' => array(
                    'headerTextAlign' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
                )
            ),
            'NOT_INVOICED' => array(
                'title' => $lang->translate('orderOverviewNotInvoiced'),
                'styles' => array(
                    'headerTextAlign' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
                )
            ),
            'PARTIALLY_INVOICED' => array(
                'title' => $lang->translate('orderOverviewPartiallyInvoiced'),
                'styles' => array(
                    'headerTextAlign' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
                )
            ),
            'INVOICED' => array(
                'title' => $lang->translate('orderOverviewInvoiced'),
                'styles' => array(
                    'headerTextAlign' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
                )
            ),
            'AMOUNTS' => array(
                'title' => $lang->translate('orderOverviewAmounts') . ':',
                'colOffset' => 1,
                'styles' => array(
                    'headerTextAlign' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
                )
            ),
            'COMMISSIONED_COMPUTED' => array(
                'title' => $lang->translate('orderOverviewOrderedInEuro'),
                'styles' => array(
                    'numberFormat' => $defaultNumberFormat,
                    'headerTextAlign' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
                )
            ),
            'INVOICED_COMPUTED' => array(
                'title' => $lang->translate('orderOverviewInvoicedInEuro'),
                'styles' => array(
                    'numberFormat' => $defaultNumberFormat,
                    'headerTextAlign' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
                )
            ),
            'OUTSTANDING_COMPUTED' => array(
                'title' => $lang->translate('orderOverviewOpenInEuro'),
                'colOffset' => 2,
                'styles' => array(
                    'numberFormat' => $defaultNumberFormat,
                    'headerTextAlign' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
                )
            )
        );

        $dataBeginRowIndex = $rowIndex;
        if (!empty($orders)) {
            foreach ($orders as $orderNumber => $orderData) {
                $rowIndex = $this->createOrderOverviewColumnHeaders($this->orderOverviewOrderColumns, $rowIndex, $this->getOrderBackgroundColor());
                $rowIndex = $this->generateOrder($orderData, $rowIndex);

                $objSheet->getStyle('A' . ($rowIndex - 1) . ':' . 'N' . ($rowIndex - 1))->applyFromArray(array(
                    'borders' => array(
                        'bottom' => array(
                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                            'color' => array('argb' => '00000000'),
                        )
                    )
                ));
            }
            $rowIndex++;

            foreach ($colStyleConfigs as $colIndex => $colStyleConfig) {
                if (isset($colStyleConfig['styles'])) {
                    $objSheet->getStyle($colIndex . $dataBeginRowIndex . ':' . $colIndex . ($rowIndex - 1))->applyFromArray($colStyleConfig['styles']);
                }

                if (isset($colStyleConfig['width'])) {
                    $objSheet->getColumnDimension($colIndex)->setWidth($colStyleConfig['width']);
                }
            }

            $rowIndex = $this->generateOrderOverviewSum($data['SUMS'], $rowIndex);
            $objSheet->getStyle('A' . ($rowIndex -  2) . ':' . 'N' . ($rowIndex - 1))->applyFromArray(array(
                'borders' => array(
                    'top' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('argb' => '00000000'),
                    ),
                    'bottom' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('argb' => '00000000'),
                    ),
                    'left' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('argb' => '00000000'),
                    ),
                    'right' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('argb' => '00000000'),
                    )
                )
            ));
        }

        return $this->getContentXlsx();
    }

    private function generateOrder($order, $rowIndex)
    {
        $objSheet = $this->activeSheet;
        $skipHeaderGeneration = false;
        $rangeBackgroundColor = false;

        if (isset($order['ORDER_POSITIONS'])) {
            $rowIndex = $this->createOrderOverviewDataRow($order, $this->orderOverviewOrderColumns, $rowIndex);
            $rowStyle = $this->getOrderOverviewStyle(array('backgroundColor' => $this->getOrderBackgroundColor()));
            $objSheet->getStyle('A' . ($rowIndex - 2). ':' . 'N' . ($rowIndex - 1))->applyFromArray($rowStyle);
            $startPositionsRowIndex = $rowIndex;

            foreach ($order['ORDER_POSITIONS'] as $orderPositionNumber => $orderPositionData) {
                if ($skipHeaderGeneration === false) {
                    $rowIndex = $this->createOrderOverviewColumnHeaders($this->orderOverviewOrderPositionsColumns, $rowIndex, $this->getOrderPositionBackgroundColor());
                }

                $previousPositionRowIndex = $rowIndex;
                $rowIndex = $this->generateOrderPosition($orderPositionData, $rowIndex);

                if ($rowIndex - 1 === $previousPositionRowIndex) {
                    $skipHeaderGeneration = true;
                } else {
                    $skipHeaderGeneration = false;
                    $rangeBackgroundColor = true;

                    $rowStyle = $this->getOrderOverviewStyle(array('backgroundColor' => $this->getOrderPositionBackgroundColor()));
                    $this->activeSheet->getStyle('A' . $startPositionsRowIndex . ':' . 'N' . $previousPositionRowIndex)->applyFromArray($rowStyle);

                    $startPositionsRowIndex = $rowIndex;
                }
            }

            if ($rangeBackgroundColor === false) {
                $rowStyle = $this->getOrderOverviewStyle(array('backgroundColor' => $this->getOrderPositionBackgroundColor()));
                $this->activeSheet->getStyle('A' . $startPositionsRowIndex . ':' . 'N' . $previousPositionRowIndex)->applyFromArray($rowStyle);
            }
        }
        return $rowIndex;
    }

    private function generateOrderPosition($orderPosition, $rowIndex)
    {
        $milestonesCounter = 0;

        $rowIndex = $this->createOrderOverviewDataRow($orderPosition, $this->orderOverviewOrderPositionsColumns, $rowIndex);
        if (isset($orderPosition['INVOICE_PLAN_ELEMENTS'])) {
            $startRowIndexOfInvoicePlanElements = $rowIndex;

            foreach ($orderPosition['INVOICE_PLAN_ELEMENTS'] as $invoicePlanElementNumber => $invoicePlanElementData) {
                if ($milestonesCounter === 0) {
                    $rowIndex = $this->createOrderOverviewColumnHeaders($this->orderOverviewInvoicePlanElementColumns, $rowIndex, $this->getInvoicePlanElementBackgroundColor());
                }
                $rowIndex = $this->createOrderOverviewDataRow($invoicePlanElementData, $this->orderOverviewInvoicePlanElementColumns, $rowIndex);
                $milestonesCounter++;
            }

            $rowStyle = $this->getOrderOverviewStyle(array('backgroundColor' => $this->getInvoicePlanElementBackgroundColor()));
            $this->activeSheet->getStyle('A' . $startRowIndexOfInvoicePlanElements . ':' . 'N' . ($rowIndex - 1))->applyFromArray($rowStyle);
        }

        return $rowIndex;
    }

    private function generateOrderOverviewSum($sums, $rowIndex)
    {
        $rowIndex = $this->createOrderOverviewColumnHeaders($this->orderOverviewSumsColumns, $rowIndex);
        $rowIndex = $this->createOrderOverviewDataRow($sums, $this->orderOverviewSumsColumns, $rowIndex);

        return $rowIndex;
    }

    private function createOrderOverviewColumnHeaders(&$columns, $rowIndex, $rowColor = null)
    {
        $colIndex = 0;
        $objSheet = $this->activeSheet;

        // create column headers
        foreach ($columns as &$colHeader) {
            $colOffset = isset($colHeader['colOffset']) ? $colHeader['colOffset'] : 0;
            $colIndex = $colIndex + $colOffset;
            $colHeader['index'] = $colIndex;
            $colHeader['indexString'] = PHPExcel_Cell::stringFromColumnIndex($colIndex++);

            $objSheet->setCellValueByColumnAndRow($colHeader['index'], $rowIndex, $colHeader['title']);

            if (isset($colHeader['styles']) && is_array($colHeader['styles'])) {
                $cellStyle = $this->getOrderOverviewStyle($colHeader['styles'], 'header');
                $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($colHeader['index'] - $colOffset) . $rowIndex . ':' . $colHeader['indexString'] . $rowIndex)->applyFromArray($cellStyle);
            }
        }

        $firstColumn = reset($columns);
        $lastColumn = end($columns);

        return $rowIndex + 1;
    }

    private function createOrderOverviewDataRow($data, $columns, $rowIndex, $rowColor = null)
    {
        $colIndex = 0;
        $objSheet = $this->activeSheet;

        if ($rowColor !== null) {
            $backgroundStyle = $this->getOrderOverviewStyle(array('backgroundColor' => $rowColor));
            $objSheet->getStyle('A' . $rowIndex . ':' . 'N' . $rowIndex)->applyFromArray($backgroundStyle);
        }

        foreach ($columns as $columnDataKey => &$colHeader) {
            $colOffset = isset($colHeader['colOffset']) ? $colHeader['colOffset'] : 0;
            $colIndex = $colIndex + $colOffset;
            $colHeader['index'] = $colIndex;
            $colHeader['indexString'] = PHPExcel_Cell::stringFromColumnIndex($colIndex++);

            if (isset($data[$columnDataKey])) {
                $value = $data[$columnDataKey];

                if (isset($colHeader['renderer'])) {
                    $value = $this->{$colHeader['renderer']}($value, $data);
                }

                if (isset($colHeader['displayZeroValues']) && $colHeader['displayZeroValues'] === true && $value == 0) {
                    $value = null;
                }

                $objSheet->setCellValueByColumnAndRow($colHeader['index'], $rowIndex, $value);
                //Set cell specific styles
                if (isset($colHeader['styles']) && is_array($colHeader['styles'])) {
                    $cellStyle = $this->getOrderOverviewStyle($colHeader['styles']);
                    $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($colHeader['index'] - $colOffset) . $rowIndex . ':' . $colHeader['indexString'] . $rowIndex)->applyFromArray($cellStyle);
                }
            }
        }

        return $rowIndex + 1;
    }

    private function getOrderOverviewStyle($styles, $context = 'data', $defaultStyles = array())
    {
        $styleConfigs = array_merge($defaultStyles, $styles);
        $phpExcelStyleConfig = array();

        if (isset($styleConfigs['bold']) && $styleConfigs['bold'] === true) {
            $phpExcelStyleConfig['font'] = array(
                'bold' => true
            );
        }

        if ($context === 'data' && isset($styleConfigs['dataTextAlign']) && is_array($styleConfigs['dataTextAlign'])) {
            $phpExcelStyleConfig['alignment'] = $styleConfigs['dataTextAlign'];
        }

        if ($context === 'header' && isset($styleConfigs['headerTextAlign']) && is_array($styleConfigs['headerTextAlign'])) {
            $phpExcelStyleConfig['alignment'] = $styleConfigs['headerTextAlign'];
        }

        if (!empty($styleConfigs['backgroundColor'])) {
            $phpExcelStyleConfig['fill'] = array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => $styleConfigs['backgroundColor']
                )
            );
        }

        if (isset($styleConfigs['numberFormat'])) {
            $phpExcelStyleConfig['numberformat'] = array(
                'code' => $styleConfigs['numberFormat']
            );
        }

        return $phpExcelStyleConfig;
    }

    private function statusRenderer($value)
    {
        $lang = &$this->translate;
        $status = '';

        $status = $lang->translate(InvoiceType::getTranslation($value));
        return $status;
    }

    private function sapKeyRenderer($value, $data)
    {
        if (isset($data['BOOKING_TYPE_NAME'])) {
            $value = $value . ' (' . $data['BOOKING_TYPE_NAME']. ')';
        }

        return $value;
    }

    private function btBsRenderer($value, $data)
    {
        if (!empty($data['OUTSTANDING'])) {
            return $value;
        }
        return false;
    }

    private function orderKindRenderer($value)
    {
        $lang = &$this->translate;
        $orderKind = '';

        switch ($value) {
            case 'ZMF':
                $orderKind = $lang->translate('orderOverviewMilestones');
                break;
            case 'ZCP':
                $orderKind = $lang->translate('orderOverviewProjectOrder');
                break;
        }

        return $orderKind;
    }

    private function setHorizontalextAlignment($alignValue, $rowIndex, $colIndex)
    {
        switch ($alignValue) {
            case 'right':
                $phpExcelAlign = PHPExcel_Style_Alignment::HORIZONTAL_RIGHT;
                break;
            case 'center':
                $phpExcelAlign = PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
                break;
            default:
                $phpExcelAlign = PHPExcel_Style_Alignment::HORIZONTAL_LEFT;
                break;
        }
        $this->activeSheet->getStyleByColumnAndRow($colIndex, $rowIndex)->getAlignment()->setHorizontal($phpExcelAlign);
    }

    private function getOrderBackgroundColor()
    {
        return 'FFA6A6A6';
    }

    private function getOrderPositionBackgroundColor()
    {
        return 'FFDCE6F1';
    }

    private function getInvoicePlanElementBackgroundColor()
    {
        return 'DDC4D79B';
    }

    private function createHorizontalRule(PHPExcel_Worksheet $objSheet, &$idx, $usedMonths)
    {
        $offset = 8;
        $style = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => 'D3D3D3D3'
                )
            )
        );

        $objSheet->setCellValueByColumnAndRow(0, $idx, " ");
        $objSheet->getStyle("A" . ($idx) . ':' . "D" . ($idx))->applyFromArray($style);
        $vertical = 0;
        foreach ($usedMonths as $month) {
            $objSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex(4 + $offset * ($vertical)) . ($idx) . ':' . PHPExcel_Cell::stringFromColumnIndex(12 + $offset * ($vertical)) . ($idx))
                ->applyFromArray($style);
            $vertical++;
        }
        $idx++;
    }

    private function createIndentedNodes(PHPExcel_Worksheet $objSheet, $usedMonths, $boldStyle, &$idx, $projectData)
    {
        $lang = $this->translate;
        $informationArray = $this->view->data['INFORMATION'];
        $offset = 8;

        foreach ($projectData as $project) {
            $level = $project['level'];
            $name = $project['node_name'];

            if ($level == 1) {
                $this->createHorizontalRule($objSheet, $idx, $usedMonths);
            }
            $objSheet->getStyleByColumnAndRow(0, $idx)->getAlignment()
                ->setWrapText(true)
                ->setIndent($level);
            $objSheet->getStyleByColumnAndRow(0, $idx)->getFont()->setItalic(true);
            $objSheet->setCellValueByColumnAndRow(0, $idx, $name);

            $objSheet->setCellValueByColumnAndRow(1, $idx, strtoupper($informationArray["FP"]));
            $objSheet->getStyle('B' . $idx)->applyFromArray($boldStyle);
            $objSheet->setCellValueByColumnAndRow(2, $idx, $project['company']);
            $objSheet->getStyle('C' . $idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objSheet->setCellValueByColumnAndRow(3, $idx, null);
            $objSheet->setCellValueByColumnAndRow(4, $idx, @$project['target_effort']);
            $objSheet->getStyleByColumnAndRow(4, $idx)->getNumberFormat()->setFormatCode('#,##0.00');

            $bookingTypeDetermined = false;
            $bookingTypeName = "FP";

            $vertical = 0;
            foreach ($usedMonths as $month) {
                $expenseComment = "";

                $bookingTypeHoursMonth = 0;
                $bookingTypeHoursBegin = 0;

                if ($bookingTypeDetermined == false) {
                    foreach ($project['months'][$month]['hours_bt_year'] as $btName => $btHours) {
                        if ($btHours > $bookingTypeHoursBegin) {
                            $bookingTypeHoursBegin = $btHours;
                            $bookingTypeName = $btName;
                        }
                    }

                    switch ($bookingTypeName) {
                        case "CR":
                            $objSheet->setCellValueByColumnAndRow(1, $idx, strtoupper($informationArray["CR"]));
                            break;
                        case "NA":
                            $objSheet->setCellValueByColumnAndRow(1, $idx, strtoupper($informationArray["NA"]));
                            break;
                        case "TM":
                            $objSheet->setCellValueByColumnAndRow(1, $idx, strtoupper($informationArray["TM"]));
                            break;
                        case "MC":
                            $objSheet->setCellValueByColumnAndRow(1, $idx, strtoupper($informationArray["MC"]));
                            break;
                    }
                    $bookingTypeDetermined = true;
                }

                $bookingTypeHoursMonth = $project['months'][$month]['hours_bt_month'][$bookingTypeName];
                $bookingTypeHoursBegin = $project['months'][$month]['hours_bt_begin'][$bookingTypeName];

                $expenseCommentHeight = 0;
                if ($bookingTypeHoursMonth != array_sum($project['months'][$month]['hours_bt_month'])) {
                    foreach ($project['months'][$month]['hours_bt_month'] as $key => $val) {
                        if ($val > 0) {
                            $expenseComment .= $informationArray[$key] . " (" . $key . "): " . number_format($val, 2, ',', '.') . "\n";
                            $expenseCommentHeight += 20;
                        }
                    }
                    $expenseCommentHeight += 10;
                }
                if (array_sum($project['months'][$month]['hours_bt_trainee_month']) > 0) {
                    if ($expenseCommentHeight > 0) {
                        $expenseComment .= "\n";
                        $expenseCommentHeight += 20;
                    }
                    $expenseComment .= $lang->translate("traineeHours") . ":\n";
                    foreach ($project['months'][$month]['hours_bt_trainee_month'] as $key => $val) {
                        if ($val > 0) {
                            $expenseComment .= $informationArray[$key] . " (" . $key . "): " . number_format($val, 2, ',', '.') . "\n";
                            $expenseCommentHeight += 20;
                        }
                    }
                    $expenseCommentHeight += 30;
                }

                $expenseCommentYear = "";
                $expenseCommentYearHeight = 0;

                if ($bookingTypeHoursBegin != array_sum($project['months'][$month]["hours_bt_begin"])) {
                    foreach ($project['months'][$month]["hours_bt_begin"] as $key => $val) {
                        if ($val > 0) {
                            $expenseCommentYear .= $informationArray[$key] . " (" . $key . "): " . number_format($val, 2, ',', '.') . "\n";
                            $expenseCommentYearHeight += 20;
                        }
                    }
                    $expenseCommentYearHeight += 10;
                }
                if (array_sum($project['months'][$month]["hours_bt_trainee_begin"]) > 0) {
                    if ($expenseCommentYearHeight > 0) {
                        $expenseCommentYear .= "\n";
                        $expenseCommentYearHeight += 20;
                    }
                    $expenseCommentYear .= $lang->translate("traineeHours") . ":\n";
                    foreach ($project['months'][$month]["hours_bt_trainee_begin"] as $key => $val) {
                        if ($val > 0) {
                            $expenseCommentYear .= $informationArray[$key] . " (" . $key . "): " . number_format($val, 2, ',', '.') . "\n";
                            $expenseCommentYearHeight += 20;
                        }
                    }
                    $expenseCommentYearHeight += 30;
                }

                //Ist-Aufwand im Zeitraum
                $objSheet->setCellValueByColumnAndRow(5 + $offset * ($vertical), $idx, $bookingTypeHoursMonth);
                $objSheet->getStyleByColumnAndRow(5 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                if ($expenseComment) {
                    $objSheet->getComment(PHPExcel_Cell::stringFromColumnIndex(5 + $offset * ($vertical)) . $idx)
                        ->setWidth(350)->setHeight($expenseCommentHeight)
                        ->getText()->createTextRun($expenseComment);
                }

                //Abgerechnet im Zeitraum
                $objSheet->setCellValueByColumnAndRow(6 + $offset * ($vertical), $idx, null);
                $objSheet->getStyleByColumnAndRow(6 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                //Ist-Aufwand seit (Jahres-)Beginn
                $objSheet->setCellValueByColumnAndRow(7 + $offset * ($vertical), $idx, $bookingTypeHoursBegin);
                $objSheet->getStyleByColumnAndRow(7 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                if ($expenseCommentYear) {
                    $objSheet->getComment(PHPExcel_Cell::stringFromColumnIndex(7 + $offset * ($vertical)) . $idx)
                        ->setWidth(350)->setHeight($expenseCommentYearHeight)
                        ->getText()->createTextRun($expenseCommentYear);
                }
                //Abgerechnet seit 1.1.
                $objSheet->setCellValueByColumnAndRow(8 + $offset * ($vertical), $idx, null);
                $objSheet->getStyleByColumnAndRow(8 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                //Rest-Aufwand
                $objSheet->setCellValueByColumnAndRow(9 + $offset * ($vertical), $idx, $project['months'][$month]['remaining_effort']);
                $objSheet->getStyleByColumnAndRow(9 + $offset * ($vertical), $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                $remainingEffortComment = $project['months'][$month]['remaining_effort_comment'];
                if ($remainingEffortComment) {
                    $objSheet->getComment(PHPExcel_Cell::stringFromColumnIndex(9 + $offset * ($vertical)) . $idx)
                        ->getText()->createTextRun($remainingEffortComment);
                }
                //Abweichung
                $objSheet->setCellValueByColumnAndRow(
                    10 + $offset * ($vertical),
                    $idx,
                    "=IF(AND(" . PHPExcel_Cell::stringFromColumnIndex(4) . $idx . "<>0,NOT(ISBLANK(" . PHPExcel_Cell::stringFromColumnIndex(9 + $offset * ($vertical)) . $idx . "))),((" .
                    PHPExcel_Cell::stringFromColumnIndex(7 + $offset * ($vertical)) . $idx . "+" .
                    PHPExcel_Cell::stringFromColumnIndex(9 + $offset * ($vertical)) . $idx . "-" .
                    PHPExcel_Cell::stringFromColumnIndex(4) . $idx . ")/" .
                    PHPExcel_Cell::stringFromColumnIndex(4) . $idx . "),\"\")"
                );
                $objSheet->getStyleByColumnAndRow(10 + $offset * ($vertical), $idx)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
                //Abgerechnet im Zeitraum
                $objSheet->setCellValueByColumnAndRow(11 + $offset * ($vertical), $idx, null);
                $objSheet->getStyleByColumnAndRow(11 + $offset * ($vertical), $idx)->getNumberFormat()
                    ->setFormatCode('#,##0.00_-[$€ ]');
                //Abgerechnet seit (Jahres-)Beginn
                $objSheet->setCellValueByColumnAndRow(12 + $offset * ($vertical), $idx, null);
                $objSheet->getStyleByColumnAndRow(12 + $offset * ($vertical), $idx)->getNumberFormat()
                    ->setFormatCode('#,##0.00_-[$€ ]');
                $vertical++;
            }
            $idx++;
        }
    }

    /**
     *  Export 'MIS Division' report.
     */
    public function exportMisDivision($asWorksheet = false)
    {
        // Get the active sheet
        $objSheet = $this->objPHPExcel->getActiveSheet();

        //Set up page setup
        $this->setUpPageSetup();

        $lang = &$this->translate;

        // prepare data
        $tsFrom = $this->view->data['REPORT_INFO']['PERIODFROM'];
        $dateFrom = new Zend_Date();
        $dateFrom->setTimestamp($tsFrom);

        $tsUntil = $this->view->data['REPORT_INFO']['PERIODUNTIL'];
        $dateUntil = new Zend_Date();
        $dateUntil->setTimestamp($tsUntil);

        $firstDateOfYear = clone $dateFrom;
        $firstDateOfYear->setDayOfYear(1);

        $targetHourlyRate = $this->view->data['REPORT_INFO']['TARGET_HOURLY_RATE'];

        // prepare style arrays
        $headerStyle = $this->getReportConfigStyle();
        $tableHeaderStyle = $this->getContentHeaderStyle();
        $tableHeaderStyle['alignment']['horizontal'] = PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
        $sectionTotalStyle = array(
            'font' => array(
                'bold' => true
            ),
            'borders' => array(
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
            ),
        );
        $subTotalStyle = array_merge($sectionTotalStyle, array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => 'FFDDDDDD'
                )
            )
        ));
        $totalStyle = array_merge($sectionTotalStyle, array(
            'font' => array(
                'bold' => true,
                'color' => array(
                    'argb' => 'FFFFFFFF'
                )
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'argb' => '00000000'
                )
            )
        ));

        // general sheet config
        $this->setUpPageSetup();
        $objSheet->getPageSetup()->setFitToHeight(0);

        // create report header
        $idx = $this->createHeader('misDivision') - 2;
        $objSheet->getStyleByColumnAndRow(0, $idx)->applyFromArray($headerStyle);
        $objSheet->setCellValueByColumnAndRow(0, $idx, $this->view->escape($lang->translate('division') . ":"));
        $objSheet->setCellValueByColumnAndRow(1, $idx++, $this->view->data['REPORT_INFO']['DIVISION']);

        $idx += 2;

        $showCorrections = (
            $dateFrom->equals($firstDateOfYear) &&
            $dateFrom->get(Zend_Date::YEAR) == $dateUntil->get(Zend_Date::YEAR)
        );

        // columns definitions
        $columns = array(
            'name'                    => array('title' => $lang->translate('misNodeName'),            'width' => null,'show' => true),
            'debtor_number'            => array('title' => $lang->translate('misDebtorNumber'),        'width' => -1,    'show' => true),
            'act_hours'                => array('title' => $lang->translate('misActHours'),            'width' => -1,    'show' => true,    'format' => '#,##0.00'),
            'net_sales'                => array('title' => $lang->translate('misNetSales'),            'width' => -1,    'show' => true, 'format' => '#,##0.00'),
            'cost'                    => array('title' => $lang->translate('misCost'),                'width' => -1,    'show' => true, 'format' => '#,##0.00'),
            'gross_profit'            => array('title' => $lang->translate('misGrossProfit'),            'width' => -1,    'show' => true, 'format' => '#,##0.00'),
            'accrual_begin_value'    => array('title' => $lang->translate('misAccrualBegin'),        'width' => -1,    'show' => true, 'format' => '#,##0.00'),
            'accrual_end_value'        => array('title' => $lang->translate('misAccrualEnd'),            'width' => -1,    'show' => true,    'format' => '#,##0.00'),
            'cost_correction_start'        => array('title' => $lang->translate('misCostCorrectionStart'),        'width' => -1,    'show' => $showCorrections, 'format' => '#,##0.00'),
            'transaction_correction' => array('title' => $lang->translate('misTransactionCorrection'),'width' => -1,    'show' => $showCorrections, 'format' => '#,##0.00'),
            'cost_correction_end'        => array('title' => $lang->translate('misCostCorrectionEnd'),        'width' => -1,    'show' => $showCorrections, 'format' => '#,##0.00'),
            'hourly_rate'            => array('title' => $lang->translate('misHourlyRate'),            'width' => -1,    'show' => true,    'format' => '0.00')
        );

        // create column headers
        $colIdx = 0;
        foreach ($columns as &$colHeader) {
            if (true !== $colHeader['show']) {
                continue;
            }

            $objSheet->getStyleByColumnAndRow($colIdx, $idx)->applyFromArray($tableHeaderStyle);
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $colHeader['title']);

            $colHeader['i'] = $colIdx;
            $colHeader['c'] = PHPExcel_Cell::stringFromColumnIndex($colIdx++);

            // set column width
            if (isset($colHeader['width'])) {
                if (-1 === $colHeader['width']) {
                    $objSheet->getColumnDimension($colHeader['c'])->setAutoSize(true);
                } else {
                    $objSheet->getColumnDimension($colHeader['c'])->setWidth($colHeader['width']);
                }
            }
            // set column format
            if (isset($colHeader['format'])) {
                // we cannot simply adders the whole column here since those styles
                // can be overridden when other styles are applied to the cell
                # great stuff, defines the cell format based on number of rows and a hard-coded number of lines in header (header must never change...)
                $numberOfRowsToApplyFormat = $idx + 10 + $this->view->data['COUNT'];
                $objSheet->getStyle($colHeader['c'] . ($idx + 1) . ":" . $colHeader['c'] . $numberOfRowsToApplyFormat)
                    ->getNumberFormat()->setFormatCode($colHeader['format']);
            }
        }

        // create report body
        $data = $this->view->data['DATA'];

        $sumRows = array();

        $count = sizeof($data);
        foreach ($data as $section => $nodes) {
            // render section node data
            $idxStart = $idx + 1;

            $reducingHours = [
                'trainee' => [
                    'hours' => 0,
                    'column' => 'ACT_HOURS_TRAINEE',
                    'text' => $lang->translate('misSubtractTraineeHours')
                ],
                'student' => [
                    'hours' => 0,
                    'column' => 'ACT_HOURS_STUDENT',
                    'text' => $lang->translate('misSubtractStudentHours')
                ],
                'intern' => [
                    'hours' => 0,
                    'column' => 'ACT_HOURS_INTERN',
                    'text' => $lang->translate('misSubtractInternHours')
                ]
            ];

            foreach ($nodes as $node) {
                $idx += 1;
                // common columns
                $objSheet->setCellValueByColumnAndRow($columns['name']['i'], $idx, $node['NAME']);
                $objSheet->setCellValueByColumnAndRow($columns['act_hours']['i'], $idx, $node['ACT_HOURS']);
                $objSheet->setCellValueByColumnAndRow($columns['cost']['i'], $idx, $node['COST']);

                // columns per section
                switch ($node['SECTION']) {
                    case 0: // debtor part
                        $objSheet->setCellValueByColumnAndRow($columns['debtor_number']['i'], $idx, $node['DEBTOR_NUMBER']);
                        $objSheet->setCellValueByColumnAndRow($columns['net_sales']['i'], $idx, $node['NET_SALES']);
                        $objSheet->setCellValueByColumnAndRow($columns['gross_profit']['i'], $idx, "=(" . $columns['net_sales']['c'] . $idx . "-" . $columns['cost']['c'] . $idx . "-" . $columns['accrual_begin_value']['c'] . $idx . "+" . $columns['accrual_end_value']['c'] . $idx . ($showCorrections === true ? ("-" . $columns['cost_correction_start']['c'] . $idx . "+" . $columns['transaction_correction']['c'] . $idx . "+" . $columns['cost_correction_end']['c'] . $idx) : '') . ")");
                        $objSheet->setCellValueByColumnAndRow($columns['accrual_begin_value']['i'], $idx, $node['ACCRUAL_BEGIN_VALUE']);
                        if (isset($node['ACCRUAL_BEGIN_COMMENT']) && !empty($node['ACCRUAL_BEGIN_COMMENT'])) {
                            $objSheet->getCommentByColumnAndRow($columns['accrual_begin_value']['i'], $idx)->getText()->createTextRun($node['ACCRUAL_BEGIN_COMMENT']);
                        }
                        $objSheet->setCellValueByColumnAndRow($columns['accrual_end_value']['i'], $idx, $node['ACCRUAL_END_VALUE']);
                        if (isset($node['ACCRUAL_END_COMMENT'])  && !empty($node['ACCRUAL_END_COMMENT'])) {
                            $objSheet->getCommentByColumnAndRow($columns['accrual_end_value']['i'], $idx)->getText()->createTextRun($node['ACCRUAL_END_COMMENT']);
                        }
                        if (true === $showCorrections) {
                            $objSheet->setCellValueByColumnAndRow($columns['cost_correction_start']['i'], $idx, $node['COST_CORRECTION_START']);
                            $objSheet->setCellValueByColumnAndRow($columns['cost_correction_end']['i'], $idx, $node['COST_CORRECTION_END']);
                            $objSheet->setCellValueByColumnAndRow($columns['transaction_correction']['i'], $idx, $node['TRANSACTION_CORRECTION']);
                            if (isset($node['COMMENT_CORRECTION_START'])  && !empty($node['COMMENT_CORRECTION_START'])
                                && isset($node['COST_CORRECTION_START'])  && !empty($node['COST_CORRECTION_START'])) {
                                $objSheet->getCommentByColumnAndRow($columns['cost_correction_start']['i'], $idx)->getText()->createTextRun($node['COMMENT_CORRECTION_START']);
                            }
                            if (isset($node['COMMENT_CORRECTION_END'])  && !empty($node['COMMENT_CORRECTION_END'])) {
                                if (isset($node['COST_CORRECTION_END'])  && !empty($node['COST_CORRECTION_END'])) {
                                    $objSheet->getCommentByColumnAndRow($columns['cost_correction_end']['i'], $idx)->getText()->createTextRun($node['COMMENT_CORRECTION_END']);
                                }
                                if (isset($node['TRANSACTION_CORRECTION'])  && !empty($node['TRANSACTION_CORRECTION'])) {
                                    $objSheet->getCommentByColumnAndRow($columns['transaction_correction']['i'], $idx)->getText()->createTextRun($node['COMMENT_CORRECTION_END']);
                                }
                            }
                        }
                        $objSheet->setCellValueByColumnAndRow($columns['hourly_rate']['i'], $idx, "=IF(" . $columns['act_hours']['c'] . $idx . '=0,"",' . $columns['gross_profit']['c'] . $idx . "/" . $columns['act_hours']['c'] . $idx . ")");
                        break;
                    case SingleNodeType::MIS_SALES:
                    case SingleNodeType::MIS_INTERNAL:
                        $objSheet->setCellValueByColumnAndRow($columns['net_sales']['i'], $idx, $node['NET_SALES']);
                        $objSheet->setCellValueByColumnAndRow($columns['gross_profit']['i'], $idx, "=(" . $columns['net_sales']['c'] . $idx . "-" . $columns['cost']['c'] . $idx . ")");
                        break;
                }
                foreach ($reducingHours as $employeeStatus => $values) {
                    $reducingHours[$employeeStatus]['hours'] +=  $node[$values['column']];
                }
            }

            // render section summary row(s)
            $idx += 1;

            if (!empty($nodes)) {
                foreach ($reducingHours as $employeeStatus => $values) {
                    $text = $reducingHours[$employeeStatus]['text'];
                    switch($section) {
                        case SingleNodeType::MIS_INTERNAL:
                            $text .= $lang->translate('misSuffixInternal');
                            break;
                        case SingleNodeType::MIS_SALES:
                            $text .= $lang->translate('misSuffixSales');
                            break;
                        default:
                            # debtor section
                            $text .= $lang->translate('misSuffixDebtor');
                    }
                    if ($reducingHours[$employeeStatus]['hours'] > 0) {
                        $sumHours = floor(0.5 * $reducingHours[$employeeStatus]['hours'] * 4) / 4;
                        $objSheet->setCellValueByColumnAndRow(0, $idx, $text);
                        $objSheet->setCellValueByColumnAndRow($columns['act_hours']['i'], $idx++, - $sumHours);
                    }
                }
            }

            // subtotals
            $objSheet->getStyle('A' . $idx . ':' . $columns['hourly_rate']['c'] . $idx)->applyFromArray($sectionTotalStyle);

            $objSheet->setCellValueByColumnAndRow($columns['act_hours']['i'], $idx, empty($nodes) ? 0 : "=SUM(" . $columns['act_hours']['c'] . $idxStart . ":" . $columns['act_hours']['c'] . ($idx - 1) . ")");
            $objSheet->setCellValueByColumnAndRow($columns['cost']['i'], $idx, empty($nodes) ? 0 : "=SUM(" . $columns['cost']['c'] . $idxStart . ":" . $columns['cost']['c'] . ($idx - 1) . ")");
            $objSheet->setCellValueByColumnAndRow($columns['gross_profit']['i'], $idx, empty($nodes) ? 0 : "=SUM(" . $columns['gross_profit']['c'] . $idxStart . ":" . $columns['gross_profit']['c'] . ($idx - 1) . ")");

            switch ($section) {
                case 0: // debtor part
                    $objSheet->setCellValueByColumnAndRow(0, $idx, $lang->translate('misSumDebtors'));
                    if (empty($nodes)) {
                        break;
                    }
                    $objSheet->setCellValueByColumnAndRow($columns['net_sales']['i'], $idx, empty($nodes) ? 0 : "=SUM(" . $columns['net_sales']['c'] . $idxStart . ":" . $columns['net_sales']['c'] . ($idx - 1) . ")");
                    $objSheet->setCellValueByColumnAndRow($columns['accrual_begin_value']['i'], $idx, empty($nodes) ? 0 : "=SUM(" . $columns['accrual_begin_value']['c'] . $idxStart . ":" . $columns['accrual_begin_value']['c'] . ($idx - 1) . ")");
                    $objSheet->setCellValueByColumnAndRow($columns['accrual_end_value']['i'], $idx, empty($nodes) ? 0 : "=SUM(" . $columns['accrual_end_value']['c'] . $idxStart . ":" . $columns['accrual_end_value']['c'] . ($idx - 1) . ")");
                    if (true === $showCorrections) {
                        $objSheet->setCellValueByColumnAndRow($columns['cost_correction_start']['i'], $idx, empty($nodes) ? 0 : "=SUM(" . $columns['cost_correction_start']['c'] . $idxStart . ":" . $columns['cost_correction_start']['c'] . ($idx - 1) . ")");
                        $objSheet->setCellValueByColumnAndRow($columns['cost_correction_end']['i'], $idx, empty($nodes) ? 0 : "=SUM(" . $columns['cost_correction_end']['c'] . $idxStart . ":" . $columns['cost_correction_end']['c'] . ($idx - 1) . ")");
                        $objSheet->setCellValueByColumnAndRow($columns['transaction_correction']['i'], $idx, empty($nodes) ? 0 : "=SUM(" . $columns['transaction_correction']['c'] . $idxStart . ":" . $columns['transaction_correction']['c'] . ($idx - 1) . ")");
                    }
                    $objSheet->setCellValueByColumnAndRow($columns['hourly_rate']['i'], $idx, empty($nodes) ? 0 : "=IF(" . $columns['act_hours']['c'] . $idx . '=0,"",' . $columns['gross_profit']['c'] . $idx . "/" . $columns['act_hours']['c'] . $idx . ")");
                    break;
                case SingleNodeType::MIS_INTERNAL:
                    $objSheet->setCellValueByColumnAndRow($columns['net_sales']['i'], $idx, empty($nodes) ? 0 : "=SUM(" . $columns['net_sales']['c'] . $idxStart . ":" . $columns['net_sales']['c'] . ($idx - 1) . ")");
                    $objSheet->setCellValueByColumnAndRow(0, $idx, $lang->translate('misSumInternal'));
                    break;
                case SingleNodeType::MIS_SALES:
                    # format cells as number to get rounded values
                    $objSheet->getStyle('C' . ($idx) . ':' . $columns['hourly_rate']['c'] . $idx)->getNumberFormat()->setFormatCode('#,##0.00');
                    $objSheet->setCellValueByColumnAndRow($columns['net_sales']['i'], $idx, empty($nodes) ? 0 : "=SUM(" . $columns['net_sales']['c'] . $idxStart . ":" . $columns['net_sales']['c'] . ($idx - 1) . ")");
                    $objSheet->setCellValueByColumnAndRow(0, $idx, $lang->translate('misSumSales'));
                    break;
            }
            $sumRows[$section] = $idx;

            // subtotal row for debtor part + internal part
            if ($section == SingleNodeType::MIS_INTERNAL) {
                $objSheet->getStyle('A' . ++$idx . ':' . $columns['hourly_rate']['c'] . $idx)->applyFromArray($subTotalStyle);
                $objSheet->setCellValueByColumnAndRow(0, $idx, $lang->translate('misSubTotalDebtorsInternal'));
                $objSheet->setCellValueByColumnAndRow($columns['act_hours']['i'], $idx, "=(" . $columns['act_hours']['c'] . $sumRows[0] . "+" . $columns['act_hours']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . ")");
                $objSheet->setCellValueByColumnAndRow($columns['net_sales']['i'], $idx, "=(" . $columns['net_sales']['c'] . $sumRows[0] . "+" . $columns['net_sales']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . ")");
                $objSheet->setCellValueByColumnAndRow($columns['cost']['i'], $idx, "=(" . $columns['cost']['c'] . $sumRows[0] . "+" . $columns['cost']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . ")");
                $objSheet->setCellValueByColumnAndRow($columns['gross_profit']['i'], $idx, "=(" . $columns['gross_profit']['c'] . $sumRows[0] . "+" . $columns['gross_profit']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . ")");
                $objSheet->setCellValueByColumnAndRow($columns['accrual_begin_value']['i'], $idx, "=(" . $columns['accrual_begin_value']['c'] . $sumRows[0] . "+" . $columns['accrual_begin_value']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . ")");
                $objSheet->setCellValueByColumnAndRow($columns['accrual_end_value']['i'], $idx, "=(" . $columns['accrual_end_value']['c'] . $sumRows[0] . "+" . $columns['accrual_end_value']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . ")");
                if (true === $showCorrections) {
                    $objSheet->setCellValueByColumnAndRow($columns['cost_correction_start']['i'], $idx, "=(" . $columns['cost_correction_start']['c'] . $sumRows[0] . "+" . $columns['cost_correction_start']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . ")");
                    $objSheet->setCellValueByColumnAndRow($columns['cost_correction_end']['i'], $idx, "=(" . $columns['cost_correction_end']['c'] . $sumRows[0] . "+" . $columns['cost_correction_end']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . ")");
                    $objSheet->setCellValueByColumnAndRow($columns['transaction_correction']['i'], $idx, "=(" . $columns['transaction_correction']['c'] . $sumRows[0] . "+" . $columns['transaction_correction']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . ")");
                }
                $objSheet->setCellValueByColumnAndRow($columns['hourly_rate']['i'], $idx, "=IF(" . $columns['act_hours']['c'] . $idx . '=0,"",' . $columns['gross_profit']['c'] . $idx . "/" . $columns['act_hours']['c'] . $idx . ")");
            }
        }

        // total row
        $objSheet->getStyle('A' . ++$idx . ':' . $columns['hourly_rate']['c'] . $idx)->applyFromArray($totalStyle);
        # format cells as number to get rounded values
        $objSheet->getStyle('C' . ($idx) . ':' . $columns['hourly_rate']['c'] . $idx)->getNumberFormat()->setFormatCode('#,##0.00');
        $objSheet->setCellValueByColumnAndRow(0, $idx, $lang->translate('misTotalSum'));

        $actHoursSum = $columns['act_hours']['c'] . $sumRows[0] . "+" . $columns['act_hours']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . "+" . $columns['act_hours']['c'] . $sumRows[SingleNodeType::MIS_SALES];
        $netSalesSum = $columns['net_sales']['c'] . $sumRows[0] . "+" . $columns['net_sales']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . "+" . $columns['net_sales']['c'] . $sumRows[SingleNodeType::MIS_SALES];
        $costsSum = $columns['cost']['c'] . $sumRows[0] . "+" . $columns['cost']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . "+" . $columns['cost']['c'] . $sumRows[SingleNodeType::MIS_SALES];
        $grossProfit = $columns['gross_profit']['c'] . $sumRows[0] . "+" . $columns['gross_profit']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . "+" . $columns['gross_profit']['c'] . $sumRows[SingleNodeType::MIS_SALES];
        $accrualBeginValueSum = $columns['accrual_begin_value']['c'] . $sumRows[0] . "+" . $columns['accrual_begin_value']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . "+" . $columns['accrual_begin_value']['c'] . $sumRows[SingleNodeType::MIS_SALES];
        $accrualEndValueSum = $columns['accrual_end_value']['c'] . $sumRows[0] . "+" . $columns['accrual_end_value']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . "+" . $columns['accrual_end_value']['c'] . $sumRows[SingleNodeType::MIS_SALES];
        $hourlyRateFormula = $columns['act_hours']['c'] . $idx . '=0,"",' . $columns['gross_profit']['c'] . $idx . "/" . $columns['act_hours']['c'] . $idx;

        $objSheet->setCellValueByColumnAndRow($columns['act_hours']['i'], $idx, "=(" . $actHoursSum . ")");
        $objSheet->setCellValueByColumnAndRow($columns['net_sales']['i'], $idx, "=(" . $netSalesSum . ")");
        $objSheet->setCellValueByColumnAndRow($columns['cost']['i'], $idx, "=(" . $costsSum . ")");
        $objSheet->setCellValueByColumnAndRow($columns['gross_profit']['i'], $idx, "=(" . $grossProfit . ")");
        $objSheet->setCellValueByColumnAndRow($columns['accrual_begin_value']['i'], $idx, "=(" . $accrualBeginValueSum . ")");
        $objSheet->setCellValueByColumnAndRow($columns['accrual_end_value']['i'], $idx, "=(" . $accrualEndValueSum . ")");
        if (true === $showCorrections) {
            $costCorrectionStart = $columns['cost_correction_start']['c'] . $sumRows[0] . "+" . $columns['cost_correction_start']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . "+" . $columns['cost_correction_start']['c'] . $sumRows[SingleNodeType::MIS_SALES];
            $costCorrectionEnd = $columns['cost_correction_end']['c'] . $sumRows[0] . "+" . $columns['cost_correction_end']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . "+" . $columns['cost_correction_end']['c'] . $sumRows[SingleNodeType::MIS_SALES];
            $transactionCorrectionSum = $columns['transaction_correction']['c'] . $sumRows[0] . "+" . $columns['transaction_correction']['c'] . $sumRows[SingleNodeType::MIS_INTERNAL] . "+" . $columns['transaction_correction']['c'] . $sumRows[SingleNodeType::MIS_SALES];

            $objSheet->setCellValueByColumnAndRow($columns['cost_correction_start']['i'], $idx, "=(" . $costCorrectionStart . ")");
            $objSheet->setCellValueByColumnAndRow($columns['cost_correction_end']['i'], $idx, "=(" . $costCorrectionEnd . ")");
            $objSheet->setCellValueByColumnAndRow($columns['transaction_correction']['i'], $idx, "=(" . $transactionCorrectionSum . ")");
        }
        $objSheet->setCellValueByColumnAndRow($columns['hourly_rate']['i'], $idx, "=IF(" . $hourlyRateFormula . ")");

        $idx++;
        $objSheet->getStyle('A' . ++$idx . ':' . $columns['hourly_rate']['c'] . $idx)->applyFromArray($totalStyle);
        $objSheet->getStyle('C' . ($idx) . ':' . $columns['hourly_rate']['c'] . $idx)->getNumberFormat()->setFormatCode('#,##0.00');
        $objSheet->setCellValueByColumnAndRow(0, $idx, $lang->translate('misTotalSumDelimited'));
        $objSheet->setCellValueByColumnAndRow($columns['net_sales']['i'], $idx, "=(" . "(" . $netSalesSum . ")" . "-" . "(" . $accrualBeginValueSum . ")" . "+" . "(" . $accrualEndValueSum . ")" . ($showCorrections === true ? "-" . "(" . $costCorrectionStart . ")" . "+" . "(". $transactionCorrectionSum . ")" : "") . ")");

        // only return Excel data if the report is not intended to be used in a sheet
        // (i.e. as part of MIS overall)
        if (!$asWorksheet) {
            return $this->getContentXlsx();
        }
    }

    /**
     *  Export 'MIS Overall' report.
     */
    public function exportMisOverall($session)
    {
        $profiles = $this->view->data['DATA'];

        // return early, if there are no profiles to be generated at all
        if (empty($profiles)) {
            return $this->getContentXlsx();
        }

        // remove the default sheet
        $objSheet = $this->objPHPExcel->removeSheetByIndex(0);

        // for each profile create a new sheet and generate the report data
        $sheetIdx = 0;
        foreach ($profiles as $profile) {
            $objSheet = $this->objPHPExcel->createSheet();
            $this->view->data = $session->misDivision[$profile['timestamp']];

            $this->objPHPExcel->setActiveSheetIndex($this->objPHPExcel->getIndex($objSheet));
            $objSheet->setTitle($this->escapeSheetTitle($profile['NAME']));

            $this->exportMisDivision(true);

            // clean up session
            unset($session->misDivision[$profile['timestamp']]);
        }

        // make sure the first sheet is active when the file is opened
        $this->objPHPExcel->setActiveSheetIndex(0);

        return $this->getContentXlsx();
    }

    /**
     * Render the absences of employees.
     * Return the changed row index.
     */
    protected function renderAbsencesOfEmployees(
        $employeesWithAbsences,
        $row,
        $dateFrom,
        $dateUntil,
        $totalDays,
        $startColIndex,
        $objSheet,
        $tableBodyStyle,
        $employeeAsGroups,
        $publicHolidays,
        $absenceCategories
    ) {
        if (true === $employeeAsGroups) {
            $indentation = '    ';
            $row -= 1;
        } else {
            $indentation = '';
        }

        $row -= 1;
        $previousGroup = null;
        $borderTableStyle = $this->getBorderTableStyle();

        foreach ($employeesWithAbsences as $employeeData) {
            $row += 1;

            // render group header if grouping is enabled and the group's name
            // is different from the previous one
            if (true === $employeeAsGroups && $employeeData['group'] != $previousGroup) {
                $groupName = $employeeData['group'];
                if ($groupName == '{SINGLE}') {
                    $groupName = $this->translate->translate('singleSelectedEmp');
                }

                $objSheet->setCellValueByColumnAndRow(0, ++$row, $groupName);
                $objSheet->getStyleByColumnAndRow(0, $row++)->getFont()->setBold(true);

                $previousGroup = $employeeData['group'];
            }
            // render employee

            $objSheet->setCellValueByColumnAndRow(
                0,
                $row,
                $indentation. ($employeeData['shortcut'] ? ( $employeeData['shortcut'] .  ' (' . $employeeData['first_name'] . ' ' . $employeeData['last_name'] . ')') : '')
            );

            //Only at the the first run he should render the public holidays
            $employeeNumberOfAbsences = 0;
            $employeeComments = array();

            foreach ($employeeData['absences'] as $employeeAbsence) {
                //render absence or holidayApplication with state "open"
                if (!empty($employeeAbsence['category_id'])) {
                    $employeeNumberOfAbsences++;

                    $startDate = new DateTime($employeeAbsence['start_date'], new DateTimeZone('UTC'));
                    $startTimestamp = $startDate->format('U');

                    $endDate = new DateTime($employeeAbsence['end_date'], new DateTimeZone('UTC'));
                    $endTimestamp = $endDate->format('U');

                    $startCellIndex = $this->getElapsedWithValidDatePeriod($dateFrom, $startTimestamp, $dateFrom, $dateUntil);
                    $numberOfCells =  $this->getElapsedWithValidDatePeriod($startTimestamp, $endTimestamp, $dateFrom, $dateUntil);

                    $category = $absenceCategories[$employeeAbsence['category_id']];
                    $this->renderCellsWithBackgroundColor(
                        $startCellIndex,
                        $row,
                        $numberOfCells,
                        $category['COLOR'],
                        $objSheet,
                        $startColIndex,
                        $borderTableStyle);

                    if (1 == $category['STRIPED']) {
                        for ($i = 0; $i <= $numberOfCells; $i++) {
                            $objSheet->getStyleByColumnAndRow(($startCellIndex + $i) + $startColIndex, $row)->getFill()
                                ->setFillType(PHPExcel_Style_Fill::FILL_PATTERN_DARKVERTICAL)
                                ->setStartColor(new PHPExcel_Style_Color($category['COLOR']))
                                ->setEndColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));
                        }
                    }

                    if (!empty($employeeAbsence['description'])) {
                        $dt = new Zend_Date($employeeAbsence['start_date']);
                        $employeeComments[($dt->getTimestamp() - $employeeAbsence['priority'])] = array( //values should be sorted by date proportionally  and unproportionally by priority
                            'description' => $employeeAbsence['description'],
                            'start_date' => $employeeAbsence['start_date']
                        );
                    }
                }
            }

            // Render comments at the last column.
            // If one absence exits then only the comment should be displayed
            // If several absences exits then the comment with the start date should be displayed
            if (!empty($employeeComments)) {
                $numberOfComments = count($employeeComments);
                if ($numberOfComments == 1 && $employeeNumberOfAbsences == 1) {
                    $comment = current($employeeComments);
                    $objSheet->setCellValueByColumnAndRow($startColIndex + $totalDays, $row, $comment['description']);
                } else {
                    // sort by key (=timestamp to make sure comments are displayed in the correct order)
                    ksort($employeeComments, SORT_NUMERIC);
                    $comments = '';
                    foreach ($employeeComments as $item) {
                        $comments .= $item['start_date'] . ': ' . $item['description'] . '; ';
                    }

                    $objSheet->setCellValueByColumnAndRow($startColIndex + $totalDays, $row, $comments);
                }
            }

            // render Saturdays and Sundays
            $colWeekDay = new Zend_Date($dateFrom);
            for ($k = 0; $k < $totalDays; ++$k) {
                $weekend = false;

                if ($colWeekDay->toString(Zend_Date::WEEKDAY_DIGIT) == '6') {
                    $weekend = true;
                    $objSheet->setCellValueByColumnAndRow($k + $startColIndex, $row, $this->view->escape($this->translate->translate('saturdayShortcut')));
                }
                if ($colWeekDay->toString(Zend_Date::WEEKDAY_DIGIT) == '0') {
                    $weekend = true;
                    $objSheet->setCellValueByColumnAndRow($k + $startColIndex, $row, $this->view->escape($this->translate->translate('sundayShortcut')));
                }

                if ($weekend) {
                    $objSheet->getStyleByColumnAndRow($k + $startColIndex, $row)->applyFromArray($tableBodyStyle);
                }

                $colWeekDay = $colWeekDay->add(1, Zend_Date::DAY);
            }

            // render public holidays
            if (!empty($publicHolidays[$employeeData['employee_id']])) {
                foreach ($publicHolidays[$employeeData['employee_id']] as $phDate => $publicHoliday) {
                    $startCellIndex = $this->getElapsedWithValidDatePeriod($dateFrom, strtotime($phDate), $dateFrom, $dateUntil);

                    $objSheet->setCellValueByColumnAndRow($startCellIndex + $startColIndex, $row, $this->view->escape($this->translate->translate('publicHolidayShortcut')));
                    $objSheet->getStyleByColumnAndRow($startCellIndex + $startColIndex, $row)->applyFromArray($tableBodyStyle);
                }
            }
        }

        return $row;
    }

    /**
     * Get days in month
     */
    protected function getDaysInMonth($monthNumber, $year)
    {
        $monthNumber--;
        $date = new Zend_Date($year, Zend_Date::YEAR_SHORT);
        $daysInMonth = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        return $monthNumber == 1 && $date->isLeapYear() ? 29 : $daysInMonth[$monthNumber];
    }

    /**
     * Render a number of cells with basic style options
     */
    protected function renderCellsWithStyleOptions($cellArray, $objSheet)
    {
        foreach ($cellArray as $item) {
            $row = $item['row'];
            $col = $item['column'];

            if (!empty($item['text'])) {
                $objSheet->setCellValueByColumnAndRow($col, $row, $item['text']);
            }
            if (!empty($item['bold'])) {
                $objSheet->getStyleByColumnAndRow($col, $row)->getFont()->setBold(true);
            }
            if (!empty($item['color'])) {
                $fill = $objSheet->getStyleByColumnAndRow($col, $row)->getFill();
                $fill->getStartColor()->setARGB($item['color']);
                if (empty($item['striped']) || true !== $item['striped']) {
                    $fill->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                } else {
                    $fill->setFillType(PHPExcel_Style_Fill::FILL_PATTERN_DARKVERTICAL)
                        ->setEndColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_WHITE));
                }
            }

            if (!empty($item['styleArray'])) {
                $objSheet->getStyleByColumnAndRow($col, $row)->applyFromArray($item['styleArray']);
            }
        }
    }

    /**
     * Render a number of cells with background color
     */
    protected function renderCellsWithBackgroundColor($startCellIndex, $rowIndex, $numberOfCells, $color, $objSheet, $startColIndex, $borderTableStyle)
    {
        for ($i = 0; $i <= $numberOfCells; $i++) {
            // if background color is white an additional cell border is needed
            if ($color === 'FFFFFF') {
                $objSheet->getStyleByColumnAndRow(($startCellIndex + $i) + $startColIndex, $rowIndex)
                    ->applyFromArray($borderTableStyle);
            } else {
                $objSheet->getStyleByColumnAndRow(($startCellIndex + $i) + $startColIndex, $rowIndex)
                    ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()->setARGB($color);
            }
        }
    }

    /**
     * Calculate the days between two dates taking account of the valid period
     * If elapsed days are negative, then the function returns 0
     * All dates should be timestamps!!!
     */
    protected function getElapsedWithValidDatePeriod($startTimestamp, $endTimestamp, $datePeriodStartTimestamp, $datePeriodEndTimestamp)
    {
        if ($datePeriodStartTimestamp > $startTimestamp) {
            $startTimestamp =  $datePeriodStartTimestamp;
        }

        if ($datePeriodEndTimestamp < $endTimestamp) {
            $endTimestamp =  $datePeriodEndTimestamp;
        }

        $diff_seconds =  $endTimestamp - $startTimestamp;

        if ($diff_seconds < 0) {
            return 0;
        }

        //Determine number of days
        $diff_days = round($diff_seconds / self::DAY_IN_SECONDS);

        return $diff_days;
    }

    /**
     * Calculate the days between two dates
     */
    protected function getElapsed($startTimestamp, $endTimestamp)
    {
        $diff_seconds = $endTimestamp - $startTimestamp;

        //Determine number of days
        $diff_days = floor($diff_seconds / self::DAY_IN_SECONDS);

        return $diff_days;
    }

    /**
     * Calculate the correct number of days for the first CW,
     * calculate how many full calendar weeks is between the dates and
     * calculate the correct number of days for the last CW,
     *
     */
    protected function getDaysForRenderCalenderWeek($start, $daysToEndDate = 0)
    {
        //hint: indizes -> 0 = Begin, 1 = Week numbers, 2 = End

        $result = array();
        $dayNumberOfStartDate = date('N', $start);
        if ($daysToEndDate != 0) {
            $result[0] = 7 - $dayNumberOfStartDate + 1;

            $restDaysToEndDate = $daysToEndDate - $result[0];

            $result[1] = floor($restDaysToEndDate / 7);

            $result[2] = $restDaysToEndDate % 7;

            if ($result[0] > $daysToEndDate) {
                $result[0] = $daysToEndDate;
                $result[2] = 0;
            }
        }

        return $result;
    }

    /**
     * Formats header row data for table report
     */
    protected function formatTableHeaderValue($object)
    {
        $value = "";
        if (isset($object['shortcut'])) {
            $value = $object['shortcut'] . "\n(" . $object['firstName'] . " " . $object['lastName'] . ")";
            if ($object['sapMaterialNumber']) {
                $value .= "\n(" . $object['sapMaterialNumber'] . ")";
            }
        } else {
            $value = $object['month'];
        }
        return $value;
    }

    /**
     * Formats hour values for table report
     */
    protected function formatTableHourValue($object, $showALHours)
    {
        if ($showALHours) {
            if ($object['hours'] == 0 && $object['hoursAL'] == 0) {
                return '';
            }
        } else {
            if ($object['hours'] == 0) {
                return '';
            }
        }
        return $showALHours ? General::formatNumber($object['hours']) . "\n AL:" . General::formatNumber($object['hoursAL']) : General::formatNumber($object['hours']);
    }

    public function exportPermission()
    {
        //Get the active sheet
        $objSheet = $this->objPHPExcel->getActiveSheet();

        //Set up page setup
        $this->setUpPageSetup();

        $styleBorder = $this->getBorderTableStyle();

        // Report Config
        $idx = $this->createPermissionHeader();



        // Columns width
        $objSheet->getColumnDimension('C')->setWidth(100);

        // Table header
        $styleTableHead = $this->getContentHeaderStyle();
        $objSheet->getStyle('A' . $idx . ':C' . $idx)->applyFromArray($styleTableHead);

        $objSheet->setCellValueByColumnAndRow(
            0,
            $idx,
            $this->view->escape($this->translate->translate('mitarbeiter'))
        );

        $objSheet->setCellValueByColumnAndRow(
            1,
            $idx,
            $this->view->escape($this->translate->translate('permissionKind'))
        );

        $objSheet->setCellValueByColumnAndRow(
            2,
            $idx,
            $this->view->escape($this->translate->translate('valueUC'))
        );

        $idx++;

        $data = $this->view->data['DATA'];
        $sort = $this->view->sort;

        //Sort the Table with the sort param
        foreach ($data as $nr => $content) {
            $employeename[$nr] = strtolower($content['EMPLOYEENAME']);
            $permissionkind[$nr] = strtolower($content['PERMISSIONKIND']);
            if (isset($content['VALUE'])) {
                $value[$nr] = strtolower($content['VALUE']);
            } else {
                $value[$nr] = '';
            }
        }
        if ($sort[0] == 'employeeName') {
            if ($sort[1] == 'ASC') {
                array_multisort($employeename, SORT_ASC, $data);
            } else {
                array_multisort($employeename, SORT_DESC, $data);
            }
        } elseif ($sort[0] == 'permissionKind') {
            if ($sort[1] == 'ASC') {
                array_multisort($permissionkind, SORT_ASC, $data);
            } else {
                array_multisort($permissionkind, SORT_DESC, $data);
            }
        } else {
            if ($sort[1] == 'ASC') {
                array_multisort($value, SORT_ASC, $data);
            } else {
                array_multisort($value, SORT_DESC, $data);
            }
        }
        //End of sort

        if (empty($data) || false == is_array($data)) {
            return $this->getContentXlsx();
        }

        foreach ($data as $item) {
            $objSheet->getStyle('A' . $idx . ':C' . $idx)->applyFromArray($styleBorder);
            $objSheet->setCellValueByColumnAndRow(
                0,
                $idx,
                $this->view->escape($item['EMPLOYEENAME'])
            );

            $objSheet->setCellValueByColumnAndRow(
                1,
                $idx,
                $this->view->escape($item['PERMISSIONKIND'])
            );

            $objSheet
                ->setCellValueByColumnAndRow(2, $idx, $this->view->escape(isset($item['VALUE']) ? $item['VALUE'] : ''));

            $idx++;
        }

        return $this->getContentXlsx();
    }

    public function exportSAPKeyHours()
    {
        $styleBorder = $this->getBorderTableStyle();

        $styleSummary = array(
            'font' => array(
                'bold' => true
            ),
            'alignment' => array(
                'horizontal'    => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                'vertical'        => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        //Get the active sheet
        $objSheet = $this->objPHPExcel->getActiveSheet();

        //Set up page setup
        $this->setUpPageSetup();

        // Report Config
        $idx = $this->createHeader('sapKeyHours');

        $colIdx = 0;
        $objBt = new BookingType();
        $bookingTypes = $objBt->getBookingTypesAsArray();
        $objSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('sapKey')));

        $objSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('nodeType')));

        $objSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('name')));

        $objSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('mitarbeiter')));

        $objSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view
            ->escape($this->translate->translate('emplNo')));

        foreach ($bookingTypes as $bookingType) {
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx))->setWidth(10);
            $objSheet->setCellValueByColumnAndRow($colIdx++, $idx, $bookingType);
        }

        $objSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view->escape($this->translate
            ->translate('total')));

        $lastCharColIndex = PHPExcel_Cell::stringFromColumnIndex($colIdx - 1);
        // Table header
        $styleTableHead = $this->getContentHeaderStyle();
        $objSheet->getStyle('A' . $idx . ':' . $lastCharColIndex . $idx)->applyFromArray($styleTableHead);

        // Columns width
        $objSheet->getColumnDimension('C')->setWidth(50);
        $objSheet->getColumnDimension('D')->setWidth(11);
        $objSheet->getColumnDimension('E')->setWidth(11);

        $objSheet->getColumnDimension($lastCharColIndex)->setWidth(15);

        //Data
        $idx++;
        $dataIdx = $idx;

        $data = $this->view->data['DATA'];
        if (empty($data) || false == is_array($data)) {
            return $this->getContentXlsx();
        }

        foreach ($data as $item) {
            $objSheet->getStyle('A' . $idx . ':' . $lastCharColIndex . $idx)->applyFromArray($styleBorder);
            $colIdx = 0;
            $objSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view->escape($item['SAP_KEY']));

            $objSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view->escape($item['SAP_KEY_NODE_TYPE']));

            $objSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view->escape($item['SAP_KEY_NAME']));

            $objSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view->escape($item['SHORTCUT']));

            $objSheet->setCellValueByColumnAndRow($colIdx++, $idx, $this->view->escape($item['SAP_MATERIAL_NUMBER']));

            foreach ($bookingTypes as $bookingType) {
                if ($item[$bookingType] != 0) {
                    $objSheet->setCellValueByColumnAndRow($colIdx, $idx, floatval($item[$bookingType]));
                    $objSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()->setFormatCode($this
                        ->numberFormat);
                } else {
                    $colIdx++;
                }
            }

            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, floatval($item['TOTALS']));
            $objSheet->getStyleByColumnAndRow($colIdx++, $idx)->getNumberFormat()->setFormatCode($this->numberFormat);

            $idx++;
        }

        //Sum
        $objSheet->setCellValueByColumnAndRow(4, $idx, $this->view->escape($this->translate
                ->translate('totals') . ':'));
        $objSheet->getStyleByColumnAndRow(4, $idx)->applyFromArray($styleSummary);

        $objSheet->setCellValue('F' . $idx, '=SUM(F' . $dataIdx . ':F' . ($idx - 1) . ')');
        $objSheet->getStyleByColumnAndRow(5, $idx)->getNumberFormat()->setFormatCode($this->numberFormat);

        $objSheet->setCellValue('G' . $idx, '=SUM(G' . $dataIdx . ':G' . ($idx - 1) . ')');
        $objSheet->getStyleByColumnAndRow(6, $idx)->getNumberFormat()->setFormatCode($this->numberFormat);

        $objSheet->setCellValue('H' . $idx, '=SUM(H' . $dataIdx . ':H' . ($idx - 1) . ')');
        $objSheet->getStyleByColumnAndRow(7, $idx)->getNumberFormat()->setFormatCode($this->numberFormat);

        $colIdx = 5;
        $numberOfSumCols = count($bookingTypes) + 1;

        for ($i = 0; $i < $numberOfSumCols; $i++) {
            $charColIndex = PHPExcel_Cell::stringFromColumnIndex($colIdx);
            $objSheet->setCellValue($charColIndex . $idx, '=SUM(' . $charColIndex . $dataIdx
                . ':' . $charColIndex . ($idx - 1) . ')');
            $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getNumberFormat()->setFormatCode($this->numberFormat);
            ++$colIdx;
        }

        return $this->getContentXlsx();
    }

    public function exportBtOrder()
    {
        $withFormatting = 250 > $this->view->data['COUNT'];

        $this->objPHPExcel->setActiveSheetIndex(0)->setTitle($this->translate->translate('overview'));

        //Get the active sheet
        $objSheet = $this->objPHPExcel->getActiveSheet();

        //Set up page setup
        $this->setUpPageSetup();

        //Create worksheet with the BT-Order
        $this->objPHPExcel->createSheet(1);

        //Set up page setup for sheet 1
        $this->setUpPageSetup();

        $styleBorder = $this->getBorderTableStyle();

        //default style for workbook
        $objSheet->getDefaultStyle()->getFont()->setName('Calibri');
        $objSheet->getDefaultStyle()->getFont()->setSize(11);

        if ($withFormatting) {
            //The third worksheet for the drop down list data
            $this->objPHPExcel->createSheet(2);

            //Set up page setup for sheet 2
            $this->setUpPageSetup();

            //The third worksheet is active, because of the drop down list data
            $this->objPHPExcel->setActiveSheetIndex(2);
            $objSheet = $this->objPHPExcel->getActiveSheet();

            $col = 'A';

            $objSheet->setCellValue($col . "1", $this->translate->translate('unmodified'));
            $objSheet->setCellValue($col . "2", $this->translate->translate('new'));
            $objSheet->setCellValue($col . "3", $this->translate->translate('changeInValue'));
            $objSheet->setCellValue($col . "4", $this->translate->translate('deletion'));
            $objSheet->setCellValue($col . "5", $this->translate->translate('putOn'));

            // Define named ranges for the one drop down list data
            $this->objPHPExcel->addNamedRange(new PHPExcel_NamedRange('kindOfChange', $objSheet, $col . '1:' . $col . '5'));

            $col = 'B';

            $objNt = new NodeType();

            $k = 1;
            foreach ($objNt->getNodeTypesAsArray($this->translate->getLocale()) as $nt) {
                $objSheet->setCellValue($col . $k++, $nt);
            }

            // Define named ranges for the one drop down list data
            $this->objPHPExcel->addNamedRange(new PHPExcel_NamedRange('nodeType', $objSheet, $col . '1:' . $col . $k));

            $col = 'C';

            $objUt = new StructureUnitType();

            $k = 1;
            foreach ($objUt->getStructureUnitTypesAsArray($this->translate->getLocale()) as $ut) {
                $objSheet->setCellValue($col . $k++, $ut);
            }

            // Define named ranges for the one drop down list data
            $this->objPHPExcel->addNamedRange(new PHPExcel_NamedRange('kind', $objSheet, $col . '1:' . $col . $k));

            $col = 'D';

            $objSheet->setCellValue($col . "1", $this->translate->translate('ja'));
            $objSheet->setCellValue($col . "2", $this->translate->translate('nein'));

            // Define named ranges for the one drop down list data
            $this->objPHPExcel->addNamedRange(new PHPExcel_NamedRange('locked', $objSheet, $col . '1:' . $col . '2'));

            $col = 'E';

            $objBt = new BookingType();

            $k = 1;
            foreach ($objBt->getBookingTypesAsArray() as $bt) {
                $objSheet->setCellValue($col . $k++, $bt);
            }

            // Define named ranges for the one drop down list data
            $this->objPHPExcel->addNamedRange(new PHPExcel_NamedRange(
                'standardBooking',
                $objSheet,
                $col . '1:' . $col . $k
            ));

            $col = 'F';

            $objSheet->setCellValue($col . "1", $this->translate->translate('btOrderNo'));
            $objSheet->setCellValue($col . "2", $this->translate->translate('noteWithPdbId'));
            $objSheet->setCellValue($col . "3", $this->translate->translate('withNote'));

            // Define named ranges for the one drop down list data
            $this->objPHPExcel->addNamedRange(new PHPExcel_NamedRange('noteMandatory', $objSheet, $col . '1:' . $col . '3'));

            //The first worksheet is now active again
            $this->objPHPExcel->setActiveSheetIndex(0);
            $objSheet = $this->objPHPExcel->getActiveSheet();
        }

        // Create header of profile
        $idx = $this->createHeader('btOrder');

        if ($withFormatting) {
            $objSheet->setCellValueByColumnAndRow(0, $idx, $this->translate->translate('btOrderNeedForChangesOfBookingTree'));

            //Border for Orderer, date and change of need
            $objSheet->getStyle('A' . $idx . ':A' . ($idx + 3))->getFont()->setBold(true);
            $objSheet->getStyle('A' . $idx)->getFont()->setSize(14);

            $objSheet->setCellValueByColumnAndRow(4, $idx, $this->translate->translate('hints') . ':');
            //hints in bold
            $objSheet->getStyle('E' . $idx)->getFont()->setBold(true);

            ++$idx;

            //Background color for optional field
            $objSheet->getStyle('E' . $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()
                ->setARGB(PHPExcel_Style_Color::COLOR_YELLOW);

            $objSheet->setCellValueByColumnAndRow(6, $idx, $this->translate->translate('btOrderMeansCanField'));

            ++$idx;
            //Background color for neccessary field
            $objSheet->getStyle('E' . $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB(self::GREEN);

            //Background color for the cells next to orderer and date
            $objSheet->getStyle('B' . $idx . ':B' . ($idx + 1))->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB(self::GREEN);
            //Border for the cells orderer and date and the cells next to
            $objSheet->getStyle('A' . $idx . ':B' . ($idx + 1))->applyFromArray($this->getBorderTableStyle());

            $objSheet->setCellValueByColumnAndRow(0, $idx, $this->translate->translate('orderer') . ':');

            $objSheet->setCellValueByColumnAndRow(6, $idx, $this->translate->translate('btOrderMeansNecessaryField'));

            ++$idx;

            $objSheet->setCellValueByColumnAndRow(0, $idx, $this->translate->translate('datum') . ':');

            //Underline a part of a string in a cell
            $objRichText = new PHPExcel_RichText();

            $objPart1 = $objRichText->createTextRun($this->translate->translate('inCaseOfModificationPart1'));
            $objPart1->getFont()->setBold(true);
            $objPart1->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
            $objPart1->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_RED));

            $objPart2 = $objRichText->createTextRun(' ' . $this->translate->translate('inCaseOfModificationPart2'));
            $objPart2->getFont()->setBold(true);
            $objPart2->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_RED));

            $objSheet->setCellValueByColumnAndRow(4, $idx, $objRichText);

            ++$idx;

            //Underline a part of a string in a cell
            $objRichText = new PHPExcel_RichText();

            $objPart1 = $objRichText->createTextRun($this->translate->translate('btOrderOnlyCopyAndPastePart1'));
            $objPart1->getFont()->setBold(true);
            $objPart1->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
            $objPart1->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_RED));

            $objPart2 = $objRichText->createTextRun(' ' . $this->translate->translate('btOrderOnlyCopyAndPastePart2'));
            $objPart2->getFont()->setBold(true);
            $objPart2->getFont()->setColor(new PHPExcel_Style_Color(PHPExcel_Style_Color::COLOR_RED));

            $objSheet->setCellValueByColumnAndRow(4, $idx, $objRichText);
        } else {
            $objSheet->setCellValueByColumnAndRow(0, $idx, $this->translate->translate('btOrderExistTreeStructure'));

            //Border for Orderer, date and change of need
            $objSheet->getStyle('A' . $idx . ':A' . ($idx + 3))->getFont()->setBold(true);
            $objSheet->getStyle('A' . $idx)->getFont()->setSize(14);
        }

        $this->objPHPExcel->setActiveSheetIndex(1)->setTitle($this->translate->translate('btOrder'));
        $objSheet = $this->objPHPExcel->getActiveSheet();

        //Table header
        $styleTableHead = $this->getContentHeaderStyle();
        $idx = 1;
        if ($withFormatting) {
            $objSheet->getStyle('A' . $idx . ':AJ' . $idx)->applyFromArray($styleTableHead);

            $objSheet->getStyle('A' . $idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB(self::LIGHTRED);

            $headerTranslateStrings = array(
                'kindOfChange', 'treeElement', 'indentation', 'indentationOld','name', 'nameOld',
                'nodeType', 'nodeTypeOld', 'kind', 'kindOld', 'lockedQuestionmark', 'lockedQuestionmarkOld',
                'bookableFrom', 'bookableFromOld', 'lockedFrom', 'lockedFromOld',
                'number', 'numberOld', 'companyName', 'companyNameOld', 'orderDescription', 'orderDescriptionOld',
                'targetHours', 'targetHoursOld', 'provider', 'providerOld', 'standardBooking', 'standardBookingOld', 'noteMandatory',
                'noteMandatoryOld', 'sapKeyCostCenter', 'sapKeyCostCenterOld', 'sapKeyCostUnit',
                'sapKeyCostObjectOld', 'notiz', 'noteOld',
            );
        } else {
            $objSheet->getStyle('A' . $idx . ':R' . $idx)->applyFromArray($styleTableHead);

            $headerTranslateStrings = array(
                'treeElement', 'indentation', 'name', 'nodeType', 'kind',
                'lockedQuestionmark', 'bookableFrom', 'lockedFrom',
                'number', 'companyName', 'orderDescription', 'targetHours', 'provider', 'standardBooking', 'noteMandatory',
                'sapKeyCostCenter',  'sapKeyCostUnit', 'notiz',
            );
        }

        $countTranlateStrings = count($headerTranslateStrings);

        //create the table Header
        for ($i = 0; $i < $countTranlateStrings; ++$i) {
            $objSheet->setCellValueByColumnAndRow($i, $idx, $this->translate->translate($headerTranslateStrings[$i]));
        }

        //Data of the table
        ++$idx;
        //Fix the position of the data before this line
        $objSheet->freezePane('A' . $idx);

        $dataIdx = $idx;

        $data = $this->view->data['DATA'];
        if (empty($data) || false == is_array($data)) {
            return $this->getContentXlsx();
        }

        //Display only the booking tree
        if ($withFormatting) {
            //Set general attributes for conditional formatting
            $objConditional = new PHPExcel_Style_Conditional();
            $objConditional->setConditionType(PHPExcel_Style_Conditional::CONDITION_EXPRESSION);
            $objConditional->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_NONE);

            //Conditional formating of kind of change
            $objConditionalKindOfChange = clone $objConditional;
            $objConditionalKindOfChange->setCondition('INDIRECT("A"&ROW())<>"' . $this->translate->translate('unmodified') . '"');
            $objConditionalKindOfChange->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(self::LIGHTRED);

            //Conditional formatting of node type. This is necessary so that the color will always appear. Reason: The condition Equal override the background color
            $objConNodeTypeGreen = clone $objConditional;
            $objConNodeTypeGreen->setCondition('1');
            $objConNodeTypeGreen->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(self::GREEN);

            // Conditional formating of kind
            $objConditionalKind = clone $objConditional;
            $objConditionalKind->setCondition('INDIRECT("G"&ROW())="' . $this->translate->translate('structureUnit') . '"');
            $objConditionalKind->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(self::GREEN);

            // Conditional formating of locked. This is necessary so that the color will always appear. Reason: The condition Equal override the background color
            $objConLockedGreen = clone $objConditional;
            $objConLockedGreen->setCondition('"true"="true"');
            $objConLockedGreen->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(self::GREEN);

            $objConBookableYellow = clone $objConditional;
            $objConBookableYellow->setCondition('"true"="true"');
            $objConBookableYellow->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(PHPExcel_Style_Color::COLOR_YELLOW);
            $objConBookableYellow->getStyle()->getNumberFormat()->setFormatCode(self::Date_FORMAT);

            // Conditional formating of DEBTOR_NUMBER Condition: ![$nodeTypeCell == 'Structure Unit' && ($kindCell == 'Subdivision' || $kindCell == 'other')] && ($nodeTypeCell != 'Activity')
            //=NOT(N(INDIRECT("G"&ROW())="Struktureinheit" ) * (N(INDIRECT("I"&ROW()) = "Sonstige") + N(INDIRECT("I"&ROW()) = "Unterbereich"))) * N(INDIRECT("G"&ROW()) <> "Aktivit�t")
            $objConNumber = clone $objConditional;
            $objConNumber->setCondition(
                'NOT(N(INDIRECT("G"&ROW())="' . $this->translate->translate('structureUnit') . '")' .
                ' * (N(INDIRECT("I"&ROW()) = "' . $this->translate->translate('sonstiges') . '")' .
                ' + N(INDIRECT("I"&ROW()) = "' . $this->translate->translate('subdivision') . '"))' .
                ') * N(INDIRECT("G"&ROW()) <> "' . $this->translate->translate('activity') . '")'
            );

            $objConNumber->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(PHPExcel_Style_Color::COLOR_YELLOW);

            //Conditional formating of name. This is necessary so that the color will always appear. Reason: The condition Equal override the background color
            $objConNameGreen = clone $objConditional;
            $objConNameGreen->setCondition('1');
            $objConNameGreen->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(self::GREEN);

            // Conditional formating of company name
            $objConCustomerName = clone $objConditional;
            $objConCustomerName->setCondition(
                'N(INDIRECT("G"&ROW())="' . $this->translate->translate('debtor') . '")'
            );
            $objConCustomerName->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(self::GREEN);

            // Conditional formating of order description
            $objConLongName = clone $objConditional;
            $objConLongName->setCondition(
                'N(INDIRECT("G"&ROW())="' . $this->translate->translate('order') . '")'
            );
            $objConLongName->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(PHPExcel_Style_Color::COLOR_YELLOW);

            // Conditional formating of Target Hours
            $objConTargetHours = clone $objConditional;
            $objConTargetHours->setCondition(
                'N(INDIRECT("G"&ROW())<>"' . $this->translate->translate('structureUnit') . '")' .
                ' * N(INDIRECT("G"&ROW())<>"' . $this->translate->translate('debtor') . '")'
            );
            $objConTargetHours->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(PHPExcel_Style_Color::COLOR_YELLOW);

            $objConProvider = clone $objConditional;
            $objConProvider->setCondition(
                'N(INDIRECT("G"&ROW())<>"' . $this->translate->translate('structureUnit') . '")' .
                ' * N(INDIRECT("G"&ROW())<>"' . $this->translate->translate('debtor') . '")'
            );
            $objConProvider->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(PHPExcel_Style_Color::COLOR_YELLOW);

            // Conditional formating of booking type
            $objConStandardBooking = clone $objConditional;
            $objConStandardBooking->setCondition('INDIRECT("G"&ROW())<>"' . $this->translate->translate('structureUnit') . '"');
            $objConStandardBooking->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(self::GREEN);

            // Conditional formating of note mandatory
            $objConNote = clone $objConditional;
            $objConNote->setCondition(
                'N(INDIRECT("G"&ROW())<>"' . $this->translate->translate('debtor') . '")' .
                ' * N(INDIRECT("G"&ROW())<>"' . $this->translate->translate('structureUnit') . '")'
            );
            $objConNote->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(self::GREEN);

            // Conditional formating of SAP key cost center
            $objConSapKeyCenter = clone $objConditional;
            $objConSapKeyCenter->setCondition('INDIRECT("G"&ROW())<>"' . $this->translate->translate('debtor') . '"');
            $objConSapKeyCenter->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(PHPExcel_Style_Color::COLOR_YELLOW);

            // Conditional formating of SAP key cost object
            //=N(INDIREKT("G"&ZEILE())<>"Struktureinheit")*N(INDIREKT("G"&ZEILE())<>"Tätigkeit")
            $objConSapKeyObject = clone $objConditional;
            $objConSapKeyObject->setCondition(
                'N(INDIRECT("G"&ROW())<>"' . $this->translate->translate('structureUnit') . '")' .
                ' * N(INDIRECT("G"&ROW())<>"' . $this->translate->translate('activity') . '")'
            );
            $objConSapKeyObject->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(PHPExcel_Style_Color::COLOR_YELLOW);

            // Conditional formating of note. This is necessary so that the color will always appear. Reason: The condition Equal override the background color
            $objConNoteYellow = clone $objConditional;
            $objConNoteYellow->setCondition('1');
            $objConNoteYellow->getStyle()->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getEndColor()->setARGB(PHPExcel_Style_Color::COLOR_YELLOW);

            // Conditional formating of hide Columns
            $objConditionalEqual = clone $objConditional;
            $objConditionalEqual->setCondition('INDIRECT(CHAR(CODE("A")+COLUMN()-1)&ROW())<>INDIRECT(CHAR(CODE("A")+COLUMN())&ROW())');

            $objConditionalEqual->getStyle()->getFont()->setItalic(true);
            $objConditionalEqual->getStyle()->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);

            //Conditional formating of hide Columns - from AA - AZ
            $objConditionalEqualRangeA = clone $objConditional;
            $objConditionalEqualRangeA->setCondition('INDIRECT(CONCATENATE("A", CHAR(CODE("A")+COLUMN()-26-1))&ROW())<>INDIRECT(CONCATENATE("A", CHAR(CODE("A")+COLUMN()-26))&ROW())');

            $objConditionalEqualRangeA->getStyle()->getFont()->setItalic(true);
            $objConditionalEqualRangeA->getStyle()->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);

            $hideColumns = array('D', 'F', 'H', 'J', 'L', 'N', 'P', 'R', 'T', 'V', 'X', 'Z', 'AB', 'AD', 'AF','AH', 'AJ');

            //hide the columns
            foreach ($hideColumns as $column) {
                $objSheet->getColumnDimension($column)->setVisible(false);
            }
        }

        if ($withFormatting) {
            $indentationCol = 'C';
            $indentationNameCol = 'E';
            $nextNoHideColumn = 2;
        } else {
            $indentationCol = 'B';
            $indentationNameCol = 'C';
            $nextNoHideColumn = 1;
        }

        foreach ($data as $item) {
            $colIdx = 0;

            if ($withFormatting) {
                $nodeTypeCell = 'G' . $idx;
                $kindCell = 'I' . $idx;

                //hide columns with the old values
                $hideColumnStart = 1;
                $hideArrayKeys = array(
                    'INDENTATION', 'NAME', 'NODE_TYPE', 'STRUCTURE_TYPE',
                    'BOOKABLE', 'BOOKABLE_FROM', 'LOCKED_FROM',
                    'DEBTOR_NUMBER', 'CUSTOMER_NAME', 'LONG_NAME', 'TARGET_EFFORT', 'PROVIDER' , 'BOOKING_TYPE',
                    'NOTE_OBLIGATION_TYPE', 'SAP_KEY_COST_CENTER',
                    'SAP_KEY_COST_OBJECT', 'NOTE',
                );

                foreach ($hideArrayKeys as $key) {
                    $hideColumnStart += 2;
                    $prevColdx = $hideColumnStart - 1;
                    if ('NOTE' == $key) {
                        $hideCell = 'AJ' . $idx;
                        $previousCell = 'AI' . $idx;
                    } else {
                        $hideCell = chr(ord('A') + $hideColumnStart) . $idx;
                        $previousCell = chr(ord('A') + $hideColumnStart - 1) . $idx;
                    }

                    $value = $item[$key];

                    if ($key == 'BOOKABLE') {
                        $value = ($value == 1) ? $this->translate->translate('nein') : $this->translate->translate('ja');
                    }

                    if ($key == 'NOTE_OBLIGATION_TYPE') {
                        if ($item['NODE_TYPE'] == $this->translate->translate('structureUnit')
                            || $item['NODE_TYPE'] == $this->translate->translate('debtor')
                        ) {
                            $value = '';
                        }
                    }

                    $conditionalStyles = $objSheet->getStyleByColumnAndRow($prevColdx, $idx)->getConditionalStyles();

                    if (in_array($key, array('NOTE', 'SAP_KEY_COST_CENTER', 'SAP_KEY_COST_OBJECT'))) {
                        array_push($conditionalStyles, $objConditionalEqualRangeA);
                    } else {
                        array_push($conditionalStyles, $objConditionalEqual);
                    }

                    $objSheet->getStyleByColumnAndRow($prevColdx, $idx)->setConditionalStyles($conditionalStyles);

                    $objSheet->setCellValueByColumnAndRow($hideColumnStart, $idx, $value);
                }

                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_push($conditionalStyles, $objConditionalKindOfChange);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);

                //Drop down list for kind of change
                $objValidationKindOfChange = $objSheet->getCellByColumnAndRow($colIdx, $idx)->getDataValidation();
                $objValidationKindOfChange->setFormula1('=kindOfChange');
                $objValidationKindOfChange->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
                $objValidationKindOfChange->setShowErrorMessage(true);
                $objValidationKindOfChange->setShowDropDown(true);
                $objValidationKindOfChange->setAllowBlank(false);

                $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->translate->translate('unmodified'));

                ++$colIdx;
            }

            $indentationCell = $indentationCol . $idx;
            $indentationNameCell = $indentationNameCol . $idx;

            $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getAlignment()->setWrapText(true);
            $objSheet->setCellValueByColumnAndRow(
                $colIdx++,
                $idx,
                '=CONCATENATE(REPT("    ", ' . $indentationCell . '), ' . $indentationNameCell . ')'
            );

            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $item['INDENTATION']);
            $objSheet->getStyle($indentationCell)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB(self::GREEN);

            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_push($conditionalStyles, $objConNameGreen);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);
            }

            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $item['NAME']);

            $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getAlignment()->setWrapText(true);

            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                //Drop down list for Node type
                $objValidationNodeType = $objSheet->getCellByColumnAndRow($colIdx, $idx)->getDataValidation();
                $objValidationNodeType->setFormula1('=nodeType');
                $objValidationNodeType->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
                $objValidationNodeType->setShowErrorMessage(true);
                $objValidationNodeType->setShowDropDown(true);
                $objValidationNodeType->setAllowBlank(false);

                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_push($conditionalStyles, $objConNodeTypeGreen);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);
            }
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $item['NODE_TYPE']);

            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                //Drop down list for kind
                $objValidationKind = $objSheet->getCellByColumnAndRow($colIdx, $idx)->getDataValidation();
                $objValidationKind->setFormula1('=kind');
                $objValidationKind->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
                $objValidationKind->setShowErrorMessage(true);
                $objValidationKind->setShowDropDown(true);
                $objValidationKind->setAllowBlank(false);

                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_push($conditionalStyles, $objConditionalKind);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);
            }
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $item['STRUCTURE_TYPE']);

            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                //Drop down list for locked
                $objValidationLocked = $objSheet->getCellByColumnAndRow($colIdx, $idx)->getDataValidation();
                $objValidationLocked->setFormula1('=locked');
                $objValidationLocked->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
                $objValidationLocked->setShowErrorMessage(true);
                $objValidationLocked->setShowDropDown(true);
                $objValidationLocked->setAllowBlank(false);

                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                $conditionalStyles[] = $objConLockedGreen;

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);
            }
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $item['BOOKABLE'] == 1 ? $this->translate
                ->translate('nein') : $this->translate->translate('ja'));

            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_unshift($conditionalStyles, $objConBookableYellow);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);
            }
            $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getNumberFormat()->setFormatCode(self::Date_FORMAT);
            $bookableFrom = $item['BOOKABLE_FROM'];
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $bookableFrom !== null ? $this->getDateCellValue(new Zend_Date($bookableFrom, 'dd.MM.YYYY')) : null);

            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_unshift($conditionalStyles, $objConBookableYellow);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);
            }
            $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getNumberFormat()->setFormatCode(self::Date_FORMAT);
            $lockedFrom = $item['LOCKED_FROM'];
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $lockedFrom !== null ? $this->getDateCellValue(new Zend_Date($lockedFrom, 'dd.MM.YYYY')) : null);

            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_push($conditionalStyles, $objConNumber);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);
            }
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $item['DEBTOR_NUMBER']);

            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_push($conditionalStyles, $objConCustomerName);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);
            }
            $objSheet->setCellValueByColumnAndRow(
                $colIdx,
                $idx,
                $item['CUSTOMER_NAME']
            );

            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_push($conditionalStyles, $objConLongName);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);
            }
            $objSheet->setCellValueByColumnAndRow(
                $colIdx,
                $idx,
                $item['LONG_NAME']
            );

            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_push($conditionalStyles, $objConTargetHours);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);
            }
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $item['TARGET_EFFORT']);
            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_push($conditionalStyles, $objConProvider);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);
            }
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $item['PROVIDER']);
            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_push($conditionalStyles, $objConStandardBooking);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);

                //Drop down list for booking type
                $objValidationBookingType = $objSheet->getCellByColumnAndRow($colIdx, $idx)->getDataValidation();
                $objValidationBookingType->setFormula1('=standardBooking');
                $objValidationBookingType->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
                $objValidationBookingType->setShowErrorMessage(true);
                $objValidationBookingType->setShowDropDown(true);
                $objValidationBookingType->setAllowBlank(false);
            }
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $item['BOOKING_TYPE']);

            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_push($conditionalStyles, $objConNote);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);

                //Drop down list for note mandatory
                $objValidationNoteMandatory = $objSheet->getCellByColumnAndRow($colIdx, $idx)->getDataValidation();
                $objValidationNoteMandatory->setFormula1('=noteMandatory');
                $objValidationNoteMandatory->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
                $objValidationNoteMandatory->setShowErrorMessage(true);
                $objValidationNoteMandatory->setShowDropDown(true);
                $objValidationNoteMandatory->setAllowBlank(false);
            }
            //This is neccesary for this report because every node has a note obligation type in the db
            $noteObligationText = '';
            if ($item['NODE_TYPE'] != $this->translate->translate('structureUnit')
                && $item['NODE_TYPE'] != $this->translate->translate('debtor')
            ) {
                $noteObligationText = $item['NOTE_OBLIGATION_TYPE'];
            }

            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $noteObligationText);

            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_push($conditionalStyles, $objConSapKeyCenter);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);
            }
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $item['SAP_KEY_COST_CENTER']);

            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_push($conditionalStyles, $objConSapKeyObject);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);
            }
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $item['SAP_KEY_COST_OBJECT']);

            $colIdx += $nextNoHideColumn;
            if ($withFormatting) {
                $conditionalStyles = $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getConditionalStyles();
                array_push($conditionalStyles, $objConNoteYellow);

                $objSheet->getStyleByColumnAndRow($colIdx, $idx)->setConditionalStyles($conditionalStyles);
            }
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $item['NOTE']);

            $idx++;
        }

        // Columns width

        $additionOld = 5;
        $colIdx = 0;
        if ($withFormatting) {
            //Columns with auto size
            $autoSizeColumns = array('AH', 'AI');

            // Columns width
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx))->setAutoSize(false);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(16);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(40);
            $objSheet->getStyleByColumnAndRow(0, 7)->getAlignment()->setWrapText(true);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(11);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(11 + $additionOld);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(36);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(36 + $additionOld);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(15);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(15 + $additionOld);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(12);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(12 + $additionOld);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(10);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(10 + $additionOld);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(14);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(14 + $additionOld);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(14);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(14 + $additionOld);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(9);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(9 + $additionOld);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(26);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(26 + $additionOld);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(26);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(26 + $additionOld);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(12);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(12 + $additionOld);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(20);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(17);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(17 + $additionOld);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(25);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(25 + $additionOld);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(21);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(21 + $additionOld);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(21);
            $objSheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($colIdx++))->setWidth(21 + $additionOld);
        } else {
            //Columns with auto size
            $autoSizeColumns = array('C', 'E', 'F', 'J', 'K', 'N', 'R');

            // Columns width
            $objSheet->getColumnDimension('A')->setWidth(60);
            $objSheet->getColumnDimension('B')->setWidth(11);
            $objSheet->getColumnDimension('D')->setWidth(15);
            $objSheet->getColumnDimension('G')->setWidth(14);
            $objSheet->getColumnDimension('H')->setWidth(14);
            $objSheet->getColumnDimension('I')->setWidth(10);
            $objSheet->getColumnDimension('L')->setWidth(12);
            $objSheet->getColumnDimension('M')->setWidth(20);
            $objSheet->getColumnDimension('O')->setWidth(25);
            $objSheet->getColumnDimension('P')->setWidth(22);
            $objSheet->getColumnDimension('Q')->setWidth(22);
        }

        foreach ($autoSizeColumns as $column) {
            $objSheet->getColumnDimension($column)->setAutoSize(true);
        }

        //Display the first sheet after open the file
        $this->objPHPExcel->setActiveSheetIndex(0);
        $objSheet = $this->objPHPExcel->getActiveSheet();

        $objSheet->getColumnDimension('A')->setAutoSize(false);
        $objSheet->getColumnDimension('A')->setWidth(16);
        $objSheet->getColumnDimension('B')->setWidth(40);
        $objSheet->getStyleByColumnAndRow(0, 7)->getAlignment()->setWrapText(true);
        $objSheet->getColumnDimension('C')->setWidth(11);
        $objSheet->getColumnDimension('D')->setWidth(11 + $additionOld);
        $objSheet->getColumnDimension('E')->setWidth(36);
        $objSheet->getColumnDimension('F')->setWidth(36 + $additionOld);
        $objSheet->getColumnDimension('G')->setWidth(15);
        $objSheet->getColumnDimension('H')->setWidth(15 + $additionOld);

        $hideColumns = array('D', 'F');

        //hide the columns
        foreach ($hideColumns as $column) {
            $objSheet->getColumnDimension($column)->setVisible(false);
        }

        return $this->getContentXlsx();
    }

    public function exportStandby()
    {
        //Get the active sheet
        $objSheet = $this->objPHPExcel->getActiveSheet();

        //Set up page setup
        $this->setUpPageSetup();

        // Report Config
        $idx = $this->createHeader('standby');

        // Table header
        $headerContentTableStyle = $this->getContentHeaderStyle();
        $objSheet->getStyle('A' . $idx . ':J' . $idx)->applyFromArray($headerContentTableStyle);

        $colIdx = 0;
        $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($this->translate
            ->translate('firma')));
        $colIdx++;

        $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($this->translate
            ->translate('division')));
        $colIdx++;

        $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($this->translate
            ->translate('employeeShortcut')));
        $objSheet->getColumnDimensionByColumn($colIdx)->setWidth(22);
        $colIdx++;

        $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($this->translate
            ->translate('employeeName')));
        $objSheet->getColumnDimensionByColumn($colIdx)->setWidth(25);
        $colIdx++;

        $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($this->translate
            ->translate('dayType')));
        $objSheet->getColumnDimensionByColumn($colIdx)->setWidth(30);
        $colIdx++;

        $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($this->translate
            ->translate('datum')));
        $objSheet->getColumnDimensionByColumn($colIdx)->setWidth(13);
        $colIdx++;

        $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($this->translate
            ->translate('begin')));
        $objSheet->getColumnDimensionByColumn($colIdx)->setWidth(9);
        $colIdx++;

        $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($this->translate
            ->translate('hoursActual')));
        $objSheet->getColumnDimensionByColumn($colIdx)->setWidth(23);
        $colIdx++;

        $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($this->translate
            ->translate('abkbkz')));
        $objSheet->getColumnDimensionByColumn($colIdx)->setWidth(30);
        $colIdx++;

        $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($this->translate
            ->translate('notiz')));
        $objSheet->getColumnDimensionByColumn($colIdx)->setWidth(40);

        $data = $this->view->data['DATA'];
        if (empty($data) || false == is_array($data)) {
            return $this->getContentXlsx();
        }

        $contentStyle = array(
            'alignment' => array(
                'vertical'    => PHPExcel_Style_Alignment::VERTICAL_TOP
            )
        );

        $idx++;
        $rowsCount = count($data);
        for ($loop = 0; $loop < $rowsCount; $loop++) {
            $objSheet->getStyle('A' . $idx . ':I' . $idx)->applyFromArray($contentStyle);

            $colIdx = 0;

            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($data[$loop]['COMPANY']));
            $colIdx++;

            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($data[$loop]['DIVISION']));
            $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getAlignment()->setWrapText(true);
            $colIdx++;

            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($data[$loop]['EMP_SHORTCUT']));
            $colIdx++;

            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($data[$loop]['EMP_NAME']));
            $colIdx++;

            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($this->translate->translate('dayType_' . $data[$loop]['DAY_TYPE'])))    ;
            $colIdx++;

            $startDate = $this->view->escape($data[$loop]['START_DATE']);
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $startDate !== null ? $this->getDateCellValue(new Zend_Date($startDate, 'dd.MM.YYYY')) : null);
            $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getNumberFormat()->setFormatCode(self::Date_FORMAT);
            $colIdx++;

            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($data[$loop]['START_TIME']));
            $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getNumberFormat()->setFormatCode($this->timeFormat);
            $colIdx++;

            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($data[$loop]['HOURS_ACTUAL']));
            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, floatval($data[$loop]['HOURS_ACTUAL']));
            $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getNumberFormat()->setFormatCode($this->numberFormat);
            $colIdx++;

            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($data[$loop]['CC']));
            $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getAlignment()->setWrapText(true);
            $colIdx++;

            $objSheet->setCellValueByColumnAndRow($colIdx, $idx, $this->view->escape($data[$loop]['NOTE']));
            $objSheet->getStyleByColumnAndRow($colIdx, $idx)->getAlignment()->setWrapText(true);

            $idx++;
        }

        return $this->getContentXlsx();
    }

    /**
     * Export 'Contingent range' report.
     *
     * @return string
     */
    public function exportContingentRange()
    {
        $objSheet = $this->activeSheet;

        $defaultNumberFormat = '#,##0.00';

        // general sheet config
        $this->setUpPageSetup();

        $configStyle = $this->getReportConfigStyle();
        $rowIndex = 5;
        $dataBeginRowIndex = $rowIndex + 1;

        $date = new Zend_Date();

        // create report header
        $objSheet->getColumnDimensionByColumn(0)->setAutoSize(true);
        $objSheet->getColumnDimensionByColumn(1)->setWidth(40);

        $objSheet->getStyleByColumnAndRow(0, 1)->applyFromArray($configStyle);
        $objSheet->getStyleByColumnAndRow(0, 1)->getFont()->setSize(12);
        $objSheet->getStyleByColumnAndRow(1, 1)->getFont()->setSize(12);
        $objSheet->setCellValueByColumnAndRow(0, 1, 'Report:');
        $objSheet->setCellValueByColumnAndRow(1, 1, $this->translate->translate("contingentRange"));

        $objSheet->getStyleByColumnAndRow(0, 3)->applyFromArray($configStyle);
        $objSheet->setCellValueByColumnAndRow(0, 3, 'Stand zum:');
        $objSheet->setCellValueByColumnAndRow(1, 3, $date->get('dd.MM.yyyy'));

        $styleBorder = $this->getBorderTableStyle();

        // Table headers
        $styleTableHead = $this->getContentHeaderStyle();
        $styleTableHead['alignment']['horizontal'] = PHPExcel_Style_Alignment::HORIZONTAL_CENTER;

        $objSheet->getStyle('A' . $rowIndex . ':R' . $rowIndex)->applyFromArray($styleTableHead);

        $fontColorConfig = [
            'red' => [
                'font' => [
                    'color' => [
                        'argb' => 'FF0000'
                    ]
                ]
            ],
            'blue' => [
                'font' => [
                    'color' => [
                        'argb' => '0000FF'
                    ]
                ]
            ]
        ];

        $columnConfig = [
            'A' => [
                'title' => 'Geführt in Bereich',
                'width' => 11,
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
                    ]
                ]
            ],
            'B' => [
                'title' => 'MA-Kürzel',
                'width' => 8,
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                    ]
                ]
            ],
            'C' => [
                'title' => 'MA-Nach-/Vorname',
                'width' => 25
            ],
            'D' => [
                'title' => 'intern / extern',
                'width' => 8,
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                    ]
                ]
            ],
            'E' => [
                'title' => 'Rest-Kontingent deckt 42 Tage ab',
                'width' => 18,
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                    ]
                ]
            ],
            'F' => [
                'title' => 'Kontingent-Rest-Std (42Tg)',
                'width' => 11,
                'styles' => [
                    'numberformat' => [
                        'code' => $defaultNumberFormat,
                    ]
                ]
            ],
            'G' => [
                'title' => 'Nicht abgedeckte Std (42Tg)',
                'width' => 15,
                'styles' => [
                    'numberformat' => [
                        'code' => $defaultNumberFormat
                    ]
                ]
            ],
            'H' => [
                'title' => 'Voraussichtl Arbeits-Std (42Tg)',
                'width' => 13,
                'styles' => [
                    'numberformat' => [
                        'code' => $defaultNumberFormat
                    ]
                ]
            ],
            'I' => [
                'title' => 'Soll-Std (42Tg)',
                'width' => 7,
                'styles' => [
                    'numberformat' => [
                        'code' => $defaultNumberFormat
                    ]
                ]
            ],
            'J' => [
                'title' => "Abwesenheits- \nStd \n(42Tg)",
                'width' => 15,
                'styles' => [
                    'numberformat' => [
                        'code' => $defaultNumberFormat
                    ]
                ]
            ],
            'K' => [
                'title' => 'Laufende Kontingent-Nr pro MA',
                'width' => 14
            ],
            'L' => [
                'title' => 'Art',
                'width' => 8,
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
                    ]
                ]
            ],
            'M' => [
                'title' => 'Bedarfsanforderungsnummer',
                'width' => 33,
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
                    ]
                ]
            ],
            'N' => [
                'title' => 'Position',
                'width' => 10
            ],
            'O' => [
                'title' => 'Kontingent gehört zu Bereich',
                'width' => 11
            ],
            'P' => [
                'title' => 'Kontingent-Beginn',
                'width' => 12,
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
                    ]
                ]
            ],
            'Q' => [
                'title' => 'Kontingent-Ende',
                'width' => 12,
                'styles' => [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
                    ]
                ]
            ],
            'R' => [
                'title' => 'Rest-Std',
                'width' => 12,
                'styles' => [
                    'numberformat' => [
                        'code' => $defaultNumberFormat
                    ]
                ]
            ],
        ];
        $colIdx = 0;

        foreach ($columnConfig as $collKey => $headerColumn) {
            $objSheet->setCellValueByColumnAndRow(
                $colIdx,
                $rowIndex,
                $headerColumn['title']
            );
            ++$colIdx;
        }

        // create report body
        $data = $this->view->data['DATA'];

        if (empty($data) || is_array($data) === false) {
            return $this->getContentXlsx();
        }

        $currentDate = new Zend_Date();
        $currentDate->setTime(0);

        $currentDatePlus42 = clone $currentDate;
        $currentDatePlus42 = $currentDatePlus42->addDay(42)->getTimestamp();

        $currentDate = $currentDate->getTimestamp();

        $remainingHoursSum = [];

        foreach ($data as $key => $contingent) {
            $contingentBegin = strtotime($contingent['CONTINGENT_BEGIN_DATE']);
            $contingentEnd = strtotime($contingent['CONTINGENT_END_DATE']);

            if ($contingentBegin <= $currentDate && $contingentEnd >= $currentDate
                || $contingentBegin >= $currentDate && $contingentEnd <= $currentDatePlus42
                || $contingentBegin <= $currentDatePlus42 && $contingentEnd >= $currentDatePlus42
            ) {
                $remainingHoursSum[$contingent['EMPLOYEE_ID']] = (isset($remainingHoursSum[$contingent['EMPLOYEE_ID']]) ? $remainingHoursSum[$contingent['EMPLOYEE_ID']] : 0) + $contingent['REMAINING_HOURS'];
            }
            $data[$key]['EXPECTED_WORKING_TIME'] = $contingent['TARGET_HOURS'] - $contingent['ABSENCE_HOURS'];
        }

        foreach ($data as $key => $contingent) {
            $data[$key]['REMAINING_HOURS_42'] = empty($remainingHoursSum[$contingent['EMPLOYEE_ID']]) ? 0 : $remainingHoursSum[$contingent['EMPLOYEE_ID']];
            $data[$key]['COVERS_NEXT_42_DAYS'] = $data[$key]['REMAINING_HOURS_42'] > $data[$key]['EXPECTED_WORKING_TIME'];
        }

        ++$rowIndex;

        foreach ($data as $contingent) {
            $objSheet->getStyle('A' . $rowIndex . ':R' . $rowIndex)->applyFromArray($styleBorder);
            $colIdx = 0;

            $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingent['EMPLOYEE_DIVISION']);
            ++$colIdx;

            $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingent['EMPLOYEE_SHORTCUT']);
            ++$colIdx;

            $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingent['EMPLOYEE_LAST_NAME'] . ', ' . $contingent['EMPLOYEE_FIRST_NAME']);
            ++$colIdx;

            $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingent['EMPLOYEE_IS_INTERNAL'] == 1 ? 'intern' : 'extern');
            ++$colIdx;

            $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, ($contingent['COVERS_NEXT_42_DAYS'] ? 'ja' : 'nein'));

            if ($contingent['COVERS_NEXT_42_DAYS'] === false) {
                $objSheet->getStyle('E' . $rowIndex)->applyFromArray($fontColorConfig['red']);
            }

            ++$colIdx;

            $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingent['REMAINING_HOURS_42']);
            ++$colIdx;

            $notCoveredHours42 = $contingent['EXPECTED_WORKING_TIME'] - $contingent['REMAINING_HOURS_42'];
            $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, max($notCoveredHours42, 0));

            ++$colIdx;

            $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingent['EXPECTED_WORKING_TIME']);
            ++$colIdx;

            $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingent['TARGET_HOURS']);
            ++$colIdx;

            $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingent['ABSENCE_HOURS']);
            ++$colIdx;

            $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingent['EMPLOYEE_CONTINGENT_NUMBER']);
            ++$colIdx;

            if ($contingent['CONTINGENT_ID']) {
                $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingent['CONTINGENT_TYPE_NAME']);
                ++$colIdx;

                $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingent['REQUISITION_NOTE_NUMBER']);
                ++$colIdx;

                $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingent['CONTINGENT_POSITION']);
                ++$colIdx;

                $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingent['CONTINGENT_DIVISION']);
                ++$colIdx;

                $contingentBeginDate = $contingent['CONTINGENT_BEGIN_DATE'];
                $objSheet->getCellByColumnAndRow($colIdx, $rowIndex)->getStyle()->getNumberFormat()->setFormatCode(self::Date_FORMAT);
                $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingentBeginDate !== null ? $this->getDateCellValue(new Zend_Date($contingentBeginDate, 'dd.MM.YYYY')) : null);
                ++$colIdx;

                $contingentEndDate = $contingent['CONTINGENT_END_DATE'];
                $objSheet->getCellByColumnAndRow($colIdx, $rowIndex)->getStyle()->getNumberFormat()->setFormatCode(self::Date_FORMAT);
                $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingentEndDate !== null ? $this->getDateCellValue(new Zend_Date($contingentEndDate, 'dd.MM.YYYY')) : null);
                if (strtotime($contingentEndDate) < $currentDatePlus42) {
                    $objSheet->getStyle('Q' . $rowIndex)->applyFromArray($fontColorConfig['red']);
                }
                ++$colIdx;

                $objSheet->setCellValueByColumnAndRow($colIdx, $rowIndex, $contingent['REMAINING_HOURS']);
                if ($contingent['REMAINING_HOURS'] < 200) {
                    $objSheet->getStyle('R' . $rowIndex)->applyFromArray($fontColorConfig['red']);
                } elseif ($contingent['REMAINING_HOURS'] >= 200 && $contingent['REMAINING_HOURS'] < 300) {
                    $objSheet->getStyle('R' . $rowIndex)->applyFromArray($fontColorConfig['blue']);
                }
            }

            ++$rowIndex;
        }

        foreach ($columnConfig as $colIndex => $colStyleConfig) {
            if (isset($colStyleConfig['styles'])) {
                $objSheet->getStyle($colIndex . $dataBeginRowIndex . ':' . $colIndex . ($rowIndex - 1))->applyFromArray($colStyleConfig['styles']);
            }

            if (isset($colStyleConfig['width'])) {
                $objSheet->getColumnDimension($colIndex)->setWidth($colStyleConfig['width']);
            }
        }
        return $this->getContentXlsx();
    }

    /**
     * @return false|string
     * @throws PHPExcel_Exception
     */
    public function exportContingentData()
    {
        $reportType = $this->view->data['REPORT_INFO']['TYPE'];
        $this->setUpPageSetup();
        $export = new Excel_ContingentData($this->activeSheet, $reportType);
        $export->setHeaderStyle($this->getContentHeaderStyle());
        $export->setBorderStyle($this->getBorderTableStyle());
        $export->setReportStyle($this->getReportConfigStyle());
        $export->generate($this->view->data['DATA']);
        return $this->getContentXlsx();
    }

    /**
     * @return false|string
     * @throws PHPExcel_Exception
     */
    public function exportPrismaValues()
    {
        $reportType = $this->view->data['REPORT_INFO']['TYPE'];
        $this->setUpPageSetup();
        $export = new Excel_PrismaValues($this->activeSheet, $reportType);
        $export->setHeaderStyle($this->getContentHeaderStyle());
        $export->setBorderStyle($this->getBorderTableStyle());
        $export->setReportStyle($this->getReportConfigStyle());
        $export->generate($this->view->data['DATA']);
        return $this->getContentXlsx();
    }
}
