<?php

class Excel_PrismaValues extends Excel
{
    private $columns = [
        [
            Excel::COLUMN_DATA_INDEX => 'COMPANY_SHORTCUT',
            Excel::COLUMN_TITLE => 'Firmenkürzel'
        ],
        [
            Excel::COLUMN_DATA_INDEX => 'EMPLOYEE_SHORTCUT',
            Excel::COLUMN_TITLE => 'Mitarbeiterkürzel'
        ],
        [
            Excel::COLUMN_DATA_INDEX => 'EMPLOYEE_FULL_NAME',
            Excel::COLUMN_TITLE => 'Name'
        ],
        [
            Excel::COLUMN_DATA_INDEX => 'DIVISION_NAME',
            Excel::COLUMN_TITLE => 'Bereich'
        ],
        [
            Excel::COLUMN_DATA_INDEX => 'BALANCE_HOLIDAYS_LAST_MONTH',
            Excel::COLUMN_TITLE => 'Urlaubssaldo letzter Monat'
        ],
        [
            Excel::COLUMN_DATA_INDEX => 'BOOKED_HOLIDAYS_CUR_MONTH',
            Excel::COLUMN_TITLE => 'Urlaubstage im aktuellen Monat bis jetzt'
        ],
        [
            Excel::COLUMN_DATA_INDEX => 'BALANCE_HOLIDAYS_ACTUAL',
            Excel::COLUMN_TITLE => 'Aktueller Urlaubssaldo'
        ],
        [
            Excel::COLUMN_DATA_INDEX => 'ANNUAL_HOLIDAY',
            Excel::COLUMN_TITLE => 'Jahresurlaub'
        ],
        [
            Excel::COLUMN_DATA_INDEX => 'BALANCE_HOLIDAYS_LAST_YEAR',
            Excel::COLUMN_TITLE => 'Vorjahres-Resturlaub'
        ],
        [
            Excel::COLUMN_DATA_INDEX => 'BALANCE_HOURS_LAST_MONTH',
            Excel::COLUMN_TITLE => 'Stundensaldo letzter Monat'
        ],
        [
            Excel::COLUMN_DATA_INDEX => 'BOOKED_HOURS',
            Excel::COLUMN_TITLE => 'Gebuchte Stunden im aktuellen Monat bis jetzt'
        ],
        [
            Excel::COLUMN_DATA_INDEX => 'WORK_DAYS',
            Excel::COLUMN_TITLE => 'Arbeitstage im aktuellen Monat bis jetzt'
        ],
        [
            Excel::COLUMN_DATA_INDEX => 'WORK_DAYS_WITHOUT_BOOKING',
            Excel::COLUMN_TITLE => 'Nicht gebuchte Arbeitstage im aktuellen Monat bis jetzt'
        ],
        [
            Excel::COLUMN_DATA_INDEX => 'HOURS_PER_WEEK',
            Excel::COLUMN_TITLE => 'Aktuelle Soll-Wochenstundenzahl'
        ],
        [
            Excel::COLUMN_DATA_INDEX => 'BALANCE_HOURS_ACTUAL',
            Excel::COLUMN_TITLE => 'Aktueller Stundensaldo'
        ]
    ];

    /**
     * @return array
     */
    protected function getColumnConfig(): array
    {
        return $this->columns;
    }

}
