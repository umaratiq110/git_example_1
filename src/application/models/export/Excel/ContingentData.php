<?php
declare(strict_types=1);

class Excel_ContingentData extends Excel
{
    /**
     * Available columns for this report
     *
     * @var array
     */
    private static $columnConfig = [
        'EMPLOYEE_TYPE' => [
            Excel::COLUMN_DATA_INDEX => 'EMPLOYEE_TYPE',
            Excel::COLUMN_TITLE => 'Intern/Extern',
            Excel::COLUMN_POSITION => 0,
            Excel::COLUMN_WIDTH => 12,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 17
            ],
            Report_ContingentData::TYPE_CONTRACT_MANAGEMENT => [
                Excel::COLUMN_POSITION => 13
            ]
        ],
        'PARTNER_NAME' => [
            Excel::COLUMN_DATA_INDEX => 'PARTNER_NAME',
            Excel::COLUMN_TITLE => 'Kommt von Partner',
            Excel::COLUMN_POSITION => 1,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 8
            ],
            Report_ContingentData::TYPE_CONTRACT_MANAGEMENT => [
                Excel::COLUMN_POSITION => 4
            ]
        ],
        'EMPLOYEE_DESCRIPTION' => [
            Excel::COLUMN_DATA_INDEX => 'EMPLOYEE_DESCRIPTION',
            Excel::COLUMN_TITLE => 'Mitarbeiter',
            Excel::COLUMN_POSITION => 2,
            Excel::COLUMN_WIDTH => 40,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 0
            ],
            Report_ContingentData::TYPE_CONTRACT_MANAGEMENT => [
                Excel::COLUMN_POSITION => 0
            ]
        ],
        'MONTH' => [
            Excel::COLUMN_DATA_INDEX => 'MONTH',
            Excel::COLUMN_TITLE => 'Monat',
            Excel::COLUMN_POSITION => 3,
            Excel::COLUMN_WIDTH => 13,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 2
            ],
            Report_ContingentData::TYPE_CONTRACT_MANAGEMENT => [
                Excel::COLUMN_POSITION => 6
            ],
            Excel::COLUMN_CONVERTER => 'convertToNamedMonth'
        ],
        'HOURS' => [
            Excel::COLUMN_DATA_INDEX => 'HOURS',
            Excel::COLUMN_TITLE => 'LN-Std-Zahl',
            Excel::COLUMN_POSITION => 4,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 3
            ],
            Excel::COLUMN_STYLE => [
                'numberformat' => [
                    'code' => '#,##0.00'
                ]
            ]
        ],
        'PURCHASE_PRICE_PER_HOUR' => [
            Excel::COLUMN_DATA_INDEX => 'PURCHASE_PRICE_PER_HOUR',
            Excel::COLUMN_TITLE => 'Einkaufspreis',
            Excel::COLUMN_POSITION => 5,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 4
            ],
            Report_ContingentData::TYPE_CONTRACT_MANAGEMENT => [
                Excel::COLUMN_POSITION => 8
            ],
            Excel::COLUMN_STYLE => [
                'numberformat' => [
                    'code' => '#,##0.00'
                ]
            ]
        ],
        'PURCHASE_PRICE' => [
            Excel::COLUMN_DATA_INDEX => 'PURCHASE_PRICE',
            Excel::COLUMN_TITLE => 'EK-Betrag',
            Excel::COLUMN_POSITION => 6,
            Excel::COLUMN_WIDTH => 13,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 5
            ],
            Excel::COLUMN_STYLE => [
                'numberformat' => [
                    'code' => '#,##0.00'
                ]
            ]
        ],
        'SALES_PRICE_PER_HOUR' => [
            Excel::COLUMN_DATA_INDEX => 'SALES_PRICE_PER_HOUR',
            Excel::COLUMN_TITLE => 'Verkaufspreis',
            Excel::COLUMN_POSITION => 9,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 18
            ],
            Excel::COLUMN_STYLE => [
                'numberformat' => [
                    'code' => '#,##0.00'
                ]
            ]
        ],
        'SALES_PRICE' => [
            Excel::COLUMN_DATA_INDEX => 'SALES_PRICE',
            Excel::COLUMN_TITLE => 'VK-Betrag',
            Excel::COLUMN_POSITION => 10,
            Excel::COLUMN_WIDTH => 13,
            Excel::COLUMN_STYLE => [
                'numberformat' => [
                    'code' => '#,##0.00'
                ]
            ]
        ],
        'MARGIN' => [
            Excel::COLUMN_DATA_INDEX => 'MARGIN',
            Excel::COLUMN_TITLE => 'Marge',
            Excel::COLUMN_POSITION => 11,
            Excel::COLUMN_WIDTH => 13,
            Excel::COLUMN_STYLE => [
                'numberformat' => [
                    'code' => '#,##0.00'
                ]
            ]
        ],
        'PRICE_GROUP_PURCHASE_NAME' => [
            Excel::COLUMN_DATA_INDEX => 'PRICE_GROUP_PURCHASE_NAME',
            Excel::COLUMN_TITLE => 'Preisgruppe EK',
            Excel::COLUMN_POSITION => 12,
            Excel::COLUMN_WIDTH => 20,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 10
            ],
            Report_ContingentData::TYPE_CONTRACT_MANAGEMENT => [
                Excel::COLUMN_POSITION => 7
            ]
        ],
        'PRICE_GROUP_SALE_NAME' => [
            Excel::COLUMN_DATA_INDEX => 'PRICE_GROUP_SALE_NAME',
            Excel::COLUMN_TITLE => 'Preisgruppe VK',
            Excel::COLUMN_POSITION => 13,
            Excel::COLUMN_WIDTH => 20
        ],
        'COST_CENTER' => [
            Excel::COLUMN_DATA_INDEX => 'COST_CENTER',
            Excel::COLUMN_TITLE => 'BKZ',
            Excel::COLUMN_POSITION => 14,
            Excel::COLUMN_WIDTH => 75,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 9
            ]
        ],
        'FRAMEWORK_CONTRACT_NAME' => [
            Excel::COLUMN_DATA_INDEX => 'FRAMEWORK_CONTRACT_NAME',
            Excel::COLUMN_TITLE => 'Rahmenvertrag',
            Excel::COLUMN_POSITION => 15,
            Excel::COLUMN_WIDTH => 32,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 11
            ],
            Report_ContingentData::TYPE_CONTRACT_MANAGEMENT => [
                Excel::COLUMN_POSITION => 5
            ]
        ],
        'CONTINGENT_NAME' => [
            Excel::COLUMN_DATA_INDEX => 'CONTINGENT_NAME',
            Excel::COLUMN_TITLE => 'Kontingent',
            Excel::COLUMN_POSITION => 16,
            Excel::COLUMN_WIDTH => 65,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 1
            ],
            Report_ContingentData::TYPE_CONTRACT_MANAGEMENT => [
                Excel::COLUMN_POSITION => 9
            ]
        ],
        'CONTINGENT_BEGIN_DATE' => [
            Excel::COLUMN_DATA_INDEX => 'CONTINGENT_BEGIN_DATE',
            Excel::COLUMN_TITLE => 'Beginn',
            Excel::COLUMN_POSITION => 17,
            Excel::COLUMN_WIDTH => 11,
            Report_ContingentData::TYPE_CONTRACT_MANAGEMENT => [
                Excel::COLUMN_POSITION => 10
            ],
            Excel::DATE_VALUE => true
        ],
        'CONTINGENT_END_DATE' => [
            Excel::COLUMN_DATA_INDEX => 'CONTINGENT_END_DATE',
            Excel::COLUMN_TITLE => 'Ende',
            Excel::COLUMN_POSITION => 18,
            Excel::COLUMN_WIDTH => 11,
            Report_ContingentData::TYPE_CONTRACT_MANAGEMENT => [
                Excel::COLUMN_POSITION => 11
            ],
            Excel::DATE_VALUE => true
        ],
        'DIVISION_NAME' => [
            Excel::COLUMN_DATA_INDEX => 'DIVISION_NAME',
            Excel::COLUMN_TITLE => 'ISO-Bereich',
            Excel::COLUMN_POSITION => 20,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 14
            ],
            Report_ContingentData::TYPE_CONTRACT_MANAGEMENT => [
                Excel::COLUMN_POSITION => 3
            ]
        ],
        'COMPANY_NAME' => [
            Excel::COLUMN_DATA_INDEX => 'COMPANY_NAME',
            Excel::COLUMN_TITLE => 'ISO-Firma',
            Excel::COLUMN_POSITION => 21,
            Excel::COLUMN_WIDTH => 30,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 12
            ],
            Report_ContingentData::TYPE_CONTRACT_MANAGEMENT => [
                Excel::COLUMN_POSITION => 2
            ]
        ],
        'CUSTOMER_NAME' => [
            Excel::COLUMN_DATA_INDEX => 'CUSTOMER_NAME',
            Excel::COLUMN_TITLE => 'Kunde',
            Excel::COLUMN_POSITION => 22,
            Excel::COLUMN_WIDTH => 35,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 13
            ],
            Report_ContingentData::TYPE_CONTRACT_MANAGEMENT => [
                Excel::COLUMN_POSITION => 1
            ]
        ],
        'SAP_ORDER' => [
            Excel::COLUMN_DATA_INDEX => 'SAP_ORDER',
            Excel::COLUMN_TITLE => 'SAP-Auftragsnummer',
            Excel::COLUMN_POSITION => 23,
            Excel::COLUMN_WIDTH => 11,
        ],
        'SAP_ORDER_POSITION' => [
            Excel::COLUMN_DATA_INDEX => 'SAP_ORDER_POSITION',
            Excel::COLUMN_TITLE => 'SAP-Auftragsposition',
            Excel::COLUMN_POSITION => 24,
            Excel::COLUMN_WIDTH => 11,
        ],
        'READY_FOR_INVOICING' => [
            Excel::COLUMN_DATA_INDEX => 'READY_FOR_INVOICING',
            Excel::COLUMN_TITLE => 'Bereit für Fakturierung',
            Excel::COLUMN_POSITION => 25,
            Excel::COLUMN_CONVERTER => 'convertToYesNo'
        ],
        'EFA_ID' => [
            Excel::COLUMN_DATA_INDEX => 'EFA_ID',
            Excel::COLUMN_TITLE => 'eFA-ID',
            Excel::COLUMN_POSITION => 26
        ],
        'INVOICE_TEXT' => [
            Excel::COLUMN_DATA_INDEX => 'INVOICE_TEXT',
            Excel::COLUMN_TITLE => 'Rechnungstext',
            Excel::COLUMN_POSITION => 27,
            Excel::COLUMN_WIDTH => 11,
        ],
        'INVOICE_NUMBER' => [
            Excel::COLUMN_DATA_INDEX => 'INVOICE_NUMBER',
            Excel::COLUMN_TITLE => 'Rechnungsnummer',
            Excel::COLUMN_POSITION => 28,
            Excel::COLUMN_WIDTH => 20,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 16
            ],
        ],
        'SUPPLIER_ORDER' => [
            Excel::COLUMN_DATA_INDEX => 'SUPPLIER_ORDER',
            Excel::COLUMN_TITLE => 'Lieferanten-Bestellnummer',
            Excel::COLUMN_POSITION => 29,
            Excel::COLUMN_WIDTH => 15,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 15
            ],
        ],
        'CONTINGENT_HOURS' => [
            Excel::COLUMN_DATA_INDEX => 'CONTINGENT_HOURS',
            Excel::COLUMN_TITLE => 'Kontingent-Std',
            Excel::COLUMN_POSITION => 19,
            Excel::COLUMN_WIDTH => 11,
            Report_ContingentData::TYPE_CONTRACT_MANAGEMENT => [
                Excel::COLUMN_POSITION => 12
            ]
        ],
        'INVOICING_PROCEDURE' => [
            Excel::COLUMN_DATA_INDEX => 'INVOICING_PROCEDURE',
            Excel::COLUMN_TITLE => 'Abrechnungsverfahren',
            Excel::COLUMN_POSITION => 7,
            Excel::COLUMN_WIDTH => 15,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 6
            ],
        ],
        'CREDIT_VOUCHER' => [
            Excel::COLUMN_DATA_INDEX => 'CREDIT_VOUCHER',
            Excel::COLUMN_TITLE => 'Gutschriftsnr',
            Excel::COLUMN_POSITION => 8,
            Excel::COLUMN_WIDTH => 11,
            Report_ContingentData::TYPE_PURCHASE_INVOICE_CHECK => [
                Excel::COLUMN_POSITION => 7
            ],
        ]
    ];

    /**
     * @var int
     */
    protected $reportType;

    /**
     * Excel_ContingentData constructor.
     * @param PHPExcel_Worksheet $worksheet
     * @param int $reportType
     */
    public function __construct(PHPExcel_Worksheet $worksheet, int $reportType)
    {
        $this->reportType = $reportType;
        parent::__construct($worksheet);
    }

    /**
     * builds config for worksheet creation
     */
    protected function getColumnConfig(): array
    {
        $columns = [];
        $applicableColumns = $this->getApplicableColumnConfig();
        foreach (self::$columnConfig as $column => $cfg) {
            if ($this->reportType === Report_ContingentData::TYPE_ALL) {
                $position = $cfg[Excel::COLUMN_POSITION];
            } elseif (!isset($cfg[$this->reportType])) {
                continue;
            } else {
                $position = $cfg[$this->reportType][Excel::COLUMN_POSITION];
            }
            if (isset($cfg[$this->reportType])) {
                unset($cfg[$this->reportType]);
            }
            $applicableConfig = [];
            $cfg[Excel::COLUMN_POSITION] = $position;
            foreach ($applicableColumns as $c => $mandatory) {
                if (array_key_exists($c, $cfg)) {
                    $applicableConfig[$c] = $cfg[$c];
                }
            }
            $columns[] = $applicableConfig;
        }
        return $columns;
    }

    /**
     * @param string|null $value
     * @return string
     */
    public function convertToYesNo($value): string
    {
        return (int)$value === 1 ? 'Ja' : 'Nein';
    }

    /**
     * @param string|null $value
     * @return string
     * @throws Exception
     */
    public function convertToNamedMonth($value): string
    {
        $date = new DateTime($value);
        return $date->format('M Y');
    }
}
