<?php

/**
 *
 */
class OrderOverview
{
    /**
     * @var Zend_Db_Adapter_Abstract
     */
    private $db;

    /**
     * @var Zend_Db_Table_Row
     */
    private $profile;

    /**
     * @var string[]
     */
    private $triggerInvoiceLoadingOrderKinds = [
        'ZV',
        'ZMF'
    ];

    /**
     * Local cache to avoid loading nodes over and over
     *
     * @var array
     */
    private $nodes;

    /**
     * @var Zend_Date
     */
    private $reportBeginDate;

    /**
     * @var Zend_Date
     */
    private $reportEndDate;

    /**
     * @var ?array
     */
    private $materialFilter;

    /**
     * @var array
     */
    private $materialFilterValues;

    /**
     * @var Iso_Log_StopWatch
     */
    private $profiler;

    /**
     * @var string
     */
    private $materialFilterCondition;

    /**
     * @param Zend_Db_Adapter_Abstract $db
     * @param Zend_Db_Table_Row $profile
     * @throws Zend_Exception
     */
    public function __construct(Zend_Db_Adapter_Abstract $db, Zend_Db_Table_Row $profile)
    {
        $this->db = $db;
        $this->profile = $profile;
        $this->profiler = Zend_Registry::get('performanceLog');
    }

    /**
     * @return array
     * @throws Zend_Db_Statement_Exception
     */
    public function getData(): array
    {
        $loadInvoice = false;
        $ordersInPeriod = [];
        $ordersNotInPeriod = [];

        $result = $this->processOrders($this->loadOrders(), $loadInvoice, $ordersInPeriod, $ordersNotInPeriod);

        $this->profiler->logStage('OrderOverview: orders loaded and processed');
        if (!empty($ordersInPeriod) || !empty($ordersNotInPeriod)) {
            $this->processOrderPositions($this->loadOrderPositions($ordersInPeriod, $ordersNotInPeriod), $result['ORDERS']);
            $this->profiler->logStage('OrderOverview: positions loaded and processed');
        }
        if ($loadInvoice) {
            $this->processInvoices($this->loadInvoicePlanElements($ordersInPeriod, $ordersNotInPeriod), $result['ORDERS']);
            $this->profiler->logStage('OrderOverview: planElements loaded and processed');
        }

        return $result;
    }

    /**
     * @return Zend_Db_Statement
     */
    private function loadOrders(): Zend_Db_Statement
    {
        $bookingTreeNodes = $this->getNodes();

        $openOrdersOnly = null;
        $includeOrdersStartingAfterPeriod = null;
        $includeOrdersStartingBeforePeriod = null;
        $includeClosedOrderPositions = null;
        $includeClosedInvoices = null;
        $includeInvoicesAfterPeriod = null;
        $includeInvoicesBeforePeriod = null;
        $futureDateLimit = " invoice_date > TO_DATE(:END_OF_PERIOD, 'yyyy-mm-dd') + INTERVAL '1' YEAR";
        $materialFilterCondition = $this->buildMaterialFilterCondition();

        if (!$this->profile->CLOSED_ORDERS) {
            # only open orders
            $openOrdersOnly = ' AND view_order.order_is_open = 1';
            $includeClosedOrderPositions = " AND order_position.position_is_open = 1 ";

            # plan elements filter
            $includeClosedInvoices = " AND (invoice_plan_element.status != 'C' AND invoice_plan_element.lock != '81' AND invoice_plan_element.invoice_value <> 0) ";
        } else {
            if (!$this->profile->WITHOUT_ORDERS_AFTER_PERIOD) {
                # plan elements filter
                $includeClosedInvoices = " AND invoice_plan_element.invoice_date >= TO_DATE(:BEGIN_OF_PERIOD, 'yyyy-mm-dd') ";
            }
        }

        if ($this->profile->ORDER_AFTER_PERIOD) {
            $includeOrdersStartingAfterPeriod = " OR (
              view_order.begin_computed > TO_DATE(:END_OF_PERIOD, 'yyyy-mm-dd')
              AND view_order.order_is_open = 1
            )";

            # plan element filter
            $includeInvoicesAfterPeriod = " OR invoice_date > TO_DATE(:END_OF_PERIOD, 'yyyy-mm-dd') ";
        }

        # without_orders_after_period is totally misleading, see spec BCAUF-030/030 for checkbox "ohne offene Auftragspositionen vor dem ausgewählten Zeitraum"
        # is used in followup queries below as well
        # meaning has probably been changed in some point of time but was not renamed in config columns
        if (!$this->profile->WITHOUT_ORDERS_AFTER_PERIOD) {
            $includeOrdersStartingBeforePeriod = " OR (
              view_order.end_computed < TO_DATE(:BEGIN_OF_PERIOD, 'yyyy-mm-dd')
              AND view_order.order_is_open = 1
            )
            ";
        } else {
            # plan element filter
            $includeInvoicesBeforePeriod = " invoice_date >= TO_DATE(:BEGIN_OF_PERIOD, 'yyyy-mm-dd') AND ";
        }

        $sql = /** @lang SQL */"
            WITH RECURSIVE node_selection (id, debtor_number) AS (
                SELECT
                    node.id,
                    node_version.debtor_number
                FROM node
                INNER JOIN node_version
                    ON node_version.node_id = node.id
                    AND node_version.version_state_id = 1
                WHERE " . QueryBuilder::createInClause('node.id', $bookingTreeNodes) . "
                
                UNION ALL
                
                SELECT 
                    node.id,
                    node_version.debtor_number
                FROM node 
                INNER JOIN node_version
                    ON node_version.node_id = node.id
                    AND node_version.version_state_id = 1
                INNER JOIN node_selection ON node_selection.id = node.parent_node_id
            ),
            invoice_plan_elements AS (
                SELECT
                    invoice_plan_element.*
                FROM invoice_plan_element
                INNER JOIN order_position ON INVOICE_PLAN_ELEMENT.ORDER_NUMBER = ORDER_POSITION.ORDER_NUMBER and INVOICE_PLAN_ELEMENT.POSITION_NUMBER = ORDER_POSITION.POSITION_NUMBER
                WHERE (
                        $includeInvoicesBeforePeriod 
                        invoice_plan_element.invoice_date <= TO_DATE(:END_OF_PERIOD, 'yyyy-mm-dd') 
                        $includeInvoicesAfterPeriod
                    )
                $includeClosedInvoices
                -- see BCAUF-30/030
                AND 1 = CASE WHEN $futureDateLimit
                    THEN 0
                    ELSE 1
                END
            )
            SELECT
                t_order.debtor_number,
                t_order.invoice_receiver_number,
                t_order.order_number,
                t_order.order_kind,
                t_order.customer_name,
                t_order.customer_order_number,
                to_char(view_order.begin_computed, 'DD.MM.YYYY') AS BEGIN,
                to_char(view_order.end_computed, 'DD.MM.YYYY') AS END,
                sums_of_order_positions.commissioned,
                sums_of_order_positions.invoiced,
                CASE 
                    WHEN view_order.order_is_open = 0 
                        THEN 0
                        ELSE COALESCE(sums_of_order_positions.outstanding, 0)
                END AS OUTSTANDING,
                CASE
                  WHEN
                      view_order.begin_computed <= to_date(:END_OF_PERIOD, 'yyyy-mm-dd')
                      AND COALESCE(view_order.end_computed, to_date(:BEGIN_OF_PERIOD, 'yyyy-mm-dd')) >= to_date(:BEGIN_OF_PERIOD, 'yyyy-mm-dd')
                  THEN 1
                  ELSE 0
                END AS in_configured_period
            FROM \"order\" t_order
            INNER JOIN sap.view_order
                ON t_order.order_number = view_order.order_number
            INNER JOIN (
                SELECT
                    order_number,
                    SUM(commissioned) AS commissioned,
                    SUM(invoiced) AS invoiced,
                    SUM(outstanding) AS outstanding
                FROM (
                    SELECT 
                        order_position.order_number,
                        CASE WHEN 
                                EXISTS (SELECT NULL FROM invoice_plan_elements WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                            THEN
                                (SELECT SUM(invoice_value) FROM invoice_plan_elements WHERE order_number = order_position.order_number AND order_position.position_number = position_number GROUP BY position_number)
                            WHEN 
                                NOT EXISTS (SELECT NULL FROM invoice_plan_element WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                                THEN order_position.net_value
                            ELSE 0
                         END commissioned,
                         CASE
                            WHEN 
                                EXISTS (SELECT NULL FROM invoice_plan_elements WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                            THEN
                                (
                                    SELECT SUM(invoice_value) 
                                    FROM invoice_plan_elements 
                                    WHERE order_number = order_position.order_number 
                                      AND order_position.position_number = position_number
                                      AND invoice_plan_elements.status = 'C'
                                    GROUP BY position_number
                                )
                            WHEN 
                                NOT EXISTS (SELECT NULL FROM invoice_plan_element WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                            THEN order_position.sum_billed_amount
                            ELSE 0
                         END invoiced,
                        CASE WHEN order_position.position_is_open = 1
                               THEN CASE WHEN 
                                        EXISTS (SELECT NULL FROM invoice_plan_elements WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                                    THEN
                                        (
                                            SELECT SUM(invoice_value) 
                                            FROM invoice_plan_elements 
                                            WHERE order_number = order_position.order_number 
                                              AND order_position.position_number = position_number
                                              AND invoice_plan_elements.status <> 'C' AND invoice_plan_elements.lock <> '81'
                                            GROUP BY position_number
                                        )
                                    WHEN 
                                        NOT EXISTS (SELECT NULL FROM invoice_plan_element WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                                    THEN net_value - sum_billed_amount 
                               END
                         ELSE 0
                        END outstanding
                    FROM order_position
                    INNER JOIN \"order\" t_order
                        ON t_order.order_number = order_position.order_number
                    INNER JOIN node_selection
                        ON t_order.debtor_number::bigint = node_selection.debtor_number
                    INNER JOIN sap.view_order 
                        ON view_order.order_number = t_order.order_number
                    WHERE order_position.has_subposition = 0
                    $materialFilterCondition
                    $includeClosedOrderPositions
                    AND (
                        EXISTS (SELECT NULL FROM invoice_plan_elements WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                        OR 
                        NOT EXISTS (SELECT NULL FROM invoice_plan_element WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                    )
                    -- distinguish between open and closed positions to handle mixed orders with different kinds of materials and position states
                    AND 1 = CASE 
                        WHEN view_order.begin_computed <= to_date(:END_OF_PERIOD, 'yyyy-mm-dd')
                            THEN 1
                            ELSE 
                                CASE WHEN order_position.position_is_open = 1
                                    -- open positions always
                                    THEN 1
                                    -- closed positions only in period
                                    ELSE 0
                                END
                            END
                ) order_positions
                GROUP BY order_positions.order_number
            ) sums_of_order_positions
                ON t_order.order_number = sums_of_order_positions.order_number
            WHERE (
                view_order.begin_computed <= to_date(:END_OF_PERIOD, 'yyyy-mm-dd')
                AND COALESCE(view_order.end_computed, to_date(:BEGIN_OF_PERIOD, 'yyyy-mm-dd')) >= to_date(:BEGIN_OF_PERIOD, 'yyyy-mm-dd')
                $openOrdersOnly
            )
            $includeOrdersStartingAfterPeriod
            $includeOrdersStartingBeforePeriod
            ORDER BY t_order.debtor_number, view_order.begin_computed
        ";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':BEGIN_OF_PERIOD', $this->getReportBeginDate()->get(Iso_Date::ORACLE_DATE));
        $stmt->bindValue(':END_OF_PERIOD', $this->getReportEndDate()->get(Iso_Date::ORACLE_DATE));
        $stmt->execute();

        return $stmt;
    }

    /**
     * @param Zend_Db_Statement $orders
     * @param bool $loadInvoice
     * @param array $ordersInPeriod
     * @param array $ordersNotInPeriod
     * @return array
     * @throws Zend_Db_Statement_Exception
     */
    private function processOrders(Zend_Db_Statement $orders, bool &$loadInvoice, array &$ordersInPeriod, array &$ordersNotInPeriod): array
    {
        $loadInvoice = false;
        $ordersInPeriod = [];
        $ordersNotInPeriod = [];
        $result = [
            'SUMS' => [
                'NOT_INVOICED' => 0,
                'PARTIALLY_INVOICED' => 0,
                'INVOICED' => 0,
                'COMMISSIONED_COMPUTED' => 0,
                'INVOICED_COMPUTED' => 0,
                'OUTSTANDING_COMPUTED' => 0,
                'ORDERS_COMPUTED' => 0
            ],
            'ORDERS' => []
        ];

        while ($order = $orders->fetch(PDO::FETCH_ASSOC)) {
            if ((int)$order['IN_CONFIGURED_PERIOD'] === 1) {
                $ordersInPeriod[] = $order['ORDER_NUMBER'];
            } else {
                $ordersNotInPeriod[] = $order['ORDER_NUMBER'];
            }

            if (in_array($order['ORDER_KIND'], $this->triggerInvoiceLoadingOrderKinds)) {
                $loadInvoice = true;
            }

            $result['SUMS']['COMMISSIONED_COMPUTED'] += $order['COMMISSIONED'];
            $result['SUMS']['INVOICED_COMPUTED'] += $order['INVOICED'];
            $result['SUMS']['OUTSTANDING_COMPUTED'] += $order['OUTSTANDING'];
            $result['SUMS']['ORDERS_COMPUTED']++;

            /* custom status calculation */
            if ($order['OUTSTANDING'] == 0) {
                $order['STATUS'] = InvoiceType::INVOICED;
                $result['SUMS']['INVOICED'] += 1;
            } elseif ($order['INVOICED'] != 0) {
                $order['STATUS'] = InvoiceType::PARTIALLY_INVOICED;
                $result['SUMS']['PARTIALLY_INVOICED'] += 1;
            } else {
                $order['STATUS'] = InvoiceType::NOT_INVOICED;
                $result['SUMS']['NOT_INVOICED'] += 1;
            }

            $result['ORDERS'][$order['ORDER_NUMBER']] = $order;
        }

        return $result;
    }

    /**
     * @param array $ordersInPeriod
     * @param array $ordersNotInPeriod
     * @return Zend_Db_Statement
     */
    private function loadOrderPositions(array $ordersInPeriod, array $ordersNotInPeriod): Zend_Db_Statement
    {
        $useBeginOfPeriod = false;
        $orderPositionsWhere = null;
        $includeClosedOrderPositions = null;
        $includeHigherLevelPositions = null;
        $includeClosedInvoices = null;
        $includeInvoicesAfterPeriod = null;
        $includeInvoicesBeforePeriod = null;
        $futureDateLimit = " invoice_date > TO_DATE(:END_OF_PERIOD, 'yyyy-mm-dd') + INTERVAL '1' YEAR";
        $materialFilterCondition = $this->buildMaterialFilterCondition();

        if (!$this->profile->WITH_HIGHER_LEVEL_POSITION) {
            $includeHigherLevelPositions = " AND has_subposition = 0 ";
        }

        if (!$this->profile->CLOSED_ORDERS) {
            $includeClosedOrderPositions = " AND order_position.position_is_open = 1 ";
            # plan elements filter
            $includeClosedInvoices = " AND (invoice_plan_element.status <> 'C' AND invoice_plan_element.lock <> '81' AND invoice_plan_element.invoice_value <> 0) ";
        } else {
            if (!$this->profile->WITHOUT_ORDERS_AFTER_PERIOD) {
                # plan elements filter
                $includeClosedInvoices = " AND invoice_plan_element.invoice_date >= TO_DATE(:BEGIN_OF_PERIOD, 'yyyy-mm-dd') ";
                $useBeginOfPeriod = true;
            }
        }

        if ($this->profile->ORDER_AFTER_PERIOD) {
            # plan element filter
            $includeInvoicesAfterPeriod = " OR invoice_date > TO_DATE(:END_OF_PERIOD, 'yyyy-mm-dd') ";
        }

        if (!empty($ordersInPeriod)) {
            $orderPositionsWhere .= " ( " . QueryBuilder::createInClause('order_position.order_number', $ordersInPeriod, false) . $includeClosedOrderPositions . ") ";
        }

        if (!empty($ordersNotInPeriod)) {
            if (!empty($ordersInPeriod)) {
                $orderPositionsWhere .= " OR ";
            }
            $orderPositionsWhere .= " (  " . QueryBuilder::createInClause('order_position.order_number', $ordersNotInPeriod, false) . " AND order_position.position_is_open = 1) ";
        }

        if ($this->profile->WITHOUT_ORDERS_AFTER_PERIOD) {
            # plan element filter
            $includeInvoicesBeforePeriod = " invoice_date >= TO_DATE(:BEGIN_OF_PERIOD, 'yyyy-mm-dd') AND ";
            $useBeginOfPeriod = true;
        }

        $sql = /** @lang SQL */"
           WITH invoice_plan_elements AS (
                SELECT
                    invoice_plan_element.*
                FROM invoice_plan_element
                INNER JOIN order_position ON INVOICE_PLAN_ELEMENT.ORDER_NUMBER = ORDER_POSITION.ORDER_NUMBER and INVOICE_PLAN_ELEMENT.POSITION_NUMBER = ORDER_POSITION.POSITION_NUMBER
                WHERE (
                        $includeInvoicesBeforePeriod 
                        invoice_plan_element.invoice_date <= TO_DATE(:END_OF_PERIOD, 'yyyy-mm-dd') 
                        $includeInvoicesAfterPeriod
                    )
                $includeClosedInvoices
                -- see BCAUF-30/030
                AND 1 = CASE WHEN $futureDateLimit
                    THEN 0
                    ELSE 1
                END
            )
            SELECT
                order_position.order_number,
                booking_type.name AS booking_type_name,
                order_position.sap_key,
                order_position.position_number,
                order_position.material,
                order_position.description,
                order_position.material_text,
                order_position.quantity,
                order_position.unit,
                order_position.unit_price,
                CASE WHEN 
                        EXISTS (SELECT NULL FROM invoice_plan_elements WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                    THEN 1
                    ELSE 0
                END has_plan_elements,
                 CASE WHEN 
                        EXISTS (SELECT NULL FROM invoice_plan_elements WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                    THEN
                        (
                            SELECT SUM(invoice_value) 
                            FROM invoice_plan_elements 
                            WHERE order_number = order_position.order_number 
                              AND order_position.position_number = position_number
                            GROUP BY position_number
                        )
                    WHEN 
                        NOT EXISTS (SELECT NULL FROM invoice_plan_element WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                        THEN order_position.net_value
                    ELSE 0
                END net_value,
                CASE WHEN 
                       EXISTS (SELECT NULL FROM invoice_plan_elements WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                    THEN
                        (
                            SELECT SUM(invoice_value) 
                            FROM invoice_plan_elements 
                            WHERE order_number = order_position.order_number 
                              AND order_position.position_number = position_number
                              AND invoice_plan_elements.status = 'C'
                            GROUP BY position_number
                        )
                    WHEN 
                        NOT EXISTS (SELECT NULL FROM invoice_plan_element WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                        THEN order_position.sum_billed_amount
                    ELSE 0
                END sum_billed_amount,
                CASE WHEN 
                       EXISTS (SELECT NULL FROM invoice_plan_elements WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                    THEN
                        (
                            SELECT SUM(invoice_value) 
                            FROM invoice_plan_elements 
                            WHERE order_number = order_position.order_number 
                              AND order_position.position_number = position_number
                              AND invoice_plan_elements.lock = '81'
                            GROUP BY position_number
                        )
                    WHEN 
                        NOT EXISTS (SELECT NULL FROM invoice_plan_element WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                        THEN order_position.sum_billed_amount
                    ELSE 0
                END sum_not_billed,
                order_position.position_is_open,
                CASE
                    WHEN order_position.unit IN ('BT', 'BS') THEN order_position.unit
                END AS bt_bs
            FROM order_position
            -- left join as not all sap-keys are present in bookingtree data
            LEFT JOIN node_version
                ON (
                    node_version.sap_key_cost_object = order_position.sap_key
                    OR node_version.sap_key_cost_center = order_position.sap_key
                )
                AND node_version.version_state_id = 1
            LEFT JOIN booking_type
                ON node_version.booking_type_id = booking_type.id
            WHERE ($orderPositionsWhere) $includeHigherLevelPositions 
            $materialFilterCondition
            AND (
                EXISTS (SELECT NULL FROM invoice_plan_elements WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
                OR NOT EXISTS (SELECT NULL FROM invoice_plan_element WHERE order_number = order_position.order_number AND order_position.position_number = position_number)
            )
            ORDER BY order_position.order_number, order_position.position_number
        ";

        $stmt = $this->db->prepare($sql);
        if ($useBeginOfPeriod) {
            $stmt->bindValue(':BEGIN_OF_PERIOD', $this->getReportBeginDate()->get(Iso_Date::ORACLE_DATE));
        }
        $stmt->bindValue(':END_OF_PERIOD', $this->getReportEndDate()->get(Iso_Date::ORACLE_DATE));
        $stmt->execute();
        return $stmt;
    }

    /**
     * @param Zend_Db_Statement $positions
     * @param array $dataToEnrich
     * @throws Zend_Db_Statement_Exception
     */
    private function processOrderPositions(Zend_Db_Statement $positions, array &$dataToEnrich)
    {
        while ($position = $positions->fetch(PDO::FETCH_ASSOC)) {
            $outstandingInEuro = 0;
            if ($position['POSITION_IS_OPEN'] == 1) {
                if ($position['HAS_PLAN_ELEMENTS'] == 1) {
                    # subtract invoice_plan_elements that will not be billed
                    $outstandingInEuro = ($position['NET_VALUE'] - $position['SUM_NOT_BILLED']) - $position['SUM_BILLED_AMOUNT'];
                }
                else {
                    if ($position['NET_VALUE'] >= 0) {
                        $outstandingInEuro = max($position['NET_VALUE'] - $position['SUM_BILLED_AMOUNT'], 0);
                    } else {
                        $outstandingInEuro = min($position['NET_VALUE'] - $position['SUM_BILLED_AMOUNT'], 0);
                    }
                }
            }
            $position['OUTSTANDING_IN_EURO'] = $outstandingInEuro;

            /* calculate outstanding */
            if (($position['UNIT'] === "BT" || $position['UNIT'] === "BS") && $outstandingInEuro > 0 && $position['UNIT_PRICE'] != 0) {
                $position['OUTSTANDING'] = $outstandingInEuro / $position['UNIT_PRICE'];
            } else {
                $position['OUTSTANDING'] = null;
            }

            /* Status */
            if ($outstandingInEuro == 0) {
                $position['STATUS'] = InvoiceType::INVOICED;
            } elseif ($position['SUM_BILLED_AMOUNT'] != 0) {
                $position['STATUS'] = InvoiceType::PARTIALLY_INVOICED;
            } else {
                $position['STATUS'] = InvoiceType::NOT_INVOICED;
            }

            /*Booking type name*/
            if ($position['BOOKING_TYPE_NAME'] == null) {
                $position['BOOKING_TYPE_NAME'] = "SAP";
            }

            $dataToEnrich[$position['ORDER_NUMBER']]['ORDER_POSITIONS'][$position['POSITION_NUMBER']] = $position;
        }
    }

    /**
     * @param array $ordersInPeriod
     * @param array $ordersNotInPeriod
     * @return Zend_Db_Statement
     */
    private function loadInvoicePlanElements(array $ordersInPeriod, array $ordersNotInPeriod): Zend_Db_Statement
    {
        $useBeginOfPeriod = false;
        $includeClosedInvoices = null;
        $includeInvoicesAfterPeriod = null;
        $invoicePlanWhere = null;
        $includeInvoicesBeforePeriod = null;
        $futureDateLimit = " invoice_date > TO_DATE(:END_OF_PERIOD, 'yyyy-mm-dd') + INTERVAL '1' YEAR";
        $materialFilterCondition = $this->buildMaterialFilterCondition();

        if (!$this->profile->CLOSED_ORDERS) {
            $includeClosedInvoices = "AND (invoice_plan_element.status <> 'C' AND invoice_plan_element.lock <> '81' AND invoice_plan_element.invoice_value <> 0)";
        } else {
            if (!$this->profile->WITHOUT_ORDERS_AFTER_PERIOD) {
                # plan elements filter
                $includeClosedInvoices = " AND invoice_plan_element.invoice_date >= TO_DATE(:BEGIN_OF_PERIOD, 'yyyy-mm-dd') ";
                $useBeginOfPeriod = true;
            }
        }
        if ($this->profile->ORDER_AFTER_PERIOD) {
            $includeInvoicesAfterPeriod = " OR invoice_date > TO_DATE(:END_OF_PERIOD, 'yyyy-mm-dd') ";
        }
        if ($this->profile->WITHOUT_ORDERS_AFTER_PERIOD) {
            $includeInvoicesBeforePeriod = " invoice_date >= TO_DATE(:BEGIN_OF_PERIOD, 'yyyy-mm-dd') AND ";
            $useBeginOfPeriod = true;
        }


        if (!empty($ordersInPeriod)) {
            $invoicePlanWhere .= " ( " . QueryBuilder::createInClause('invoice_plan_element.order_number', $ordersInPeriod, false) . $includeClosedInvoices . ") ";
        }

        if (!empty($ordersNotInPeriod)) {
            if (!empty($ordersInPeriod)) {
                $invoicePlanWhere .= " OR ";
            }
            $invoicePlanWhere .= " (  " . QueryBuilder::createInClause('invoice_plan_element.order_number', $ordersNotInPeriod, false) . $includeClosedInvoices . ") ";
        }
        $sql = "
            SELECT
                invoice_plan_element.order_number,
                invoice_plan_element.position_number,
                invoice_plan_element.element_number,
                CASE
                    WHEN 
                        invoice_plan_element.status = 'C'
                        OR invoice_plan_element.lock = '81' 
                        OR invoice_plan_element.invoice_value = 0
                        THEN " . InvoiceType::INVOICED . "
                    ELSE " . InvoiceType::NOT_INVOICED . "
                END AS STATUS,
                invoice_plan_element.text,
                invoice_plan_element.invoice_period,
                to_char(invoice_plan_element.invoice_date, 'DD.MM.YYYY') AS invoice_date,
                invoice_plan_element.invoice_value as COMMISSIONED,
                CASE
                    WHEN invoice_plan_element.status = 'C' THEN INVOICE_PLAN_ELEMENT.INVOICE_VALUE
                    ELSE 0
                END AS INVOICED,
                CASE
                    WHEN invoice_plan_element.status = 'C' OR invoice_plan_element.lock = '81' THEN 0
                    ELSE INVOICE_PLAN_ELEMENT.INVOICE_VALUE
                END AS OUTSTANDING_IN_EURO
            FROM invoice_plan_element
            INNER JOIN order_position
            ON order_position.position_number = invoice_plan_element.position_number AND order_position.order_number = invoice_plan_element.order_number
            WHERE order_position.HAS_SUBPOSITION = 0 
                AND (" . $invoicePlanWhere . ") 
                AND (" . $includeInvoicesBeforePeriod . " invoice_plan_element.invoice_date <= TO_DATE(:END_OF_PERIOD, 'yyyy-mm-dd') $includeInvoicesAfterPeriod)
                $materialFilterCondition
                -- see BCAUF-30/030
                AND 1 = CASE WHEN $futureDateLimit
                    THEN 0
                    ELSE 1
                END
            ORDER BY invoice_plan_element.element_number
        ";

        $stmt = $this->db->prepare($sql);
        if ($useBeginOfPeriod) {
            $stmt->bindValue(':BEGIN_OF_PERIOD', $this->getReportBeginDate()->get(Iso_Date::ORACLE_DATE));
        }
        $stmt->bindValue(':END_OF_PERIOD', $this->getReportEndDate()->get(Iso_Date::ORACLE_DATE));
        $stmt->execute();

        return $stmt;
    }

    /**
     * @param Zend_Db_Statement $invoices
     * @param $dataToEnrich
     * @throws Zend_Db_Statement_Exception
     */
    private function processInvoices(Zend_Db_Statement $invoices, &$dataToEnrich)
    {
        while ($invoice = $invoices->fetch(PDO::FETCH_ASSOC)) {

            if (isset($dataToEnrich[$invoice['ORDER_NUMBER']]['ORDER_POSITIONS'][$invoice['POSITION_NUMBER']])) {
                $dataToEnrich[$invoice['ORDER_NUMBER']]['ORDER_POSITIONS'][$invoice['POSITION_NUMBER']]['INVOICE_PLAN_ELEMENTS'][$invoice['ELEMENT_NUMBER']] = $invoice;
            }
        }
    }

    /**
     * @return int[]
     */
    public function getNodes(): array
    {
        if ($this->nodes !== null) {
            return $this->nodes;
        }
        $nodes = ChartProfil::getProfileNodes($this->profile->ID, $this->db);
        if (empty($nodes)) {
            // unspecified fallback to root node of booking tree (taken from existing code)
            $nodes = [1];
        }
        $this->nodes = $nodes;
        return $this->nodes;
    }

    /**
     * @return Zend_Date
     */
    public function getReportBeginDate(): Zend_Date
    {
        if ($this->reportBeginDate instanceof Zend_Date) {
            return $this->reportBeginDate;
        }
        // ugly fetching begin/end-date for report; but as long there is no effort for re-factoring...
        Date::determinePeriod($this->profile->ID, $this->reportBeginDate, $this->reportEndDate, $this->db);
        return $this->reportBeginDate;
    }

    /**
     * @return Zend_Date
     */
    public function getReportEndDate(): Zend_Date
    {
        if ($this->reportEndDate instanceof Zend_Date) {
            return $this->reportEndDate;
        }
        // ugly fetching begin/end-date for report; but as long there is no effort for re-factoring...
        Date::determinePeriod($this->profile->ID, $this->reportBeginDate, $this->reportEndDate, $this->db);
        return $this->reportEndDate;
    }

    /**
     * @return array
     */
    public function getMaterialFilter(): array
    {
        if ($this->materialFilter !== null) {
            return $this->materialFilter;
        }
        $this->materialFilter = (new ChartProfileMaterialFilter())->getMaterialFilter($this->profile->ID);
        return $this->materialFilter;
    }

    /**
     * @return string|null
     */
    private function buildMaterialFilterCondition()
    {
        if ($this->materialFilterCondition !== null) {
            return $this->materialFilterCondition;
        }
        $fieldName = 'order_position.material';
        $prefix = 'AND';
        $condition = null;
        $filterConditionsInclude = $filterConditionsExclude = [];

        foreach($this->getMaterialFilterValues() as $filters) {
            # escape underscores to make like work as expected; see https://stackoverflow.com/questions/19588455/why-does-using-an-underscore-character-in-a-like-filter-give-me-all-the-results
            $filterInclude = str_replace('_', "\_", $filters['FILTER_INCLUDE']);
            $filterExclude = str_replace('_', "\_", $filters['FILTER_EXCLUDE']);
            $includeConditions = $excludeConditions = [];
            if (!empty($filterInclude)) {
                $includeConditions = explode(';', $filterInclude);
            }
            if (!empty($filterExclude)) {
                $excludeConditions = explode(';', $filterExclude);
            }
            foreach ($includeConditions as $like) {
                # php backslash madness
                $filterConditionsInclude[] = str_replace('\\\\', '\\', sprintf(
                    ' %s LIKE %s ',
                    $fieldName,
                    $this->db->quote($like)
                ));
            }
            foreach ($excludeConditions as $like) {
                $filterConditionsExclude[] = str_replace('\\\\', '\\', sprintf(
                    ' %s NOT LIKE %s ',
                    $fieldName,
                    $this->db->quote($like)
                ));
            }
        }

        if ($filterConditionsInclude) {
            $condition = sprintf(
                ' %s (%s) ',
                $prefix,
                implode(" \nOR ", $filterConditionsInclude)
            );
        }
        if ($filterConditionsExclude) {
            $condition = sprintf(
                ' %s %s (%s) ',
                $condition,
                $prefix,
                implode(" \nAND ", $filterConditionsExclude)
            );
        }
        $this->materialFilterCondition = $condition;

        return $this->materialFilterCondition;
    }

    /**
     * @return mixed|null
     */
    private function getMaterialFilterValues(): array
    {
        if ($this->materialFilterValues !== null) {
            return $this->materialFilterValues;
        }
        $filterIds = $this->getMaterialFilter();
        if (!$filterIds) {
            return [];
        }
        array_walk($filterIds, function(&$value) {
            $value = (int)$value['MATERIAL_FILTER_ID'];
        });

        $m = new MaterialFilter();
        $this->materialFilterValues = $m->getMaterialFilterValues($filterIds);

        return $this->materialFilterValues;
    }
}