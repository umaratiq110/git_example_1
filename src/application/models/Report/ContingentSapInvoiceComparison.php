<?php
declare(strict_types=1);

/**
 * Considers only confirmation of expenses with hours <> 0!
 *
 * Class Report_ContingentSapInvoiceComparison
 */
class Report_ContingentSapInvoiceComparison
{
    const NODE_NAME_BA_DEBTORS = 'BA Debitoren';
    const VERSION_STATE_ACTIVE = 1;

    /**
     * @var Zend_Db_Adapter_Abstract
     */
    private $db;

    /**
     * @var string
     */
    private $month;

    /**
     * @var array
     */
    private $divisionTotalsDefaultResult = [];

    /**
     * @var array
     */
    private $baDebtorDivisions = [];

    /**
     * Report_ContingentData constructor.
     * @param Zend_Db_Adapter_Abstract $db
     * @param DateTime $month
     * @throws Zend_Db_Statement_Exception
     */
    public function __construct(Zend_Db_Adapter_Abstract $db, DateTime $month)
    {
        $this->db = $db;
        $this->month = $month;
        $this->baDebtorDivisions = $this->getBaDebtorDivisions();
        $this->buildInvoiceTotalsDefaultResult();
    }

    /**
     */
    private function buildInvoiceTotalsDefaultResult()
    {
        foreach ($this->baDebtorDivisions as $divisionName) {
            $this->divisionTotalsDefaultResult[$divisionName] = [
                'DIVISION_NAME' => $divisionName,
                'TOTAL' => 0
            ];
        }
    }

    /**
     * @throws Zend_Db_Statement_Exception
     */
    public function getBaDebtorDivisions(): array
    {
        $set = new Set();
        $startNodeIds = $set->getNodesBySetName(self::NODE_NAME_BA_DEBTORS);
        $result = [];

        $sql = sprintf(/** @lang SQL */
            'SELECT
                node.id,
                node_version.sap_key_cost_object,
                node_version.name AS node_name
            FROM node 
            INNER JOIN node_version 
                ON node_version.node_id = node.id
                AND node_version.version_state_id = :versionStateActive
            WHERE node.id IN (%s)
            ORDER BY node_version.name', $this->db->quote($startNodeIds));

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue('versionStateActive', self::VERSION_STATE_ACTIVE, PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($data as $row) {
            $divisionName = $this->extractDivisionFromString($row['NODE_NAME']);
            if (in_array($divisionName, $result) === false && in_array($divisionName, BaContingentManagement::getDivisionNames(), false) === true) {
                $result[] = $divisionName;
            }
        }

        return $result;
    }

    /**
     * Merges totals from contingentManagement and from SAP system for divisions
     *
     * @return array
     * @throws Zend_Db_Statement_Exception
     */
    public function getDivisionTotals(): array
    {
        $divisionTotalsContingent = $this->getDivisionInvoiceTotals();
        $divisionTotalsSAP = $this->getDivisionInvoiceTotalsSAP();
        $divisionExpectedTotalInvoice = $this->getDivisionExpectedInvoiceTotals();

        $result = [];
        foreach ($divisionTotalsContingent as $division => $data) {
            $result[$division] = $data;
            $result[$division]['SAP_TOTAL'] = $divisionTotalsSAP[$division]['TOTAL'];
            $result[$division]['EXPECTED_TOTAL'] = $divisionExpectedTotalInvoice[$division]['TOTAL'];
        }
        return $result;
    }

    /**
     * @return array
     */
    public function getDivisionInvoiceTotals(): array
    {
        $sql = sprintf(/** @lang SQL */
            "WITH coe AS (
                SELECT
                    coe.id,
                    division.name AS divisionName,
                    round((coe.hours * coalesce(contingent.special_hourly_rate_sale, priceGroupSale.HOURLY_RATE))::DECIMAL, 2) AS total
                FROM BA_CONFIRMATION_OF_EXPENSE coe
                JOIN BA_CONTINGENT contingent
                    ON coe.contingent_id = contingent.id
                JOIN BA_REQUISITION_NOTE requisitionNote
                    ON requisitionNote.id = contingent.requisition_note_id
                JOIN BA_FRAMEWORK_CONTRACT frameworkContract
                    ON frameworkContract.id = requisitionNote.framework_contract_id
                JOIN DIVISION 
                    ON division.id = frameworkContract.division_id
                JOIN BA_PRICE_GROUP_SALE priceGroupSale
                    ON priceGroupSale.id = contingent.PRICE_GROUP_SALE_ID
                WHERE date_trunc('month', coe.month) = date_trunc('month', to_date(:month, 'YYYY-MM'))
                    AND coe.hours <> 0
            )
            SELECT
                coe.divisionName AS division_name,
                sum(coe.total) AS total
            FROM ZAHLUNGSPAKET
            JOIN coe
                ON coe.id = ZAHLUNGSPAKET.CONFIRMATION_OF_EXPENSE_ID
                AND ZAHLUNGSPAKET.NEXT_ID IS NULL
                AND ZAHLUNGSPAKET.FAKTURIERUNGSID IS NOT NULL
            WHERE coe.divisionName IN (%s)
            GROUP BY coe.divisionName
            ORDER BY coe.divisionName",
            $this->db->quote($this->baDebtorDivisions)
        );

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue('month', $this->month->format('Y-m'));
        $stmt->execute();

        $data = $stmt->fetchAll();
        $result = $this->divisionTotalsDefaultResult;
        foreach ($data as $row) {
            $result[$row['DIVISION_NAME']] = $row;
        }
        return $result;
    }

    /**
     * @return array
     * @throws Zend_Db_Statement_Exception
     */
    public function getDivisionInvoiceTotalsSAP(): array
    {
        $sapKeys = $this->getSAPKeysForDivisions();
        $sql = null;

        foreach ($sapKeys as $division => $keys) {
            if ($sql !== null) {
                $sql .= " UNION ";
            }

            $sql .= "SELECT '". $division . "' as division_name,
                SUM (amount) over() AS total 
                FROM COST_AND_REVENUE_PER_POSITION
                WHERE " . QueryBuilder::createInClause('SAP_KEY', $keys, false) . "
                AND date_trunc('month', month) = date_trunc('month', to_date(:month, 'YYYY-MM'))
                AND COST_AND_REVENUE_PER_POSITION.AMOUNT_KIND = :amountKindRevenue";
        }

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue('month', $this->month->format('Y-m'));
        $stmt->bindValue('amountKindRevenue', Sap::AMOUNT_KIND_REVENUE);;
        $stmt->execute();
        $data = $stmt->fetchAll();
        $result = $this->divisionTotalsDefaultResult;

        foreach ($data as $row) {
            $result[$row['DIVISION_NAME']] = $row;
        }
        return $result;
    }

    /**
     * @return array
     */
    public function getDivisionExpectedInvoiceTotals(): array
    {
        $sql = sprintf(/** @lang SQL */
            "WITH coe AS (
                SELECT
                    division.name AS divisionName,
                    round((coe.hours * coalesce(contingent.special_hourly_rate_sale, priceGroupSale.HOURLY_RATE))::DECIMAL, 2) AS total
                FROM BA_CONFIRMATION_OF_EXPENSE coe
                JOIN BA_CONTINGENT contingent
                    ON coe.contingent_id = contingent.id
                JOIN BA_REQUISITION_NOTE requisitionNote
                    ON requisitionNote.id = contingent.requisition_note_id
                JOIN BA_FRAMEWORK_CONTRACT frameworkContract
                    ON frameworkContract.id = requisitionNote.framework_contract_id
                JOIN DIVISION 
                    ON division.id = frameworkContract.division_id
                JOIN BA_PRICE_GROUP_SALE priceGroupSale
                    ON priceGroupSale.id = contingent.PRICE_GROUP_SALE_ID
                WHERE date_trunc('month', coe.month) = date_trunc('month', to_date(:month, 'YYYY-MM'))
                    AND coe.hours <> 0
            )
            SELECT
                coe.divisionName AS division_name,
                sum(coe.total) AS total
            FROM coe
            WHERE coe.divisionName IN (%s)
            GROUP BY coe.divisionName
            ORDER BY coe.divisionName",
            $this->db->quote($this->baDebtorDivisions)
        );

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue('month', $this->month->format('Y-m'));
        $stmt->execute();

        $data = $stmt->fetchAll();
        $result = $this->divisionTotalsDefaultResult;
        foreach ($data as $row) {
            $result[$row['DIVISION_NAME']] = $row;
        }

        return $result;
    }

    /**
     * @return array
     * @throws Zend_Db_Statement_Exception
     */
    private function getSAPKeysForDivisions(): array
    {
        $set = new Set();
        $startNodeIds = $set->getNodesBySetName(self::NODE_NAME_BA_DEBTORS);

        $sql = sprintf(/** @lang SQL */'
            WITH RECURSIVE nodes AS (
                SELECT
                    node.id,
                    node_version.sap_key_cost_object,
                    node_version.name AS node_name
                FROM node 
                INNER JOIN node_version 
                    ON node_version.node_id = node.id
                    AND node_version.version_state_id = :versionStateActive
                WHERE node.id IN (%s)
                UNION ALL 
                SELECT
                    node.id,
                    node_version.sap_key_cost_object,
                    nodes.node_name AS node_name
                FROM node 
                INNER JOIN nodes ON nodes.id = node.parent_node_id
                INNER JOIN node_version 
                    ON node_version.node_id = node.id
                    AND node_version.version_state_id = :versionStateActive
            )
            SELECT
                sap_key_cost_object,
                node_name
            FROM nodes
            WHERE sap_key_cost_object IS NOT NULL
        ', $this->db->quote($startNodeIds));

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue('versionStateActive', self::VERSION_STATE_ACTIVE, PDO::PARAM_INT);
        $stmt->execute();

        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $result = [];
        $divisionMap = [];
        foreach ($data as $row) {
            $divisionName = $this->extractDivisionFromString($row['NODE_NAME']);

            if (in_array($divisionName, $this->baDebtorDivisions, false) === true) {
                if (!isset($divisionMap[$row['NODE_NAME']])) {
                    $divisionNumber = $divisionName;
                } else {
                    $divisionNumber = $divisionMap[$row['NODE_NAME']];
                }

                $result[$divisionNumber][] = $row['SAP_KEY_COST_OBJECT'];
            }
        }

        return $result;
    }

    /**
     * @param string $string
     * @return bool|string
     */
    private function extractDivisionFromString(string $string)
    {
        return substr($string, 0, 2);
    }

    /**
     * @return array
     */
    public function getConfirmationOfExpensesWithoutInvoice(): array
    {
        $sql = "
            WITH employees AS (
                SELECT
                    BA_EMPLOYEE.ID AS employeeId,
                    coalesce(BA_EXTERNAL_EMPLOYEE.FIRST_NAME, EMPLOYEE.FIRST_NAME) AS firstName,
                    coalesce(BA_EXTERNAL_EMPLOYEE.LAST_NAME, EMPLOYEE.LAST_NAME) AS lastName,
                    CASE WHEN BA_EMPLOYEE.INTERNAL_EMPLOYEE_ID IS NOT NULL
                    THEN
                        EMPLOYEE.LAST_NAME || ' ' || EMPLOYEE.FIRST_NAME || ' (' || EMPLOYEE.SHORTCUT || ')'
                    ELSE
                        BA_EXTERNAL_EMPLOYEE.LAST_NAME || ' (' || BA_EXTERNAL_EMPLOYEE.FIRST_NAME || ' ' || BA_EXTERNAL_EMPLOYEE.EMAIL || ')'
                    END employeeDescription
                FROM BA_EMPLOYEE
                LEFT JOIN BA_EXTERNAL_EMPLOYEE
                    ON BA_EMPLOYEE.EXTERNAL_EMPLOYEE_ID = BA_EXTERNAL_EMPLOYEE.ID
                LEFT JOIN EMPLOYEE
                    ON EMPLOYEE.ID = BA_EMPLOYEE.INTERNAL_EMPLOYEE_ID
            ),
            coe AS (
                SELECT *
                FROM BA_CONFIRMATION_OF_EXPENSE
                WHERE date_trunc('month', BA_CONFIRMATION_OF_EXPENSE.month) = date_trunc('month', to_date(:month, 'YYYY-MM'))
                    AND hours <> 0
            ),
            contingents_without_coe AS (
                SELECT
                    *
                FROM BA_CONTINGENT
                WHERE date_trunc('month', BA_CONTINGENT.BEGIN_DATE) <= date_trunc('month', to_date(:month, 'YYYY-MM'))
                    AND date_trunc('month', BA_CONTINGENT.END_DATE) >= date_trunc('month', to_date(:month, 'YYYY-MM'))
                    AND BA_CONTINGENT.ID NOT IN (SELECT DISTINCT contingent_id from coe)
            )
            -- confirmation of expenses without invoice
            SELECT * FROM (
                SELECT
                    division.name AS division_name,
                    employees.employeeDescription AS employee,
                    requisitionNote.REQUISITION_NOTE_NUMBER
                            || ' (Pos: ' || contingent.POSITION || ') '
                            || BA_CONTINGENT_TYPE.NAME || ' ('
                            || employees.firstName || ') ' || employees.lastName
                        AS contingent_name,
                    coe.hours,
                    coe.ready_for_invoicing,
                    ZAHLUNGSPAKET.EFA_ID
                FROM coe
                LEFT JOIN ZAHLUNGSPAKET
                    ON ZAHLUNGSPAKET.CONFIRMATION_OF_EXPENSE_ID = coe.ID
                    AND ZAHLUNGSPAKET.NEXT_ID IS NULL
                JOIN BA_CONTINGENT contingent
                    ON contingent.id = coe.CONTINGENT_ID
                JOIN employees
                    ON employees.employeeId = coe.employee_id
                JOIN BA_REQUISITION_NOTE requisitionNote
                    ON requisitionNote.id = contingent.REQUISITION_NOTE_ID
                JOIN BA_FRAMEWORK_CONTRACT frameworkContract
                    ON frameworkContract.id = requisitionNote.FRAMEWORK_CONTRACT_ID
                JOIN DIVISION
                    ON DIVISION.ID = frameworkContract.DIVISION_ID
                JOIN BA_CONTINGENT_TYPE
                    ON BA_CONTINGENT_TYPE.ID = contingent.TYPE_ID
                WHERE ZAHLUNGSPAKET.FAKTURIERUNGSID IS NULL
    
                UNION ALL
    
                -- contingents without confirmation of expense
                SELECT
                    division.name AS division_name,
                    employees.employeeDescription AS employee,
                    requisitionNote.REQUISITION_NOTE_NUMBER
                            || ' (Pos: ' || contingents_without_coe.POSITION || ') '
                            || BA_CONTINGENT_TYPE.NAME || ' ('
                            || employees.firstName || ' ' || employees.lastName || ')'
                        AS contingent_name,
                    NULL AS hours,
                    NULL AS ready_for_invoicing,
                    NULL AS EFA_ID
                FROM contingents_without_coe
                JOIN employees
                    ON employees.employeeId = contingents_without_coe.employee_id
                JOIN BA_REQUISITION_NOTE requisitionNote
                    ON requisitionNote.id = contingents_without_coe.REQUISITION_NOTE_ID
                JOIN BA_FRAMEWORK_CONTRACT frameworkContract
                    ON frameworkContract.id = requisitionNote.FRAMEWORK_CONTRACT_ID
                JOIN DIVISION
                    ON DIVISION.ID = frameworkContract.DIVISION_ID
                JOIN BA_CONTINGENT_TYPE
                    ON BA_CONTINGENT_TYPE.ID = contingents_without_coe.TYPE_ID
            ) t
            ORDER BY division_name, employee, contingent_name COLLATE \"POSIX\"
            ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue('month', $this->month->format('Y-m'));
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * @return array
     */
    public function getInvoiceTotals(): array
    {
        $sql = "
            WITH contingentManagementTotals AS (
                SELECT
                    -- padding needed due to inconsistent data from SAP
                    lpad(ZAHLUNGSPAKET.FAKTURIERUNGSID, 10, '0') AS invoice_number,
                    ZAHLUNGSPAKET.EFA_ID,
                    DIVISION.NAME AS division_name,
                    sum(
                        round((coe.hours * coalesce(contingent.special_hourly_rate_sale, priceGroupSale.HOURLY_RATE))::DECIMAL, 2)
                    ) AS total
                FROM BA_CONFIRMATION_OF_EXPENSE coe
                JOIN ZAHLUNGSPAKET
                    ON ZAHLUNGSPAKET.CONFIRMATION_OF_EXPENSE_ID = coe.ID
                    AND ZAHLUNGSPAKET.NEXT_ID IS NULL
                    AND ZAHLUNGSPAKET.FAKTURIERUNGSID IS NOT NULL
                JOIN BA_CONTINGENT contingent
                    ON contingent.id = coe.CONTINGENT_ID
                JOIN BA_REQUISITION_NOTE requisitionNote
                    ON requisitionNote.id = contingent.REQUISITION_NOTE_ID
                JOIN BA_FRAMEWORK_CONTRACT frameworkContract
                    ON frameworkContract.id = requisitionNote.FRAMEWORK_CONTRACT_ID
                JOIN DIVISION
                    ON DIVISION.ID = frameworkContract.DIVISION_ID
                JOIN BA_PRICE_GROUP_SALE priceGroupSale
                    ON priceGroupSale.id = contingent.PRICE_GROUP_SALE_ID
                WHERE date_trunc('month', coe.month) = date_trunc('month', to_date(:month, 'YYYY-MM'))
                    AND coe.hours <> 0
                GROUP BY DIVISION.NAME, ZAHLUNGSPAKET.FAKTURIERUNGSID, ZAHLUNGSPAKET.EFA_ID
            ),
            sapTotals AS (
                SELECT
                    DOC_NR AS invoice_number,
                    round(sum(AMOUNT)::DECIMAL, 2) AS total
                FROM COST_AND_REVENUE_PER_POSITION
                WHERE date_trunc('month', month) = date_trunc('month', to_date(:month, 'YYYY-MM'))
                    AND AMOUNT_KIND = :amountKindRevenue
                GROUP BY DOC_NR
            )
            SELECT 
                contingentManagementTotals.division_name,
                contingentManagementTotals.invoice_number,
                contingentManagementTotals.efa_id,
                contingentManagementTotals.total AS kv_total,
                sapTotals.total AS sap_total
            FROM contingentManagementTotals
            LEFT JOIN sapTotals
                ON sapTotals.invoice_number = contingentManagementTotals.invoice_number
            ORDER BY division_name, invoice_number
        ";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue('month', $this->month->format('Y-m'));
        $stmt->bindValue('amountKindRevenue', Sap::AMOUNT_KIND_REVENUE);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
