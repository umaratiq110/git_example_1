<?php
declare(strict_types=1);

class Report_ContingentData
{
    const TYPE_ALL = 1;
    const TYPE_PURCHASE_INVOICE_CHECK = 2;
    const TYPE_CONTRACT_MANAGEMENT = 3;
    /**
     * @var Zend_Db_Adapter_Abstract
     */
    private $db;

    /**
     * Report_ContingentData constructor.
     * @param Zend_Db_Adapter_Abstract $db
     */
    public function __construct(Zend_Db_Adapter_Abstract $db)
    {
        $this->db = $db;
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @return array
     */
    public function getData(DateTime $from, DateTime $to): array
    {
        $sql = " 
            WITH coes AS (
                SELECT
                    *
                FROM BA_CONFIRMATION_OF_EXPENSE coe
                WHERE coe.month BETWEEN date_trunc('month', to_date(:fromDate, 'YYYY-MM-DD')) AND date_trunc('month', to_date(:toDate, 'YYYY-MM-DD'))
            ),
            employees AS (
                SELECT
                    BA_EMPLOYEE.ID AS employeeId,
                    coalesce(BA_EXTERNAL_EMPLOYEE.FIRST_NAME, EMPLOYEE.FIRST_NAME) AS firstName,
                    coalesce(BA_EXTERNAL_EMPLOYEE.LAST_NAME, EMPLOYEE.LAST_NAME) AS lastName,
                    CASE WHEN BA_EMPLOYEE.INTERNAL_EMPLOYEE_ID IS NOT NULL
                    THEN
                        EMPLOYEE.LAST_NAME || ' ' || EMPLOYEE.FIRST_NAME || ' ' || EMPLOYEE.SHORTCUT
                    ELSE
                        BA_EXTERNAL_EMPLOYEE.LAST_NAME || ' ' || BA_EXTERNAL_EMPLOYEE.FIRST_NAME || ' ' || BA_EXTERNAL_EMPLOYEE.EMAIL
                    END employeeDescription,
                    CASE WHEN BA_EMPLOYEE.INTERNAL_EMPLOYEE_ID IS NOT NULL
                    THEN
                        'intern'
                    ELSE
                        'extern'
                    END AS employeeType,
                    BA_EXTERNAL_EMPLOYEE.PARTNER_ID
                FROM BA_EMPLOYEE
                LEFT JOIN BA_EXTERNAL_EMPLOYEE
                    ON BA_EMPLOYEE.EXTERNAL_EMPLOYEE_ID = BA_EXTERNAL_EMPLOYEE.ID
                LEFT JOIN EMPLOYEE
                    ON EMPLOYEE.ID = BA_EMPLOYEE.INTERNAL_EMPLOYEE_ID
            ),
            contingents_without_coe AS (
                SELECT
                    *
                FROM BA_CONTINGENT
                WHERE date_trunc('month', BA_CONTINGENT.BEGIN_DATE) <= date_trunc('month', to_date(:fromDate, 'YYYY-MM-DD'))
                    AND date_trunc('month', BA_CONTINGENT.END_DATE) >= date_trunc('day', to_date(:fromDate, 'YYYY-MM-DD'))
                    AND BA_CONTINGENT.ID NOT IN (SELECT DISTINCT contingent_id from coes)
            )
            SELECT
                v.*
            FROM (
                SELECT
                    employees.lastName || employees.firstName AS employee_name,
                    employees.employeeDescription AS employee_description,
                    employees.employeeType AS employee_type,
                    BA_REQUISITION_NOTE.REQUISITION_NOTE_NUMBER
                        || ' (Pos: ' || BA_CONTINGENT.POSITION || ') '
                        || BA_CONTINGENT_TYPE.NAME || ' '
                        || employees.firstName || ' ' || employees.lastName
                    AS contingent_name,
                    coes.MONTH,
                    coes.HOURS,
                    coalesce(BA_CONTINGENT.SPECIAL_HOURLY_RATE_PURCHASE, BA_PRICE_GROUP_PURCHASE.HOURLY_RATE) AS purchase_price_per_hour,
                    coalesce(BA_CONTINGENT.SPECIAL_HOURLY_RATE_SALE, BA_PRICE_GROUP_SALE.HOURLY_RATE) AS sales_price_per_hour,
                    BA_PRICE_GROUP_SALE.NAME AS price_group_sale_name,
                    BA_PRICE_GROUP_PURCHASE.NAME AS price_group_purchase_name,
                    coes.HOURS * coalesce(BA_CONTINGENT.SPECIAL_HOURLY_RATE_PURCHASE, BA_PRICE_GROUP_PURCHASE.HOURLY_RATE, 0) AS purchase_price,
                    coes.HOURS * coalesce(BA_CONTINGENT.SPECIAL_HOURLY_RATE_SALE, BA_PRICE_GROUP_SALE.HOURLY_RATE, 0) AS sales_price,
                    (coes.HOURS * coalesce(BA_CONTINGENT.SPECIAL_HOURLY_RATE_SALE, BA_PRICE_GROUP_SALE.HOURLY_RATE, 0))
                        - (coes.HOURS * coalesce(BA_CONTINGENT.SPECIAL_HOURLY_RATE_PURCHASE, BA_PRICE_GROUP_PURCHASE.HOURLY_RATE, 0)) AS margin,
                    NODE_VERSION.NAME AS cost_center,
                    BA_PARTNER.NAME AS partner_name,
                    coes.READY_FOR_INVOICING,
                    ZAHLUNGSPAKET.EFA_ID,
                    ZAHLUNGSPAKET.FAKTURIERUNGSID AS invoice_number,
                    BA_FRAMEWORK_CONTRACT.NAME AS framework_contract_name,
                    COMPANY.NAME AS company_name,
                    DIVISION.NAME AS division_name,
                    BA_CUSTOMER.NAME AS customer_name,
                    BA_CONTINGENT.SAP_ORDER,
                    BA_CONTINGENT.SAP_ORDER_POSITION,
                    BA_CONTINGENT.INVOICE_TEXT,
                    BA_CONTINGENT.SUPPLIER_ORDER,
                    to_char(BA_CONTINGENT.BEGIN_DATE, 'DD.MM.YYYY') AS contingent_begin_date,
                    to_char(BA_CONTINGENT.END_DATE, 'DD.MM.YYYY') AS contingent_end_date,
                    BA_CONTINGENT.HOURS AS contingent_hours,
                    ba_invoicing_procedure.name AS invoicing_procedure,
                    coes.credit_voucher
                FROM coes
                LEFT JOIN employees
                    ON employees.employeeId = coes.EMPLOYEE_ID
                LEFT JOIN BA_CONTINGENT
                    ON coes.contingent_id = BA_CONTINGENT.id
                LEFT JOIN BA_REQUISITION_NOTE
                    ON BA_CONTINGENT.requisition_note_id = BA_REQUISITION_NOTE.id
                LEFT JOIN BA_FRAMEWORK_CONTRACT
                    ON BA_REQUISITION_NOTE.framework_contract_id = BA_FRAMEWORK_CONTRACT.id
                LEFT JOIN BA_CUSTOMER
                    ON BA_CUSTOMER.id = BA_FRAMEWORK_CONTRACT.customer_id
                LEFT JOIN BA_CONTINGENT_TYPE
                    ON BA_CONTINGENT_TYPE.ID = BA_CONTINGENT.TYPE_ID
                LEFT JOIN BA_PRICE_GROUP_PURCHASE
                    ON BA_PRICE_GROUP_PURCHASE.ID = BA_CONTINGENT.PRICE_GROUP_PURCHASE_ID
                LEFT JOIN BA_PRICE_GROUP_SALE
                    ON BA_PRICE_GROUP_SALE.ID = BA_CONTINGENT.PRICE_GROUP_SALE_ID
                LEFT JOIN BA_PARTNER
                    ON employees.PARTNER_ID = BA_PARTNER.ID
                LEFT JOIN ba_invoicing_procedure
                    ON ba_partner.invoicing_procedure_id = ba_invoicing_procedure.id
                LEFT JOIN ZAHLUNGSPAKET
                    ON ZAHLUNGSPAKET.CONFIRMATION_OF_EXPENSE_ID = coes.ID
                    AND ZAHLUNGSPAKET.NEXT_ID IS NULL
                LEFT JOIN DIVISION
                    ON DIVISION.ID = BA_FRAMEWORK_CONTRACT.DIVISION_ID
                LEFT JOIN COMPANY
                    ON COMPANY.ID = BA_CUSTOMER.COMPANY_ID
                LEFT JOIN NODE_VERSION
                    ON NODE_VERSION.NODE_ID = BA_CONTINGENT.NODE_ID
                    AND NODE_VERSION.VERSION_STATE_ID = :nodeVersionStateActive
            
                UNION ALL
            
                SELECT
                    employees.lastName || employees.firstName AS employee_name,
                    employees.employeeDescription AS employee_description,
                    employees.employeeType AS employee_type,
                    BA_REQUISITION_NOTE.REQUISITION_NOTE_NUMBER
                        || ' (Pos: ' || contingents_without_coe.POSITION || ') '
                        || BA_CONTINGENT_TYPE.NAME || ' '
                        || employees.firstName || ' ' || employees.lastName
                    AS contingent_name,
                    date_trunc('month', to_date(:fromDate, 'YYYY-MM-DD')),
                    null AS hours,
                    coalesce(contingents_without_coe.SPECIAL_HOURLY_RATE_PURCHASE, BA_PRICE_GROUP_PURCHASE.HOURLY_RATE) AS purchase_price_per_hour,
                    coalesce(contingents_without_coe.SPECIAL_HOURLY_RATE_SALE, BA_PRICE_GROUP_SALE.HOURLY_RATE) AS sales_price_per_hour,
                    BA_PRICE_GROUP_SALE.NAME AS price_group_sale_name,
                    BA_PRICE_GROUP_PURCHASE.NAME AS price_group_purchase_name,
                    null AS purchase_price,
                    null AS sales_price,
                    null AS margin,
                    NODE_VERSION.NAME AS cost_center,
                    BA_PARTNER.NAME AS partner_name,
                    null AS READY_FOR_INVOICING,
                    null AS efa_id,
                    null AS invoice_number,
                    BA_FRAMEWORK_CONTRACT.NAME AS framework_contract_name,
                    COMPANY.NAME AS company_name,
                    DIVISION.NAME AS division_name,
                    BA_CUSTOMER.NAME AS customer_name,
                    contingents_without_coe.SAP_ORDER,
                    contingents_without_coe.SAP_ORDER_POSITION,
                    contingents_without_coe.INVOICE_TEXT,
                    contingents_without_coe.SUPPLIER_ORDER,
                    to_char(contingents_without_coe.BEGIN_DATE, 'DD.MM.YYYY') AS contingent_begin_date,
                    to_char(contingents_without_coe.END_DATE, 'DD.MM.YYYY') AS contingent_end_date,
                    contingents_without_coe.HOURS AS contingent_hours,
                    ba_invoicing_procedure.name AS invoicing_procedure,
                    null AS credit_voucher
                FROM contingents_without_coe
                LEFT JOIN employees
                    ON employees.employeeId = contingents_without_coe.EMPLOYEE_ID
                LEFT JOIN BA_REQUISITION_NOTE
                    ON contingents_without_coe.requisition_note_id = BA_REQUISITION_NOTE.id
                LEFT JOIN BA_FRAMEWORK_CONTRACT
                    ON BA_REQUISITION_NOTE.framework_contract_id = BA_FRAMEWORK_CONTRACT.id
                LEFT JOIN BA_CUSTOMER
                    ON BA_CUSTOMER.id = BA_FRAMEWORK_CONTRACT.customer_id
                LEFT JOIN BA_CONTINGENT_TYPE
                    ON BA_CONTINGENT_TYPE.ID = contingents_without_coe.TYPE_ID
                LEFT JOIN BA_PRICE_GROUP_PURCHASE
                    ON BA_PRICE_GROUP_PURCHASE.ID = contingents_without_coe.PRICE_GROUP_PURCHASE_ID
                LEFT JOIN BA_PRICE_GROUP_SALE
                    ON BA_PRICE_GROUP_SALE.ID = contingents_without_coe.PRICE_GROUP_SALE_ID
                LEFT JOIN BA_PARTNER
                    ON employees.PARTNER_ID = BA_PARTNER.ID
                LEFT JOIN ba_invoicing_procedure
                    ON ba_partner.invoicing_procedure_id = ba_invoicing_procedure.id
                LEFT JOIN DIVISION
                    ON DIVISION.ID = BA_FRAMEWORK_CONTRACT.DIVISION_ID
                LEFT JOIN COMPANY
                    ON COMPANY.ID = BA_CUSTOMER.COMPANY_ID
                LEFT JOIN NODE_VERSION
                    ON NODE_VERSION.NODE_ID = contingents_without_coe.NODE_ID
                    AND NODE_VERSION.VERSION_STATE_ID = :nodeVersionStateActive  
            ) v
            ORDER BY employee_type DESC, employee_name
        ";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':nodeVersionStateActive', 1);
        $stmt->bindValue(':fromDate', $from->format('Y-m-d'));
        $stmt->bindValue(':toDate', $to->format('Y-m-d'));
        $stmt->execute();
        return $stmt->fetchAll();
    }
}
