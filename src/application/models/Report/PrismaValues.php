<?php
declare(strict_types=1);

/**
 * Class Report_PrismaValues
 *
 */
class Report_PrismaValues
{
    /**
     * @var Zend_Db_Adapter_Abstract
     */
    private $db;

    /**
     * @var array
     */
    private $employeeSources;
    /**
     * @var int
     */
    private $profileId;

    /**
     * Report_ContingentData constructor.
     * @param Zend_Db_Adapter_Abstract $db
     * @param array $employeeSources
     * @param int $profileId
     */
    public function __construct(Zend_Db_Adapter_Abstract $db, array $employeeSources, int $profileId)
    {
        $this->db = $db;
        $this->employeeSources = $employeeSources;
        $this->profileId = $profileId;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getData(): array
    {
        /** @var Zend_date $periodFrom */
        Date::determinePeriod($this->profileId, $periodFrom, $periodUntil, $this->db);
        // report only working for today, so we just consider from date
        $dateTime = new DateTime($periodFrom->get(Iso_Date::ORACLE_DATE));

        $companiesWhere = $divisionsWhere = $employeesWhere = null;

        if (!empty($this->employeeSources['companies'])) {
            $companiesWhere = 'ep.company_id IN (' . implode(',', $this->employeeSources['companies']) . ')';
        }

        if (!empty($this->employeeSources['divisions'])) {
            $divisionsWhere = 'efp.division_id IN (' . implode(',', $this->employeeSources['divisions']) . ')';
        }

        if (!empty($this->employeeSources['employees'])) {
            $employeesWhere = QueryBuilder::createInClause('employee.id', $this->employeeSources['employees']);
        }

        $sql = sprintf(/** @lang SQL */"
            WITH employees AS (
                SELECT
                    company.shortcut AS company_shortcut,
                    employee.shortcut AS employee_shortcut,
                    employee.last_name || ', ' || employee.first_name as employee_full_name,
                    employee.id AS employee_id,
                    ep.hours_per_week AS hours_per_week,
                    division.name AS division_name,
                    tah.holidays,
                    employee.location_id
                FROM employment_period ep
                    JOIN employee
                        ON ep.employee_id = employee.id
                    JOIN employee_function_period efp
                        ON efp.employee_id = employee.id
                    JOIN company
                        ON company.id = ep.company_id
                    JOIN division
                        ON division.id = efp.division_id
                    LEFT JOIN tpw_annual_holiday tah
                        ON tah.employee_id = employee.id
                            AND tah.year = date_trunc('year', to_date(:reportDate, :inputDateFormat))
                WHERE to_date(:reportDate, :inputDateFormat) between ep.entry_date AND coalesce(ep.leaving_date, max_date())
                    AND to_date(:reportDate, :inputDateFormat)  between efp.start_date AND coalesce(efp.end_date, max_date())
                    %s
                ORDER BY company_shortcut, employee.shortcut
            ),
            tpw_month_data AS (
                SELECT
                    employee_id,
                    cdate,
                    tpwmonth.cholidayscurmonthremain AS remaining_holidays_cur_month,
                    tpwmonth.cholidaysprevmonthremain AS remaining_holidays_last_month,
                    tpwmonth.chrsprevmonthremain AS remaining_hours_last_month,
                    tpwmonth.chrscurmonthremain AS remaining_hours_cur_month
                FROM employees
                    JOIN tpwmonth
                         ON tpwmonth.cemployee_id = employees.employee_id
                WHERE date_trunc('month', tpwmonth.cdate) = date_trunc('month', to_date( :reportDate, :inputDateFormat))
                   OR date_trunc('month', tpwmonth.cdate) = date_trunc('month', to_date(:reportDate, :inputDateFormat)) - INTERVAL '1' MONTH
            ),
            balance_holidays AS (
                SELECT
                    employees.employee_id,
                    coalesce(
                        last_month.remaining_holidays_cur_month,
                        current_month.remaining_holidays_last_month
                    ) AS balance
                FROM employees
                    LEFT JOIN tpw_month_data current_month
                        ON current_month.employee_id = employees.employee_id
                            AND date_trunc('month', current_month.cdate) = date_trunc('month', to_date(:reportDate, :inputDateFormat))
                    LEFT JOIN tpw_month_data last_month
                        ON last_month.employee_id = employees.employee_id
                            AND date_trunc('month', last_month.cdate) = date_trunc('month', to_date(:reportDate, :inputDateFormat)) - INTERVAL '1' MONTH
            ),
            balance_hours AS (
                SELECT
                    employees.employee_id,
                    coalesce(
                        last_month.remaining_hours_cur_month,
                        current_month.remaining_hours_last_month
                    ) AS balance
                FROM employees
                    LEFT JOIN tpw_month_data current_month
                        ON current_month.employee_id = employees.employee_id
                            AND date_trunc('month', current_month.cdate) = date_trunc('month', to_date(:reportDate, :inputDateFormat))
                    LEFT JOIN tpw_month_data last_month
                        ON last_month.employee_id = employees.employee_id
                            AND date_trunc('month', last_month.cdate) = date_trunc('month', to_date(:reportDate, :inputDateFormat)) - INTERVAL '1' MONTH
            ),
            booked_holidays AS (
                SELECT
                    employee_id,
                    CASE WHEN date_trunc('day', to_date(:reportDate, :inputDateFormat)) = date_trunc('month', to_date(:reportDate, :inputDateFormat))
                        THEN
                        -- first day of month, so we display zero (see spec) although someone might already have booked holiday
                            0
                        ELSE
                            coalesce(count(cid), 0)
                        END AS num
                FROM employees
                    LEFT JOIN tpwworkinghours t
                        ON employees.employee_id = t.cemployee_id
                            AND date_trunc('month', cdate) = date_trunc('month', to_date(:reportDate, :inputDateFormat))
                            AND date_trunc('day', cdate) <= date_trunc('day', to_date(:reportDate, :inputDateFormat) - INTERVAL '1' DAY)
                    LEFT JOIN node
                        ON node.id = t.cnode_id
                    LEFT JOIN node_version
                        ON node_version.node_id = node.id
                            AND node_version.version_state_id = :versionStateActive
                WHERE node_version.is_holiday = 1
                GROUP BY employee_id
            ),
            booked_hours AS (
                SELECT
                    employees.employee_id,
                    CASE WHEN date_trunc('day', to_date(:reportDate, :inputDateFormat)) = date_trunc('month', to_date(:reportDate, :inputDateFormat))
                        THEN
                            -- first day of month, so we display zero (see spec) although someone might already have booked hours
                            0
                        ELSE
                            coalesce(sum(chours), 0) + coalesce(sum(choursal), 0)
                        END AS booked_hours
                FROM employees
                     LEFT JOIN tpwworkinghours
                         ON employee_id = cemployee_id
                             AND date_trunc('month', cdate) = date_trunc('month', to_date(:reportDate, :inputDateFormat))
                             AND date_trunc('day', cdate) <= date_trunc('day', to_date(:reportDate, :inputDateFormat) - INTERVAL '1' DAY)
                GROUP BY employees.employee_id
            ),
            balance_holidays_actual AS (
                SELECT
                    employees.employee_id,
                    -- no negative numbers, see spec
                    greatest(
                        (coalesce(balance_holidays.balance, 0) - coalesce(booked_holidays.num, 0)),
                        0
                    ) AS balance
                FROM employees
                    LEFT JOIN balance_holidays
                        ON balance_holidays.employee_id = employees.employee_id
                    LEFT JOIN booked_holidays
                        ON booked_holidays.employee_id = employees.employee_id
            ),
            work_days AS (
                SELECT
                    employee_id,
                    CASE WHEN date_trunc('day', to_date(:reportDate, :inputDateFormat)) = date_trunc('month', to_date(:reportDate, :inputDateFormat))
                        THEN
                            -- first day of month, so we display zero (see spec) although someone might already have booked hours
                            0
                        ELSE
                            greatest (
                                pack_overtime_holiday.getWorkDays(
                                    employees.employee_id,
                                    (date_trunc('month', to_date(:reportDate, :inputDateFormat)))::timestamp,
                                    (to_date(:reportDate, :inputDateFormat) - interval '1' day)::timestamp
                                ),
                                0
                            )
                        END AS work_days
                FROM employees
            ),
            work_days_dates AS (
                WITH RECURSIVE days AS (
                    SELECT
                        (date_trunc('month', to_date(:reportDate, :inputDateFormat)))::date AS day_date
                    UNION ALL
                    SELECT
                        (day_date + INTERVAL '1' DAY)::date AS day_date
                    FROM days
                    WHERE days.day_date < to_date(:reportDate, :inputDateFormat) - INTERVAL '1' DAY
                )
                select
                    day_date
                FROM days
                WHERE EXTRACT(ISODOW FROM day_date) NOT IN (6,7)
            ),
            balance_hours_actual AS (
                SELECT
                    employees.employee_id,
                    CASE WHEN balance_hours.balance IS NOT NULL THEN
                        coalesce(balance_hours.balance, 0) + booked_hours.booked_hours - (work_days.work_days * (hours_per_week/5))
                    END AS balance
                FROM employees
                    JOIN booked_hours
                        ON booked_hours.employee_id = employees.employee_id
                    JOIN work_days
                        ON work_days.employee_id = employees.employee_id
                    JOIN balance_hours
                        ON balance_hours.employee_id = employees.employee_id
            ),
            work_days_without_booking AS (
                SELECT
                    employee_id,
                    CASE WHEN date_trunc('day', to_date(:reportDate, :inputDateFormat)) = date_trunc('month', to_date(:reportDate, :inputDateFormat))
                        THEN
                            -- first day of month, so we display zero (see spec) although someone might already have booked hours
                            0
                        ELSE
                            count(day_date)
                        END AS amount
                FROM work_days_dates
                    CROSS JOIN employees
                    LEFT JOIN tpwpublicholidays holidays
                        ON holidays.clocation_id = employees.location_id
                            AND holidays.cdate = date_trunc('day', day_date)
                WHERE NOT EXISTS (
                    SELECT NULL
                    FROM tpwworkinghours
                    WHERE cdate = date_trunc('day', day_date)
                        AND cemployee_id = employees.employee_id
                    )
                    AND holidays.cid IS NULL
                GROUP BY employee_id
            )
            SELECT
                employees.employee_id,
                employees.company_shortcut,
                employees.employee_shortcut,
                employees.employee_full_name,
                employees.division_name,
                CASE WHEN balance_holidays.balance IS NULL THEN
                    'Keine Prisma-Daten da für diesen und letzten Monat'
                ELSE 
                    balance_holidays.balance::varchar
                END AS balance_holidays_last_month,
                coalesce(booked_holidays.num, 0) AS booked_holidays_cur_month,
                CASE WHEN balance_holidays.balance IS NULL THEN
                    'Keine Prisma-Daten da für diesen und letzten Monat'
                ELSE
                    balance_holidays_actual.balance::varchar
                END AS balance_holidays_actual,
                employees.holidays AS annual_holiday,
                CASE WHEN balance_holidays.balance IS NULL THEN
                    'Keine Prisma-Daten da für diesen und letzten Monat'
                ELSE
                    (greatest((balance_holidays_actual.balance - employees.holidays), 0))::varchar
                END AS balance_holidays_last_year,
                CASE WHEN balance_hours.balance IS NULL THEN
                    'Keine Prisma-Daten da für diesen und letzten Monat'
                ELSE
                    to_char(balance_hours.balance, :decimalFormat)
                END AS balance_hours_last_month,
                work_days.work_days,
                coalesce(work_days_without_booking.amount, 0) AS work_days_without_booking,
                to_char(booked_hours.booked_hours, :decimalFormat) AS booked_hours,
                to_char(employees.hours_per_week, :decimalFormat) AS hours_per_week,
                CASE WHEN balance_hours_actual.balance IS NULL THEN
                    'Keine Prisma-Daten da für diesen und letzten Monat'
                ELSE
                    to_char(balance_hours_actual.balance, :decimalFormat)
                END AS balance_hours_actual
            FROM employees
                LEFT JOIN balance_holidays
                    ON balance_holidays.employee_id = employees.employee_id
                LEFT JOIN booked_holidays
                    ON booked_holidays.employee_id = employees.employee_id
                LEFT JOIN balance_holidays_actual
                    ON balance_holidays_actual.employee_id = employees.employee_id
                LEFT JOIN balance_hours
                    ON balance_hours.employee_id = employees.employee_id
                LEFT JOIN booked_hours
                    ON booked_hours.employee_id = employees.employee_id
                LEFT JOIN work_days
                    ON work_days.employee_id = employees.employee_id
                LEFT JOIN balance_hours_actual
                    ON balance_hours_actual.employee_id = employees.employee_id
                LEFT JOIN work_days_without_booking
                    ON work_days_without_booking.employee_id = employees.employee_id
            ORDER BY company_shortcut, employee_shortcut",
            QueryBuilder::conditionOr('AND', [$companiesWhere, $divisionsWhere, $employeesWhere])
        );

        try {
            # need to set search_path to include intranet, otherwise the packages could fail with permission error
            $this->db->query('SET search_path TO intranet,projektdb,public');

            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(':reportDate', $dateTime->format('Y-m-d'));
            $stmt->bindValue(':decimalFormat', 'fm999990.00');
            $stmt->bindValue(':inputDateFormat', 'YYYY-MM-DD');
            $stmt->bindValue(':versionStateActive', 1);
            $stmt->execute();

            return $stmt->fetchAll();
        } finally {
            $this->db->query('SET search_path TO DEFAULT');
        }
    }
}
