<?php
class MaXProjekt extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    /**
     * Table name
     * @var string
     */
    protected $_name = 'MAXPROJEKT';

       /**
        * Primary key
        * @var string
        */
    protected $_primary = 'ID';
    // @codingStandardsIgnoreEnd
}
