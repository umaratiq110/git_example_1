<?php
/**
 * GenerationLog
 */
class GenerationLog
{
    const REPORT_GENERATION_TYPE_MANUAL = 0;
    const REPORT_GENERATION_TYPE_CYCLIC = 1;

    /**
     * Insert an row to report generation log table
     *
     * @return boolean
     */
    public static function insertGenerationEntry($reportType, $mitarbeiterId, $generationType)
    {
        $db = Zend_Registry::get('db');
        $sql = "
                INSERT INTO BC_CHART_PROFIL_GENERATION_LOG (generation_date, report_type_id, mitarbeiter_id, generation_type) 
                VALUES (localtimestamp, :REPORT_TYPE, :MITARBEITER_ID, :GENERATION_TYPE)
        ";

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':REPORT_TYPE', $reportType);
        $stmt->bindParam(':MITARBEITER_ID', $mitarbeiterId);
        $stmt->bindParam(':GENERATION_TYPE', $generationType);

        return $stmt->execute();
    }
}
