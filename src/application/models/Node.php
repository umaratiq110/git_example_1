<?php
/**
 * Node model
 */
class Node extends Iso_Model_IsoModel
{
    const DUMMY = -1;

    // Ignored because used in Zend class
	// @codingStandardsIgnoreStart
    protected static $_dateFormat = 'dd.MM.YYYY';

    protected $_name = 'NODE';
    protected $_primary = 'ID';
    protected $_translate;
	// @codingStandardsIgnoreEnd

    /**
     * Model constructor
     *
     * @param Zend_Translate|Zend_View_Helper_Translate $translate
     */
    public function __construct($translate)
    {
        parent::__construct();

        $this->_db = Zend_Registry::get('db');
        $this->_translate = $translate;
    }

    /**
     * Get the parent tree of a node
     *
     * @param int $empId Employee id
     * @param int $startId Node to start at
     * @param int $endId Node to stop at
     * @return array
     */
    public function getTree($empId, $startId = null, $endId = null)
    {
        $startIdColumn = 'node.id';
        if ($startId !== null) {
            $startIdCompareClause = "= :startId";
        } else {
            $startIdColumn = 'node.parent_node_id';
            $startIdCompareClause = "IS null";
        }
        $whereClause = sprintf('WHERE %s %s', $startIdColumn, $startIdCompareClause);

        $sql = /** @lang SQL */"
            WITH nodes AS (
                WITH RECURSIVE tree AS (
                    SELECT 
                        node.id,
                        ARRAY[lower(node_version.name)] AS sort_order,
                        node_version.id AS node_version_id
                    FROM node
                    INNER JOIN node_version
                        ON node_version.node_id = node.id
                        AND node_version.version_state_id = 1
                    " . $whereClause . "
                    UNION ALL 
                    SELECT 
                        node.id,
                        tree.sort_order || lower(node_version.name) AS sort_order,
                        node_version.id AS node_version_id
                    FROM node 
                    INNER JOIN node_version
                        ON node_version.node_id = node.id
                        AND node_version.version_state_id = 1
                    INNER JOIN tree ON tree.id = node.parent_node_id
                )
                SELECT * FROM tree
            ),
            node_permissions AS (
                WITH RECURSIVE nodes_granted AS (
                    -- nodes the employee has permission for
                    SELECT
                        node.id
                    FROM node
                    WHERE id IN (
                        SELECT node_id from employee_node_permission WHERE employee_id = :employeeId
                    )
                    UNION ALL
                    SELECT
                        node.id
                    FROM node
                             INNER JOIN nodes_granted ON nodes_granted.id = node.parent_node_id
                ),
                nodes_not_granted AS (
                    -- nodes the employee has no permission for
                   SELECT
                       node.id,
                       node.parent_node_id,
                       true as ignore
                   FROM node
                   WHERE id IN (
                       SELECT node_id from employee_node_permission WHERE employee_id = :employeeId
                   )
                   UNION ALL
                   SELECT
                       node.id,
                       node.parent_node_id,
                       false AS ignore
                   FROM node
                            INNER JOIN nodes_not_granted ON nodes_not_granted.parent_node_id = node.id
                )
                SELECT id, 1 AS selectable FROM nodes_granted
                UNION
                SELECT id, 0 AS selectable from nodes_not_granted WHERE ignore = false
            )
    
            SELECT
                node.id,
                node.parent_node_id,
                node.node_type_id,
                node_version.structure_unit_type_id,
                node.bookable,
                (CASE
                    WHEN node_version.bookable_from IS null OR node_version.bookable_from <= current_date THEN 0
                    ELSE 1
                END) AS not_yet_bookable,
                node_version.sap_key_cost_center,
                node_version.sap_key_cost_object,
                node_version.debtor_number,
                node_version.target_effort,
                node_version.is_holiday,
                node.is_leaf,
                (CASE
                    WHEN node_version.long_name IS NOT null THEN node_version.name || ' [' || node_version.long_name || ']'
                    WHEN node_version.customer_name IS NOT null THEN node_version.name || ' [' || node_version.customer_name || ']'
                    ELSE node_version.name
                END) AS name,
                node_type.booking_allowed,
                booking_type.id AS booking_type_id,
                booking_type.name AS booking_type_name,
                booking_type.long_name_" . $this->_translate->getLocale() . " AS booking_type_long_name,
                node_permissions.selectable
            FROM nodes
            INNER JOIN node ON nodes.id = node.id
            INNER JOIN node_version
                ON node_version.id = nodes.node_version_id
            INNER JOIN node_type ON node_type.id = node.node_type_id
            INNER JOIN node_permissions ON node_permissions.id = node.id
            LEFT JOIN booking_type ON booking_type.id = node_version.booking_type_id
        ";

        if (null !== $endId) {
            $sql .= "
                WHERE node.parent_node_id IN (
                    WITH RECURSIVE parents AS (
                        SELECT
                            node.id,
                            parent_node_id,
                            1 AS level
                        FROM node
                        WHERE node.id = :endId
                        UNION ALL
                        SELECT
                            node.id,
                            node.parent_node_id,
                            parents.level + 1 AS level
                        FROM node
                                 INNER JOIN parents ON parents.parent_node_id = node.id
                    )
                    SELECT id FROM parents WHERE level > 1
                )
                -- special case root node
                OR node.parent_node_id IS NULL
            ";
        } else {
            $sql .= "
                WHERE parent_node_id $startIdCompareClause
            ";
        }
        $sql .= "
            ORDER BY nodes.sort_order
        ";

        $stmt = $this->getAdapter()->prepare($sql);
        $stmt->bindValue(":employeeId", $empId, PDO::PARAM_INT);
        if ($startId !== null) {
            $stmt->bindValue(":startId", $startId, PDO::PARAM_INT);
        }
        if (null !== $endId) {
            $stmt->bindValue(":endId", $endId, PDO::PARAM_INT);
        }
        $stmt->execute();

        $lookup = array();
        $result = [];
        while (false !== ($row = $stmt->fetch())) {
            $nodeData =  array(
                // ExtJS specific properties
                'uid'                   => 'n' . $row['ID'], // add prefix to guarantee unique ids for nodes AND sets
                'text' => $row['NAME'] . ($row['NOT_YET_BOOKABLE'] == 1 ? ' -- (' . $this->_translate->translate('notYetBookableSuffix') . ')' : ''),
                'leaf'                  => $row['IS_LEAF'] == 1,
                'iconCls'               => self::getIconCls((int)$row['NODE_TYPE_ID']),
                'cls' => self::getCls((int)$row['NODE_TYPE_ID'], (int)$row['STRUCTURE_UNIT_TYPE_ID']),
                'checked'               => $row['SELECTABLE'] == 1 ? false : null,
                // other properties
                'id'                    => $row['ID'],
                'name'                  => $row['NAME'],
                'nodeTypeId'            => $row['NODE_TYPE_ID'],
                'nodeType'              => null,
                'structureUnitTypeId'   => $row['STRUCTURE_UNIT_TYPE_ID'],
                'bookingTypeId'         => $row['BOOKING_TYPE_ID'],
                'bookingType'           => null,
                'locked'                => $row['BOOKABLE'] == 0,
                'sapKeyCostCenter'      => $row['SELECTABLE'] ? $row['SAP_KEY_COST_CENTER'] : null,
                'sapKeyCostUnit'        => $row['SELECTABLE'] ? $row['SAP_KEY_COST_OBJECT'] : null,
                'debtorNumber'         => $row['SELECTABLE'] ? $row['DEBTOR_NUMBER'] : null,
                // Either single cc or target cc value
                'bookingAllowed'        => $row['BOOKING_ALLOWED'],
                'targetEffort'          => $row['TARGET_EFFORT'],
                'isHoliday'             => $row['IS_HOLIDAY'],
            );

            if (null !== $row['BOOKING_TYPE_ID']) {
                $data['bookingType'] = array(
                    'ID'        => $row['BOOKING_TYPE_ID'],
                    'NAME'      => $row['BOOKING_TYPE_NAME'],
                    'LONG_NAME' => $row['BOOKING_TYPE_LONG_NAME']
                );
            }

            if ($row['PARENT_NODE_ID'] !== null && isset($lookup[$row['PARENT_NODE_ID']])) {
                if (!isset($lookup[$row['PARENT_NODE_ID']]['children'])) {
                    $lookup[$row['PARENT_NODE_ID']]['children'] = array();
                }
                $i = array_push($lookup[$row['PARENT_NODE_ID']]['children'], $nodeData);
                $lookup[$row['ID']] = &$lookup[$row['PARENT_NODE_ID']]['children'][$i - 1];
            } else {
                $i = array_push($result, $nodeData);
                $lookup[$row['ID']] = &$result[$i - 1];
            }
        }
        return $result;
    }

    /**
     * Search for a node
     *
     * @param int $empId Employee id
     * @param string $searchPhrase Search phrase
     * @return array
     */
    public function search($empId, $criteria, $searchPhrase)
    {
        if (!is_array($criteria)) {
            $criteria = array($criteria);
        }
        $searchConditions = array();
        foreach ($criteria as $c) {
            switch ($c) {
                case 'debtorNumber':
                    $searchConditions[] = "node_version.debtor_number = :searchPhrase";
                    break;
                case 'sapKeyCostCenter':
                    $searchConditions[] = "node_version.sap_key_cost_center = :searchPhrase";
                    break;
                case 'sapKeyCostUnit':
                    $searchConditions[] = "node_version.sap_key_cost_object = :searchPhrase";
                    break;
                default:
                    $searchConditions[] = "
                        lower(
                            CASE
                                WHEN node_version.long_name IS NOT null THEN node_version.name || ' [' || node_version.long_name || ']'
                                WHEN node_version.customer_name IS NOT null THEN node_version.name || ' [' || node_version.customer_name || ']'
                                ELSE node_version.name
                            END
                        ) LIKE '%'||lower(:searchPhrase)||'%'
                    ";
            }
        }

        $sql = /** @lang SQL */"
            WITH RECURSIVE nodes AS (
                SELECT
                    node.id,
                    node.id::varchar AS path
                FROM node
                WHERE parent_node_id IS NULL
                UNION ALL
                SELECT
                    node.id,
                    nodes.path || '/' || node.id AS path
                FROM node
                         INNER JOIN nodes ON nodes.id = node.parent_node_id
            ),
            permitted_node AS (
                WITH RECURSIVE node_permissions AS (
                    -- all nodes the user has permission for (including all children)
                    SELECT
                        node.id,
                        ARRAY[lower(name)] AS sort_order,
                        node_version.id AS node_version_id
                    FROM node
                    INNER JOIN intranet.node_version
                        ON node.id = node_version.node_id
                            AND node_version.version_state_id = 1
                    WHERE node.id IN (
                        SELECT node_id from employee_node_permission WHERE employee_id = :employeeId
                    )
                    UNION ALL
                    SELECT
                        node.id,
                        node_permissions.sort_order || lower(node_version.name) AS sort_order,
                        node_version.id AS node_version_id
                    FROM node
                    INNER JOIN intranet.node_version
                        ON node.id = node_version.node_id
                            AND node_version.version_state_id = 1
                    INNER JOIN node_permissions ON node_permissions.id = node.parent_node_id
                )
                SELECT
                    nodes.id,
                    nodes.path,
                    node_permissions.sort_order,
                    node_permissions.node_version_id
                FROM nodes
                INNER JOIN node_permissions ON nodes.id = node_permissions.id
            )
            select
                permitted_node.id,
                path
            from permitted_node
            INNER JOIN node_version ON node_version_id = node_version.id
            WHERE " . implode(' OR ', $searchConditions) . "
            ORDER BY sort_order
        ";
        $stmt = $this->getAdapter()->prepare($sql);
        $stmt->bindParam(":employeeId", $empId);
        $stmt->bindValue(':searchPhrase', $searchPhrase);
        $stmt->execute();

        $result = $stmt->fetchAll();
        return $result;
    }

    /**
     * Return the path from a node to the root node
     *
     * @param int $nodeId
     * @return array
     */
    public function getPath($nodeId)
    {
        $sql = /** @lang SQL */"
            WITH RECURSIVE nodes AS (
                SELECT
                    node.id,
                    node.parent_node_id,
                    1 AS level
                FROM node
                WHERE node.id = :nodeId
                UNION ALL
                SELECT
                    node.id,
                    node.parent_node_id,
                    nodes.level + 1 AS level
                FROM node
                         INNER JOIN nodes ON nodes.parent_node_id = node.id
            )
            SELECT
                node_version.node_type_id,
                CASE
                    WHEN node_version.long_name IS NOT null THEN node_version.name || ' [' || node_version.long_name || ']'
                    WHEN node_version.customer_name IS NOT null THEN node_version.name || ' [' || node_version.customer_name || ']'
                    ELSE node_version.name
                END AS name
            FROM nodes
            INNER JOIN node_version
                ON node_version.node_id = nodes.id
                    AND node_version.version_state_id = 1
            ORDER BY nodes.level DESC
        ";

        $stmt = $this->getAdapter()->prepare($sql);
        $stmt->bindParam(":nodeId", $nodeId);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $resultArr = array();
        foreach ($result as $item) {
            $resultArr[] = array(
                'text'      => $item['NAME'],
                'iconCls'   => Node::getIconCls((int)$item['NODE_TYPE_ID'])
            );
        }

        return $resultArr;
    }

    /**
     * @return array
     */
    public function getCostcentres()
    {
        $sql = "
            SELECT
                node.id,
                CASE
                    WHEN node_version.long_name IS NOT null THEN node_version.name || ' [' || node_version.long_name || ']'
                    WHEN node_version.customer_name IS NOT null THEN node_version.name || ' [' || node_version.customer_name || ']'
                    ELSE node_version.name
                END AS name,
                to_char(node_version.lock_date,'DD.MM.YYYY') AS lock_date,
                node.bookable,
                CASE WHEN node_version.bookable_from IS null OR node_version.bookable_from <= current_date THEN 0 ELSE 1 END AS
                not_yet_bookable,
                node_version.booking_type_id
            FROM node
            INNER JOIN node_version
                ON node_version.node_id = node.id
                AND node_version.version_state_id = 1
            INNER JOIN node_type ON node_type.id = node.node_type_id
            WHERE node.is_leaf = 1
            AND node_version.is_holiday = 0
            AND node_type.booking_allowed = 1
            ORDER BY name
        ";

        $stmt = $this->getAdapter()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * @param int $employeeId
     * @return array
     */
    public function getEmployeeCostcentres($employeeId)
    {
        $sql = /** @lang SQL */"
            WITH RECURSIVE nodes AS (
                SELECT
                    node.id
                FROM node
                WHERE node.id IN (
                    SELECT employee_node_permission.node_id
                    FROM employee_node_permission
                    WHERE employee_id = :employeeId
                )
                UNION ALL
                SELECT node.id
                FROM node
                         INNER JOIN nodes ON nodes.id = node.parent_node_id
            )
            SELECT
                node.id,
                CASE
                    WHEN node_version.long_name IS NOT null THEN node_version.name || ' [' || node_version.long_name || ']'
                    WHEN node_version.customer_name IS NOT null THEN node_version.name || ' [' || node_version.customer_name || ']'
                    ELSE node_version.name
                    END AS name,
                CASE WHEN lock_date < current_date THEN 1 ELSE 0 END AS locked,
                to_char(node_version.lock_date,'DD.MM.YYYY') AS lock_date,
                node.bookable,
                CASE WHEN node_version.bookable_from IS null OR node_version.bookable_from <= current_date THEN 0 ELSE 1 END AS
                    not_yet_bookable,
                node_version.booking_type_id,
                to_char(node_accounting.last_accounting_date,'DD.MM.YYYY') as last_accounting_date
            FROM nodes
            INNER JOIN node ON node.id = nodes.id
            INNER JOIN node_version
                    ON node_version.node_id = nodes.id
                        AND node_version.version_state_id = 1
            INNER JOIN node_type ON node_type.id = node.node_type_id
            LEFT JOIN (
                SELECT node_id,
                       max(root_node_accounting_period_to) AS last_accounting_date
                FROM (
                         WITH RECURSIVE nodes_accounted AS (
                             SELECT node.id                       AS node_id,
                                    max_date.accounting_period_to AS root_node_accounting_period_to
                             FROM node
                                      LEFT JOIN (
                                 SELECT node_id,
                                        max(accounting_period_to) AS accounting_period_to
                                 FROM zahlungspaket
                                          LEFT JOIN zahlungspaketstatus
                                                    ON zahlungspaketstatus.id = zahlungspaket.zahlungspaketstatus_id
                                 WHERE zahlungspaketstatus.status_kuerzel = 'FT'
                                   AND zahlungspaket.next_id IS NULL
                                   AND accounting_period_to IS NOT NULL
                                 GROUP BY node_id
                             ) max_date ON max_date.node_id = node.id
                             WHERE max_date.accounting_period_to IS NOT NULL
            
                             UNION ALL
            
                             SELECT node.id AS node_id,
                                    nodes_accounted.root_node_accounting_period_to
                             FROM node
                                      INNER JOIN nodes_accounted ON nodes_accounted.node_id = node.parent_node_id
                         )
                         SELECT *
                         FROM nodes_accounted
                     ) t
                GROUP BY node_id
            ) node_accounting
            ON node.id = node_accounting.node_id
            WHERE node.is_leaf = 1
              AND node_version.is_holiday = 0
              AND node_type.booking_allowed = 1
                  ORDER BY name
        ";

        $stmt = $this->getAdapter()->prepare($sql);
        $stmt->bindParam(":employeeId", $employeeId);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Determine icon of a node
     *
     * @param int $nodeType
     * @return string
     */
    public static function getIconCls($nodeType)
    {
        $cls = 'folder';
        switch ($nodeType) {
            case NodeType::DEBTOR:
                $cls = 'star';
                break;
            case NodeType::ORDER:
                $cls = 'page-white-stack';
                break;
            case NodeType::UNIT_OF_ORDER:
                $cls = 'page-white-text';
                break;
            case NodeType::ACTIVITY:
                $cls = 'page-white-edit';
                break;
            case NodeType::SAP_BOOKING:
                $cls = 'sap';
                break;
        }
        return $cls;
    }

    /**
     * Determine class of a node
     *
     * @param int $nodeType
     * @param int $structureUnitType
     * @return string
     */
    public static function getCls($nodeType, $structureUnitType)
    {
        $cls = '';

        switch ($nodeType) {
            case NodeType::STRUCTURE_UNIT:
                switch ($structureUnitType) {
                    case StructureUnitType::COMPANY:
                        $cls = 'nt-su-sut-company';
                        break;
                    case StructureUnitType::DIVISION:
                        $cls = 'nt-su-sut-division';
                        break;
                }
                break;
            case NodeType::DEBTOR:
                $cls = 'nt-debtor';
                break;
        }

        return $cls;
    }
}
