<?php
class IsoPMToolInfo extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    /**
     * Table name
     * @var string
     */
    protected $_name = 'ISOPMTOOL_INFO';

       /**
        * Primary key
        * @var string
        */
    protected $_primary = 'ID';
    // @codingStandardsIgnoreEnd
    
    
    public function getInfos($tool, $mitarbeiter_id)
    {
        // Info nur anzeigen wenn sich der User ein oder kein Mal eingeloggt hat
        $stmt = $this->_db->prepare(
            "SELECT isopmtool_info.text_de, isopmtool_info.text_en, produkt_kennung.kennung,
            to_char(installation.inst_datum, 'dd.mm.yyyy') as inst_datum, isopmtool_info.releasenotes_datei_de,
            isopmtool_info.releasenotes_datei_en " .
            "FROM isopmtool_info INNER JOIN isopmtool ON isopmtool_info.isopmtool_id = isopmtool.id " .
            "LEFT OUTER JOIN produkt_kennung ON produkt_kennung.id = isopmtool_info.produktkennung_id " .
            "LEFT OUTER JOIN installation ON produkt_kennung.id = installation.id_prodkenn AND isopmtool.projekt_id =
            installation.id_projekt " .
            "WHERE isopmtool.name = :NAME AND " .
            //Da die Zeiträume jetzt minutengenau sind fallen die Tagesvergleiche weg
            "(current_date BETWEEN isopmtool_info.zeitraum_von AND isopmtool_info.zeitraum_bis ) " .
            "and 2 >( " .
            "SELECT count(Login.id) from Login " .
            "where Login.timestamp > isopmtool_info.zeitraum_von AND isopmtool.id = login.isopmtool_id AND
            login.mitarbeiter_id = :MA_ID) "
        );
        
        $stmt->bindValue(':NAME', $tool);
        $stmt->bindValue(':MA_ID', $mitarbeiter_id);
                
        if ($stmt->execute() && $row = $stmt->fetch()) {
            return $row;
        } else {
            return null;
        }
    }
    
    
    public function getCurrentRelease($tool)
    {
        $stmt = $this->_db->prepare(
            'SELECT  to_char(Inst_Datum,\'dd.mm.yyyy\') as Installationsdatum, Kennung,Produkt_Kennung.Prodk_Kommentar
            AS KOMMENTAR
            from Kundensystem, Installation, Produkt_Kennung
            where Installation.ID_Projekt = (select PROJEKT_ID from ISOPMTOOL where Name = :TOOL1)
            and Kundensystem.Typ = 1
            and Kundensystem.ID_Projekt = Installation.ID_Projekt
            and Installation.ID_Prodkenn = Produkt_Kennung.ID
            and Installation.ID_Kdsystem = Kundensystem.ID
            and Kundensystem.Zustand = 1 and to_char(Inst_Datum,\'dd.mm.yyyy\') =
            (select max(Inst_Datum)
            from Kundensystem, Installation
            where Installation.ID_Projekt = Kundensystem.ID_Projekt
            and Kundensystem.Typ = 1
            and Kundensystem.ID_Projekt = (select PROJEKT_ID from ISOPMTOOL where Name = :TOOL2) and
            Installation.ID_Kdsystem = Kundensystem.ID
            and Kundensystem.Zustand = 1)'
        );
            
        $stmt->bindValue(':TOOL1', $tool);
        $stmt->bindValue(':TOOL2', $tool);
        
        if ($stmt->execute() && $row = $stmt->fetch()) {
            return $row;
        } else {
            return null;
        }
    }
}
