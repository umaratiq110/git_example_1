<?php
declare(strict_types=1);

class Authentication_LdapLogin
{
    private $ldapOptions;
    /**
     * @var Zend_Db_Adapter_Abstract
     */
    private $dbAdapter;

    /**
     * Authentication_LdapLogin constructor.
     */
    public function __construct()
    {
        /** @var Zend_Application_Bootstrap_Bootstrap $application */
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $this->ldapOptions = $bootstrap->getResource('config')->ldap->params->toArray();
        $this->dbAdapter = $bootstrap->getResource('db');
    }

    /**
     * @param string $username
     * @param string $password
     * @return Zend_Auth_Result
     * @throws Authentication_Exception_AuthenticationFailedException
     */
    public function authenticate(string $username, string $password): Zend_Auth_Result
    {
        $authAdapter = new Iso_Auth_Adapter_LdapDb($this->ldapOptions, $username, $password);
        $authAdapter->setDb($this->dbAdapter)
            ->setTableName('MITARBEITER')
            ->setIdentityColumn('KUERZEL');

        $auth = Zend_Auth::getInstance();

        $result = $auth->authenticate($authAdapter);

        if (!$result->isValid()) {
            $e = new Authentication_Exception_AuthenticationFailedException(sprintf('authentication for user: %s failed', $username));
            $e->setDetails($result->getMessages());
            throw $e;
        }
        return $result;
    }
}
