<?php
declare(strict_types=1);

/**
 * Logs in user by http headers; REMOTE_USER or X_FORWARDED_USER
 * Class HttpLogin
 */
class Authentication_HttpLogin
{
    /**
     * @return Zend_Auth_Result
     * @throws Authentication_Exception_AuthenticationFailedException
     * @throws Authentication_Exception_UsernameMissingException
     */
    public function authenticate(): Zend_Auth_Result
    {
        $username = self::getUsernameFromHttpHeaders();

        if ($username === null) {
            throw new Authentication_Exception_UsernameMissingException('no username found in http-headers');
        }
        $authAdapter = new Iso_Auth_Adapter_Normal($username);

        $auth = Zend_Auth::getInstance();

        $result = $auth->authenticate($authAdapter);

        if (!$result->isValid()) {
            throw new Authentication_Exception_AuthenticationFailedException(sprintf('authentication for user: %s failed', $username));
        }
        return $result;
    }

    /**
     * @return string|null
     */
    public static function getUsernameFromHttpHeaders()
    {
        # check for empty needed as it might be null or empty string, depending on the web-server
        if (!empty($_SERVER['REMOTE_USER'])) {
            return $_SERVER['REMOTE_USER'];
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED_USER'])) {
            return $_SERVER['HTTP_X_FORWARDED_USER'];
        }
        return null;
    }
}
