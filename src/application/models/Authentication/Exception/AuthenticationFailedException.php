<?php
declare(strict_types=1);

class Authentication_Exception_AuthenticationFailedException extends Exception
{
    private $details = [];

    /**
     * @return array
     */
    public function getDetails(): array
    {
        return $this->details;
    }

    /**
     * @param array $details
     */
    public function setDetails(array $details)
    {
        $this->details = $details;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s - %s', $this->getMessage(), implode('--', $this->getDetails()));
    }
}
