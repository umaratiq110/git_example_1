<?php
class ChartProfileBookingType extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'BC_CHART_PROFIL_X_BT';
    // @codingStandardsIgnoreEnd
    
    public function getBookingTypes($profileId)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, 'BOOKING_TYPE_ID')
            ->where('BC_CHART_PROFIL_ID = ?', $profileId);
        
        $result = array();
        $queryResult = $select->query()->fetchAll();
        foreach ($queryResult as $item) {
            array_push($result, (int)$item['BOOKING_TYPE_ID']);
        }
        
        return $result;
    }

    public function getBookingTypesShortcuts($profileId)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, array())
            ->join('BOOKING_TYPE', 'BOOKING_TYPE.id = BC_CHART_PROFIL_X_BT.BOOKING_TYPE_ID', array('name'))
            ->where('BC_CHART_PROFIL_ID = ?', $profileId)
            ->order(array('sort_order', 'name'));

        $result = array();
        $queryResult = $select->query()->fetchAll();
        foreach ($queryResult as $item) {
            array_push($result, $item['NAME']);
        }

        return $result;
    }

    public function setBookingTypes($profileId, $bt = array())
    {
        $this->getAdapter()->beginTransaction();
        try {
            // Remove all nodes for a profil
            $this->getAdapter()->delete($this->_name, $this->getAdapter()
                ->quoteInto('BC_CHART_PROFIL_ID = ?', $profileId));
                            
            //Add nodes to a profile
            foreach ($bt as $item) {
                $data = array(
                    'BC_CHART_PROFIL_ID' => $profileId,
                    'BOOKING_TYPE_ID' => (int)$item
                );
                
                $this->getAdapter()->insert($this->_name, $data);
            }
        } catch (Exception $e) {
            $this->getAdapter()->rollback();
            return false;
        }
        $this->getAdapter()->commit();
        return true;
    }

    public function copy($srcProfileId, $destProfileId)
    {
        $this->getAdapter()->beginTransaction();
        try {
            // Remove all nodes for a profil
            $this->getAdapter()->delete($this->_name, $this->getAdapter()
                ->quoteInto('BC_CHART_PROFIL_ID = ?', $destProfileId));
                            
            $sql = 'INSERT INTO BC_CHART_PROFIL_X_BT (BC_CHART_PROFIL_ID, BOOKING_TYPE_ID) ' .
                    'SELECT :destProfileId, BOOKING_TYPE_ID from BC_CHART_PROFIL_X_BT
                     where BC_CHART_PROFIL_ID = :srcProfileId';
                    
            $statment = $this->getAdapter()->prepare($sql);
            $statment->bindValue(':destProfileId', $destProfileId);
            $statment->bindValue(':srcProfileId', $srcProfileId);

            $statment->execute();
        } catch (Exception $e) {
            $this->getAdapter()->rollback();
            return false;
        }
        $this->getAdapter()->commit();
        return true;
    }
}
