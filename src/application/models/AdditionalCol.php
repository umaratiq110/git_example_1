<?php
/**
 * AdditionalCol
 */
class AdditionalCol extends Iso_Model_IsoModel
{
    const MONTH_END_COMPANY             = 1;
    const MONTH_END_DIVISION            = 2;
    const MONTH_END_HOLIDAY_MANAGER     = 3;
    const MONTH_END_RESPONSIBLE_LEADING = 4;
    const MONTH_END_WEEKLY_HRS          = 5;
    const MONTH_END_HOLIDAY_PER_YEAR    = 6;
    const MONTH_END_LEAVING_DATE        = 7;
    const MONTH_END_ENTRY_DATE          = 8;
    const MONTH_END_STATUS              = 9;
    const MONTH_END_SICK_LEAVE_DAYS     = 10;
    const MONTH_END_TAKEN_HOLIDAYS      = 11;
    const MONTH_END_ACCEPTED_HOLIDAYS   = 12;
    const LIST_COMPANY                  = 13;
    const LIST_DIVISION                 = 14;
    const LIST_EMPLOYEE_NAME            = 15;
    const LIST_DAY_TYPE                 = 16;
    const LIST_BEGIN_END                = 17;
    const LIST_FROM_TILL                = 18;
    const MONTH_END_ACT_OVERTIME        = 19;
    const MONTH_END_ACCOUNTABLE_HOURS   = 20;
    const BUSINESS_KILOMETERS           = 21;
    const LIST_BREAK                    = 22;
    const MONTH_END_DAYS_IN_OFFICE      = 23;

    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'BC_ADDITIONAL_COL';

    protected $_primary = 'ID';
    
    protected $_columns = array(
        'ID'
    );
    // @codingStandardsIgnoreEnd
    
    public function __construct()
    {
        parent::__construct();
        
        $this->_db = Zend_Registry::get('db');
    }
    
    /**
     * Returns MonthEnd extra columns
     *
     * @param string $language
     * @return array
     */
    public function getAdditionalCols($language, $reportType = null)
    {
        // language switch
        $columns = $this->_columns;
        $columns['NAME'] = 'NAME_' . strtoupper($language);
        $columns['TOOLTIP'] = 'TOOLTIP_' . strtoupper($language);
        
        $select = $this->getAdapter()->select()
            ->from($this->_name, $columns)
            ->join(
                'bc_report_type',
                'bc_report_type.id = bc_additional_col.report_type_id',
                array('report_type' => 'shortcut')
            )
            ->order(array('sort_order'));
        
        if (!is_null($reportType)) {
            $select->where('report_type_id = ?', $reportType);
        }
        
        return $select->query()->fetchAll();
    }
}
