<?php
/*
 * ten, 2009.06.09 iso-7465-i:
 * Ermittelt die Informationen zu einer Meldung
 */
class PDBEntry extends Iso_Model_IsoModel
{
    private $product;
    private $project;
    private $pdbid;
    private $classification;
    //Zahlungspakete Infos laden
    private $zahlungspaket;
    
    private $title;
    private $state_de;
    private $state_en;

    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_primary = 'id';
    // @codingStandardsIgnoreEnd

    public function __construct($pdbid)
    {
        parent::__construct();
        
        $this->pdbid = $pdbid;
        
        $this->fetchPdbInfo();
        $this->fetchClassificationInfo();
        //Zahlungspakete Infos laden
        $this->fetchZpInfo();
    }
    
    /*
     * Informationen als Array zurückliefern
     */
    public function getInfos($language = 'EN-GB')
    {
        //Zahlungspakete Infos mit ausgeben
        if (strtoupper($language) == 'DE-DE') {
            return array('PRODUCT' => $this->product,
                         'PROJECT' => $this->project,
                         'TITLE' => $this->title,
                         'PDBID' => $this->pdbid,
                         'CLASSIFICATION' => $this->prepareClassification($language),
                         'ZAHLUNGSPAKETE' => $this->zahlungspaket,
                         'STATE' => $this->state_de
                        );
        } else {
            return array('PRODUCT' => $this->product,
                         'PROJECT' => $this->project,
                         'TITLE' => $this->title,
                         'PDBID' => $this->pdbid,
                         'CLASSIFICATION' => $this->prepareClassification($language),
                         'ZAHLUNGSPAKETE' => $this->zahlungspaket,
                         'STATE' => $this->state_en
                        );
        }
    }
    
    /*
     * besitzt die Meldung Infos?
     */
    public function hasInfos()
    {
        if (empty($this->title)
                && empty($this->project)
                && empty($this->product)) {
            return false;
        } else {
            return true;
        }
    }
    
    /*
     * Klassifikationen für die Meldung formatieren
     */
    private function prepareClassification($language)
    {
        $result = array();
        
        if ($this->classification != null) {
            for ($loop = 0; $loop < count($this->classification); $loop++) {
                $result[$loop]['DATE'] = $this->classification[$loop]['DATUM'];
                $result[$loop]['TYP']  = $this->classification[$loop]['TYP'];
            
                if (strtoupper($language) == 'DE-DE') {
                    $result[$loop]['CLASSIFICATION'] = $this->classification[$loop]['CLASSIFICATION_DE'];
                } else {
                    $result[$loop]['CLASSIFICATION'] = $this->classification[$loop]['CLASSIFICATION_EN'];
                }
            }
        }
        return $result;
    }
    
    
    private function fetchPdbInfo()
    {
        /*
         * Produkt.Name, Projekt.Name, MeldungsTitel, Status für Meldung laden
         *
         */
        $sql = 'SELECT produkt.name as product, projekt.name as project, fmaa.titel as title, status.wert as state_de,
                status.wert_en as state_en
				FROM produkt INNER JOIN projekt ON produkt.id = projekt.id_produkt
							 INNER JOIN fmaa    ON projekt.id = fmaa.id_projekt
							 INNER JOIN fmaaxstatus ON fmaa.id = fmaaxstatus.id_fmaa
							 INNER JOIN status ON status.id = fmaaxstatus.id_status
				WHERE fmaaxstatus.akt_status = 1 AND fmaa.kundenid = ?';
                
        $row = $this->getAdapter()->fetchRow($sql, array($this->pdbid));
        
                
        if ($row) {
            $this->product            = $row['PRODUCT'];
            $this->project            = $row['PROJECT'];
            $this->title            = $row['TITLE'];
            $this->state_de            = $row['STATE_DE'];
            $this->state_en            = $row['STATE_EN'];
        } else {
            $this->product            = '';
            $this->project            = '';
            $this->title            = '';
            $this->state_de            = '';
            $this->state_en            = '';
        }
    }
    
    private function fetchClassificationInfo()
    {
        /*
         * Klassifikationen für die Meldungsid laden
         */
        
        $sql = '(
				SELECT fmaa_aufnahme.datum, kunden_klass.kat_k as classification_de, kunden_klass.kat_k_en as
				classification_en, typ
				FROM fmaa INNER JOIN fmaa_aufnahme ON fmaa.id = fmaa_aufnahme.id_fmaa
						  INNER JOIN kunden_klass ON fmaa_aufnahme.id_kundenklass = kunden_klass.id
				WHERE fmaa.kundenid = ?
				)
				union
				(
				SELECT fmaa_stellungnahme.klassdat, iso_klass.kat_i as classification_de, iso_klass.kat_i_en as
				classification_en, -1
				FROM fmaa INNER JOIN fmaa_stellungnahme ON fmaa.id = fmaa_stellungnahme.id_fmaa
						  INNER JOIN iso_klass ON fmaa_stellungnahme.id_isoklass = iso_klass.id
				WHERE fmaa.kundenid = ?
				)
				ORDER BY datum DESC
				';
                
        $rows = $this->getAdapter()->fetchAll($sql, array($this->pdbid, $this->pdbid));
        
                
        if ($rows) {
            $this->classification = $rows;
        } else {
            $this->classification = '';
        }
    }
    
    /**
     * Lädt alle Zahlungspaketeinträge zu einer Meldung
     */
    private function fetchZpInfo()
    {
        $sql = "SELECT ZAHLUNGSPAKET.Bezeichnung,
                       ZAHLUNGSPAKETSTATUS.Status_kuerzel
                FROM ZAHLUNGSPAKET
                LEFT JOIN FMAA ON FMAA.id = ZAHLUNGSPAKET.fmaa_id
                LEFT JOIN ZAHLUNGSPAKETSTATUS ON ZAHLUNGSPAKET.zahlungspaketstatus_id = ZAHLUNGSPAKETSTATUS.id
                LEFT JOIN JIRA_ISSUE_ID issue ON ZAHLUNGSPAKET.issue_id = issue.id
                LEFT JOIN JIRA_PROJECT_NAME jira_project ON issue.project_id = jira_project.id
                WHERE ZAHLUNGSPAKET.next_id IS NULL AND ((FMAA.kundenid IS NOT NULL AND FMAA.kundenid = ?) OR
                (jira_project.id IS NOT NULL AND (jira_project.name || '-' || issue.issue_number) = ?))";
        $rows = $this->getAdapter()->fetchAll($sql, [ $this->pdbid, $this->pdbid ]);
        if ($rows) {
            $this->zahlungspaket = $rows;
        } else {
            $this->zahlungspaket = '';
        }
    }
}
