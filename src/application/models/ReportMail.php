<?php
/**
 * Report class which send emails with reports as attachments
 * to the users with given frequency (daily/weekly/monthly).
 *
 * @author les
 */
class ReportMail
{
    const DAILY = 1;
    const WEEKLY = 2;
    const MONTHLY = 3;

    /**
     * @var Iso_Controller_DefaultController
     */
    protected $controller;

    /**
     * @var Zend_Db_Adapter_Abstract
     */
    protected $db;

    /**
     * @var Zend_Config
     */
    protected $config;

    /**
     * @var Zend_Cache_Core
     */
    protected $cache;

    /**
     * @var Zend_Log
     */
    protected $log;

    /**
     * @var Zend_Session_Namespace
     */
    protected $session;

    /**
     * @var Zend_View
     */
    protected $view;

    /**
     * @var Zend_View_Helper_Translate[]
     */
    protected $translateHelpers;

    /**
     * @var Zend_Mail_Transport_Smtp
     */
    protected $transport;

    /**
     * Constructor
     *
     * @param Iso_Controller_DefaultController $controller
     */
    public function __construct(Iso_Controller_DefaultController $controller)
    {
        $this->controller = $controller;

        $bootstrap = $controller->getInvokeArg('bootstrap');

        $this->db = $bootstrap->getResource('db');
        $this->config = $bootstrap->getResource('config');
        $this->log = $bootstrap->getResource('log');
        $this->session = $bootstrap->getResource('sessionNamespace');

        // use array (= memory) caching class to prevent unnecessary disk I/O by the default cache backend "File"
        $this->cache = new Iso_Cache_Array();
        $bootstrap->getResource('cache')->setCache('report', $this->cache);
    }

    /**
     * Iterates over active profiles and sends emails with the reports
     */
    public function send(string $shortcut = null)
    {
        $errorCount = 0;
        $transportFailure = false;

        try {
            $this->log->info("Report mail generation started");

            $this->log->debug('Detecting report mail types to be generated');
            $date = new Zend_Date();
            // daily reports each time
            $frequency = [self::DAILY];

            $weekDay = $date->get(Zend_Date::WEEKDAY_DIGIT);
            // weekly reports on Mondays
            if ($weekDay == 1) {
                $frequency[] = self::WEEKLY;
            }

            // monthly reports when bookings are finalized for this month and report has not been sent yet
            if (true === $this->checkBookingFinalizationTable()) {
                $frequency[] = self::MONTHLY;
            }
            $this->log->info(sprintf(
                "Report mail types to be generated: %s",
                str_replace(
                    [self::DAILY, self::WEEKLY, self::MONTHLY],
                    ['daily', 'weekly', 'monthly'],
                    implode(', ', $frequency)
                )
            ));

            $this->log->debug("Fetching profiles with report mails to be sent");

            $select = $this->db
                ->select()
                ->from(['profil' => 'bc_chart_profil'], ['id', 'bezeichnung', 'anzeige', 'mitarbeiter_id', 'notiztext'])
                ->join('mitarbeiter', 'mitarbeiter.id = profil.mitarbeiter_id', [])
                ->join('firma', 'firma.id = mitarbeiter.id_firma', [])
                ->where('profil.report_mail IN (?)', $frequency)
                ->where('profil.aktiv = ?', 'J')
                ->where('profil.bezeichnung IS NOT null')
                ->where('mitarbeiter.status = ?', 'aktiv')
                ->where('firma.kuerzel = ?', $this->config->company->ownId)
                ->order(['mitarbeiter.id']);
            if ($shortcut !== null) {
                $select->where('mitarbeiter.kuerzel = ?', strtolower($shortcut));
            }
            $stmt = $select->query();

            $employeeProfiles = [];
            while (($row = $stmt->fetch(Zend_Db::FETCH_ASSOC)) !== false) {
                $employeeProfiles[$row['MITARBEITER_ID']][] = $row;
            }

            foreach ($employeeProfiles as $employeeId => &$profiles) {
                $attachments = [];
                $invalidProfiles = [];
                $generatedProfileCount = 0;
                $translate = null;

                $user = new Iso_User($this->config->company->ownId, $employeeId);

                if (!$user || !$user->isActive() || !$user->isExistant()) {
                    $this->log->err(sprintf("Unable to process employee '%s' (%s)", $user->getKuerzel(), $user
                        ->getId()));
                    $errorCount += 1;
                    continue;
                }

                $this->log->info(sprintf("Processing employee '%s' (%s) with %s profile(s)", $user->getKuerzel(), $user
                    ->getId(), sizeof($profiles)));

                foreach ($profiles as &$profile) {
                    $this->log->debug(sprintf("Adding profile '%s' (%s)", $profile['BEZEICHNUNG'], $profile['ID']));

                    $reportType = $profile['ANZEIGE'];

                    if ($reportType !== 'Q') {
                        // Check if the report starts before 01/01/2012
                        $periodFrom = null;
                        $periodUntil = null;
                        Date::determinePeriod($profile['ID'], $periodFrom, $periodUntil, $this->db);
                        if ((int)$periodFrom->get(Zend_Date::YEAR) < 2012) {
                            $this->log->notice(
                                sprintf(
                                    "Skipping profile '%s' (%s) because its period starts before 2012",
                                    $profile['BEZEICHNUNG'],
                                    $profile['ID']
                                )
                            );
                            continue;
                        }
                    }

                    $lang = $this->controller->checkLanguage($user->getSprache());
                    $indexTranslate =  $this->getTranslateHelper('BC_index.php', $lang);
                    $translate = $this->getTranslateHelper('BC_Export.php', $lang);

                    $renderer = new ReportRenderer($this->session, $user, $indexTranslate, GenerationLog::REPORT_GENERATION_TYPE_CYCLIC);
                    $timestamp = time();
                    $result = $renderer->validateAndRender($this->controller, $profile['ID'], $timestamp);

                    if (isset($result['error']) && false !== $result['error']) {
                        $this->log->notice(
                            sprintf(
                                "Skipping profile '%s' (%s) because it's invalid",
                                $profile['BEZEICHNUNG'],
                                $profile['ID']
                            )
                        );
                        array_push($invalidProfiles, $profile['BEZEICHNUNG']);
                        continue;
                    }

                    $this->log->debug("Generating export file");

                    $view = new Zend_View();
                    $view->addHelperPath(APPLICATION_PATH . '/../library/Iso/View/Helper/', 'Iso_View_Helper_');

                    $exportParams = [];
                    $fileExt = '.xlsx';
                    switch ($reportType) {
                        case 'L':
                            $type = 'liste';
                            $exportFn = 'exportList';

                            $view->sort_column = 'CDATE';
                            break;
                        case 'T':
                            $type = 'table';
                            $exportFn = 'exportTable';
                            break;
                        case 'B':
                            $type = 'bookingsWith';
                            $exportFn = 'exportBookings';
                            $exportParams[] = true;
                            break;
                        case 'M':
                            $type = 'monthEnd';
                            $exportFn = 'exportMonthEnd';
                            break;
                        case 'S':
                            $type = 'sapKeyHours';
                            $exportFn = 'exportSAPKeyHours';
                            break;
                        case 'H':
                            $type = 'standby';
                            $exportFn = 'exportStandby';
                            break;
                        case 'C':
                            $type = 'absenceCalendar';
                            $exportFn = 'exportAbsenceCalendar';
                            break;
                        case 'K':
                            $type = 'status';
                            $exportFn = 'exportStatus';
                            break;
                        case 'V':
                            $type = 'orderOverview';
                            $exportFn = 'exportOrderOverview';
                            break;
                        case 'Q':
                            $type = 'contingentRange';
                            $exportFn = 'exportContingentRange';
                            break;
                        case 'Z':
                            $type = 'contingentData';
                            $exportFn = 'exportContingentData';
                            break;
                    }

                    $view->type = $type;
                    // data from session (direct download reports)
                    if (isset($this->session->$type) && isset($this->session->{$type}[$timestamp])) {
                        $view->data = $this->session->{$type}[$timestamp];
                        unset($this->session->{$type}[$timestamp]);
                    } else {
                        // data from cache
                        $cacheId = $profile['ID'] . '_' . $timestamp;
                        $view->data = $this->cache->load($cacheId);
                        $this->cache->remove($cacheId);
                    }

                    try {
                        Report::setExportHeaderData($this->controller, (object)$profile, $user);
                        $filename = $profile['BEZEICHNUNG'] . $this->controller->filename_suffix . $fileExt;
                        $exportObj = new Export($view, $translate, $this->config->main->export->save_path, $filename);

                        $content = call_user_func_array([$exportObj, $exportFn], $exportParams);
                    }
                    catch (Exception $e) {
                        $this->log->crit($e);
                        $this->sendErrorMail($e, [
                            'employeeId' => $employeeId,
                            'profileId' => $profile['ID'],
                            'reportType' => $reportType
                        ]);
                        continue;
                    }

                    $this->log->debug("Export file generation finished");

                    array_push($attachments, [
                        'filename' => $filename,
                        'content' => $content
                    ]);

                    ++$generatedProfileCount;

                    // garbage collection
                    unset($renderer, $view, $exportObj, $content);
                }

                if (empty($attachments) && empty($invalidProfiles)) {
                    $this->log->warn(
                        sprintf(
                            "No reports have been generated for employee '%s' (%s)",
                            $user->getKuerzel(),
                            $user->getId()
                        )
                    );
                    continue;
                }

                $this->log->debug("Submitting email to mail server");
                try {
                    $this->sendMail($user->getUserMail(), $attachments, $invalidProfiles, $translate);
                    $this->log->info(sprintf(
                        "Successfully sent mail with %s report(s) to employee '%s' (%s)",
                        $generatedProfileCount,
                        $user->getKuerzel(),
                        $user->getId()
                    ));
                } catch (Zend_Mail_Exception $e) {
                    $this->log->err(sprintf(
                        "Failed to send report mail to employee '%s' (%s) %s %s",
                        $user->getKuerzel(),
                        $user->getId(),
                        PHP_EOL,
                        $e
                    ));
                    ++$errorCount;
                    $transportFailure = true;
                }

                // garbage collection
                unset($attachments);

                //$this->logMemoryUsage();
            }

            if (!$transportFailure && true === $this->checkBookingFinalizationTable()) {
                $this->log->info("Writing report generation date for monthly reports to database");
                $this->updateBookingFinalizationTable();
            }
        } catch (Exception $e) {
            $this->log->crit(sprintf("Uncaught exception: %s %s", PHP_EOL, $e));
            return 1;
        }

        if ($errorCount > 0) {
            $this->log->warn(sprintf("Report mail generation finished with %s errors", $errorCount));
        } else {
            $this->log->info("Report mail generation finished without errors");
        }

        return 0;
    }

    /**
     * Sends an email with given attachments
     *
     * @param string $to
     * @param array $attachments
     * @param array $invalidReports
     * @param object $translator
     * @throws Zend_Mail_Exception
     * @return void
     */
    protected function sendMail($to, $attachments, $invalidReports, $translator)
    {
        $subject = $translator->translate('reportmailsubject');
        $message = '';
        if (false == empty($invalidReports)) {
            $message = $translator->translate('reportValidatorExportDataInvalid');
            $message .= "\n\n- ";
            $message .= implode("\n- ", $invalidReports);
        }

        $mail = new Iso_Mail('utf-8');
        $mail->addTo($to)
            ->setSubject($subject)
            ->setBodyText($message);

        foreach ($attachments as $item) {
            $att = new Zend_Mime_Part($item['content']);
            $att->type        = 'application/octet-stream';
            $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
            $att->encoding    = Zend_Mime::ENCODING_BASE64;
            $att->filename    = $item['filename'];

            $mail->addAttachment($att);
        }

        $mail->send($this->getTransport());
    }

    /**
     * Creates translate adapter
     *
     * @param string $languageFile
     * @param string $lang
     * @return Zend_View_Helper_Translate
     */
    protected function getTranslateHelper($languageFile, $lang)
    {
        $key = $languageFile . ':' . $lang;

        if (!isset($this->translationAdapters[$key])) {
            $this->translateHelpers[$key] = new Zend_View_Helper_Translate(
                new Iso_Translate_DbTranslate('database', $languageFile, $lang)
            );
        }

        return $this->translateHelpers[$key];
    }

    /**
     * Checks if bookings are finalized for month
     *
     * @return bool
     * @throws Zend_Db_Statement_Exception
     */
    protected function checkBookingFinalizationTable(): bool
    {
        $sql = "
            SELECT
                count(*) AS row_count
            FROM booking_finalization
            WHERE report_generated IS null
            AND month = date_trunc('month', current_date) - interval '1' month
        ";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = (int)$stmt->fetch(Zend_Db::FETCH_COLUMN);

        return $result !== 0;
    }

    /**
     * Marks report as generated (only valid for month reports)
     */
    protected function updateBookingFinalizationTable()
    {
        $sql = "
            UPDATE booking_finalization
            SET report_generated = localtimestamp
            WHERE report_generated IS null
            AND month = date_trunc('month', localtimestamp) - interval '1' month
        ";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
    }

    /**
     * Logs current memory usage for debugging purposes
     */
    protected function logMemoryUsage()
    {
        static $lastVal = 0;
        static $lastPeak = 0;
        $val = memory_get_usage();
        $peak = memory_get_peak_usage();

        $this->log->debug(sprintf(
            "Memory: %.1fM (%+.1fM); Peak: %.1fM (%+.1fM)",
            $val / 1048576,
            ($val - $lastVal) / 1048576,
            $peak / 1048576,
            ($peak - $lastPeak) / 1048576
        ));

        $lastVal = $val;
        $lastPeak = $peak;
    }

    /**
     * @param Exception $exception
     * @param array $context
     * @throws Zend_Exception
     */
    public function sendErrorMail(Exception $exception, array $context)
    {
        $subject = 'BKZ Cockpit - Error in Report Mail Generation';
        $contextInfo = null;
        foreach ($context as $k => $info) {
            $contextInfo .= sprintf("%s: %s\n", $k, $info);
        }
        $body = sprintf(
            "%s\n%s",
            $exception,
            $contextInfo
        );
        try {
            $mail = new Iso_Mail('utf-8');
            $mail->addTo($this->config->environment->mail)
                ->setSubject($subject)
                ->setBodyText($body);
            $mail->send();
        }
        catch (Zend_Mail_Exception $e) {
            $this->log->crit($e);
        }
    }

    /**
     * Returns the SMTP transport object after making sure that the connection
     * to the SMTP server was established successfully
     *
     * @return Zend_Mail_Transport_Smtp
     * @throws Zend_Mail_Protocol_Exception
     */
    protected function getTransport()
    {
        $options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getPluginResource('mail')->getOptions();
        $host = $options['transport']['host'];
        $port = $options['transport']['port'] ?? 25;
        if (!isset($this->transport)) {
            $this->log->debug("Initializing the SMTP transport");
            $this->transport = new Zend_Mail_Transport_Smtp($host, ['port' => $port]);
        }

        $connection = $this->transport->getConnection();
        if ($connection instanceof Zend_Mail_Protocol_Smtp) {
            try {
                @$connection->rset();
            } catch (Zend_Mail_Protocol_Exception $e) {
                $this->log->warn("SMTP connection timeout");
                $connection = null;
            }
        }

        if (!($connection instanceof Zend_Mail_Protocol_Smtp)) {
            $this->log->debug("Initializing the SMTP connection");
            $connection = new Zend_Mail_Protocol_Smtp($host, $port);
            $connection->connect();
            $connection->helo();

            $this->transport->setConnection($connection);
        }

        return $this->transport;
    }
}
