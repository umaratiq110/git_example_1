<?php
/**
 * BookingType
 */
class BookingType extends Iso_Model_IsoModel
{
    // booking type ids
    const FIXED_PRICE = 1;
    const NOT_ACCOUNTABLE = 2;
    const TIME_AND_MATERIAL = 3;
    const CHANGE_REQUEST = 4;
    const MAINTENANCE = 5;
    
    // ids of booking types which are accountable or not accountable (for report 'Accounting')
    const ID_REPORT_ACCOUNTABLE = 3;
    const ID_REPORT_NOT_ACCOUNTABLE = 2;

    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'BOOKING_TYPE';
    protected $_primary = 'ID';
    
    protected $_columns = array(
        'ID', 'NAME'
    );
    // @codingStandardsIgnoreEnd
    
    public function __construct()
    {
        parent::__construct();
        
        $this->_db = Zend_Registry::get('db');
    }

    /**
     * Returns booking types
     *
     * @param string $language
     * @return array
     */
    public function getBookingTypes($language)
    {
        // language switch
        $columns = $this->_columns;
        $columns['LONG_NAME'] = 'LONG_NAME_' . strtoupper($language);
        $columns['INFORMATION_NAME'] = 'INFORMATION_NAME_' . strtoupper($language);
        
        $select = $this->getAdapter()->select()
            ->from($this->_name, $columns)
            ->order(array('sort_order', 'name'));
        
        return $select->query()->fetchAll();
    }
    
    /**
     * Returns booking types as flattened array
     *
     * @param string $language
     * @return array
     */
    public function getBookingTypesAsArray($ids = null)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, $this->_columns)
            ->order(array('sort_order', 'name'));
        
        if ($ids) {
            $select->where($this->_primary . ' IN (' . join(',', $ids) . ')');
        }
        
        $result = $select->query()->fetchAll();
                
        $bookingTypes = array();
        foreach ($result as $item) {
            $bookingTypes[$item['ID']] = $item['NAME'];
        }
        
        return $bookingTypes;
    }
    
    /**
     * Returns booking types as flattened array
     *
     * @param string $language
     * @return array
     */
    public function getBookingTypesAsFullArray($language)
    {
        $columns = $this->_columns;
        $columns['LONG_NAME'] = 'LONG_NAME_' . strtoupper($language);
        $columns['INFORMATION_NAME'] = 'INFORMATION_NAME_' . strtoupper($language);
            
        $select = $this->getAdapter()->select()
            ->from($this->_name, $columns)
            ->order(array('sort_order', 'name'));
        
        $result = $select->query()->fetchAll();
        
        $bookingTypes = array();
        foreach ($result as $item) {
            $bookingTypes[$item['ID']] = array(
                    'name' => $item['NAME'],
                    'long_name' => $item['LONG_NAME'],
                    'information_name' => $item['INFORMATION_NAME']
                );
        }
        
        return $bookingTypes;
    }
    
    public function getBookingTypesHoursAsArray($ids = null)
    {
        $types = $this->getBookingTypesAsArray($ids);
        $hours = array();
        foreach ($types as $type) {
            array_push($hours, $type . "Hours");
        }
        return $hours;
    }
}
