<?php
class ChartProfil extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart

    /**
     * Table name
     * @var string
     */
    protected $_name = 'BC_CHART_PROFIL';

    /**
     * Primary key
     * @var string
     */
    protected $_primary = 'ID';

    /**
     * Sequence
     * @var string
     */
    protected $_sequence = 'SEQ_BC_CHART_PROFIL';
    // @codingStandardsIgnoreEnd

    const OVERTIME_PAYMENT_MODEL_ID_CASHABLE_WITHOUT_SURCHARGE = 3;

    /**
     * Kopiert ein Chart-Profil in das Default-Profil
     */
    public static function copyChartProfileToDefault($profil_id, $session, $db)
    {
        $defaultprofil_id = $session->defaultprofil;

        $chartprofil = new ChartProfil();
        $sel_profil  = $chartprofil->find($profil_id)->current();
        $def_profil  = $chartprofil->find($defaultprofil_id)->current();

        $def_profil->DATUM            = new Zend_Db_Expr("localtimestamp");
        $def_profil->DIAGRAMM_TYP    = $sel_profil->DIAGRAMM_TYP;
        $def_profil->ZEITRAUM_VON    = $sel_profil->ZEITRAUM_VON;
        $def_profil->ZEITRAUM_BIS    = $sel_profil->ZEITRAUM_BIS;
        $def_profil->LIMIT            = $sel_profil->LIMIT;
        $def_profil->STARTWERT        = $sel_profil->STARTWERT;
        $def_profil->PROJEKT_ID        = $sel_profil->PROJEKT_ID;
        $def_profil->AKTIV            = $sel_profil->AKTIV;
        $def_profil->IST            = $sel_profil->IST;
        $def_profil->ANZEIGE        = $sel_profil->ANZEIGE;
        $def_profil->NOTIZTEXT        = $sel_profil->NOTIZTEXT;
        $def_profil->DYNAMIC_PERIOD = $sel_profil->DYNAMIC_PERIOD;
        $def_profil->COST_AND_REVENUE = $sel_profil->COST_AND_REVENUE;
        $def_profil->EMPLOYEE_BOOKING = $sel_profil->EMPLOYEE_BOOKING;
        $def_profil->TARGET_HOURS = $sel_profil->TARGET_HOURS;
        $def_profil->EMP_WITHOUT_BC_PERMISSION = $sel_profil->EMP_WITHOUT_BC_PERMISSION;
        $def_profil->EMPLOYEES_AS_GROUPS = $sel_profil->EMPLOYEES_AS_GROUPS;
        $def_profil->EMPLOYEES_WITH_ABSENCES = $sel_profil->EMPLOYEES_WITH_ABSENCES;
        $def_profil->PERFORMED_OR_BOOKING_DATE = $sel_profil->PERFORMED_OR_BOOKING_DATE;
        $def_profil->WITH_LOCKED_NODES = $sel_profil->WITH_LOCKED_NODES;
        $def_profil->save();

        if ($profil_id != $defaultprofil_id) {
            $chartProfileNode = new ChartProfileNode();
            $chartProfileNode->copy($profil_id, $defaultprofil_id);

            $chartProfileEmployee = new ChartProfileEmployee();
            $chartProfileEmployee->copy($profil_id, $defaultprofil_id);

            $chartProfileBT = new ChartProfileBookingType();
            $chartProfileBT->copy($profil_id, $defaultprofil_id);

            $chartProfileAdditionalCols = new ChartProfileAdditionalCol();
            $chartProfileAdditionalCols->copy($profil_id, $defaultprofil_id);
        }
    }

    /**
     * IST-Wert und Limit-Ueberschreitung von der Datenbank berechnen lassen
     */
    public static function calculateChartProfile($profil, $db)
    {
        /* IST-Wert und Limit-Überschreitung berechnen */
        $statement = $db->prepare('CALL PACK_BKZCOCKPIT.calculateChartProfile(:PROFIL_ID)');
        /* Profil ID binden */
        $statement->bindValue(':PROFIL_ID', $profil);
        $statement->execute();
    }

    /**
     * Chart Profil in Datenbank speichern
     */
    public static function saveChartProfile($name, $configuration, $params, $user, $db)
    {
        if ($configuration == null) {
            return false;
        }
        $profileName = empty($name) ? null : $name;
        $chart_profil = new ChartProfil();
        $select = null;

        if ($profileName == null) {
            // Default profile
            $select = $chart_profil->select()
                ->where('BEZEICHNUNG IS NULL')
                ->where('MITARBEITER_ID = ?', $user->getId());
        } else {
            // For $params->create do not set anything
            if ($params->update && $params->profileData !== null) {
                $select = $chart_profil->select();

                if ($params->profileData->own === true) {
                    $select->where('BEZEICHNUNG = ?', $params->update)->where('MITARBEITER_ID = ?', $user->getId());
                } else {
                    $select->where('ID = ?', $params->profileData->id);
                }

            }

            if ($params->remove) {
                $toRemove = $chart_profil->select()
                    ->where('BEZEICHNUNG = ?', $params->remove)
                    ->where('MITARBEITER_ID = ?', $user->getId());

                self::removeProfile($chart_profil->fetchRow($toRemove)->ID);
            }
        }

        $row = $select ? $chart_profil->fetchRow($select) : null;

        if ($configuration->zeitraum_von != '') {
            if (strlen($configuration->zeitraum_von) == 10) {
                $zeitraum_von = date('Y-m-d', gmmktime(0, 0, 0, substr($configuration->zeitraum_von, 5, 2), substr($configuration->zeitraum_von, 8, 2), substr($configuration->zeitraum_von, 0, 4)));
            } else {
                $zeitraum_von = date('Y-m-d', gmmktime(0, 0, 0, substr($configuration->zeitraum_von, 5, 2), 1, substr($configuration->zeitraum_von, 0, 4)));
            }
            $zeitraum_von = new Zend_Db_Expr("to_date('" . $zeitraum_von . "', 'YYYY-MM-DD')");
        } else {
            $zeitraum_von = null;
        }

        if ($configuration->zeitraum_bis != '') {
            if (strlen($configuration->zeitraum_bis) == 10) {
                $zeitraum_bis = date('Y-m-d', gmmktime(0, 0, 0, substr($configuration->zeitraum_bis, 5, 2), substr($configuration->zeitraum_bis, 8, 2), substr($configuration->zeitraum_bis, 0, 4)));
            } else {
                $zeitraum_bis = date('Y-m-d', gmmktime(0, 0, 0, substr($configuration->zeitraum_bis, 5, 2) + 1, 0, substr($configuration->zeitraum_bis, 0, 4)));
            }
            $zeitraum_bis = new Zend_Db_Expr("to_date('" . $zeitraum_bis . "', 'YYYY-MM-DD')");
        } else {
            $zeitraum_bis = null;
        }

        $startwert = 'N';

        if ($configuration->startwert == true) {
            $startwert = 'J';
        }

        $projekt = null;

        if ($configuration->projekt != '') {
            $projekt = intval($configuration->projekt);
        }

        $limit = null;

        if ($configuration->limit != '') {
            $limit = round(General::toFloat($configuration->limit), 2);
        }

        $notiztext = null;
        if ($configuration->notiztext != '') {
            $notiztext = $configuration->notiztext;
        }

        // report type
        $reportTypes = ['D','L','T','E','B','A','R','M','N','S','P','O','F','H','C','I','J','K','G','V','Q','Z','X', 'Y'];
        if (empty($configuration->anzeige) || in_array($configuration->anzeige, $reportTypes) === false) {
            throw new Exception("Report type must not be empty and one of: " . implode(',', $reportTypes));
        } else {
            $anzeige = $configuration->anzeige;
        }

        // unset act-hours mail for report types which do not support it
        if (in_array($anzeige, ['D', 'L', 'T', 'B', 'V']) && $configuration->eMail != '') {
            $eMail = $configuration->eMail;
        } else {
            $eMail = 0;
        }

        // unset report mail for report types which do not support it
        if (in_array($anzeige, ['L', 'T', 'B', 'M', 'S', 'H', 'C', 'K', 'Q', 'Y']) && $configuration->reportMail != '') {
            $reportMail = $configuration->reportMail;
        } else {
            $reportMail = 0;
        }

        // Permission, BT-Order and Contingent Range report has no dynamic period
        if (in_array($anzeige, ['P', 'F', 'Q'])) {
            $dynamicPeriod = 0;
        } else {
            $dynamicPeriod = $configuration->dynamicPeriod;
        }

        // Permission, BT-Order and Contingent Range report has no relative period
        if (in_array($anzeige, ['P', 'F', 'Q', 'Y'])) {
            $zeitraum_von = null;
            $zeitraum_bis = null;
        } else {
            $dynamicPeriod = $configuration->dynamicPeriod;
        }

        /*
         * Chart-Profil existiert bereits => bestehendes ändern (update)
         */
        if ($row != null) {
            $row->BEZEICHNUNG    = $params->profileData !== null && $params->profileData->own === true ? $profileName : $row->BEZEICHNUNG;
            $row->DATUM        = new Zend_Db_Expr("localtimestamp");
            $row->DIAGRAMM_TYP  = strtoupper($configuration->diagrammtyp);
            $row->ZEITRAUM_VON  = $zeitraum_von;
            $row->ZEITRAUM_BIS  = $zeitraum_bis;
            $row->LIMIT        = $limit;
            $row->STARTWERT     = $startwert;
            $row->PROJEKT_ID    = $projekt;
            $row->ANZEIGE        = $anzeige;
            $row->MAILTYPE        = $eMail;
            $row->REPORT_MAIL    = $reportMail;
            $row->DYNAMIC_PERIOD = $dynamicPeriod;
            $row->NOTIZTEXT        = $notiztext;
            $row->EMPLOYEE_BOOKING    = $configuration->showEmpBookings;
            $row->COST_AND_REVENUE    = $configuration->showCostAndRevenue;
            $row->BOOKING_TYPE_HOURS = $configuration->showBTHours;
            $row->TARGET_HOURS         = $configuration->showTargetHours;
            $row->BOOKINGS_WITH_BEGIN_END = $configuration->showBookingsWithBeginEnd;
            $row->ONLY_REMOTE_WORK_DAYS = $configuration->showRemoteWorkDays;
            $row->EMP_WITHOUT_BC_PERMISSION    = $configuration->showEmployeesWithoutBCPermission;
            $row->EMPLOYEES_AS_GROUPS    = $configuration->showEmployeesAsGroups;
            $row->EMPLOYEES_WITH_ABSENCES    = $configuration->showEmployeesWithAbsences;
            $row->PERFORMED_OR_BOOKING_DATE    = $configuration->showPerformedOrBookingDate;
            $row->WITH_LOCKED_NODES    = $configuration->showWithLockedNodes;
            $row->CC_WITH_HOUR_BOOKINGS    = $configuration->ccWithHourBookings;
            $row->X_AXIS = $configuration->xAxis;
            $row->DIVISION_ID = $configuration->divisionId;
            $row->TARGET_HOURLY_RATE = $configuration->targetHourlyRate;
            $row->ZERO_HOURS_PER_WEEK = $configuration->showZeroHoursPerWeek;
            $row->ONE_LINE_PER_EMPLOYEE = $configuration->showOneLinePerEmployee;
            $row->ONE_TAB_PER_COMPANY = $configuration->showOneTabPerCompany;
            $row->DISPLAY_FIGURES = $configuration->displayFigures;
            $row->DISPLAY_LINES = $configuration->displayLines;
            $row->DISPLAY_ONLY_SUBTREES = $configuration->displayOnlySubtrees;
            $row->CLOSED_ORDERS = $configuration->showClosedOrders;
            $row->ORDER_AFTER_PERIOD = $configuration->showOrdersAfterPeriod;
            $row->WITHOUT_ORDERS_AFTER_PERIOD = $configuration->showWithoutOrdersAfterPeriod;
            $row->WITH_HIGHER_LEVEL_POSITION = $configuration->showWithHigherLevelPosition;
            $row->WITH_EXTERNAL_EMPLOYEES = $configuration->showExternalEmployees;
            $row->CONTINGENT_DATA_REPORT_TYPE = $configuration->contingentDataReportType;
            $row->save();
            $chart_profil_id = $row->ID;
        } else {
            /*
             * Chart-Profil existiert noch nicht => neu anlegen (insert)
             */
            $data = [
                'MITARBEITER_ID' => intval($user->getid()),
                'BEZEICHNUNG'     => $profileName,
                'DIAGRAMM_TYP'     => strtoupper($configuration->diagrammtyp),
                'ZEITRAUM_VON'     => $zeitraum_von,
                'ZEITRAUM_BIS'     => $zeitraum_bis,
                'LIMIT'         => $limit,
                'STARTWERT'         => $startwert,
                'PROJEKT_ID'     => $projekt,
                'ANZEIGE'         => $anzeige,
                'MAILTYPE'         => $eMail,
                'REPORT_MAIL'     => $reportMail,
                'DYNAMIC_PERIOD' => $dynamicPeriod,
                'NOTIZTEXT'         => $notiztext,
                'EMPLOYEE_BOOKING'    => $configuration->showEmpBookings,
                'COST_AND_REVENUE'    => $configuration->showCostAndRevenue,
                'BOOKING_TYPE_HOURS' => $configuration->showBTHours,
                'TARGET_HOURS'         => $configuration->showTargetHours,
                'BOOKINGS_WITH_BEGIN_END' => $configuration->showBookingsWithBeginEnd,
                'ONLY_REMOTE_WORK_DAYS' => $configuration->showRemoteWorkDays,
                'EMP_WITHOUT_BC_PERMISSION'    => $configuration->showEmployeesWithoutBCPermission,
                'EMPLOYEES_WITH_ABSENCES' => $configuration->showEmployeesWithAbsences,
                'EMPLOYEES_AS_GROUPS'    => $configuration->showEmployeesAsGroups,
                'PERFORMED_OR_BOOKING_DATE'    => $configuration->showPerformedOrBookingDate,
                'WITH_LOCKED_NODES'    => $configuration->showWithLockedNodes,
                'CC_WITH_HOUR_BOOKINGS'    => $configuration->ccWithHourBookings,
                'X_AXIS' => $configuration->xAxis,
                'DIVISION_ID' => $configuration->divisionId,
                'TARGET_HOURLY_RATE' => $configuration->targetHourlyRate,
                'ZERO_HOURS_PER_WEEK' => $configuration->showZeroHoursPerWeek,
                'DISPLAY_FIGURES' => $configuration->displayFigures,
                'DISPLAY_LINES' => $configuration->displayLines,
                'ONE_LINE_PER_EMPLOYEE' => $configuration->showOneLinePerEmployee,
                'ONE_TAB_PER_COMPANY' => $configuration->showOneTabPerCompany,
                'DISPLAY_ONLY_SUBTREES' => $configuration->displayOnlySubtrees,
                'CLOSED_ORDERS' => $configuration->showClosedOrders,
                'ORDER_AFTER_PERIOD' => $configuration->showOrdersAfterPeriod,
                'WITHOUT_ORDERS_AFTER_PERIOD' => $configuration->showWithoutOrdersAfterPeriod,
                'WITH_HIGHER_LEVEL_POSITION' => $configuration->showWithHigherLevelPosition,
                'WITH_EXTERNAL_EMPLOYEES' => $configuration->showExternalEmployees,
                'CONTINGENT_DATA_REPORT_TYPE' => $configuration->contingentDataReportType
            ];

            $chart_profil_id = $chart_profil->insert($data);
        }

        ChartProfil::modifyChartProfileNodes($chart_profil_id, $configuration);
        ChartProfil::modifyChartProfileEmployees($chart_profil_id, $configuration);
        ChartProfil::modifyChartProfileBookingTypes($chart_profil_id, $configuration);
        ChartProfil::modifyChartProfileShowToUsers($db, $chart_profil_id, $configuration);
        ChartProfil::modifyChartProfileSharedUsers($db, $chart_profil_id, $configuration);
        ChartProfil::modifyChartProfileAdditionalCols($chart_profil_id, $configuration);
        self::modifyChartProfileMaterialFilter($chart_profil_id, $configuration->materialFilter ?? []);
        return $chart_profil_id;
    }

    /**
     * ChartProfile nodes
     */
    public static function modifyChartProfileNodes($profileId, $data)
    {
        $chartProfileNode = new ChartProfileNode();
        $chartProfileNode->setNodes($profileId, isset($data->nodes) ? $data->nodes : []);
    }

    /**
     * ChartProfile employees
     */
    public static function modifyChartProfileEmployees($profileId, $data)
    {
        $chartProfileEmployee = new ChartProfileEmployee();
        $chartProfileEmployee->setEmployees($profileId, isset($data->employees) ? $data->employees : []);
    }

    /**
     * ChartProfile booking types
     */
    public static function modifyChartProfileBookingTypes($profileId, $data)
    {
        $chartProfileBT = new ChartProfileBookingType();
        $chartProfileBT->setBookingTypes($profileId, isset($data->bookingTypes) ? $data->bookingTypes : []);
    }

    /**
     * Changes profile show to users
     */
    public static function modifyChartProfileSharedUsers($db, $profileId, $data)
    {
        // Save the users the profile should be shared with
        $sql = "DELETE FROM bc_chartp_x_shared_emp WHERE bc_chart_profil_id = :PROFIL_ID";
        $statement = $db->prepare($sql);
        $statement->bindValue(':PROFIL_ID', $profileId);
        $statement->execute();

        $addEmployees = $data->shareWithUsers;
        $itemCount = count($addEmployees);
        for ($i = 0; $i < $itemCount; $i++) {
            $sql = "INSERT INTO bc_chartp_x_shared_emp (bc_chart_profil_id, mitarbeiter_id) values (:PROFIL_ID, :MA_ID)";

            $statement = $db->prepare($sql);
            $statement->bindValue(':PROFIL_ID', $profileId);
            $statement->bindValue(':MA_ID', (int)$addEmployees[$i]);
            $statement->execute();
        }
    }

    /**
     * Changes profile shared users
     */
    public static function modifyChartProfileShowToUsers($db, $profileId, $data)
    {
        // Save the users to whom the profile is to be displayed
        $sql = "DELETE FROM bc_chartp_x_addemp WHERE bc_chart_profil_id = :PROFIL_ID";
        $statement = $db->prepare($sql);
        $statement->bindValue(':PROFIL_ID', $profileId);
        $statement->execute();

        $addEmployees = $data->showToUsers;
        $itemCount = count($addEmployees);
        for ($i = 0; $i < $itemCount; $i++) {
            $sql = "INSERT INTO bc_chartp_x_addemp (bc_chart_profil_id, mitarbeiter_id) values (:PROFIL_ID, :MA_ID)";

            $statement = $db->prepare($sql);
            $statement->bindValue(':PROFIL_ID', $profileId);
            $statement->bindValue(':MA_ID', (int)$addEmployees[$i]);
            $statement->execute();
        }
    }

    /*
     * ChartProfile month end extra columns
     */
    public static function modifyChartProfileAdditionalCols($profileId, $data)
    {
        $chartProfileAddCol = new ChartProfileAdditionalCol();
        $chartProfileAddCol->setAdditionalCols($profileId, isset($data->additionalCols) ? $data->additionalCols : []);
    }

    /*
     * Gets chart profile booking types
     */
    public static function getBookingTypes($profileId)
    {
        $chartProfileBT = new ChartProfileBookingType();
        return $chartProfileBT->getBookingTypes($profileId);
    }

    /*
     * Gets chart profile monthEnd extra columns
     */
    public static function getAdditionalColumns($profileId)
    {
        $chartProfileAddCol = new ChartProfileAdditionalCol();
        return $chartProfileAddCol->getAdditionalCols($profileId);
    }

    /*
     * Gets chart profile monthEnd flags
     */
    public static function getChartProfile($profileId)
    {
        $sql = "SELECT * FROM BC_CHART_PROFIL WHERE ID = :profil_id";
        $statement = Zend_Registry::get('db')->prepare($sql);
        $statement->bindValue(':profil_id', $profileId);
        $statement->execute();
        return current($statement->fetchAll());
    }

    /**
     * Removes profile with all associated data
     */
    public static function removeProfile($profileId)
    {
        try {
            $chartProfileNode = new ChartProfileNode();
            $chartProfileNode->setNodes($profileId, []);

            $chartProfileEmployee = new ChartProfileEmployee();
            $chartProfileEmployee->setEmployees($profileId, []);

            $chartProfileBT = new ChartProfileBookingType();
            $chartProfileBT->setBookingTypes($profileId, []);

            $chartProfileAddCol = new ChartProfileAdditionalCol();
            $chartProfileAddCol->setAdditionalCols($profileId, []);

            $sql = "DELETE FROM bc_chartp_x_addemp WHERE bc_chart_profil_id = :PROFIL_ID";
            $statement = Zend_Registry::get('db')->prepare($sql);
            $statement->bindValue(':PROFIL_ID', $profileId);
            $statement->execute();

            $sql = "DELETE FROM bc_chart_profil WHERE id = :PROFIL_ID";
            $statement = Zend_Registry::get('db')->prepare($sql);
            $statement->bindValue(':PROFIL_ID', $profileId);
            $statement->execute();
        } catch (Zend_Db_Exception $e) {
            return false;
        }

        return true;
    }

    public static function loadChartData($profil, $db, $user)
    {
        /*
         * User pruefen, nicht dass ein User ein nicht berechtigtes Profil oeffnet!
         */
        if ($profil->MITARBEITER_ID == $user->getId()) {
            // Determine period of this profile
            Date::determinePeriod($profil->ID, $dateFrom, $dateUntil, $db);

            $profileNodes = ChartProfil::getProfileNodes($profil->ID, $db, false);

            //Determine leafes of profile (section beams and sets) as in statement
            $nodesIn = ChartProfil::getLeafesOfProfile($profil->ID, $db);

            // Determine profile companies
            $companies = ChartProfil::getProfileCompanies($profil->ID);
            // Determine profile divisions
            $divisions = ChartProfil::getProfileDivisions($profil->ID);
            // Determine the selected users
            $employees = ChartProfil::getProfileEmployees($db, $profil->ID);

            $userAndGroupIds = self::getUserAndGroupIdsOfProfile($profil->ID, $db);

            // if resolved employee selection results in no employees at all (e.g. due to empty groups), then report has no data
            if (!empty($userAndGroupIds) && self::noEmployeeSelected($profil->ID, [
                    "companies" => $companies,
                    "divisions" => $divisions,
                    "employees" => $employees['id']
                ])
            ) {
                $employees['id'] = [Employee::DUMMY];
            }

            $companiesWhere = '';
            $divisionsWhere = '';
            $employeesWhere = '';
            $employeesFromBookings = [];
            if (!empty($companies)) {
                $companiesWhere = "employment_period.company_id IN (" . implode(',', $companies) . ")";
            }

            if (!empty($divisions)) {
                $divisionsWhere = "employee_function_period.division_id IN (" . implode(',', $divisions) . ")";
            }

            if (!empty($employees['id'])) {
                $employeesWhere = QueryBuilder::createInClause('cemployee_id', $employees['id']);
            }

            $notiz_where = '';
            if ($profil->NOTIZTEXT != "") {
                $notiz_where = "AND (UPPER(TPWWORKINGHOURS.CNOTE) LIKE UPPER(:NOTIZ_TEXT) OR
                                UPPER(TPWWORKINGHOURS.CNOTEAL) LIKE UPPER(:NOTIZ_TEXT))";
            }

            /*
             * 1. Retrieve data for selected nodes
             */
            $data = [];
            $startwert = null;

            if (substr($profil->DIAGRAMM_TYP, 0, 1) == 'T') {
                $sql = "
					SELECT
						date_to_unixts(cdate) AS datum,
						sum(coalesce(chours, 0)) + sum(coalesce(choursal, 0)) AS ist
					FROM tpwworkinghours
					LEFT JOIN employment_period
						ON employment_period.employee_id = tpwworkinghours.cemployee_id
						AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
					LEFT JOIN employee_function_period
						ON employee_function_period.employee_id = tpwworkinghours.cemployee_id
						AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
					WHERE cnode_id IS NOT null
					AND cdate BETWEEN to_date(:ZEITRAUM_VON, 'YYYY-MM-DD') AND to_date(:ZEITRAUM_BIS, 'YYYY-MM-DD')
					" . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
					$nodesIn
					$notiz_where
					GROUP BY cdate
					ORDER BY datum
				";
            } else {
                $sql = "
					SELECT
						to_char(cdate, 'YYYYMM') AS datum,
						sum(coalesce(chours, 0)) + sum(coalesce(choursal, 0)) AS IST
					FROM tpwworkinghours
					LEFT JOIN employment_period
						ON employment_period.employee_id = tpwworkinghours.cemployee_id
						AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
					LEFT JOIN employee_function_period
						ON employee_function_period.employee_id = tpwworkinghours.cemployee_id
						AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
					WHERE cnode_id IS NOT null
					AND cdate BETWEEN to_date(:ZEITRAUM_VON, 'YYYY-MM-DD') AND to_date(:ZEITRAUM_BIS, 'YYYY-MM-DD')
					" . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
					$nodesIn
					$notiz_where
					GROUP BY to_char(cdate, 'YYYYMM')
					ORDER BY datum
				";
            }

            /*
             * SQL ausführen
             */
            $statement = $db->prepare($sql);
            $statement->bindValue(':ZEITRAUM_VON', $dateFrom->get(Iso_Date::ORACLE_DATE));
            $statement->bindValue(':ZEITRAUM_BIS', $dateUntil->get(Iso_Date::ORACLE_DATE));

            if ($profil->NOTIZTEXT != "") {
                $statement->bindValue(':NOTIZ_TEXT', '%' . $profil->NOTIZTEXT . '%');
            }

            $statement->execute();
            $hours = $statement->fetchAll();

            // for profiles with "cumulated" and day or month values a column with "0" has to added
            if ($profil->DIAGRAMM_TYP == 'TK' || $profil->DIAGRAMM_TYP == 'MK') {
                array_unshift($hours, ['DATUM' => 0, 'IST' => 0]);
            }

            // calculate start value if "cumulated" and "start value" was selected
            if ($profil->STARTWERT == 'J' && substr($profil->DIAGRAMM_TYP, 1, 1) == 'K') {
                $sql_startwert = "
					SELECT
						sum(coalesce(chours,0)) + sum(coalesce(choursal, 0)) AS ist
					FROM tpwworkinghours
					LEFT JOIN employment_period
					  ON employment_period.employee_id = tpwworkinghours.cemployee_id
					  AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
					LEFT JOIN employee_function_period
					  ON employee_function_period.employee_id = tpwworkinghours.cemployee_id
					  AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
					WHERE cdate < to_date(:ZEITRAUM_VON, 'YYYY-MM-DD')
					" . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
					$nodesIn
					$notiz_where
				";

                $stmt_startwert = $db->prepare($sql_startwert);
                $stmt_startwert->bindValue(':ZEITRAUM_VON', $dateFrom->get(Iso_Date::ORACLE_DATE));

                if ($profil->NOTIZTEXT != "") {
                    $stmt_startwert->bindValue(':NOTIZ_TEXT', '%' . $profil->NOTIZTEXT . '%');
                }
                $stmt_startwert->execute();

                $startwert = $stmt_startwert->fetchAll();
            }

            $mitarbeiter_info = ChartProfil::getUserAndGroupNamesOfProfile($profil->ID, $db);
            // Fetch explicit employees who booked hours in case no employees/groups was selected for profile
            if (empty($mitarbeiter_info['employees']) && empty($mitarbeiter_info['groups'])) {
                $sql = "
					SELECT
						DISTINCT
						lower(employee.shortcut) AS shortcut
					FROM tpwworkinghours
					LEFT JOIN employee ON tpwworkinghours.cemployee_id = employee.id
					LEFT JOIN employment_period
					  ON employment_period.employee_id = tpwworkinghours.cemployee_id
					  AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
					LEFT JOIN employee_function_period
					  ON employee_function_period.employee_id = tpwworkinghours.cemployee_id
					  AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
					WHERE cnode_id IS NOT null
					AND cdate BETWEEN to_date(:ZEITRAUM_VON, 'YYYY-MM-DD') AND to_date(:ZEITRAUM_BIS, 'YYYY-MM-DD')
					" . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
					$nodesIn
					$notiz_where
					order by shortcut
				";

                $statement = $db->prepare($sql);
                $statement->bindValue(':ZEITRAUM_VON', $dateFrom->get(Iso_Date::ORACLE_DATE));
                $statement->bindValue(':ZEITRAUM_BIS', $dateUntil->get(Iso_Date::ORACLE_DATE));
                if ($profil->NOTIZTEXT != "") {
                    $statement->bindValue(':NOTIZ_TEXT', '%' . $profil->NOTIZTEXT . '%');
                }

                $statement->execute();
                $mitarbeiter_info['employees'] = $statement->fetchAll(Zend_Db::FETCH_COLUMN);
            }

            $data[0]['IST']                = $hours;
            $data[0]['STARTWERT']        = $startwert;
            $data[0]['MITARBEITERINFO']    = $mitarbeiter_info;

            /*
             * 2. Retrieve data for single CC selected nodes
             */

            // get all single CC selections
            $sql = "
				SELECT single_node_id
				FROM bc_chart_profil_x_node
				WHERE bc_chart_profil_id = :PROFIL_ID
				AND single_node_id IS NOT NULL
				AND node_id IS NOT NULL
				AND set_id IS NULL
			";
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':PROFIL_ID', $profil->ID);
            $stmt->execute();
            $selectedccs = $stmt->fetchAll();

            /*
             * determine ACT hours (sums) per CC per day/month
             */
            // day values
            if (substr($profil->DIAGRAMM_TYP, 0, 1) == 'T') {
                $sql = "
					SELECT
						sum(coalesce(chours, 0)) + sum(coalesce(choursal, 0)) AS ist,
						date_to_unixts(cdate) AS datum
					FROM tpwworkinghours
					LEFT JOIN employment_period
					  ON employment_period.employee_id = tpwworkinghours.cemployee_id
					  AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
					LEFT JOIN employee_function_period
					  ON employee_function_period.employee_id = tpwworkinghours.cemployee_id
					  AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
					WHERE cdate BETWEEN to_date(:ZEITRAUM_VON, 'YYYY-MM-DD') AND to_date(:ZEITRAUM_BIS, 'YYYY-MM-DD')
					AND cnode_id = :CC
					" . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
					$notiz_where
					GROUP BY cdate
					ORDER BY cdate
				";
            } else {
                // month values
                $sql = "
					SELECT
						sum(coalesce(chours, 0)) + sum(coalesce(choursal, 0)) AS ist,
						to_char(cdate, 'YYYYMM') AS datum
					FROM tpwworkinghours
					LEFT JOIN employment_period
					  ON employment_period.employee_id = tpwworkinghours.cemployee_id
					  AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
					LEFT JOIN employee_function_period
					  ON employee_function_period.employee_id = tpwworkinghours.cemployee_id
					  AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
					WHERE cdate BETWEEN to_date(:ZEITRAUM_VON, 'YYYY-MM-DD') AND to_date(:ZEITRAUM_BIS, 'YYYY-MM-DD')
					AND cnode_id = :CC
					" . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
					$notiz_where
					GROUP BY to_char(cdate, 'YYYYMM')
					ORDER BY datum
				";
            }

            $statement = $db->prepare($sql);
            $statement->bindValue(':ZEITRAUM_VON', $dateFrom->get(Iso_Date::ORACLE_DATE));
            $statement->bindValue(':ZEITRAUM_BIS', $dateUntil->get(Iso_Date::ORACLE_DATE));
            if ($profil->NOTIZTEXT != "") {
                $statement->bindValue(':NOTIZ_TEXT', '%' . $profil->NOTIZTEXT . '%');
            }

            // calculate start value if "cumulated" and "start value" was selected
            $startwert = [];
            if ($profil->STARTWERT == 'J' && substr($profil->DIAGRAMM_TYP, 1, 1) == 'K') {
                $sql_startwert = "
					SELECT
						sum(coalesce(chours,0)) + sum(coalesce(choursal, 0)) AS ist
					FROM tpwworkinghours
					LEFT JOIN employment_period
					  ON employment_period.employee_id = tpwworkinghours.cemployee_id
					  AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
					LEFT JOIN employee_function_period
					  ON employee_function_period.employee_id = tpwworkinghours.cemployee_id
					  AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
					WHERE cdate < to_date(:ZEITRAUM_VON, 'YYYY-MM-DD')
					AND cnode_id = :CC
					" . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
					$notiz_where
				";

                $stmt_startwert = $db->prepare($sql_startwert);
                $stmt_startwert->bindValue(':ZEITRAUM_VON', $dateFrom->get(Iso_Date::ORACLE_DATE));
                if ($profil->NOTIZTEXT != "") {
                    $stmt_startwert->bindValue(':NOTIZ_TEXT', '%' . $profil->NOTIZTEXT . '%');
                }
            }

            // calculate ACT hours and start value for all single CCs
            foreach ($selectedccs as $selectedcc) {
                // ACT hours
                $statement->bindValue(':CC', $selectedcc['SINGLE_NODE_ID']);
                $statement->execute();
                $hourspercc = $statement->fetchAll();

                // start value
                if (isset($stmt_startwert)) {
                    $stmt_startwert->bindValue(':CC', $selectedcc['SINGLE_NODE_ID']);
                    $stmt_startwert->execute();
                    $startwert = $stmt_startwert->fetchAll();
                }

                $data[$selectedcc['SINGLE_NODE_ID']]['IST']       = $hourspercc;
                $data[$selectedcc['SINGLE_NODE_ID']]['STARTWERT'] = $startwert;
            }

            return ['DATA' => $data, 'FROM' => $dateFrom->get(Zend_Date::TIMESTAMP), 'UNTIL' => $dateUntil->get(Zend_Date::TIMESTAMP)];
        }

        return null;
    }

    /*
     * Chart erzeugen
     * Die Datenm muessen in ein verwendbares Format gewandelt werden und
     * evtl. vorhandene Luecken (keine Werte vorhanden) gefuellt werden.
     */
    public static function loadChart($profile, $timestamp, $db, $cache, $translate, $data)
    {
        $diagramm = null;

        Date::determinePeriod($profile->ID, $dateFrom, $dateUntil, $db);

        // Horizontale Beschriftung
        $xlabels = [];

        // add a zero plot point for line diagrams to support cases where only one day/month was selected
        // (line diagrams need at least 2 plot points to form the line)
        if (substr($profile->DIAGRAMM_TYP, 1, 1) === 'K') {
            $xlabels = [''];
        }

        // Bei tagesgenauen Werten wird eine X-Achsen Beschriftung benötigt,
        // die die einzelnen Tage wieder gibt.
        if (substr($profile->DIAGRAMM_TYP, 0, 1) == 'T') {
            $zeitpunkt = clone $dateFrom;
            while (!$zeitpunkt->isLater($dateUntil)) {
                $xlabels[] = $zeitpunkt->get('yyyy/MM/dd');
                $zeitpunkt->add(1, Zend_Date::DAY);
            }
        } else {
            $zeitpunkt = clone $dateFrom;
            $zeitpunkt->setDay(1);
            while (!$zeitpunkt->isLater($dateUntil)) {
                $xlabels[] = $zeitpunkt->get('yyyy/MM');
                $zeitpunkt->add(1, Zend_Date::MONTH);
            }
        }

        // Legende erzeugen
        // Alle Einzel-BKZ-Kuerzel laden, die vom User in dem Profil selektiert wurden.
        $sql = "
            SELECT node_version.name
            FROM bc_chart_profil_x_node
            INNER JOIN node_version
                ON node_version.node_id = bc_chart_profil_x_node.single_node_id
                AND node_version.version_state_id = 1
            WHERE bc_chart_profil_id = :PROFIL_ID
            AND bc_chart_profil_x_node.node_id IS NOT null
            AND bc_chart_profil_x_node.set_id IS null
        ";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':PROFIL_ID', $profile->ID);
        $stmt->execute();
        $ccs_label = array_merge(
            [$translate->translate('chartlinie')],
            $stmt->fetchAll(Zend_Db::FETCH_COLUMN)
        );

        // Diagrammform ermitteln und Daten hinzufügen
        $diagramm = null;

        // Diagrammtyp einzeln ist immer ein Balkendiagramm
        if (substr($profile->DIAGRAMM_TYP, 1, 1) == 'E') {
            $diagramm = new Iso_BarDiagramm($translate);

            $ccs_data = [];
            $summen = [];

            // Die Daten fuer die Einzel-Darstellung aufbereiten
            foreach ($data as $selectedccid) {
                // Bei Einzel sind nur die eigentlichen IST-Daten interessant.
                // Startwerte werden nicht benötigt!
                $ist = $selectedccid['IST'];

                $cc_data = [];
                $cc_counter = 0;
                $summe = 0;

                // Für jeden X-Wert muss ein Y-Wert gefunden werden. Wird kein
                // Wert in der Liste gefunden, muss 0 eingetragen werden.
                $numLabels = count($xlabels);
                for ($loop = 0; $loop < $numLabels; $loop++) {
                    // Es können nur solange IST-Werte verwendet werden, wie auch welche vorhanden sind.
                    // Zusätzlich sollen diese zu dem Datum (Tag bzw. Monat) passen.
                    if (count($ist) > $cc_counter && (
                            substr($profile->DIAGRAMM_TYP, 0, 1) == 'T' && $xlabels[$loop] == date('Y/m/d', $ist[$cc_counter]['DATUM']) ||
                            substr($profile->DIAGRAMM_TYP, 0, 1) == 'M' && str_replace('/', '', $xlabels[$loop]) == $ist[$cc_counter]['DATUM']
                        )
                    ) {
                        // Wert hinzufügen und Counter für IST-Array hochzählen
                        $value = General::toFloat($ist[$cc_counter]['IST']);

                        array_push($cc_data, $value);
                        $cc_counter++;
                    } else {
                        // Da kein passender Wert gefunden wurde - 0 eintragen.
                        $value = 0;

                        // Wert dem aktuellen BKZ hinzufügen
                        array_push($cc_data, $value);
                    }

                    $summe += $value;
                }

                // Daten des aktuellen BKZs den restlichen Daten hinzfügen
                array_push($ccs_data, $cc_data);
                array_push($summen, $summe);
            }

            // Daten und Beschriftung dem Diagramm hinzufügen
            $diagramm->setData($ccs_data, $ccs_label, $summen);

            // Update the Act-Hours value in DB
            ChartProfil::setActProfileValue($profile, $db, $summen[0]);
        } else {
            // Diagrammtyp kummuliert ist immer ein Liniendiagramm
            $diagramm = new Iso_LineDiagramm($translate);

            $ccs_data = [];
            $summen = [];

            // Wenn Zeitraum Bis über das aktuelle Datum hinausgeht, dann
            // soll beim aktuellen Tag / Monat eine vertikale Linie gezeichnet
            // werden
            $drawCurrentLine = false;

            $currentDate = Zend_Date::now()->setTime(0);
            if ($dateFrom->isEarlier($currentDate) && $dateUntil->isLater($currentDate)) {
                $drawCurrentLine = true;
            }

            // Die Daten für die Einzel-Darstellung aufbereiten
            foreach ($data as $selectedccid) {
                // Bei Auswahl kummuliert muss evtl. auch ein Startwert berücksichtigt werden.
                $ist = $selectedccid['IST'];
                $startwert = $selectedccid['STARTWERT'];

                // Wenn ein Startwert vorhanden ist, soll dieser verwendet werden, ansonsten 0.
                if ($startwert != null && is_array($startwert) &&  $startwert[0]['IST'] != null) {
                    $startwert = General::toFloat($startwert[0]['IST']);
                } else {
                    $startwert = 0;
                }

                $cc_data = [];
                $cc_counter = 0;
                $summe = 0;

                // Für jeden X-Wert muss ein Y-Wert gefunden werden. Wird kein
                // Wert in der Liste gefunden, muss 0 eingetragen werden.
                $numLabels = sizeof($xlabels);
                for ($loop = 0; $loop < $numLabels; $loop++) {
                    // Es können nur solange IST-Werte verwendet werden, wie auch welche vorhanden sind.
                    // Zusätzlich sollen diese zu dem Datum (Tag bzw. Monat) passen.
                    if (sizeof($ist) > $cc_counter && (
                            substr($profile->DIAGRAMM_TYP, 0, 1) == 'T' && $xlabels[$loop] == date('Y/m/d', $ist[$cc_counter]['DATUM']) ||
                            substr($profile->DIAGRAMM_TYP, 0, 1) == 'M' && str_replace('/', '', $xlabels[$loop]) == $ist[$cc_counter]['DATUM']
                        )
                    ) {
                        // Beim ersten Wert muss stets der Startwert mit berücksichtigt werden
                        // Bei den restlichen Werten soll stets der vorherige Wert dem aktuellen
                        // Wert hinzuaddiert werden.
                        if ($loop == 0) {
                            // Startwert addieren
                            $value = General::toFloat($ist[$cc_counter]['IST']) + $startwert;
                        } else {
                            // vorherigen Wert addieren
                            $value = General::toFloat($cc_data[$loop - 1]) + General::toFloat($ist[$cc_counter]['IST']);
                        }
                        $summe += General::toFloat($ist[$cc_counter]['IST']);
                        array_push($cc_data, $value);
                        $cc_counter++;
                    } else {
                        // Da kein passender Wert gefunden wurde, muss beim ersten Wert, der Startwert verwendet werden.
                        // Bei den restlichen Werten der vorherige Wert.
                        if ($loop == 0) {
                            // Startwert verwenden
                            $value = $startwert;

                            // Bei kumulierten Tages-/Monatswerten ist der 0te Tag/Monat ebenfalls ein
                            // gueltiges BKZ, deswegen muss hier der Counter erhoeht werden
                            if (($profile->DIAGRAMM_TYP == 'TK' || $profile->DIAGRAMM_TYP == 'MK')  && sizeof($ist) > 0 && $ist[$cc_counter]['DATUM'] == 0) {
                                $cc_counter++;
                            }
                        } elseif (isset($cc_data[$loop - 1])) {
                            //vorherigen Wert verwenden
                            $value = $cc_data[$loop - 1];
                        } else {
                            $value = null;
                        }

                        // Wert dem aktuellen BKZ hinzufügen
                        array_push($cc_data, $value);
                    }

                    // vertikale Linie zeichnen, bei aktuellem Monat oder Tag
                    if ($drawCurrentLine == true && (
                            substr($profile->DIAGRAMM_TYP, 0, 1) == 'T' && $xlabels[$loop] == date('Y/m/d')
                            || substr($profile->DIAGRAMM_TYP, 0, 1) == 'M' && str_replace('/', '', $xlabels[$loop]) == date('Ym')
                        )
                    ) {
                        $diagramm->setCurrentLine($loop);

                        // Der Wert muss nur einmal gesetzt werden
                        $drawCurrentLine = false;
                    }
                }

                // aktuelles BKZ den Daten hinzufuegen
                array_push($ccs_data, $cc_data);
                array_push($summen, $summe);
            }

            // Daten und Beschriftung dem Diagramm hinzufügen
            $diagramm->setData($ccs_data, $ccs_label, $summen);

            // Update the Act-Hours value in DB
            ChartProfil::setActProfileValue($profile, $db, $summen[0]);
        }

        // nur wenn ein Diagramm-Objekt erzeugt wurde fortfahren
        if ($diagramm != null) {
            // Beschriftung der X-Achse hinzufügen
            if ((substr($profile->DIAGRAMM_TYP, 0, 1) == 'T')) {
                $numLabels = count($xlabels);
                for ($i = 0; $i < $numLabels; $i++) {
                    $xlabels[$i] = substr($xlabels[$i], 5, 5);
                }
            }
            $diagramm->setXLabels($xlabels);

            // Limit generieren, wenn Wert vorhanden ist
            if (isset($profile->LIMIT) && $profile->LIMIT != null) {
                if (empty($data[0]['STARTWERT'])) {
                    $limit_startwert = 0;
                } else {
                    $limit_startwert = General::toFloat($data[0]['STARTWERT'][0]['IST']);
                }

                // die einzelnen Punkte für den Limit - Graphen berechnen
                $limit_value = General::toFloat($profile->LIMIT);

                // Bei Auswahl "einzeln" wird eine Graph: y = limit
                // benötigt. (horizontale Linie)
                if (substr($profile->DIAGRAMM_TYP, 1, 1) == 'E') {
                    $limit = $limit_value;
                } else {
                    // Bei Auswahl "kummuliert" wird eine Graph: y = limit * x
                    // benoetigt. (schräg ansteigende Linie)
                    $limit = [];

                    // bei kumulierten Werten fängt die X-Achse mit 0 an,
                    // der zugehörige Y - Wert ist dann entweder 0 oder der Startwert
                    if ($limit_startwert != null) {
                        array_push($limit, $limit_startwert);
                    } else {
                        array_push($limit, 0);
                    }

                    $numLabels = count($xlabels);
                    for ($loop = 1; $loop < $numLabels; $loop++) {
                        $faktor = $loop / (sizeof($xlabels) - 1);

                        if ($limit_startwert != null) {
                            array_push($limit, $limit_value * $faktor + $limit_startwert);
                        } else {
                            array_push($limit, $limit_value * $faktor);
                        }
                    }
                }

                $diagramm->setLimit($limit);
            }

            // save generated image in cache
            $cacheId = $profile->ID . '_' . $timestamp;
            $cache->save($diagramm, $cacheId);
        }

        return $summen[0];
    }

    public static function loadEmployeeBooking($profile, $timestamp, $db)
    {
        // Determine period of a profile
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);
        $yesterday = Zend_Date::now()
            ->setTimezone('UTC')
            ->setTime(0)
            ->sub(1, Zend_Date::DAY);

        // cut off all future dates (if necessary)
        if ($yesterday->isEarlier($periodUntil)) {
            $periodUntil->set($yesterday);
        }

        // Determine profile companies
        $companies = ChartProfil::getProfileCompanies($profile->ID);
        // Determine profile divisions
        $divisions = ChartProfil::getProfileDivisions($profile->ID);
        // Determine the selected users
        $employees = ChartProfil::getProfileEmployees($db, $profile->ID);

        $userAndGroupIds = self::getUserAndGroupIdsOfProfile($profile->ID, $db);

        // if resolved employee selection results in no employees at all (e.g. due to empty groups), then report has no data
        if (!empty($userAndGroupIds) && self::noEmployeeSelected($profile->ID, [
                "companies" => $companies,
                "divisions" => $divisions,
                "employees" => $employees['id']
            ])
        ) {
            $employees['id'] = [Employee::DUMMY];
        }

        $companiesWhere = '';
        $divisionsWhere = '';
        $employeesWhere = '';
        if (!empty($companies)) {
            $companiesWhere = "employment_period.company_id IN (" . implode(',', $companies) . ")";
        }

        if (!empty($divisions)) {
            $divisionsWhere = "employee_function_period.division_id IN (" . implode(',', $divisions) . ")";
        }

        if (!empty($employees['id'])) {
            $employeesWhere = QueryBuilder::createInClause('employee.id', $employees['id']);
        }

        $sql = "
            WITH days AS (
                SELECT days.day::date AS dt
                FROM generate_series(to_date(:PERIOD_FROM, 'YYYY-MM-DD')::date, to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')::date, '1 day') days(day)
            ),
            holidays AS (SELECT cdate, clocation_id FROM tpwpublicholidays JOIN days ON dt = cdate)
			SELECT
			  employee.shortcut,
			  to_char(days.dt, 'DD.MM.YYYY') AS dt
			FROM days
			CROSS JOIN employee
			LEFT JOIN employment_period
			  ON employment_period.employee_id = employee.id
			  AND days.dt BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
			LEFT JOIN employee_function_period
			  ON employee_function_period.employee_id = employee.id
			  AND days.dt BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
			LEFT JOIN location ON location.id = employment_period.location_id
			LEFT JOIN (
			  SELECT
			    cemployee_id,
			    cdate AS dt
			  FROM tpwworkinghours
			  WHERE cnode_id IS NOT null
			  AND cdate BETWEEN to_date(:PERIOD_FROM::text, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL::text, 'YYYY-MM-DD')
			  AND (cnode_id = 0 OR coalesce(choursal,0) + coalesce(chours,0) > 0)
			  GROUP BY cemployee_id, cdate
			) tpwworkinghours
			  ON tpwworkinghours.cemployee_id = employee.id
			  AND tpwworkinghours.dt = days.dt
			-- employment check
			WHERE employment_period.id IS NOT null AND employment_period.hours_per_week > 0
			-- weekend check
			AND (
			  (location.id IS null OR location.name != 'Dubai') AND to_char(days.dt, 'D') NOT IN('1','7')
			  OR location.name = 'Dubai' AND to_char(days.dt, 'D') NOT IN ('7','6')
			)
			-- holidays check
            AND NOT EXISTS (SELECT null from holidays WHERE clocation_id = employment_period.location_id AND cdate = days.dt)
			-- booking check
			AND tpwworkinghours.dt IS null
			" . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
			ORDER BY employee.shortcut, days.dt
		";

        $stmt = $db->query($sql, [
            'PERIOD_FROM'    => $periodFrom->get(Iso_Date::ORACLE_DATE),
            'PERIOD_UNTIL'    => $periodUntil->get(Iso_Date::ORACLE_DATE)
        ]);
        $result = $stmt->fetchAll();

        $tmpBookings = [];
        $emptyBookings = [];
        foreach ($result as $item) {
            $tmpBookings[$item['SHORTCUT']][] = $item['DT'];
        }
        foreach ($tmpBookings as $shortcut => $dates) {
            $emptyBookings[] = [
                'employee'                => $shortcut,
                'emptyBookingDates'    => implode(', ', $dates)
            ];
        }

        // Fetch the user shortcuts from DB (if there are any)
        $booked = ChartProfil::getUserAndGroupNamesOfProfile($profile->ID, $db);

        return [
            'BOOKINGS'        => ['emptyBookings' => $emptyBookings],
            'FROM'            => $periodFrom->get(Zend_Date::TIMESTAMP),
            'UNTIL'            => $periodUntil->get(Zend_Date::TIMESTAMP),
            'EMPLOYEEINFO'    => ['booked' => $booked]
        ];
    }

    /**
     * @param $config
     * @param $log
     * @param $userToken
     * @return array
     */
    private static function getGrantedJiraProjects($config, $log, $userToken): array
    {
        $grantedJiraProjects = [];
        $jiraDataRetriever = new Jira_FetchRoles($config, $log);
        $projectRoles = $jiraDataRetriever->fetch($userToken);

        if ($projectRoles !== null) {
            foreach ($projectRoles->getRoles() as $projectRole) {
                $grantedJiraProjects[] = $projectRole->getKey();
            }
        }

        return $grantedJiraProjects;
    }

    /**
     * Retrieves data for the report type 'List'
     */
    public static function loadList($profile, $timestamp, $db, $cache, $translate, $config, $log, $userToken)
    {
        // Determine period of this profile
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);

        //Determine leaves of profile (section beams and sets) as in statement
        $nodesIn = ChartProfil::getLeafesOfProfile($profile->ID, $db);

        // Determine profile companies
        $companies = ChartProfil::getProfileCompanies($profile->ID);
        // Determine profile divisions
        $divisions = ChartProfil::getProfileDivisions($profile->ID);
        // Determine the selected users
        $employees = ChartProfil::getProfileEmployees($db, $profile->ID);

        $userAndGroupIds = self::getUserAndGroupIdsOfProfile($profile->ID, $db);

        // if resolved employee selection results in no employees at all (e.g. due to empty groups), then report has no data
        if (!empty($userAndGroupIds) && self::noEmployeeSelected($profile->ID, [
                "companies" => $companies,
                "divisions" => $divisions,
                "employees" => $employees['id']
            ])
        ) {
            $employees['id'] = [Employee::DUMMY];
        }

        $companiesWhere = '';
        $divisionsWhere = '';
        $employeesWhere = '';
        $employeesFromBookings = [];
        $remoteWorkReport = $profile->ONLY_REMOTE_WORK_DAYS == 1;
        if (!empty($companies)) {
            $companiesWhere = "employment_period.company_id IN (" . implode(',', $companies) . ")";
        }

        if (!empty($divisions)) {
            $divisionsWhere = "employee_function_period.division_id IN (" . implode(',', $divisions) . ")";
        }

        if (!empty($employees['id'])) {
            $columnName = $remoteWorkReport ? 'absence.employee_id' : 'tpwworkinghours.cemployee_id';
            $employeesWhere = QueryBuilder::createInClause($columnName, $employees['id']);
        }

        $note_where = '';
        if (!$remoteWorkReport && $profile->NOTIZTEXT != "") {
            $note_where = " AND (upper(tpwworkinghours.cnote) LIKE upper(:NOTIZ_TEXT) OR
                                 upper(tpwworkinghours.cnoteal) LIKE upper(:NOTIZ_TEXT)) ";
        }

        $beginEndWhere = '';
        if ($profile->BOOKINGS_WITH_BEGIN_END == 1) {
            $beginEndWhere = " AND tpwworkinghours.cbegin IS NOT null AND tpwworkinghours.cend IS NOT null ";
        }

        // additional columns
        $profileAdditionalCols = ChartProfil::getAdditionalColumns($profile->ID);
        //$extraCols = !empty($monthEndCols);
        $additionalColsObj = new AdditionalCol();
        $additionalCols = [];
        foreach ($additionalColsObj->getAdditionalCols($translate->getLocale(), ReportType::LISTE) as $col) {
            if (in_array((int)$col['ID'], $profileAdditionalCols)) {
                $additionalCols[(int)$col['ID']] = $col;
            }
        }

        // remove unused additional columns of remote work report
        if ($remoteWorkReport) {
            unset(
                $additionalCols[AdditionalCol::LIST_BREAK],
                $additionalCols[AdditionalCol::LIST_BEGIN_END],
                $additionalCols[AdditionalCol::LIST_FROM_TILL]
            );
        }

        $addColSqlSelect = "";
        $addColSqlJoin = "";

        if (!empty($additionalCols)) {
            if (isset($additionalCols[AdditionalCol::LIST_COMPANY])) {
                $addColSqlSelect .= ", company.shortcut AS company_name";
                $addColSqlJoin .= " LEFT JOIN company ON company.id = employment_period.company_id";
            }

            if (isset($additionalCols[AdditionalCol::LIST_DIVISION])) {
                $addColSqlSelect .= ", division.name || ' -- ' || division.long_name AS division_name";
                $addColSqlJoin .= " LEFT JOIN division ON division.id = employee_function_period.division_id";
            }

            if (isset($additionalCols[AdditionalCol::LIST_FROM_TILL])
                || isset($additionalCols[AdditionalCol::LIST_BREAK])) {
                if (isset($additionalCols[AdditionalCol::LIST_FROM_TILL])) {
                    $addColSqlSelect .= "
                        , to_char(tpwworkperiod.ctimestart, 'HH24:MI') AS presence_from
                        , to_char(tpwworkperiod.ctimeend, 'HH24:MI') AS presence_to
                    ";
                }
                if (isset($additionalCols[AdditionalCol::LIST_BREAK])) {
                    $addColSqlSelect .= "
                        , tpwworkperiod.cbreak_time AS break
                    ";
                }
                $addColSqlJoin .= "
                    LEFT JOIN tpwworkperiod
                        ON tpwworkperiod.cemployee_id = tpwworkinghours.cemployee_id
                        AND tpwworkperiod.cdate = tpwworkinghours.cdate
                ";
            }

            if (isset($additionalCols[AdditionalCol::LIST_DAY_TYPE])) {
                $columnName = $remoteWorkReport ? 'absence.start_date' : 'tpwworkinghours.cdate';
                $addColSqlSelect .= "
                    , CASE
                        WHEN tpwpublicholidays.cdate IS NOT null OR to_char($columnName, 'ID') = '7' THEN 3
                        WHEN to_char($columnName, 'ID') = '6' THEN 2
                        ELSE 1
                    END AS day_type
                ";
                $addColSqlJoin .= "
                    LEFT JOIN tpwpublicholidays
                        ON tpwpublicholidays.clocation_id = employment_period.location_id
                        AND tpwpublicholidays.cdate = $columnName
                ";
            }

            if (isset($additionalCols[AdditionalCol::LIST_BEGIN_END])) {
                $addColSqlSelect .= "
                    , to_char(tpwworkinghours.cbegin, 'HH24:MI') AS booking_begin
                    , to_char(tpwworkinghours.cend, 'HH24:MI') AS booking_end
                ";
            }
        }

        if ($remoteWorkReport) {
            // Determine the remote work days
            $sql = "
                SELECT
                    employee.shortcut,
                    employee.first_name,
                    employee.last_name,
                    to_char(absence.start_date, 'YYYY-MM-DD') AS cdate,
  		            CASE WHEN absence.start_time IS NOT NULL OR absence.end_time IS NOT NULL 
		                THEN format('" . $translate->translate('remoteWorkFromTo') . "', to_char(absence.start_time, 'HH24:MI'), to_char(absence.end_time, 'HH24:MI'))
		                ELSE '" . $translate->translate('remoteWorkWholeDay') . "'
		            END AS cnote           
                    $addColSqlSelect
                FROM absence
                INNER JOIN absence_category ON
                absence_category.id = absence.category_id
                INNER JOIN employee ON
                absence.employee_id = employee.id
                LEFT JOIN employment_period ON
                employment_period.employee_id = employee.id
                -- Homeoffice absence periods always last one day only
                AND absence.start_date BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                LEFT JOIN employee_function_period ON
                employee_function_period.employee_id = absence.employee_id
                AND absence.start_date BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
                $addColSqlJoin
                WHERE absence_category.name_de = 'Homeoffice'
                AND absence.start_date BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')         
                " . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) .
                "ORDER BY employee.shortcut, absence.start_date";
        } else {
            // Determine the bookings
            $sql = "
                SELECT
                    employee.shortcut,
                    employee.first_name,
                    employee.last_name,
                    node_version.name,
                    to_char(tpwworkinghours.cdate, 'YYYY-MM-DD') AS cdate,
                    tpwworkinghours.chours,
                    tpwworkinghours.choursal,
                    tpwworkinghours.cnote,
                    tpwworkinghours.cnoteal,
                    booking_type.name AS booking_type
                    $addColSqlSelect
                FROM tpwworkinghours
                INNER JOIN node ON tpwworkinghours.cnode_id = node.id
                INNER JOIN node_version ON node_version.node_id = node.id AND node_version.version_state_id = 1
                INNER JOIN employee ON tpwworkinghours.cemployee_id = employee.id
                INNER JOIN booking_type ON tpwworkinghours.cbooking_type_id = booking_type.id
                LEFT JOIN employment_period ON
                    employment_period.employee_id = employee.id
                    AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                LEFT JOIN employee_function_period ON
                    employee_function_period.employee_id = tpwworkinghours.cemployee_id
                    AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
                $addColSqlJoin
                WHERE tpwworkinghours.cdate BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
                " . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
                $nodesIn
                $note_where
                $beginEndWhere
                ORDER BY tpwworkinghours.cdate, employee.shortcut, node_version.name
            ";
        }

        $statement = $db->prepare($sql);
        $statement->bindValue(':PERIOD_FROM', $periodFrom->get(Iso_Date::ORACLE_DATE));
        $statement->bindValue(':PERIOD_UNTIL', $periodUntil->get(Iso_Date::ORACLE_DATE));
        if (!$remoteWorkReport && $profile->NOTIZTEXT != "") {
            $statement->bindValue(':NOTIZ_TEXT', '%' . $profile->NOTIZTEXT . '%');
        }

        if ($statement->execute()) {

            $rows = $statement->fetchAll();
            $grantedJiraProjects = self::getGrantedJiraProjects($config, $log, $userToken);

            $summe = 0;
            $size = count($rows);
            $toRemove = [];

            if (!$remoteWorkReport) {
                for ($loop = 0; $loop < $size; $loop++) {
                    $rows[$loop]['CHOURS'] = General::toFloat($rows[$loop]['CHOURS']);
                    $rows[$loop]['CHOURSAL'] = General::toFloat($rows[$loop]['CHOURSAL']);
                    $rows[$loop]['CNOTE'] = stripslashes(($rows[$loop]['CNOTE']));

                    $note = "";
                    if (!empty($rows[$loop]['CNOTE']) && !empty($rows[$loop]['CNOTEAL'])) {
                        $note = $rows[$loop]['CNOTE'] . ', ' . $rows[$loop]['CNOTEAL'];
                    } elseif (!empty($rows[$loop]['CNOTE'])) {
                        $note = $rows[$loop]['CNOTE'];
                    } elseif (!empty($rows[$loop]['CNOTEAL'])) {
                        $note = $rows[$loop]['CNOTEAL'];
                    }

                    $rows[$loop]['CNOTE'] = $note;

                    $hours = 0;
                    if (!empty($rows[$loop]['CHOURS']) && !empty($rows[$loop]['CHOURSAL'])) {
                        $hours = $rows[$loop]['CHOURS'] + $rows[$loop]['CHOURSAL'];
                    } elseif (!empty($rows[$loop]['CHOURS'])) {
                        $hours = $rows[$loop]['CHOURS'];
                    } elseif (!empty($rows[$loop]['CHOURSAL'])) {
                        $hours = $rows[$loop]['CHOURSAL'];
                    }

                    if (floatval($hours) == 0.0) {
                        array_push($toRemove, $loop);
                        continue;
                    }

                    $rows[$loop]['CHOURS'] = $hours;
                    $summe += $hours;

                    $rows[$loop]['BKZ'] = htmlentities($rows[$loop]['NAME']);

                    $rows[$loop]['PDBIDS'] = General::extractJiraIds($rows[$loop]['CNOTE'], $grantedJiraProjects);

                    array_push($employeesFromBookings, strtolower($rows[$loop]['SHORTCUT']));
                }
                // Remove zero hours items
                foreach ($toRemove as $item) {
                    unset($rows[$item]);
                }
            }
            $rows = array_values($rows);

            // Update the Act-Hours value in DB
            ChartProfil::setActProfileValue($profile, $db, $summe);

            // Fetch the user shortcuts from DB (if there are any)
            $booked = ChartProfil::getUserAndGroupNamesOfProfile($profile->ID, $db);
            if (empty($booked['employees']) && empty($bookied['groups']) && !empty($employeesFromBookings)) {
                $booked['employees'] = array_unique($employeesFromBookings);
                sort($booked['employees']);
            }

            // Fetch the selected nodes from DB (if there are any)
            $nodes = ChartProfil::getNodesOfProfile($profile->ID, $db);

            // save report data in cache
            $cacheId = $profile->ID . '_' . $timestamp;
            $cache->save([
                'REPORT_INFO' => [
                    'TYPE' => $profile->ANZEIGE,
                    'EMPLOYEES' => isset($booked) ? $booked : '',
                    'NODES' => $nodes,
                    'PERIODFROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
                    'PERIODUNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP),
                    'REMOTE_WORK_REPORT' => $remoteWorkReport
                ],
                'DATA' => $rows,
                'ADDITIONAL_COLS' => $additionalCols,
                'order' => SORT_ASC,
                'sort_column' => 'CDATE'
            ], $cacheId);

            return [
                'DATA' => ['result' => true, 'liste' => $rows, 'order' => SORT_ASC, 'sort_column' => 'CDATE'],
                'EMPLOYEEINFO' => ['booked' => isset($booked) ? $booked : ''],
                'FROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
                'UNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP),
                'ACTHOURS' => $summe
            ];
        } else {
            return [
                'DATA' => ['result' => false, 'liste' => ''],
                'EMPLOYEEINFO' => '',
                'FROM' => $periodFrom,
                'UNTIL' => $periodUntil
            ];
        }
    }

    public static function loadTable($profil, $timestamp, $db, $cache, $translate)
    {
        // Determine period of this profile
        Date::determinePeriod($profil->ID, $periodFrom, $periodUntil, $db);

        $profileNodes = self::getProfileNodes($profil->ID, $db, false);

        //Determine leafes of profile (nodes and sets)
        $leafNodes = self::getLeafesOfProfile($profil->ID, $db, true);

        // Determine profile companies
        $companies = self::getProfileCompanies($profil->ID);
        // Determine profile divisions
        $divisions = self::getProfileDivisions($profil->ID);
        // Determine the selected users
        $employees = self::getProfileEmployees($db, $profil->ID);

        $userAndGroupIds = self::getUserAndGroupIdsOfProfile($profil->ID, $db);

        // if resolved employee selection results in no employees at all (e.g. due to empty groups), then report has no data
        if (!empty($userAndGroupIds) && self::noEmployeeSelected($profil->ID, [
                "companies" => $companies,
                "divisions" => $divisions,
                "employees" => $employees['id']
            ])
        ) {
            $employees['id'] = [Employee::DUMMY];
        }

        $companiesWhere = '';
        $divisionsWhere = '';
        $employeesWhere = '';
        $employeesFromBookings = [];
        if (!empty($companies)) {
            $companiesWhere = "employment_period.company_id IN (" . implode(',', $companies) . ")";
        }

        if (!empty($divisions)) {
            $divisionsWhere = "employee_function_period.division_id IN (" . implode(',', $divisions) . ")";
        }

        if (!empty($employees['id'])) {
            $employeesWhere = QueryBuilder::createInClause('cemployee_id', $employees['id']);
        }

        if (empty($profil->NOTIZTEXT)) {
            $bookingNoteWhere = " ";
        } else {
            $bookingNoteWhere = " and (lower(tpwwh.cnote) like lower('%" . $profil->NOTIZTEXT . "%') or lower(tpwwh.cnoteal) like lower('%" . $profil->NOTIZTEXT . "%')) ";
        }

        $ccWithHourBookings = $profil->CC_WITH_HOUR_BOOKINGS == 1 ? true : false;
        $xAxis = $profil->X_AXIS;
        $displayNodes = self::getDisplayNodes($profil->ID, $db);

        /*
        If root node 'ISO-Gruppe' is selected and no leaf nodes
        (means all nodes because if root is selected then sql in clause for nodes can be ommitted - performance reason)
        then make booking hours select statement valid (include all CCs)
        */
        $nodeCondition = " 1 = 0 ";
        if (sizeof($profileNodes) == 1 && empty($leafNodes)) {
            $nodeCondition = " 1 = 1 ";
        }

        $ccWithHourBookingsUnion = " ";
        if (!$ccWithHourBookings) {
            $ccWithHourBookingsUnion = "
                union all
                select
                    null as shortcut,
                    null as last_name,
                    null as first_name,
                    null as sap_material_number,
                    null as booking_date,
                    null as month,
                    null as chours,
                    null as choursal,
                    node.id as node_id,
                    node_version.name as node_name,
                    node_version.target_effort,
                    cc_with_sap_key.sap_key
                FROM node
                INNER JOIN node_version
                    ON node_version.node_id = node.id
                    AND node_version.version_state_id = 1
                LEFT JOIN cc_with_sap_key ON node.id = cc_with_sap_key.node_id
                WHERE node.is_leaf = 1 " . (empty($leafNodes) ? (empty($displayNodes['id']) ? " " : " AND " . $nodeCondition) : " AND " . QueryBuilder::createInClause('node.id', $leafNodes)) . "
            ";
        }

        $orderBy = " shortcut ";
        if ($xAxis == 'M') {
            $orderBy = " booking_date ";
        }

        $sqlBookings = "
            SELECT
                employee.shortcut,
                employee.last_name,
                employee.first_name,
                employee.sap_material_number,
                tpwwh.cdate as booking_date,
                to_char(tpwwh.cdate, 'YYYY/MM') as month,
                tpwwh.chours,
                tpwwh.choursal,
                node.id as node_id,
                node_version.name as node_name,
                node_version.target_effort,
                cc_with_sap_key.sap_key
            FROM node
            INNER JOIN node_version
                ON node_version.node_id = node.id
                AND node_version.version_state_id = 1
            INNER JOIN tpwworkinghours tpwwh ON node.id = tpwwh.cnode_id
            INNER JOIN employee on tpwwh.cemployee_id = employee.id
            LEFT JOIN employment_period ON
                employment_period.employee_id = employee.id
                AND tpwwh.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
            LEFT JOIN employee_function_period ON
                employee_function_period.employee_id = tpwwh.cemployee_id
                AND tpwwh.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
            LEFT JOIN cc_with_sap_key ON node.id = cc_with_sap_key.node_id
            WHERE (coalesce(tpwwh.chours,0) + coalesce(tpwwh.choursal,0)) > 0
            AND tpwwh.cdate BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD') "
            . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
        ";

        $sql =
            $sqlBookings
            . (empty($leafNodes) ? (empty($displayNodes['id']) ? " " : " AND " . $nodeCondition) : " AND " . QueryBuilder::createInClause('cnode_id', $leafNodes)) . "
            $bookingNoteWhere
            $ccWithHourBookingsUnion
            ORDER BY $orderBy
        ";

        $statement = $db->prepare($sql);
        $statement->bindValue(':PERIOD_FROM', $periodFrom->get(Iso_Date::ORACLE_DATE));
        $statement->bindValue(':PERIOD_UNTIL', $periodUntil->get(Iso_Date::ORACLE_DATE));
        $statement->execute();
        $bookings = $statement->fetchAll();

        $data = [
            'header' => [
                'isHeader' => true,
                'name' => '(' . $translate->translate('std') . ')',
                'sapKey' => $translate->translate('sapKey'),
                'total' => $translate->translate('summe'),
                'targetEffort' => $translate->translate('abbrTargetHours'),
                'percent' => $translate->translate('prozent'),
                'xAxisData' => []
            ]
        ];

        // Create header row
        foreach ($bookings as $item) {
            if (is_null($item['SHORTCUT']) || is_null($item['MONTH'])) {
                continue;
            }

            switch ($xAxis) {
                case 'E':
                    $data['header']['xAxisData'][$item['SHORTCUT']] = [
                        'shortcut' => $item['SHORTCUT'],
                        'firstName' => $item['FIRST_NAME'],
                        'lastName' => $item['LAST_NAME'],
                        'sapMaterialNumber' => $item['SAP_MATERIAL_NUMBER']
                    ];
                    break;
                case 'M':
                    $data['header']['xAxisData'][$item['MONTH']] = [
                        'month' => $item['MONTH']
                    ];
                    break;
            }

            array_push($employeesFromBookings, $item['SHORTCUT']);
        }

        $xAxisKeys = array_keys($data['header']['xAxisData']);
        foreach ($bookings as $item) {
            if (!isset($data[$item['NODE_ID']])) {
                $data[$item['NODE_ID']] = [
                    'name' => '(' . $translate->translate('abkbkz') . ') ' . $item['NODE_NAME'],
                    'sapKey' => $item['SAP_KEY'],
                    'total' => [
                        'hours' => 0.0,
                        'hoursAL' => 0.0,
                        'isTotal' => true
                    ],
                    'targetEffort' => empty($item['TARGET_EFFORT']) ? 0.0 : General::toFloat($item['TARGET_EFFORT']),
                    'percent' => 0,
                    'xAxisData' => []
                ];

                foreach ($xAxisKeys as $xAxisKey) {
                    $data[$item['NODE_ID']]['xAxisData'][$xAxisKey] = [
                        'hours' => 0.0,
                        'hoursAL' => 0.0
                    ];
                }
            }

            $key = $item['SHORTCUT'];
            if ($xAxis == 'M') {
                $key = $item['MONTH'];
            }

            if (is_null($key)) {
                continue;
            }

            $xAxisItem = &$data[$item['NODE_ID']]['xAxisData'][$key];
            $xAxisItem['hours'] += General::toFloat($item['CHOURS']) + General::toFloat($item['CHOURSAL']);
            $xAxisItem['hoursAL'] += General::toFloat($item['CHOURSAL']);

            unset($xAxisItem);

            array_push($employeesFromBookings, $item['SHORTCUT']);
        }

        // Get node keys without 'header' key
        $ccNodeKeys = array_slice(array_keys($data), 1);
        $renderTotalRow = true;

        // Detect which display cc nodes are child nodes of display nodes
        foreach (array_keys($displayNodes['id']) as $displayNode) {
            foreach ($ccNodeKeys as $nodeKeyId) {
                if (General::isChildOfNode($nodeKeyId, $displayNode)) {
                    $renderTotalRow = false;
                    break;
                }
            }
        }

        // Detect which display nodes have parent->child relation (must be skipped in sum calculation)
        $displayNodes['isChild'] = [];
        foreach (array_keys($displayNodes['id']) as $displayNodeChild) {
            foreach (array_keys($displayNodes['id']) as $displayNodeParent) {
                if ($displayNodeChild == $displayNodeParent) {
                    continue;
                }
                if (!isset($displayNodes['isChild'][$displayNodeChild])) {
                    $displayNodes['isChild'][$displayNodeChild] = false;
                }

                if (!$displayNodes['isChild'][$displayNodeChild] && General::isChildOfNode($displayNodeChild, $displayNodeParent)) {
                    $displayNodes['isChild'][$displayNodeChild] = true;
                    $renderTotalRow = false;
                }
            }
        }

        // Add display nodes
        foreach ($displayNodes['id'] as $displayNode => $nodeData) {
            $displayNodeTargetEffort = $nodeData['TARGET_EFFORT'];
            if (!isset($data[$displayNode])) {
                $data[$displayNode] = [
                    'isTotal' => true,
                    'name' => $displayNodes['name'][$displayNode],
                    'sapKey' => $displayNodes['sapKey'][$displayNode],
                    'total' => [
                        'hours' => 0.0,
                        'hoursAL' => 0.0,
                        'isTotal' => true
                    ],
                    'targetEffort' => 0.0,
                    'percent' => 0,
                    'xAxisData' => []
                ];

                foreach ($xAxisKeys as $xAxisKey) {
                    $data[$displayNode]['xAxisData'][$xAxisKey] = [
                        'hours' => 0.0,
                        'hoursAL' => 0.0,
                        'isTotal' => true
                    ];
                }
            }

            // Get bookings for each display node
            $sql =
                $sqlBookings
                . (empty($displayNodes['cc'][$displayNode]) ? " AND 1 = 0 " : " AND " . QueryBuilder::createInClause('cnode_id', array_column($displayNodes['cc'][$displayNode], 'ID'))) . "
                $bookingNoteWhere
                ORDER BY $orderBy
            ";

            $statement = $db->prepare($sql);
            $statement->bindValue(':PERIOD_FROM', $periodFrom->get(Iso_Date::ORACLE_DATE));
            $statement->bindValue(':PERIOD_UNTIL', $periodUntil->get(Iso_Date::ORACLE_DATE));
            $statement->execute();
            $result = $statement->fetchAll();

            $targetEfforts = [];

            foreach ($result as $item) {
                $key = $item['SHORTCUT'];
                if ($xAxis == 'M') {
                    $key = $item['MONTH'];
                }

                // Check if key is existing
                if (!isset($data[$displayNode]['xAxisData'][$key])) {
                    $data[$displayNode]['xAxisData'][$key] = [
                        'hours' => 0.0,
                        'hoursAL' => 0.0,
                        'isTotal' => true
                    ];
                }

                // Update header data
                if (!isset($data['header']['xAxisData'][$key])) {
                    $header = [
                        'shortcut' => $item['SHORTCUT'],
                        'firstName' => $item['FIRST_NAME'],
                        'lastName' => $item['LAST_NAME'],
                        'sapMaterialNumber' => $item['SAP_MATERIAL_NUMBER']
                    ];

                    if ($xAxis == 'M') {
                        $header = [
                            'month' => $item['MONTH']
                        ];
                    }

                    $data['header']['xAxisData'][$key] = $header;

                    // Update xAxisKeys
                    $xAxisKeys = array_keys($data['header']['xAxisData']);
                }

                // Update display cc data
                foreach ($ccNodeKeys as $nodeId) {
                    if (!isset($data[$nodeId]['xAxisData'][$key])) {
                        $data[$nodeId]['xAxisData'][$key] = [
                            'hours' => 0.0,
                            'hoursAL' => 0.0
                        ];
                    }
                }

                // Update other display nodes (may have different key values)
                foreach ($data as $nodeKey => &$node) {
                    if ($nodeKey == 'header' || $nodeKey == 'total' || !isset($node['isTotal']) || false == $node['isTotal']) {
                        continue;
                    }

                    if (!isset($node['xAxisData'][$key])) {
                        $node['xAxisData'][$key] = [
                            'hours' => 0.0,
                            'hoursAL' => 0.0,
                            'isTotal' => true
                        ];
                    }
                    unset($node);
                }


                $targetEfforts[$item['NODE_ID']] = empty($item['TARGET_EFFORT']) ? 0.0 : General::toFloat($item['TARGET_EFFORT']);

                $xAxisItem = &$data[$displayNode]['xAxisData'][$key];
                $xAxisItem['hours'] += General::toFloat($item['CHOURS']) + General::toFloat($item['CHOURSAL']);
                $xAxisItem['hoursAL'] += General::toFloat($item['CHOURSAL']);

                unset($xAxisItem);

                array_push($employeesFromBookings, $item['SHORTCUT']);

            }
            $data[$displayNode]['targetEffort'] = General::toFloat($displayNodeTargetEffort ?? array_sum($targetEfforts));
        }

        // Add total row
        if ($renderTotalRow) {
            $data['total'] = [
                'isTotal' => true,
                'name' => $translate->translate('summe'),
                'sapKey' => '',
                'total' => [
                    'hours' => 0.0,
                    'hoursAL' => 0.0,
                    'isTotal' => true
                ],
                'targetEffort' => 0.0,
                'percent' => 0,
                'xAxisData' => []
            ];

            foreach ($xAxisKeys as $xAxisKey) {
                $data['total']['xAxisData'][$xAxisKey] = [
                    'hours' => 0.0,
                    'hoursAL' => 0.0,
                    'isTotal' => true
                ];
            }

            foreach ($data as $key => $node) {
                if ($key == 'header' || $key == 'total') {
                    continue;
                }

                // If display cc node is child node of display node then skip adding to total
                // its value will be added as display node value
                if (!isset($node['isTotal']) || false == $node['isTotal']) {
                    $skip = false;
                    foreach ($displayNodes['cc'] as $displayNode => $ccNodes) {
                        $ccNodes = array_column($ccNodes, 'ID');
                        if (in_array($key, $ccNodes)) {
                            $skip = true;
                            break;
                        }
                    }

                    if ($skip) {
                        continue;
                    }
                } elseif ($node['isTotal']) {
                    if (isset($displayNodes['isChild'][$key]) && $displayNodes['isChild'][$key]) {
                        continue;
                    }
                }

                foreach ($node['xAxisData'] as $xAxisKey => $item) {
                    $data['total']['xAxisData'][$xAxisKey]['hours'] += $item['hours'];
                    $data['total']['xAxisData'][$xAxisKey]['hoursAL'] += $item['hoursAL'];
                }

                $data['total']['targetEffort'] += $node['targetEffort'];
            }
        }

        // Compute total values
        $total = [
            'hours' => 0.0,
            'hoursAL' => 0.0
        ];
        foreach ($data as $key => &$node) {
            if ($key == 'header') {
                continue;
            }

            foreach ($node['xAxisData'] as $item) {
                $node['total']['hours'] += $item['hours'];
                $node['total']['hoursAL'] += $item['hoursAL'];
            }

            // Skip total row when total value is calculated
            if ($key == 'total') {
                continue;
            }

            // If display cc node is child node of display node then skip adding to total
            // its value will be added as display node value
            if (!isset($node['isTotal']) || false == $node['isTotal']) {
                $skip = false;
                foreach ($displayNodes['cc'] as $displayNode => $ccNodes) {
                    $ccNodes = array_column($ccNodes, 'ID');
                    if (in_array($key, $ccNodes)) {
                        $skip = true;
                        break;
                    }
                }

                if ($skip) {
                    continue;
                }
            } elseif ($node['isTotal']) {
                if (isset($displayNodes['isChild'][$key]) && $displayNodes['isChild'][$key]) {
                    continue;
                }
            }

            $total['hours'] += $node['total']['hours'];
            $total['hoursAL'] += $node['total']['hoursAL'];
        }

        // Compute percent values
        foreach ($data as $key => &$node) {
            if ($key == 'header') {
                continue;
            }
            $node['percent'] = $total['hours'] > 0 ? round((100 / $total['hours']) * $node['total']['hours'], 0) : 0;
        }

        // Arrange found elements in proper order, i.e. booking tree order (DFS)
        $tableData = [];
        $sql = /** @lang SQL */"
            WITH RECURSIVE nodes AS (
                SELECT
                    node.id,
                    ARRAY[upper(name)] AS hierarchy
                FROM node
                INNER JOIN node_version nv on node.id = nv.node_id
                    AND nv.version_state_id = 1
                WHERE node.parent_node_id IS NULL
                UNION ALL
                SELECT
                    node.id,
                    nodes.hierarchy || upper(n.name)
                FROM node
                INNER JOIN nodes ON nodes.id = node.parent_node_id
                INNER JOIN node_version n on node.id = n.node_id
                    AND n.version_state_id = 1
            )
            SELECT nodes.id FROM nodes ORDER BY hierarchy
        ";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $nodeOrder = $stmt->fetchAll(Zend_Db::FETCH_COLUMN);

        $tableData = [
            'header' => $data['header']
        ];

        if (isset($data['total'])) {
            $tableData['total'] = $data['total'];
        }

        foreach ($nodeOrder as $item) {
            if (isset($data[$item])) {
                $tableData[$item] = $data[$item];
            }
        }

        // Fetch the user shortcuts from DB (if there are any)
        $booked = ChartProfil::getUserAndGroupNamesOfProfile($profil->ID, $db);
        if (empty($booked['employees']) && empty($booked['groups']) && !empty($employeesFromBookings)) {
            $booked['employees'] = array_unique($employeesFromBookings);
            sort($booked['employees']);
        }

        // Fetch the selected nodes from DB (if there are any)
        $nodes = ChartProfil::getNodesOfProfile($profil->ID, $db);

        // Remove keys from result array
        $count = count($tableData);
        foreach ($tableData as &$node) {
            if ($xAxis == 'E') {
                ksort($node['xAxisData']);
            }
            $node['xAxisData'] = array_values($node['xAxisData']);
        }
        $tableData = array_values($tableData);

        // save report data in cache
        $cacheId = $profil->ID . '_' . $timestamp;
        $cache->save([
            'REPORT_INFO' => [
                'TYPE' => $profil->ANZEIGE,
                'EMPLOYEES' => isset($booked) ? $booked : '',
                'NODES' => $nodes,
                'PERIODFROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
                'PERIODUNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP)
            ],
            'DATA' => $tableData
        ], $cacheId);

        return [
            'DATA' => ['data' => $tableData],
            'FROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
            'UNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP),
            'EMPLOYEEINFO' => ['booked' => isset($booked) ? $booked : ''],
            'ACTHOURS' => $total['hours']
        ];
    }

    public static function getProjectName($id, $db)
    {
        $sql = "SELECT NAME FROM PROJEKT WHERE ID = :ID";
        $statement = $db->prepare($sql);
        $statement->bindValue(':ID', $id);
        $statement->execute();
        $value = $statement->fetchColumn();

        if ($value) {
            return $value;
        } else {
            return '';
        }
    }

    public static function getAdditionalEmployees4Profile($profileId, $db)
    {
        $sql = "SELECT MITARBEITER.KUERZEL FROM BC_CHARTP_X_ADDEMP INNER JOIN MITARBEITER ON BC_CHARTP_X_ADDEMP.MITARBEITER_ID = MITARBEITER.ID " .
            "WHERE BC_CHART_PROFIL_ID =:CHARTPROFIL";
        $statement = $db->prepare($sql);
        $statement->bindValue(':CHARTPROFIL', $profileId);
        $statement->execute();
        $rows = $statement->fetchAll();

        $maKuerzel = '';
        foreach ($rows as $row) {
            $maKuerzel .= $row['KUERZEL'] . ', ';
        }

        $maKuerzel = trim($maKuerzel, ', ');
        return $maKuerzel;
    }

    public static function getEmployees2Monitor($profileId, $db)
    {
        $sql = "SELECT MITARBEITER.KUERZEL FROM BC_CHARTP_X_MA INNER JOIN MITARBEITER ON BC_CHARTP_X_MA.MITARBEITER_ID = MITARBEITER.ID " .
            "WHERE BC_CHART_PROFIL_ID =:CHARTPROFIL";
        $statement = $db->prepare($sql);
        $statement->bindValue(':CHARTPROFIL', $profileId);
        $statement->execute();
        $rows = $statement->fetchAll();

        $maKuerzel = '';
        foreach ($rows as $row) {
            $maKuerzel .= $row['KUERZEL'] . ', ';
        }

        $maKuerzel = trim($maKuerzel, ', ');
        return $maKuerzel;
    }

    /**
     * Booking Overview
     *
     * @param $profil
     * @param $timestamp
     * @param Zend_Db_Adapter_Abstract $db
     * @param $cache
     * @param $translator
     * @param $user
     * @return array
     * @throws Zend_Locale_Exception
     */
    public static function loadBooking($profil, $timestamp, Zend_Db_Adapter_Abstract $db, $cache, $translator, $user)
    {
        Date::determinePeriod($profil->ID, $period_from, $period_until, $db);

        // Determine node id of this profile
        $userSelectedNodes = Chartprofil::getProfileNodes($profil->ID, $db, false);
        $hasUserSelectedNodes = count($userSelectedNodes) > 0;

        // Determine profile companies
        $companies = ChartProfil::getProfileCompanies($profil->ID);
        // Determine profile divisions
        $divisions = ChartProfil::getProfileDivisions($profil->ID);
        // Determine the selected users
        $employees = ChartProfil::getProfileEmployees($db, $profil->ID);

        $profileBookingTypes = ChartProfil::getBookingTypes($profil->ID);

        $userAndGroupIds = self::getUserAndGroupIdsOfProfile($profil->ID, $db);

        // if resolved employee selection results in no employees at all (e.g. due to empty groups), then report has no data
        if (!empty($userAndGroupIds) && self::noEmployeeSelected($profil->ID, [
                "companies" => $companies,
                "divisions" => $divisions,
                "employees" => $employees['id']
            ])
        ) {
            $employees['id'] = [Employee::DUMMY];
        }

        $employeeSources = [
            'employment_period.company_id' => $companies,
            'employee_function_period.division_id' => $divisions,
            'employee.id' => $employees['id']
        ];
        $companiesWhere = '';
        $divisionsWhere = '';
        $employeesWhere = '';
        if (!empty($companies)) {
            $companiesWhere = "employment_period.company_id IN (" . implode(',', $companies) . ")";
        }

        if (!empty($divisions)) {
            $divisionsWhere = "employee_function_period.division_id IN (" . implode(',', $divisions) . ")";
        }

        if (!empty($employees['id'])) {
            $employeesWhere = QueryBuilder::createInClause('cemployee_id', $employees['id']);
        }

        $bookingTypesWhere = '';

        $employeesFromBookings = [];
        $dayOffBookingsAvailable = false;
        $btObj = new BookingType();
        if ($profil->DISPLAY_ONLY_SUBTREES) {
            $bookingTypesWhere = "AND %s IN (" . implode(',', $profileBookingTypes) . ")";
            $btKeys = $btObj->getBookingTypesHoursAsArray($profileBookingTypes);
        } else {
            $btKeys = $btObj->getBookingTypesHoursAsArray();
        }
        if ($profil->WITH_LOCKED_NODES == 0) {
            $lockedAnd = " AND (node_version.lock_date IS null OR node_version.lock_date > current_date) ";
            $lockedWhere = " WHERE (node_version.lock_date IS null OR node_version.lock_date > current_date) ";
        } else {
            $lockedAnd = "";
            $lockedWhere = "";
        }

        if (!empty($profil->NOTIZTEXT)) {
            $noteWhere =  " AND (lower(cnote) LIKE lower(:NOTE) OR lower(cnoteal) LIKE lower(:NOTE)) ";
        } else {
            $noteWhere = "";
        }
        $employeeConditions = [];

        foreach ($employeeSources as $identifier => $ids) {
            if (!empty($ids)) {
                $employeeConditions[] = QueryBuilder::createInClause($identifier, $ids);
            }
        }
        $employeeRestrictions = QueryBuilder::conditionOr("WHERE", $employeeConditions);

        $withClauseEmployees = "
            WITH employees AS (
                    SELECT employee.id, shortcut
                    FROM EMPLOYEE
                             LEFT JOIN employment_period ON employee.id = employment_period.employee_id
                             AND (
                                 (entry_date BETWEEN TO_DATE(:PERIOD_FROM, 'YYYY-MM-DD') AND TO_DATE(:PERIOD_UNTIL, 'YYYY-MM-DD'))
                                 OR
                                 (entry_date < TO_DATE(:PERIOD_FROM, 'YYYY-MM-DD') AND coalesce(leaving_date, max_date()) > TO_DATE(:PERIOD_FROM, 'YYYY-MM-DD'))
                             )
                             LEFT JOIN employee_function_period ON employee.id = employee_function_period.employee_id
                             AND (
                                 (start_date BETWEEN TO_DATE(:PERIOD_FROM, 'YYYY-MM-DD') AND TO_DATE(:PERIOD_UNTIL, 'YYYY-MM-DD'))
                                 OR
                                 (start_date < TO_DATE(:PERIOD_FROM, 'YYYY-MM-DD') AND coalesce(end_date, max_date()) > TO_DATE(:PERIOD_FROM, 'YYYY-MM-DD'))
                             )
                    " . $employeeRestrictions . "
                    group by employee.id, employee.shortcut
                )
        ";

        // get the tree...
        if ($hasUserSelectedNodes) {
            $sql = "
                " . $withClauseEmployees . "
                SELECT
                    node.*,
                    tpwwh.summe,
                    tpwwh.summeal,
                    last_remaining_effort.remaining_effort AS remaining_effort,
                    to_char(last_remaining_effort.remaining_effort_month, 'DD.MM.YYYY') AS remaining_effort_month
                FROM (
                    WITH RECURSIVE nodes AS (
                        SELECT
                            node.id,
                            1 AS lvl
                        FROM node
                        WHERE " . QueryBuilder::createInClause('node.id', $userSelectedNodes) . "
                        UNION ALL
                        SELECT
                            node.id,
                            nodes.lvl + 1 AS lvl
                        FROM node
                                 INNER JOIN nodes ON nodes.id = node.parent_node_id
                    ) 
                    SELECT
                        nodes.id,
                        coalesce(node.parent_node_id, 0) AS parent_node_id,
                        node.node_type_id,
                        node_version.structure_unit_type_id,
                        node.is_leaf,
                        node_version.lock_date,
                        node.bookable,
                        node_version.debtor_number,
                        node_version.order_number,
                        node_version.booking_type_id,
                        node_version.order_unit_number,
                        node_version.sap_key_cost_center,
                        node_version.sap_key_cost_object,
                        node_version.target_effort,
                        CASE
                            WHEN node_version.long_name IS NOT null THEN node_version.name || ' [' || node_version.long_name || ']'
                            WHEN node_version.customer_name IS NOT null THEN node_version.name || ' [' || node_version.customer_name || ']'
                            ElSE node_version.name
                            END AS name,
                        nodes.lvl
                    FROM nodes
                    INNER JOIN node ON node.id = nodes.id
                    INNER JOIN node_version
                        ON node_version.node_id = nodes.id
                            AND node_version.version_state_id = 1
                    " . $lockedWhere . "
                ) node
                LEFT JOIN (
                    SELECT
                        sum(tpwworkinghours.chours) AS summe,
                        sum(tpwworkinghours.choursal) AS summeal,
                        tpwworkinghours.cnode_id
                    FROM employees
                    LEFT JOIN tpwworkinghours
                        ON cemployee_id = employees.id
                    LEFT JOIN employment_period
                        ON employment_period.employee_id = tpwworkinghours.cemployee_id
                        AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                    LEFT JOIN employee_function_period ON
                        employee_function_period.employee_id = tpwworkinghours.cemployee_id
                        AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
                    INNER JOIN node ON node.id = tpwworkinghours.cnode_id
                    INNER JOIN node_version
                        ON node_version.node_id = node.id
                        AND node_version.version_state_id = 1
                    WHERE cdate BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
                    " . $lockedAnd . "
                    " . $noteWhere . "
                    " . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
                    " . sprintf($bookingTypesWhere, 'tpwworkinghours.cbooking_type_id') . "
                    GROUP BY tpwworkinghours.cnode_id
                ) tpwwh
                    ON tpwwh.cnode_id = node.id
                LEFT JOIN (
                    SELECT DISTINCT
                        node.id AS node_id,
                        first_value(node_mis_info.remainingeffort_value) OVER (PARTITION BY node.id ORDER BY node_mis_info.month_end DESC) AS remaining_effort,
                        first_value(node_mis_info.month_end) OVER (PARTITION BY node.id ORDER BY node_mis_info.month_end DESC) AS remaining_effort_month
                    FROM node
                    INNER JOIN node_version
                        ON node_version.node_id = node.id
                        AND node_version.version_state_id = 1
                    LEFT JOIN node_mis_info
                        ON node_mis_info.node_id = node.id
                        AND node_mis_info.month_end BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
                    WHERE node_mis_info.remainingeffort_value IS NOT NULL
                    " . $lockedAnd . "
                    " . sprintf($bookingTypesWhere, 'node_version.booking_type_id') . "
                ) last_remaining_effort
                    ON last_remaining_effort.node_id = node.id
                ORDER BY lvl, name
            ";

            $statement = $db->prepare($sql);
            if (!empty($profil->NOTIZTEXT)) {
                $statement->bindValue(':NOTE', '%'.$profil->NOTIZTEXT.'%');
            }
            $statement->bindValue(':PERIOD_FROM', $period_from->get(Iso_Date::ORACLE_DATE));
            $statement->bindValue(':PERIOD_UNTIL', $period_until->get(Iso_Date::ORACLE_DATE));
            $statement->execute();
            $rows = $statement->fetchAll();
        } else {
            // ...with only employees selected

            $sql = "
                " . $withClauseEmployees . "
                SELECT
                    sum(tpwworkinghours.chours) AS summe,
                    sum(tpwworkinghours.choursal) AS summeal,
                    tpwworkinghours.cnode_id
                FROM employees
                LEFT JOIN tpwworkinghours
                    ON CEMPLOYEE_ID = employees.id
                LEFT JOIN employment_period
                    ON employment_period.employee_id = tpwworkinghours.cemployee_id
                    AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                LEFT JOIN employee_function_period
                    ON employee_function_period.employee_id = tpwworkinghours.cemployee_id
                    AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
                LEFT JOIN node ON node.id = tpwworkinghours.cnode_id
                LEFT JOIN node_version
                    ON node_version.node_id = node.id
                    AND node_version.version_state_id = 1
                WHERE tpwworkinghours.cnode_id IS NOT null
                AND tpwworkinghours.cdate BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
                " . $lockedAnd . "
                " . $noteWhere . "
                " . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
                " . sprintf($bookingTypesWhere, 'tpwworkinghours.cbooking_type_id') . "
                GROUP BY tpwworkinghours.cnode_id
                ORDER BY tpwworkinghours.cnode_id
            ";
            $statement = $db->prepare($sql);
            if (!empty($profil->NOTIZTEXT)) {
                $statement->bindValue(':NOTE', '%'.$profil->NOTIZTEXT.'%');
            }
            $statement->bindValue(':PERIOD_FROM', $period_from->get(Iso_Date::ORACLE_DATE));
            $statement->bindValue(':PERIOD_UNTIL', $period_until->get(Iso_Date::ORACLE_DATE));
            $statement->execute();

            $rows = $statement->fetchAll();

            $hasOnlyDayOffBookings = false;

            if (count($rows) > 0) {
                if (isset($rows[0]["CNODE_ID"])) {
                    $dayOffBookingsAvailable = (int)$rows[0]["CNODE_ID"] === 0;
                    $hasOnlyDayOffBookings = (int)end($rows)["CNODE_ID"] === 0;
                    reset($rows);
                }

                //Check if array is empty or the array has only day off bookings
                if (!$hasOnlyDayOffBookings) {
                    $bookings = [];
                    $bookedLeafes = [];
                    foreach ($rows as $row) {
                        $bookings[$row['CNODE_ID']] = General::toFloat($row['SUMME']) + General::toFloat($row['SUMMEAL']);
                        $bookedLeafes[] = $row['CNODE_ID'];
                    }

                    $sql = "
                    SELECT
                        node.*,
                        last_remaining_effort.remaining_effort AS remaining_effort,
                        to_char(last_remaining_effort.remaining_effort_month, 'DD.MM.YYYY') AS remaining_effort_month
                    FROM (
                        WITH RECURSIVE nodes AS (
                            SELECT
                                node.id,
                                1 AS lvl
                            FROM node
                            WHERE node.parent_node_id IS NULL
                            UNION ALL
                            SELECT
                                node.id,
                                nodes.lvl + 1 AS lvl
                            FROM node
                                     INNER JOIN nodes ON nodes.id = node.parent_node_id
                        ) 
                        SELECT
                            nodes.id,
                            coalesce(node.parent_node_id, 0) AS parent_node_id,
                            node.node_type_id,
                            node_version.structure_unit_type_id,
                            node.is_leaf,
                            node_version.lock_date,
                            node.bookable,
                            node_version.debtor_number,
                            node_version.order_number,
                            node_version.booking_type_id,
                            node_version.order_unit_number,
                            node_version.sap_key_cost_center,
                            node_version.sap_key_cost_object,
                            node_version.target_effort,
                            CASE
                                WHEN node_version.long_name IS NOT null THEN node_version.name || ' [' || node_version.long_name || ']'
                                WHEN node_version.customer_name IS NOT null THEN node_version.name || ' [' || node_version.customer_name || ']'
                                ElSE node_version.name
                                END AS name,
                            nodes.lvl
                        FROM nodes
                        INNER JOIN node ON node.id = nodes.id
                        INNER JOIN node_version
                            ON node_version.node_id = nodes.id
                                AND node_version.version_state_id = 1
                        " . $lockedWhere . "
                    ) node
                    INNER JOIN (
                        WITH RECURSIVE nodes AS (
                            SELECT
                                node.id,
                                node.parent_node_id
                            FROM node
                            WHERE " . QueryBuilder::createInClause('id', $bookedLeafes) . "
                            UNION ALL
                            SELECT
                                node.id,
                                node.parent_node_id
                            FROM node
                                INNER JOIN nodes ON nodes.parent_node_id = node.id
                        ) select DISTINCT id from nodes
                    ) booked_node
                        ON node.id = booked_node.id
                    LEFT JOIN (
                        SELECT DISTINCT
                          node.id AS node_id,
                          first_value(node_mis_info.remainingeffort_value) OVER (PARTITION BY node.id ORDER BY node_mis_info.month_end DESC) AS remaining_effort,
                          first_value(node_mis_info.month_end) OVER (PARTITION BY node.id ORDER BY node_mis_info.month_end DESC) AS remaining_effort_month
                        FROM node
                        INNER JOIN node_version
                            ON node_version.node_id = node.id
                            AND node_version.version_state_id = 1
                        LEFT JOIN node_mis_info
                            ON node_mis_info.node_id = node.id
                               AND node_mis_info.month_end BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
                        WHERE node_mis_info.remainingeffort_value IS NOT NULL
                        " . $lockedAnd . "
                        " . sprintf($bookingTypesWhere, 'node_version.booking_type_id') . "
                    ) last_remaining_effort
                        ON last_remaining_effort.node_id = node.id
                    ORDER BY lvl, node.name
                ";

                    $statement = $db->prepare($sql);
                    $statement->bindValue(':PERIOD_FROM', $period_from->get(Iso_Date::ORACLE_DATE));
                    $statement->bindValue(':PERIOD_UNTIL', $period_until->get(Iso_Date::ORACLE_DATE));
                    $statement->execute();
                    $rows = $statement->fetchAll();
                } else {
                    $rows = [];
                }
            }
        }

        $tree = []; // tree data array
        $refNodes = [];                // index array for all nodes
        $refLeavesWithBookings = [];    // index array for leaf nodes w/ bookings
        $refLeavesWithSAPKey = [];        // index array for leaf nodes w/ sap key
        $btHours = [];

        if ($profil->BOOKING_TYPE_HOURS == 1 && count($rows) > 0) {
            $nodesWithBookings = [];
            foreach ($rows as $row) {
                # as the variable name implies, only nodes with bookings should be considered...
                if ($hasUserSelectedNodes) {
                    # filtering only possible when user has nodes selected
                    if ($row['SUMME'] !== null || $row['SUMMEAL'] !== null) {
                        $nodesWithBookings[] = (int)$row['ID'];
                    }
                }
                else {
                    $nodesWithBookings[] = (int)$row['ID'];
                }
            }

            $nodesWithBookingsWhere = "";
            if (!empty($nodesWithBookings)) {
                $nodesWithBookingsWhere = " AND " . QueryBuilder::createInClause('cnode_id', $nodesWithBookings);
            }
            $sql = "
                " . $withClauseEmployees . "
                SELECT
                    sum(tpwworkinghours.chours) AS chours_sum,
                    sum(tpwworkinghours.choursal) AS choursal_sum,
                    tpwworkinghours.cnode_id,
                    booking_type.name AS bt_name
                FROM employees 
                LEFT JOIN tpwworkinghours
                    ON tpwworkinghours.CEMPLOYEE_ID = employees.id
                LEFT JOIN employment_period
                    ON employment_period.employee_id = tpwworkinghours.cemployee_id
                    AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                LEFT JOIN employee_function_period
                    ON employee_function_period.employee_id = tpwworkinghours.cemployee_id
                    AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
                LEFT JOIN booking_type ON tpwworkinghours.cbooking_type_id = booking_type.id
                INNER JOIN node ON node.id = cnode_id
                INNER JOIN node_version
                    ON node_version.node_id = node.id
                    AND node_version.version_state_id = 1
                WHERE tpwworkinghours.cdate BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
                " . $noteWhere . "
                " . $lockedAnd . "
                " . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
                " . $nodesWithBookingsWhere . "
                " . sprintf($bookingTypesWhere, 'booking_type.id') . "
                GROUP BY cnode_id, booking_type.name, booking_type.id
            ";

            $statement = $db->prepare($sql);
            if (!empty($profil->NOTIZTEXT)) {
                $statement->bindValue(':NOTE', '%'.$profil->NOTIZTEXT.'%');
            }

            $statement->bindValue(':PERIOD_FROM', $period_from->get(Iso_Date::ORACLE_DATE));
            $statement->bindValue(':PERIOD_UNTIL', $period_until->get(Iso_Date::ORACLE_DATE));

            $statement->execute();
            $result = $statement->fetchAll();

            foreach ($result as $item) {
                if (false == is_null($item['BT_NAME'])) {
                    $key = $item['CNODE_ID'];
                    $value = $item['BT_NAME'] . 'Hours';
                    $hours = empty($item['CHOURS_SUM']) ? 0.0 : General::toFloat($item['CHOURS_SUM']);
                    $hoursAl = empty($item['CHOURSAL_SUM']) ? 0.0 : General::toFloat($item['CHOURSAL_SUM']);
                    $hours = floatval($hours + $hoursAl);

                    if (false == isset($btHours[$key])) {
                        $btHours[$key] = [];
                    }
                    $btHours[$key][$value] = isset($btHours[$key][$value])
                        ? number_format(General::toFloat($btHours[$key][$value]) + $hours, 2, '.', '')
                        : number_format($hours, 2, '.', '');
                }
            }
        }

        if ($dayOffBookingsAvailable === true) {
            $dayOffArtificialNode = [
                'BOOKABLE' => '1',
                'DEBTOR_NUMBER' => null,
                'ID' => '-2',
                'IS_LEAF' => '1',
                'NAME' => 'TagFrei',
                'NODE_TYPE_ID' => '1',
                'ORDER_UNIT_NUMBER' => null,
                'PARENT_NODE_ID' => '0',
                'ORDER_NUMBER' => null,
                'SAP_KEY_COST_CENTER' => null,
                'SAP_KEY_COST_OBJECT' => null,
                'STRUCTURE_UNIT_TYPE_ID' => '1',
                'TARGET_EFFORT' => null,
                'REMAINING_EFFORT' => null,
                'REMAINING_EFFORT_MONTH' => null
            ];

            $rows[-1] = $dayOffArtificialNode;
            //day off node is displayed as the first item
            ksort($rows);
        }

        // Iterate over the results and build the tree structure
        foreach ($rows as $row) {
            // create tree element
            $element = [
                'id'                                => $row['ID'],
                'node'                                => $row['BOOKABLE'] == 1 ? $row['NAME'] : '<span style="color:grey"><i>' . $row['NAME'] . '</i></span>',
                'bookable'                            => $row['BOOKABLE'] == 1,
                'bookings'                            => '',
                'leaf'                                => $row['IS_LEAF'] == 1,
                'iconCls'                            => Node::getIconCls((int)$row['NODE_TYPE_ID']),
                'cls'                                => Node::getCls((int)$row['NODE_TYPE_ID'], (int)$row['STRUCTURE_UNIT_TYPE_ID']),
                'nodeType'                            => $row['NODE_TYPE_ID'],
                'suType'                            => $row['STRUCTURE_UNIT_TYPE_ID'],
                'parentId'                            => $row['PARENT_NODE_ID'],
                'sapKeyCostUnit'                    => $row['SAP_KEY_COST_OBJECT'],
                'sapKeyCostCenter'                    => $row['SAP_KEY_COST_CENTER'],
                'target_effort'                        => 0,
                'target_effort_is_aggregated'        => false,
                'remaining_effort'                => $row['REMAINING_EFFORT'],
                'remaining_effort_month'            => $row['REMAINING_EFFORT_MONTH'],
                'remaining_effort_is_aggregated'    => false
            ];

            // target effort
            if (!$profil->DISPLAY_ONLY_SUBTREES || in_array($row['BOOKING_TYPE_ID'], $profileBookingTypes)) {
                $element['target_effort'] = $row['TARGET_EFFORT'];
            }

            // debtor/project/unitoforder number
            if ($row['DEBTOR_NUMBER'] != '') {
                $element['debtor'] = $row['DEBTOR_NUMBER'];
            } elseif ($row['ORDER_NUMBER'] != '') {
                $element['debtor'] = $row['ORDER_NUMBER'];
            } else {
                // is null for node type <> unit of order, such as activity
                $element['debtor'] = $row['ORDER_UNIT_NUMBER'];
            }

            // Hours
            if (count($userSelectedNodes) > 0) {
                $tempSumme = !is_null($row['SUMME']) ? General::toFloat($row['SUMME']) : 0;
                $tempSummeAl = !is_null($row['SUMMEAL']) ? General::toFloat($row['SUMMEAL']) : 0;
                $element['hours'] = number_format($tempSumme + $tempSummeAl, 2, '.', '');
            } else {
                $element['hours'] = isset($bookings[$element['id']]) ? number_format(General::toFloat($bookings[$element['id']]), 2, '.', '') : 0;
            }

            if ($profil->BOOKING_TYPE_HOURS == 1) {
                foreach ($btKeys as $btKey) {
                    $element[$btKey] = '';
                }
                if (isset($btHours[$element['id']])) {
                    foreach ($btHours[$element['id']] as $btKey => $btValue) {
                        $element[$btKey] = $btValue;
                    }
                }
            }

            // SAP key
            if ($profil->COST_AND_REVENUE == 1) {
                $element['sapKey'] = '';
                if ($row['SAP_KEY_COST_CENTER'] != '') {
                    $element['sapKey'] = $row['SAP_KEY_COST_CENTER'];
                }
                if ($row['SAP_KEY_COST_OBJECT'] != '') {
                    $element['sapKey'] = $row['SAP_KEY_COST_OBJECT'];
                }
            }

            $element['cost'] = 0;
            $element['revenue'] = 0;
            // Because cost/revenue can be attached to every node, not just to leafes
            $element['ownCost'] = 0;
            $element['ownRevenue'] = 0;

            // aggregate hours
            $sumParent = &$refNodes[$element['parentId']];

            $stopTargetEffortAggregation = false;
            $stopRemainingEffortAggregation = false;

            while (isset($sumParent)) {
                if ($element['hours'] > 0) {
                    $sumParent['hours'] = number_format($sumParent['hours'] + $element['hours'], 2, '.', '');
                }

                if (($sumParent['remaining_effort'] == null || $sumParent['remaining_effort_is_aggregated'])
                    && $element['remaining_effort'] != null
                    && $stopRemainingEffortAggregation === false) {
                    $sumParent['remaining_effort'] = $sumParent['remaining_effort'] + $element['remaining_effort'];
                    $sumParent['remaining_effort_is_aggregated'] = true;
                    $sumParent['remaining_effort_month'] = null;
                } else {
                    $stopRemainingEffortAggregation = true;
                }

                if (($sumParent['target_effort'] == 0 || $sumParent['target_effort_is_aggregated'])
                    && $stopTargetEffortAggregation === false) {
                    $sumParent['target_effort'] = $sumParent['target_effort'] + $element['target_effort'];
                    $sumParent['target_effort_is_aggregated'] = true;
                } else {
                    $stopTargetEffortAggregation = true;
                }

                // aggregate booking type hours
                if ($profil->BOOKING_TYPE_HOURS == 1) {
                    foreach ($btKeys as $item) {
                        if (false == empty($element[$item])) {
                            $sumParent[$item] = $sumParent[$item] + $element[$item];
                        }
                    }
                }

                $sumParent = &$refNodes[$sumParent['parentId']];
            }

            $element['children'] = [];

            // Add current element to its parent's 'children'-array using '$refNodes'
            if (count($tree) == 0 || false == isset($refNodes[$row['PARENT_NODE_ID']])) {
                // First element
                $lastIndex = array_push($tree, $element);
                $refNodes[$element['id']] = &$tree[$lastIndex - 1];
                if ($element['hours'] > 0) {
                    $refLeavesWithBookings[count($refLeavesWithBookings)] = &$tree[$lastIndex - 1];
                }
                if ($profil->COST_AND_REVENUE == 1 && $element['sapKey'] != '' &&
                    ($profil->DISPLAY_ONLY_SUBTREES == 0 || in_array($row['BOOKING_TYPE_ID'], $profileBookingTypes))
                ) {
                    $refLeavesWithSAPKey[$element['sapKey']] = &$tree[$lastIndex - 1];
                }
            } else {
                //find the parent element
                $parent = &$refNodes[$row['PARENT_NODE_ID']];
                $lastIndex = array_push($parent['children'], $element);
                $refNodes[$element['id']] = &$parent['children'][$lastIndex - 1];

                if ($element['hours']  > 0) {
                    $refLeavesWithBookings[count($refLeavesWithBookings)] = &$parent['children'][$lastIndex - 1];
                }
                if ($profil->COST_AND_REVENUE == 1 && $element['sapKey'] != '' &&
                    ($profil->DISPLAY_ONLY_SUBTREES == 0 || in_array($row['BOOKING_TYPE_ID'], $profileBookingTypes))
                ) {
                    $refLeavesWithSAPKey[$element['sapKey']] = &$parent['children'][$lastIndex - 1];
                }
            }
        }

        // Cost & revenue from SAP
        if ($profil->COST_AND_REVENUE == 1 && count($refLeavesWithSAPKey) > 0) {
            // build SQL IN() part
            $keys = [];
            foreach ($refLeavesWithSAPKey as $leaf) {
                $keys[] = $leaf['sapKey'];
            }

            $sql = "
				SELECT
				  sap_key,
				  sum(cost) AS cost,
				  sum(revenue) * -1 AS revenue
				FROM cost_and_revenue
				WHERE " . QueryBuilder::createInClause('sap_key', $keys, false) . "
				AND month BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
				GROUP BY sap_key
			";

            $statement = $db->prepare($sql);
            $statement->bindValue(':PERIOD_FROM', $period_from->get(Iso_Date::ORACLE_DATE));
            $statement->bindValue(':PERIOD_UNTIL', $period_until->get(Iso_Date::ORACLE_DATE));
            $statement->execute();
            $rows = $statement->fetchAll();

            foreach ($rows as $row) {
                $refLeavesWithSAPKey[$row['SAP_KEY']]['ownCost'] = number_format(General::toFloat($row['COST']), 2, '.', '');
                $refLeavesWithSAPKey[$row['SAP_KEY']]['ownRevenue'] = number_format(General::toFloat($row['REVENUE']), 2, '.', '');
                $refLeavesWithSAPKey[$row['SAP_KEY']]['cost'] = number_format($refLeavesWithSAPKey[$row['SAP_KEY']]['cost'] + General::toFloat($row['COST']), 2, '.', '');
                $refLeavesWithSAPKey[$row['SAP_KEY']]['revenue'] = number_format($refLeavesWithSAPKey[$row['SAP_KEY']]['revenue'] + General::toFloat($row['REVENUE']), 2, '.', '');

                // aggregate cost & revenue
                if ($refLeavesWithSAPKey[$row['SAP_KEY']]['cost'] != 0 || $refLeavesWithSAPKey[$row['SAP_KEY']]['revenue'] != 0) {
                    $parent = &$refNodes[$refLeavesWithSAPKey[$row['SAP_KEY']]['parentId']];
                    while (isset($parent)) {
                        $parent['cost'] = number_format($parent['cost'] + $refLeavesWithSAPKey[$row['SAP_KEY']]['ownCost'], 2, '.', '');
                        $parent['revenue'] = number_format($parent['revenue'] + $refLeavesWithSAPKey[$row['SAP_KEY']]['ownRevenue'], 2, '.', '');
                        $parent = &$refNodes[$parent['parentId']];
                    }
                }
            }
        }

        // Cut off empty nodes
        if (!empty($tree)) {
            for ($i = 0, $c = sizeof($tree); $i < $c; $i++) {
                ChartProfil::cutOffEmptyNodes($tree[$i], $profil);
            }
        }

        if ($profil->EMPLOYEE_BOOKING == 1 && (count($refLeavesWithBookings) > 0 || $dayOffBookingsAvailable)) {
            $locale = new Zend_Locale($user->getSprache());

            if (count($refLeavesWithBookings) > 0) {
                // build SQL IN() part
                $ids = [];
                foreach ($refLeavesWithBookings as $leafNode) {
                    $ids[] = $leafNode['id'];
                }
                $sqlIn = QueryBuilder::createInClause('cnode_id', $ids);

                /*
                 * Has to be evaluated if this SQL Statement is faster as soon as we can test with an appropriate amount of data:
                 *
                 * "SELECT tpwworkinghours.cid, tpwworkinghours.cemployee_id, tpwworkinghours.chours
                 * 		FROM node LEFT JOIN tpwworkinghours ON tpwworkinghours.cnode_id = node.id
                 * 		WHERE node.is_leaf = 1 AND (node.is_leaf = 0 OR tpwworkinghours.cid IS NOT null)
                 * 		START WITH id = 1
                 * 		CONNECT BY PRIOR id = parent_node_id;"
                 */

                $sql = "
                    " . $withClauseEmployees . "
					SELECT
					  tpwworkinghours.cid,
					  cnode_id,
					  coalesce(chours,0) + coalesce(choursal,0) AS hours,
					  cflagpublichday,
					  CASE
						WHEN cnote IS NOT null AND cnoteal IS NOT null THEN cnoteal || ', ' || cnoteal
						ELSE coalesce(cnote, cnoteal)
					  END AS note,
					  to_char(cdate, 'YYYY-MM-DD') AS cdate,
					  employees.shortcut,
					  booking_type.name AS bt
					FROM employees
					LEFT JOIN tpwworkinghours
					    ON CEMPLOYEE_ID = employees.id
                    LEFT JOIN employment_period
                        ON employment_period.employee_id = tpwworkinghours.cemployee_id
                        AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                    LEFT JOIN employee_function_period
                        ON employee_function_period.employee_id = tpwworkinghours.cemployee_id
                        AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
					LEFT JOIN booking_type ON tpwworkinghours.cbooking_type_id = booking_type.id
					WHERE " . $sqlIn . "
					".$noteWhere."
					AND cdate BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
					" . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
					" . sprintf($bookingTypesWhere, 'tpwworkinghours.cbooking_type_id') . "
					ORDER BY employees.shortcut, cdate
					";

                $statement = $db->prepare($sql);
                if (!empty($profil->NOTIZTEXT)) {
                    $statement->bindValue(':NOTE', '%'.$profil->NOTIZTEXT.'%');
                }

                $statement->bindValue(':PERIOD_FROM', $period_from->get(Iso_Date::ORACLE_DATE));
                $statement->bindValue(':PERIOD_UNTIL', $period_until->get(Iso_Date::ORACLE_DATE));
                $statement->execute();
                $rows = $statement->fetchAll();
            }

            $employeeBookings = [];

            if (count($refLeavesWithBookings) > 0) {
                foreach ($rows as $row) {
                    array_push($employeesFromBookings, strtolower($row['SHORTCUT']));
                    // make sure there is an entry for this node
                    if (!isset($employeeBookings[$row['CNODE_ID']])) {
                        $employeeBookings[$row['CNODE_ID']] = [];
                    }
                    // make sure there is an entry for this employee at this node
                    if (!isset($employeeBookings[$row['CNODE_ID']][$row['SHORTCUT']])) {
                        $employeeBookings[$row['CNODE_ID']][$row['SHORTCUT']] = [
                            'Employee'    => $row['SHORTCUT'],
                            'Hours'        => 0,
                            'Bookings'    => []
                        ];
                    }

                    // add booking entry
                    $employeeBookings[$row['CNODE_ID']][$row['SHORTCUT']]['Bookings'][] = [
                        'cnode_id' => $row['CNODE_ID'],
                        'Hours'    => General::toFloat($row['HOURS']),
                        'Date'    => $row['CDATE'],
                        'Note'    => $row['NOTE'],
                        'BT' => $row['BT']
                    ];
                    // update sum
                    $employeeBookings[$row['CNODE_ID']][$row['SHORTCUT']]['Hours'] += General::toFloat($row['HOURS']);
                }

                // add the employee bookings to the internal representation

                ChartProfil::addBookingInfo($refLeavesWithBookings, $employeeBookings, $refNodes, $locale);
            }

            if ($dayOffBookingsAvailable) {
                //Get day off bookings
                $sql = "
                    " . $withClauseEmployees . "
					SELECT
					  cnode_id,
					  to_char(cdate, 'YYYY-MM-DD') AS cdate,
					  employees.shortcut
					FROM employees
					LEFT JOIN tpwworkinghours
					    ON CEMPLOYEE_ID = employees.id
					LEFT JOIN employment_period
                        ON employment_period.employee_id = tpwworkinghours.cemployee_id
                        AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                    LEFT JOIN employee_function_period
                        ON employee_function_period.employee_id = tpwworkinghours.cemployee_id
                        AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
					WHERE cnode_id = 0
					    AND cdate BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
					    " . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
					ORDER BY employees.shortcut, cdate
				";

                $statement = $db->prepare($sql);
                $statement->bindValue(':PERIOD_FROM', $period_from->get(Iso_Date::ORACLE_DATE));
                $statement->bindValue(':PERIOD_UNTIL', $period_until->get(Iso_Date::ORACLE_DATE));
                $statement->execute();

                $dayOffBookings = $statement->fetchAll();

                $employeeDayOffs = [];

                foreach ($dayOffBookings as $row) {
                    array_push($employeesFromBookings, strtolower($row['SHORTCUT']));

                    if (!isset($employeeDayOffs[0][$row['SHORTCUT']])) {
                        $employeeDayOffs[0][$row['SHORTCUT']] = [
                            'Employee'    => $row['SHORTCUT'],
                            'Hours'        => 0,
                            'Bookings'    => []
                        ];
                    }

                    // add booking entry
                    $employeeDayOffs[0][$row['SHORTCUT']]['Bookings'][] = [
                        'cnode_id' => '-2',
                        'Hours'        => 0.00,
                        'Date'        => $row['CDATE'],
                        'Note'        => '',
                        'BT'        => 'TM'
                    ];
                }

                // add the employee day off bookings to the internal representation
                if (!empty($employeeDayOffs)) {
                    ChartProfil::addBookingInfoDayOff($employeeDayOffs, $refNodes, $locale);
                    //-2 is the id of the day off root node
                    $employeeBookings[-2] = $employeeDayOffs[0];
                }
            }
        } elseif (count($refLeavesWithBookings) > 0) {
            // Call this statement only to get employee information
            $ids = [];
            foreach ($refLeavesWithBookings as $leafNode) {
                $ids[] = $leafNode['id'];
            }
            $sqlIn = QueryBuilder::createInClause('cnode_id', $ids);

            $sql = "
                " . $withClauseEmployees . "
				SELECT employees.shortcut
				FROM employees 
				LEFT JOIN tpwworkinghours
				    ON CEMPLOYEE_ID = employees.id
				LEFT JOIN employment_period
                    ON employment_period.employee_id = tpwworkinghours.cemployee_id
                    AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                LEFT JOIN employee_function_period
                    ON employee_function_period.employee_id = tpwworkinghours.cemployee_id
                    AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
				WHERE " . $sqlIn . "
				AND cdate BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
				" . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
				ORDER BY employees.shortcut
			";

            $statement = $db->prepare($sql);
            $statement->bindValue(':PERIOD_FROM', $period_from->get(Iso_Date::ORACLE_DATE));
            $statement->bindValue(':PERIOD_UNTIL', $period_until->get(Iso_Date::ORACLE_DATE));
            $statement->execute();
            $rows = $statement->fetchAll();

            $employeeBookings = [];
            foreach ($rows as $row) {
                array_push($employeesFromBookings, strtolower($row['SHORTCUT']));
            }
        }

        $actValue = 0;
        foreach ($tree as $item) {
            $actValue = number_format($actValue + $item['hours'], 2, '.', '');
        }

        // Update the Act-Hours value in DB
        ChartProfil::setActProfileValue($profil, $db, $actValue);

        // Add artificial root node
        $rowCount = count($tree);
        if ($rowCount > 1 && !$dayOffBookingsAvailable) {
            $rootNode = [
                'id' => 0,
                'parentId' => -1,
                'node' => $translator->translate('ergebnis'),
                'leaf' => false,
                'iconCls' => 'empty-icon',
                'children' => $tree,
                'nodeType' => null,
                'suType' => null,
                'debtor' => null,
                'hours' => $actValue,
                'cost' => 0,
                'revenue' => 0,
                'sapKeyCostUnit' => null,
                'sapKeyCostCenter' => null,
                'remaining_effort' => null,
                'target_effort' => null
            ];

            foreach ($btKeys as $btKey) {
                $rootNode[$btKey] = '';
            }

            // Update children 'parentId' property
            for ($i = 0; $i < $rowCount; $i++) {
                $tree[$i]['parentId'] = 0;
            }

            // Aggregate cost & revenue
            if ($profil->COST_AND_REVENUE == 1 && count($refLeavesWithSAPKey) > 0) {
                foreach ($tree as $element) {
                    if ($element['cost'] > 0 || $element['revenue'] > 0) {
                        $rootNode['cost'] = number_format($rootNode['cost'] + $element['cost'], 2, '.', '');
                        $rootNode['revenue'] = number_format($rootNode['revenue'] + $element['revenue'], 2, '.', '');
                    }
                }
            }

            // Aggregate target/remaining effort
            if ($profil->TARGET_HOURS == 1) {
                foreach ($tree as $element) {
                    if ($element['remaining_effort'] > 0) {
                        $rootNode['remaining_effort'] = number_format(($rootNode['remaining_effort'] ?: 0) + $element['remaining_effort'], 2, '.', '');
                        $rootNode['remaining_effort_is_aggregated'] = true;
                    }
                    if ($element['target_effort'] > 0) {
                        $rootNode['target_effort'] = number_format(($rootNode['target_effort'] ?: 0) + $element['target_effort'], 2, '.', '');
                        $rootNode['target_effort_is_aggregated'] = true;
                    }
                }
            }

            // Aggregate booking type hours
            if ($profil->BOOKING_TYPE_HOURS == 1) {
                foreach ($btKeys as $item) {
                    foreach ($tree as $element) {
                        if (false == empty($element[$item])) {
                            $rootNode[$item] = number_format(General::toFloat($rootNode[$item]) + General::toFloat($element[$item]), 2, '.', '');
                        }
                    }
                }
            }

            $tree = [$rootNode];
        }

        // Fetch the user shortcuts from DB (if there are any)
        $booked = ChartProfil::getUserAndGroupNamesOfProfile($profil->ID, $db);
        if (empty($booked['employees']) && empty($booked['groups']) && !empty($employeesFromBookings)) {
            $booked['employees'] = array_unique($employeesFromBookings);
            sort($booked['employees']);
        }

        // Fetch the selected nodes from DB (if there are any)
        $dbNodes = ChartProfil::getNodesOfProfile($profil->ID, $db);
        $chartProfileBookingType = new ChartProfileBookingType();

        $bookingTypesShortcuts = $chartProfileBookingType->getBookingTypesShortcuts($profil->ID);

        // save report data in cache
        $cacheId = $profil->ID . '_' . $timestamp;
        $cache->save([
            'REPORT_INFO' => [
                'TYPE' => $profil->ANZEIGE,
                'EMPLOYEES' => isset($booked) ? $booked : '',
                'NODES' => $dbNodes,
                'PERIODFROM' => $period_from->get(Zend_Date::TIMESTAMP),
                'PERIODUNTIL' => $period_until->get(Zend_Date::TIMESTAMP),
                'COSTREVENUE' => $profil->COST_AND_REVENUE,
                'EMP_BOOKINGS' => $profil->EMPLOYEE_BOOKING,
                'BOOKING_TYPE_HOURS' => $profil->BOOKING_TYPE_HOURS,
                'PROFILE_BOOKING_TYPES' => $bookingTypesShortcuts,
                'TARGET_HOURS' => $profil->TARGET_HOURS,
                'DISPLAY_ONLY_SUBTREES' => $profil->DISPLAY_ONLY_SUBTREES],
            'TREE' => $tree,
            'EMP_BOOKINGS' => isset($employeeBookings)? $employeeBookings : []
        ], $cacheId);

        return [
            'BOOKINGS' => [
                'bookings' => ['text' => '.', 'children' => $tree]
            ],
            'FROM' => $period_from->get(Zend_Date::TIMESTAMP),
            'UNTIL' => $period_until->get(Zend_Date::TIMESTAMP),
            'EMPLOYEEINFO' => ['booked' => isset($booked) ? $booked : ''],
            'ACTHOURS' => $actValue
        ];
    }

    /**
     * Gets the data for the 'accounting' report.
     */
    public static function loadAccounting($profile, $timestamp, $db)
    {
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);

        $employees = self::getProfileEmployees($db, $profile->ID);
        $companies = self::getProfileCompanies($profile->ID);
        $divisions = self::getProfileDivisions($profile->ID);
        $groupAllIsSelected = self::isEmployeeGroupSelected($profile->ID);

        $nodes = self::getProfileNodes($profile->ID, $db, false);

        // if resolved employee selection results in no employees at all (e.g. due to empty groups), then report has no data
        $userAndGroupIds = self::getUserAndGroupIdsOfProfile($profile->ID, $db);
        if (!empty($userAndGroupIds) && self::noEmployeeSelected($profile->ID, [
                "companies" => $companies,
                "divisions" => $divisions,
                "employees" => $employees['id']
            ])
        ) {
            $employees['id'] = [Employee::DUMMY];
        }

        $where = [];

        $employeesWhere = [];
        if (!$groupAllIsSelected) {
            if (!empty($employees['id'])) {
                $employeesWhere[] = QueryBuilder::createInClause('emp.id', $employees['id']);
            }
            if (!empty($companies)) {
                $employeesWhere[] = QueryBuilder::createInClause('employment_period.company_id', $companies);
            }
            if (!empty($divisions)) {
                $employeesWhere[] = QueryBuilder::createInClause('employee_function_period.division_id', $divisions);
            }
        }

        if (!empty($nodes)) {
            $where[] = "
                node.id IN (
                    WITH RECURSIVE nodes AS (
                        SELECT
                            node.id
                        FROM node
                        WHERE node.id IN (
                            SELECT coalesce(profil.node_id, setnode.node_id)
                            FROM bc_chart_profil_x_node profil
                                     LEFT JOIN nodeset_x_node setnode ON setnode.set_id = profil.set_id
                            WHERE profil.bc_chart_profil_id = :profile_id
                        )
                    
                        UNION ALL
                    
                        SELECT node.id
                        FROM node
                        INNER JOIN nodes ON nodes.id = node.parent_node_id
                    )
                    SELECT id FROM nodes
                )
            ";
        }

        if (!empty($employeesWhere)) {
            $where[] = QueryBuilder::conditionOr('', $employeesWhere);
        }
        $where = QueryBuilder::conditionAnd('AND', $where);

        $sql = "
            SELECT
                emp.shortcut,
                emp.first_name,
                emp.last_name,
                date_to_unixts(wh.cdate) AS cdate,
                nv.name AS cc,
                bt.id AS bt_id,
                bt.name AS bt_name,
                coalesce(wh.chours, 0) + coalesce(wh.choursal, 0) AS chours,
                CASE
                    WHEN wh.cnote IS NOT null THEN wh.cnote
                    WHEN wh.cnoteal IS NOT null THEN (
                        SELECT titel FROM fmaa WHERE kundenid = substr(wh.cnoteal, 1, position(':' IN wh.cnoteal) - 1)
                    )
                    ELSE null
                END AS cnote,
                (
                    WITH RECURSIVE nodes AS (
                       SELECT n.id,
                              n.parent_node_id
                       FROM node n
                       WHERE n.id = node.id
                    
                       UNION ALL
                    
                       SELECT n.id,
                              n.parent_node_id
                       FROM node n
                       INNER JOIN nodes ON nodes.parent_node_id = n.id
                    )
                    SELECT
                           nv.name || CASE WHEN nv.order_number IS NOT null THEN (' [' || nv.order_number || ']') ELSE '' END
                    from nodes
                            INNER JOIN node_version nv
                                       ON nv.node_id = nodes.id
                                           AND nv.version_state_id = :versionStateActive
                    WHERE nv.node_type_id = :nodeTypeOrder
                    LIMIT 1
                ) AS project,
                (
                    WITH
                        RECURSIVE nodes AS (
                        SELECT n.id,
                            n.parent_node_id
                        FROM node n
                        WHERE n.id = node.id
                
                        UNION ALL
                
                        SELECT n.id,
                            n.parent_node_id
                        FROM node n
                                 INNER JOIN nodes ON nodes.parent_node_id = n.id
                    )
                    SELECT nv.name || CASE WHEN nv.debtor_number IS NOT null THEN (' [' || nv.debtor_number || ']') ELSE '' END
                    from nodes
                             INNER JOIN node_version nv
                                        ON nv.node_id = nodes.id
                                            AND nv.version_state_id = :versionStateActive
                    WHERE nv.node_type_id = :nodeTypeDebtor
                        OR nv.node_type_id = :nodeTypeStructureUnit AND nv.structure_unit_type_id = :structureUnitTypeCompany
                    LIMIT 1
                ) AS debtor,
                (
                    WITH
                        RECURSIVE nodes AS (
                        SELECT n.id,
                            n.parent_node_id
                        FROM node n
                        WHERE n.id = node.id
                
                        UNION ALL
                
                        SELECT n.id,
                            n.parent_node_id
                        FROM node n
                                 INNER JOIN nodes ON nodes.parent_node_id = n.id
                    )
                    SELECT nv.name
                    from nodes
                             INNER JOIN node_version nv
                                        ON nv.node_id = nodes.id
                                            AND nv.version_state_id = :versionStateActive
                    WHERE nv.node_type_id = :nodeTypeStructureUnit AND nv.structure_unit_type_id = :structureUnitTypeCompany
                    LIMIT 1
                ) AS company
            FROM tpwworkinghours wh
            INNER JOIN employee emp ON emp.id = wh.cemployee_id
            INNER JOIN node ON node.id = wh.cnode_id
            INNER JOIN node_version nv ON nv.node_id = node.id AND nv.version_state_id = 1
            INNER JOIN booking_type bt ON bt.id = wh.cbooking_type_id
            LEFT JOIN employment_period
                ON employment_period.employee_id = emp.id
                AND wh.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
            LEFT JOIN employee_function_period
                ON employee_function_period.employee_id = emp.id
                AND wh.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
            WHERE wh.cdate BETWEEN to_date(:period_from, 'YYYY-MM-DD') AND to_date(:period_till, 'YYYY-MM-DD')
            AND (wh.cbooking_type_id = :id_accountable OR wh.cbooking_type_id = :id_notaccountable)
            AND node.is_leaf = 1
            $where
            ORDER BY debtor, project, emp.shortcut, cdate
        ";

        $stmt = $db->prepare($sql);
        $stmt->bindValue(':period_from', $periodFrom->get(Iso_Date::ORACLE_DATE));
        $stmt->bindValue(':period_till', $periodUntil->get(Iso_Date::ORACLE_DATE));
        $stmt->bindValue(':id_accountable', BookingType::ID_REPORT_ACCOUNTABLE);
        $stmt->bindValue(':id_notaccountable', BookingType::ID_REPORT_NOT_ACCOUNTABLE);
        $stmt->bindValue(':profile_id', $profile->ID);
        $stmt->bindValue(':versionStateActive', 1);
        $stmt->bindValue(':nodeTypeDebtor', NodeType::DEBTOR);
        $stmt->bindValue(':nodeTypeOrder', NodeType::ORDER);
        $stmt->bindValue(':nodeTypeStructureUnit', NodeType::STRUCTURE_UNIT);
        $stmt->bindValue(':structureUnitTypeCompany', StructureUnitType::COMPANY);
        $stmt->execute();

        $groupByEmployees = !empty($employeesWhere) || $groupAllIsSelected;

        $result = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $deb = $row['DEBTOR'];
            $prj = $row['PROJECT'];

            if ($deb === null || $prj === null) {
                continue;
            }

            $groupKey = $groupByEmployees ? 'employee_' . $row['SHORTCUT'] : trim(substr($row['COMPANY'], 0, 3));

            if (!isset($result[$groupKey][$deb][$prj])) {
                $result[$groupKey][$deb][$prj] = [
                    'bookings'  => [$row['SHORTCUT'] => [$row]],
                    'company'   => $row['COMPANY'],
                    'sumAcc'    => (int)(BookingType::ID_REPORT_ACCOUNTABLE == $row['BT_ID'])
                ];
            } else {
                $result[$groupKey][$deb][$prj]['bookings'][$row['SHORTCUT']][] = $row;
                // count accountable rows
                $result[$groupKey][$deb][$prj]['sumAcc'] += (int)(BookingType::ID_REPORT_ACCOUNTABLE == $row['BT_ID']);
            }
        }

        return [
            'FROM'      => $periodFrom->get(Zend_Date::TIMESTAMP),
            'UNTIL'     => $periodUntil->get(Zend_Date::TIMESTAMP),
            'BOOKINGS'  => $result,
            'TIMESTAMP' => $timestamp
        ];
    }

    /**
     * Gets the data for the 'rebook' report.
     */
    public static function loadRebook($profile, $timestamp, $db, $user, $config, $log)
    {
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);

        //Fallback date if the from date is not before the reference day
        $referenceDay = (new Zend_Date())->setYear(1999);
        $date = new Zend_Date();

        $date->sub('15', Zend_Date::DAY);
        $quarter = ceil(($date->get(Zend_Date::MONTH_SHORT)) / 3);
        $month = ($quarter - 1) * 3 + 1;
        $date->setMonth($month);

        $date->setDay(1);
        $date->setHour(0);
        $date->setMinute(0);
        $date->setSecond(0);

        // The reference day is the first day of the quarter the day 15 days before current day belongs to.
        if ($periodFrom->isEarlier($date)) {
            $referenceDay = $date;
        }

        // fetch target CC
        $sql = "
            SELECT
                n.id,
                CASE
                    WHEN nv.long_name IS NOT null THEN nv.name || ' [' || nv.long_name || ']'
                    WHEN nv.customer_name IS NOT null THEN nv.name || ' [' || nv.customer_name || ']'
                    ElSE nv.name
                END AS name,
                bt.name AS bt_name,
                to_char(node_accounting.last_accounting_date,'DD.MM.YYYY') as last_accounting_date,
                to_char(nv.lock_date,'DD.MM.YYYY') as lock_date
            FROM bc_chart_profil_x_node chpxn
            INNER JOIN node n ON n.id = chpxn.single_node_id
            INNER JOIN node_version nv
                ON nv.node_id = n.id
                AND nv.version_state_id = 1
            LEFT JOIN booking_type bt ON nv.booking_type_id = bt.id
            LEFT JOIN (
                WITH RECURSIVE nodes AS (
                    SELECT
                        node.id AS node_id,
                        max_date.accounting_period_to AS root_node_accounting_period_to
                    FROM node
                             LEFT JOIN (
                        SELECT
                            node_id,
                            max(accounting_period_to) AS accounting_period_to
                        FROM zahlungspaket
                            LEFT JOIN zahlungspaketstatus ON zahlungspaketstatus.id = zahlungspaket.zahlungspaketstatus_id
                        WHERE zahlungspaketstatus.status_kuerzel = 'FT'
                          AND zahlungspaket.next_id IS NULL
                          AND accounting_period_to IS NOT NULL
                        GROUP BY node_id
                    ) max_date ON max_date.node_id = node.id
                    WHERE max_date.accounting_period_to IS NOT NULL
                    UNION ALL
                    SELECT
                        node.id AS node_id,
                        nodes.root_node_accounting_period_to
                    FROM node
                             INNER JOIN nodes ON nodes.node_id = node.parent_node_id
                )
                SELECT
                    node_id,
                    root_node_accounting_period_to AS last_accounting_date
                FROM nodes
            ) node_accounting ON n.id = node_accounting.node_id
            WHERE chpxn.bc_chart_profil_id  = :PROFILE_ID
            AND (chpxn.single_node_types & ". SingleNodeType::REBOOK_TO_CC .") > 0
            ORDER BY name
        ";

        $statement = $db->prepare($sql);
        $statement->bindValue(':PROFILE_ID', $profile->ID);
        $statement->execute();
        $targetCCRow = $statement->fetch();

        $targetCC = null;
        if (!empty($targetCCRow)) {
            $targetCC = [
                'id' => $targetCCRow['ID'],
                'name' => $targetCCRow['NAME'],
                'lastAccountingDate' => $targetCCRow['LAST_ACCOUNTING_DATE'],
                'lockDate' => $targetCCRow['LOCK_DATE']
            ];

            if (!empty($targetCCRow['BT_NAME'])) {
                $targetCC['name'] .= ' (' . $targetCCRow['BT_NAME'] . ')';
                $targetCC['bookingType'] = $targetCCRow['BT_NAME'];
            }
        }

        // Determine profile nodes
        $nodes = ChartProfil::getProfileNodes($profile->ID, $db, false);

        // Determine profile companies
        $companies = ChartProfil::getProfileCompanies($profile->ID);
        // Determine profile divisions
        $divisions = ChartProfil::getProfileDivisions($profile->ID);
        // Determine the selected users
        $employees = ChartProfil::getProfileEmployees($db, $profile->ID);

        $userAndGroupIds = self::getUserAndGroupIdsOfProfile($profile->ID, $db);

        // if resolved employee selection results in no employees at all (e.g. due to empty groups), then report has no data
        if (!empty($userAndGroupIds) && self::noEmployeeSelected($profile->ID, [
                "companies" => $companies,
                "divisions" => $divisions,
                "employees" => $employees['id']
            ])
        ) {
            $nodes = [Node::DUMMY];
            $employees['id'] = [Employee::DUMMY];
        }

        $companiesWhere = '';
        $divisionsWhere = '';
        $employeesWhere = '';
        $employeesFromBookings = [];
        if (!empty($companies)) {
            $companiesWhere = "employment_period.company_id IN (" . implode(',', $companies) . ")";
        }

        if (!empty($divisions)) {
            $divisionsWhere = "employee_function_period.division_id IN (" . implode(',', $divisions) . ")";
        }

        if (!empty($employees['id'])) {
            $employeesWhere = QueryBuilder::createInClause('cemployee_id', $employees['id']);
        }

        $bookingTypes = ChartProfil::getBookingTypes($profile->ID);
        if (empty($bookingTypes)) {
            $bookingTypesWhere = " ";
        } else {
            $bookingTypesWhere = " AND tpwwh.cbooking_type_id IN (" . implode(',', $bookingTypes) . ") ";
        }

        if (empty($profile->NOTIZTEXT)) {
            $bookingNoteWhere = " ";
        } else {
            $bookingNoteWhere = " AND (lower(tpwwh.cnote) LIKE lower('%" . $profile->NOTIZTEXT . "%') OR lower(tpwwh.cnoteal) LIKE lower('%" . $profile->NOTIZTEXT . "%')) ";
        }

        if (!empty($nodes)) {
            $nodeJoin = "
                (
                    WITH RECURSIVE nodes AS (
                        SELECT
                            node.id,
                            nv.name,
                            nv.long_name,
                            nv.customer_name,
                            nv.is_holiday,
                            node.parent_node_id,
                            nv.booking_type_id
                        FROM node
                        INNER JOIN node_version nv on node.id = nv.node_id
                            AND nv.version_state_id = 1
                        WHERE " . QueryBuilder::createInClause('node.id', $nodes) . "
                        UNION ALL
                        SELECT
                            node.id,
                            nv.name,
                            nv.long_name,
                            nv.customer_name,
                            nv.is_holiday,
                            node.parent_node_id,
                            nv.booking_type_id
                        FROM node
                        INNER JOIN node_version nv on node.id = nv.node_id
                            AND nv.version_state_id = 1
                        INNER JOIN nodes ON nodes.id = node.parent_node_id
                    )
                    SELECT * FROM nodes
                )
            ";
        } else {
            $nodeJoin = "
                (
                    SELECT
                        node.id,
                        node_version.name,
                        node_version.long_name,
                        node_version.customer_name,
                        node_version.is_holiday,
                        node.parent_node_id,
                        node_version.booking_type_id
                        FROM node
                        INNER JOIN node_version
                            ON node_version.node_id = node.id
                            AND node_version.version_state_id = 1
                )
            ";
        }

        $sql = "
            SELECT
                e.shortcut,
                nn.id,
                nn.is_holiday,
                CASE
                    WHEN nn.long_name IS NOT null THEN nn.name || ' [' || nn.long_name || ']'
                    WHEN nn.customer_name IS NOT null THEN nn.name || ' [' || nn.customer_name || ']'
                    ElSE nn.name
                END AS name,
                tpwwh.cid,
                to_char(tpwwh.cdate, 'DD.MM.YYYY') AS cdate,
                tpwwh.chours,
                tpwwh.choursal,
                tpwwh.cnote,
                tpwwh.cnoteal,
                bt.name AS bt_name,
                default_bt.name as default_bt_name,
                CASE WHEN tpwwh.cdate <= node_accounting.last_accounting_date THEN 1 ELSE 0 END AS is_before_last_accounting,
                CASE WHEN tpwwh.cdate < TO_DATE(:REFERENCE_DAY, 'YYYY-MM-DD') THEN 1 ELSE 0 END AS is_before_reference_day
            FROM tpwworkinghours tpwwh
            INNER JOIN $nodeJoin nn ON nn.id = tpwwh.cnode_id
            INNER JOIN employee e on tpwwh.cemployee_id = e.id
            LEFT JOIN employment_period ON
                employment_period.employee_id = e.id
                AND tpwwh.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
            LEFT JOIN employee_function_period
                ON employee_function_period.employee_id = tpwwh.cemployee_id
                AND tpwwh.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
            LEFT JOIN booking_type bt ON tpwwh.cbooking_type_id = bt.id
            LEFT JOIN booking_type default_bt ON nn.booking_type_id = default_bt.id
            LEFT JOIN (
                WITH RECURSIVE nodes AS (
                    SELECT
                        node.id AS node_id,
                        max_date.accounting_period_to AS root_node_accounting_period_to
                    FROM node
                             LEFT JOIN (
                        SELECT
                            node_id,
                            max(accounting_period_to) AS accounting_period_to
                        FROM zahlungspaket
                                 LEFT JOIN zahlungspaketstatus ON zahlungspaketstatus.id = zahlungspaket.zahlungspaketstatus_id
                        WHERE zahlungspaketstatus.status_kuerzel = 'FT'
                          AND zahlungspaket.next_id IS NULL
                          AND accounting_period_to IS NOT NULL
                        GROUP BY node_id
                    ) max_date ON max_date.node_id = node.id
                    WHERE max_date.accounting_period_to IS NOT NULL
                    UNION ALL
                    SELECT
                        node.id AS node_id,
                        nodes.root_node_accounting_period_to
                    FROM node
                             INNER JOIN nodes ON nodes.node_id = node.parent_node_id
                )
                SELECT
                    node_id,
                    max(root_node_accounting_period_to) AS last_accounting_date
                FROM nodes
                GROUP BY node_id
            ) node_accounting
                ON nn.id = node_accounting.node_id
            WHERE tpwwh.cnode_id IS NOT null
            AND tpwwh.cdate BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
            " . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
            $bookingTypesWhere
            $bookingNoteWhere
            ORDER BY tpwwh.cdate, e.shortcut, nn.name
        ";

        $statement = $db->prepare($sql);

        $statement->bindValue(':PERIOD_FROM', $periodFrom->get(Iso_Date::ORACLE_DATE));
        $statement->bindValue(':PERIOD_UNTIL', $periodUntil->get(Iso_Date::ORACLE_DATE));
        $statement->bindValue(':REFERENCE_DAY', $referenceDay->get(Iso_Date::ORACLE_DATE));
        $statement->execute();

        $elements = [];
        $sum = 0;

        $bookingsBeforeLastAccountingWarning = false;
        $grantedJiraProjects = self::getGrantedJiraProjects($config, $log, $user->getKuerzel());

        while (false !== ($item = $statement->fetch(Zend_Db::FETCH_ASSOC))) {
            if (1 == $item['IS_BEFORE_LAST_ACCOUNTING']) {
                $bookingsBeforeLastAccountingWarning = true;
            }

            $hours = empty($item['CHOURS']) ? 0.0 : General::toFloat($item['CHOURS']);
            $hoursAl = empty($item['CHOURSAL']) ? 0.0 : General::toFloat($item['CHOURSAL']);

            $note = TPWWorkingHours::buildCompleteWorkingsHoursNote([$item['CNOTE'], $item['CNOTEAL']]);

            $sumItem = floatval($hours + $hoursAl);
            if ($sumItem == 0.0) {
                continue;
            }
            $sum += $sumItem;

            $elements[] = [
                'bookingId' => $item['CID'],
                'date' => $item['CDATE'],
                'employee' => $item['SHORTCUT'],
                'ccId' => $item['ID'],
                'cc' => $item['NAME'],
                'bookingType' => $item['BT_NAME'],
                'isHoliday' => $item['IS_HOLIDAY'],
                'defaultBookingType' => $item['DEFAULT_BT_NAME'],
                'hours' =>  $hours + $hoursAl,
                'note' =>  $note,
                'pdbIds' => General::extractJiraIds($note, $grantedJiraProjects),
                'canOnlyRebookedByFinance' => max(intval($item['IS_BEFORE_LAST_ACCOUNTING']), intval($item['IS_BEFORE_REFERENCE_DAY']))
            ];
            $employeesFromBookings[] = strtolower($item['SHORTCUT']);
        }

        // Fetch the user shortcuts from DB (if there are any)
        $booked = ChartProfil::getUserAndGroupNamesOfProfile($profile->ID, $db);
        if (empty($booked['employees']) && empty($booked['groups']) && !empty($employeesFromBookings)) {
            $booked['employees'] = array_unique($employeesFromBookings);
            sort($booked['employees']);
        }

        // Update the Act-Hours value in DB
        ChartProfil::setActProfileValue($profile, $db, $sum);

        $flags = [
            'bookingBeforeLastAccounting' => $bookingsBeforeLastAccountingWarning
        ];

        return [
            'BOOKINGS' => ['bookings' => $elements, 'flags' => $flags],
            'FROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
            'UNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP),
            'EMPLOYEEINFO' => ['booked' => isset($booked) ? $booked : ''],
            'SUMMARY' => ['targetCC' => $targetCC],
            'ACTHOURS' => $sum
        ];
    }

    /**
     * Gets the data for the 'MonthEnd' report.
     */
    public static function loadMonthEnd($profile, $timestamp, $db, $cache, $translate)
    {
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);

        $monthEndCols = ChartProfil::getAdditionalColumns($profile->ID);
        $chartProfile = self::getChartProfile($profile->ID);
        $additionalColsObj = new AdditionalCol();
        $extraColsTranslations = $additionalColsObj->getAdditionalCols($translate->getLocale(), ReportType::MONTH_END);
        $len = count($extraColsTranslations);

        for ($i = 0; $i < $len; $i++) {
            if ($extraColsTranslations[$i]['ID'] == AdditionalCol::MONTH_END_ACCOUNTABLE_HOURS) {
                unset($extraColsTranslations[$i]);
                array_push($extraColsTranslations, [
                    "ID" => '20a',
                    "NAME" => $translate->translate('hours_accountable_monthend'),
                    "TOOLTIP" => null,
                    "REPORT_TYPE" => "M"
                ]);
                array_push($extraColsTranslations, [
                    "ID" => '20b',
                    "NAME" => $translate->translate('hours_not_accountable_monthend'),
                    "TOOLTIP" => null,
                    "REPORT_TYPE" => "M"
                ]);
                array_push($extraColsTranslations, [
                    "ID" => '20c',
                    "NAME" => $translate->translate('hours_accountable_percent_monthend'),
                    "TOOLTIP" => null,
                    "REPORT_TYPE" => "M"
                ]);
                $extraColsTranslations = array_values($extraColsTranslations);
                break;
            }
        }

        $monthRange = Date::generateMonthRange($periodFrom, $periodUntil);
        $groupAllIsSelected = ChartProfil::isEmployeeGroupSelected($profile->ID);

        // Determine selected users
        $employees = ChartProfil::getProfileEmployees($db, $profile->ID, false);
        // Determine selected companies
        $companies = ChartProfil::getProfileCompanies($profile->ID);
        // Determine profile divisions
        $divisions = ChartProfil::getProfileDivisions($profile->ID);

        $userAndGroupIds = self::getUserAndGroupIdsOfProfile($profile->ID, $db);

        // if resolved employee selection results in no employees at all (e.g. due to empty groups), then report has no data
        if (!empty($userAndGroupIds) && self::noEmployeeSelected($profile->ID, [
                "companies" => $companies,
                "divisions" => $divisions,
                "employees" => $employees['id']
            ])
        ) {
            $employees['id'] = [Employee::DUMMY];
        }

        $withEmployeesWhere = '';
        if (!$groupAllIsSelected) {
            $employeesWhere = '';
            $companiesWhere = '';
            $divisionsWhere = '';

            if (!empty($employees['id'])) {
                $employeesWhere = QueryBuilder::createInClause('employee.id', $employees['id']);
            }
            if (!empty($companies)) {
                $companiesWhere = "employment_period.company_id IN (" . implode(',', $companies) . ")";
            }
            if (!empty($divisions)) {
                $divisionsWhere = "employee_function_period.division_id IN (" . implode(',', $divisions) . ")";
            }
            $withEmployeesWhere = QueryBuilder::conditionOr("AND", [$employeesWhere, $companiesWhere, $divisionsWhere]);
        }

        $withEmployeesZeroHoursWhere = '';
        if ($profile->ZERO_HOURS_PER_WEEK == 0) {
            $withEmployeesZeroHoursWhere = "WHERE " . QueryBuilder::conditionOr("", ['zero_hours_per_week = 0', QueryBuilder::createInClause('employee_id', $employees['id'])]);
        }

        $sqlWith = [
            "months AS (
                WITH RECURSIVE months AS (
                    SELECT
                        date_trunc('year', to_date(:PERIOD_FROM, 'YYYY-MM-DD'))::date AS dt
                    UNION ALL
                    SELECT
                        (months.dt + INTERVAL '1' MONTH)::date AS dt
                    FROM months
                    WHERE dt + INTERVAL '1' MONTH < date_trunc('year', to_date(:PERIOD_UNTIL, 'YYYY-MM-DD') + INTERVAL '12' MONTH) - INTERVAL '1' DAY
                )
                SELECT
                    dt AS month_start,
                    (dt + INTERVAL '1' MONTH - INTERVAL '1' DAY)::date AS month_end
                from months
            )",
            "employee_months AS (
                SELECT
                    employee_id,
                    month
                FROM (
                    SELECT
                        employee.id AS employee_id,
                        months.month_start AS month,
                        CASE WHEN SUM(SUM(hours_per_week)) OVER (PARTITION BY employee.id) > 0 THEN 0 ELSE 1 END AS zero_hours_per_week
                    FROM employee
                    CROSS JOIN months
                    INNER JOIN employment_period
                        ON employment_period.employee_id = employee.id
                        AND months.month_start 
                            BETWEEN date_trunc('month', employment_period.entry_date) 
                                AND coalesce(
                                    date_trunc('month', employment_period.leaving_date) + INTERVAL '1' MONTH - INTERVAL '1' DAY, 
                                    max_date()
                                )
                    LEFT JOIN employee_function_period
                        ON employee_function_period.employee_id = employee.id
                        AND months.month_start 
                            BETWEEN date_trunc('month', employee_function_period.start_date) 
                                AND coalesce(
                                    date_trunc('month', employee_function_period.end_date) + INTERVAL '1' MONTH - INTERVAL '1' DAY, 
                                    max_date()
                                )
                    WHERE months.month_start BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
                    $withEmployeesWhere
                    GROUP BY employee.id, months.month_start
                ) t
                $withEmployeesZeroHoursWhere
            )",
            "hour_changes AS (
                SELECT
                    bc.month,
                    bc.affected_employee_id,
                    bc.commentary
                FROM (
                    SELECT
                        max(datetime) AS latest_date,
                        month,
                        affected_employee_id
                    FROM tpw_balance_change
                    WHERE hours_change IS NOT NULL
                    GROUP BY month, affected_employee_id
                ) tpwbc_latest
                INNER JOIN tpw_balance_change bc
                    ON tpwbc_latest.latest_date = bc.datetime
                    AND tpwbc_latest.month = bc.month
                    AND tpwbc_latest.affected_employee_id = bc.affected_employee_id
                WHERE bc.hours_change <> 0
            )",
            "holiday_changes AS (
                SELECT
                    bc.month,
                    bc.affected_employee_id,
                    bc.commentary
                FROM (
                    SELECT
                        max(datetime) AS latest_date,
                        month,
                        affected_employee_id
                    FROM tpw_balance_change
                    WHERE holidays_change IS NOT NULL
                    GROUP BY month, affected_employee_id
                ) tpwbc_latest
                INNER JOIN tpw_balance_change bc
                    ON tpwbc_latest.latest_date = bc.datetime
                    AND tpwbc_latest.month = bc.month
                    AND tpwbc_latest.affected_employee_id = bc.affected_employee_id
                WHERE bc.holidays_change <> 0
            )",
            "employment_periods AS (
                SELECT
                    employee_months.employee_id,
                    employee_months.month,
                    string_agg(DISTINCT coalesce(employment_period.hours_per_week::varchar, '0'), ' + ') AS hours_per_week,
                    string_agg(DISTINCT company.shortcut, ' + ') AS company,
                    string_agg(DISTINCT employment_status.name_de, ' + ') AS employment_status,
                    count(remote_work_days.day) AS home_office_days
                FROM employee_months
                LEFT JOIN employment_period
                    ON employment_period.employee_id = employee_months.employee_id
                    AND employee_months.month 
                        BETWEEN date_trunc('month', employment_period.entry_date) 
                            AND coalesce(
                                date_trunc('month', employment_period.leaving_date) + INTERVAL '1' MONTH - INTERVAL '1' DAY,
                                max_date()
                            )
                LEFT JOIN intranet.remote_work_days
                ON remote_work_days.employee_id = employee_months.employee_id
                AND remote_work_days.day BETWEEN employee_months.month AND employee_months.month + INTERVAL '1 month - 1 day'                        
                LEFT JOIN company ON company.id = employment_period.company_id
                LEFT JOIN employment_status ON employment_status.id = employment_period.status_id
                GROUP BY employee_months.employee_id, employee_months.month
            )"
        ];
        $sqlColumns = [
            "employee.shortcut",
            "employee.last_name",
            "employee.first_name",
            "to_char(employee_months.month, 'YYYY/MM')  AS month",
            "tpwmonth.chrscurmonthactual                AS act_worked_hours",
            "tpwmonth.chrspayregular                    AS regular_hours_to_pay",
            "tpwmonth.chrspaynight                      AS night_hours_to_pay",
            "tpwmonth.chrspaypublichday                 AS holiday_hours_to_pay",
            "tpwmonth.cholidayscurmonthactual           AS holiday_days",
            "tpwmonth.chrscurmonthremain                AS remaining_hours",
            "tpwmonth.cholidayscurmonthremain           AS remaining_holiday_days",
            "tpwmonth.chrscurmonthchange                AS current_hours_month_change",
            "tpwmonth.cholidayscurmonthchange           AS current_holiday_month_change",
            "hour_changes.commentary                    AS hours_change_reason",
            "holiday_changes.commentary                 AS holiday_change_reason",
            "CASE 
                WHEN 
                    employment_period.hours_per_week < 40
                    AND employee.overtime_payment_model_id = "
            . self::OVERTIME_PAYMENT_MODEL_ID_CASHABLE_WITHOUT_SURCHARGE . "
                THEN 'N'
                ELSE ''
            END                                         AS pay_overtime_surcharge,
            employment_periods.home_office_days         AS home_office_days
"
        ];
        $sqlJoin = [
            "INNER JOIN employee ON employee.id = employee_months.employee_id",
            "LEFT JOIN tpwmonth
                ON tpwmonth.cemployee_id = employee_months.employee_id
                AND tpwmonth.cdate = employee_months.month",
            "LEFT JOIN hour_changes
                ON hour_changes.affected_employee_id = employee_months.employee_id
                AND hour_changes.month = employee_months.month",
            "LEFT JOIN holiday_changes
                ON holiday_changes.affected_employee_id = employee_months.employee_id
                AND holiday_changes.month = employee_months.month",
            "LEFT JOIN employment_periods
                    ON employment_periods.employee_id = employee_months.employee_id
                    AND employment_periods.month = employee_months.month",
            "LEFT JOIN employment_period
                ON employment_period.employee_id = employee.id
                AND employment_period.entry_date <= employment_periods.month 
                AND (employment_period.leaving_date >= employment_periods.month OR employment_period.leaving_date IS NULL)"
        ];
        $sqlOrder = [
            "employee.last_name",
            "employee_months.month"
        ];

        if (in_array(AdditionalCol::MONTH_END_ACT_OVERTIME, $monthEndCols)) {
            $sqlColumns[] = "coalesce(tpwmonth.chrscurmonthactual - tpwmonth.chrscurmonthtarget, 0) AS act_overtime";
        }

        if (in_array(AdditionalCol::MONTH_END_COMPANY, $monthEndCols)
            || in_array(AdditionalCol::MONTH_END_WEEKLY_HRS, $monthEndCols)
            || in_array(AdditionalCol::MONTH_END_STATUS, $monthEndCols)
            || $chartProfile['ONE_TAB_PER_COMPANY'] === '1'
        ) {
            if (in_array(AdditionalCol::MONTH_END_COMPANY, $monthEndCols) || $chartProfile['ONE_TAB_PER_COMPANY'] === '1') {
                $sqlColumns[] = "employment_periods.company";
            }
            if (in_array(AdditionalCol::MONTH_END_WEEKLY_HRS, $monthEndCols)) {
                $sqlColumns[] = "employment_periods.hours_per_week";
            }
            if (in_array(AdditionalCol::MONTH_END_STATUS, $monthEndCols)) {
                $sqlColumns[] = "employment_periods.employment_status AS status";
            }
        }

        if (in_array(AdditionalCol::MONTH_END_DIVISION, $monthEndCols)
            || in_array(AdditionalCol::MONTH_END_RESPONSIBLE_LEADING, $monthEndCols)
        ) {
            $sqlWith[] = "
                employee_function_periods AS (
                    SELECT
                        employee_months.employee_id,
                        employee_months.month,
                        string_agg(
                        DISTINCT
                        CASE 
                            WHEN division.id IS NOT null 
                                THEN division.name || ' (' || division.long_name || ')' 
                            ELSE null 
                        END, 
                        ' + '
                    ) AS division,
                    string_agg(DISTINCT staff_manager.shortcut, ' + ') AS staff_manager
                    FROM employee_months
                    LEFT JOIN employee_function_period
                        ON employee_function_period.employee_id = employee_months.employee_id
                        AND employee_months.month 
                            BETWEEN date_trunc('month', employee_function_period.start_date) 
                                AND coalesce(
                                    date_trunc('month', employee_function_period.end_date) + INTERVAL '1' MONTH - INTERVAL '1' DAY,
                                    max_date()
                                )
                    LEFT JOIN division ON division.id = employee_function_period.division_id
                    LEFT JOIN employee staff_manager ON staff_manager.id = employee_function_period.staff_manager_id
                    GROUP BY employee_months.employee_id, employee_months.month
                )
            ";
            $sqlJoin[] = "
                LEFT JOIN employee_function_periods
                    ON employee_function_periods.employee_id = employee_months.employee_id
                    AND employee_function_periods.month = employee_months.month
            ";

            if (in_array(AdditionalCol::MONTH_END_DIVISION, $monthEndCols)) {
                $sqlColumns[] = "employee_function_periods.division";
            }
            if (in_array(AdditionalCol::MONTH_END_RESPONSIBLE_LEADING, $monthEndCols)) {
                $sqlColumns[] = "employee_function_periods.staff_manager";
            }
        }

        if (in_array(AdditionalCol::MONTH_END_HOLIDAY_PER_YEAR, $monthEndCols)) {
            $sqlWith[] = "years AS (
                SELECT DISTINCT date_trunc('year', month_start) AS year FROM months
            )";
            $sqlColumns[] = "holidays.holidays AS holiday";
            $sqlJoin[] = "
                LEFT JOIN (
                    SELECT year,
                      employee_id,
                      holidays
                    FROM
                      (
                        SELECT 
                            years.year AS year,
                            employee_id,
                            tpw_annual_holiday.holidays,
                            ROW_NUMBER() OVER (PARTITION BY employee_id,years.year ORDER BY tpw_annual_holiday.year DESC) AS rn
                        FROM years
                        LEFT JOIN tpw_annual_holiday
                          ON years.year >= tpw_annual_holiday.year
                      ) t
                    WHERE rn = 1
                ) holidays ON holidays.employee_id = employee.id AND holidays.year = date_trunc('year', employee_months.month)
            ";
        }

        if (in_array(AdditionalCol::MONTH_END_HOLIDAY_MANAGER, $monthEndCols)) {
            $sqlColumns[] = "holiday_manager.shortcut AS holiday_manager";
            $sqlJoin[] = "
                LEFT JOIN employee holiday_manager
                    ON holiday_manager.id = employee.holiday_manager_id
            ";
        }

        if (in_array(AdditionalCol::MONTH_END_ENTRY_DATE, $monthEndCols)) {
            $sqlColumns[] = "to_char(first_period.entry_date, 'DD.MM.YYYY') AS emp_entry_date";
            $sqlJoin[] = "
                INNER JOIN (
                    SELECT
                        employee_id,
                        min(entry_date) AS entry_date
                    FROM employment_period
                    GROUP BY employee_id
                ) first_period
                    ON first_period.employee_id = employee.id
            ";
        }

        if (in_array(AdditionalCol::MONTH_END_LEAVING_DATE, $monthEndCols)) {
            $sqlColumns[] = "to_char(last_period.leaving_date, 'DD.MM.YYYY') AS emp_leaving_date";
            $sqlJoin[] = "
                INNER JOIN (
                    select distinct on (employee_id)
                        employee_id,
                        first_value(entry_date::date) OVER w AS entry_date,
                        first_value(leaving_date::date) OVER w AS leaving_date
                    from employment_period
                    WINDOW w AS (
                        PARTITION BY employee_id ORDER BY entry_date DESC
                        ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
                    )
                ) last_period
                    ON last_period.employee_id = employee.id
            ";
        }

        if (in_array(AdditionalCol::MONTH_END_SICK_LEAVE_DAYS, $monthEndCols)
            || in_array(AdditionalCol::MONTH_END_TAKEN_HOLIDAYS, $monthEndCols)
            || in_array(AdditionalCol::MONTH_END_ACCEPTED_HOLIDAYS, $monthEndCols)
        ) {
            $sqlWith[] = "
                profile_employees AS (
                    SELECT
                        DISTINCT employee_id AS id
                    FROM employee_months
                )
            ";
        }

        if (in_array(AdditionalCol::BUSINESS_KILOMETERS, $monthEndCols)) {
            $sqlColumns[] = "
                business_kilometers.kilometers AS business_kilometers
            ";

            $sqlJoin[] = "
                LEFT JOIN business_kilometers ON employee.id = business_kilometers.employee_id
                AND business_kilometers.month = employee_months.month
            ";
        }

        if (in_array(AdditionalCol::MONTH_END_SICK_LEAVE_DAYS, $monthEndCols)
            || in_array(AdditionalCol::MONTH_END_TAKEN_HOLIDAYS, $monthEndCols)
        ) {
            $sqlWith[] = "
                employee_months_year AS (
                    SELECT
                        profile_employees.id AS employee_id,
                        months.month_start AS month
                    FROM profile_employees
                    CROSS JOIN months
                )
            ";
            $sqlWith[] = "
                holiday_illness AS (
                    SELECT
                        v.employee_id,
                        v.month,
                        v.sum_illness,
                        SUM(coalesce(v.num_holidays, 0)) OVER (PARTITION BY v.employee_id, date_trunc('year', v.month) ORDER BY v.month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS sum_holidays
                    FROM (
                        SELECT
                            employee_months_year.employee_id AS employee_id,
                            employee_months_year.month AS month,
                            COUNT(CASE WHEN node_version.is_holiday = 1 THEN 1 ELSE null END) AS num_holidays,
                            ROUND(SUM(CASE
                                WHEN node_version.is_illness = 1 AND coalesce(employment_period.hours_per_week, 0) <> 0
                                THEN tpwworkinghours.chours / (employment_period.hours_per_week / 5)
                                ELSE 0
                            END)::DECIMAL, 1) AS sum_illness
                        FROM employee_months_year
                        LEFT JOIN tpwworkinghours
                            ON tpwworkinghours.cemployee_id = employee_months_year.employee_id
                            AND date_trunc('month', tpwworkinghours.cdate) = employee_months_year.month
                        LEFT JOIN node ON node.id = tpwworkinghours.cnode_id
                        LEFT JOIN node_version
                            ON node_version.node_id = node.id
                            AND node_version.version_state_id = 1
                        LEFT JOIN employment_period
                            ON employment_period.employee_id = tpwworkinghours.cemployee_id
                            AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                        GROUP BY employee_months_year.employee_id, employee_months_year.month
                    ) v
                )
            ";
            $sqlJoin[] = "
                LEFT JOIN holiday_illness
                    ON holiday_illness.employee_id = employee_months.employee_id
                    AND holiday_illness.month = employee_months.month
            ";

            if (in_array(AdditionalCol::MONTH_END_TAKEN_HOLIDAYS, $monthEndCols)) {
                $sqlColumns[] = "holiday_illness.sum_holidays AS taken_holidays";
            }
            if (in_array(AdditionalCol::MONTH_END_SICK_LEAVE_DAYS, $monthEndCols)) {
                $sqlColumns[] = "coalesce(holiday_illness.sum_illness, 0) AS sick_leave_days";
            }
        }

        if (in_array(AdditionalCol::MONTH_END_ACCEPTED_HOLIDAYS, $monthEndCols)) {
            $sqlWith[] = "
                days AS (
                    WITH RECURSIVE days AS (
                        SELECT
                            to_date(:PERIOD_FROM, 'YYYY-MM-DD') AS dt
                        UNION ALL
                        SELECT
                            (days.dt + INTERVAL '1' DAY)::date AS dt
                        FROM days
                        WHERE dt + INTERVAL '1' DAY < date_trunc('year', to_date(:PERIOD_UNTIL, 'YYYY-MM-DD') + INTERVAL '12' MONTH) - INTERVAL '1' DAY
                    )
                    SELECT
                        dt AS day,
                        date_trunc('month', dt)::date AS month
                    from days
                )
            ";
            $sqlWith[] = "
                working_days AS (
                    SELECT
                        days.day,
                        days.month,
                        id AS location_id
                    FROM days
                    CROSS JOIN location
                    LEFT JOIN tpwpublicholidays
                        ON tpwpublicholidays.clocation_id = location.id
                        AND tpwpublicholidays.cdate = days.day
                    WHERE tpwpublicholidays.cid IS NULL
                    AND (
                        (location.name = 'Dubai' AND to_char(days.day,'D') NOT IN ('7','6'))
                        OR (location.name != 'Dubai' AND to_char(days.day,'D') NOT IN('1','7'))
                    )
                )
            ";
            $sqlWith[] = "
                applications AS (
                    SELECT
                        profile_employees.id          AS employee_id,
                        holiday_application.date_from AS date_from,
                        holiday_application.date_till AS date_till
                    FROM profile_employees
                    LEFT JOIN holiday_application
                        ON profile_employees.id = holiday_application.employee_id
                    WHERE (
                        holiday_application.date_till BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND date_trunc('year', to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')) + INTERVAL '12' MONTH
                        OR holiday_application.date_from BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND date_trunc('year', to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')) + INTERVAL '12' MONTH
                        OR holiday_application.date_from  < to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND holiday_application.date_till > date_trunc('year', to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')) + INTERVAL '12' MONTH
                    )
                    AND holiday_application.state = 1
                )
            ";
            $sqlWith[] = "
                employee_accepted_applications AS (
                    SELECT
                        employee_id,
                        month,
                        SUM(coalesce(day_count, 0)) OVER (PARTITION BY employee_id, date_trunc('year', month) ORDER BY month DESC) AS days
                    FROM (
                        SELECT
                            profile_employees.id AS employee_id,
                            working_days.month - INTERVAL '1' MONTH AS month,
                            sum(CASE WHEN applications.date_from IS NOT null THEN 1 ELSE 0 END) AS day_count
                        FROM profile_employees
                        INNER JOIN employment_period
                            ON employment_period.employee_id = profile_employees.id
                        INNER JOIN working_days
                            ON working_days.location_id = employment_period.location_id
                            AND working_days.day BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                        LEFT JOIN applications
                            ON applications.employee_id = profile_employees.id
                            AND working_days.day BETWEEN applications.date_from AND applications.date_till
                        GROUP BY profile_employees.id, working_days.month
                    ) t
                )
            ";
            $sqlColumns[] = "coalesce(employee_accepted_applications.days, 0) AS accepted_holidays";

            $sqlJoin[] = "
                LEFT JOIN employee_accepted_applications
                    ON employee_accepted_applications.employee_id = employee_months.employee_id
                    AND employee_accepted_applications.month = employee_months.month
            ";
        }

        if (in_array(AdditionalCol::MONTH_END_ACCOUNTABLE_HOURS, $monthEndCols)) {
            $sqlWith[] = "
                accountable_hours AS (
                    SELECT
                        cemployee_id AS employee_id,
                        date_trunc('month', cdate) AS month,
                        SUM(CASE WHEN booking_type.name <> 'NA' THEN coalesce(chours, 0) + coalesce(choursal, 0) ELSE 0 END) AS accountable_hours,
                        SUM(CASE WHEN booking_type.name = 'NA' THEN coalesce(chours, 0) + coalesce(choursal, 0) ELSE 0 END) AS not_accountable_hours
                    FROM tpwworkinghours
                    INNER JOIN node
                        ON node.id = tpwworkinghours.cnode_id
                    INNER JOIN node_version
                        ON node_version.node_id = node.id
                        AND node_version.version_state_id = 1
                    INNER JOIN booking_type
                        ON booking_type.id = node_version.booking_type_id
                    WHERE cdate BETWEEN to_date(:PERIOD_FROM,'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL,'YYYY-MM-DD')
                    AND node_version.is_absence = 0
                    GROUP BY tpwworkinghours.cemployee_id, date_trunc('month', tpwworkinghours.cdate)
                )
            ";

            $sqlColumns[] = "coalesce(accountable_hours.accountable_hours, 0) AS accountable_hours";
            $sqlColumns[] = "coalesce(accountable_hours.not_accountable_hours, 0) AS not_accountable_hours";
            $sqlColumns[] = "
                CASE
                    WHEN coalesce(accountable_hours.accountable_hours, 0) = 0 
                        THEN 0
                    ELSE round(
                        (accountable_hours.accountable_hours / (accountable_hours.accountable_hours + coalesce(accountable_hours.not_accountable_hours, 0)))::DECIMAL, 2)
                END AS accountable_hours_perc
            ";

            $sqlJoin[] = "
                LEFT JOIN accountable_hours
                    ON accountable_hours.employee_id = employee_months.employee_id
                    AND accountable_hours.month = employee_months.month
            ";
        }

        if (in_array(AdditionalCol::MONTH_END_DAYS_IN_OFFICE, $monthEndCols)) {
            $sqlWith[] = "
                office_day_counts AS (
                    SELECT
                        count(office_days.day) AS day_count,
                        employee_months.month,
                        employee_months.employee_id
                    FROM intranet.office_days
                    INNER JOIN employee_months
                    ON office_days.employee_id = employee_months.employee_id
                    AND office_days.day BETWEEN employee_months.month AND employee_months.month + INTERVAL '1 month - 1 day'
                    GROUP BY employee_months.employee_id, employee_months.month
			    )
            ";

            $sqlColumns[] = "coalesce(office_day_counts.day_count, 0) AS office_days";

            $sqlJoin[] = "
                LEFT JOIN office_day_counts
                    ON office_day_counts.employee_id = employee_months.employee_id
                    AND office_day_counts.month = employee_months.month
            ";
        }

        $sql = "
            WITH
                " . implode(",\n", $sqlWith) . "
            SELECT
                " . implode(",\n", $sqlColumns) . "
            FROM employee_months
            " . implode("\n", $sqlJoin) . "
            ORDER BY " . implode(", ", $sqlOrder) . "
        ";

        $statement = $db->prepare($sql);

        $statement->bindValue(':PERIOD_FROM', $periodFrom->get(Iso_Date::ORACLE_DATE));
        $statement->bindValue(':PERIOD_UNTIL', $periodUntil->get(Iso_Date::ORACLE_DATE));
        $statement->execute();

        $result = $statement->fetchAll();

        // employee aggregation
        if ($profile->ONE_LINE_PER_EMPLOYEE == 1) {
            $modifiedResult = [];
            $modifiedRow = [];
            $employee = null;
            $monthStart = null;
            $monthEnd = null;
            $employeesShortcutsSort = [];
            $employeesLastNameSort = [];

            foreach ($result as $row) {
                $employeesShortcutsSort[] = $row['SHORTCUT'];
            }

            array_multisort($employeesShortcutsSort, $result);

            foreach ($result as $row) {
                //if no employee is set (at the beginning) or if row with new employee is reached
                if ($employee == null || $employee != $row['SHORTCUT']) {
                    //if employee is set then
                    if ($employee) {
                        if ($monthStart != $monthEnd) {
                            $modifiedRow['MONTH'] = $monthStart . " - " . $monthEnd;
                        } else {
                            $modifiedRow['MONTH'] = $monthStart;
                        }
                        $modifiedRow['SHORTCUT'] = $employee;
                        array_push($modifiedResult, $modifiedRow);
                    }

                    $modifiedRow = [];
                    $employee = $row['SHORTCUT'];
                    $monthStart = $row['MONTH'];
                    $modifiedRow['LAST_NAME'] = $row['LAST_NAME'];
                    $modifiedRow['FIRST_NAME'] = $row['FIRST_NAME'];
                }

                $monthEnd = $row['MONTH'];

                //value from row with highest month is in aggregated row
                $keysHighest = [
                    'ACCEPTED_HOLIDAYS',
                    'HOLIDAY_MANAGER',
                    'STAFF_MANAGER',
                    'COMPANY',
                    'COMPANY_ID',
                    'DIVISION',
                    'DIVISION_ID',
                    'STATUS',
                    'TAKEN_HOLIDAYS',
                    'EMP_ENTRY_DATE_TIMESTAMP',
                    'EMP_ENTRY_DATE',
                    'EMP_LEAVING_DATE',
                    'HOLIDAY',
                    'FUNCTION_START_DATE',
                    'HOURS_PER_WEEK',
                    'REMAINING_HOURS',
                    'REMAINING_HOLIDAY_DAYS'
                ];

                foreach ($keysHighest as $key) {
                    if (array_key_exists($key, $row)) {
                        $modifiedRow[$key] = $row[$key];
                    }
                }

                //values from all rows are summed up
                $keysSum = [
                    'ACT_OVERTIME',
                    'HOLIDAY_DAYS',
                    'ACT_WORKED_HOURS',
                    'CURRENT_HOURS_MONTH_CHANGE',
                    'CURRENT_HOLIDAY_MONTH_CHANGE',
                    'REGULAR_HOURS_TO_PAY',
                    'NIGHT_HOURS_TO_PAY',
                    'HOLIDAY_HOURS_TO_PAY',
                    'ACCOUNTABLE_HOURS',
                    'NOT_ACCOUNTABLE_HOURS',
                    'SICK_LEAVE_DAYS',
                    'HOME_OFFICE_DAYS',
                    'OFFICE_DAYS'
                ];

                foreach ($keysSum as $key) {
                    if (array_key_exists($key, $row)) {
                        if (!array_key_exists($key, $modifiedRow)) {
                            $modifiedRow[$key] = 0;
                        }
                        $modifiedRow[$key] += $row[$key];
                    }
                }

                if (array_key_exists('BUSINESS_KILOMETERS', $row) && !is_null($row['BUSINESS_KILOMETERS'])) {
                    if (!array_key_exists('BUSINESS_KILOMETERS', $modifiedRow)) {
                        $modifiedRow['BUSINESS_KILOMETERS'] = 0;
                    }

                    $modifiedRow['BUSINESS_KILOMETERS'] += $row['BUSINESS_KILOMETERS'];
                }

                // Accountable hours percentage is special case
                if (array_key_exists("ACCOUNTABLE_HOURS_PERC", $row)) {
                    if ($modifiedRow["ACCOUNTABLE_HOURS"] == 0) {
                        $modifiedRow["ACCOUNTABLE_HOURS_PERC"] = 0;
                    } else {
                        $modifiedRow["ACCOUNTABLE_HOURS_PERC"] = number_format(($modifiedRow["ACCOUNTABLE_HOURS"] / ($modifiedRow["ACCOUNTABLE_HOURS"] + $modifiedRow['NOT_ACCOUNTABLE_HOURS'])), 2);
                    }
                }
            }

            //if employee is set then
            if (!empty($modifiedRow)) {
                if ($monthStart != $monthEnd) {
                    $modifiedRow['MONTH'] = $monthStart . " - " . $monthEnd;
                } else {
                    $modifiedRow['MONTH'] = $monthStart;
                }
                $modifiedRow['SHORTCUT'] = $employee;
                array_push($modifiedResult, $modifiedRow);
            }
            $result = $modifiedResult;

            foreach ($result as $row) {
                $employeesLastNameSort[] = $row['LAST_NAME'];
            }

            array_multisort($employeesLastNameSort, $result);
        }

        // Fetch the user shortcuts from DB (if there are any)
        $employeesShortcuts = ChartProfil::getUserAndGroupNamesOfProfile($profile->ID, $db);

        // save report data in cache
        $cacheId = $profile->ID . '_' . $timestamp;
        $cache->save([
            'REPORT_INFO' => [
                'TYPE'                        => $profile->ANZEIGE,
                'EMPLOYEES'                    => isset($employeesShortcuts) ? $employeesShortcuts : '',
                'PERIODFROM'                => $periodFrom->get(Zend_Date::TIMESTAMP),
                'PERIODUNTIL'                => $periodUntil->get(Zend_Date::TIMESTAMP),
                'MONTHS'                    => count($monthRange),
                'EXTRA_COLS'                => $monthEndCols,
                'EXTRA_COLS_TRANSLATIONS'    => $extraColsTranslations,
                'FLAGS' => [
                    'ZERO_HOURS_PER_WEEK' => $profile->ZERO_HOURS_PER_WEEK,
                    'ONE_LINE_PER_EMPLOYEE' => $profile->ONE_LINE_PER_EMPLOYEE,
                    'ONE_TAB_PER_COMPANY' => $profile->ONE_TAB_PER_COMPANY
                ]
            ],
            'DATA' => $result
        ], $cacheId);

        return [
            'BOOKINGS' => [
                'bookings' => $result,
                'flags' => [
                    'zeroHoursPerWeek' => $profile->ZERO_HOURS_PER_WEEK,
                    'oneLinePerEmployee' => $profile->ONE_LINE_PER_EMPLOYEE,
                    'oneTabPerCompany' => $profile->ONE_TAB_PER_COMPANY
                ]
            ],
            'FROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
            'UNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP),
            'EMPLOYEEINFO' => ['booked' => isset($employeesShortcuts) ? $employeesShortcuts : ''],
            'SUMMARY' => ['months' => count($monthRange)]
        ];
    }

    /**
     * Gets the data for the 'BT-Modifications' report.
     *
     * @author Steffen Glatzel <gla@isogmbh.de>
     */
    public static function loadBtModifications($profile, $timestamp, $db, $cache)
    {
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);

        $nodeId = Chartprofil::getProfileNodes($profile->ID, $db, false);

        //If no node is selected then use 'ISO-Gruppe'
        if (empty($nodeId)) {
            $nodeId = ['1'];
        }

        $sql = "
            SELECT
                'new' AS modification,
                to_char(first_version.modification_datetime, 'YYYY-MM-DD HH24:MI') AS dateTime,
                node.id,
                current_version.name AS node_name,
                node.is_leaf,
                current_version.sap_key_cost_object,
                current_version.sap_key_cost_center,
                node_type.shortcut AS node_type,
                employee.shortcut AS authorized
            FROM node
            INNER JOIN node_version current_version
                ON current_version.node_id = node.id
                AND current_version.version_state_id = 1
            INNER JOIN node_version first_version
                ON first_version.node_id = node.id
                AND first_version.previous_version_id IS NULL
            LEFT JOIN node_type ON node_type.id = node.node_type_id
            LEFT JOIN employee ON employee.id = current_version.requester_id
            WHERE date_trunc('day', first_version.modification_datetime) BETWEEN to_date(:period_from, 'YYYY-MM-DD') AND to_date(:period_till, 'YYYY-MM-DD')
            AND node.id IN (
                WITH RECURSIVE nodes AS (
                    SELECT
                        node.id
                    FROM node
                    WHERE " . QueryBuilder::createInClause('id', $nodeId) . "
                    UNION ALL
                    SELECT
                        node.id
                    FROM node
                    INNER JOIN nodes ON nodes.id = node.parent_node_id
                ) 
                SELECT id FROM nodes
            )
            
            UNION ALL
            
            SELECT
                'locked' AS modification,
                to_char(node_version.lock_date, 'YYYY-MM-DD HH24:MI') AS dateTime,
                node.id,
                node_version.name AS node_name,
                node.is_leaf,
                node_version.sap_key_cost_object,
                node_version.sap_key_cost_center,
                node_type.shortcut AS node_type,
                null AS authorized
            FROM node
            INNER JOIN node_version
                ON node_version.node_id = node.id
                AND node_version.version_state_id = 1
            LEFT JOIN node_type ON node_type.id = node.node_type_id
            WHERE node.bookable = 0
            AND date_trunc('day', node_version.lock_date) BETWEEN to_date(:period_from, 'YYYY-MM-DD') AND to_date(:period_till, 'YYYY-MM-DD')
            AND node.id IN (
                WITH RECURSIVE nodes AS (
                    SELECT
                        node.id
                    FROM node
                    WHERE " . QueryBuilder::createInClause('id', $nodeId) . "
                    UNION ALL
                    SELECT
                        node.id
                    FROM node
                    INNER JOIN nodes ON nodes.id = node.parent_node_id
                ) 
                SELECT id FROM nodes
            )
            
            ORDER BY modification DESC, dateTime
        ";

        $stmt = $db->prepare($sql);
        $stmt->bindValue(':period_from', $periodFrom->get(Iso_Date::ORACLE_DATE));
        $stmt->bindValue(':period_till', $periodUntil->get(Iso_Date::ORACLE_DATE));
        $stmt->execute();

        $result = $stmt->fetchAll();

        // Fetch the selected nodes from DB (if there are any)
        $nodes = ChartProfil::getNodesOfProfile($profile->ID, $db);

        // save report data in cache
        $cacheId = $profile->ID . '_' . $timestamp;
        $cache->save([
            'REPORT_INFO' => [
                'TYPE' => $profile->ANZEIGE,
                'PERIODFROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
                'PERIODUNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP),
                'NODES' => $nodes
            ],
            'DATA' => $result
        ], $cacheId);

        return [
            'FROM'        => $periodFrom->get(Zend_Date::TIMESTAMP),
            'UNTIL'        => $periodUntil->get(Zend_Date::TIMESTAMP),
            'DATA'        => ['list' => $result]
        ];
    }

    /**
     * Gets the data for the 'SAPKey-Hours' report.
     *
     * @author Steffen Glatzel <gla@isogmbh.de>
     */
    public static function loadSAPKeyHours($profile, $timestamp, $db, $cache)
    {
        // Determine period of this profile
        Date::determinePeriod($profile->ID, $zeitraum_von, $zeitraum_bis, $db);

        $profileNodes = Chartprofil::getProfileNodes($profile->ID, $db, false);

        //Determine leafes of profile (section beams and sets) as in statement
        $nodesIn = Chartprofil::getLeafesOfProfile($profile->ID, $db);

        // Determine profile companies
        $companies = ChartProfil::getProfileCompanies($profile->ID);
        // Determine profile divisions
        $divisions = ChartProfil::getProfileDivisions($profile->ID);
        // Determine the selected users
        $employees = ChartProfil::getProfileEmployees($db, $profile->ID);

        $userAndGroupIds = self::getUserAndGroupIdsOfProfile($profile->ID, $db);

        // if resolved employee selection results in no employees at all (e.g. due to empty groups), then report has no data
        if (!empty($userAndGroupIds) && self::noEmployeeSelected($profile->ID, [
                "companies" => $companies,
                "divisions" => $divisions,
                "employees" => $employees['id']
            ])
        ) {
            $employees['id'] = [Employee::DUMMY];
        }

        $companiesWhere = '';
        $divisionsWhere = '';
        $employeesWhere = '';
        $employeesFromBookings = [];

        if (!empty($companies)) {
            $companiesWhere = "employment_period.company_id IN (" . implode(',', $companies) . ")";
        }

        if (!empty($divisions)) {
            $divisionsWhere = "employee_function_period.division_id IN (" . implode(',', $divisions) . ")";
        }

        if (!empty($employees['id'])) {
            $employeesWhere = QueryBuilder::createInClause('cemployee_id', $employees['id']);
        }
        $bookingTypeColumns = "";
        $bookingTypeColumnsInner = "";

        $btObj = new BookingType();
        $bookingTypes = $btObj->getBookingTypesAsArray();

        foreach ($bookingTypes as $bookingType) {
            $bookingTypeColumns .= " SUM(CASE WHEN booking_type_name = '" . $bookingType
                . "' THEN coalesce(CHOURSAL, 0) + coalesce(CHOURS, 0)  ELSE 0 END) as "
                . $bookingType . ", ";
            $bookingTypeColumnsInner .= $bookingType.", ";
        }

        // Determine the bookings
        $sql = "
            SELECT
                sap_key,
                sap_key_node_type,
                sap_key_name,
                shortcut,
                sap_material_number,
                " . $bookingTypeColumns . "
                SUM(coalesce(choursal, 0) + coalesce(chours, 0)) as totals
            FROM (
                SELECT
                    cc_with_sap_key.sap_key,
                    CASE
                        WHEN cc_with_sap_key.sap_key LIKE '4____-%' THEN node_type.shortcut
                        ELSE cc_with_sap_key.sap_key_node_type
                    END AS sap_key_node_type,
                    CASE
                        WHEN cc_with_sap_key.sap_key LIKE '4____-%' THEN node_version.name
                        ELSE cc_with_sap_key.sap_key_name
                    END AS sap_key_name,
                    employee.shortcut,
                    employee.sap_material_number,
                    tpwworkinghours.choursal,
                    tpwworkinghours.chours,
                    booking_type.name AS booking_type_name
                FROM tpwworkinghours
                INNER JOIN node ON tpwworkinghours.cnode_id = node.id
                INNER JOIN node_version
                    ON node_version.node_id = node.id
                    AND node_version.version_state_id = 1
                INNER JOIN node_type ON node_type.id = node.node_type_id
                INNER JOIN employee ON tpwworkinghours.cemployee_id = employee.id
                INNER JOIN booking_type ON tpwworkinghours.cbooking_type_id = booking_type.ID
                LEFT JOIN employment_period ON
                    employment_period.employee_id = employee.id
                    AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                LEFT JOIN employee_function_period ON
                    employee_function_period.employee_id = tpwworkinghours.cemployee_id
                    AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
                LEFT JOIN cc_with_sap_key ON cc_with_sap_key.node_id = node.id
                WHERE tpwworkinghours.cdate BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
                " . QueryBuilder::conditionOr("AND", [
                $companiesWhere,
                $divisionsWhere,
                $employeesWhere
            ]) . "
                $nodesIn
            ) t
            GROUP BY sap_key, sap_key_node_type, sap_key_name, shortcut, sap_material_number
            ORDER BY sap_key, sap_key_name, shortcut
        ";

        $statement = $db->prepare($sql);
        $statement->bindValue(':PERIOD_FROM', $zeitraum_von->get(Iso_Date::ORACLE_DATE));
        $statement->bindValue(':PERIOD_UNTIL', $zeitraum_bis->get(Iso_Date::ORACLE_DATE));

        if ($statement->execute()) {
            $rows = $statement->fetchAll();
            $employeesFromBookings = [];

            foreach ($rows as $key => $item) {
                $employeesFromBookings[] = strtolower($item['SHORTCUT']);
            }

            // Fetch the selected nodes from DB (if there are any)
            $nodes = ChartProfil::getNodesOfProfile($profile->ID, $db);

            // Fetch the user shortcuts from DB (if there are any)
            $booked = ChartProfil::getUserAndGroupNamesOfProfile($profile->ID, $db);
            if (empty($booked['employees']) && empty($booked['groups']) && !empty($employeesFromBookings)) {
                $booked['employees'] = array_unique($employeesFromBookings);
                sort($booked['employees']);
            }

            // save report data in cache
            $cacheId = $profile->ID . '_' . $timestamp;
            $cache->save([
                'REPORT_INFO' => [
                    'TYPE' => $profile->ANZEIGE,
                    'EMPLOYEES' => isset($booked) ? $booked : '',
                    'NODES' => $nodes,
                    'PERIODFROM' => $zeitraum_von->get(Zend_Date::TIMESTAMP),
                    'PERIODUNTIL' => $zeitraum_bis->get(Zend_Date::TIMESTAMP)
                ],
                'DATA' => $rows
            ], $cacheId);

            return [
                'DATA' => ['result' => true, 'liste' => $rows],
                'EMPLOYEEINFO' => ['booked' => isset($booked) ? $booked : ''],
                'FROM' => $zeitraum_von->get(Zend_Date::TIMESTAMP),
                'UNTIL' => $zeitraum_bis->get(Zend_Date::TIMESTAMP)
            ];
        } else {
            return [
                'DATA' => ['result' => false, 'liste' => ''],
                'EMPLOYEEINFO' => '',
                'FROM' => $zeitraum_von->get(Zend_Date::TIMESTAMP),
                'UNTIL' => $zeitraum_bis->get(Zend_Date::TIMESTAMP)
            ];
        }
    }

    /**
     * Gets the data for the 'RebookLog' report.
     *
     * @author Steffen Glatzel <gla@isogmbh.de>
     */
    public static function loadRebookLog($profile, $timestamp, $db)
    {
        // Determine period of this profile
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);

        $nodeId = Chartprofil::getProfileNodes($profile->ID, $db);

        $nodeIdWhere = '';
        $nodeIdWith = '';
        if (!empty($nodeId)) {
            $nodeIdWith = "WITH VALID_NODES AS (
                WITH RECURSIVE nodes AS (
                    SELECT
                        node.id
                    FROM node
                    WHERE " . QueryBuilder::createInClause('id', $nodeId) . "
                    UNION ALL
                    SELECT 
                        node.id
                    FROM node
                    INNER JOIN nodes ON nodes.id = node.parent_node_id
                )
                SELECT id FROM nodes
            ) ";

            $nodeIdWhere = " AND (reacxc.PREVIOUS_CC_ID IN (SELECT * FROM VALID_NODES)
                             OR reacxc.NODE_ID IN (SELECT * FROM VALID_NODES)) ";
        }

        if ($profile->PERFORMED_OR_BOOKING_DATE == 0) {
            $dateWhere = "WHERE date_trunc('day', rebooking_date) BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')";
        } else {
            $dateWhere = "WHERE booking_date BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')";
        }

        $sql = $nodeIdWith . "
            SELECT
                reac.id,
                to_char(reac.rebooking_date, 'DD.MM.YYYY') AS rebooking_date,
                to_char(reacxc.booking_date, 'DD.MM.YYYY') AS booking_date,
                requestor.shortcut AS requestor,
                performer.shortcut AS performer,
                reac.reason,
                coalesce(reacxc.hoursal, 0) + coalesce(reacxc.hours, 0) AS hours,
                previous_cc_nv.name AS cc_old,
                cc_nv_new.name AS cc_new,
                newbt.name AS new_booking_type,
                oldbt.name AS old_booking_type,
                reacxc.note,
                reacxc.noteal,
                reacxc.old_note,
                employee.shortcut AS booking_employee,
                reac.sum_of_modified_totals
            FROM rebooking_action reac
            LEFT JOIN rebooking_action_x_booking reacxc ON reac.id = reacxc.rebooking_action_id
            INNER JOIN employee requestor ON reac.requestor = requestor.id
            LEFT JOIN employee ON reacxc.employee_id = EMPLOYEE.ID
            INNER JOIN employee performer ON reac.performer = performer.ID
            INNER JOIN node previous_cc ON reacxc.previous_cc_id = previous_cc.id
            INNER JOIN node_version previous_cc_nv
                ON previous_cc_nv.node_id = previous_cc.id
                AND previous_cc_nv.version_state_id = 1
            LEFT JOIN node cc_new ON reacxc.node_id = cc_new.id
            LEFT JOIN node_version cc_nv_new
                ON cc_nv_new.node_id = cc_new.id
                AND cc_nv_new.version_state_id = 1
            LEFT JOIN booking_type newBT ON reacxc.bt_id = newbt.id
            LEFT JOIN booking_type oldBT ON reacxc.previous_bt_id = oldbt.id
            " . $dateWhere . "
            " . $nodeIdWhere . "
            ORDER BY reac.rebooking_date, requestor
        ";

        $statement = $db->prepare($sql);
        $statement->bindValue(':PERIOD_FROM', $periodFrom->get(Iso_Date::ORACLE_DATE));
        $statement->bindValue(':PERIOD_UNTIL', $periodUntil->get(Iso_Date::ORACLE_DATE));

        if ($statement->execute()) {
            $rows = $statement->fetchAll();

            foreach ($rows as $key => $item) {
                $rebookingDateElements = explode('.', $item['REBOOKING_DATE']);
                //ID for the correct grouping and sorting in extjs
                $rows[$key]['ID'] = $rebookingDateElements[2] . $rebookingDateElements[1]
                    . $rebookingDateElements[0] . $item['REQUESTOR'] . $item['ID'];

                $rows[$key]['NOTE'] = TPWWorkingHours::buildCompleteWorkingsHoursNote([$item['NOTE'], $item['NOTEAL']]);
            }

            // Fetch the selected nodes from DB (if there are any)
            $nodes = ChartProfil::getNodesOfProfile($profile->ID, $db);

            return [
                'DATA'  => ['result' => true, 'list' => $rows],
                'FROM'  => $periodFrom->get(Zend_Date::TIMESTAMP),
                'UNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP),
                'NODES' => $nodes
            ];
        } else {
            return [
                'DATA'    => ['result' => false, 'list' => ''],
                'FROM'    => $periodFrom->get(Zend_Date::TIMESTAMP),
                'UNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP),
                'NODES' => $nodes
            ];
        }
    }

    /**
     * Gets the data for the 'BT-Order' report.
     *
     * @author Steffen Glatzel <gla@isogmbh.de>
     */
    public static function loadBtOrder($profile, $timestamp, $db, $session, $translate)
    {
        $nodeId = Chartprofil::getProfileNodes($profile->ID, $db, false);

        // Fetch the selected nodes from DB (if there are any)
        $nodes = ChartProfil::getNodesOfProfile($profile->ID, $db);

        $lockedWhere = '';

        if ($profile->WITH_LOCKED_NODES == 1) {
            $lockedWhere = ' WHERE nodes.bookable = 1 ';
        }

        $sqlCount = /** @lang SQL */ "
            WITH RECURSIVE nodes AS (
                SELECT 
                    node.id,
                    node.bookable
                FROM node
                WHERE " . QueryBuilder::createInClause('node.id', $nodeId) . "
                UNION ALL
                SELECT
                    node.id,
                    node.bookable
                FROM node
                INNER JOIN nodes ON nodes.id = node.parent_node_id
            )
            SELECT count(*) AS Count 
            FROM nodes
            " . $lockedWhere . " 
		";

        $statementCount = $db->prepare($sqlCount);
        $statementCount->execute();

        $count = $statementCount->fetch();

        $sql = /** @lang SQL */"
            WITH nodes AS (
                WITH RECURSIVE nodes AS (
                    SELECT 
                        node.id,
                        node.bookable,
                        node_version.id AS node_version_id,
                        ARRAY[lower(node_version.name)] AS sort_order,
                        1 AS level
                    FROM node
                    INNER JOIN node_version ON node_version.node_id = node.id
                        AND node_version.version_state_id = 1
                    WHERE " . QueryBuilder::createInClause('node.id', $nodeId) . "
                    UNION ALL
                    SELECT
                        node.id,
                        node.bookable,
                        node_version.id AS node_version_id,
                        nodes.sort_order || lower(node_version.name) AS sort_order,
                        nodes.level + 1 AS level
                    FROM node
                    INNER JOIN nodes ON nodes.id = node.parent_node_id
                    INNER JOIN node_version ON node_version.node_id = node.id
                        AND node_version.version_state_id = 1
                )
                SELECT 
                    node.id,
                    node.is_leaf,
                    node.node_type_id,
                    node.parent_node_id,
                    nodes.node_version_id,
                    nodes.sort_order,
                    nodes.bookable,
                    level
                FROM nodes
                INNER JOIN node ON nodes.id = node.id
                " . $lockedWhere . "
            )
            SELECT
                node_version.name,
                node_type." . $translate->translate('btOrderLanguageName') . " AS node_type,
                structure_unit_type." . $translate->translate('btOrderLanguageName') . " AS structure_type,
                CASE WHEN employee.id IS NOT NULL 
                    THEN employee.first_name || ' ' || employee.last_name || ' (' || employee.shortcut || ')'
                    ELSE null 
                END AS provider,
                node.bookable,
                to_char(node_version.bookable_from, 'DD.MM.YYYY') AS bookable_from,
                to_char(node_version.lock_date, 'DD.MM.YYYY') AS locked_from,
                CASE node.node_type_id
                    WHEN " . NodeType::ORDER . " THEN node_version.order_number
                    WHEN " . NodeType::UNIT_OF_ORDER . " THEN node_version.order_unit_number
                    ELSE node_version.debtor_number
                END as debtor_number,
                node_version.customer_name,
                node_version.long_name,
                booking_type.name AS booking_type,
                node_version.target_effort,
                note_obligation_type.name AS note_obligation_type,
                node_version.sap_key_cost_center,
                node_version.sap_key_cost_object,
                node_version.commentary AS note,
                node.id,
                coalesce(node.parent_node_id, 0) AS parent_node_id,
                node.is_leaf,
                level - 1 AS indentation
            FROM nodes AS node
            INNER JOIN node_version
                ON node_version.id = node.node_version_id
            LEFT JOIN structure_unit_type ON  node_version.structure_unit_type_id = structure_unit_type.id
            INNER JOIN node_type ON  node.node_type_id = node_type.id
            LEFT JOIN note_obligation_type ON node_version.note_obligation_type_id = note_obligation_type.id
            LEFT JOIN booking_type ON node_version.booking_type_id = booking_type.id
            LEFT JOIN employee ON employee.id = node_version.remaining_effort_provider
            ORDER by node.sort_order
        ";

        $statement = $db->prepare($sql);

        if ($statement->execute()) {
            $rows = $statement->fetchAll();

            $session->btOrder[$timestamp] = [
                'REPORT_INFO' => [
                    'NODES' => $nodes
                ],
                'DATA' => $rows,
                'COUNT' => $count['COUNT']
            ];
        } else {
            $session->btOrder[$timestamp] = [
                'REPORT_INFO' => [
                    'NODES' => $nodes
                ],
                'DATA' => [],
                'COUNT' => 0
            ];
        }

        return ['timestamp' => $timestamp];
    }

    /**
     * Gets the data for the 'Permission' report.
     *
     * @author Steffen Glatzel <gla@isogmbh.de>
     */
    public static function loadPermission($profile, $timestamp, $db, $cache, $translate)
    {
        $permissionValues = [];
        $listEmployeesWithNoBCPermission = [];
        $employeesWithNoBCPermission = [];
        $selectedGroupsAndEmployeesID = ChartProfil::getUserAndGroupIdsOfProfile($profile->ID, $db);

        $list = ChartProfil::getEmployeesWithPermissions($selectedGroupsAndEmployeesID, $db, $profile);
        $list = $list['listEmployeesWithBCPermission'];

        if ($profile->EMP_WITHOUT_BC_PERMISSION) {
            //Employee information with no BC permission
            $listEmployeesWithNoBCPermission = ChartProfil::getEmployeesWithPermissions($selectedGroupsAndEmployeesID, $db, $profile);
            $listEmployeesWithNoBCPermission = $listEmployeesWithNoBCPermission['listEmployeesWithNoBCPermission'];
        }

        $ids = [];
        $employeesWithNothing = [];

        if ($profile->EMP_WITHOUT_BC_PERMISSION) {
            foreach ($listEmployeesWithNoBCPermission as $item) {
                //Array with correct presentation
                $employeesWithNoBCPermission[] = [
                    'EMPLOYEENAME' => strtoupper($item['SHORTCUT']) . ' (' . $item['FIRST_NAME'] . ' ' . $item['LAST_NAME'] . ')',
                    'PERMISSIONKIND' => $translate->translate('noBCPermissionEntry')
                ];
            }
        }

        // Fetch the user shortcuts from DB (if there are any)
        $booked = ChartProfil::getUserAndGroupNamesOfProfile($profile->ID, $db);
        if (empty($booked['employees']) && empty($booked['groups'])) {
            $booked['groups'][] = [
                'name'        => 'ALLE'
            ];
        }

        if ($list != null || $listEmployeesWithNoBCPermission != null) {
            if ($list != null) {
                foreach ($list as $item) {
                    $ids[] = $item['EMP_ID'];

                    if ($item['BKZCOCKPIT_RIGHT'] > 2) {
                        $permissionName = '';

                        for ($privilegeBit = 1 << 2; $privilegeBit < 1 << (PHP_INT_SIZE*8-2); $privilegeBit <<= 1) {
                            if (($item['BKZCOCKPIT_RIGHT'] & $privilegeBit) === 0) {
                                continue;
                            }

                            // @codingStandardsIgnoreStart
                            switch ($privilegeBit) {
                                case Iso_User::RIGHT_TO_SEE_COST_AND_REVENUE:
                                    $permissionName = $translate->translate('rtCostAndRevenue');
                                    break;
                                case Iso_User::RIGHT_TO_MONTH_END:
                                    $permissionName = $translate->translate('rtMonthEnd');
                                    break;
                                case Iso_User::RIGHT_TO_ACCOUNTING:
                                    $permissionName = $translate->translate('rtAccounting');
                                    break;
                                case Iso_User::RIGHT_TO_REBOOK:
                                    $permissionName = $translate->translate('btRebookOnlyLastQuarter');
                                    break;
                                case Iso_User::RIGHT_TO_SEE_PERMISSION:
                                    $permissionName = $translate->translate('rtPermission');
                                    break;
                                case Iso_User::RIGHT_TO_SEE_SAP_KEY_HOURS:
                                    $permissionName = $translate->translate('rtSAPKeyHours');
                                    break;
                                case Iso_User::RIGHT_TO_SEE_REBOOK_LOG:
                                    $permissionName = $translate->translate('rebookLog');
                                    break;
                                case Iso_User::RIGHT_TO_STANDBY:
                                    $permissionName = $translate->translate('standby');
                                    break;
                                case Iso_User::RIGHT_TO_ABSENCE_CALENDAR:
                                    $permissionName = $translate->translate('absenceCalendar');
                                    break;
                                case Iso_User::RIGHT_TO_MIS_DIVISION_OVERALL:
                                    $permissionName = $translate->translate('misDivisionOverall');
                                    break;
                                case Iso_User::RIGHT_TO_PROJECT_STATUS:
                                    $permissionName = $translate->translate('projectStatus');
                                    break;
                                case Iso_User::RIGHT_TO_ACCOUNTABLE_HOURS:
                                    $permissionName = $translate->translate('accountableHours');
                                    break;
                                case Iso_User::RIGHT_TO_ORDER_OVERVIEW:
                                    $permissionName = $translate->translate('orderOverview');
                                    break;
                                case Iso_User::RIGHT_TO_REBOOK_ALL_BOOKINGS:
                                    $permissionName = $translate->translate('btRebookAllBooking');
                                    break;
                                case Iso_User::RIGHT_TO_CONTINGENT_RANGE:
                                    $permissionName = $translate->translate('contingentRange');
                                    break;
                                case Iso_User::RIGHT_TO_CONTINGENT_DATA:
                                    $permissionName = $translate->translate('contingentData');
                                    break;
                                case Iso_User::RIGHT_TO_CONTINGENT_SAP_INVOICE_CMP:
                                    $permissionName = $translate->translate('contingentSapInvoiceComparison');
                                    break;
                                case Iso_User::RIGHT_TO_PRISMA_VALUES:
                                    $permissionName = $translate->translate('prismaValues');
                                    break;
                            }
                            // @codingStandardsIgnoreEnd

                            $permissionValues[] = [
                                'EMPLOYEENAME' => strtoupper($item['SHORTCUT']) . ' (' . $item['FIRST_NAME'] . ' ' . $item['LAST_NAME'] . ')',
                                'PERMISSIONKIND' => $translate->translate('reportTypeSep'), 'VALUE' => $permissionName
                            ];
                        }
                    } else {
                        // employees without permisisons for report types
                        array_push($employeesWithNothing, $item['EMP_ID']);
                    }
                }

                //Collect all parts of report at once for all employees
                $employeeNodes = ChartProfil::getAssignedNodes($ids, $db, $translate);

                $employeeSets = ChartProfil::getAssignedSets($ids, $db, $translate);

                $employeeEmployees = ChartProfil::getAssignedEmployees($ids, $db, $translate);

                $employeeGroups = ChartProfil::getAssignedEmployeeGroups($ids, $db, $translate);

                $employeeNothing = [];
                if ($profile->EMP_WITHOUT_BC_PERMISSION) {
                    if (!empty($employeesWithNothing)) {
                        // get employees without permisisons for report types or nodes/sets/employees/groups
                        $employeeNothing = ChartProfil::getEmployeesWithNothing($employeesWithNothing, $db, $translate);
                    }
                }

                $reportdata = array_merge($permissionValues, $employeeNodes, $employeeSets, $employeeEmployees, $employeeGroups, $employeeNothing, $employeesWithNoBCPermission);
            } else {
                $reportdata = $employeesWithNoBCPermission;
            }

            // save report data in cache
            $cacheId = $profile->ID . '_' . $timestamp;
            $cache->save([
                'REPORT_INFO' => [
                    'TYPE' => $profile->ANZEIGE,
                    'EMPLOYEES' => $booked
                ],
                'DATA' => $reportdata
            ], $cacheId);

            return [
                'DATA' => ['result' => true, 'list' => $reportdata],
                'EMPLOYEEINFO' => ['booked' => $booked]
            ];
        } else {
            return [
                'DATA' => ['result' => false, 'list' => ''],
                'EMPLOYEEINFO' => ['booked' => $booked]
            ];
        }
    }

    /**
     * Gets the data for the 'Standby' report.
     *
     * @author Roebrt Lesniak <les@isogmbh.de>
     */
    public static function loadStandby($profile, $timestamp, Zend_Db_Adapter_Abstract $db, $cache)
    {
        // Determine period of this profile
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);

        $profileNodes = Chartprofil::getProfileNodes($profile->ID, $db, false);

        //Determine leafes of profile (section beams and sets) as in statement
        $leafNodes = Chartprofil::getLeafesOfProfile($profile->ID, $db, true);

        // Determine profile companies
        $companies = ChartProfil::getProfileCompanies($profile->ID);
        // Determine profile divisions
        $divisions = ChartProfil::getProfileDivisions($profile->ID);
        // Determine the selected users
        $employees = ChartProfil::getProfileEmployees($db, $profile->ID);

        $userAndGroupIds = self::getUserAndGroupIdsOfProfile($profile->ID, $db);

        // if resolved employee selection results in no employees at all (e.g. due to empty groups), then report has no data
        if (!empty($userAndGroupIds) && self::noEmployeeSelected($profile->ID, [
                "companies" => $companies,
                "divisions" => $divisions,
                "employees" => $employees['id']
            ])
        ) {
            $employees['id'] = [Employee::DUMMY];
        }

        $companiesWhere = '';
        $divisionsWhere = '';
        $employeesWhere = '';
        $employeesFromBookings = [];

        if (!empty($companies)) {
            $companiesWhere = "employment_period.company_id IN (" . implode(',', $companies) . ")";
        }

        if (!empty($divisions)) {
            $divisionsWhere = "employee_function_period.division_id IN (" . implode(',', $divisions) . ")";
        }

        if (!empty($employees['id'])) {
            $employeesWhere = QueryBuilder::createInClause('st.employee_id', $employees['id']);
        }

        $sql = "
            SELECT
                CASE
                    WHEN c_st.id is not null THEN c_st.shortcut
                    ELSE null
                END AS company,
                CASE
                    WHEN d_st.id is not null THEN d_st.name || ' - ' || d_st.long_name
                    ELSE null
                END AS division,
                employee.shortcut AS emp_shortcut,
                employee.first_name || ' ' || employee.last_name AS emp_name,
                CASE
                  WHEN tpwpublicholidays.cdate is not null OR EXTRACT(ISODOW FROM st.datetime)::INTEGER = 7 THEN 3
                  WHEN EXTRACT(ISODOW FROM st.datetime)::INTEGER = 6 THEN 2
                  ELSE 1
                END AS day_type,
                to_char(st.datetime, 'dd.mm.yyyy') AS start_date,
                to_char(st.datetime, 'hh24:mi') AS start_time,
                --st.hours_period,
                st.hours_actual,
                node_version.name AS cc,
                st.note
            FROM tpw_standby_time st
            INNER JOIN employee ON st.employee_id = employee.id
            INNER JOIN node ON st.node_id = node.id
            INNER JOIN node_version
                ON node_version.node_id = node.id
                AND node_version.version_state_id = 1
            LEFT JOIN employment_period
                ON employment_period.employee_id = employee.id
                AND date_trunc('day', st.datetime) BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
            LEFT JOIN employee_function_period
                ON employee_function_period.employee_id = employee.id
                AND date_trunc('day', st.datetime) BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
            LEFT JOIN company c_st ON employment_period.company_id = c_st.id
            LEFT JOIN division d_st ON employee_function_period.division_id = d_st.id
            LEFT JOIN tpwpublicholidays
                ON tpwpublicholidays.clocation_id = employment_period.location_id
                AND tpwpublicholidays.cdate = date_trunc('day', st.datetime)
            LEFT JOIN cc_with_sap_key ON cc_with_sap_key.node_id = node.id
            WHERE date_trunc('day', st.datetime) >= to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND date_trunc('day', st.datetime) <= to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
            " . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
            " . (empty($leafNodes) ? " " : " AND " . QueryBuilder::createInClause('node.id', $leafNodes)) . "
            ORDER BY company, emp_shortcut, st.datetime
        ";

        $statement = $db->prepare($sql);
        $statement->bindValue(':PERIOD_FROM', $periodFrom->get(Iso_Date::ORACLE_DATE));
        $statement->bindValue(':PERIOD_UNTIL', $periodUntil->get(Iso_Date::ORACLE_DATE));

        if ($statement->execute()) {
            $rows = $statement->fetchAll();

            $employeesFromBookings = [];
            foreach ($rows as $key => $item) {
                $employeesFromBookings[] = strtolower($item['EMP_SHORTCUT']);
            }

            // Fetch the user shortcuts from DB (if there are any)
            $booked = ChartProfil::getUserAndGroupNamesOfProfile($profile->ID, $db);
            if (empty($booked['employees']) && empty($booked['groups']) && !empty($employeesFromBookings)) {
                $booked['employees'] = array_unique($employeesFromBookings);
                sort($booked['employees']);
            }

            // Fetch the selected nodes from DB (if there are any)
            $nodes = ChartProfil::getNodesOfProfile($profile->ID, $db);

            // save report data in cache
            $cacheId = $profile->ID . '_' . $timestamp;
            $cache->save(
                [
                    'REPORT_INFO' => [
                        'TYPE' => $profile->ANZEIGE,
                        'EMPLOYEES' => isset($booked) ? $booked : '',
                        'NODES' => $nodes,
                        'PERIODFROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
                        'PERIODUNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP)
                    ],
                    'DATA' => $rows
                ],
                $cacheId
            );

            return [
                'DATA' => [
                    'result' => true,
                    'data' => $rows
                ],
                'EMPLOYEEINFO' => ['booked' => isset($booked) ? $booked : ''],
                'FROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
                'UNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP)
            ];
        } else {
            return [
                'DATA' => ['result' => false, 'liste' => ''],
                'EMPLOYEEINFO' => '',
                'FROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
                'UNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP)
            ];
        }
    }

    /**
     * Gets the data for the 'Absence Calendar' report.
     *
     * @author Steffen Glatzel <gla@isogmbh.de>
     */
    public static function loadAbsenceCalendar($profile, $timestamp, $db, $cache, $translate)
    {
        // Config options of the report
        $employeesAsGroups = $profile->EMPLOYEES_AS_GROUPS == 1;
        $employeesWithAbsences = $profile->EMPLOYEES_WITH_ABSENCES == 1;

        // Determine the selected groups
        $groups = ChartProfil::getGroupIdsOfProfile($profile->ID);
        // Determine the selected users
        $selectedEmployees = ChartProfil::getEmployeeIdsOfProfile($profile->ID);
        $selectedGroupsAndEmployeesID = ChartProfil::getUserAndGroupIdsOfProfile($profile->ID, $db);
        // Check if group all is selected
        $groupAllSelected = ChartProfil::isSpecialGroupSelected($selectedGroupsAndEmployeesID, $db);
        // Determine period of this profile
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);
        // Determine profile companies
        $companies = ChartProfil::getProfileCompanies($profile->ID, false);
        // Determine profile divisions
        $divisions = ChartProfil::getProfileDivisions($profile->ID, false);

        // get absence categories
        $sqlCategories = "
			SELECT
			  -1 AS id,
			  '" . $translate->translate('holidayApplication') . "' AS name,
			  (SELECT color FROM absence_category WHERE name_de = 'Urlaub') AS color,
			  1 AS striped,
			  (SELECT priority FROM absence_category WHERE name_de = 'Urlaub') AS priority
			UNION
			SELECT
			  id AS id,
			  name_" . $translate->getLocale() . " AS name,
			  color AS color,
			  0 AS striped,
			  priority
			FROM absence_category
			ORDER BY name
		";
        $absenceCategories = $db->fetchAll($sqlCategories);

        // build absence SQL
        $employeesWhere = '';
        $companiesWhere = '';
        $divisionsWhere = '';
        $groupsWhere = '';

        if (!empty($groups)) {
            $groupsWhere = QueryBuilder::createInClause('employee_x_employee_group.employee_group_id ', $groups);
        }
        if (!empty($companies)) {
            $companiesWhere = "employment_period.company_id IN (" . implode(',', $companies) . ")";
        }
        if (!empty($divisions)) {
            $divisionsWhere = "employee_function_period.division_id IN (" . implode(',', $divisions) . ")";
        }
        if (!empty($selectedEmployees)) {
            $employeesWhere = QueryBuilder::createInClause('EMPLOYEE.ID', $selectedEmployees);
            if (true === $employeesAsGroups && !empty($groupsWhere)) {
                $employeesWhere = '(' . $employeesWhere .  ')';
            }
        }
        $where = QueryBuilder::conditionOr("", [$groupsWhere, $employeesWhere, $companiesWhere, $divisionsWhere]);

        $withClauseAbsences = "WITH merged_absence AS
				  (SELECT absence.employee_id,
					absence.id,
					absence.start_date,
					absence.end_date,
					absence.start_time,
					absence.end_time,
					absence.description,
					absence.category_id,
					absence_category.priority,
					absence_category.time_relevant
				  FROM absence
				  LEFT JOIN absence_category
				  ON absence_category.id = absence.category_id
                  INNER JOIN employment_period
					ON absence.employee_id = employment_period.employee_id
					AND (
						(employment_period.entry_date BETWEEN absence.start_date AND absence.end_date)
						OR (coalesce(employment_period.leaving_date, max_date()) BETWEEN absence.start_date AND absence.end_date)
						OR (employment_period.entry_date < absence.start_date and coalesce(employment_period.leaving_date, max_date()) > absence.end_date)
					)
			
					LEFT JOIN employee_function_period ON
						employee_function_period.employee_id = employment_period.employee_id
						AND (
							absence.start_date BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
							OR absence.end_date BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
						)
					LEFT JOIN division ON employee_function_period.division_id = division.id
					LEFT JOIN company ON employment_period.company_id = company.id
				
					LEFT JOIN employee_x_employee_group
						ON employee_x_employee_group.employee_id = employment_period.employee_id
					LEFT JOIN employee_group
						ON employee_x_employee_group.employee_group_id = employee_group.id
					LEFT JOIN employee ON employee.id = absence.employee_id
				 WHERE $where 
				  AND ( (absence.start_date BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD'))
				  OR (absence.end_date BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD'))
				  OR (absence.start_date < to_date(:PERIOD_FROM, 'YYYY-MM-DD')
				  AND absence.end_date   > to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')) )
				  UNION
				  SELECT holiday_application.employee_id,
					-holiday_application.id       AS id,
					holiday_application.date_from AS start_date,
					holiday_application.date_till AS end_date,
					NULL::date                    AS start_time,
					NULL::date                    AS end_time,
					''                            AS description, --holiday application comment should be empty,
					-1                            AS category_id,
					(SELECT priority FROM absence_category WHERE name_de = 'Urlaub'
					) AS priority,
					0                             AS time_relevant					
				  FROM holiday_application
                  INNER JOIN employment_period
					ON holiday_application.employee_id = employment_period.employee_id
					AND (
						(employment_period.entry_date BETWEEN holiday_application.date_from AND holiday_application.date_till)
						OR (coalesce(employment_period.leaving_date, max_date()) BETWEEN holiday_application.date_from AND holiday_application.date_till)
						OR (employment_period.entry_date < holiday_application.date_from and coalesce(employment_period.leaving_date, max_date()) > holiday_application.date_till)
					)
			
					LEFT JOIN employee_function_period ON
						employee_function_period.employee_id = employment_period.employee_id
						AND (
							holiday_application.date_from BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
							OR holiday_application.date_till BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
						)
					LEFT JOIN division ON employee_function_period.division_id = division.id
					LEFT JOIN company ON employment_period.company_id = company.id
				
					LEFT JOIN employee_x_employee_group
						ON employee_x_employee_group.employee_id = employment_period.employee_id
					LEFT JOIN employee_group
						ON employee_x_employee_group.employee_group_id = employee_group.id
					LEFT JOIN employee ON employee.id = holiday_application.employee_id
				 WHERE $where
				  AND holiday_application.state = 0
				  AND ( (holiday_application.date_from BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD'))
				  OR (holiday_application.date_till BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD'))
				  OR (holiday_application.date_from < to_date(:PERIOD_FROM, 'YYYY-MM-DD')
				  AND holiday_application.date_till > to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')) )
				  )";

        if (true === $employeesAsGroups) {
            $sqlUnion = $sql = "
				$withClauseAbsences
				SELECT
					employee.id AS employee_id,
					employee.shortcut AS shortcut,
					employee.first_name,
					employee.last_name,
					absence.id AS absence_id,
					to_char(absence.start_date, 'YYYY-MM-DD') as start_date,
                    to_char(absence.end_date, 'YYYY-MM-DD') as end_date,
                    to_char(start_time, 'HH24:MI') AS start_time,
                    to_char(end_time, 'HH24:MI') AS end_time,
					absence.description,
					absence.category_id AS absence_category_id,
					absence.priority,
					absence.time_relevant
			";

            if (!empty($companiesWhere) || !empty($divisionsWhere)) {
                $sql .= "
					, division.long_name AS division,
					division.id AS division_id,
					company.shortcut AS company,
					company.id AS company_id
				";
                $sqlUnion .= "
					, null AS division,
					null AS division_id,
					null AS company,
					null AS company_id
				";
            }

            if (!empty($groupsWhere)) {
                $sql .= " , CASE WHEN " . $groupsWhere . " THEN employee_group.name ELSE null END AS group_name ";
            } else {
                $sql .= ", null AS group_name";
            }
            $sqlUnion .= ", '" . $translate->translate('allUC') . "' AS group_name";

            $sqlFrom = "
				FROM employee
				" . ($employeesWithAbsences == 1 ? ' INNER ' : 'LEFT ') . " JOIN merged_absence ABSENCE
					ON absence.employee_id = employee.id
				INNER JOIN employment_period
					ON employee.id = employment_period.employee_id
					AND (
						(employment_period.entry_date BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') and  to_date(:PERIOD_UNTIL, 'YYYY-MM-DD'))
						OR (coalesce(employment_period.leaving_date, max_date()) BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD'))
						OR (employment_period.entry_date < to_date(:PERIOD_FROM, 'YYYY-MM-DD') and coalesce(employment_period.leaving_date, max_date()) > to_date(:PERIOD_UNTIL, 'YYYY-MM-DD'))
					)
			";

            $sql .= $sqlFrom;
            $sqlUnion .= $sqlFrom;

            //For special groups(companies and divisions)
            if (!empty($companiesWhere) || !empty($divisionsWhere)) {
                $sql .= "
					LEFT JOIN employee_function_period ON
						employee_function_period.employee_id = employee.id
						AND (
							to_date(:PERIOD_FROM, 'YYYY-MM-DD') BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
							OR to_date(:PERIOD_UNTIL, 'YYYY-MM-DD') BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
						)
					LEFT JOIN division ON employee_function_period.division_id = division.id
					LEFT JOIN company ON employment_period.company_id = company.id
				";
            }

            if (!empty($groupsWhere)) {
                $sql .= "
					LEFT JOIN employee_x_employee_group
						ON employee_x_employee_group.employee_id = employee.id
					LEFT JOIN employee_group
						ON employee_x_employee_group.employee_group_id = employee_group.id
				";
            }

            $sql .= " WHERE " . $where;
            if ($groupAllSelected) {
                // group "all" was selected along with other groups/employees
                if (!empty($where)) {
                    $sql .= " UNION ALL " . $sqlUnion;
                } else {
                    // group "all" only was selected
                    $sql = $sqlUnion;
                }
            }
            $sql .= " ORDER BY " . ($groupsWhere ? "GROUP_NAME, " : ' ') . "  SHORTCUT, START_DATE";
        } else {
            $sql = "
				$withClauseAbsences
				SELECT DISTINCT
					employee.id AS employee_id,
					employee.shortcut AS shortcut,
					employee.first_name,
					employee.last_name,
					absence.id AS absence_id,
					to_char(absence.start_date, 'YYYY-MM-DD') as start_date,
                    to_char(absence.end_date, 'YYYY-MM-DD') as end_date,
                    to_char(start_time, 'HH24:MI') AS start_time,
                    to_char(end_time, 'HH24:MI') AS end_time,
					absence.description,
					absence.category_id AS absence_category_id,
					absence.priority,
				    absence.time_relevant
				FROM employee
				" . ($employeesWithAbsences == 1 ? ' INNER ' : 'LEFT ') . " JOIN merged_absence absence
				ON absence.employee_id = employee.id
				INNER JOIN employment_period
					ON employee.ID = employment_period.employee_id
					AND (
						(employment_period.entry_date BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD'))
						OR (coalesce(employment_period.leaving_date, max_date()) BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD'))
						OR (employment_period.entry_date < to_date(:PERIOD_FROM, 'YYYY-MM-DD') and coalesce(employment_period.leaving_date, max_date()) > to_date(:PERIOD_UNTIL, 'YYYY-MM-DD'))
					)
			";

            //For special groups(companies and divisions)
            if (!empty($companiesWhere) || !empty($divisionsWhere)) {
                $sql .= "
					LEFT JOIN employee_function_period ON
						employee_function_period.employee_id = employee.id
						AND (
							to_date(:PERIOD_FROM, 'YYYY-MM-DD') BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
							OR to_date(:PERIOD_UNTIL, 'YYYY-MM-DD') BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
						)
					LEFT JOIN division ON employee_function_period.division_id = division.id
					LEFT JOIN company ON employment_period.company_id = company.id
				";
            }

            if (!empty($groupsWhere)) {
                $sql .= "
					LEFT JOIN employee_x_employee_group
						ON employee_x_employee_group.employee_id = employee.id
					LEFT JOIN employee_group
						ON employee_x_employee_group.employee_group_id = employee_group.id
				";
            }

            if (!$groupAllSelected) {
                $sql .= " WHERE " . $where;
            }

            $sql .= " ORDER BY SHORTCUT, start_date";
        }

        $statement = $db->prepare($sql);
        $statement->bindValue(':PERIOD_FROM', $periodFrom->get(Iso_Date::ORACLE_DATE));
        $statement->bindValue(':PERIOD_UNTIL', $periodUntil->get(Iso_Date::ORACLE_DATE));

        if ($statement->execute()) {
            $absences = [];

            // use a counter for each employee added to ensure unique ids
            $idCounter = [];

            while (false !== ($row = $statement->fetch())) {
                $employeeId = $row['EMPLOYEE_ID'];

                $indexes = [];
                if (true === $employeesAsGroups) {
                    if (!empty($row['GROUP_NAME'])) {
                        $indexes[] = $row['GROUP_NAME'] . '_' . $row['SHORTCUT'];
                    }
                    if (!empty($selectedEmployees) && in_array($employeeId, $selectedEmployees)) {
                        $indexes[] = '{SINGLE}' . '_' . $row['SHORTCUT'];
                    }
                    if (!empty($divisions) && in_array($row['DIVISION_ID'], $divisions)) {
                        $indexes[] = $row['DIVISION'] . '_' . $row['SHORTCUT'];
                    }
                    if (!empty($companies) && in_array($row['COMPANY_ID'], $companies)) {
                        $indexes[] = $row['COMPANY'] . '_' . $row['SHORTCUT'];
                    }
                } else {
                    $indexes[] = $row['SHORTCUT'];
                }

                $absencesAdded = []; // helper array

                $dateComparator = new DateComparator();

                foreach ($indexes as $idx) {
                    $idxCounter = self::getOverlappingIndex($dateComparator, $absences, $row, $idx, 0);
                    if ($idxCounter > 0) {
                        self::createAbsence($idx.'$'.$idxCounter, $employeeId, $row, $employeesAsGroups, $absences, $idCounter, $absencesAdded, true); //create absence with key-suffix
                    } else {
                        self::createAbsence($idx, $employeeId, $row, $employeesAsGroups, $absences, $idCounter, $absencesAdded, false); //create absence with plain key
                    }
                }
            }

            ksort($absences);
            $absences = array_values($absences);

            $publicHolidays = [];
            if (!empty($idCounter)) {
                // get public holidays per employee
                $publicHolidaySql = "
					SELECT
						employee.id,
						employment_period.location_id,
						to_char(tpwpublicholidays.cdate, 'yyyy-mm-dd') AS cdate,
						tpwpublicholidays.ctitle
					FROM employee
					INNER JOIN employment_period ON employment_period.employee_id = employee.id
					INNER JOIN tpwpublicholidays
						ON tpwpublicholidays.clocation_id = employment_period.location_id
						AND tpwpublicholidays.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
					WHERE employee_id IN (" . implode(',', array_keys($idCounter)) . ")
					AND tpwpublicholidays.cdate BETWEEN to_date(:period_from, 'YYYY-MM-DD') AND to_date(:period_until, 'YYYY-MM-DD')
				";

                $statementPublicHoliday = $db->prepare($publicHolidaySql);
                $statementPublicHoliday->bindValue(':period_from', $periodFrom->get(Iso_Date::ORACLE_DATE));
                $statementPublicHoliday->bindValue(':period_until', $periodUntil->get(Iso_Date::ORACLE_DATE));
                $statementPublicHoliday->execute();

                while (false !== ($row = $statementPublicHoliday->fetch())) {
                    $publicHolidays[$row['ID']][$row['CDATE']] = $row['CTITLE'];
                }
            }

            // Fetch the user shortcuts from DB (if there are any)
            $booked = ChartProfil::getUserAndGroupNamesOfProfile($profile->ID, $db);
            if (empty($booked['employees']) && empty($booked['groups']) && !empty($employeesFromBookings)) {
                $booked['employees'] = array_unique($employeesFromBookings);
                sort($booked['employees']);
            }

            $cacheId = $profile->ID . '_' . $timestamp;
            $cache->save([
                'REPORT_INFO' => [
                    'TYPE' => $profile->ANZEIGE,
                    'EMPLOYEES' => isset($booked) ? $booked : '',
                    'PERIODFROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
                    'PERIODUNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP),
                    'EMPLOYEES_AS_GROUPS'    => $employeesAsGroups,
                    'EMPLOYEES_WITH_ABSENCES' => $employeesWithAbsences
                ],
                'DATA' => [
                    'publicHolidays' => $publicHolidays,
                    'absenceCategories' => $absenceCategories,
                    'employeeAbsences' => $absences
                ]
            ], $cacheId);

            return [
                'DATA' => [
                    'success' => true,
                    'employeesAsGroups' => $employeesAsGroups,
                    'publicHolidays' => $publicHolidays,
                    'absenceCategories' => $absenceCategories,
                    'employeeAbsences' => $absences
                ],
                'EMPLOYEEINFO' => ['booked' => isset($booked) ? $booked : ''],
                'FROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
                'UNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP)
            ];
        } else {
            return [
                'DATA' => ['success' => false],
                'EMPLOYEEINFO' => '',
                'FROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
                'UNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP)
            ];
        }
    }

    private static function getOverlappingIndex($dateComparator, $absences, $row, $idx, $idxCounter, $overlappedInPreviousRow = false)
    {
        if ($idxCounter == 0) {
            $checkIdx = $idx; //use plain index
        } else {
            $checkIdx = $idx . '$' . $idxCounter; //use index with $0, $1, ... etc. suffix
        }
        if (isset($absences [$checkIdx])) { //if there is absences in this row
            foreach ($absences [$checkIdx] ['absences'] as $absence) { //check all absences from this row if are overlapping
                if ($row ['ABSENCE_ID'] == $absence ['id']) { //for option 'employeesAsGroups' it can be that there are many entries with same id for diffrent groups, companies etc.
                    return $idxCounter; // in this case return same suffix
                }
                if ($dateComparator->isOverlapping($absence ['start_date'], $absence ['end_date'], $row ['START_DATE'], $row ['END_DATE'])) {
                    return self::getOverlappingIndex($dateComparator, $absences, $row, $idx, $idxCounter + 1, true); //if absence is overlapping, then check next row if it is overlapping,
                }                                                                                                    //and mark that it was overlapping in this row
            }
            if ($overlappedInPreviousRow) { //if in this row absence was not overlapping, but overlapped in previous row, the return index of this row
                return $idxCounter;
            } else { //if there was no absence overlapping in this row then check next row
                return self::getOverlappingIndex($dateComparator, $absences, $row, $idx, $idxCounter + 1);
            }
        } elseif ($overlappedInPreviousRow) { //if there are no overlappings in this row, but row was overlapped in previous row
            return $idxCounter; //then return index of this row
        } else {
            return 0; //in other add absence to first row
        }
    }

    private static function createAbsence($idx, $employeeId, $row, $employeesAsGroups, &$absences, &$idCounter, &$absencesAdded, $removeLabel = false)
    {
        if (!isset($absences [$idx])) {
            if (!isset($idCounter [$employeeId])) {
                $idCounter [$employeeId] = 1;
            }
            $absences [$idx] = [
                'id'=>$employeeId . '#' . $idCounter [$employeeId] ++,
                'employee_id'=>$employeeId,
                'shortcut'=> !$removeLabel? $row ['SHORTCUT']:'',
                'first_name'=>!$removeLabel?$row ['FIRST_NAME']:'',
                'last_name'=>!$removeLabel?$row ['LAST_NAME']:'',
                'absences'=>[]
            ];
            $absencesAdded [$idx] = [];
        }

        // do not add null values (occurs if employees w/o absences are shown)
        // do not add absences twice (to a group)
        $absId = $row ['ABSENCE_ID'];
        if (!is_null($absId) && !isset($absencesAdded [$idx] [$absId])) {
            // iso-a6975-i for some categories description should be empty
            $description = in_array($row ['ABSENCE_CATEGORY_ID'], [
                1, // Ilness
                2, // Holiday
                4 // Maternity protection
            ]) ? "" : $row ['DESCRIPTION'];

            $absences [$idx] ['absences'] [] = [
                'id'=>$row ['ABSENCE_ID'],
                'category_id'=>$row ['ABSENCE_CATEGORY_ID'],
                'start_date'=>$row ['START_DATE'],
                'end_date'=>$row ['END_DATE'],
                'start_time' => $row['TIME_RELEVANT'] === '1' ? $row['START_TIME'] : null,
                'end_time' => $row['TIME_RELEVANT'] === '1' ? $row['END_TIME'] : null,
                'priority'=>$row ['PRIORITY'],
                'description'=>$description
            ];
            // remember absences which were already added in helper array
            $absencesAdded [$idx] [$absId] = true;
        }

        // add group name
        if (true === $employeesAsGroups) {
            $names = explode('_', $idx);
            $absences [$idx] ['group'] = $names [0];
        }
    }

    /**
     * Gets the data for the 'MIS Division' report.
     * @param $profile
     * @param $timestamp
     * @param Zend_Db_Adapter_Abstract $db
     * @param $session
     * @param $translate
     * @return array
     */
    public static function loadMisDivision($profile, $timestamp, $db, $session, $translate)
    {
        // Determine period of this profile
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);

        $debtorNodes = self::getProfileNodesPerType($profile->ID, $db, NodeType::DEBTOR);
        $singleNodes = self::getSingleNodesOfProfile($profile->ID, $db);

        $singleNodesWhere = "";
        if (!empty($singleNodes['all'])) {
            $singleNodesWhere = " OR " . QueryBuilder::createInClause('node.id', $singleNodes['all']);
        }

        // get division data
        $sql = "
            SELECT
                name,
                name || ' -- ' || long_name AS formatted_name
            FROM division
            WHERE id = :division_id
        ";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':division_id', (int)$profile->DIVISION_ID);
        $stmt->execute();
        $division = $stmt->fetch(Zend_Db::FETCH_ASSOC);

        $sql = "
            WITH
                node_hours AS (
                    SELECT
                        leaf_nodes.root_node_id AS node_id,
                        sum(CASE WHEN employment_period.status_id <> 5 THEN coalesce(tpwworkinghours.chours, 0) + coalesce(tpwworkinghours.choursal, 0) ELSE 0 END) AS sum_hours,
                        sum(CASE WHEN employment_period.status_id = 7 THEN coalesce(tpwworkinghours.chours, 0) + coalesce(tpwworkinghours.choursal, 0) ELSE 0 END) AS sum_trainee_hours,
                        sum(CASE WHEN employment_period.status_id = 2 THEN coalesce(tpwworkinghours.chours, 0) + coalesce(tpwworkinghours.choursal, 0) ELSE 0 END) AS sum_student_hours,
                        sum(CASE WHEN employment_period.status_id = 1 THEN coalesce(tpwworkinghours.chours, 0) + coalesce(tpwworkinghours.choursal, 0) ELSE 0 END) AS sum_intern_hours
                    FROM tpwworkinghours
                    RIGHT JOIN (
                        WITH RECURSIVE nodes AS (
                            SELECT
                                node.id AS root_node_id,
                                node.id,
                                node.is_leaf
                            FROM node
                            WHERE (
                                node.node_type_id = " . NodeType::DEBTOR . "
                                $singleNodesWhere
                            )
                            UNION ALL
                            SELECT
                                nodes.root_node_id,
                                node.id,
                                node.is_leaf
                            FROM node
                            INNER JOIN nodes ON nodes.id = node.parent_node_id
                        )
                        SELECT
                            root_node_id,
                            nodes.id
                        FROM nodes
                        INNER JOIN node_version
                            ON node_version.node_id = nodes.id
                                AND node_version.version_state_id = 1
                        WHERE nodes.is_leaf = 1
                    ) leaf_nodes
                        ON tpwworkinghours.cnode_id = leaf_nodes.id
                    LEFT JOIN employment_period
                        ON employment_period.employee_id = tpwworkinghours.cemployee_id
                        AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                    WHERE tpwworkinghours.cid IS NOT null
                    AND tpwworkinghours.cdate BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
                    GROUP BY leaf_nodes.root_node_id
                ),
                node_cost_and_revenue AS (
                    SELECT
                        node_id,
                        -sum(coalesce(revenue,0)) AS sum_revenue,
                        sum(coalesce(cost,0)) AS sum_cost
                    FROM (
                        WITH RECURSIVE nodes AS (
                            SELECT
                                node.id AS node_id,
                                node.id
                            FROM node
                            WHERE (
                                node.node_type_id = " . NodeType::DEBTOR . "
                                $singleNodesWhere
                            )
                            UNION ALL
                            SELECT
                                nodes.node_id,
                                node.id
                            FROM node
                            INNER JOIN nodes ON nodes.id = node.parent_node_id
                        )
                        SELECT
                            nodes.node_id,
                            coalesce(node_version.sap_key_cost_object, node_version.sap_key_cost_center) AS sap_key
                        FROM nodes
                        INNER JOIN node_version
                            ON node_version.node_id = nodes.id
                            AND node_version.version_state_id = 1
                        WHERE coalesce(node_version.sap_key_cost_object, node_version.sap_key_cost_center) IS NOT null
                    ) node
                    INNER JOIN cost_and_revenue ON cost_and_revenue.sap_key = node.sap_key
                    WHERE cost_and_revenue.month BETWEEN to_date(:PERIOD_FROM, 'YYYY-MM-DD') AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
                    GROUP BY node.node_id
                )
        ";

        $sqlSelects = [];
        if (!empty($debtorNodes)) {
            $sql .= "
                ,
                accrual_begin AS (
                    SELECT
                        node_id,
                        accrual_value AS accrual_value,
                        accrual_comment
                    FROM node_mis_info
                    WHERE month_end = to_date(:PERIOD_FROM, 'YYYY-MM-DD') -1
                    AND accrual_value IS NOT null
                ),
                accrual_end AS (
                    SELECT
                        node_id,
                        accrual_value,
                        accrual_comment
                    FROM node_mis_info
                    WHERE month_end = to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
                    AND accrual_value IS NOT null
                ),
                correction_end AS (
                    SELECT
                        node_id,
                        transaction_correction,
                        cost_correction AS cost_correction_end,
                        comment_correction AS comment_correction_end
                    FROM node_mis_info
                    WHERE month_end = to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
                    AND (transaction_correction IS NOT null	OR cost_correction IS NOT null)
                ),
                correction_start AS (
                    SELECT
                        node_id,
                        transaction_correction AS cost_correction_start,
                        comment_correction AS comment_correction_start
                    FROM node_mis_info
                    WHERE month_end = to_date(:PERIOD_FROM, 'YYYY-MM-DD') -1
                    AND (transaction_correction IS NOT null	OR cost_correction IS NOT null)
                )
            ";

            $sqlSelects[] = "
                SELECT
                    0 AS section,
                    node.id,
                    CASE WHEN node_version.customer_name IS NOT null THEN node_version.name || ' [' || node_version.customer_name || ']' ELSE node_version.name END AS name,
                    node_version.debtor_number AS debtor_number,
                    coalesce(node_hours.sum_hours, 0) AS act_hours,
                    coalesce(node_hours.sum_trainee_hours, 0) AS act_hours_trainee,
                    coalesce(node_hours.sum_student_hours, 0) AS act_hours_student,
                    coalesce(node_hours.sum_intern_hours, 0) AS act_hours_intern,
                    coalesce(node_cost_and_revenue.sum_revenue, 0) AS net_sales,
                    coalesce(node_cost_and_revenue.sum_cost, 0) AS cost,
                    accrual_begin.accrual_value AS accrual_begin_value,
                    accrual_begin.accrual_comment AS accrual_begin_comment,
                    accrual_end.accrual_value AS accrual_end_value,
                    accrual_end.accrual_comment AS accrual_end_comment,
                    correction_end.transaction_correction,
                    correction_end.cost_correction_end,
                    correction_end.comment_correction_end,
                    correction_start.cost_correction_start,
                    correction_start.comment_correction_start
                FROM node
                INNER JOIN node_version
                    ON node_version.node_id = node.id
                    AND node_version.version_state_id = 1
                LEFT JOIN node_hours ON node_hours.node_id = node.id
                LEFT JOIN node_cost_and_revenue ON node_cost_and_revenue.node_id = node.id
                LEFT JOIN accrual_begin ON accrual_begin.node_id = node.id
                LEFT JOIN accrual_end ON accrual_end.node_id = node.id
                LEFT JOIN correction_end ON correction_end.node_id = node.id
                LEFT JOIN correction_start ON correction_start.node_id = node.id
                WHERE (
                    coalesce(node_hours.sum_hours, 0) > 0
                    OR coalesce(node_cost_and_revenue.sum_cost, 0) <> 0
                    OR coalesce(node_cost_and_revenue.sum_revenue, 0) <> 0
                    OR coalesce(accrual_begin.accrual_value, 0) <> 0
                    OR coalesce(accrual_end.accrual_value, 0) <> 0
                    OR (
                        to_date(:PERIOD_FROM, 'YYYY-MM-DD') = date_trunc('year', to_date(:PERIOD_FROM, 'YYYY-MM-DD'))
                        AND to_date(:PERIOD_UNTIL, 'YYYY-MM-DD') = (date_trunc('quarter', to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')) + INTERVAL '3' MONTH) - INTERVAL '1' DAY
                        AND date_trunc('year', to_date(:PERIOD_FROM, 'YYYY-MM-DD')) = date_trunc('year', to_date(:PERIOD_UNTIL, 'YYYY-MM-DD'))
                        AND (
                            coalesce(correction_end.transaction_correction, 0) <> 0
                            OR coalesce(correction_end.cost_correction_end, 0) <> 0
                            OR coalesce(correction_start.cost_correction_start, 0) <> 0
                        )
                    )
                )
                AND " . QueryBuilder::createInClause('node.id', $debtorNodes) . "
            ";
        }
        if (!empty($singleNodes['MIS_INTERNAL'])) {
            $sqlSelects[] = "
                SELECT
                    " . SingleNodeType::MIS_INTERNAL . " AS section,
                    node.id,
                    node_version.name,
                    null AS debtor_number,
                    coalesce(node_hours.sum_hours, 0) AS act_hours,
                    coalesce(node_hours.sum_trainee_hours, 0) AS act_hours_trainee,
                    coalesce(node_hours.sum_student_hours, 0) AS act_hours_student,
                    coalesce(node_hours.sum_intern_hours, 0) AS act_hours_intern,
                    coalesce(node_cost_and_revenue.sum_revenue, 0) AS net_sales,
                    coalesce(node_cost_and_revenue.sum_cost, 0) AS cost,
                    null AS accrual_begin_value,
                    null AS accrual_begin_comment,
                    null AS accrual_end_value,
                    null AS accrual_end_comment,
                    null AS transaction_correction,
                    null AS cost_correction_start,
                    null AS cost_correction_end,
                    null AS comment_correction_end,
                    Null AS comment_correction_start
                FROM node
                INNER JOIN node_version
                    ON node_version.node_id = node.id
                    AND node_version.version_state_id = 1
                LEFT JOIN node_hours ON node_hours.node_id = node.id
                LEFT JOIN node_cost_and_revenue ON node_cost_and_revenue.node_id = node.id
                WHERE (
                    coalesce(node_hours.sum_hours, 0) > 0
                    OR coalesce(node_cost_and_revenue.sum_revenue, 0) > 0
                    OR coalesce(node_cost_and_revenue.sum_cost, 0) > 0
                )                
                AND " . QueryBuilder::createInClause('node.id', $singleNodes['MIS_INTERNAL']) . "
            ";
        }
        if (!empty($singleNodes['MIS_SALES'])) {
            $sqlSelects[] = "
                SELECT
                    " . SingleNodeType::MIS_SALES . " AS section,
                    node.id,
                    node_version.name,
                    null AS debtor_number,
                    coalesce(node_hours.sum_hours, 0) AS act_hours,
                    coalesce(node_hours.sum_trainee_hours, 0) AS act_hours_trainee,
                    coalesce(node_hours.sum_student_hours, 0) AS act_hours_student,
                    coalesce(node_hours.sum_intern_hours, 0) AS act_hours_intern,
                    coalesce(node_cost_and_revenue.sum_revenue, 0) AS net_sales,
                    coalesce(node_cost_and_revenue.sum_cost, 0) AS cost,
                    null AS accrual_begin_value,
                    null AS accrual_begin_comment,
                    null AS accrual_end_value,
                    null AS accrual_end_comment,
                    null AS transaction_correction,
                    null AS cost_correction_start,
                    null AS cost_correction_end,
                    null AS comment_correction_end,
                    null AS comment_correction_start
                FROM node
                INNER JOIN node_version
                    ON node_version.NODE_ID = node.id
                    AND node_version.version_state_id = 1
                LEFT JOIN node_hours ON node_hours.node_id = node.id
                LEFT JOIN node_cost_and_revenue ON node_cost_and_revenue.node_id = node.id
                WHERE (
                    coalesce(node_hours.sum_hours, 0) > 0
                    OR coalesce(node_cost_and_revenue.sum_revenue, 0) > 0
                    OR coalesce(node_cost_and_revenue.sum_cost, 0) > 0
                )                                 
                AND " . QueryBuilder::createInClause('node.id', $singleNodes['MIS_SALES']) . "
            ";
        }

        $data = [
            0 => [],
            SingleNodeType::MIS_INTERNAL => [],
            SingleNodeType::MIS_SALES => []
        ];
        $count = 0;

        // if all select parts are empty, we do not need to execute the query
        if (!empty($sqlSelects)) {
            $sql .= implode(' UNION ALL ', $sqlSelects) . " ORDER BY section, name";

            $stmt = $db->prepare($sql);
            $stmt->bindValue(':PERIOD_FROM', $periodFrom->get(Iso_Date::ORACLE_DATE));
            $stmt->bindValue(':PERIOD_UNTIL', $periodUntil->get(Iso_Date::ORACLE_DATE));

            if ($stmt->execute()) {
                while (false !== ($row = $stmt->fetch())) {
                    $data[$row['SECTION']][] = $row;
                    ++$count;
                }
            }
        }

        $session->misDivision[$timestamp] = [
            'REPORT_INFO' => [
                'TYPE' => $profile->ANZEIGE,
                'PERIODFROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
                'PERIODUNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP),
                'NODES' => self::getNodesOfProfile($profile->ID, $db),
                'DIVISION' => $division['FORMATTED_NAME'],
                'TARGET_HOURLY_RATE' => $profile->TARGET_HOURLY_RATE
            ],
            'DATA' => $data,
            'COUNT' => $count
        ];

        return ['timestamp' => $timestamp];
    }

    /**
     * Gets the data for the 'MIS Overall' report.
     */
    public static function loadMisOverall($profile, $timestamp, $db, $session, $translate)
    {
        // Determine period of this profile
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);

        // get all of the user's profiles of type MIS division with the same period configuration
        $sql = "
            WITH chpshraedemp AS (" . self::getChartProfileSharedEmployeesSql() . ")

			SELECT
				id,
				bezeichnung AS name
			FROM bc_chart_profil
            LEFT JOIN chpshraedemp on bc_chart_profil.id = chpshraedemp.bc_chart_profil_id
			WHERE anzeige = 'I'
			AND (
			    mitarbeiter_id = :EMPLOYEE_ID
                OR bc_chart_profil.id IN (
                    SELECT bc_chart_profil_id 
                    FROM chpshraedemp 
                    WHERE bc_chartp_mitarbeiter_id = :EMPLOYEE_ID
                )
                OR bc_chart_profil.id IN (
                    SELECT bc_chartp_x_addemp.bc_chart_profil_id
                    FROM bc_chartp_x_addemp
                    WHERE bc_chartp_x_addemp.mitarbeiter_id = :EMPLOYEE_ID
                )
            )
			AND PACK_BKZCOCKPIT.getPeriodFrom(id) = to_date(:PERIOD_FROM, 'YYYY-MM-DD')
			AND PACK_BKZCOCKPIT.getPeriodUntil(id) = to_date(:PERIOD_UNTIL, 'YYYY-MM-DD')
			AND bezeichnung IS NOT null
			GROUP BY id, bezeichnung
			ORDER BY bezeichnung COLLATE \"POSIX\"
		";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':EMPLOYEE_ID', $profile->MITARBEITER_ID);
        $stmt->bindValue(':PERIOD_FROM', $periodFrom->get(Iso_Date::ORACLE_DATE));
        $stmt->bindValue(':PERIOD_UNTIL', $periodUntil->get(Iso_Date::ORACLE_DATE));

        if ($stmt->execute()) {
            // instantiate classes
            $chartprofil = new ChartProfil();
            $validator = new ReportValidator($translate);

            $profiles = [];
            $count = 0;
            while (false !== ($row = $stmt->fetch())) {
                // get profile
                $misProfile = $chartprofil->find($row['ID'])->current();
                if (!$misProfile) {
                    continue;
                }

                // check if profile is valid
                $validationResult = $validator->validate($misProfile);
                if (isset($validationResult['error']) && true === $validationResult['error']) {
                    continue;
                }

                // load profile data
                $result = self::loadMisDivision($misProfile, $timestamp . ++$count, $db, $session, $translate);
                $profiles[] = array_merge($row, $result);
            }

            $session->misOverall[$timestamp] = [
                'REPORT_INFO' => [
                    'TYPE' => $profile->ANZEIGE,
                    'PERIODFROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
                    'PERIODUNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP)
                ],
                'DATA' => $profiles,
                'COUNT' => sizeof($profiles)
            ];
        } else {
            $session->misOverall[$timestamp] = [
                'REPORT_INFO' => [
                    'TYPE' => $profile->ANZEIGE,
                ],
                'DATA' => [],
                'COUNT' => 0
            ];
        }

        return ['timestamp' => $timestamp];
    }

    /**
     * @param $profile
     * @param $timestamp
     * @param Zend_Db_Adapter_Abstract $db
     * @param $session
     * @param $translate
     * @param $cache
     * @return array
     */
    public static function loadStatus($profile, $timestamp, $db, $session, $translate, $cache)
    {
        $chpbt = new ChartProfileBookingType();
        $information = $chpbt->getBookingTypes($profile['ID']);

        // determine period of this profile
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);

        $btObj = new BookingType();
        $bookingTypes = [];
        foreach ($btObj->getBookingTypes($translate->getLocale()) as $bt) {
            $bookingTypes[$bt['ID']] = $bt['NAME'];
        }

        // determine sections to be considered and add mapping to corresponding booking type
        $btSections = [];
        $infNames = [];
        $usedInfNames = [];

        foreach ($btObj->getBookingTypesAsFullArray($translate->getLocale()) as $key => $val) {
            if (in_array($key, $information)) {
                $btSections[$key] = ['id' => $key, 'name' => $val['name']];
                $usedInfNames[$val['name']] = trim($val['information_name']);
            }
        }

        foreach ($btObj->getBookingTypesAsFullArray($translate->getLocale()) as $key => $val) {
            $infNames[$val['name']] = trim($val['information_name']);
        }

        $lockedWhere = "";
        if ($profile->WITH_LOCKED_NODES == 0) {
            $lockedWhere = "WHERE node_version.lock_date IS null OR node_version.lock_date > current_date";
        }

        $sql = /** @lang SQL */ "
            WITH
                /* get profile nodes */
                profile_node AS (
                    SELECT
                        node_id
                    FROM bc_chart_profil_x_node
                    WHERE bc_chart_profil_id = :PROFILE_ID
                    AND node_id IS NOT null
                ),
                /* get profile single nodes */
                profile_single_node AS (
                    SELECT
                        single_node_id AS node_id,
                        single_node_types
                    FROM bc_chart_profil_x_node
                    WHERE bc_chart_profil_id = :PROFILE_ID
                    AND single_node_id IS NOT null
                    AND single_node_types IS NOT null
                ),
                /* get all months of profile period */
                period_month AS (
                    WITH RECURSIVE months AS (
                        SELECT
                            to_date(:DATE_FROM, 'YYYY-MM-DD') AS month
                        UNION ALL
                        SELECT
                            (months.month + INTERVAL '1' MONTH)::date
                        FROM months
                        WHERE months.month < date_trunc('month', to_date(:DATE_UNTIL, 'YYYY-MM-DD'))
                    )
                    SELECT month from months
                ),
                /* get all months from 01.01.2012 (begin of valid hour data) till end of period */
                hour_period_month AS (
                    WITH RECURSIVE months AS (
                        SELECT
                            date '2012-01-01' AS month
                        UNION ALL
                        SELECT
                            (months.month + INTERVAL '1' MONTH)::date
                        FROM months
                        WHERE months.month < date_trunc('month', to_date(:DATE_UNTIL, 'YYYY-MM-DD'))
                    )
                    SELECT month from months
                ),
                /* get all months from 01.01.2013 (begin of valid revenue data) till end of period */
                revenue_period_month AS (
                    WITH RECURSIVE months AS (
                        SELECT
                            date '2013-01-01' AS month
                        UNION ALL
                        SELECT
                            (months.month + INTERVAL '1' MONTH)::date
                        FROM months
                        WHERE months.month < date_trunc('month', to_date(:DATE_UNTIL, 'YYYY-MM-DD'))
                    )
                    SELECT month from months
                ),
                /* get all nodes beloging to a company */
                company_node AS (
                    WITH RECURSIVE nodes AS (
                        SELECT
                            node.id AS child_node_id,
                            nv.name,
                            nv.name AS root_node_name,
                            1 AS lvl
                        FROM node
                        INNER JOIN node_version nv on node.id = nv.node_id
                            AND nv.version_state_id = 1
                        WHERE node.node_type_id = 1
                            AND nv.structure_unit_type_id = 1
                        UNION ALL
                        SELECT
                            node.id AS child_node_id,
                            nv.name,
                            root_node_name,
                            nodes.lvl + 1 AS lvl
                        FROM node
                        INNER JOIN nodes ON nodes.child_node_id = node.parent_node_id
                        INNER JOIN node_version nv on node.id = nv.node_id
                            AND nv.version_state_id = 1
                    )
                    SELECT
                        child_node_id,
                        min(company_node_name) AS company_node_name,
                        max(lvl) AS lvl
                    FROM (
                        SELECT
                            nodes.*,
                            first_value(root_node_name) OVER (PARTITION BY child_node_id ORDER BY lvl) AS company_node_name
                        FROM nodes
                    ) t
                    GROUP  by child_node_id
                ),
                /* get context nodes of profile_nodes */
                context_node AS (
                    SELECT
                        node.id,
                        (CASE
                            WHEN node_version.long_name IS NOT null THEN node_version.name || ' [' || node_version.long_name || ']'
                            WHEN node_version.customer_name IS NOT null THEN node_version.name || ' [' || node_version.customer_name || ']'
                            ELSE node_version.name
                        END) AS name,
                        node_type.id AS node_type_id,
                        node_type.shortcut AS node_type_name,
                        node_version.sap_key_cost_object AS sap_key,
                        node_version.target_effort,
                        node_version.maintenance_part,
                        company_node.company_node_name
                    FROM node
                    INNER JOIN node_version
                        ON node_version.node_id = node.id
                        AND node_version.version_state_id = 1
                    INNER JOIN node_type ON node_type.id = node.node_type_id
                    INNER JOIN company_node ON company_node.child_node_id = node.id
                    INNER JOIN (
                        SELECT
                            DISTINCT
                            coalesce(child_node.id, parent_node.id, profile_node.node_id) AS id
                        FROM profile_node
                        LEFT JOIN (
                            WITH RECURSIVE nodes AS (
                                SELECT
                                    node.id AS root_node_id,
                                    node.id,
                                    nv.sap_key_cost_object
                            FROM node
                            INNER JOIN node_version nv on node.id = nv.node_id
                                AND nv.version_state_id = 1
                            WHERE node.id IN (SELECT node_id FROM profile_node)
                            UNION ALL
                            SELECT
                                nodes.root_node_id,
                                node.id,
                                nv.sap_key_cost_object
                            FROM node
                            INNER JOIN node_version nv ON nv.node_id = node.id
                                AND nv.version_state_id = 1
                            INNER JOIN nodes ON node.parent_node_id = nodes.id
                                AND nodes.sap_key_cost_object IS NULL
                            )
                            SELECT
                                root_node_id,
                                id 
                            FROM nodes
                            WHERE nodes.sap_key_cost_object IS NOT NULL
                        ) child_node
                            ON child_node.root_node_id = profile_node.node_id
                        LEFT JOIN (
                            WITH RECURSIVE nodes AS (
                                SELECT
                                    node.id AS root_node_id,
                                    node.id,
                                    node.parent_node_id,
                                    nv.sap_key_cost_object
                            FROM node
                            INNER JOIN node_version nv on node.id = nv.node_id
                                AND nv.version_state_id = 1
                            WHERE node.id IN (SELECT node_id FROM profile_node)
                            UNION ALL
                            SELECT
                                nodes.root_node_id,
                                node.id,
                                node.parent_node_id,
                                nv.sap_key_cost_object
                            FROM node
                            INNER JOIN node_version nv ON nv.node_id = node.id
                                AND nv.version_state_id = 1
                            INNER JOIN nodes ON node.id = nodes.parent_node_id
                                AND nodes.sap_key_cost_object IS NULL
                            )
                            SELECT
                                root_node_id,
                                id
                            FROM nodes
                            WHERE nodes.sap_key_cost_object IS NOT NULL
                        ) parent_node
                            ON child_node.id IS null
                            AND parent_node.root_node_id = profile_node.node_id
                    ) n
                        ON n.id = node.id
                    $lockedWhere
                ),
                /* nodes at selection 'Distinct row' */
                distinct_node AS (
                    SELECT
                        profile_single_node.node_id AS root_node_id,
                        coalesce(parent_node.id, profile_single_node.node_id) AS sap_node_id,
                        node.id,
                        (CASE
                            WHEN node_version.long_name IS NOT null THEN node_version.name || ' [' || node_version.long_name || ']'
                            WHEN node_version.customer_name IS NOT null THEN node_version.name || ' [' || node_version.customer_name || ']'
                            ELSE node_version.name
                        END) AS name,
                        node_type.id AS node_type_id,
                        node_type.shortcut AS node_type_name,
                        node_version.sap_key_cost_object AS sap_key,
                        node_version.target_effort,
                        node_version.maintenance_part,
                        company_node.company_node_name,
                        CASE
                            WHEN node.id = profile_single_node.node_id THEN 1
                            ELSE 2
                        END AS lvl
                    FROM profile_single_node
                    INNER JOIN node
                        ON node.id = profile_single_node.node_id
                        OR (
                            (single_node_types & " . SingleNodeType::STATUS_WITHNEXTLEVEL . ")::INTEGER > 0
                            AND node.parent_node_id = profile_single_node.node_id
                        )
                    INNER JOIN node_version
                        ON node_version.node_id = node.id
                        AND node_version.version_state_id = 1
                    INNER JOIN node_type ON node_type.id = node.node_type_id
                    INNER JOIN company_node ON company_node.child_node_id = node.id
                    LEFT JOIN (
                        WITH RECURSIVE nodes AS (
                            SELECT
                                node.id AS root_node_id,
                                node.id,
                                node.parent_node_id,
                                context_node.id AS context_node_id
                        FROM node
                        LEFT JOIN context_node ON context_node.id  = node.id
                        WHERE node.id IN (
                            SELECT node_id
                            FROM profile_single_node
                            WHERE (single_node_types & " . SingleNodeType::STATUS_DISTINCTROW . ")::INTEGER > 0
                        )
                        UNION ALL
                        SELECT
                            nodes.root_node_id,
                            node.id,
                            node.parent_node_id,
                            context_node.id AS context_node_id
                        FROM node
                        LEFT JOIN context_node ON context_node.id  = node.id
                        INNER JOIN nodes ON node.id = nodes.parent_node_id
                            AND nodes.context_node_id IS NULL
                        )
                        SELECT
                            root_node_id,
                            id
                        FROM nodes
                    ) parent_node
                        ON parent_node.root_node_id = profile_single_node.node_id
                ),
                /* get sums of hour bookings per month of all context nodes per booking type */
                node_bt_hours AS (
                    SELECT
                        n.id AS node_id,
                        m.month,
                        booking_type.id AS booking_type_id,
                        coalesce(v.sum_hours, 0) AS sum_hours,
                        coalesce(v.sum_trainee_hours, 0) AS sum_trainee_hours,
                        sum(coalesce(v.sum_hours, 0)) OVER (PARTITION BY n.id, booking_type.id, date_trunc('year', m.month) ORDER BY m.month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS sum_hours_year,
                        sum(coalesce(v.sum_hours, 0)) OVER (PARTITION BY n.id, booking_type.id ORDER BY m.month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS sum_hours_all,
                        sum(coalesce(v.sum_trainee_hours, 0)) OVER (PARTITION BY n.id, booking_type.id, date_trunc('year', m.month) ORDER BY m.month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS sum_trainee_hours_year,
                        sum(coalesce(v.sum_trainee_hours, 0)) OVER (PARTITION BY n.id, booking_type.id ORDER BY m.month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS sum_trainee_hours_all
                    FROM hour_period_month m
                    CROSS JOIN (
                        SELECT id FROM context_node
                        UNION
                        SELECT id FROM distinct_node
                    ) n
                    CROSS JOIN booking_type
                    LEFT JOIN (
                        SELECT
                            context_leaf_node.root_node_id AS node_id,
                            date_trunc('month', tpwworkinghours.cdate) AS month,
                            tpwworkinghours.cbooking_type_id AS booking_type_id,
                            sum(coalesce(tpwworkinghours.chours,0) + coalesce(tpwworkinghours.choursal, 0)) AS sum_hours,
                            sum(CASE WHEN employment_period.status_id = 7 THEN coalesce(tpwworkinghours.chours,0) + coalesce(tpwworkinghours.choursal,0) ELSE 0 END) AS sum_trainee_hours
                        FROM tpwworkinghours
                        INNER JOIN (
                            WITH RECURSIVE nodes AS (
                                SELECT 
                                    node.id AS root_node_id,
                                    node.id,
                                    node.is_leaf
                                FROM node
                                WHERE node.id IN (
                                    SELECT id FROM context_node
                                    UNION
                                    SELECT id FROM distinct_node
                                )
                                UNION ALL
                                SELECT 
                                    nodes.root_node_id,
                                    node.id,
                                    node.is_leaf
                                FROM node 
                                INNER JOIN nodes ON nodes.id = node.parent_node_id
                            )
                            SELECT
                                root_node_id,
                                id AS node_id
                            FROM nodes 
                            WHERE is_leaf = 1
                        ) context_leaf_node
                            ON context_leaf_node.node_id = tpwworkinghours.cnode_id
                        LEFT JOIN employment_period
                            ON employment_period.employee_id = tpwworkinghours.cemployee_id
                            AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                        GROUP BY context_leaf_node.root_node_id, tpwworkinghours.cbooking_type_id, date_trunc('month', tpwworkinghours.cdate)
                    ) v
                        ON v.node_id = n.id
                        AND v.month = m.month
                        AND v.booking_type_id = booking_type.id
                ),
                /* get sums of revenues per month of context single nodes */
                node_revenue AS (
                    SELECT
                        v.*,
                        sum(v.sum_revenue_fp) OVER (PARTITION BY v.node_id ORDER BY v.month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS sum_revenue_fp_all,
                        sum(v.sum_revenue_fp) OVER (PARTITION BY v.node_id, date_trunc('year', v.month) ORDER BY v.month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS sum_revenue_fp_year,
                        sum(v.sum_revenue_fp_lic) OVER (PARTITION BY v.node_id ORDER BY v.month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS sum_revenue_fp_lic_all,
                        sum(v.sum_revenue_fp_lic) OVER (PARTITION BY v.node_id, date_trunc('year', v.month) ORDER BY v.month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS sum_revenue_fp_lic_year,
                        sum(v.sum_revenue_tm) OVER (PARTITION BY v.node_id, date_trunc('year', v.month) ORDER BY v.month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS sum_revenue_tm_year,
                        sum(v.sum_revenue_cr) OVER (PARTITION BY v.node_id, date_trunc('year', v.month) ORDER BY v.month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS sum_revenue_cr_year,
                        sum(v.sum_revenue_mc) OVER (PARTITION BY v.node_id, date_trunc('year', v.month) ORDER BY v.month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS sum_revenue_mc_year,
                        sum(v.sum_revenue_mc_pax) OVER (PARTITION BY v.node_id, date_trunc('year', v.month) ORDER BY v.month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS sum_revenue_mc_pax_year
                    FROM (
                        SELECT
                            context_sap_node.id AS node_id,
                            m.month,
                            /**
                             * Fixed price:
                             * - amounts from revenue_per_material with material like 'DL_LE*'
                             *   => certain debtor ranges with SAP key like '*-001-*' are excluded (see logic for 'time and material' below)
                             * - amounts from revenue_per_material with material like '*_LIZ_*'
                             */
                            sum(
                                CASE
                                    WHEN
                                        revenue.material LIKE 'DL\_LE%'
                                        AND NOT (
                                            revenue.sap_key LIKE '%-001-%'
                                            AND (
                                                (substr(revenue.sap_key, 1, position('-' IN revenue.sap_key)-1))::integer BETWEEN 20000 AND 29999
                                                OR (substr(revenue.sap_key, 1, position('-' IN revenue.sap_key)-1))::integer BETWEEN 55100 AND 59999
                                                OR (substr(revenue.sap_key, 1, position('-' IN revenue.sap_key)-1))::integer BETWEEN 60000 AND 69999
                                                OR (substr(revenue.sap_key, 1, position('-' IN revenue.sap_key)-1))::integer = 15106
                                            )
                                        )
                                    THEN revenue.amount
                                    ELSE 0
                                END
                            ) AS sum_revenue_fp,
                            sum(CASE WHEN revenue.material LIKE 'LIZENZ\_%' OR revenue.material LIKE '%\_LIZ\_%' THEN revenue.amount ELSE 0 END) AS sum_revenue_fp_lic,
                            /**
                             * Time and material:
                             * - amounts from revenue_per_material with material like 'DL_B*'
                             * - amounts from revenue_per_material with material like 'DL_PAU*'
                             * - amounts from revenue_per_material with material like 'MA00000'
                             * - for certain debtor ranges with SAP key like '*-001-*': amounts from revenue_per_material with material like 'DL_LE*'
                             * - amounts from period with contract_type = 'DL'
                             */
                            sum(
                                CASE WHEN revenue.material LIKE 'DL\_B%' THEN revenue.amount ELSE 0 END
                                +
                                CASE WHEN revenue.material LIKE 'DL\_PAU%' THEN revenue.amount ELSE 0 END
                                +
                                CASE WHEN revenue.material ~ 'MA[0-9]+' THEN revenue.amount ELSE 0 END
                                +
                                CASE
                                    WHEN
                                        revenue.material LIKE 'DL\_LE%'
                                        AND (
                                            revenue.sap_key LIKE '%-001-%'
                                            AND (
                                                (substr(revenue.sap_key, 1, position('-' IN revenue.sap_key)-1))::integer BETWEEN 20000 AND 29999
                                                OR (substr(revenue.sap_key, 1, position('-' IN revenue.sap_key)-1))::integer BETWEEN 55100 AND 59999
                                                OR (substr(revenue.sap_key, 1, position('-' IN revenue.sap_key)-1))::integer BETWEEN 60000 AND 69999
                                                OR (substr(revenue.sap_key, 1, position('-' IN revenue.sap_key)-1))::integer = 15106
                                            )
                                        )
                                    THEN revenue.amount
                                    ELSE 0
                                END
                                +
                                CASE
                                    WHEN
                                        revenue.begin IS NOT null
                                        AND revenue.contract_type = 'DL'
                                    THEN
                                        (
                                            least(
                                                (date_trunc('day', m.month) + INTERVAL '1' MONTH - INTERVAL '1' DAY)::date, 
                                                 revenue.end::date
                                            ) 
                                                - 
                                            (greatest(m.month::date, revenue.begin::date) + INTERVAL '1' DAY)::date
                                        )::integer
                                        * revenue.amount 
                                        / (revenue.end::date - (revenue.begin + INTERVAL '1' DAY)::date)::INTEGER
                                    ELSE 0
                                END
                            ) AS sum_revenue_tm,
                            /**
                             * Change request:
                             * - amounts from revenue_per_material with material like 'CR_*'
                             */
                            sum(CASE WHEN revenue.material LIKE 'CR\_%' THEN revenue.amount ELSE 0 END) AS sum_revenue_cr,
                            /**
                             * Maintenance:
                             * - amounts from period with contract_type = 'W'
                             * - amounts from revenue_per_material with material like 'PAX_*'
                             */
                            sum(
                                /* amounts from period contracts with contract type 'W' (maintenance) */
                                CASE
                                    WHEN
                                        revenue.begin IS NOT null
                                        AND revenue.contract_type = 'W'
                                    THEN
                                        (
                                            least(
                                                (date_trunc('day', m.month) + INTERVAL '1' MONTH - INTERVAL '1' DAY)::date, 
                                                 revenue.end::date
                                            ) 
                                                - 
                                            (greatest(m.month::date, revenue.begin::date) - INTERVAL '1' DAY)::date
                                        )::integer
                                        * revenue.amount 
                                        / (revenue.end - revenue.begin + 1)::INTEGER
                                    ELSE 0
                                END
                            ) AS sum_revenue_mc,
                            sum(CASE WHEN revenue.material LIKE 'PAX\_%' THEN revenue.amount ELSE 0 END) AS sum_revenue_mc_pax
                        FROM revenue_period_month m
                        CROSS JOIN (
                            WITH RECURSIVE nodes AS (
                                SELECT
                                    node.id AS root_node_id,
                                    node.id
                                FROM node 
                                WHERE node.id IN (
                                    SELECT id FROM context_node
                                )
                                UNION ALL
                                SELECT
                                    nodes.root_node_id,
                                    node.id
                                FROM node 
                                INNER JOIN nodes ON nodes.id = node.parent_node_id
                            )
                            SELECT
                                nodes.root_node_id AS id,
                                nv.sap_key_cost_object AS sap_key
                            FROM nodes 
                            INNER JOIN node_version nv
                                ON nv.node_id = nodes.id
                                AND nv.version_state_id = 1
                        ) context_sap_node
                        LEFT JOIN (
                            SELECT sap_key, month, material, revenue AS amount, hours, null::date AS begin, null::date AS end, null AS contract_type FROM revenue_per_material WHERE contract_type <> 'W'
                            UNION ALL
                            SELECT sap_key, date_trunc('month', period.begin) AS month, null, amount, null, period.begin::date, period.end::date, contract_type FROM period
                            where fakt_status = 1
                        ) revenue
                            ON revenue.sap_key = context_sap_node.sap_key
                            AND m.month BETWEEN revenue.month AND coalesce(revenue.end, revenue.month)
                        GROUP BY context_sap_node.id, m.month
                    ) v
                ),
                /* get remaining effort per month of context distinct nodes */
                node_mis_data AS (
                    SELECT
                        n.id AS node_id,
                        m.month,
                        CASE
                            WHEN remainingeffort_value IS NOT null THEN remainingeffort_value
                            WHEN last_value(remainingeffort_value) OVER (PARTITION BY n.id ORDER BY m.month NULLS FIRST) = 0 THEN 0
                            ELSE null
                        END AS remainingeffort_value,
                        CASE
                            WHEN remainingeffort_value IS NOT null THEN remainingeffort_comment
                            ELSE null
                        END AS remainingeffort_comment
                    FROM revenue_period_month m
                    CROSS JOIN (
                        SELECT
                            DISTINCT id
                        FROM distinct_node
                    ) n
                    LEFT JOIN node_mis_info
                        ON node_mis_info.node_id = n.id
                        AND date_trunc('month', node_mis_info.month_end) = m.month
                ),
                /* get eab data of context nodes */
                node_eab_data AS (
                    SELECT
                        n.id AS node_id,
                        m.month,
                        last_value(eab.month) OVER (PARTITION BY n.id ORDER BY m.month NULLS FIRST) AS eab_month,
                        last_value(eab.pending_hours) OVER (PARTITION BY n.id ORDER BY m.month NULLS FIRST) AS pending_hours
                    FROM revenue_period_month m
                    CROSS JOIN context_node n
                    LEFT JOIN (
                        SELECT
                            latest_eab.node_id,
                            latest_eab.month,
                            sum(coalesce(eab_hour_group.hours, 0)) AS pending_hours
                        FROM (
                            SELECT
                                max(eab.id) AS id,
                                eab.node_id,
                                eab.eab_month AS month
                            FROM eab
                            WHERE eab.eab_status_id = " . EabStatus::READY_FOR_CUSTOMER . "
                            GROUP BY eab.node_id, eab.eab_month
                        ) latest_eab
                        LEFT JOIN eab_hour_group
                            ON eab_hour_group.eab_id = latest_eab.id
                            AND eab_hour_group.eab_hour_group_status_id = " . EabHourGroupStatus::PENDING . "
                        GROUP BY latest_eab.node_id, latest_eab.month
                    ) eab
                        ON eab.node_id = n.id
                        AND eab.month = m.month
                ),
                node_hourly_rate AS (
                    SELECT
                        node_id,
                        max(hourly_rate) AS hourly_rate
                        FROM (
                            SELECT 
                                node_id,
                                first_value(hour_rate) OVER (partition by node_id order by hour_rate_date DESC, hour_rate DESC) AS hourly_rate
                            FROM context_node n
                            INNER JOIN debtor_hour_rate 
                                ON debtor_hour_rate.node_id = n.id
                                    AND debtor_hour_rate.hour_rate_date <= date_trunc(
                                        'year', 
                                        to_date(:DATE_FROM, 'YYYY-MM-DD')
                                    )::timestamp
                        ) t
                    GROUP BY node_id
                )

            SELECT
                1                                       AS section,
                null                                    AS sap_node_id,
                null                                    AS root_node_id,
                null                                    AS lvl,
                context_node.id                         AS node_id,
                context_node.name                       AS node_name,
                context_node.node_type_id               AS node_type_id,
                context_node.node_type_name             AS node_type_name,
                context_node.sap_key                    AS sap_key,
                context_node.target_effort              AS target_effort,
                context_node.maintenance_part           AS maintenance_part,
                context_node.company_node_name          AS company_node_name,
                period_month.month                      AS month,
                (
                    to_char(period_month.month, 'DD.MM.YYYY')
                    || ' - ' ||
                    to_char((date_trunc('month', period_month.month) + INTERVAL '1' MONTH - INTERVAL '1' DAY), 'DD.MM.YYYY')
                )                                        AS display_month,
        ";
        foreach ($bookingTypes as $bt) {
            $sql .= "
                node_${bt}_hours.sum_hours              AS sum_${bt}_hours,
                node_${bt}_hours.sum_hours_year         AS sum_${bt}_hours_year,
                node_${bt}_hours.sum_hours_all          AS sum_${bt}_hours_all,
                null                                    AS sum_${bt}_trainee_hours,
                null                                    AS sum_${bt}_trainee_hours_year,
                null                                    AS sum_${bt}_trainee_hours_all,
            ";
        }
        $sql .= "
                null                                    AS remainingeffort_value,
                null                                    AS remainingeffort_comment,
                to_char(node_eab_data.eab_month, 'YYYY-MM') AS eab_month,
                node_eab_data.pending_hours             AS eab_pending_hours,
                node_hourly_rate.hourly_rate            AS hourly_rate,
                node_revenue.sum_revenue_fp             AS sum_fp_revenue,
                node_revenue.sum_revenue_fp_all         AS sum_fp_revenue_all,
                node_revenue.sum_revenue_fp_lic         AS sum_fp_license_revenue,
                node_revenue.sum_revenue_fp_lic_all     AS sum_fp_license_revenue_all,
                node_revenue.sum_revenue_tm             AS sum_tm_revenue,
                node_revenue.sum_revenue_tm_year        AS sum_tm_revenue_year,
                node_revenue.sum_revenue_cr             AS sum_cr_revenue,
                node_revenue.sum_revenue_cr_year        AS sum_cr_revenue_year,
                node_revenue.sum_revenue_mc             AS sum_mc_revenue,
                node_revenue.sum_revenue_mc_year        AS sum_mc_revenue_year,
                node_revenue.sum_revenue_mc_pax         AS sum_mc_pax_revenue,
                node_revenue.sum_revenue_mc_pax_year    AS sum_mc_pax_revenue_year
            FROM context_node
            CROSS JOIN period_month
        ";
        foreach ($bookingTypes as $id => $bt) {
            $sql .= "
            LEFT JOIN node_bt_hours node_${bt}_hours
                ON node_${bt}_hours.node_id = context_node.id
                AND node_${bt}_hours.month = period_month.month
                AND node_${bt}_hours.booking_type_id = $id
            ";
        }
        $sql .= "
            LEFT JOIN node_revenue
                ON node_revenue.node_id = context_node.id
                AND node_revenue.month = period_month.month
            LEFT JOIN node_eab_data
                ON node_eab_data.node_id = context_node.id
                AND node_eab_data.month = period_month.month
            LEFT JOIN node_hourly_rate
                ON node_hourly_rate.node_id = context_node.id

            UNION ALL

            SELECT
                2                                       AS section,
                distinct_node.sap_node_id               AS sap_node_id,
                distinct_node.root_node_id              AS root_node_id,
                distinct_node.lvl                       AS lvl,
                distinct_node.id                        AS node_id,
                distinct_node.name                      AS node_name,
                distinct_node.node_type_id              AS node_type_id,
                distinct_node.node_type_name            AS node_type_name,
                distinct_node.sap_key                   AS sap_key,
                distinct_node.target_effort             AS target_effort,
                distinct_node.maintenance_part          AS maintenance_part,
                distinct_node.company_node_name         AS company_node_name,
                period_month.month                      AS month,
                (
                    to_char(period_month.month, 'DD.MM.YYYY')
                    || ' - ' ||
                    to_char((date_trunc('month', period_month.month) + INTERVAL '1' MONTH - INTERVAL '1' DAY), 'DD.MM.YYYY')
                )                                       AS display_month,
        ";
        foreach ($bookingTypes as $bt) {
            $sql .= "
                node_${bt}_hours.sum_hours				AS sum_${bt}_hours,
                node_${bt}_hours.sum_hours_year			AS sum_${bt}_hours_year,
                node_${bt}_hours.sum_hours_all			AS sum_${bt}_hours_all,
                node_${bt}_hours.sum_trainee_hours		AS sum_${bt}_trainee_hours,
                node_${bt}_hours.sum_trainee_hours_year	AS sum_${bt}_trainee_hours_year,
                node_${bt}_hours.sum_trainee_hours_all	AS sum_${bt}_trainee_hours_all,
            ";
        }
        $sql .= "
                node_mis_data.remainingeffort_value		AS remainingeffort_value,
                node_mis_data.remainingeffort_comment	AS remainingeffort_comment,
                null									AS eab_month,
                null									AS eab_pending_hours,
                null									AS hourly_rate,
                null									AS sum_fp_revenue,
                null									AS sum_fp_revenue_all,
                null									AS sum_fp_license_revenue,
                null									AS sum_fp_license_revenue_all,
                null									AS sum_tm_revenue,
                null									AS sum_tm_revenue_year,
                null									AS sum_cr_revenue,
                null									AS sum_cr_revenue_year,
                null									AS sum_mc_revenue,
                null									AS sum_mc_revenue_year,
                null									AS sum_mc_pax_revenue,
                null									AS sum_mc_pax_revenue_year
            FROM distinct_node
            CROSS JOIN period_month
        ";
        foreach ($bookingTypes as $id => $bt) {
            $sql .= "
            LEFT JOIN node_bt_hours node_${bt}_hours
                ON node_${bt}_hours.node_id = distinct_node.id
                AND node_${bt}_hours.month = period_month.month
                AND node_${bt}_hours.booking_type_id = $id
            ";
        }
        $sql .= "
            LEFT JOIN node_mis_data
                ON node_mis_data.node_id = distinct_node.id
                AND node_mis_data.month = period_month.month

            ORDER BY section, company_node_name, root_node_id, lvl, node_name, month
        ";

        /** @var PDOStatement $stmt */
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':PROFILE_ID', $profile->ID);
        $stmt->bindValue(':DATE_FROM', $periodFrom->get(Iso_Date::ORACLE_DATE));
        $stmt->bindValue(':DATE_UNTIL', $periodUntil->get(Iso_Date::ORACLE_DATE));

        // prepare data structure
        $data = [];
        $singleProjectData = [];
        $months = [];

        if ($stmt->execute()) {
            if ($profile->DISPLAY_FIGURES == DisplayFigure::ONCE_FOR_WHOLE_PERIOD) {
                $month = $periodFrom->get('dd.MM.yyyy') . ' - ' . $periodUntil->get('dd.MM.yyyy');
            }

            while (false !== ($row = $stmt->fetch())) {
                $nodeId = $row['NODE_ID'];

                if ($profile->DISPLAY_FIGURES == DisplayFigure::PER_MONTH) {
                    $month = $row['DISPLAY_MONTH'];
                }

                // add month to months array
                $months[$month] = true;

                // information related part
                if ($row['SECTION'] == 1) {
                    // prepare array structure
                    if (!isset($data[$nodeId])) {
                        $company = explode(' - ', $row['COMPANY_NODE_NAME']);

                        // common attributes
                        $data[$nodeId] = [
                            'node_id'                => $nodeId,
                            'node_name'                => $row['NODE_NAME'],
                            'node_type_name'        => $row['NODE_TYPE_NAME'],
                            'sap_key'                => $row['SAP_KEY'],
                            'target_effort'            => $row['TARGET_EFFORT'],
                            'maintenance_part'        => $row['MAINTENANCE_PART'],
                            'hourly_rate'            => $row['HOURLY_RATE'],
                            'company'                => $company[0],
                            'sectionData'            => [],
                            'sectionDataAvailable'    => [],
                            'projectData'            => []
                        ];
                    }

                    // add BT information sections
                    foreach ($btSections as $sect => $bt) {
                        $bt = $bt['name'];

                        if (!isset($data[$nodeId]['sectionData'][$sect])
                            || !isset($data[$nodeId]['sectionData'][$sect][$month])
                        ) {
                            $data[$nodeId]['sectionData'][$sect][$month] = [
                                'hours_bt_month'    => 0
                            ];
                            if ($sect != BookingType::NOT_ACCOUNTABLE) {
                                $data[$nodeId]['sectionData'][$sect][$month]['revenue_month'] = 0;
                            }
                        }
                        // helper variable
                        $ref = &$data[$nodeId]['sectionData'][$sect][$month];

                        // common attributes
                        $ref['hours_bt_month'] += (float)$row["SUM_${bt}_HOURS"];
                        if (isset($row["SUM_${bt}_REVENUE"])) {
                            $ref['revenue_month'] += (float)$row["SUM_${bt}_REVENUE"];
                        }

                        // section specific attributes
                        if ($sect == BookingType::FIXED_PRICE) {
                            if (!isset($ref['revenue_license_month'])) {
                                $ref['revenue_license_month'] = 0;
                            }
                            $ref['revenue_license_month'] += (float)$row["SUM_${bt}_LICENSE_REVENUE"];
                            $ref['revenue_license_all'] = (float)$row["SUM_${bt}_LICENSE_REVENUE_ALL"];

                            // fixed price's cumulative sum refers to all data
                            $ref['hours_bt_all'] = (float)$row["SUM_${bt}_HOURS_ALL"];
                            $ref['revenue_all'] = (float)$row["SUM_${bt}_REVENUE_ALL"];
                        } else {
                            // other booking type's cumlative sums refer to beginning of the year
                            $ref['hours_bt_year'] = (float)$row["SUM_${bt}_HOURS_YEAR"];
                            if (isset($row["SUM_${bt}_REVENUE_YEAR"])) {
                                $ref['revenue_year'] = (float)$row["SUM_${bt}_REVENUE_YEAR"];
                            }
                        }

                        if ($sect == BookingType::TIME_AND_MATERIAL) {
                            $ref['eab_month'] = $row['EAB_MONTH'];
                            $ref['eab_pending_hours'] = $row['EAB_PENDING_HOURS'];
                        }

                        if ($sect == BookingType::MAINTENANCE) {
                            if (!isset($ref['revenue_pax_month'])) {
                                $ref['revenue_pax_month'] = 0;
                            }
                            $ref['revenue_pax_month'] += (float)$row["SUM_${bt}_PAX_REVENUE"];
                            $ref['revenue_pax_year'] = (float)$row["SUM_${bt}_PAX_REVENUE_YEAR"];
                        }

                        // data availability indicator
                        if (!isset($data[$nodeId]['sectionDataAvailable'][$sect])) {
                            $data[$nodeId]['sectionDataAvailable'][$sect] = 0;
                        }
                        if ($data[$nodeId]['sectionDataAvailable'][$sect] < 3) {
                            foreach ($ref as $key => $val) {
                                $data[$nodeId]['sectionDataAvailable'][$sect] = max(
                                    $data[$nodeId]['sectionDataAvailable'][$sect],
                                    (int)($val > 0) +
                                    (int)($val > 0 && substr($key, -6) == '_month') +
                                    (int)($val > 0 && substr($key, -6) == '_month' && substr($key, 0, 6) == 'hours_')
                                );
                            }
                        }
                    }
                } elseif ($row['SECTION'] == 2) {
                    // project related part
                    // setup helper variable
                    if (!isset($data[$row['SAP_NODE_ID']])) {
                        if (!isset($singleProjectData[$nodeId . '|' . $row['LVL']])) {
                            $singleProjectData[$nodeId . '|' . $row['LVL']] = null;
                        }
                        $ref = &$singleProjectData[$nodeId . '|' . $row['LVL']];
                    } else {
                        if (!isset($data[$row['SAP_NODE_ID']]['projectData'][$nodeId . '|' . $row['LVL']])) {
                            $data[$row['SAP_NODE_ID']]['projectData'][$nodeId . '|' . $row['LVL']] = null;
                        }
                        $ref = &$data[$row['SAP_NODE_ID']]['projectData'][$nodeId . '|' . $row['LVL']];
                    }

                    if (null === $ref) {
                        // prepare array structure
                        $company = explode(' - ', $row['COMPANY_NODE_NAME']);
                        $ref = [
                            'node_id'        => $nodeId,
                            'node_name'        => $row['NODE_NAME'],
                            'level'            => $row['LVL'],
                            'target_effort'    => $row['TARGET_EFFORT'],
                            'company'        => $company[0],
                            'months'        => []
                        ];
                    }

                    // BT related hours
                    foreach ($bookingTypes as $id => $bt) {
                        if (!isset($ref['months'][$month]['hours_bt_month'][$bt])) {
                            $ref['months'][$month]['hours_bt_month'][$bt] = 0;
                            $ref['months'][$month]['hours_bt_trainee_month'][$bt] = 0;
                        }

                        // regular hours
                        $ref['months'][$month]['hours_bt_month'][$bt] += (float)$row["SUM_${bt}_HOURS"];
                        $ref['months'][$month]['hours_bt_year'][$bt] = (float)$row["SUM_${bt}_HOURS_YEAR"];

                        // trainee hours
                        $ref['months'][$month]['hours_bt_trainee_month'][$bt] += (float)$row["SUM_${bt}_TRAINEE_HOURS"];

                        if ($id == BookingType::FIXED_PRICE) {
                            $ref['months'][$month]['hours_bt_begin'][$bt] = (float)$row["SUM_${bt}_HOURS_ALL"];
                            $ref['months'][$month]['hours_bt_trainee_begin'][$bt] = (float)$row["SUM_${bt}_TRAINEE_HOURS_ALL"];
                        } else {
                            $ref['months'][$month]['hours_bt_begin'][$bt] = (float)$row["SUM_${bt}_HOURS_YEAR"];
                            $ref['months'][$month]['hours_bt_trainee_begin'][$bt] = (float)$row["SUM_${bt}_TRAINEE_HOURS_YEAR"];
                        }
                    }

                    // remaining effort values
                    $ref['months'][$month]['remaining_effort'] = $row['REMAININGEFFORT_VALUE'];
                    $ref['months'][$month]['remaining_effort_comment'] = $row['REMAININGEFFORT_COMMENT'];
                }
            }
        }

        foreach ($data as $nodeId => &$nodeData) {
            // remove sections with values == 0 (depending on configuration)
            foreach ($nodeData['sectionDataAvailable'] as $sect => $dataAvailable) {
                if ($profile->DISPLAY_LINES == DisplayLines::ONLY_IF_HOURS_AMOUNTS_IN_SELECTED_GENERAL_PERIOD && $dataAvailable < 1
                    || $profile->DISPLAY_LINES == DisplayLines::ONLY_IF_HOURS_AMOUNTS_IN_SELECTED_PERIOD && $dataAvailable < 2
                    || $profile->DISPLAY_LINES == DisplayLines::ONLY_IF_HOURS_IN_SELECTED_PERIOD && $dataAvailable < 3
                ) {
                    unset($nodeData['sectionData'][$sect]);
                }
            }
            unset($nodeData['sectionDataAvailable']);

            // remove SAP nodes which are empty due to the changes above
            if (empty($nodeData['sectionData'])) {
                // move related project data to single projects array
                if (!empty($nodeData['projectData'])) {
                    $singleProjectData += $nodeData['projectData'];
                }
                unset($data[$nodeId]);
            }
        }

        // append data from single projects
        if (!empty($singleProjectData)) {
            $data[-1] = $singleProjectData;
        }

        // get node selections
        $nodes = [
            'start'        => [],
            'project'    => []
        ];
        $sql = "
            SELECT
                nv1.name AS node_name,
                nv2.name AS single_node_name,
                single_node_types
            FROM bc_chart_profil_x_node
            LEFT JOIN node n1 ON n1.id = bc_chart_profil_x_node.node_id
            LEFT JOIN node_version nv1
                ON nv1.node_id = n1.id
                AND nv1.version_state_id = 1
            LEFT JOIN node n2 ON n2.id = bc_chart_profil_x_node.single_node_id
            LEFT JOIN node_version nv2
                ON nv2.node_id = n2.id
                AND nv2.version_state_id = 1
            WHERE bc_chart_profil_id = :PROFILE_ID
        ";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':PROFILE_ID', $profile->ID);
        $stmt->execute();

        while (false !== ($row = $stmt->fetch())) {
            if (null !== $row['NODE_NAME']) {
                $nodes['start'][] = $row['NODE_NAME'];
            }
            if (null !== $row['SINGLE_NODE_NAME']) {
                if (($row['SINGLE_NODE_TYPES'] & SingleNodeType::STATUS_DISTINCTROW) > 0) {
                    $nodes['project'][] = [
                        'name'            => $row['SINGLE_NODE_NAME'],
                        'with_next_lvl'    => ($row['SINGLE_NODE_TYPES'] & SingleNodeType::STATUS_WITHNEXTLEVEL) > 0
                    ];
                }
            }
        }

        $result = [
            'REPORT_INFO' => [
                'TYPE'                => $profile->ANZEIGE,
                'PERIODFROM'        => $periodFrom->get(Zend_Date::TIMESTAMP),
                'PERIODUNTIL'        => $periodUntil->get(Zend_Date::TIMESTAMP),
                'NODES'                => $nodes,
                'USED_INFORMATION'    => $usedInfNames,
                'DISPLAY_FIGURES'    => $profile->DISPLAY_FIGURES,
                'WITH_LOCKED_NODES'    => $profile->WITH_LOCKED_NODES,
                'DISPLAY_LINES' => $profile->DISPLAY_LINES
            ],
            'DATA' => $data,
            'MONTHS' => array_keys($months),
            'INFORMATION' => $infNames
        ];
        $cacheId = $profile->ID . '_' . $timestamp;
        $cache->save($result, $cacheId);

        return [
            'DATA'    => $result,
            'FROM'    => $periodFrom->get(Zend_Date::TIMESTAMP),
            'UNTIL'    => $periodUntil->get(Zend_Date::TIMESTAMP)
        ];
    }

    /**
     * Gets the data for the 'Order Overview" report
     */
    public static function loadOrderOverview($profile, $timestamp, $db, $session, $translate, $cache)
    {
        $report = new OrderOverview($db, $profile);

        $data = $report->getData();

        // save report data in cache for excel export
        $cacheId = $profile->ID . '_' . $timestamp;
        $cache->save([
            'REPORT_INFO' => [
                'TYPE' => $profile->ANZEIGE,
                'NODES' => self::getNodesOfProfile($profile->ID, $db),
                'PERIODFROM' => $report->getReportBeginDate()->get(Zend_Date::TIMESTAMP),
                'PERIODUNTIL' => $report->getReportEndDate()->get(Zend_Date::TIMESTAMP),
                'CLOSED_ORDERS' => $profile->CLOSED_ORDERS,
                'ORDER_AFTER_PERIOD' => $profile->ORDER_AFTER_PERIOD,
                'WITHOUT_ORDERS_AFTER_PERIOD' => $profile->WITHOUT_ORDERS_AFTER_PERIOD,
                'WITH_HIGHER_LEVEL_POSITION' => $profile->WITH_HIGHER_LEVEL_POSITION,
                'MATERIAL_FILTER' => $report->getMaterialFilter()
            ],
            'DATA' => $data
        ], $cacheId);

        return [
            'DATA' => $data,
            'FROM' => $report->getReportBeginDate()->get(Zend_Date::TIMESTAMP),
            'UNTIL' => $report->getReportEndDate()->get(Zend_Date::TIMESTAMP)
        ];
    }

    /**
     * Gets the data for the 'Order Overview" report
     */
    public static function loadContingentRange($profile, $timestamp, Zend_Db_Adapter_Abstract $db, $session, $translate)
    {
        // Determine selected companies, divisions, employees
        $companies = ChartProfil::getProfileCompanies($profile->ID);
        $divisions = ChartProfil::getProfileDivisions($profile->ID);
        $employees = ChartProfil::getProfileEmployees($db, $profile->ID)['id'];

        if (self::noEmployeeSelected($profile->ID, ["companies" => $companies, "divisions" => $divisions, "employees" => $employees])) {
            $employees = [Employee::DUMMY];
        }

        $employeesWhere = '';
        if (!empty($employees)) {
            $employeesWhere = QueryBuilder::createInClause('employee.id', $employees);
        }

        $companiesWhere = '';
        if (!empty($companies)) {
            $companiesWhere = QueryBuilder::createInClause('employment_period.company_id', $companies);
        }

        $divisionsWhere = '';
        if (!empty($divisions)) {
            $divisionsWhere = QueryBuilder::createInClause('employee_function_period.division_id', $divisions);
        }

        $sql = "
            WITH
                internal_employee AS (
                    SELECT
                        ba_employee.id,
                        employee.id AS internal_id,
                        employee.first_name,
                        employee.last_name,
                        employee.shortcut,
                        division.name AS division,
                        least(
                            current_date + INTERVAL '42' DAY,
                            coalesce(lastep.leaving_date, max_date()),
                            coalesce(employee.last_presence_in_office, max_date())
                        )::date AS report_end_date,
                        1 AS is_internal
                    FROM ba_employee
                    INNER JOIN employee ON employee.id = ba_employee.internal_employee_id
                    LEFT JOIN intranet.last_employment_period view_lastep ON view_lastep.employee_id = employee.id
                    LEFT JOIN employment_period lastep ON lastep.id = view_lastep.id
                    LEFT JOIN employment_period
                        ON employment_period.employee_id = employee.id
                        AND current_date BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                    LEFT JOIN employee_function_period
                        ON employee_function_period.employee_id = employee.id
                        AND current_date BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
                    LEFT JOIN division ON division.id = employee_function_period.division_id
                    " . QueryBuilder::conditionOr("WHERE", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
                ),
                external_employee AS (
                    SELECT
                        ba_employee.id,
                        ba_external_employee.id AS external_id,
                        ba_external_employee.first_name,
                        ba_external_employee.last_name,
                        null AS shortcut,
                        null AS division,
                        null::date AS report_end_date,
                        0 AS is_internal
                    FROM ba_employee
                    INNER JOIN ba_external_employee ON ba_external_employee.id = ba_employee.external_employee_id
                    WHERE 1 = :withExternalEmployees
                    AND coalesce(ba_external_employee.inactive_from, max_date()) > current_date
                ),
                report_employee AS (
                    SELECT * FROM internal_employee
                    UNION
                    SELECT * FROM external_employee 
                ),
                employee_period_data AS (
                    SELECT
                        internal_employee.id AS employee_id,
                        coalesce(sum(employment_period.hours_per_week / 5), 0) AS work_hours,
                        sum(
                            CASE WHEN absence.id IS NOT null THEN employment_period.hours_per_week / 5 ELSE 0 END
                        ) AS absence_hours
                    FROM internal_employee
                    INNER JOIN (
                        WITH RECURSIVE dates AS (
                            SELECT
                                current_date::date AS dt
                            UNION ALL
                            SELECT
                                (dt + INTERVAL '1' DAY)::date AS dt
                            FROM dates
                            WHERE dt + INTERVAL '1' DAY <= current_date + INTERVAL '42' DAY
                        )
                        SELECT dt FROM dates
                    ) days
                        ON days.dt BETWEEN current_date AND internal_employee.report_end_date
                    LEFT JOIN employment_period
                        ON employment_period.employee_id = internal_employee.internal_id
                        AND days.dt BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                    LEFT JOIN location ON location.id = employment_period.location_id
                    LEFT JOIN tpwpublicholidays
                        ON tpwpublicholidays.clocation_id = location.id
                        AND tpwpublicholidays.cdate = days.dt
                    LEFT JOIN absence
                        ON absence.employee_id = internal_employee.internal_id
                        AND days.dt BETWEEN absence.start_date AND absence.end_date
                        AND absence.category_id IN (1,2,3,4,7)
                    WHERE employment_period.id IS NOT null
                    AND (
                        location.name = 'Dubai' AND to_char(days.dt, 'D') NOT IN ('7','6')
                        OR location.name != 'Dubai' AND to_char(days.dt, 'D') NOT IN('1','7')
                    )
                    AND tpwpublicholidays.cid IS null
                    GROUP BY internal_employee.id
                ),
                report_contingent AS (
                    SELECT 
                        rc.contingent_id,
                        rc.employee_id, 
                        SUM(rc.booked_hours) AS booked_hours
                    FROM
                    (
                        SELECT
                            ba_contingent.id AS contingent_id,
                            ba_contingent.employee_id AS employee_id,
                            sum(coalesce(ba_confirmation_of_expense.hours, 0)) AS booked_hours
                        FROM ba_contingent
                        LEFT JOIN ba_confirmation_of_expense
                            ON ba_confirmation_of_expense.contingent_id =  ba_contingent.id
                            AND ba_confirmation_of_expense.month < date_trunc('month', current_date) - INTERVAL '1' DAY
                        WHERE ba_contingent.end_date >= current_date
                        GROUP BY ba_contingent.id, ba_contingent.employee_id
                        UNION
                        SELECT
                            ba_contingent.id AS contingent_id,
                            ba_contingent.employee_id AS employee_id,
                            sum(coalesce(tpw.chours, 0) + coalesce(tpw.choursal, 0)) AS booked_hours
                        FROM ba_contingent
                        INNER JOIN node
                            ON ba_contingent.node_id = node.id
                        LEFT JOIN tpwworkinghours tpw
                            ON node.id = tpw.cnode_id 
                                AND tpw.cdate BETWEEN date_trunc('month', current_date) AND current_date
                                AND tpw.cdate BETWEEN ba_contingent.begin_date AND ba_contingent.end_date
                        WHERE ba_contingent.end_date >= current_date
                        GROUP BY ba_contingent.id, ba_contingent.employee_id
                    ) rc
                    GROUP BY rc.contingent_id, rc.employee_id
                ),
                employee_monthly_booking AS (
                    SELECT
                        employee_id,
                        sum(hours) / count(DISTINCT month) AS hours
                    FROM ba_confirmation_of_expense
                    WHERE month < date_trunc('month', current_date)
                    GROUP BY employee_id
                )
            SELECT
                report_employee.id AS employee_id,
                report_employee.division AS employee_division,
                report_employee.shortcut AS employee_shortcut,
                report_employee.first_name AS employee_first_name,
                report_employee.last_name AS employee_last_name,
                report_employee.is_internal AS employee_is_internal,
                row_number() OVER (PARTITION BY report_employee.id ORDER BY ba_contingent.begin_date, ba_contingent.id) AS employee_contingent_number,
                ba_contingent.id AS contingent_id,
                ba_contingent.position AS contingent_position,
                ba_contingent.begin_date AS contingent_begin_date,
                ba_contingent.end_date AS contingent_end_date,
                ba_requisition_note.requisition_note_number,
                division.name AS contingent_division,
                ba_contingent.hours - coalesce(report_contingent.booked_hours, 0) AS remaining_hours,
                CASE
                    WHEN report_employee.is_internal = 1 THEN employee_period_data.work_hours
                    WHEN employee_monthly_booking.hours <> 0 THEN 1.5 * employee_monthly_booking.hours
                    ELSE 240
                END AS target_hours,
                coalesce(employee_period_data.absence_hours, 0) AS absence_hours,
                ba_contingent_type.name AS contingent_type_name
            FROM report_employee
            LEFT JOIN report_contingent ON report_contingent.employee_id = report_employee.id
            LEFT JOIN ba_contingent ON ba_contingent.id = report_contingent.contingent_id
            LEFT JOIN ba_requisition_note ON ba_requisition_note.id = ba_contingent.requisition_note_id
            LEFT JOIN ba_framework_contract ON ba_framework_contract.id = ba_requisition_note.framework_contract_id
            LEFT JOIN division ON division.id = ba_framework_contract.division_id
            LEFT JOIN employee_period_data ON employee_period_data.employee_id = report_employee.id
            LEFT JOIN employee_monthly_booking ON employee_monthly_booking.employee_id = report_employee.id
            LEFT JOIN ba_contingent_type ON ba_contingent_type.id = ba_contingent.type_id
            ORDER BY report_employee.is_internal, report_employee.division, report_employee.last_name, report_employee.first_name, employee_contingent_number, contingent_type_name DESC
        ";

        $stmt = $db->prepare($sql);
        $stmt->bindValue(':withExternalEmployees', (int)$profile->WITH_EXTERNAL_EMPLOYEES);
        $stmt->execute();

        $data = $stmt->fetchAll();

        $session->contingentRange[$timestamp] = [
            'REPORT_INFO' => [
                'TYPE' => $profile->ANZEIGE,
                'MONTH' => (new Zend_Date())->setTime(0)->get('dd.MM.yyyy')
            ],
            'DATA' => $data,
            'COUNT' => count($data)
        ];

        return ['timestamp' => $timestamp];
    }

    /**
     * Gets the data for the 'Accountable Hours" report
     */
    public static function loadAccountableHours($profile, $timestamp, $db, $session, $translate)
    {
        // determine period of this profile
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);

        $sqlEmployees = "
            SELECT
                v.*,
                CASE
                    WHEN v.std_gesamt-v.std_urlaub > 0 THEN round((v.std_abrechenbar / (v.std_gesamt-v.std_urlaub))::numeric * 100, 1)
                    ELSE 0
                    END AS prozent_std_abrechenbar
            FROM (
                SELECT
                    employee.shortcut AS ma_kuerzel,
                    employee.first_name || ' ' || employee.last_name AS name,
                    CASE WHEN division.name IS NOT NULL THEN division.name || ' -- ' || division.long_name ELSE NULL END AS aktueller_bereich,
                    CASE WHEN employment_period.id IS NOT NULL THEN 1 ELSE 0 END AS aktiv,
                    sum(coalesce(tpwworkinghours.chours, 0)) + sum(coalesce(tpwworkinghours.choursal, 0)) AS std_gesamt,
                    sum(CASE WHEN node_version.is_holiday = 1 THEN coalesce(chours, 0) ELSE 0 END) AS std_urlaub,
                    sum(CASE WHEN node_version.is_illness = 1 OR node_version.is_illness_child = 1 THEN coalesce(chours, 0) ELSE 0 END) AS std_krankheit,
                    sum(CASE WHEN node_version.is_short_time = 1 THEN coalesce(chours, 0) ELSE 0 END) AS std_kurzarbeit,
                    sum(CASE WHEN booking_type.name = 'NA' AND node_version.is_periodically_bookable = 0 
                        THEN coalesce(chours, 0) ELSE 0 END) + sum(CASE WHEN booking_type.name = 'NA' THEN coalesce(choursal, 0) ELSE 0 END) AS std_nicht_abrechenbar,
                    sum(CASE WHEN booking_type.name != 'NA' THEN coalesce(chours, 0) ELSE 0 END) + sum(CASE WHEN booking_type.name != 'NA' 
                        THEN coalesce(choursal, 0) ELSE 0 END) AS std_abrechenbar
                FROM employee
                     LEFT JOIN employment_period
                        ON employment_period.employee_id = employee.id
                            AND date_trunc('day', now()) BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                     LEFT JOIN employee_function_period
                        ON employee_function_period.employee_id = employee.id
                            AND date_trunc('day', now()) BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
                     LEFT JOIN division ON division.id = employee_function_period.division_id
                     LEFT JOIN tpwworkinghours ON tpwworkinghours.cemployee_id = employee.id
                     LEFT JOIN node ON node.id = tpwworkinghours.cnode_id
                     LEFT JOIN node_version ON node_version.node_id = node.id AND node_version.version_state_id = 1
                     LEFT JOIN booking_type ON booking_type.id = tpwworkinghours.cbooking_type_id
                WHERE tpwworkinghours.cdate BETWEEN to_date(:dateFrom, 'YYYY-MM-DD') AND to_date(:dateTill, 'YYYY-MM-DD')
                GROUP BY employee.shortcut, employee.first_name, employee.last_name, division.name, division.long_name, employment_period.id
            ) v
            ORDER BY prozent_std_abrechenbar, std_gesamt, std_nicht_abrechenbar
        ";

        $statement = $db->prepare($sqlEmployees);
        $statement->bindValue(':dateFrom', $periodFrom->get(Iso_Date::ORACLE_DATE));
        $statement->bindValue(':dateTill', $periodUntil->get(Iso_Date::ORACLE_DATE));
        $statement->execute();
        $resultEmployees = $statement->fetchAll();

        $sqlCompanies = "
            SELECT
                v.*,
                CASE
                    WHEN v.std_gesamt-v.std_urlaub > 0 THEN round((v.std_abrechenbar / (v.std_gesamt-v.std_urlaub))::numeric * 100, 1)
                    ELSE 0
                    END AS prozent_std_abrechenbar
            FROM (
                SELECT
                    company.shortcut AS firmenkuerzel,
                    CASE WHEN company.name IS NOT NULL THEN company.name ELSE 'keine Firmenzuordnung' END AS firma,
                    sum(coalesce(tpwworkinghours.chours, 0)) + sum(coalesce(tpwworkinghours.choursal, 0)) AS std_gesamt,
                    sum(CASE WHEN node_version.is_holiday = 1 THEN coalesce(chours, 0) ELSE 0 END) AS std_urlaub,
                    sum(CASE WHEN node_version.is_illness = 1 OR node_version.is_illness_child = 1 THEN coalesce(chours, 0) ELSE 0 END) AS std_krankheit,
                    sum(CASE WHEN node_version.is_short_time = 1 THEN coalesce(chours, 0) ELSE 0 END) AS std_kurzarbeit,
                    sum(CASE WHEN booking_type.name = 'NA' AND node_version.is_periodically_bookable = 0
                        THEN coalesce(chours, 0) ELSE 0 END) + sum(CASE WHEN booking_type.name = 'NA' THEN coalesce(choursal, 0) ELSE 0 END) AS std_nicht_abrechenbar,
                    sum(CASE WHEN booking_type.name != 'NA' THEN coalesce(chours, 0) ELSE 0 END) + sum(CASE WHEN booking_type.name != 'NA' 
                        THEN coalesce(choursal, 0) ELSE 0 END) AS std_abrechenbar
                FROM employee
                    INNER JOIN tpwworkinghours ON tpwworkinghours.cemployee_id = employee.id
                    LEFT JOIN employment_period
                        ON employment_period.employee_id = employee.id
                            AND tpwworkinghours.cdate BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
                    LEFT JOIN company ON company.id = employment_period.company_id
                    LEFT JOIN node ON node.id = tpwworkinghours.cnode_id
                    LEFT JOIN node_version ON node_version.node_id = node.id AND node_version.version_state_id = 1
                    LEFT JOIN booking_type ON booking_type.id = tpwworkinghours.cbooking_type_id
                WHERE tpwworkinghours.cdate BETWEEN to_date(:dateFrom, 'YYYY-MM-DD') AND to_date(:dateTill, 'YYYY-MM-DD')
                GROUP BY company.shortcut, company.name
            ) v
            ORDER BY prozent_std_abrechenbar, std_gesamt, std_nicht_abrechenbar
        ";

        $statementCompanies = $db->prepare($sqlCompanies);
        $statementCompanies->bindValue(':dateFrom', $periodFrom->get(Iso_Date::ORACLE_DATE));
        $statementCompanies->bindValue(':dateTill', $periodUntil->get(Iso_Date::ORACLE_DATE));
        $statementCompanies->execute();
        $resultCompanies = $statementCompanies->fetchAll();

        $sqlDivisions = "
            SELECT
                v.*,
                CASE
                    WHEN v.std_gesamt-v.std_urlaub > 0 THEN round((v.std_abrechenbar / (v.std_gesamt-v.std_urlaub))::numeric * 100, 1)
                    ELSE 0
                    END AS prozent_std_abrechenbar
            FROM (
                SELECT
                    CASE WHEN division.name IS NOT NULL THEN division.name || ' -- ' || division.long_name ELSE 'keine Bereichszuordnung' END AS bereich,
                    sum(coalesce(tpwworkinghours.chours, 0)) + sum(coalesce(tpwworkinghours.choursal, 0)) AS std_gesamt,
                    sum(CASE WHEN node_version.is_holiday = 1 THEN coalesce(chours, 0) ELSE 0 END) AS std_urlaub,
                    sum(CASE WHEN node_version.is_illness = 1 OR node_version.is_illness_child = 1 THEN coalesce(chours, 0) ELSE 0 END) AS std_krankheit,
                    sum(CASE WHEN node_version.is_short_time = 1 THEN coalesce(chours, 0) ELSE 0 END) AS std_kurzarbeit,
                    sum(CASE WHEN booking_type.name = 'NA' AND node_version.is_periodically_bookable = 0
                        THEN coalesce(chours, 0) ELSE 0 END) + sum(CASE WHEN booking_type.name = 'NA' THEN coalesce(choursal, 0) ELSE 0 END) AS std_nicht_abrechenbar,
                    sum(CASE WHEN booking_type.name != 'NA' THEN coalesce(chours, 0) ELSE 0 END) + sum(CASE WHEN booking_type.name != 'NA' 
                        THEN coalesce(choursal, 0) ELSE 0 END) AS std_abrechenbar
                FROM employee
                    INNER JOIN tpwworkinghours ON tpwworkinghours.cemployee_id = employee.id
                    LEFT JOIN employee_function_period
                        ON employee_function_period.employee_id = employee.id
                            AND tpwworkinghours.cdate BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
                    LEFT JOIN division ON division.id = employee_function_period.division_id
                    LEFT JOIN node ON node.id = tpwworkinghours.cnode_id
                    LEFT JOIN node_version ON node_version.node_id = node.id AND node_version.version_state_id = 1
                    LEFT JOIN booking_type ON booking_type.id = tpwworkinghours.cbooking_type_id
                WHERE tpwworkinghours.cdate BETWEEN to_date(:dateFrom, 'YYYY-MM-DD') AND to_date(:dateTill, 'YYYY-MM-DD')
                GROUP BY division.name, division.long_name
            ) v
            ORDER BY prozent_std_abrechenbar, std_gesamt, std_nicht_abrechenbar
        ";

        $statementDivisions = $db->prepare($sqlDivisions);
        $statementDivisions->bindValue(':dateFrom', $periodFrom->get(Iso_Date::ORACLE_DATE));
        $statementDivisions->bindValue(':dateTill', $periodUntil->get(Iso_Date::ORACLE_DATE));
        $statementDivisions->execute();
        $resultDivisions = $statementDivisions->fetchAll();

        $data = [
            'EMPLOYEES' => $resultEmployees,
            'DIVISIONS' => $resultDivisions,
            'COMPANIES' => $resultCompanies
        ];

        $result = [
            'REPORT_INFO' => [
                'TYPE'                => $profile->ANZEIGE,
                'PERIODFROM'        => $periodFrom->get(Zend_Date::TIMESTAMP),
                'PERIODUNTIL'        => $periodUntil->get(Zend_Date::TIMESTAMP)
            ],
            'DATA' => $data,
        ];

        $session->accountableHours[$timestamp] = $result;
        return ['timestamp' => $timestamp];
    }

    /**
     * Fetches employees with BC permissions or employees with BC permisson and without permission
     *
     * return array
     */
    private static function getEmployeesWithPermissions($selectedGroupsAndEmployeesID, $db, $profile)
    {
        $isSpecialGroupSelected = ChartProfil::isSpecialGroupSelected($selectedGroupsAndEmployeesID, $db);

        $companiesWhere = '';
        $divisionsWhere = '';
        $employeesWhere = '';
        $bkzcockpitRightWhere = '';

        if (false == $isSpecialGroupSelected) {
            $companies = ChartProfil::getCompanies($selectedGroupsAndEmployeesID, $db);
            $divisions = ChartProfil::getDivisions($selectedGroupsAndEmployeesID, $db);
            $employees = ChartProfil::getEmployees($selectedGroupsAndEmployeesID, $db);

            $userAndGroupIds = self::getUserAndGroupIdsOfProfile($profile->ID, $db);

            // if resolved employee selection results in no employees at all (e.g. due to empty groups), then report has no data
            if (!empty($userAndGroupIds) && self::noEmployeeSelected($profile->ID, [
                    "companies" => $companies,
                    "divisions" => $divisions,
                    "employees" => $employees
                ])
            ) {
                $employees = [Employee::DUMMY];
            }

            if (!empty($companies)) {
                $companiesWhere = "employment_period.company_id IN (" . implode(',', $companies) . ")";
            }

            if (!empty($divisions)) {
                $divisionsWhere = "employee_function_period.division_id IN (" . implode(',', $divisions) . ")";
            }

            if (!empty($employees)) {
                $employeesWhere = QueryBuilder::createInClause('emp.ID', $employees);
            }
        }

        if ($profile->EMP_WITHOUT_BC_PERMISSION) {
            $mitarbeiterJoin = ' left join mitarbeiter pdbEmp on upper(emp.shortcut) = upper(pdbEmp.kuerzel) ';
        } else {
            $mitarbeiterJoin = ' inner join mitarbeiter pdbEmp on upper(emp.shortcut) = upper(pdbEmp.kuerzel) ';
        }

        if (!$profile->EMP_WITHOUT_BC_PERMISSION) {
            $bkzcockpitRightWhere = ' and (pdbEmp.BKZCOCKPIT_RECHTE & 1) = 1 ';// first bit means 'permission to use bc'
        }

        //Get all employees which are active at execution time of report and, if given, which fullfill additional conditions
        $sql = "select emp.ID as emp_id,
			lower(emp.SHORTCUT) as shortcut,
			emp.FIRST_NAME,
			emp.LAST_NAME,
			pdbEmp.ID as pdb_emp_id,
			pdbEmp.BKZCOCKPIT_RECHTE as bkzcockpit_right
			from EMPLOYEE emp
			" . $mitarbeiterJoin . "
			inner join employment_period
				 ON employment_period.employee_id = emp.ID
				 AND current_date BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
			left join employee_function_period
				ON employee_function_period.employee_id = emp.ID
			  	AND current_date BETWEEN employee_function_period.start_date AND coalesce(employee_function_period.end_date, max_date())
			where (pdbEmp.ID_FIRMA = 1 or pdbEmp.ID_FIRMA is NULL)" . // ID_FIRMA is NULL is for the left join
            $bkzcockpitRightWhere . "
			" . QueryBuilder::conditionOr("AND", [$companiesWhere, $divisionsWhere, $employeesWhere]) . "
			order by shortcut";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

        $listEmployeesWithNoBCPermission = [];
        $listEmployeesWithBCPermission = [];

        if ($profile->EMP_WITHOUT_BC_PERMISSION) {
            foreach ($result as $key => $item) {
                if (!empty($item['BKZCOCKPIT_RECHTE'])) {
                    $listEmployeesWithNoBCPermission[] = $item;
                } else {
                    $listEmployeesWithBCPermission[] = $item;
                }
            }
            return ['listEmployeesWithBCPermission' => $listEmployeesWithBCPermission, 'listEmployeesWithNoBCPermission' => $listEmployeesWithNoBCPermission];
        }

        return ['listEmployeesWithBCPermission' => $result];
    }
    /**
     * Map  holiday applications to employees
     */
    private static function mapHolidayAppToEmp($firstIndex, $empShortcut, $holidayAppOfEmp, &$toArray)
    {
        if (!empty($holidayAppOfEmp[$empShortcut])) {
            foreach ($holidayAppOfEmp[$empShortcut] as $holidayApplications) {
                $toArray[$firstIndex][$empShortcut][] = $holidayApplications;
            }
        }
    }

    /**
     * Checks if group 'Alle' is selected
     */
    private static function isSpecialGroupSelected($selectedGroupsAndEmployeesID, $db)
    {
        $groups = [];
        foreach ($selectedGroupsAndEmployeesID as $item) {
            if ($item['ISGROUP']) {
                array_push($groups, $item['GROUP_ID']);
            }
        }

        if (empty($groups)) {
            return false;
        }

        $sql = "
			select count(*) as rec_count
			from employee_group eg
			where upper(eg.name) = upper('Alle') and " . QueryBuilder::createInClause('eg.id', $groups);

        $statement = $db->prepare($sql);
        $statement->execute();
        $result = (int)$statement->fetch(Zend_Db::FETCH_COLUMN);

        return $result !== 0;
    }

    /**
     * Gets array of all companies.
     */
    private static function getCompanies($selectedGroupsAndEmployeesID, $db)
    {
        $companies = [];
        foreach ($selectedGroupsAndEmployeesID as $item) {
            if ($item['ISGROUP']) {
                array_push($companies, $item['GROUP_ID']);
            }
        }

        if (empty($companies)) {
            return [];
        }

        $sql = "
			select cmp.id
			from employee_group eg
        	inner join company cmp on upper(eg.name) = upper(cmp.shortcut)
			where " . QueryBuilder::createInClause('eg.id', $companies);

        $statement = $db->prepare($sql);
        $statement->execute();

        return $statement->fetchAll(Zend_Db::FETCH_COLUMN);
    }

    /**
     * Gets array of all divisions.
     */
    private static function getDivisions($selectedGroupsAndEmployeesID, $db)
    {
        $divisions = [];
        foreach ($selectedGroupsAndEmployeesID as $item) {
            if ($item['ISGROUP']) {
                array_push($divisions, $item['GROUP_ID']);
            }
        }

        if (empty($divisions)) {
            return [];
        }

        $sql = "
			select div.id
			from employee_group eg
			inner join division div on upper(eg.name) = upper(div.name || ' -- ' || div.long_name)
			where " . QueryBuilder::createInClause('eg.id', $divisions);

        $statement = $db->prepare($sql);
        $statement->execute();

        return $statement->fetchAll(Zend_Db::FETCH_COLUMN);
    }

    /**
     * Gets array of all employees.
     */
    private static function getEmployees($selectedGroupsAndEmployeesID, $db)
    {
        $groups = [];
        $employees = [];
        foreach ($selectedGroupsAndEmployeesID as $item) {
            if ($item['ISGROUP']) {
                array_push($groups, $item['GROUP_ID']);
            } else {
                array_push($employees, $item['EMPLOYEE_ID']);
            }
        }

        if (empty($groups) && empty($employees)) {
            return [];
        }

        $sql = "
            select distinct
            e.id
            from employee e
            where " . QueryBuilder::conditionOr("", ["1 = 0", QueryBuilder::createInClause('e.id', $employees)]) . "
	        
	        UNION 
            
            select distinct
            e.id
            from employee_group eg
            inner join employee_x_employee_group exeg on eg.id = exeg.employee_group_id
            inner join employee e on exeg.employee_id = e.id
            where upper(eg.name) not in (select upper(shortcut) from company)
            " . QueryBuilder::conditionOr('and', ["1 = 0", QueryBuilder::createInClause('eg.id', $groups)]) . "
	        ";

        $statement = $db->prepare($sql);
        $statement->execute();

        return $statement->fetchAll(Zend_Db::FETCH_COLUMN);
    }

    /**
     * Gets assigned nodes
     *
     * @param array $empIds
     */
    private static function getAssignedNodes($empIds, $db, $translate)
    {
        $sql = "
            SELECT
                emp.shortcut || ' (' || emp.first_name || ' ' || emp.last_name || ')' as employeeName,
                '" . $translate->translate('BtNode') . "' AS permissionkind,
                node_version.name AS value,
                node.id AS nodeid
            FROM employee_node_permission enp
            INNER JOIN employee emp
                ON enp.employee_id = emp.id
                AND enp.employee_id in (" . implode(',', $empIds) . ")
            INNER JOIN node ON enp.node_id = node.id
            INNER JOIN node_version
                ON node_version.node_id = node.id
                AND node_version.version_state_id = 1
        ";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * Gets sets
     *
     * @param array $empIds
     */
    private static function getAssignedSets($empIds, $db, $translate)
    {
        $sql = "
			select
			emp.SHORTCUT || ' (' || emp.FIRST_NAME || ' ' || emp.LAST_NAME || ')' as employeeName,
			'" . $translate->translate('set') . "' as PERMISSIONKIND,
			s.NAME as VALUE
			from NODESET s
			inner join EMPLOYEE_SET_PERMISSION esp on (esp.SET_ID = s.ID and EMPLOYEE_ID in (" . implode(',', $empIds) . "))
			inner join EMPLOYEE emp on emp.id = esp.EMPLOYEE_ID
		";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * Gets employees
     *
     * @param array $empIds
     */
    private static function getAssignedEmployees($empIds, $db, $translate)
    {
        $sql = "
            SELECT
                emp2.SHORTCUT || ' (' || emp2.first_name || ' ' || emp2.last_name || ')' AS employeeName,
                '" . $translate->translate('mitarbeiter') . "' AS permissionkind,
                emp.shortcut || ' ' || '(' || emp.last_name || ' ' || emp.first_name || ')'
                    || CASE WHEN employee_state.is_active = 0 THEN ' [' || '" . $translate->translate('inactive') . "' || ']' ELSE '' END
                    AS value
            FROM employee emp
            INNER JOIN employee_state ON employee_state.employee_id = emp.id
            INNER JOIN employee_permission ep 
                ON ep.assigned_employee_id = emp.id 
                AND privileged_employee_id IN (" . implode(',', $empIds) . ")
            INNER JOIN employee emp2 ON ep.privileged_employee_id = emp2.id
        ";

        $stmt = $db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Gets employee groups
     *
     * @param array $empIds
     */
    private static function getAssignedEmployeeGroups($empIds, $db, $translate)
    {
        $sql = "
			select
			emp.SHORTCUT || ' (' || emp.FIRST_NAME || ' ' || emp.LAST_NAME || ')' as employeeName,
			'" . $translate->translate('group') . "' as PERMISSIONKIND,
			eg.NAME as VALUE
			from
			EMPLOYEE_GROUP eg
			inner join EMPLOYEE_GROUP_PERMISSION egp on (egp.EMPLOYEE_GROUP_ID = eg.ID and egp.EMPLOYEE_ID in (" . implode(',', $empIds) . "))
			inner join employee emp
			on emp.id = egp.employee_id
		";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * Gets employee which have no nodes, sets and groups and employees
     *
     * @param array $empIds
     */
    private static function getEmployeesWithNothing($empIds, $db, $translate)
    {
        if (count($empIds) > 1) {
            $empIds = implode(',', $empIds);
        } else {
            $empIds = $empIds[0];
        }

        $sql = "
			SELECT
			SHORTCUT || ' (' || FIRST_NAME || ' ' || LAST_NAME || ')' as employeeName,
			'" . $translate->translate('noBCPermissionEntry') . "' as PERMISSIONKIND
			FROM employee
			WHERE ID IN (" . $empIds . ")
			AND ID NOT IN (
				SELECT PRIVILEGED_EMPLOYEE_ID as EMPLOYEE_ID FROM EMPLOYEE_PERMISSION
				UNION
				SELECT EMPLOYEE_ID
				FROM EMPLOYEE_SET_PERMISSION
				UNION
				SELECT EMPLOYEE_ID
				FROM EMPLOYEE_GROUP_PERMISSION
				UNION
				SELECT EMPLOYEE_ID
				FROM EMPLOYEE_NODE_PERMISSION
			)
		";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * Recursively go through the tree and cut off empty node, i.e. nodes
     * without hours, cost or revenue
     */
    private static function cutOffEmptyNodes(&$node, $profil)
    {
        for ($i = 0, $c = count($node['children']); $i < $c; $i++) {
            if ($node['children'][$i]['hours'] == 0
                && (!$profil->COST_AND_REVENUE || $node['children'][$i]['cost'] == 0 && $node['children'][$i]['revenue'] == 0)
                && (!$profil->TARGET_HOURS || $node['children'][$i]['target_effort'] == 0 && $node['children'][$i]['remaining_effort'] == null)
            ) {
                // remove child
                array_splice($node['children'], $i--, 1);
                $c -= 1;
            } else {
                // continue with child's children
                ChartProfil::cutOffEmptyNodes($node['children'][$i], $profil);
            }
        }
    }

    /**
     * Add the detailed booking info to the tree using the index.
     */
    private static function addBookingInfo($refLeavesWithBookings, $employeeBookings, $refNodes, Zend_Locale $locale)
    {
        $symbols = Zend_Locale_Data::getList($locale, 'symbols');

        foreach ($refLeavesWithBookings as $key => $leafNode) {
            if (isset($employeeBookings[$leafNode['id']])) { //if there is booking for that node then add information, in other case remove leaf
                $bookingInfo = $employeeBookings[$leafNode['id']];

                $bookingsString = '';
                // iterate over the employees booked on this node
                foreach ($bookingInfo as $empBookingEntries) {
                    $bookingsString .= sprintf(
                        '<img src="files/images/user_gray.png" />%sh %s<br />',
                        number_format($empBookingEntries['Hours'], 2, $symbols['decimal'], $symbols['group']),
                        $empBookingEntries['Employee']
                    );

                    // iterate over the bookings of one emplyee
                    foreach ($empBookingEntries['Bookings'] as $empBookingEntry) {
                        $bookingsString .= sprintf(
                            '<span class="indent">%sh %s %s %s</span><br />',
                            number_format($empBookingEntry['Hours'], 2, $symbols['decimal'], $symbols['group']),
                            $empBookingEntry['BT'],
                            $empBookingEntry['Date'],
                            htmlspecialchars($empBookingEntry['Note'])
                        );
                    }
                }

                // Display bookings on not bookable leafs grey
                if ($refNodes[$leafNode['id']]['bookable']) {
                    $refNodes[$leafNode['id']]['bookings'] = $bookingsString;
                } else {
                    $refNodes[$leafNode['id']]['bookings'] = '<span style="color:grey"><i>' . $bookingsString . '</i></span>';
                }
            } else {
                unset($refLeavesWithBookings[$key]);
            }
        }
    }

    /**
     * Add the detailed day off booking info to the tree using the index.
     */
    private static function addBookingInfoDayOff($employeeDayOffs, $refNodes, Zend_Locale $locale)
    {
        $symbols = Zend_Locale_Data::getList($locale, 'symbols');

        $bookingInfo = $employeeDayOffs[0];

        $bookingsString = '';
        // iterate over the employees booked on this node
        foreach ($bookingInfo as $empBookingEntries) {
            $bookingsString .= sprintf(
                '<img src="files/images/user_gray.png" />%sh %s<br />',
                number_format($empBookingEntries['Hours'], 2, $symbols['decimal'], $symbols['group']),
                $empBookingEntries['Employee']
            );

            // iterate over the bookings of one emplyee
            foreach ($empBookingEntries['Bookings'] as $empBookingEntry) {
                $bookingsString .= sprintf(
                    '<span class="indent">%sh %s %s %s</span><br />',
                    number_format($empBookingEntry['Hours'], 2, $symbols['decimal'], $symbols['group']),
                    $empBookingEntry['BT'],
                    $empBookingEntry['Date'],
                    htmlspecialchars($empBookingEntry['Note'])
                );
            }
        }
        //-2 is the id of the day off root node
        $refNodes[-2]['bookings'] = $bookingsString;
    }


    /**
     * Gets array of all employee ids assinged to a profile.
     *
     * @param object $db
     * @param integer $profileId
     * @param integer $activeFrom
     * @param integer $activeTo
     */
    private static function getProfileEmployees($db, $profileId, $considerAll = true)
    {
        $hasMagicGroup = ChartProfil::isEmployeeGroupSelected($profileId);

        if ($hasMagicGroup && $considerAll) {
            return [
                'id' => [],
                'shortcut' => []
            ];
        } else {
            $sql = "
                SELECT
                    *
                FROM (
                    SELECT
                        DISTINCT
                        e.id,
                        e.shortcut
                    FROM bc_chart_profil_x_employee chpxe
                    INNER JOIN employee e ON chpxe.employee_id = e.id
                    WHERE chpxe.bc_chart_profil_id = :PROFILE_ID
                ) t

                UNION

                SELECT
                    DISTINCT
                    e.id,
                    e.shortcut
                FROM bc_chart_profil_x_employee chpxe
                INNER JOIN employee_group eg ON chpxe.employee_group_id = eg.id
                INNER JOIN employee_x_employee_group exeg ON eg.id = exeg.employee_group_id
                INNER JOIN employee e ON exeg.employee_id = e.id
                WHERE bc_chart_profil_id = :PROFILE_ID
                AND upper(eg.name) NOT IN (SELECT upper(shortcut) FROM company)
                AND upper(eg.name) NOT IN (SELECT upper(name || ' -- ' || long_name) FROM division)
            ";
        }

        // Determine the selected users
        $statement = $db->prepare($sql);
        $statement->bindValue(':PROFILE_ID', $profileId);
        $statement->execute();
        $rows = $statement->fetchAll();

        $employeeIdArr = [];
        $employeeShortcutArr = [];
        foreach ($rows as $item) {
            if (empty($item['ID']) && empty($item['EMPLOYEE_ID'])) {
                continue;
            }

            $employeeIdArr[] = $item['ID'] ? (int)$item['ID'] : (int)$item['EMPLOYEE_ID'];
            $employeeShortcutArr[] = $item['ID'] ? strtolower($item['SHORTCUT']) : strtolower($item['SHORTCUT_EMPLOYEE_ID']);
        }

        sort($employeeShortcutArr);
        return [
            'id' => array_unique($employeeIdArr),
            'shortcut' => array_unique($employeeShortcutArr)
        ];
    }

    /**
     * Gets array of all companies assinged to a profile.
     *
     * @param object $db
     * @param integer $profileId
     */
    private static function getProfileCompanies($profileId, $considerAll = true)
    {
        if ($considerAll == true) {
            $hasMagicGroup = ChartProfil::isEmployeeGroupSelected($profileId, 'Alle');

            if ($hasMagicGroup) {
                return [];
            }
        }

        $sql = "
			select cmp.id
			from bc_chart_profil_x_employee chpxe
			inner join employee_group eg on chpxe.employee_group_id = eg.id
        	inner join company cmp on upper(eg.name) = upper(cmp.shortcut)
			where chpxe.bc_chart_profil_id = :PROFILE_ID
		";

        $statement = Zend_Registry::get('db')->prepare($sql);
        $statement->bindValue(':PROFILE_ID', $profileId);
        $statement->execute();

        return $statement->fetchAll(Zend_Db::FETCH_COLUMN);
    }

    /**
     * Gets array of all divisions assinged to a profile.
     *
     * @param object $db
     * @param integer $profileId
     */
    private static function getProfileDivisions($profileId, $considerAll = true)
    {
        if ($considerAll == true) {
            $hasMagicGroup = ChartProfil::isEmployeeGroupSelected($profileId, 'Alle');

            if ($hasMagicGroup) {
                return [];
            }
        }

        $sql = "
			select division.id
			from bc_chart_profil_x_employee chpxe
			inner join employee_group eg on chpxe.employee_group_id = eg.id
			inner join division on upper(eg.name) = upper(division.name || ' -- ' || division.long_name)
			where chpxe.bc_chart_profil_id = :PROFILE_ID
		";

        $statement = Zend_Registry::get('db')->prepare($sql);
        $statement->bindValue(':PROFILE_ID', $profileId);
        $statement->execute();

        return $statement->fetchAll(Zend_Db::FETCH_COLUMN);
    }

    /**
     * Checks if the employee group $groupName is selected in a profile.
     * Default group name is 'Alle'.
     *
     * @param int $profileId
     * @param string $groupName
     * @return bool
     */
    private static function isEmployeeGroupSelected($profileId, $groupName = 'Alle')
    {
        $sql = "
            SELECT eg.name
            FROM bc_chart_profil_x_employee chpxe
            INNER JOIN  employee_group eg ON chpxe.employee_group_id = eg.id
            WHERE chpxe.bc_chart_profil_id = :PROFILE_ID
        ";

        $stmt = Zend_Registry::get('db')->prepare($sql);
        $stmt->bindParam(':PROFILE_ID', $profileId);
        $stmt->execute();
        $result = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

        foreach ($result as $item) {
            if (strtoupper($item['NAME']) == strtoupper($groupName)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine the booking tree nodes of the given profile
     *
     * @param integer $profileId
     * @param object $db
     * @param boolean $clearIfRoot empty list is returned in case only root node is selected
     */
    public static function getProfileNodes($profileId, $db, $clearIfRoot = true)
    {
        // Gets explicit nodes and sets nodes together (using with as clause) and returns unique nodes ids, i.e.:
        // which child subtrees are independed (main select clause).
        // Nodes with overlapping paths are removed (paths which has beginning like another whole path)
        $sql = "
            WITH paths AS
            (
                SELECT * FROM
                (
                    WITH RECURSIVE nodes AS (
                        SELECT
                            node.id,
                            '/' || node.id AS path
                        FROM node
                        WHERE parent_node_id IS NULL
                        UNION ALL
                        SELECT
                            node.id,
                            nodes.path || '/' || node.id AS path
                        FROM node
                                 INNER JOIN nodes ON nodes.id = node.parent_node_id
                    ) select * from nodes order by id
                ) p
                INNER JOIN (
                    SELECT DISTINCT
                        CASE
                            WHEN bc_chart_profil_x_node.node_id IS NOT null THEN bc_chart_profil_x_node.node_id
                            ELSE nxn.node_id
                        END AS node_id
                    FROM bc_chart_profil_x_node
                    LEFT JOIN nodeset_x_node nxn ON nxn.set_id = bc_chart_profil_x_node.set_id
                    WHERE (nxn.node_id IS NOT null OR bc_chart_profil_x_node.node_id IS NOT null)
                    AND bc_chart_profil_id = :PROFIL_ID
                ) n
                    ON p.id = n.node_id
                ORDER BY path
            )
            SELECT p1.id
            FROM paths p1
            WHERE p1.path NOT IN (
                SELECT p1.path
                FROM paths p2
                WHERE p1.path != p2.path
                AND substr(p1.path||'/', 0, length(p2.path||'/')) = p2.path||'/'
            )
        ";

        $statement = $db->prepare($sql);
        $statement->bindValue(':PROFIL_ID', $profileId);
        $statement->execute();
        $rows = $statement->fetchAll(Zend_Db::FETCH_COLUMN);

        // Check if root node is selected, i.e. 'ISO-Gruppe'
        if ($clearIfRoot && count($rows) == 1) {
            $sql = "SELECT parent_node_id FROM node WHERE id = :NODE_ID";

            $statement = $db->prepare($sql);
            $statement->bindValue(':NODE_ID', $rows[0]);
            $statement->execute();
            $parent = $statement->fetch(Zend_Db::FETCH_COLUMN);

            if ($parent !== false && $parent === null) {
                $rows = [];
            }
        }

        return $rows;
    }

    /**
     * Determine the display nodes of the given profile (table report)
     *
     * @param integer $profileId
     * @param object $db
     */
    private static function getDisplayNodes($profileId, $db)
    {
        $sql = "
            SELECT
                DISTINCT
                bc_chart_profil_x_node.single_node_id AS node_id,
                node_version.name AS node_name,
                node_version.TARGET_EFFORT,
                cc_with_sap_key.sap_key AS sap_key
            FROM bc_chart_profil_x_node
            INNER JOIN bc_chart_profil ON bc_chart_profil_x_node.bc_chart_profil_id = bc_chart_profil.id
            INNER JOIN node ON bc_chart_profil_x_node.single_node_id = node.id
            INNER JOIN node_version
                ON node_version.node_id = node.id
                AND node_version.version_state_id = 1
            LEFT JOIN cc_with_sap_key ON node.id = cc_with_sap_key.node_id
            WHERE bc_chart_profil_x_node.single_node_id IS NOT NULL
            AND bc_chart_profil.id = :PROFIL_ID
        ";

        $statement = $db->prepare($sql);
        $statement->bindValue(':PROFIL_ID', $profileId);
        $statement->execute();
        $rows = $statement->fetchAll();

        $nodeIdArr = [];
        $nodeNameArr = [];
        $nodeSapKeyArr = [];
        $nodeCCArr = [];
        foreach ($rows as $item) {
            $nodeIdArr[$item['NODE_ID']] = [
                'TARGET_EFFORT' => $item['TARGET_EFFORT']
            ];
            $nodeNameArr[$item['NODE_ID']] = $item['NODE_NAME'];
            $nodeSapKeyArr[$item['NODE_ID']] = $item['SAP_KEY'];

            $sql = /** @lang SQL */"
                WITH RECURSIVE nodes AS (
                    SELECT
                        node.id
                    FROM node
                    WHERE node.id = :ID
                    UNION ALL
                    SELECT
                        node.id
                    FROM node
                    INNER JOIN nodes ON nodes.id = node.parent_node_id
                
                )
                SELECT
                    nodes.id,
                    target_effort
                FROM nodes
                INNER JOIN node ON node.id = nodes.id
                INNER JOIN node_version ON node_id = nodes.id
                    AND node_version.version_state_id = 1
                WHERE node.is_leaf = 1
            ";
            $statement = $db->prepare($sql);
            $statement->bindValue(':ID', $item['NODE_ID']);
            $statement->execute();

            $nodeCCArr[$item['NODE_ID']] = $statement->fetchAll();
        }

        return [
            'id' => $nodeIdArr,
            'name' => $nodeNameArr,
            'cc' => $nodeCCArr,
            'sapKey' => $nodeSapKeyArr
        ];
    }

    /**
     *
     * Update the act-hours value in DB
     *
     * @param object $profil
     * @param object $db
     * @param integer $sum
     */
    private static function setActProfileValue($profil, $db, $sum)
    {
        if ($profil->BEZEICHNUNG != '') {
            $sql = "UPDATE bc_chart_profil SET ist = " . $sum . " WHERE id = :PROFIL_ID";
            $statement = $db->prepare($sql);
            $statement->bindValue(':PROFIL_ID', (int)$profil->ID);
            $statement->execute();
        }
    }

    /**
     *
     * Returns the list of group names and employee shortcuts
     *
     * @param unknown_type $profil
     * @param unknown_type $db
     */
    private static function getUserAndGroupNamesOfProfile($profilId, $db)
    {
        $sql = "
			SELECT
			  emp_group.name AS grp,
			  coalesce(empg.shortcut, emp.shortcut) AS employee
			FROM bc_chart_profil_x_employee pxe
			LEFT JOIN employee emp ON emp.id = pxe.employee_id
			LEFT JOIN employee_group emp_group ON emp_group.id = pxe.employee_group_id
			LEFT JOIN employee_x_employee_group emp_x_group ON emp_x_group.employee_group_id = emp_group.id
			LEFT JOIN employee empg ON empg.id = emp_x_group.employee_id
			WHERE bc_chart_profil_id = :PROFIL_ID
			ORDER BY emp_group.name ASC, employee ASC
		";
        $statement = $db->prepare($sql);
        $statement->bindValue(':PROFIL_ID', $profilId);
        $statement->execute();

        $groups = [];
        $employees = [];

        while (false !== ($row = $statement->fetch())) {
            if (!is_null($row['GRP'])) {
                if (!isset($groups[$row['GRP']])) {
                    // add group to array
                    $groups[$row['GRP']] = [
                        'name'        => strtoupper($row['GRP']),
                        'members'    => []
                    ];
                }
                if (!is_null($row['EMPLOYEE'])) {
                    // group has members
                    $groups[$row['GRP']]['members'][] = strtolower($row['EMPLOYEE']);
                }
            } else {
                // employee
                $employees[] = strtolower($row['EMPLOYEE']);
            }
        }
        return [
            'employees' => $employees,
            'groups'    => array_values($groups)
        ];

        $booked = [];

        // convert groups w/ its members to string
        if (sizeof($groups) > 0) {
            foreach ($groups as $i => $group) {
                $groups[$i] = $i;

                $members = implode(', ', $group);
                if (!empty($members)) {
                    $groups[$i] .= ' (' . $members . ')';
                }
            }
            $booked[] = implode(', ', $groups);
        }

        if (!empty($employees)) {
            $booked[] = implode(', ', $employees);
        }

        return !empty($booked) ? implode(' - ', $booked) : null;
    }

    /**
     *
     * Returns the list of group IDs and employee IDs
     *
     * @param unknown_type $profile
     * @param unknown_type $db
     */
    private static function getUserAndGroupIdsOfProfile($profileId, $db)
    {
        $sql = "
			SELECT
			  employee_group_id AS group_id,
			  employee_ID,
			  CASE WHEN  employee_group_id IS NULL THEN  0 ELSE 1 END as isGroup
			FROM bc_chart_profil_x_employee pxe
			WHERE bc_chart_profil_id = :PROFIL_ID
			ORDER BY employee_group_id
		";
        $statement = $db->prepare($sql);
        $statement->bindValue(':PROFIL_ID', $profileId);
        $statement->execute();

        $data = $statement->fetchAll();

        return $data;
    }

    /**
     *
     * Returns the list of group IDs except group all id
     *
     * @param unknown_type $profil
     */
    private static function getGroupIdsOfProfile($profilId)
    {
        $sql = "
			SELECT
			  employee_group_id AS group_id
			FROM bc_chart_profil_x_employee pxe
			WHERE bc_chart_profil_id = :PROFIL_ID AND employee_group_id IS NOT NULL AND employee_group_id != 1 " .
            "ORDER BY employee_group_id
		";

        $statement = Zend_Registry::get('db')->prepare($sql);
        $statement->bindValue(':PROFIL_ID', $profilId);
        $statement->execute();

        return $statement->fetchAll(Zend_Db::FETCH_COLUMN);
    }

    /**
     *
     * Returns the list of employee IDs
     *
     * @param unknown_type $profil
     */
    private static function getEmployeeIdsOfProfile($profilId)
    {
        $sql = "
			SELECT
			  employee_ID
			FROM bc_chart_profil_x_employee pxe
			WHERE bc_chart_profil_id = :PROFIL_ID AND employee_ID IS NOT NULL
			ORDER BY employee_ID
		";

        $statement = Zend_Registry::get('db')->prepare($sql);
        $statement->bindValue(':PROFIL_ID', $profilId);
        $statement->execute();

        return $statement->fetchAll(Zend_Db::FETCH_COLUMN);
    }

    /**
     *
     * Returns the list of nodes and sets of a profile
     *
     * @param int $profilId
     * @param object $db
     */
    public static function getNodesOfProfile($profilId, $db)
    {
        $sql = "
            SELECT
                s.name AS set_name,
                nv.name AS node_name,
                n.node_type_id AS node_type
            FROM bc_chart_profil_x_node chpxn
            LEFT JOIN node n ON chpxn.node_id = n.id
            LEFT JOIN node_version nv
                ON nv.node_id = n.id
                AND nv.version_state_id = 1
            LEFT JOIN nodeset s ON chpxn.set_id = s.id
            WHERE chpxn.bc_chart_profil_id = :PROFIL_ID
            ORDER BY node_name, set_name
        ";

        $statement = $db->prepare($sql);
        $statement->bindValue(':PROFIL_ID', $profilId);
        $statement->execute();
        $rows = $statement->fetchAll();

        $nodes = [];
        foreach ($rows as $item) {
            if (false == is_null($item['SET_NAME'])) {
                array_push($nodes, [
                    'set' => true,
                    'name' => $item['SET_NAME']
                ]);
            } elseif (false == is_null($item['NODE_NAME'])) {
                array_push($nodes, [
                    'set' => false,
                    'name' => $item['NODE_NAME'],
                    'type' => $item['NODE_TYPE']
                ]);
            }
        }

        return $nodes;
    }

    /**
     * Returns all leaf nodes of a profile, optionally pre-formatted as an
     * SQL IN() clause
     *
     * @param integer $profilId
     * @param object $db
     * @param bool $asArray true to return an array instead of SQL IN() clause
     * @return string|array
     */
    private static function getLeafesOfProfile($profilId, $db, $asArray = false)
    {
        // Determine authorized node id(s) of this profile
        $nodeId = Chartprofil::getProfileNodes($profilId, $db);

        // Get the list of relevant nodes (if there is at least one tree node selected)
        if (count($nodeId) > 0) {
            $sql = /** @lang SQL */"
                WITH RECURSIVE nodes AS (
                    SELECT
                        node.id,
                        node.is_leaf
                    FROM node
                    WHERE " . QueryBuilder::createInClause('node.id', $nodeId) . "
                    UNION ALL
                    SELECT
                        node.id,
                        node.is_leaf
                    FROM node
                    INNER JOIN nodes ON nodes.id = node.parent_node_id
                )
                SELECT nodes.id
                FROM nodes
                         INNER JOIN node_version
                                    ON node_version.node_id = nodes.id
                                        AND node_version.version_state_id = 1
                WHERE is_leaf = 1
            ";

            $statement = $db->prepare($sql);
            $statement->execute();
            $rows = $statement->fetchAll(Zend_Db::FETCH_COLUMN);

            if ($asArray) {
                return $rows;
            }

            $nodesIn = QueryBuilder::createInClause('cnode_id', $rows);
            if (!empty($nodesIn)) {
                $nodesIn = " AND $nodesIn ";
            }
        }

        return $nodesIn ?? '';
    }

    /**
     * Return all nodes of a profile with a specific node type
     *
     * @param int $profileId
     * @param Zend_Db_Adapter $db
     * @param int $nodeType
     * @return array
     */
    private static function getProfileNodesPerType($profileId, $db, $nodeType)
    {
        // Determine authorized node id(s) of this profile
        $nodeId = self::getProfileNodes($profileId, $db, false);
        if (empty($nodeId)) {
            return [];
        }

        $sql = "
            WITH RECURSIVE nodes AS (
                SELECT
                    node.id,
                    ARRAY[lower(node_version.name)] AS sort_order,
                    node.node_type_id
                FROM node
                INNER JOIN node_version
                    ON node_version.node_id = node.id
                        AND node_version.version_state_id = 1
                WHERE " . QueryBuilder::createInClause('node.id', $nodeId) . "
                UNION ALL
                SELECT 
                    node.id,
                    nodes.sort_order || lower(node_version.name) AS sort_order,
                    node.node_type_id
                FROM node
                INNER JOIN node_version
                    ON node_version.node_id = node.id
                        AND node_version.version_state_id = 1
                INNER JOIN nodes ON nodes.id = node.parent_node_id
            )
            SELECT
                nodes.id
            FROM nodes
            WHERE nodes.node_type_id = :nodeType
            ORDER BY sort_order
        ";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':nodeType', $nodeType);
        $stmt->execute();
        $rows = $stmt->fetchAll(Zend_Db::FETCH_COLUMN);

        return $rows;
    }

    /**
     * Get all single node selections of a profile, optionally only those of a specific single node type
     *
     * @param int $profileId
     * @param Zend_Db_Adapter $db
     * @param int $singleNodeType
     * @return array
     */
    private static function getSingleNodesOfProfile($profileId, $db, $singleNodeType = null)
    {
        $sql = "
			SELECT
				single_node_id,
				single_node_types
			FROM bc_chart_profil_x_node
			WHERE bc_chart_profil_id = :profileId
			AND single_node_id IS NOT null
		";

        if (!is_null($singleNodeType)) {
            $sql .= " AND single_node_types & " . (int)$singleNodeType . " > 0";
        }

        $stmt = $db->prepare($sql);
        $stmt->bindValue(':profileId', $profileId);
        $stmt->execute();
        $rows = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

        $singleNodeTypes = SingleNodeType::getAll();

        $result = [];
        foreach ($rows as $row) {
            $result['all'][] = $row['SINGLE_NODE_ID'];
            foreach ($singleNodeTypes as $key => $bitValue) {
                if (((int)$row['SINGLE_NODE_TYPES'] & $bitValue) > 0) {
                    $result[$key][] = $row['SINGLE_NODE_ID'];
                }
            }
        }

        return $result;
    }

    /**
     * Find first available previous month data from before specific point in time.
     *
     * @param $employeeId
     * @param $period
     */
    private static function findPreviousMonthData(Zend_Date $period, $employeeId)
    {
        $db = Zend_Registry::get('db');
        $sql = "
			SELECT
				chrscurmonthremain AS remaining_hours,
				cholidayscurmonthremain AS remaining_holiday_days
			FROM tpwmonth tpwm
			INNER JOIN (
				SELECT max(cdate) AS cdate
				FROM tpwmonth
				WHERE cemployee_id = :EMPLOYEE_ID
				AND cdate < to_date(:TF, 'YYYY-MM-DD')
			) previous
				ON tpwm.cdate = previous.cdate
			WHERE tpwm.cemployee_id = :EMPLOYEE_ID
		";

        $statement = $db->prepare($sql);
        $statement->bindValue(':TF', $period->get(Iso_Date::ORACLE_DATE));
        $statement->bindValue(':EMPLOYEE_ID', $employeeId);
        $statement->execute();

        $result = $statement->fetch();

        return empty($result)
            ? [
                'REMAINING_HOURS' => 0,
                'REMAINING_HOLIDAY_DAYS' => 0
            ]
            : $result;
    }

    /**
     * Checks if any employee is selected for a profile
     *
     * @param int $profileId
     * @param array $config
     */
    private static function noEmployeeSelected($profileId, $config = ['companies' => [], 'divisions' => [], 'employees' => []])
    {
        if (ChartProfil::isEmployeeGroupSelected($profileId, 'Alle')) {
            return false;
        }

        return empty($config['companies']) && empty($config['divisions']) && empty($config['employees']);
    }

    public static function loadContingentData($profile, $timestamp, Zend_Db_Adapter_Abstract $db, $session, $translate)
    {
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);
        $from = new DateTime($periodFrom->get(Iso_Date::ORACLE_DATE));
        $to = new DateTime($periodUntil->get(Iso_Date::ORACLE_DATE));
        $report = new Report_ContingentData($db);
        $data = $report->getData($from, $to);
        $session->contingentData[$timestamp] = [
            'REPORT_INFO' => [
                'TYPE' => $profile->CONTINGENT_DATA_REPORT_TYPE,
                'MONTH' => (new Zend_Date())->setTime(0)->get('dd.MM.yyyy')
            ],
            'DATA' => $data,
            'COUNT' => count($data)
        ];
        return ['timestamp' => $timestamp];
    }

    public static function loadContingentSapInvoiceComparison($profile, $timestamp, $db, $session, $translate)
    {
        Date::determinePeriod($profile->ID, $periodFrom, $periodUntil, $db);
        // only considering from-date as report allows only single months
        $from = new DateTime($periodFrom->get(Iso_Date::ORACLE_DATE));
        $report = new Report_ContingentSapInvoiceComparison($db, $from);
        $sameAmounts = $differentAmounts = [];
        $invoiceTotals = $report->getInvoiceTotals();
        foreach ($invoiceTotals as $total) {
            if ($total['KV_TOTAL'] !== $total['SAP_TOTAL']) {
                $differentAmounts[] = $total;
            } else {
                $sameAmounts[] = $total;
            }
        }
        return [
            'DATA' => [
                'divisionTotals' => $report->getDivisionTotals(),
                'confirmationOfExpenseWithoutInvoice' => $report->getConfirmationOfExpensesWithoutInvoice(),
                'differentInvoiceTotals' => $differentAmounts,
                'sameInvoiceTotals' => $sameAmounts
            ],
            'FROM' => $periodFrom->get(Zend_Date::TIMESTAMP),
            'UNTIL' => $periodUntil->get(Zend_Date::TIMESTAMP)
        ];
    }

    /**
     * @param $profile
     * @param $timestamp
     * @param $db
     * @param $session
     * @param $translate
     * @return array
     * @throws Zend_Date_Exception
     */
    public static function loadPrismaValues($profile, $timestamp, $db, $session, $translate): array
    {
        // Determine profile companies
        $companies = self::getProfileCompanies($profile->ID);
        // Determine profile divisions
        $divisions = self::getProfileDivisions($profile->ID);
        // Determine the selected users
        $employees = self::getProfileEmployees($db, $profile->ID);

        $userAndGroupIds = self::getUserAndGroupIdsOfProfile($profile->ID, $db);

        // if resolved employee selection results in no employees at all (e.g. due to empty groups), then report has no data
        if (!empty($userAndGroupIds) && self::noEmployeeSelected($profile->ID, [
                'companies' => $companies,
                'divisions' => $divisions,
                'employees' => $employees['id']
            ])
        ) {
            $employees['id'] = [Employee::DUMMY];
        }
        $employeeSources = [
            'companies' => $companies,
            'divisions' => $divisions,
            'employees' => $employees['id']
        ];
        $report = new Report_PrismaValues($db, $employeeSources, $profile->ID);
        $data = $report->getData();
        $session->prismaValues[$timestamp] = [
            'REPORT_INFO' => [
                'TYPE' => $profile->CONTINGENT_DATA_REPORT_TYPE,
                'MONTH' => (new Zend_Date())->setTime(0)->get('dd.MM.yyyy')
            ],
            'DATA' => $data,
            'COUNT' => count($data)
        ];
        return ['timestamp' => $timestamp];
    }

    /**
     * @return string
     */
    public static function getChartProfileSharedEmployeesSql(): string
    {
        return "SELECT
                    bc_chart_profil_id,
                    m.id AS share_emp_id,
                    bc_chartp_x_shared_emp.mitarbeiter_id AS bc_chartp_mitarbeiter_id
                FROM bc_chartp_x_shared_emp
                INNER JOIN mitarbeiter m ON bc_chartp_x_shared_emp.mitarbeiter_id = m.id
                INNER JOIN employee emp ON lower(m.kuerzel) = lower(emp.shortcut)
                INNER JOIN employee_state ON employee_state.employee_id = emp.id
                INNER JOIN employee_node_permission ON employee_node_permission.employee_id = emp.id
                WHERE m.status = 'aktiv'
                AND employee_state.is_active = 1
                AND m.bkzcockpit_rechte > 0
                AND m.bkzcockpit_rechte & ". Iso_User::RIGHT_TO_MIS_DIVISION_OVERALL ." > 0
                AND employee_node_permission.node_id = 1";
    }

    /**
     * @param $profileId
     * @param array $materialFilter
     * @throws Exception
     */
    public static function modifyChartProfileMaterialFilter($profileId, array $materialFilter)
    {
        $materialFilter = array_unique($materialFilter);
        $chartProfileMaterialFilter = new ChartProfileMaterialFilter();
        $chartProfileMaterialFilter->setmaterialFilter($profileId, $materialFilter);
    }
}
