<?php
class RebookingAction extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'REBOOKING_ACTION';
    protected $_primary = 'ID';
    protected $_sequence = 'SEQ_REBOOKING_ACTION';
    // @codingStandardsIgnoreEnd

    /**
     * Writes a log about rebooking.
     */
    public function log($profile, $requestor, $performer, $reason, $targetCC, $total, $bookings)
    {
        $this->getAdapter()->beginTransaction();
        try {
            $chartProfile = new ChartProfil();
            $select = $chartProfile->select()->where('ID = ?', $profile);
            $profleObj = $this->getAdapter()->fetchRow($select);
            $profleObj['bookingTypes'] = ChartProfil::getBookingTypes($profleObj['ID']);
            
            $bt = new BookingType();
            $bookingTypes = $bt->getBookingTypesAsArray();
            
            $FP = in_array(array_search('FP', $bookingTypes), $profleObj['bookingTypes']) ? 1 : 0;
            $NA = in_array(array_search('NA', $bookingTypes), $profleObj['bookingTypes']) ? 1 : 0;
            $TM = in_array(array_search('TM', $bookingTypes), $profleObj['bookingTypes']) ? 1 : 0;
            
            $chpNode = new ChartProfileNode();
            $nodes = $chpNode->getNodes($profleObj['ID']);
            $nodeId = null;
            foreach ($nodes as $item) {
                if ($item['isSet'] == false && $item['id'] != null) {
                    $nodeId = (int)$item['id'];
                    break;
                }
            }
            
            $data = array(
                'REBOOKING_DATE' => new Zend_Db_Expr("localtimestamp"),
                'REQUESTOR' => $requestor,
                'PERFORMER' => $performer,
                'REASON' => $reason,
                'FROM_DATE' => $profleObj['ZEITRAUM_VON'],
                'END_DATE' => $profleObj['ZEITRAUM_BIS'],
                'RELATIVE_PERIOD' => $profleObj['DYNAMIC_PERIOD'],
                'NODE' => $nodeId,
                'TARGET_CC' => $targetCC,
                'BOOKING_TYPE_FP' => $FP,
                'BOOKING_TYPE_NA' => $NA,
                'BOOKING_TYPE_TM' => $TM,
                'NOTE' => $profleObj['NOTIZTEXT'],
                'SUM_OF_MODIFIED_TOTALS' => $total
            );
            
            $raId = $this->insert($data);
            
            $raxb = new RebookingActionXBooking();
            foreach ($bookings as $item) {
                $data = array(
                    'REBOOKING_ACTION_ID' => $raId,
                    'BOOKING_ID'    => $item['bookingId'],
                    'PREVIOUS_CC_ID' => $item['previousCC'],
                    'PREVIOUS_BT_ID' => $item['previousBT'],
                    'EMPLOYEE_ID'    => $item['employeeId'],
                    'NODE_ID'        => $item['nodeId'],
                    'BOOKING_DATE'    => $item['bookingDate'],
                    'HOURS'        => $item['hours'],
                    'HOURSAL'      => $item['hoursAL'],
                    'NOTE'         => $item['note'],
                    'NOTEAL'       => $item['noteAL'],
                    'OLD_NOTE'     => $item['oldNote'],
                    'BT_ID'        => $item['btId']
                );
                $raxb->insert($data);
            }
        } catch (Zend_Db_Exception $e) {
            $this->getAdapter()->rollback();
            return false;
        }
        
        $this->getAdapter()->commit();
        return true;
    }
}
