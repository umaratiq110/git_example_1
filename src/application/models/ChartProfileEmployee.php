<?php
class ChartProfileEmployee extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'BC_CHART_PROFIL_X_EMPLOYEE';
    // @codingStandardsIgnoreEnd
    
    public function getEmployees($profileId)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, '*')
            ->where('BC_CHART_PROFIL_ID = ?', $profileId);
        
        $result = array();
        $queryResult = $select->query()->fetchAll();
        foreach ($queryResult as $item) {
            array_push($result, self::createEmployee($item));
        }
        
        return $result;
    }

    public function setEmployees($profileId, $employees = array())
    {
        $this->getAdapter()->beginTransaction();
        try {
            // Remove all employees for a profil
            $this->getAdapter()->delete($this->_name, $this->getAdapter()
                ->quoteInto('BC_CHART_PROFIL_ID = ?', $profileId));
                            
            //Add employees to a profile
            foreach ($employees as $item) {
                if ((bool)$item->isGroup) {
                    $data = array(
                        'BC_CHART_PROFIL_ID' => $profileId,
                        'EMPLOYEE_GROUP_ID' => (int)$item->id,
                        'EMPLOYEE_ID' => null
                    );
                } else {
                    $data = array(
                        'BC_CHART_PROFIL_ID' => $profileId,
                        'EMPLOYEE_GROUP_ID' => null,
                        'EMPLOYEE_ID' => (int)$item->id
                    );
                }
                
                $this->getAdapter()->insert($this->_name, $data);
            }
        } catch (Exception $e) {
            $this->getAdapter()->rollback();
            return false;
        }
        $this->getAdapter()->commit();
        return true;
    }
    
    public function copy($srcProfileId, $destProfileId)
    {
        $this->getAdapter()->beginTransaction();
        try {
            // Remove all nodes for a profil
            $this->getAdapter()->delete($this->_name, $this->getAdapter()
                ->quoteInto('BC_CHART_PROFIL_ID = ?', $destProfileId));
                            
            $sql = 'INSERT INTO BC_CHART_PROFIL_X_EMPLOYEE (BC_CHART_PROFIL_ID, EMPLOYEE_ID, EMPLOYEE_GROUP_ID) ' .
                    'SELECT :destProfileId, EMPLOYEE_ID, EMPLOYEE_GROUP_ID from BC_CHART_PROFIL_X_EMPLOYEE
                    where BC_CHART_PROFIL_ID = :srcProfileId';
                    
            $statment = $this->getAdapter()->prepare($sql);
            $statment->bindValue(':destProfileId', $destProfileId);
            $statment->bindValue(':srcProfileId', $srcProfileId);
            $statment->execute();
        } catch (Exception $e) {
            $this->getAdapter()->rollback();
            return false;
        }
        $this->getAdapter()->commit();
        return true;
    }
    
    public static function createEmployee($item)
    {
        if (!is_null($item['EMPLOYEE_GROUP_ID'])) {
            return array(
                'uid'        => 'g' . $item['EMPLOYEE_GROUP_ID'],
                'id'        => $item['EMPLOYEE_GROUP_ID'],
                'name'        => $item['EMPLOYEE_GROUP_NAME'],
                'checked'    => true,
                'isGroup'    => true
            );
        } elseif (!is_null($item['EMPLOYEE_ID'])) {
            return array(
                'uid'                => 'e' . $item['EMPLOYEE_ID'],
                'id'                => $item['EMPLOYEE_ID'],
                'shortcut'            => $item['EMPLOYEE_SHORTCUT'],
                'first_name'        => $item['EMPLOYEE_FIRST_NAME'],
                'last_name'            => $item['EMPLOYEE_LAST_NAME'],
                'checked'            => true,
                'isGroup'            => false
            );
        }
        return false;
    }
}
