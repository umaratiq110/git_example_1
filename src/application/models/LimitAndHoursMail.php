<?php
declare(strict_types=1);

class LimitAndHoursMail
{
    const TYPE_ACTUAL_HOURS = 1;
    const TYPE_LIMIT_EXCEEDED = 2;

    /**
     * @var Zend_Db_Adapter_Abstract
     */
    private $db;

    /**
     * @var Zend_Translate_Adapter[]
     */
    private $translators;

    /**
     * @var NumberFormatter[]
     */
    private $numberFormatters = [];

    /**
     * @var Zend_Log
     */
    private $log;

    /**
     * LimitAndHoursMail constructor.
     */
    public function __construct()
    {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $this->db = $bootstrap->getResource('db');
        $this->log = $bootstrap->getResource('log');
    }

    /**
     * @throws Zend_Db_Statement_Exception
     * @throws Exception
     */
    public function sendMails()
    {
        $this->log->info(sprintf('starting sending limit mails, environment: %s', APPLICATION_ENV));
        $employeeProfiles = $this->getEmployeesAndProfiles();

        $this->log->info(sprintf('found %d employee-profiles', count($employeeProfiles)));
        foreach ($employeeProfiles as $employeeId => $data) {
            $mailTypes = [
                self::TYPE_ACTUAL_HOURS => [],
                self::TYPE_LIMIT_EXCEEDED => [],
            ];
            $locale = $data['locale'];
            foreach ($data['profiles'] as $profile) {
                $mailType = $this->determineMailType($profile);
                if ($mailType === null) {
                    # no mail for this profile
                    continue;
                }
                $mailTypes[$mailType][$profile['id']] = $profile;
            }
            $this->log->info(sprintf('sending mail for employee: %s', $data['email']));
            $this->sendMailsForEmployee($data['email'], $mailTypes, $locale);
        }

        $this->db->query('CALL PACK_BKZCOCKPIT.calculateChartProfiles()');
    }

    /**
     * @param int $profileId
     * @param $from
     * @param $to
     */
    private function getProfilePeriod(int $profileId, &$from, &$to)
    {
        Date::determinePeriod($profileId, $zendFrom, $zendTo, $this->db);

        if ($zendFrom !== null) {
            $from = DateTime::createFromFormat('U', $zendFrom->get());
        }
        else {
            $from  = new DateTime();
        }
        if ($zendTo !== null) {
            $to = DateTime::createFromFormat('U', $zendTo->get());
        }
        else {
            $to = new DateTime();
        }
    }

    /**
     * @return array
     * @throws Zend_Db_Statement_Exception
     */
    private function getEmployeesAndProfiles(): array
    {
        $data = [];
        $sql = "SELECT
            BC_CHART_PROFIL.ID AS id,
            BC_CHART_PROFIL.ZEITRAUM_VON,
            BC_CHART_PROFIL.ZEITRAUM_BIS,
            BC_CHART_PROFIL.MAILTYPE,
            BC_CHART_PROFIL.MITARBEITER_ID,
            BC_CHART_PROFIL.LIMIT,
            BC_CHART_PROFIL.BEZEICHNUNG,
            MITARBEITER.EMAIL,
            MITARBEITER.SPRACHE,
            MITARBEITER.NACHNAME
            FROM bc_chart_profil
            JOIN mitarbeiter on BC_CHART_PROFIL.MITARBEITER_ID = MITARBEITER.ID
            -- profile and employee must be active
            WHERE bc_chart_profil.aktiv = 'J'
            AND mitarbeiter.status = 'aktiv'
            -- manually created profiles only
            AND bc_chart_profil.bezeichnung is not null
            -- consider only profiles with limit/hours mail
            AND bc_chart_profil.mailtype IN (:typeHours,:typeLimit)
            -- exclude list reports with remote work data since hours or limit are not relevant for it            
            AND NOT(anzeige = 'L' AND only_remote_work_days = 1)
            ORDER BY MITARBEITER_ID
             ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':typeHours', self::TYPE_ACTUAL_HOURS);
        $stmt->bindValue(':typeLimit', self::TYPE_LIMIT_EXCEEDED);
        $stmt->execute();
        $result = $stmt->fetchAll();
        foreach ($result as $row) {
            $profileId = (int)$row['ID'];
            $employeeId = (int)$row['MITARBEITER_ID'];
            $mailType = (int)$row['MAILTYPE'];

            if (!isset($data[$employeeId])) {
                $data[$employeeId] = [
                    'id' => $employeeId,
                    'profiles' => [],
                    'email' => $row['EMAIL'],
                    'locale' => $row['SPRACHE'],
                    'name' => $row['NACHNAME']
                ];
            }
            $this->getProfilePeriod($profileId, $from, $to);
            $data[$employeeId]['profiles'][$profileId] = [
                'id' => $profileId,
                'name' => $row['BEZEICHNUNG'],
                'periodFrom' => $from,
                'periodTo' => $to,
                'limit' => (int)$row['LIMIT'],
                'mailType' => $mailType,
                # as the values in the DB are not trustable (???), we need to re-calculate the hours
                'actualHours' => $this->calculateHoursForPeriod($profileId, $from, $to)
            ];
        }
        return $data;
    }

    /**
     * @param int $profileId
     * @param DateTime $from
     * @param DateTime $to
     * @return float
     * @throws Zend_Db_Statement_Exception
     */
    private function calculateHoursForPeriod(int $profileId, DateTime $from, DateTime $to): float
    {
        $sql = "
            SELECT 
                pack_bkzcockpit.calculateActHoursOfProfile(:profileId, to_date(:dateFrom, 'YYYY-MM-DD'), to_date(:dateTo, 'YYYY-MM-DD')) AS hours
        ";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':profileId', $profileId);
        $stmt->bindValue(':dateFrom', $from->format('Y-m-d'));
        $stmt->bindValue(':dateTo', $to->format('Y-m-d'));
        $stmt->execute();
        $actualSum = $stmt->fetchColumn();

        return (float)$actualSum;
    }

    /**
     * @param int $profileId
     * @return bool
     * @throws Zend_Db_Statement_Exception
     */
    private function isValid(int $profileId): bool
    {
        $sql = "
            SELECT 
                pack_bkzcockpit.isChartProfileValidWrapper(:profileId) as is_valid
        ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':profileId', $profileId);
        $stmt->execute();
        $isValid = $stmt->fetchColumn();

        return (bool)$isValid;
    }

    /**
     * @param int $mailType
     * @param string $locale
     * @return string
     * @throws Zend_Translate_Exception
     */
    private function getSubject(int $mailType, string $locale): string
    {
        $now = new DateTime();
        $subject = sprintf(
            '%s %s - %s',
            $this->translate('bkzcockpit', $locale),
            $this->translate('report', $locale),
            $now->format('d.M.Y H:i')
        );
        if ($mailType === self::TYPE_LIMIT_EXCEEDED) {
            $subject = sprintf(
                '%s - %s %s!',
                $subject,
                strtoupper($this->translate('limit', $locale)),
                strtoupper($this->translate('ueberschritten', $locale))
            );
        }
        return $subject;
    }

    /**
     * @param $locale
     * @return string
     * @throws Zend_Translate_Exception
     */
    private function getMailHeader($locale): string
    {
        $header = sprintf(
            "%s %s - %s",
            $this->translate('bkzcockpit', $locale),
            $this->translate('report', $locale),
            (new DateTime())->format('d.M.Y')
        );
        $header = sprintf(
            "%s\n%s",
            $header,
            str_pad('', strlen($header), '=')
        );
        $header = sprintf(
            "%s\n%s\n%s\n%s\n%s\n\n",
            $header,
            $this->translate('hinweis', $locale),
            $this->translate('zahlenbeizeichnenstunden', $locale),
            $this->translate('zahlohneklammern', $locale),
            $this->translate('zahlinklammern', $locale)
        );

        return $header;
    }

    /**
     * @param array $profileData
     * @param string $locale
     * @return string
     * @throws Zend_Db_Statement_Exception
     * @throws Exception
     */
    private function getProfileText(array $profileData, string $locale): string
    {
        if (!$this->isValid($profileData['id'])) {
            return $this->getInvalidProfileMailText($profileData['name'], $locale);
        }
        if ($this->isProfileOutDated($profileData['periodTo'] ?? new DateTime())) {
            return $this->getOutdatedMailText($locale);
        }
        $lastWorkday = $this->getLastWorkDay();
        $hoursForLastWorkday = $this->calculateHoursForPeriod($profileData['id'], $lastWorkday, $lastWorkday);
        $limitExceededText = null;
        if ($this->isLimitExceeded($profileData)) {
            $limitExceededText = sprintf(
                " --- %s %s",
                $this->translate('limit', $locale),
                $this->translate('ueberschritten', $locale)
            );
        }
        $limitText = null;
        if ($profileData['limit'] > 0) {
            $limitText = sprintf(
                '%s: %d',
                $this->translate('limit', $locale),
                $profileData['limit']
            );
        }
        return sprintf(
            "%s: %s %s\n%s: %s - %s\n%s: %s (%s) %s\n------------------------------------------------------------\n",
            $this->translate('chartprofil', $locale),
            $profileData['name'],
            $limitExceededText,
            $this->translate('zeitraumal', $locale),
            $profileData['periodFrom']->format('d.M.Y'),
            $profileData['periodTo']->format('d.M.Y'),
            $this->translate('iststunden', $locale),
            $this->formatNumber($profileData['actualHours'], $locale),
            $this->formatNumber($hoursForLastWorkday, $locale),
            $limitText
        );
    }

    /**
     * @param $locale
     * @return Zend_Translate_Adapter
     * @throws Zend_Translate_Exception
     */
    private function getTranslator($locale): Zend_Translate_Adapter
    {
        # it is necessary to create a separate translator for every locale (ZF nonsense)
        if (!isset($this->translator[$locale])) {
            $t = new Iso_Translate_DbTranslate('database', 'bkzcockpitmail.java', $locale);
            $this->translators[$locale] = $t->getAdapter();
        }
        return $this->translators[$locale];
    }

    /**
     * @param string $identifier
     * @param string $locale
     * @return string
     * @throws Zend_Translate_Exception
     */
    private function translate(string $identifier, string $locale): string
    {
        return $this->getTranslator($locale)->translate($identifier);
    }

    /**
     * @param DateTime|null $date
     * @param int $monthsAgo
     * @return bool
     * @throws Exception
     */
    private function isDateBefore(DateTime $date = null, int $monthsAgo = 2): bool
    {
        if ($date === null) {
            return false;
        }

        $checkDate = new DateTime(sprintf('-%d month', $monthsAgo));

        return $date < $checkDate;
    }

    /**
     * @param array $profile
     * @return bool
     * @throws Exception
     */
    private function isLimitExceeded(array $profile): bool
    {
        $periodTo = $profile['periodTo'];
        if ($periodTo === null) {
            return false;
        }

        # not relevant for profiles regarding periods more than 2 months ago
        if ($this->isDateBefore($periodTo)) {
            return false;
        }
        $limit = $profile['limit'];
        $actualHours = $profile['actualHours'];
        return $limit > 0 && $actualHours > $limit;
    }

    /**
     * @param string $name
     * @param string $locale
     * @return string
     * @throws Zend_Translate_Exception
     */
    private function getInvalidProfileMailText(string $name, string $locale): string
    {
        return sprintf(
            "\n%s: %s\n%s\n------------------------------------------------------------\n",
            $this->translate('chartprofil', $locale),
            $name,
            $this->translate('reportValidatorDataInvalid', $locale)
        );
    }

    /**
     * @param string $locale
     * @return string
     * @throws Zend_Translate_Exception
     */
    private function getOutdatedMailText(string $locale): string
    {
        return sprintf(
            '%s',
            $this->translate('zeitraumaelteralsvormonat', $locale)
        );
    }

    private function isProfileOutDated(DateTime $periodEnd): bool
    {
        return $periodEnd < new DateTime('2012-01-01');
    }

    /**
     * @param array $profileData
     * @return int|null
     * @throws Exception
     */
    private function determineMailType(array $profileData)
    {
        $limitIsExceeded = $this->isLimitExceeded($profileData);
        # ActualHours mail only when limit is not exceeded
        if ($profileData['mailType'] === self::TYPE_ACTUAL_HOURS && !$limitIsExceeded) {
            return self::TYPE_ACTUAL_HOURS;
        }

        if ($limitIsExceeded) {
            return self::TYPE_LIMIT_EXCEEDED;
        }
        # when limit exceeded mail is configured but limit is not reached yet
        return null;
    }

    /**
     * Returns the last workday (mon-fri, holidays are not considered; Saturday seems not to be a general workday in ISO...)
     * @return DateTime
     */
    private function getLastWorkDay(): DateTime
    {
        $date = new DateTime();
        $day = $date->format('N');
        switch ($day) {
            # monday
            case 1:
                return $date->modify('-3 day');
            #sunday
            case 7:
                return $date->modify('-2 day');
            default:
                return $date->modify('-1 day');
        }
    }

    /**
     * @param $number
     * @param $locale
     * @return false|string
     */
    private function formatNumber($number, $locale)
    {
        return $this->getNumberFormatter($locale)->format($number);
    }

    /**
     * @param $locale
     * @return NumberFormatter
     */
    private function getNumberFormatter($locale): NumberFormatter
    {
        if (!isset($this->numberFormatters[$locale])) {
            $f = new NumberFormatter($locale, NumberFormatter::DECIMAL);
            $f->setAttribute(NumberFormatter::MIN_FRACTION_DIGITS, 2);
            $f->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, 2);
            $this->numberFormatters[$locale] = $f;
        }
        return $this->numberFormatters[$locale];
    }

    /**
     * @param string $recipient
     * @param string $subject
     * @param string $body
     * @throws Zend_Exception
     * @throws Zend_Mail_Exception
     */
    private function sendMail(string $recipient, string $subject, string $body)
    {
        $mail = new Iso_Mail('utf-8');
        $mail->addTo($recipient)
            ->setSubject($subject)
            ->setBodyText($body);

        $mail->send();
    }

    /**
     * @param string $recipient
     * @param array $mailsByType
     * @param string $locale
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     * @throws Zend_Mail_Exception
     * @throws Zend_Translate_Exception
     */
    private function sendMailsForEmployee(string $recipient, array $mailsByType, string $locale)
    {
        foreach ($mailsByType as $mailType => $profiles) {
            if (empty($profiles)) {
                continue;
            }
            $subject = $this->getSubject($mailType, $locale);
            $mailBody = $this->getMailHeader($locale);
            foreach ($profiles as $profileId => $profile) {
                $mailBody .= $this->getProfileText($profile, $locale);
            }
            $this->sendMail($recipient, $subject, $mailBody);
        }
    }
}
