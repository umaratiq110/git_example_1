<?php
class Firma extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    /**
     * Table name
     * @var string
     */
    protected $_name = 'FIRMA';

       /**
        * Primary key
        * @var string
        */
    protected $_primary = 'ID';
    // @codingStandardsIgnoreEnd
    
    public function getCompanyId($company)
    {
        $select = $this->select()
                       ->where('KUERZEL = ?', $company);
                       
        $row   = $this->fetchRow($select);

        return $row['ID'];
    }
}
