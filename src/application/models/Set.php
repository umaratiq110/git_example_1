<?php
/**
 * Set model
 */
class Set extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'NODESET';
    protected $_primary = 'ID';
    // @codingStandardsIgnoreEnd
    protected $translate;
    
    protected static $setCls = 'set';
    
    /**
     * Gets icon css class
     */
    public static function getIconCls(): string
    {
        return self::$setCls;
    }
    
    /**
     * Model constructor
     */
    public function __construct($translate = null)
    {
        parent::__construct();

        /** @var Zend_Db_Adapter_Abstract _db */
        $this->_db = Zend_Registry::get('db');
        $this->translate = $translate;
    }

    /**
     * Get sets as tree
     *
     * @param int $empId
     * @return array
     * @throws Zend_Db_Statement_Exception
     */
    public function getTree(int $empId): array
    {
        $sql = "
            SELECT
                s.id              AS id,
                s.name            AS name,
                s.description     AS description,
                node.id           AS node_id,
                node_version.name AS node_name,
                node.node_type_id AS node_type_id
            FROM nodeset s
            INNER JOIN employee_set_permission esp ON esp.set_id = s.id
            LEFT JOIN nodeset_x_node nxn ON nxn.set_id = s.id
            LEFT JOIN node ON node.id = nxn.node_id
            LEFT JOIN node_version
                ON node_version.node_id = node.id
                AND node_version.version_state_id = 1
            WHERE esp.employee_id = :employeeId
            ORDER BY s.name, node_version.name
        ";

        $stmt = $this->getAdapter()->prepare($sql);
        $stmt->bindParam(':employeeId', $empId);
        $stmt->execute();

        $result = array();
        while (false !== ($row = $stmt->fetch(Zend_Db::FETCH_ASSOC))) {
            if (!isset($result[$row['ID']])) {
                $result[$row['ID']] = array(
                    // ExtJS specific properties
                    'uid'            => 's' . $row['ID'],
                    'text'            => $row['NAME'],
                    'leaf'            => true,
                    'iconCls'        => self::$setCls,
                    'checked'        => false,
                    // other properties
                    'id'            => $row['ID'],
                    'name'            => $row['NAME'],
                    'description'    => $row['DESCRIPTION'],
                    'isSet'            => true
                );
            }
            // add set nodes if there are any
            if (isset($row['NODE_ID'])) {
                $result[$row['ID']]['leaf'] = false;
                $result[$row['ID']]['children'][] = array(
                    // ExtJS specific properties
                    'uid'            => 's' . $row['ID'] . 'n' . $row['NODE_ID'],
                    'text'            => $row['NODE_NAME'],
                    'leaf'            => true,
                    'iconCls'        => Node::getIconCls((int)$row['NODE_TYPE_ID']),
                    'checked'        => null,
                    // other properties
                    'id'        => $row['NODE_ID'],
                    'name'        => $row['NODE_NAME'],
                    'isSet'        => false
                );
            }
        }
        
        return array_values($result);
    }

    /**
     * @param $name
     * @return array
     * @throws Zend_Db_Statement_Exception
     */
    public function getNodesBySetName($name): array
    {
        $sql = 'SELECT n.NODE_ID
            FROM NODESET 
            LEFT JOIN NODESET_X_NODE n
                ON n.SET_ID = NODESET.id
            WHERE NODESET.NAME = :name';

        $stmt = $this->getAdapter()->prepare($sql);
        $stmt->bindParam('name', $name);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_COLUMN);
    }
}
