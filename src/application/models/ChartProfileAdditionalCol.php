<?php
class ChartProfileAdditionalCol extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'BC_CHARTP_X_ADDITIONAL_COL';
    // @codingStandardsIgnoreEnd

    public function getAdditionalCols($profileId)
    {
        $select = $this->getAdapter()->select()
            ->from(array($this->_name), array())
            ->join(
                'bc_additional_col',
                'bc_additional_col.id = bc_chartp_x_additional_col.bc_additional_col_id',
                array('id')
            )
            ->join('bc_report_type', 'bc_report_type.id = bc_additional_col.report_type_id', array())
            ->join('bc_chart_profil', 'bc_chart_profil.id = bc_chartp_x_additional_col.bc_chart_profil_id', array())
            ->where('bc_chartp_x_additional_col.bc_chart_profil_id = ?', $profileId)
            ->where('bc_chart_profil.anzeige = bc_report_type.shortcut')
            ->order('bc_additional_col.sort_order');

        $stmt = $select->query();
        
        $result = array();
        while (false !== ($col = $stmt->fetch(Zend_Db::FETCH_COLUMN))) {
            $result[] = (int)$col;
        }

        return $result;
    }
    
    public function setAdditionalCols($profileId, $cols = array())
    {
        $this->getAdapter()->beginTransaction();
        try {
            // Remove all nodes for a profil
            $this->getAdapter()->delete($this->_name, $this->getAdapter()
                ->quoteInto('BC_CHART_PROFIL_ID = ?', $profileId));
            
            //Add nodes to a profile
            foreach ($cols as $item) {
                $data = array(
                    'BC_CHART_PROFIL_ID' => $profileId,
                    'BC_ADDITIONAL_COL_ID' => (int)$item
                );
                
                $this->getAdapter()->insert($this->_name, $data);
            }
        } catch (Exception $e) {
            $this->getAdapter()->rollback();
            return false;
        }
        $this->getAdapter()->commit();
        return true;
    }

    public function copy($srcProfileId, $destProfileId)
    {
        $this->getAdapter()->beginTransaction();
        try {
            // Remove all nodes for a profil
            $this->getAdapter()->delete($this->_name, $this->getAdapter()
                ->quoteInto('bc_chart_profil_id = ?', $destProfileId));

            $sql = "
                INSERT INTO bc_chartp_x_additional_col (
                    bc_chart_profil_id,
                    bc_additional_col_id
                )
                SELECT
                    :destProfileId,
                    bc_additional_col_id
                FROM bc_chartp_x_additional_col
                WHERE bc_chart_profil_id = :srcProfileId
            ";

            $statment = $this->getAdapter()->prepare($sql);
            $statment->bindValue(':destProfileId', $destProfileId);
            $statment->bindValue(':srcProfileId', $srcProfileId);
            
            $statment->execute();
        } catch (Exception $e) {
            $this->getAdapter()->rollback();
            return false;
        }
        $this->getAdapter()->commit();
        return true;
    }
}
