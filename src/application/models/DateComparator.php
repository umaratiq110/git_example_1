<?php
class DateComparator
{
    private $startDate1;
    private $endDate1;
    private $startDate2;
    private $endDate2;

    public function __construct()
    {
        $this->startDate1 = new Zend_Date();
        $this->startDate2 = new Zend_Date();
        $this->endDate1 = new Zend_Date();
        $this->endDate2 = new Zend_Date();
    }

    public function isOverlapping($period1Start, $period1End, $period2Start, $period2End, $format = "dd.MM.yyyy")
    {
        $this->startDate1->set($period1Start, $format);
        $this->startDate2->set($period2Start, $format);
        $this->endDate1->set($period1End, $format);
        $this->endDate2->set($period2End, $format);

        return (($this->startDate1->isEarlier($this->endDate2) || $this->startDate1->equals($this->endDate2))
            &&  ($this->endDate1->isLater($this->startDate2) || $this->endDate1->equals($this->startDate2))
        );
    }
}
