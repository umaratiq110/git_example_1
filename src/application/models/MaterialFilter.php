<?php

/**
 *
 */
class MaterialFilter extends Iso_Model_IsoModel
{
    /**
     * @var string
     */
    protected $_name = 'bc_order_material_filter';

    /**
     * @var string
     */
    protected $_primary = 'id';

    /**
     * @throws Zend_Exception
     */
    public function __construct()
    {
        parent::__construct();
        /** @var Zend_Db_Adapter_Abstract _db */
        $this->_db = Zend_Registry::get('db');
    }

    /**
     * @param string $language
     * @return array
     */
    public function getMaterialFilter(string $language): array
    {
        $sql = "
            SELECT
                id,
                name_" . $language . " AS name
            FROM bc_order_material_filter
            ORDER BY sort_order
        ";

        /** @var PDOStatement $stmt */
        $stmt = $this->_db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public function getMaterialFilterValues(array $ids = [])
    {
        $select = $this->_db->select()
            ->from($this->_name, ['filter_include', 'filter_exclude']);
        if (!empty($ids)) {
            $select->where($this->_primary . ' IN (' . implode(',', $ids) . ')');
        }

        return $select->query()->fetchAll();
    }
}