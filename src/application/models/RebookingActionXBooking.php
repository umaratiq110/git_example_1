<?php
class RebookingActionXBooking extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'REBOOKING_ACTION_X_BOOKING';
    protected $_primary = 'ID';
    protected $_sequence = 'SEQ_REBOOKING_ACTION_X_BOOKING';
    // @codingStandardsIgnoreEnd
}
