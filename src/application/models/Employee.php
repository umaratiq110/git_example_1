<?php
/**
 * Employee model
 */
class Employee extends Iso_Model_IsoModel
{
    const DUMMY = -1;

    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'EMPLOYEE';
    protected $_primary = 'ID';
    // @codingStandardsIgnoreEnd
    
    protected static $activeEmployeeCls = 'user-green';
    protected static $inactiveEmployeeCls = 'user-gray';
    
    /**
     * Gets icon css class
     *
     * @param int $active
     */
    public static function getIconCls($active)
    {
        return $active ? self::$activeEmployeeCls : self::$inactiveEmployeeCls;
    }
    
    /**
     * Model constructor
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->_db = Zend_Registry::get('db');
    }

    /**
     * Gets all active employees
     *
     * @return array
     * @throws Zend_Db_Statement_Exception
     */
    public function getAllEmployees()
    {
        $sql = "
			SELECT
				employee.id,
				lower(employee.shortcut) ||' - '|| employee.last_name || ' ' || employee.first_name AS name
			FROM employee
			INNER JOIN employment_period ON employment_period.employee_id = employee.id
			WHERE current_date
			BETWEEN employment_period.entry_date AND coalesce(employment_period.leaving_date, max_date())
			ORDER BY employee.shortcut
		";

        $stmt = $this->getAdapter()->query($sql);
        return $stmt->fetchAll(Zend_Db::FETCH_ASSOC);
    }
    
    /**
     * Get employees as tree
     *
     * @param int $empId
     * @return array
     */
    public function getTree($empId)
    {
        $sql = "
            SELECT
                employee.id AS employee_id,
                employee.shortcut AS shortcut,
                employee.last_name AS last_name,
                employee.first_name AS first_name,
                employee_state.is_active AS active
            FROM all_employee_permission
            INNER JOIN employee ON employee.id = all_employee_permission.assigned_employee_id
            INNER JOIN employee_state ON employee_state.employee_id = employee.id
            WHERE all_employee_permission.privileged_employee_id = :employeeId
            ORDER BY employee.last_name
        ";
        
        $stmt = $this->getAdapter()->prepare($sql);
        $stmt->bindParam(':employeeId', $empId);
        $stmt->execute();
        
        $result = array(array(),array());
        while (false !== ($row = $stmt->fetch(Zend_Db::FETCH_ASSOC))) {
            $active = (int)$row['ACTIVE'];
            $result[1 - $active][] = array(
                // ExtJS specific properties
                'uid'            => 'e' . $row['EMPLOYEE_ID'],
                'text'            => $row['LAST_NAME'] . ', ' . $row['FIRST_NAME'] . ' (' . $row['SHORTCUT'] . ')',
                'leaf'            => true,
                'iconCls'        => self::getIconCls((bool)$active),
                'checked'        => false,
                // other properties
                'id'            => $row['EMPLOYEE_ID'],
                'shortcut'        => $row['SHORTCUT'],
                'first_name'    => $row['FIRST_NAME'],
                'last_name'        => $row['LAST_NAME'],
                'locked'        => !(bool)$active,
                'isGroup'        => false
            );
        }
        
        return $result;
    }
}
