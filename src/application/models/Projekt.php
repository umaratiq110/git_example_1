<?php
class Projekt extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    /**
     * Table name
     * @var string
     */
    protected $_name = 'PROJEKT';

       /**
        * Primary key
        * @var string
        */
    protected $_primary = 'ID';
    // @codingStandardsIgnoreEnd
}
