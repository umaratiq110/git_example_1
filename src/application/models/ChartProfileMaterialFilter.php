<?php

class ChartProfileMaterialFilter extends Iso_Model_IsoModel
{
    /**
     * @var string
     */
    protected $_name = 'bc_chartp_x_mat_filter';

    /**
     * @param int $profileId
     * @return array
     */
    public function getMaterialFilter(int $profileId): array
    {
        $sql = "
            SELECT
                bc_chart_profil_id,
                material_filter_id,
                name_de,
                name_en
            FROM BC_CHARTP_X_MAT_FILTER
            INNER JOIN BC_ORDER_MATERIAL_FILTER
                ON BC_CHARTP_X_MAT_FILTER.MATERIAL_FILTER_ID = BC_ORDER_MATERIAL_FILTER.ID
            WHERE BC_CHART_PROFIL_ID = :profileId
        ";


        return $this->_db->fetchAll($sql, ['profileId' => $profileId]);
    }

    /**
     * @param int $profileId
     * @param array $types
     * @return bool
     * @throws Zend_Db_Adapter_Exception
     */
    public function setmaterialFilter(int $profileId, array $types = []): bool
    {
        $this->getAdapter()->beginTransaction();
        try {
            $this->getAdapter()->delete(
                $this->_name,
                $this->getAdapter()
                    ->quoteInto('bc_chart_profil_id = ?', $profileId)
            );
            foreach ($types as $typeId) {
                $data = [
                    'bc_chart_profil_id' => $profileId,
                    'material_filter_id' => $typeId
                ];

                $this->getAdapter()->insert($this->_name, $data);
            }
        } catch (Exception $e) {
            $this->getAdapter()->rollback();
            throw $e;
        }
        $this->getAdapter()->commit();
        return true;
    }
}