<?php
/**
 * Employee group model
 */
class EmployeeGroup extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'EMPLOYEE_GROUP';
    protected $_primary = 'ID';
    // @codingStandardsIgnoreEnd
    
    protected static $groupCls = 'group';
    
    /**
     * Gets icon css class
     */
    public static function getIconCls()
    {
        return self::$groupCls;
    }
    
    /**
     * Model constructor
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->_db = Zend_Registry::get('db');
    }
    
    /**
     * Get employee groups as tree
     *
     * @param int $empId
     * @return array
     */
    public function getTree($empId)
    {
        $sql = "
            SELECT
                eg.name AS name,
                eg.id AS id,
                emp.id AS employee_id,
                emp.last_name AS last_name,
                emp.first_name AS first_name,
                emp.shortcut AS shortcut,
                eg.description AS description,
                employee_state.is_active AS active
            FROM employee_group_permission egp
            INNER JOIN employee_group eg ON eg.id = egp.employee_group_id
            LEFT JOIN employee_x_employee_group eeg ON eeg.employee_group_id = eg.id
            LEFT JOIN employee emp ON emp.id = eeg.employee_id
            LEFT JOIN employee_state ON employee_state.employee_id = emp.id
            WHERE egp.employee_id = :employeeId
            ORDER BY eg.name, emp.last_name, emp.first_name
        ";

        $stmt = $this->getAdapter()->prepare($sql);
        $stmt->bindParam(':employeeId', $empId);
        $stmt->execute();

        $result = [];
        while (false !== ($row = $stmt->fetch(Zend_Db::FETCH_ASSOC))) {
            if (!isset($result[$row['ID']])) {
                $result[$row['ID']] = [
                    // ExtJS specific properties
                    'uid'           => 'g' . $row['ID'],
                    'text'          => $row['NAME'],
                    'leaf'          => true,
                    'iconCls'       => self::$groupCls,
                    'checked'       => false,
                    // other properties
                    'id'            => $row['ID'],
                    'name'          => $row['NAME'],
                    'description'   => $row['DESCRIPTION'],
                    'isGroup'       => true
                ];
            }
            // add group employees if there are any
            if (isset($row['EMPLOYEE_ID'])) {
                $result[$row['ID']]['leaf'] = false;
                $result[$row['ID']]['children'][] = [
                    // ExtJS specific properties
                    'uid'           => 'g' . $row['ID'] . 'e' . $row['EMPLOYEE_ID'],
                    'text'          => $row['LAST_NAME'] . ', ' . $row['FIRST_NAME'],
                    'leaf'          => true,
                    'iconCls'       => Employee::getIconCls((bool)(int)$row['ACTIVE']),
                    'locked'        => !(bool)(int)$row['ACTIVE'],
                    'checked'       => null,
                    // other properties
                    'id'            => $row['EMPLOYEE_ID'],
                    'shortcut'      => $row['SHORTCUT'],
                    'first_name'    => $row['FIRST_NAME'],
                    'last_name'     => $row['LAST_NAME'],
                    'isGroup'       => false
                ];
            }
        }

        return array_values($result);
    }
}
