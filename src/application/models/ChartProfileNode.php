<?php
class ChartProfileNode extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'BC_CHART_PROFIL_X_NODE';
    // @codingStandardsIgnoreEnd
    
    public function getNodes($profileId)
    {
        $select = $this->getAdapter()->select()
            ->from($this->_name, '*')
            ->joinLeft(
                'node_version',
                'node_version.node_id = coalesce(bc_chart_profil_x_node.node_id, bc_chart_profil_x_node.single_node_id) AND node_version.version_state_id = 1',
                [
                    'node_name' => new Zend_Db_Expr("
                        CASE
                            WHEN node_version.long_name IS NOT null THEN node_version.name || ' [' || node_version.long_name || ']'
                            WHEN node_version.customer_name IS NOT null THEN node_version.name || ' [' || node_version.customer_name || ']'
                            ELSE node_version.name
                        END
                    "),
                    'node_type_id',
                    'structure_unit_type_id'
                ]
            )
            ->joinLeft(
                'nodeset',
                'nodeset.id = bc_chart_profil_x_node.set_id',
                ['set_name' => 'name']
            )
            ->where('bc_chart_profil_id = ?', $profileId);

        $result = [];
        $queryResult = $select->query()->fetchAll();
        foreach ($queryResult as $item) {
            array_push($result, self::createNode($item));
        }

        return $result;
    }

    public function setNodes($profileId, $nodes = array())
    {
        $this->getAdapter()->beginTransaction();
        try {
            // Remove all nodes for a profil
            $this->getAdapter()->delete($this->_name, $this->getAdapter()
                ->quoteInto('BC_CHART_PROFIL_ID = ?', $profileId));
            
            //Add nodes to a profile
            foreach ($nodes as $item) {
                if ((bool)$item->isSet) {
                    $data = array(
                        'BC_CHART_PROFIL_ID' => $profileId,
                        'SET_ID' => (int)$item->id,
                        'NODE_ID' => null,
                        'SINGLE_NODE_ID' => null,
                        'SINGLE_NODE_TYPES' => null
                    );
                } else {
                    $data = array(
                        'BC_CHART_PROFIL_ID' => $profileId,
                        'SET_ID' => null,
                        'NODE_ID' => (bool)$item->checked ? (int)$item->id : null,
                        'SINGLE_NODE_ID' => $item->singleNodeTypes ? (int)$item->id : null,
                        'SINGLE_NODE_TYPES' => $item->singleNodeTypes ? (int)$item->singleNodeTypes : null
                    );
                }
                
                $this->getAdapter()->insert($this->_name, $data);
            }
        } catch (Exception $e) {
            $this->getAdapter()->rollback();
            return false;
        }
        $this->getAdapter()->commit();
        return true;
    }

    public function copy($srcProfileId, $destProfileId)
    {
        $this->getAdapter()->beginTransaction();
        try {
            // Remove all nodes for a profil
            $this->getAdapter()
                ->delete($this->_name, $this->getAdapter()
                ->quoteInto('bc_chart_profil_id = ?', $destProfileId));

            $sql = "
                INSERT INTO bc_chart_profil_x_node (
                    bc_chart_profil_id,
                    node_id,
                    set_id,
                    single_node_id,
                    single_node_types
                )
                SELECT
                    :destProfileId,
                    node_id,
                    set_id,
                    single_node_id,
                    single_node_types
                FROM bc_chart_profil_x_node
                WHERE bc_chart_profil_id = :srcProfileId
            ";
            $statment = $this->getAdapter()->prepare($sql);
            $statment->bindValue(':destProfileId', $destProfileId);
            $statment->bindValue(':srcProfileId', $srcProfileId);
            $statment->execute();
        } catch (Exception $e) {
            $this->getAdapter()->rollback();
            return false;
        }
        $this->getAdapter()->commit();
        return true;
    }

    public static function createNode($item)
    {
        if (!is_null($item['SET_ID'])) {
            return array(
                'uid'       => 's' . $item['SET_ID'],
                'id'        => $item['SET_ID'],
                'name'      => $item['SET_NAME'],
                'isSet'     => true,
                'checked'   => true,
                'iconCls'   => Set::getIconCls()
            );
        } elseif (!is_null($item['NODE_ID']) || !is_null($item['SINGLE_NODE_ID'])) {
            $id = !is_null($item['NODE_ID']) ? $item['NODE_ID'] : $item['SINGLE_NODE_ID'];
            return array(
                'uid'                   => 'n' . $id,
                'id'                    => $id,
                'isSet'                 => false,
                'name'                  => $item['NODE_NAME'],
                'checked'               => !is_null($item['NODE_ID']),
                'singleNodeTypes'       => $item['SINGLE_NODE_TYPES'],
                'nodeTypeId'            => $item['NODE_TYPE_ID'],
                'structureUnitTypeId'   => $item['STRUCTURE_UNIT_TYPE_ID'],
                'path_id'               => isset($item['NODE_PATH_ID']) ? $item['NODE_PATH_ID'] : null,
                'path_name'             => isset($item['NODE_PATH_NAME']) ? $item['NODE_PATH_ID'] : null,
                'iconCls'               => Node::getIconCls($item['NODE_TYPE_ID'])
            );
        }
        return false;
    }
}
