<?php
/**
 * NodeType
 */
class NodeType extends Iso_Model_IsoModel
{
    const STRUCTURE_UNIT    = 1;
    const DEBTOR            = 2;
    const ORDER            = 3;
    const UNIT_OF_ORDER    = 4;
    const ACTIVITY            = 5;
    const SAP_BOOKING        = 6;

    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'NODE_TYPE';
    protected $_primary = 'ID';
    
    protected $_columns = array(
        'ID', 'SHORTCUT'
    );
    // @codingStandardsIgnoreEnd
    
    /**
     * Model constructor
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->_db = Zend_Registry::get('db');
    }
    
    /**
     * Returns node types
     *
     * @param string $language
     * @return array
     */
    public function getNodeTypes($language)
    {
        // language switch
        $columns = $this->_columns;
        $columns['NAME'] = 'NAME_' . strtoupper($language);
        
        $select = $this->select()
            ->from($this->_name, $columns);
        
        return $select->query()->fetchAll();
    }
    
    /**
     * Returns node types as flattened array
     *
     * @param string $language
     * @return array
     */
    public function getNodeTypesAsArray($language)
    {
        $columns = $this->_columns;
        $columns['NAME'] = 'NAME_' . strtoupper($language);
        $select = $this->getAdapter()->select()
            ->from($this->_name, $columns)
            ->order(array('id'));
        
        $result = $select->query()->fetchAll();
                
        $nodeTypes = array();
        foreach ($result as $item) {
            $nodeTypes[$item['ID']] = $item['NAME'];
        }
        
        return $nodeTypes;
    }
}
