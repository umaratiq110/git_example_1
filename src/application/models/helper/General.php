<?php

class General
{
    
    /**
     * ten, 2009.06.09 iso-7465-i:
     * via regulaeren Ausdrucks die MeldungsIds aus einem Text auslesen
     */
    public static function extractJiraIds($text, $grantedJiraProjects)
    {
        $regex = '([a-zA-Z_]+-[a-zA-Z]*[0-9]+(-[iI])?)';
        if (preg_match_all($regex, $text, $matches)) {
            $jiraids_arr = [];
            foreach (array_unique($matches[0]) as $value) {
                $projectKey = explode('-', $value)[0];
                if (in_array($projectKey, $grantedJiraProjects)) {
                    $jiraids_arr[] = $value;
                }
            }
            if (count($jiraids_arr) > 0) {
                return $jiraids_arr;
            } else {
                return null;
            }
        }
        
        return null;
    }

    /**
     * Wandelt einen DB-Kommawert in ein float Wert um
     */
    public static function toFloat($value)
    {
        return floatval(str_replace(',', '.', $value));
    }

    /**
     *
     * Makes sure that there are always 2 digits after the decimalseparator
     *
     * e.g. '10' is transformed to '10.00'
     *      '10.5' is transformed to '10.50'
     *
     * @param $number the number to format
     */
    public static function formatNumber($number)
    {
        if (preg_match('/^[0-9]+$/', $number)) {
            return $number . '.00';
        }
    
        if (preg_match('/^[0-9]+\.[0-9]$/', $number)) {
            return $number . '0';
        }
    
        return $number;
    }
    
    /**
    * Checks if node is child of another node
    *
    * @param int $nodeToCheckId
    * @param int $nodeId
    */
    public static function isChildOfNode($nodeToCheckId, $nodeId)
    {
        $sql = "
			select
				count(*) as rec_count
			from node n
			inner join (
			    WITH RECURSIVE nodes AS (
			        SELECT id FROM node 
			        WHERE id = :id
			        UNION ALL 
			        SELECT node.id from node 
			        INNER JOIN nodes ON nodes.id = node.parent_node_id
			    ) SELECT id FROM nodes 
			) cn on n.id = cn.id
			where n.id = :toCheckId
		";
        
        $stmt = Zend_Registry::get('db')->prepare($sql);
        $stmt->bindParam(":toCheckId", $nodeToCheckId);
        $stmt->bindParam(":id", $nodeId);
        $stmt->execute();
        $result = (int)$stmt->fetch(Zend_Db::FETCH_COLUMN);
    
        return $result !== 0;
    }
}
