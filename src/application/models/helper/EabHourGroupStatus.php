<?php
/**
 * eAB hour group status
 */
class EabHourGroupStatus
{
    const ACCOUNTABLE = 1;
    const EFFORT_FREE_OF_CHARGE = 2;
    const PENDING = 3;
    const NOT_IN_CONFIRMATION_OF_EFFORT = 4;

    public static function getAll()
    {
        $reflection = new ReflectionClass(__CLASS__);
        return $reflection->getConstants();
    }
}
