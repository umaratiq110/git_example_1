<?php
/**
 * Locked nodes
 */
class LockedNodes
{
    const WITH_LOCKED_NODES    = 1;
    const WITHOUT_LOCKED_NODES = 0;
    
    public static function getAll()
    {
        $reflection = new ReflectionClass(__CLASS__);
        return $reflection->getConstants();
    }
}
