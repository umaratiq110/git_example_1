<?php

/**
 * Display figure
 */
class DisplayFigure
{

    const ONCE_FOR_WHOLE_PERIOD    = 0;
    const PER_MONTH        = 1;
    
    public static function getAll()
    {
        $reflection = new ReflectionClass('DisplayFigure');
        return $reflection->getConstants();
    }
}
