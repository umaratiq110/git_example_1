<?php
/**
 * ReportType
 */
class ReportType extends Iso_Model_IsoModel
{
    const BOOKING_OVERVIEW        = 1;
    const TABLE                   = 2;
    const LISTE                   = 3;
    const DIAGRAM                 = 4;
    const SAP_KEY_HOURS           = 5;
    const ACCOUNTING              = 6;
    const NOT_BOOKED              = 7;
    const MONTH_END               = 8;
    const REBOOK                  = 9;
    const REBOOK_LOG              = 10;
    const BT_ORDER                = 11;
    const BT_MODIFICATION_LIST    = 12;
    const PERMISSION              = 13;
    const STANDBY                 = 14;
    const ABSENCE_CALENDAR        = 15;
    const MIS_DIVISION            = 16;
    const MIS_OVERALL             = 17;
    const STATUS                  = 18;
    const ORDER_OVERVIEW          = 19;

    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'BC_REPORT_TYPE';
    protected $_primary = 'ID';
    
    protected $_columns = array(
        'ID', 'SHORTCUT'
    );
    // @codingStandardsIgnoreEnd
    
    /**
     * Model constructor
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->_db = Zend_Registry::get('db');
    }
    
    /**
     * Returns booking types
     *
     * @param string $language
     * @return array
     */
    public function getReportTypes($language)
    {
        // language switch
        $columns = $this->_columns;
        $columns['NAME'] = 'NAME_' . strtoupper($language);
        
        $select = $this->select()
            ->from($this->_name, $columns);
        
        return $select->query()->fetchAll();
    }
}
