<?php
/**
 * Helper class to simplify building parts of SQL queries
 *
 * @author les
 */
class QueryBuilder
{

    /**
     * Creates in clause and splits it into 1000 chunks when contains > 1000 items
     * Returns string in format:
     * (fieldName IN (1,2,3,4,....) OR fieldName IN (1001,1002...) ... )
     *
     * Last parameter denotes if values are integers or string values (they will be enclosed by '')
     *
     * @param string $fieldName
     * @param array $data
     * @param bool $intValues
     */
    public static function createInClause($fieldName, $data = array(), $intValues = true)
    {
        if (false == is_array($data)) {
            return "";
        }
        
        $data = array_filter($data);
        
        if (empty($data)) {
            return "";
        }
        
        $chunks = array_chunk($data, 1000);
        
        $inClause = '';
        foreach ($chunks as $i => $chunk) {
            if (0 < $i) {
                $inClause .= ' OR ';
            }
            if ($intValues) {
                $inClause .= $fieldName . " IN (" . implode(",", $chunk) . ")";
            } else {
                $inClause .= $fieldName . " IN ('" . implode("','", $chunk) . "')";
            }
        }
        return "(" . $inClause . ")";
    }
    
    /**
     * Joins arrays items to create conditions glued with 'OR'
     *
     * @param string $prefix
     * @param array $conditions
     */
    public static function conditionOr($prefix, $conditions)
    {
        return self::createCondition($prefix, $conditions, "OR");
    }
    
    /**
     * Joins arrays items to create conditions glued with 'AND'
     *
     * @param string $prefix
     * @param array $conditions
     */
    public static function conditionAnd($prefix, $conditions)
    {
        return self::createCondition($prefix, $conditions, "AND");
    }
    
    /**
     * Joins arrays items to create conditions glued with specified link
     *
     * @param string $prefix
     * @param array $conditions
     * @param string $link
     */
    private static function createCondition($prefix, $conditions, $link)
    {
        $conditions = array_filter($conditions);

        if (empty($conditions)) {
            return "";
        }

        return " " . $prefix . " (" . implode(" " . $link . " ", $conditions) . ") ";
    }
}
