<?php
/**
 * StructureUnitType model
 */
class StructureUnitType extends Iso_Model_IsoModel
{
    const COMPANY        = 1;
    const DIVISION        = 2;
    const SUBDIVISION    = 3;
    const OTHER        = 4;

    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'STRUCTURE_UNIT_TYPE';
    protected $_primary = 'ID';
    
    protected $_columns = array(
        'ID'
    );
    // @codingStandardsIgnoreEnd
    
    /**
     * Model constructor
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->_db = Zend_Registry::get('db');
    }
    
    /**
     * Returns structure unit types
     *
     * @param string $language
     * @return array
     */
    public function getStructureUnitTypes($language)
    {
        // language switch
        $columns = $this->_columns;
        $columns['NAME'] = 'NAME_' . strtoupper($language);
        
        $select = $this->select()
            ->from($this->_name, $columns);
        
        return $select->query()->fetchAll();
    }
    
    /**
     * Returns structure unit types as flattened array
     *
     * @param string $language
     * @return array
     */
    public function getStructureUnitTypesAsArray($language)
    {
        $columns = $this->_columns;
        $columns['NAME'] = 'NAME_' . strtoupper($language);
        $select = $this->getAdapter()->select()
            ->from($this->_name, $columns)
            ->order(array('id'));
        
        $result = $select->query()->fetchAll();
                
        $unitTypes = array();
        foreach ($result as $item) {
            $unitTypes[$item['ID']] = $item['NAME'];
        }
        
        return $unitTypes;
    }
}
