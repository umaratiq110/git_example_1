<?php
/**
 * Serves as a base class to check if all required data is provided for a report type.
 *
 * @author les
 */
class ReportValidator
{
    protected $db;
    protected $translator;
    
    public function __construct($translator)
    {
        $this->db = Zend_Registry::get('db');
        $this->translator = $translator;
    }
    
    public function validate($profile)
    {
        $hint = false;
        
        if ($profile->ANZEIGE == 'F') {
            $nodeId = Chartprofil::getProfileNodes($profile->ID, $this->db, false);
            
            $lockedWhere = '';
            
            if ($profile->WITH_LOCKED_NODES == 1) {
                $lockedWhere = ' WHERE nodes.bookable = 1 ';
            }
            $sqlCount = /** @lang SQL */ "
                WITH RECURSIVE nodes AS (
                    SELECT
                        node.id,
                        node.bookable
                    FROM node
                    WHERE " . QueryBuilder::createInClause('node.id', $nodeId) . "
                    UNION ALL
                    SELECT
                        node.id,
                        node.bookable
                    FROM node
                    INNER JOIN nodes ON nodes.id = node.parent_node_id
                ) 
                SELECT count(*) FROM nodes
                " . $lockedWhere . "
            ";
        
            $statementCount = $this->db->prepare($sqlCount);
            $statementCount->execute();
            
            $count = $statementCount->fetch();
            
            if ($count['COUNT'] > 250) {
                $hint = true;
            } else {
                $hint = false;
            }
        }
        
        //Check if the ChartProfile is valid e.g. if the user lost some rights
        /** @var Zend_Db_Statement $statement */
        $statement = $this->db
            ->prepare("SELECT PACK_BKZCOCKPIT.isChartProfileValidWrapper(:PROFILE_ID)");
        $statement->bindValue(':PROFILE_ID', $profile->ID);
        $statement->execute();
        $isValid = $statement->fetchColumn();
        
        if ($isValid == 1) {
            return array('error' => false, 'hint' => $hint);
        } else {
            return array('error' => true, 'message' => $this->translator->translate('reportValidatorDataInvalid'));
        }
    }
}
