<?php
/**
 * SingleNodeType model
 */
class SingleNodeType
{
    // use ascending powers of 2
    // i.e. 1,2,4,8,16,...
    const MIS_INTERNAL    = 1;
    const MIS_SALES        = 2;
    const STATUS_WITHNEXTLEVEL = 4;
    const STATUS_DISTINCTROW = 8;
    const TABLE_DISPLAY_NODE = 16;
    const DIAGRAM_SINGLE_CC = 32;
    const REBOOK_TO_CC = 64;
    
    public static function getAll()
    {
        $reflection = new ReflectionClass(__CLASS__);
        return $reflection->getConstants();
    }
}
