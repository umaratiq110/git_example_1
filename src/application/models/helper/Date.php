<?php
class Date
{
    public static function determinePeriod($profileId, &$dateFrom, &$dateUntil, $db)
    {
        $sql = "CALL pack_bkzcockpit.determinePeriod(:PROFILE_ID, NULL, NULL)";
        $statement = $db->prepare($sql);
        $statement->bindValue(':PROFILE_ID', $profileId);
        $statement->execute();
        $result = $statement->fetch();
        
        if (isset($result['P_FROM']) && isset($result['P_UNTIL'])) {
            $dateFrom = new Zend_Date($result['P_FROM'], Iso_Date::ORACLE_DATE);
            $dateFrom->setTime(0);
            $dateUntil = new Zend_Date($result['P_UNTIL'], Iso_Date::ORACLE_DATE);
            $dateUntil->setTime(0);
        }
    }
    
    public static function generateMonthRange(Zend_Date $from, Zend_Date $until, $format = "yyyy/mm")
    {
        $db = Zend_Registry::get('db');
        $strFrom = $from->get(Iso_Date::ORACLE_DATE);
        $strUntil = $until->get(Iso_Date::ORACLE_DATE);
        
        $sql = /** @lang SQL */"
			WITH RECURSIVE months AS (
                SELECT
                    to_date(:START_DATE, 'YYYY-MM-DD') AS dt
                UNION ALL
                SELECT
                    (months.dt + INTERVAL '1' MONTH)::date AS dt
                FROM months
                WHERE dt + INTERVAL '1' MONTH < to_date(:END_DATE, 'YYYY-MM-DD')
            )
            SELECT to_char(months.dt, :FORMAT) as dt from months
		";
        /** @var PDOStatement $statement */
        $statement = $db->prepare($sql);
        $statement->bindValue('START_DATE', $strFrom);
        $statement->bindValue('END_DATE', $strUntil);
        $statement->bindValue('FORMAT', $format);
        $statement->execute();
        
        return $statement->fetchAll(Zend_Db::FETCH_COLUMN);
    }
}
