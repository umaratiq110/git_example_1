<?php
/**
 * Report renderer class renders a chart profile as a type of report
 */
class ReportRenderer
{
    /**
     * @var Zend_Cache_Core
     */
    protected $cache;

    /**
     * @var Zend_Session_Namespace
     */
    protected $session;

    /**
     * @var Zend_Config
     */
    protected $config;

    /**
     * @var Zend_Db_Adapter_Abstract
     */
    protected $db;

    /**
     * @var Zend_Log
     */
    protected $log;

    protected $employee;
    protected $translate;
    protected $generationType = GenerationLog::REPORT_GENERATION_TYPE_MANUAL;
    
    public function __construct($session, $employee, $translate, $generationType = null)
    {
        $this->session = $session;
        $this->employee = $employee;
        $this->translate = $translate;

        if ($generationType === GenerationLog::REPORT_GENERATION_TYPE_CYCLIC) {
            $this->generationType = GenerationLog::REPORT_GENERATION_TYPE_CYCLIC;
        }
        
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $this->config = $bootstrap->getResource('config');
        $this->db = $bootstrap->getResource('db');
        $this->cache = $bootstrap->getResource('cache')->getCache('report');
        $this->log = $bootstrap->getResource('log');
    }
    
    /**
     * Validates and renders report with the given id.
     * @param $profileId
     * @param $timestamp
     */
    public function validateAndRender($controller, $profileId, $timestamp = null)
    {
        $validator = new ReportValidator($this->translate);
        $chartprofil = new ChartProfil();
        $profile = $chartprofil->find($profileId)->current();
        
        // Validate if report has all necessary data
        $result = $validator->validate($profile);
        if (isset($result['error']) && true === $result['error']) {
            return $result;
        }
        
        return $this->render($controller, $profileId, $timestamp);
    }
    
    /**
     * Renders report for direct download.
     *
     * @param $controller
     * @param $profileId
     * @param $timestamp
     * @return array
     */
    public function renderToDownload($controller, $profileId, $timestamp = null)
    {
        $chartprofil = new ChartProfil();
        $profile = $chartprofil->find($profileId)->current();
        
        $result = $this->render($controller, $profileId, $timestamp);
        $controller->initView();
        $content = null;
        
        switch ($result['type']) {
            case 'F':
                $controller->view->data = $this->session->btOrder[$result['timestamp']];
                $controller->view->user = $controller->getUser()->getKuerzel();
                
                $filename = 'btOrder_' . date("Ymd", time()) . '.xlsx';

                $this->session->btOrder[$result['timestamp']]['filename'] = $filename;
                $this->session->btOrder[$result['timestamp']]['contentType'] = 'application/vnd.openXMLformats-officedocument.spreadsheetml.sheet';
                
                //write the node types of selected nodes in view
                # very likely to be a bug to set the profileId to the userId but no one knows for sure...
                $profileData = (object) array("ID" => $controller
                    ->getUser()->getId(), "NOTIZTEXT" => null, "ANZEIGE" => 'F');
                Report::setExportHeaderData($controller, $profileData, $controller->getUser());
                
                $exportObj = new Export($controller->view, $this->translate, $this->config->main->export->save_path, $filename);
                $content = $exportObj->exportBtOrder();
                break;
            case 'I':
                $controller->view->data = $this->session->misDivision[$result['timestamp']];
                $controller->view->user = $controller->getUser()->getKuerzel();
                
                $filename = 'MISBereich_' . ($profile->BEZEICHNUNG != null ? $profile->BEZEICHNUNG . '_' : '')
                    . date("Y-m-d", time()) . '.xlsx';

                $this->session->misDivision[$result['timestamp']]['filename'] = $filename;
                $this->session->misDivision[$result['timestamp']]['contentType'] = 'application/vnd.openXMLformats-officedocument.spreadsheetml.sheet';
                
                Report::setExportHeaderData($controller, $profile, $controller->getUser());
                
                $exportObj = new Export($controller->view, $this->translate, $this->config->main->export->save_path, $filename);
                $content = $exportObj->exportMisDivision();
                break;
            case 'J':
                $controller->view->data = $this->session->misOverall[$result['timestamp']];
                $controller->view->user = $controller->getUser()->getKuerzel();
                
                $filename = 'MISGesamt_' . ($profile->BEZEICHNUNG != null ? $profile->BEZEICHNUNG . '_' : '')
                    . date("Y-m-d", time()) . '.xlsx';

                $this->session->misOverall[$result['timestamp']]['filename'] = $filename;
                $this->session->misOverall[$result['timestamp']]['contentType'] = 'application/vnd.openXMLformats-officedocument.spreadsheetml.sheet';
                
                Report::setExportHeaderData($controller, $profile, $controller->getUser());
                
                $exportObj = new Export($controller->view, $this->translate, $this->config->main->export->save_path, $filename);
                $content = $exportObj->exportMisOverall($this->session);
                break;
            case 'G':
                $controller->view->data = $this->session->accountableHours[$result['timestamp']];
                $controller->view->user = $controller->getUser()->getKuerzel();
                
                $filename = 'AccountableHours_' . ($profile->BEZEICHNUNG != null ? $profile->BEZEICHNUNG . '_' : '')
                    . date("Y-m-d", time()) . '.xlsx';

                $this->session->accountableHours[$result['timestamp']]['filename'] = $filename;
                $this->session->accountableHours[$result['timestamp']]['contentType'] = 'application/vnd.openXMLformats-officedocument.spreadsheetml.sheet';
                
                Report::setExportHeaderData($controller, $profile, $controller->getUser());
                
                $exportObj = new Export($controller->view, $this->translate, $this->config->main->export->save_path, $filename);
                $content = $exportObj->exportAccountableHours();
                break;
            case 'Q':
                $controller->view->data = $this->session->contingentRange[$result['timestamp']];
                $controller->view->user = $controller->getUser()->getKuerzel();

                $filename = 'Kontingentreichweite_' . ($profile->BEZEICHNUNG != null ? $profile->BEZEICHNUNG . '_' : '')
                    . date("Y-m-d", time()) . '.xlsx';

                $this->session->contingentRange[$result['timestamp']]['filename'] = $filename;
                $this->session->contingentRange[$result['timestamp']]['contentType'] = 'application/vnd.openXMLformats-officedocument.spreadsheetml.sheet';

                Report::setExportHeaderData($controller, $profile, $controller->getUser());

                $exportObj = new Export($controller->view, $this->translate, $this->config->main->export->save_path, $filename);
                $content = $exportObj->exportContingentRange();
                break;
            case 'Z':
                $controller->view->data = $this->session->contingentData[$result['timestamp']];
                $controller->view->user = $controller->getUser()->getKuerzel();

                $filename = 'KV-Daten_' . ($profile->BEZEICHNUNG != null ? $profile->BEZEICHNUNG . '_' : '')
                    . date("Y-m-d", time()) . '.xlsx';

                $this->session->contingentData[$result['timestamp']]['filename'] = $filename;
                $this->session->contingentData[$result['timestamp']]['contentType'] = 'application/vnd.openXMLformats-officedocument.spreadsheetml.sheet';

                Report::setExportHeaderData($controller, $profile, $controller->getUser());

                $exportObj = new Export($controller->view, $this->translate, $this->config->main->export->save_path, $filename);
                $content = $exportObj->exportContingentData();
                break;
            case 'Y':
                $controller->view->data = $this->session->prismaValues[$result['timestamp']];
                $controller->view->user = $controller->getUser()->getKuerzel();
                $date = new DateTime('now', new DateTimeZone('Europe/Berlin'));

                $filename = 'Prisma-Aktuelle-Werte_' . ($profile->BEZEICHNUNG != null ? $profile->BEZEICHNUNG . '_' : '')
                    . $date->format('Y-m-d_H-i') . '.xlsx';

                $this->session->prismaValues[$result['timestamp']]['filename'] = $filename;
                $this->session->prismaValues[$result['timestamp']]['contentType'] = 'application/vnd.openXMLformats-officedocument.spreadsheetml.sheet';

                Report::setExportHeaderData($controller, $profile, $controller->getUser());

                $exportObj = new Export($controller->view, $this->translate, $this->config->main->export->save_path, $filename);
                $content = $exportObj->exportPrismaValues();
                break;
        }

        if ($content) {
            $path = $this->config->main->download->save_path . $result['type'] . '_' . $result['timestamp'];

            file_put_contents($path, $content);
        }
        
        return $result;
    }

    /**
     * Renders report with the given id.
     *
     * @param $profileId
     * @param $timestamp
     * @return array
     */
    public function render($controller, $profileId, $timestamp = null, $language = null)
    {
        $timestamp = ($timestamp == null ? time() : $timestamp);
        $employeeInfo = '';
        $list = array();
        $chartprofil = new ChartProfil();

        $profile = $chartprofil->find($profileId)->current();
        $controller->initView();

        $reportTypes = (new ReportType())->getReportTypes('de');

        foreach ($reportTypes as $reportType) {
            if ($reportType["SHORTCUT"] === $profile->ANZEIGE) {
                $reportTypeId = $reportType["ID"];
                break;
            }
        }
        
        GenerationLog::insertGenerationEntry($reportTypeId, $this->employee->getid(), $this->generationType);

        switch ($profile->ANZEIGE) {
            case 'D':
                $result = ChartProfil::loadChartData($profile, $this->db, $this->employee);
                if ($result['DATA'] != null) {
                    $result['ACTHOURS'] = ChartProfil::loadChart($profile, $timestamp, $this->db, $this->cache, $this->translate, $result['DATA']);
                    $employeeInfo['booked'] = $result['DATA'][0]['MITARBEITERINFO'];
                }
                break;
            case 'L':
                $result = ChartProfil::loadList($profile, $timestamp, $this->db, $this->cache, $this->translate, $this->config, $this->log, $this->employee->getKuerzel());
                
                $list = $result["DATA"];
                $employeeInfo = $result["EMPLOYEEINFO"];
                break;
            case 'T':
                $result = ChartProfil::loadTable($profile, $timestamp, $this->db, $this->cache, $this->translate);
                
                $list = $result["DATA"];
                $employeeInfo = $result["EMPLOYEEINFO"];
                break;
            case 'E':
                $result = ChartProfil::loadEmployeeBooking($profile, $timestamp, $this->db);
                
                $list = $result['BOOKINGS'];
                $employeeInfo = $result['EMPLOYEEINFO'];
                break;
            case 'B':
                $result = ChartProfil::loadBooking($profile, $timestamp, $this->db, $this->cache, $this
                    ->translate, $this->employee);
                
                $list = $result['BOOKINGS'];
                $employeeInfo = $result['EMPLOYEEINFO'];
                break;
            case 'M':
                $result = ChartProfil::loadMonthEnd($profile, $timestamp, $this->db, $this->cache, $this->translate);
                
                $list = $result['BOOKINGS'];
                $employeeInfo = $result['EMPLOYEEINFO'];
                $summary = $result['SUMMARY'];
                break;
            case 'A':
                $result = ChartProfil::loadAccounting($profile, $timestamp, $this->db);
                $result['type'] = 'A';
                $list = $result;
                break;
            case 'R':
                $result = ChartProfil::loadRebook($profile, $timestamp, $this->db, $this->employee, $this->config, $this->log);
                
                $list = $result['BOOKINGS'];
                $employeeInfo = $result['EMPLOYEEINFO'];
                $summary = $result['SUMMARY'];
                break;
            case 'N':
                $result = ChartProfil::loadBtModifications($profile, $timestamp, $this->db, $this->cache);
                
                $list = $result['DATA'];
                break;
            case 'S':
                $result = ChartProfil::loadSAPKeyHours($profile, $timestamp, $this->db, $this->cache);
                
                $employeeInfo = $result["EMPLOYEEINFO"];
                $list = $result['DATA'];
                break;
            case 'P':
                $result = ChartProfil::loadPermission($profile, $timestamp, $this->db, $this->cache, $this->translate);
                
                $employeeInfo = $result["EMPLOYEEINFO"];
                $list = $result['DATA'];
                break;
            case 'O':
                $result = ChartProfil::loadRebookLog($profile, $timestamp, $this->db);
                
                $list = $result['DATA'];
                break;
            case 'F':
                $result = ChartProfil::loadBtOrder($profile, $timestamp, $this->db, $this->session, $this->translate);
                $result['type'] = 'F';
                $list = $result;
                break;
            case 'H':
                $result = ChartProfil::loadStandby($profile, $timestamp, $this->db, $this->cache);
                $list = $result['DATA'];
                $employeeInfo = $result['EMPLOYEEINFO'];
                break;
            case 'C':
                $result = ChartProfil::loadAbsenceCalendar($profile, $timestamp, $this->db, $this->cache, $this->translate);
                
                $list = $result['DATA'];
                $employeeInfo = $result['EMPLOYEEINFO'];
                break;
            case 'I':
                $result = ChartProfil::loadMisDivision($profile, $timestamp, $this->db, $this->session, $this->translate);
                $result['type'] = 'I';
                $list = $result;
                break;
            case 'J':
                $result = ChartProfil::loadMisOverall($profile, $timestamp, $this->db, $this->session, $this->translate);
                $result['type'] = 'J';
                $list = $result;
                break;
            case 'K':
                $result = ChartProfil::loadStatus($profile, $timestamp, $this->db, $this->session, $this->translate, $this->cache);
                $result['type'] = 'K';
                
                $controller->view->data = $result['DATA'];
                $controller->view->language =  $language;
                unset($result['DATA']);
                $result['view'] = $controller->view->render('report/status.phtml');
                
                $list = $result;
                break;
            case 'G':
                $result = ChartProfil::loadAccountableHours($profile, $timestamp, $this->db, $this->session, $this->translate);
                $result['type'] = 'G';
                $list = $result;
                break;
            case 'V':
                $result = ChartProfil::loadOrderOverview($profile, $timestamp, $this->db, $this->session, $this->translate, $this->cache);
                $result['type'] = 'V';

                $controller->view->data = $result['DATA'];
                $controller->view->language = $language;
                unset($result['DATA']);
                $result['view'] = $controller->view->render('report/orderOverview.phtml');

                $list = $result;
                break;
            case 'Q':
                $result = ChartProfil::loadContingentRange($profile, $timestamp, $this->db, $this->session, $this->translate);
                $result['type'] = 'Q';
                $list = $result;
                break;
            case 'Z':
                $result = ChartProfil::loadContingentData($profile, $timestamp, $this->db, $this->session, $this->translate);
                $result['type'] = 'Z';
                $list = $result;
                break;
            case 'X':
                $result = ChartProfil::loadContingentSapInvoiceComparison($profile, $timestamp, $this->db, $this->session, $this->translate);
                $result['type'] = 'X';
                $controller->view->data = $result['DATA'];
                unset($result['DATA']);
                $result['view'] = $controller->view->render('report/contingentSapInvoiceComparison.phtml');
                $list = $result;
                break;
            case 'Y':
                $result = ChartProfil::loadPrismaValues($profile, $timestamp, $this->db, $this->session, $this->translate);
                $result['type'] = 'Y';
                $list = $result;
                break;
        }

        $config = array();
        if (!in_array($profile->ANZEIGE, array('A', 'F'))) {
            $config = array(
                'mitarbeiterinfo'    => isset($employeeInfo) ? $employeeInfo : '',
                'summary'            => isset($summary) ? $summary : '',
                'profil'            => $profileId,
                'from'                => isset($result['FROM']) ? $result['FROM'] : null,
                'until'            => isset($result['UNTIL']) ? $result['UNTIL'] : null,
                'acthours'            => isset($result['ACTHOURS']) ? $result['ACTHOURS'] : null
            );
        }
        
        return array_merge($config, $list);
    }

    /**
     * Saves the report into a defined folder
     *
     * @param $controller
     * @param $profileId
     * @param $timestamp
     * @return array
     */
    public function saveAccounting($controller, $profileId, $timestamp = null)
    {
        $data = $this->render($controller, $profileId, $timestamp);
        $controller->initView();

        $directories = [];
        $result = [];
        foreach ($data['BOOKINGS'] as $directoryKey => $debtors) {
            // setup directory
            $dir = $this->config->main->accounting->save_path .
                $directoryKey . '-' . date('Y-m-d', $data['FROM']) . '-' . date('Y-m-d', $data['UNTIL']);

            if (file_exists($dir)) {
                $iterator = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS),
                    RecursiveIteratorIterator::CHILD_FIRST
                );
                foreach ($iterator as $path) {
                    $path->isDir() && !$path->isLink() ? rmdir($path->getPathname()) : unlink($path->getPathname());
                }
            } else {
                mkdir($dir);
            }
            $directories[] = $dir;

            // generate reports
            foreach ($debtors as $debtorName => $debtorProjects) {
                foreach ($debtorProjects as $projectName => $project) {
                    // only proceed if project has at least 1 accountable booking entry
                    if ($project['sumAcc'] == 0) {
                        continue;
                    }

                    $controller->view->dateStart = $data['FROM'];
                    $controller->view->dateEnd = $data['UNTIL'];
                    $controller->view->companyName = $project['company'];
                    $controller->view->debtorName = $debtorName;
                    $controller->view->projectName = $projectName;
                    $controller->view->bookings = $project['bookings'];
                    $controller->view->date = new Zend_Date();
                    $controller->view->pdf = new Iso_Pdf();
                    $controller->view->translate = $this->translate;
                    $controller->view->setEscape(array('Iso_Escaping', 'pdf'));

                    $content = $controller->view->render('report/accounting.ppdf');

                    if ($content) {
                        $path = $dir . DIRECTORY_SEPARATOR .
                            trim(substr($project['company'], 0, 3)) . '-' .
                            $debtorName . '-' .
                            Iso_Escaping::fileNames($projectName) . '.pdf';

                        file_put_contents($path, $content);

                        $filesAddedArr[] = $path;
                        $result = array('success' => true);
                    }
                }
            }
        }

        // remove directories which are empty (due to the exclusion of projects w/o any accountable hours)
        foreach ($directories as $dir) {
            if (!(new \FilesystemIterator($dir))->valid()) {
                rmdir($dir);
            }
        }
        return $result;
    }
}
