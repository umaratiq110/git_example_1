<?php
/**
 * EmployeeMapper
 */
class EmployeeMapper
{
    public static function pdbEmployee2PrismaEmployee($pdbEmp)
    {
        $db = Zend_Registry::get('db');
        
        $select = $db->select()
            ->from('employee', array('id'));
        
        if (is_numeric($pdbEmp)) {
            $select->join('mitarbeiter', 'upper(mitarbeiter.kuerzel) = upper(employee.shortcut)', array())
                   ->where('mitarbeiter.id = ?', (int)$pdbEmp);
        } else {
            $select->where('upper(shortcut) = upper(?)', (string)$pdbEmp);
        }
        
        if ($prismaEmpId = $select->query()->fetch(Zend_Db::FETCH_COLUMN)) {
            return (int)$prismaEmpId;
        }
        return null;
    }
    
    public static function prismaEmployee2PdbEmployee($prismaEmp)
    {
        $db = Zend_Registry::get('db');
        
        $select = $db->select()
            ->from('mitarbeiter', array('id'));
        
        if (is_numeric($prismaEmp)) {
            $select->join('employee', 'upper(employee.shortcut) = upper(mitarbeiter.kuerzel)', array())
                   ->where('employee.id = ?', (int)$prismaEmp);
        } else {
            $select->where('upper(kuerzel) = upper(?)', (string)$prismaEmp);
        }
        
        if ($pdbEmpId = $select->query()->fetch(Zend_Db::FETCH_COLUMN)) {
            return (int)$pdbEmpId;
        }
        
        return null;
    }
}
