<?php

/**
 * Invoice Type
 */
class InvoiceType
{
    const NOT_INVOICED          = 0;
    const PARTIALLY_INVOICED    = 1;
    const INVOICED              = 2;

    public static function getAll()
    {
        $reflection = new ReflectionClass('InvoiceType');
        return $reflection->getConstants();
    }

    public static function getIcon($value)
    {
        $path = 'files/images/';
        $type = '.png';

        switch ($value) {
            case self::NOT_INVOICED:
                return $path.'white_circle'.$type;
            break;
            case self::PARTIALLY_INVOICED:
                return $path.'white_green_circle'.$type;
            break;
            case self::INVOICED:
                return $path.'green_circle'.$type;
            break;
        }
        return false;
    }

    public static function getTranslation($value)
    {
        switch ($value) {
            case self::NOT_INVOICED:
                return 'orderOverviewNotInvoiced';
                break;
            case self::PARTIALLY_INVOICED:
                return 'orderOverviewPartiallyInvoiced';
                break;
            case self::INVOICED:
                return 'orderOverviewInvoiced';
                break;
        }
        return false;
    }
}
