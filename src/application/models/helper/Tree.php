<?php
/**
 * Tree helper
 */
class Tree
{
    /**
     * Gets tree of nodes and sets or employees and groups for a given employee
     *
     * @param bool $nodes
     * @param int $empId
     * @param object $translate
     */
    public static function getTree($nodes, $empId, $translate)
    {
        $parentId = 0;
        $outArray = array();
        
        if ($nodes === true) {
            $set = new Set($translate);
            $node = new Node($translate);
            
            $nodes = $node->getTree($empId);
            $outArray[] = array(
                'uid'        => 'n0',
                'parentId'    => $parentId,
                'text'        => $translate->translate('bookingTree'),
                'iconCls'    => 'folder',
                'leaf'        => empty($nodes),
                'expanded'    => !empty($nodes),
                'children'    => $nodes
            );
            
            $sets = $set->getTree($empId);
            $outArray[] = array(
                'uid'        => 's0',
                'parentId'    => $parentId,
                'text'        => $translate->translate('sets'),
                'iconCls'    => 'folder',
                'leaf'        => empty($sets),
                'children'    => $sets
            );
        } else {
            $group = new EmployeeGroup();
            $employee = new Employee();
            
            $employees = $employee->getTree($empId);
            $outArray[] = array(
                'uid'        => 'e0',
                'parentId'    => $parentId,
                'text'        => $translate->translate('distinctEmps'),
                'iconCls'    => 'folder',
                'leaf'        => false,
                'expanded'    => true,
                'children'    => array(
                    // active employees
                    array(
                        'uid'        => 'ea',
                        'parentId'    => 'e0',
                        'text'        => $translate->translate('activeEmps'),
                        'iconCls'    => 'folder',
                        'leaf'        => empty($employees[0]),
                        'expanded'    => false,
                        'children'    => $employees[0]
                    ),
                    // inactive employees
                    array(
                        'uid'        => 'ei',
                        'parentId'    => 'e0',
                        'text'        => $translate->translate('inactiveEmps'),
                        'iconCls'    => 'folder',
                        'leaf'        => empty($employees[1]),
                        'expanded'    => false,
                        'children'    => $employees[1]
                    )
                )
            );
            
            $groups = $group->getTree($empId);
            $outArray[] = array(
                'uid'        => 'g0',
                'parentId'    => $parentId,
                'text'        => $translate->translate('groups'),
                'iconCls'    => 'folder',
                'leaf'        => false,
                'expanded'    => true,
                'children'    => $groups
            );
        }
        return $outArray;
    }
}
