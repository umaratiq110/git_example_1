<?php

/**
 * Display lines
 */
class DisplayLines
{
    //only if there are hours or amounts in the selected period or in the general period
    const ONLY_IF_HOURS_AMOUNTS_IN_SELECTED_GENERAL_PERIOD = 0;
    //only if there are hours or amounts in the selected period
    const ONLY_IF_HOURS_AMOUNTS_IN_SELECTED_PERIOD = 1;
    //only if there are hours in the selected period
    const ONLY_IF_HOURS_IN_SELECTED_PERIOD = 2;
    
    public static function getAll()
    {
        $reflection = new ReflectionClass('DisplayLines');
        return $reflection->getConstants();
    }
}
