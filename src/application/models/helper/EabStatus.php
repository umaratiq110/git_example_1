<?php
/**
 * eAB status
 */
class EabStatus
{
    const IN_PROCESS = 1;
    const READY_FOR_CUSTOMER = 2;

    public static function getAll()
    {
        $reflection = new ReflectionClass(__CLASS__);
        return $reflection->getConstants();
    }
}
