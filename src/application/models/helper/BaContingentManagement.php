<?php
declare(strict_types=1);

class BaContingentManagement
{
    const DIVISION_01 = '01';
    const DIVISION_26 = '26';
    const DIVISION_27 = '27';
    const DIVISION_28 = '28';

    private static $supportedDivisions = [
        self::DIVISION_01,
        self::DIVISION_26,
        self::DIVISION_27,
        self::DIVISION_28,
    ];

    public static function getDivisionNames(): array
    {
        return self::$supportedDivisions;
    }
}
