<?php
/**
 * Report helper
 *
 * @author les
 */
class Report
{
    /**
     * Sets report header data for export
     *
     * @param $controller
     * @param $profilData
     * @param $user
     * @throws Zend_Date_Exception
     * @throws Zend_Exception
     * @throws Zend_Locale_Exception
     */
    public static function setExportHeaderData($controller, $profilData, $user)
    {
        $view = $controller->view;
        $db = Zend_Registry::get('db');
        $view->user = $user->getKuerzel();
        $view->locale = new Zend_Locale($user->getSprache());
        $dateFrom = $dateTo = null;
        
        if ($profilData != null) {
            if ($profilData->ANZEIGE != 'P' || $profilData->ANZEIGE != 'F') {
                # not sure if it is intended to run into this block for report type 'F' (bookingTree Order)
                # but it makes no sense to determine the period by a profileId that is the userId
                if ($profilData->ANZEIGE !== 'F') {
                    Date::determinePeriod($profilData->ID, $dateFrom, $dateTo, $db);
                }
                
                if (!($dateFrom instanceof Zend_Date)) {
                    $dateFrom = new Zend_Date();
                    $dateFrom->set(1, Zend_Date::DAY);
                }
                if (!($dateTo instanceof Zend_Date)) {
                    $dateTo = new Zend_Date();
                }
                $view->zeitraum_von = $dateFrom->get('yyyy/MM/dd');
                $view->zeitraum_bis = $dateTo->get('yyyy/MM/dd');
                
                if ($profilData->ANZEIGE == 'M') {
                    $controller->filename_suffix = '_' . $dateFrom->toString('yyyyMM');
                } else {
                    $controller
                        ->filename_suffix = '_' . $dateFrom->toString('yyyyMM') . '-' . $dateTo->toString('yyyyMM');
                }
            } elseif ($profilData->ANZEIGE == 'P') {
                $dateFrom = new Zend_Date();
                $controller->filename_suffix = '_' . $dateFrom->toString('yyyyMMdd');
            }
            $view->notiztext = $profilData->NOTIZTEXT;
            
            //Set filter
            if (isset($controller->filter_text) && $controller->filter_text != '') {
                $view->filter_text   = $controller->filter_text;
                $view->filter_column = $controller->filter_column;
            }
            
            // Fetch node types
            $sql = "SELECT id, shortcut FROM node_type";
            $statement = $db->prepare($sql);
            $statement->execute();
            $rows = $statement->fetchAll();
            
            $nodeTypes = array();
            foreach ($rows as $row) {
                $nodeTypes[$row['ID']] = $row['SHORTCUT'];
            }
            
            $view->nodeTypes = $nodeTypes;
        }
    }
}
