<?php

/**
 * BookingType
 */
class Division extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'DIVISION';
    protected $_primary = 'ID';

    protected $_columns = array(
        'ID', 'NAME', 'LONG_NAME'
    );

    // @codingStandardsIgnoreEnd

    public function __construct()
    {
        parent::__construct();

        $this->_db = Zend_Registry::get('db');
    }

    /**
     * Returns division
     *
     * @return array
     */
    public function getDivisions()
    {
        $this->_columns['ACTIVE'] = new Zend_Db_Expr(
            'CASE WHEN current_date BETWEEN coalesce(active_from,min_date()) AND coalesce(active_until,max_date())
            THEN 1 ELSE 0 END '
        );
        $select = $this->getAdapter()->select()
            ->from($this->_name, $this->_columns)
            ->order(array('name', 'long_name'));
        return $select->query()->fetchAll();
    }

    /**
     * @param array $names
     * @return mixed
     */
    public function getDivisionsByName(array $names)
    {
        $sql = sprintf(
            'SELECT * FROM DIVISION WHERE NAME IN (%s)',
            $this->_db->quote($names)
        );
        $stmt = $this->getAdapter()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
}
