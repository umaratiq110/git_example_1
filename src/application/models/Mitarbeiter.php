<?php
class Mitarbeiter extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    /**
     * Table name
     * @var string
     */
    protected $_name = 'MITARBEITER';

       /**
        * Primary key
        * @var string
        */
    protected $_primary = 'ID';
    // @codingStandardsIgnoreEnd
    
    
    
    public function getAuthorizedEmployees($company_id)
    {
        $select = $this->select()
                       ->from($this, array('ID', 'KUERZEL', 'NACHNAME', 'VORNAME'))
                       ->where('BKZCOCKPIT_RECHTE > ?', '0')
                       ->where('ID_FIRMA = ?', $company_id)
                       ->order('kuerzel');
                       
        $rows   = $this->fetchAll($select);

        return $rows;
    }

    public function getAvaibleEmployees($employee_id, $company_id)
    {
        $sql =  "SELECT MITARBEITER.ID, Mitarbeiter.Kuerzel ||' - '|| trim(Mitarbeiter.Vorname) || ' ' ||
                trim(Mitarbeiter.Nachname) as NAME, MITARBEITER.STATUS " .
                "FROM MITARBEITER " .
                "INNER JOIN FIRMA ON FIRMA.ID = MITARBEITER.ID_FIRMA " .
                //"WHERE Mitarbeiter.Status ='aktiv' ".
                "AND UPPER(FIRMA.Kuerzel) = UPPER(?) " .
                "AND MITARBEITER.KUERZEL != 'admin' " .
                "AND MITARBEITER.ID NOT IN(SELECT EMP_ID FROM BC_USER_X_EMP WHERE BC_USER_X_EMP.USER_ID = ?) " .
                "ORDER BY MITARBEITER.KUERZEL ";
                        
        $rows = $this->getAdapter()->fetchAll($sql, array($company_id, $employee_id));
        return $rows;
    }
    
    public function getRelatedEmployees($employee_id, $company_id)
    {
        $sql =  "SELECT MITARBEITER.ID, Mitarbeiter.Kuerzel ||' - '|| trim(Mitarbeiter.Vorname) || ' ' ||
                trim(Mitarbeiter.Nachname) as NAME, MITARBEITER.STATUS " .
                "FROM MITARBEITER " .
                "INNER JOIN FIRMA ON FIRMA.ID = MITARBEITER.ID_FIRMA " .
                //"WHERE Mitarbeiter.Status ='aktiv' ".
                "AND UPPER(FIRMA.Kuerzel) = UPPER(?) " .
                "AND MITARBEITER.KUERZEL != 'admin' " .
                "AND MITARBEITER.ID IN(SELECT EMP_ID FROM BC_USER_X_EMP WHERE BC_USER_X_EMP.USER_ID = ?) " .
                "ORDER BY MITARBEITER.KUERZEL ";
        
        $rows = $this->getAdapter()->fetchAll($sql, array($company_id, $employee_id));
        
        return $rows;
    }
    
    /**
     *
     * Returns all active bc users from own company except for the current user
     *
     */
    public function getAllBCUsers($employee_id, $company_id)
    {
        $sql = "
            SELECT
                m.kuerzel AS shortcut,
                m.id, m.kuerzel ||' - '|| trim(m.vorname) || ' ' || trim(m.nachname) AS name
            FROM mitarbeiter m
            INNER JOIN firma cmp ON cmp.id = m.id_firma
            INNER JOIN employee emp ON lower(m.kuerzel) = lower(emp.shortcut)
            INNER JOIN employee_state ON employee_state.employee_id = emp.id
            WHERE m.status = 'aktiv'
            AND employee_state.is_active = 1
            AND upper(cmp.kuerzel) = upper(?)
            AND m.kuerzel NOT IN ('admin', 'tooladmin')
            AND m.id != ?
            AND m.bkzcockpit_rechte > 0
            AND m.bkzcockpit_rechte & 1 > 0
            ORDER BY m.kuerzel
        ";

        return $this->getAdapter()->fetchAll($sql, [$company_id, $employee_id]);
    }


    /**
     * Gets all active bc users who is able to share
     *
     * @param $employee_id
     * @param $company_id
     * @return array
     */
    public function getAllAbleToShareBcUsers($employee_id, $company_id): array
    {
        $sql = "
            SELECT
                m.kuerzel AS shortcut,
                m.id, m.kuerzel ||' - '|| trim(m.vorname) || ' ' || trim(m.nachname) AS name
            FROM mitarbeiter m
            INNER JOIN firma cmp ON cmp.id = m.id_firma
            INNER JOIN employee emp ON lower(m.kuerzel) = lower(emp.shortcut)
            INNER JOIN employee_state ON employee_state.employee_id = emp.id
            INNER JOIN employee_node_permission ON employee_node_permission.employee_id = emp.id
            WHERE m.status = 'aktiv'
            AND employee_state.is_active = 1
            AND upper(cmp.kuerzel) = upper(?)
            AND m.kuerzel NOT IN ('admin', 'tooladmin')
            AND m.id != ?
            AND m.bkzcockpit_rechte > 0
            AND m.bkzcockpit_rechte & " . Iso_User::RIGHT_TO_MIS_DIVISION_OVERALL ." > 0
            AND employee_node_permission.node_id = 1
            ORDER BY m.kuerzel
        ";

        return $this->getAdapter()->fetchAll($sql, [$company_id, $employee_id]);
    }

    public function addRelation($employee_id, $employee_id_to_add)
    {
        $sql = "INSERT INTO BC_USER_X_EMP(ID,USER_ID,EMP_ID) VALUES(SEQ_BC_USER_X_EMP.NEXTVAL,:userid,:empid)";
        
        $statement = $this->getAdapter()->prepare($sql);
        $statement->bindValue(':userid', $employee_id);
        $statement->bindValue(':empid', $employee_id_to_add);
                
        $statement->execute();
    }
    
    public function removeRelation($employee_id, $employee_id_to_add)
    {
        $sql = "DELETE FROM BC_USER_X_EMP WHERE USER_ID = :userid AND EMP_ID = :empid";
        
        $statement = $this->getAdapter()->prepare($sql);
        $statement->bindValue(':userid', $employee_id);
        $statement->bindValue(':empid', $employee_id_to_add);
        
        $statement->execute();
    }
}
