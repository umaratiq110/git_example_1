<?php
/**
 * Table containing all bookings
 */
class TPWWorkingHours extends Iso_Model_IsoModel
{
    // Ignored because used in Zend class
    // @codingStandardsIgnoreStart
    protected $_name = 'TPWWORKINGHOURS';
    protected $_primary = 'CID';
    protected $_sequence = 'SEQ_TPWWORKINGHOURS';
    // @codingStandardsIgnoreEnd

    private $isAllowedToRebookAllBookings;

    public function __construct($isAllowedToRebookAllBookings)
    {
        parent::__construct();
        $this->isAllowedToRebookAllBookings = $isAllowedToRebookAllBookings;
        // Ignored because used in Zend class
        // @codingStandardsIgnoreStart
        $this->_db = Zend_Registry::get('db');
        // @codingStandardsIgnoreEnd
    }

    /**
     * Performs rebooking of hours to specified cost center.
     */
    public function rebook($bookings, $targetCC)
    {
        $bt = new BookingType();
        $bookingTypes = $bt->getBookingTypesAsArray();
        if ($targetCC === "") {
            $targetCC = null;
        }

        if (!$this->isAllowedToRebookAllBookings) {
            $sql = "
                SELECT 
                    to_char(node_version.lock_date, 'DD.MM.YYYY') AS lock_date
                FROM node
                INNER JOIN node_version
                    ON node_version.node_id = node.id
                    AND node_version.version_state_id = 1
                WHERE node.id = :id
            ";
            // Ignored because used in Zend class
            // @codingStandardsIgnoreStart
            $statement = $this->_db->prepare($sql);
            // @codingStandardsIgnoreEnd
            $statement->bindParam('id', $targetCC);
            $statement->execute();
            $lockDate = $statement->fetch(Zend_Db::FETCH_COLUMN);
            if ($lockDate) {
                $lockDate = new Zend_Date($lockDate, 'dd.MM.yyyy');
            } else {
                $lockDate = new Zend_Date('31.12.2099', 'dd.MM.yyyy');
            }
        }

        $result = array();
        $this->getAdapter()->beginTransaction();
        # cross schema updates/deletes done from a reporting tool...
        # need to set search_path to include intranet, otherwise the trigger/packages will fail
        $this->getAdapter()->query('SET search_path TO intranet,projektdb,public');
        try {
            foreach ($bookings as $item) {
                if ($this->isAllowedToRebookAllBookings || $this->validate($lockDate, $item)) {
                    $result = array_merge($result, $this->doRebooking($item, $targetCC, $bookingTypes));
                } else {
                    $this->getAdapter()->rollback();
                    return "targetCCLocked";
                }
            }
        } catch (Zend_Db_Exception $e) {
            $this->getAdapter()->rollback();
            Zend_Registry::get('log')->err($e);
            return false;
        } finally {
            # restoring default search_path for the user
            $this->getAdapter()->query('SET search_path TO DEFAULT');
        }

        $this->getAdapter()->commit();
        return $result;
    }

    protected function validate($targetCCLockDate, $item)
    {
        $date = new Zend_Date($item['date'], 'dd.MM.yyyy');
        return $date->isEarlier($targetCCLockDate);
    }

    /**
     * Gets booking by id.
     */
    protected function getBooking($id)
    {
        $select = $this->select()->where('CID = ?', $id);
        return $this->fetchRow($select);
    }

    /**
     * Internal method to perform rebooking of hours from a singe booking.
     */
    protected function doRebooking($booking, $targetCC, $bookingTypes)
    {
        $onlyNoteChange = false;
        $currBooking = $this->getBooking($booking['id']);
        $hours = empty($currBooking['CHOURS']) ? 0.0 : General::toFloat($currBooking['CHOURS']);
        $hoursAl = empty($currBooking['CHOURSAL']) ? 0.0 : General::toFloat($currBooking['CHOURSAL']);
        $affected = array();
        $sum = $hours + $hoursAl;

        if ($sum == 0.0) {
            return $affected;
        }

        $oldCompleteNote = self::buildCompleteWorkingsHoursNote([$currBooking['CNOTE'], $currBooking['CNOTEAL']]);

        $isNewNote = (strlen($booking['newNote']) > 0 || strlen($oldCompleteNote) > 0)
            && $booking['newNote'] !== $oldCompleteNote;

        if ($isNewNote) {
            $note = $booking['newNote'];
        } else {
            $note = $oldCompleteNote;
        }
        $noteAl = null;

        $btObj = new BookingType();
        $sumBookingType = 0;
        $btArr = array();
        foreach ($btObj->getBookingTypesAsArray() as $bt) {
            $btValue = General::toFloat($booking[$bt]);
            $btArr[$bt] = $btValue;
            $sumBookingType += $btValue;
        }

        $data = array(
            'CEMPLOYEE_ID'          => $currBooking['CEMPLOYEE_ID'],
            'CTPWZWSCCS_CZWS_ID'    => $currBooking['CTPWZWSCCS_CZWS_ID'],
            'CDATE'                 => $currBooking['CDATE'],
            'CFLAGPUBLICHDAY'       => $currBooking['CFLAGPUBLICHDAY'],
            'CNODE_ID'              => empty($targetCC) ? $currBooking['CNODE_ID'] : $targetCC
        );

        $btToBeRebooked = array_filter($btArr);

        //there is only booking type
        if (count($btToBeRebooked) === 1) {
            $keys = array_keys($btToBeRebooked);
            //same node and booking type as before, only note was changed, in this case use old id
            if ($currBooking['CNODE_ID'] == $data['CNODE_ID'] && array_search(reset($keys), $bookingTypes) === intval($currBooking['CBOOKING_TYPE_ID'])
                && $sumBookingType == $sum) {
                if ($isNewNote) {
                    $data['CID'] = $booking['id'];
                    $onlyNoteChange = true;
                } else {
                    //Nothing was changed
                    return $affected;
                }
            }
        }

        $this->getAdapter()->query("
            UPDATE intranet.package_configuration_bool 
            SET config_value = true 
            WHERE config_name = 'trg_rebookingActive'"
        );

        // Delete data first in order to ensure that trigger of TPWMONTH will calculate hours correctly
        if ($sumBookingType == $sum && !$onlyNoteChange) {
            $this->delete(array($this->getAdapter()->quoteInto('CID = ?', $booking['id'])));
        }

        foreach ($btToBeRebooked as $btKey => $btValue) {
            $this->setHoursAndNotes($data, $hours, $hoursAl, $note, $noteAl, $btValue, $sum);
            $data['CBOOKING_TYPE_ID'] = array_search($btKey, $bookingTypes);

            if (!$onlyNoteChange) {
                array_push($affected, $this->getRebookingData($currBooking, $data, $this->insert($data)));
            }
        }

        if ($sumBookingType != $sum) {
            $data = array();
            $data['CHOURS'] = $hours == 0.0 ? null : $hours;
            $data['CHOURSAL'] = $hoursAl == 0.0 ? null : $hoursAl;
            $this->update($data, array($this->getAdapter()->quoteInto('CID = ?', $currBooking['CID'])));
        } else if ($onlyNoteChange) {
            $this->update($data, array($this->getAdapter()->quoteInto('CID = ?', $currBooking['CID'])));
            array_push($affected, $this->getRebookingData($currBooking, $data, $data['CID']));
        }

        $this->getAdapter()->query("
            UPDATE intranet.package_configuration_bool 
            SET config_value = false 
            WHERE config_name = 'trg_rebookingActive'
        ");
        return $affected;
    }

    /**
     * Sets hours and notes for new booking entries
     */
    private function setHoursAndNotes(&$data, &$hours, &$hoursAl, $note, $noteAl, $value, $total)
    {
        if ($value == $total) {
            $data['CHOURS'] =  $hours == 0.0 ? null : $hours;
            $data['CHOURSAL'] = $hoursAl == 0.0 ? null : $hoursAl;
            $data['CNOTE'] = $note;
            $data['CNOTEAL'] = $noteAl;

            $hours = 0.0;
            $hoursAl = 0.0;

            return;
        }

        if ($hours >= $value) {
            $data['CHOURS'] = $value;
            $data['CHOURSAL'] = null;
            $data['CNOTE'] = $note;
            $data['CNOTEAL'] = $noteAl;

            $hours -= $value;
        } else {
            $data['CHOURS'] = $hours == 0.0 ? null : $hours;
            $data['CHOURSAL'] = ($value - $hours);
            $data['CNOTE'] = $note;
            $data['CNOTEAL'] = $noteAl;

            $hoursAl -= ($value - $hours);
            $hours = 0.0;
        }
    }

    /**
     * Gets rebooking data
     */
    private function getRebookingData($currBooking, $rebookedBooking, $bookingId)
    {
        return array(
            'previousCC'    => $currBooking['CNODE_ID'],
            'previousBT'    => $currBooking['CBOOKING_TYPE_ID'],
            'bookingId'    => $bookingId,
            'employeeId'    => $rebookedBooking['CEMPLOYEE_ID'],
            'nodeId'        => $rebookedBooking['CNODE_ID'],
            'bookingDate'    => $rebookedBooking['CDATE'],
            'hours'            => $rebookedBooking['CHOURS'],
            'hoursAL'        => $rebookedBooking['CHOURSAL'],
            'note'            => $rebookedBooking['CNOTE'],
            'oldNote'            => self::buildCompleteWorkingsHoursNote([$currBooking['CNOTE'], $currBooking['CNOTEAL']]),
            'noteAL'        => $rebookedBooking['CNOTEAL'],
            'btId'            => $rebookedBooking['CBOOKING_TYPE_ID']
        );
    }

    /**
     * @param array $notes
     * @return string
     */
    public static function buildCompleteWorkingsHoursNote(array $notes)
    {
        return implode(
            '; ',
            array_filter(
                $notes,
                function ($value) {
                    return $value !== null;
                }
            )
        );
    }
}
