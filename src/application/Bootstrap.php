<?php
/**
 * Application Bootstrap
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     * @var Zend_Registry
     */
    protected $registry;

    /**
     * Set up config
     *
     * @return Zend_Config
     * @throws Zend_Config_Exception
     */
    protected function _initConfig(): Zend_Config
    {
        $config = new Zend_Config($this->getOptions(), true);

        $localIni = APPLICATION_PATH . '/config/local.ini';
        if (file_exists($localIni) === true) {
            $config->merge(new Zend_Config_Ini($localIni, APPLICATION_ENV));
            $this->setOptions($config->toArray());
        }
        return $config;
    }

    /**
     * Set up log
     *
     * @return Zend_Log
     */
    protected function _initLog()
    {
        $log = $this->getPluginResource('log')->getLog();
        $this->getRegistry()->set('log', $log);
        return $log;
    }

    /**
     * Set up cache
     *
     * @return Zend_Cache_Manager
     */
    protected function _initCache()
    {
        $cacheManager = $this->getPluginResource('cacheManager')->getCacheManager();
        return $cacheManager;
    }

    /**
     * Set up Database Connection
     *
     * @return Zend_Db_Adapter_Abstract
     */
    protected function _initDb()
    {
        $this->bootstrap('config');
        $config = $this->getResource('config');

        Iso_Db_Profiler_File::setLogFile($config->resources->db->params->profiler->logfile);

        /** @var Zend_Application_Resource_Db $resource */
        $resource = $this->getPluginResource('db');
        $db = $resource->getDbAdapter();

        Zend_Db_Table::setDefaultAdapter($db);

        /** @var PDO $conn */
        $conn = $db->getConnection();
        $conn->setAttribute(Zend_Db::ATTR_STRINGIFY_FETCHES, true);
        # ensure column names are returned always upper case; dunno why (windows legacy?)
        $conn->setAttribute(Zend_Db::ATTR_CASE, PDO::CASE_UPPER);

        $this->getRegistry()->set('db', $db);

        return $db;
    }

    /**
     * Set up session
     *
     * @return Zend_Session_Namespace
     */
    protected function _initSessionNamespace()
    {
        $this->bootstrap(['config', 'session']);

        $config = $this->getResource('config');
        $session = new Zend_Session_Namespace('bkzcockpit');

        // reset session counter only if it's not timeoutcheck action
        if (PHP_SAPI !== 'cli' && !preg_match("/.*timeoutcheck.*/", $_SERVER['REQUEST_URI'])) {
            $session->setExpirationSeconds($config->resources->session->remember_me_seconds);
        }

        return $session;
    }

    /**
     * Set up mail
     *
     * @return void
     */
    protected function _initEmail()
    {
        $this->bootstrap('config');
        $config = $this->getResource('config');

        Iso_Mail::setDefaultRedirectRecipient($config->environment->mail);
    }

    /**
     * Set up PHP Server API
     *
     * @return void
     */
    protected function _initSapi()
    {
        $this->bootstrap('frontcontroller');
        $controller = $this->getResource('frontcontroller');

        if (PHP_SAPI === 'cli') {
            $controller->setRequest(new Iso_Controller_Request_Cli());
            $router = new Iso_Controller_Router_Cli();
        } else {
            $router = new Zend_Controller_Router_Rewrite();
        }
        $controller->setRouter($router);
    }

    /**
     * Set up plugins
     *
     * @return void
     * @throws Zend_Application_Bootstrap_Exception
     */
    protected function _initPlugins()
    {
        $this->bootstrap(['db', 'sessionNamespace']);

        $db = $this->getPluginResource('db')->getDbAdapter();
        $session = $this->getResource('session');

        Zend_Controller_Front::getInstance()->registerPlugin(
            new Iso_Controller_Plugin_SwitchedUser($session, $db)
        );
    }

    protected function _initPerformanceLogging()
    {
        $this->bootstrap(['config', 'db']);
        $config = $this->getResource('config');
        $logger = new Iso_Log_StopWatch($config, $this->getPluginResource('db')->getDbAdapter());

        $this->getRegistry()->set('performanceLog', $logger);
    }

    /**
     * @return Zend_Registry
     */
    private function getRegistry()
    {
        if (!isset($this->registry)) {
            $this->registry = Zend_Registry::getInstance();
        }
        return $this->registry;
    }
}
